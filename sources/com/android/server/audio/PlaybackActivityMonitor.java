package com.android.server.audio;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioPlaybackConfiguration;
import android.media.AudioPlaybackConfiguration.PlayerDeathMonitor;
import android.media.AudioSystem;
import android.media.IPlaybackConfigDispatcher;
import android.media.PlayerBase.PlayerIdCard;
import android.media.VolumeShaper.Configuration;
import android.media.VolumeShaper.Configuration.Builder;
import android.media.VolumeShaper.Operation;
import android.os.Binder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.ArrayUtils;
import com.android.server.audio.AudioEventLogger.Event;
import com.android.server.audio.AudioEventLogger.StringEvent;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class PlaybackActivityMonitor implements PlayerDeathMonitor, PlayerFocusEnforcer {
    private static final boolean DEBUG = false;
    private static final Configuration DUCK_ID = new Configuration(1);
    private static final Configuration DUCK_VSHAPE = new Builder().setId(1).setCurve(new float[]{0.0f, 1.0f}, new float[]{1.0f, 0.2f}).setOptionFlags(2).setDuration((long) MediaFocusControl.getFocusRampTimeMs(3, new AudioAttributes.Builder().setUsage(5).build())).build();
    private static final int FLAGS_FOR_SILENCE_OVERRIDE = 192;
    private static final Operation PLAY_CREATE_IF_NEEDED = new Operation.Builder(Operation.PLAY).createIfNeeded().build();
    private static final Operation PLAY_SKIP_RAMP = new Operation.Builder(PLAY_CREATE_IF_NEEDED).setXOffset(1.0f).build();
    public static final String TAG = "AudioService.PlaybackActivityMonitor";
    private static final int[] UNDUCKABLE_PLAYER_TYPES = new int[]{13, 3};
    private static final int VOLUME_SHAPER_SYSTEM_DUCK_ID = 1;
    private static final AudioEventLogger sEventLogger = new AudioEventLogger(100, "playback activity as reported through PlayerBase");
    private final ArrayList<Integer> mBannedUids = new ArrayList();
    private final ArrayList<PlayMonitorClient> mClients = new ArrayList();
    private final Context mContext;
    private final DuckingManager mDuckingManager = new DuckingManager();
    private boolean mHasPublicClients = false;
    private final int mMaxAlarmVolume;
    private final ArrayList<Integer> mMutedPlayers = new ArrayList();
    private final Object mPlayerLock = new Object();
    private final HashMap<Integer, AudioPlaybackConfiguration> mPlayers = new HashMap();
    private int mPrivilegedAlarmActiveCount = 0;
    private int mSavedAlarmVolume = -1;

    private static final class AudioAttrEvent extends Event {
        private final AudioAttributes mPlayerAttr;
        private final int mPlayerIId;

        AudioAttrEvent(int piid, AudioAttributes attr) {
            this.mPlayerIId = piid;
            this.mPlayerAttr = attr;
        }

        public String eventToString() {
            return new String("player piid:" + this.mPlayerIId + " new AudioAttributes:" + this.mPlayerAttr);
        }
    }

    private static final class DuckEvent extends Event {
        private final int mClientPid;
        private final int mClientUid;
        private final int mPlayerIId;
        private final boolean mSkipRamp;

        DuckEvent(AudioPlaybackConfiguration apc, boolean skipRamp) {
            this.mPlayerIId = apc.getPlayerInterfaceId();
            this.mSkipRamp = skipRamp;
            this.mClientUid = apc.getClientUid();
            this.mClientPid = apc.getClientPid();
        }

        public String eventToString() {
            return "ducking player piid:" + this.mPlayerIId + " uid/pid:" + this.mClientUid + "/" + this.mClientPid + " skip ramp:" + this.mSkipRamp;
        }
    }

    private static final class DuckingManager {
        private final HashMap<Integer, DuckedApp> mDuckers;

        private static final class DuckedApp {
            private final ArrayList<Integer> mDuckedPlayers = new ArrayList();
            private final int mUid;

            DuckedApp(int uid) {
                this.mUid = uid;
            }

            void dump(PrintWriter pw) {
                pw.print("\t uid:" + this.mUid + " piids:");
                for (Integer intValue : this.mDuckedPlayers) {
                    pw.print(" " + intValue.intValue());
                }
                pw.println("");
            }

            void addDuck(AudioPlaybackConfiguration apc, boolean skipRamp) {
                int piid = new Integer(apc.getPlayerInterfaceId()).intValue();
                if (!this.mDuckedPlayers.contains(Integer.valueOf(piid))) {
                    try {
                        PlaybackActivityMonitor.sEventLogger.log(new DuckEvent(apc, skipRamp).printLog(PlaybackActivityMonitor.TAG));
                        apc.getPlayerProxy().applyVolumeShaper(PlaybackActivityMonitor.DUCK_VSHAPE, skipRamp ? PlaybackActivityMonitor.PLAY_SKIP_RAMP : PlaybackActivityMonitor.PLAY_CREATE_IF_NEEDED);
                        this.mDuckedPlayers.add(Integer.valueOf(piid));
                    } catch (Exception e) {
                        Log.e(PlaybackActivityMonitor.TAG, "Error ducking player piid:" + piid + " uid:" + this.mUid, e);
                    }
                }
            }

            void removeUnduckAll(HashMap<Integer, AudioPlaybackConfiguration> players) {
                for (Integer intValue : this.mDuckedPlayers) {
                    int piid = intValue.intValue();
                    AudioPlaybackConfiguration apc = (AudioPlaybackConfiguration) players.get(Integer.valueOf(piid));
                    if (apc != null) {
                        try {
                            PlaybackActivityMonitor.sEventLogger.log(new StringEvent("unducking piid:" + piid).printLog(PlaybackActivityMonitor.TAG));
                            apc.getPlayerProxy().applyVolumeShaper(PlaybackActivityMonitor.DUCK_ID, Operation.REVERSE);
                        } catch (Exception e) {
                            Log.e(PlaybackActivityMonitor.TAG, "Error unducking player piid:" + piid + " uid:" + this.mUid, e);
                        }
                    }
                }
                this.mDuckedPlayers.clear();
            }

            void removeReleased(AudioPlaybackConfiguration apc) {
                this.mDuckedPlayers.remove(new Integer(apc.getPlayerInterfaceId()));
            }
        }

        private DuckingManager() {
            this.mDuckers = new HashMap();
        }

        synchronized void duckUid(int uid, ArrayList<AudioPlaybackConfiguration> apcsToDuck) {
            if (!this.mDuckers.containsKey(Integer.valueOf(uid))) {
                this.mDuckers.put(Integer.valueOf(uid), new DuckedApp(uid));
            }
            DuckedApp da = (DuckedApp) this.mDuckers.get(Integer.valueOf(uid));
            for (AudioPlaybackConfiguration apc : apcsToDuck) {
                da.addDuck(apc, false);
            }
        }

        synchronized void unduckUid(int uid, HashMap<Integer, AudioPlaybackConfiguration> players) {
            DuckedApp da = (DuckedApp) this.mDuckers.remove(Integer.valueOf(uid));
            if (da != null) {
                da.removeUnduckAll(players);
            }
        }

        synchronized void checkDuck(AudioPlaybackConfiguration apc) {
            DuckedApp da = (DuckedApp) this.mDuckers.get(Integer.valueOf(apc.getClientUid()));
            if (da != null) {
                da.addDuck(apc, true);
            }
        }

        synchronized void dump(PrintWriter pw) {
            for (DuckedApp da : this.mDuckers.values()) {
                da.dump(pw);
            }
        }

        synchronized void removeReleased(AudioPlaybackConfiguration apc) {
            DuckedApp da = (DuckedApp) this.mDuckers.get(Integer.valueOf(apc.getClientUid()));
            if (da != null) {
                da.removeReleased(apc);
            }
        }
    }

    private static final class NewPlayerEvent extends Event {
        private final int mClientPid;
        private final int mClientUid;
        private final AudioAttributes mPlayerAttr;
        private final int mPlayerIId;
        private final int mPlayerType;

        NewPlayerEvent(AudioPlaybackConfiguration apc) {
            this.mPlayerIId = apc.getPlayerInterfaceId();
            this.mPlayerType = apc.getPlayerType();
            this.mClientUid = apc.getClientUid();
            this.mClientPid = apc.getClientPid();
            this.mPlayerAttr = apc.getAudioAttributes();
        }

        public String eventToString() {
            return new String("new player piid:" + this.mPlayerIId + " uid/pid:" + this.mClientUid + "/" + this.mClientPid + " type:" + AudioPlaybackConfiguration.toLogFriendlyPlayerType(this.mPlayerType) + " attr:" + this.mPlayerAttr);
        }
    }

    private static final class PlayMonitorClient implements DeathRecipient {
        static final int MAX_ERRORS = 5;
        static PlaybackActivityMonitor sListenerDeathMonitor;
        final IPlaybackConfigDispatcher mDispatcherCb;
        int mErrorCount = 0;
        final boolean mIsPrivileged;

        PlayMonitorClient(IPlaybackConfigDispatcher pcdb, boolean isPrivileged) {
            this.mDispatcherCb = pcdb;
            this.mIsPrivileged = isPrivileged;
        }

        public void binderDied() {
            Log.w(PlaybackActivityMonitor.TAG, "client died");
            sListenerDeathMonitor.unregisterPlaybackCallback(this.mDispatcherCb);
        }

        boolean init() {
            try {
                this.mDispatcherCb.asBinder().linkToDeath(this, 0);
                return true;
            } catch (RemoteException e) {
                Log.w(PlaybackActivityMonitor.TAG, "Could not link to client death", e);
                return false;
            }
        }

        void release() {
            this.mDispatcherCb.asBinder().unlinkToDeath(this, 0);
        }
    }

    private static final class PlayerEvent extends Event {
        final int mPlayerIId;
        final int mState;

        PlayerEvent(int piid, int state) {
            this.mPlayerIId = piid;
            this.mState = state;
        }

        public String eventToString() {
            return "player piid:" + this.mPlayerIId + " state:" + AudioPlaybackConfiguration.toLogFriendlyPlayerState(this.mState);
        }
    }

    private static final class PlayerOpPlayAudioEvent extends Event {
        final boolean mHasOp;
        final int mPlayerIId;
        final int mUid;

        PlayerOpPlayAudioEvent(int piid, boolean hasOp, int uid) {
            this.mPlayerIId = piid;
            this.mHasOp = hasOp;
            this.mUid = uid;
        }

        public String eventToString() {
            return "player piid:" + this.mPlayerIId + " has OP_PLAY_AUDIO:" + this.mHasOp + " in uid:" + this.mUid;
        }
    }

    PlaybackActivityMonitor(Context context, int maxAlarmVolume) {
        this.mContext = context;
        this.mMaxAlarmVolume = maxAlarmVolume;
        PlayMonitorClient.sListenerDeathMonitor = this;
        AudioPlaybackConfiguration.sPlayerDeathMonitor = this;
    }

    public void disableAudioForUid(boolean disable, int uid) {
        synchronized (this.mPlayerLock) {
            int index = this.mBannedUids.indexOf(new Integer(uid));
            if (index >= 0) {
                if (!disable) {
                    this.mBannedUids.remove(index);
                }
            } else if (disable) {
                for (AudioPlaybackConfiguration apc : this.mPlayers.values()) {
                    checkBanPlayer(apc, uid);
                }
                this.mBannedUids.add(new Integer(uid));
            }
        }
    }

    private boolean checkBanPlayer(AudioPlaybackConfiguration apc, int uid) {
        boolean toBan = apc.getClientUid() == uid;
        if (toBan) {
            int piid = apc.getPlayerInterfaceId();
            try {
                Log.v(TAG, "banning player " + piid + " uid:" + uid);
                apc.getPlayerProxy().pause();
            } catch (Exception e) {
                Log.e(TAG, "error banning player " + piid + " uid:" + uid, e);
            }
        }
        return toBan;
    }

    public int trackPlayer(PlayerIdCard pic) {
        int newPiid = AudioSystem.newAudioPlayerId();
        AudioPlaybackConfiguration apc = new AudioPlaybackConfiguration(pic, newPiid, Binder.getCallingUid(), Binder.getCallingPid());
        apc.init();
        sEventLogger.log(new NewPlayerEvent(apc));
        synchronized (this.mPlayerLock) {
            this.mPlayers.put(Integer.valueOf(newPiid), apc);
        }
        return newPiid;
    }

    public void playerAttributes(int piid, AudioAttributes attr, int binderUid) {
        boolean handleAudioAttributesEvent;
        synchronized (this.mPlayerLock) {
            AudioPlaybackConfiguration apc = (AudioPlaybackConfiguration) this.mPlayers.get(new Integer(piid));
            if (checkConfigurationCaller(piid, apc, binderUid)) {
                sEventLogger.log(new AudioAttrEvent(piid, attr));
                handleAudioAttributesEvent = apc.handleAudioAttributesEvent(attr);
            } else {
                Log.e(TAG, "Error updating audio attributes");
                handleAudioAttributesEvent = false;
            }
        }
        if (handleAudioAttributesEvent) {
            dispatchPlaybackChange(false);
        }
    }

    private void checkVolumeForPrivilegedAlarm(AudioPlaybackConfiguration apc, int event) {
        if ((event != 2 && apc.getPlayerState() != 2) || (apc.getAudioAttributes().getAllFlags() & FLAGS_FOR_SILENCE_OVERRIDE) != FLAGS_FOR_SILENCE_OVERRIDE || apc.getAudioAttributes().getUsage() != 4 || this.mContext.checkPermission("android.permission.MODIFY_PHONE_STATE", apc.getClientPid(), apc.getClientUid()) != 0) {
            return;
        }
        int i;
        if (event == 2 && apc.getPlayerState() != 2) {
            i = this.mPrivilegedAlarmActiveCount;
            this.mPrivilegedAlarmActiveCount = i + 1;
            if (i == 0) {
                this.mSavedAlarmVolume = AudioSystem.getStreamVolumeIndex(4, 2);
                AudioSystem.setStreamVolumeIndex(4, this.mMaxAlarmVolume, 2);
            }
        } else if (event != 2 && apc.getPlayerState() == 2) {
            i = this.mPrivilegedAlarmActiveCount - 1;
            this.mPrivilegedAlarmActiveCount = i;
            if (i == 0 && AudioSystem.getStreamVolumeIndex(4, 2) == this.mMaxAlarmVolume) {
                AudioSystem.setStreamVolumeIndex(4, this.mSavedAlarmVolume, 2);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void playerEvent(int r11, int r12, int r13) {
        /*
        r10 = this;
        r9 = 2;
        r4 = 0;
        r5 = r10.mPlayerLock;
        monitor-enter(r5);
        r6 = r10.mPlayers;	 Catch:{ all -> 0x00aa }
        r7 = new java.lang.Integer;	 Catch:{ all -> 0x00aa }
        r7.<init>(r11);	 Catch:{ all -> 0x00aa }
        r0 = r6.get(r7);	 Catch:{ all -> 0x00aa }
        r0 = (android.media.AudioPlaybackConfiguration) r0;	 Catch:{ all -> 0x00aa }
        if (r0 != 0) goto L_0x0016;
    L_0x0014:
        monitor-exit(r5);
        return;
    L_0x0016:
        r6 = sEventLogger;	 Catch:{ all -> 0x00aa }
        r7 = new com.android.server.audio.PlaybackActivityMonitor$PlayerEvent;	 Catch:{ all -> 0x00aa }
        r7.<init>(r11, r12);	 Catch:{ all -> 0x00aa }
        r6.log(r7);	 Catch:{ all -> 0x00aa }
        if (r12 != r9) goto L_0x0065;
    L_0x0022:
        r6 = r10.mBannedUids;	 Catch:{ all -> 0x00aa }
        r3 = r6.iterator();	 Catch:{ all -> 0x00aa }
    L_0x0028:
        r6 = r3.hasNext();	 Catch:{ all -> 0x00aa }
        if (r6 == 0) goto L_0x0065;
    L_0x002e:
        r2 = r3.next();	 Catch:{ all -> 0x00aa }
        r2 = (java.lang.Integer) r2;	 Catch:{ all -> 0x00aa }
        r6 = r2.intValue();	 Catch:{ all -> 0x00aa }
        r6 = r10.checkBanPlayer(r0, r6);	 Catch:{ all -> 0x00aa }
        if (r6 == 0) goto L_0x0028;
    L_0x003e:
        r4 = sEventLogger;	 Catch:{ all -> 0x00aa }
        r6 = new com.android.server.audio.AudioEventLogger$StringEvent;	 Catch:{ all -> 0x00aa }
        r7 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00aa }
        r7.<init>();	 Catch:{ all -> 0x00aa }
        r8 = "not starting piid:";
        r7 = r7.append(r8);	 Catch:{ all -> 0x00aa }
        r7 = r7.append(r11);	 Catch:{ all -> 0x00aa }
        r8 = " ,is banned";
        r7 = r7.append(r8);	 Catch:{ all -> 0x00aa }
        r7 = r7.toString();	 Catch:{ all -> 0x00aa }
        r6.<init>(r7);	 Catch:{ all -> 0x00aa }
        r4.log(r6);	 Catch:{ all -> 0x00aa }
        monitor-exit(r5);
        return;
    L_0x0065:
        r6 = r0.getPlayerType();	 Catch:{ all -> 0x00aa }
        r7 = 3;
        if (r6 != r7) goto L_0x006e;
    L_0x006c:
        monitor-exit(r5);
        return;
    L_0x006e:
        r6 = checkConfigurationCaller(r11, r0, r13);	 Catch:{ all -> 0x00aa }
        if (r6 == 0) goto L_0x008e;
    L_0x0074:
        r10.checkVolumeForPrivilegedAlarm(r0, r12);	 Catch:{ all -> 0x00aa }
        r1 = r0.handleStateEvent(r12);	 Catch:{ all -> 0x00aa }
    L_0x007b:
        if (r1 == 0) goto L_0x0084;
    L_0x007d:
        if (r12 != r9) goto L_0x0084;
    L_0x007f:
        r6 = r10.mDuckingManager;	 Catch:{ all -> 0x00aa }
        r6.checkDuck(r0);	 Catch:{ all -> 0x00aa }
    L_0x0084:
        monitor-exit(r5);
        if (r1 == 0) goto L_0x008d;
    L_0x0087:
        if (r12 != 0) goto L_0x008a;
    L_0x0089:
        r4 = 1;
    L_0x008a:
        r10.dispatchPlaybackChange(r4);
    L_0x008d:
        return;
    L_0x008e:
        r6 = "AudioService.PlaybackActivityMonitor";
        r7 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00aa }
        r7.<init>();	 Catch:{ all -> 0x00aa }
        r8 = "Error handling event ";
        r7 = r7.append(r8);	 Catch:{ all -> 0x00aa }
        r7 = r7.append(r12);	 Catch:{ all -> 0x00aa }
        r7 = r7.toString();	 Catch:{ all -> 0x00aa }
        android.util.Log.e(r6, r7);	 Catch:{ all -> 0x00aa }
        r1 = 0;
        goto L_0x007b;
    L_0x00aa:
        r4 = move-exception;
        monitor-exit(r5);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.audio.PlaybackActivityMonitor.playerEvent(int, int, int):void");
    }

    public void playerHasOpPlayAudio(int piid, boolean hasOpPlayAudio, int binderUid) {
        sEventLogger.log(new PlayerOpPlayAudioEvent(piid, hasOpPlayAudio, binderUid));
    }

    public void releasePlayer(int piid, int binderUid) {
        synchronized (this.mPlayerLock) {
            AudioPlaybackConfiguration apc = (AudioPlaybackConfiguration) this.mPlayers.get(new Integer(piid));
            if (checkConfigurationCaller(piid, apc, binderUid)) {
                sEventLogger.log(new StringEvent("releasing player piid:" + piid));
                this.mPlayers.remove(new Integer(piid));
                this.mDuckingManager.removeReleased(apc);
                checkVolumeForPrivilegedAlarm(apc, 0);
                apc.handleStateEvent(0);
            }
        }
    }

    public void playerDeath(int piid) {
        releasePlayer(piid, 0);
    }

    protected void dump(PrintWriter pw) {
        pw.println("\nPlaybackActivityMonitor dump time: " + DateFormat.getTimeInstance().format(new Date()));
        synchronized (this.mPlayerLock) {
            pw.println("\n  playback listeners:");
            synchronized (this.mClients) {
                for (PlayMonitorClient pmc : this.mClients) {
                    pw.print(" " + (pmc.mIsPrivileged ? "(S)" : "(P)") + pmc.toString());
                }
            }
            pw.println("\n");
            pw.println("\n  players:");
            List<Integer> piidIntList = new ArrayList(this.mPlayers.keySet());
            Collections.sort(piidIntList);
            for (Integer piidInt : piidIntList) {
                AudioPlaybackConfiguration apc = (AudioPlaybackConfiguration) this.mPlayers.get(piidInt);
                if (apc != null) {
                    apc.dump(pw);
                }
            }
            pw.println("\n  ducked players piids:");
            this.mDuckingManager.dump(pw);
            pw.print("\n  muted player piids:");
            for (Integer intValue : this.mMutedPlayers) {
                pw.print(" " + intValue.intValue());
            }
            pw.println();
            pw.print("\n  banned uids:");
            for (Integer intValue2 : this.mBannedUids) {
                pw.print(" " + intValue2.intValue());
            }
            pw.println("\n");
            sEventLogger.dump(pw);
        }
    }

    private static boolean checkConfigurationCaller(int piid, AudioPlaybackConfiguration apc, int binderUid) {
        if (apc == null) {
            return false;
        }
        if (binderUid == 0 || apc.getClientUid() == binderUid) {
            return true;
        }
        Log.e(TAG, "Forbidden operation from uid " + binderUid + " for player " + piid);
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void dispatchPlaybackChange(boolean r10) {
        /*
        r9 = this;
        r6 = r9.mClients;
        monitor-enter(r6);
        r5 = r9.mClients;	 Catch:{ all -> 0x001b }
        r5 = r5.isEmpty();	 Catch:{ all -> 0x001b }
        if (r5 == 0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r6);
        return;
    L_0x000d:
        monitor-exit(r6);
        r6 = r9.mPlayerLock;
        monitor-enter(r6);
        r5 = r9.mPlayers;	 Catch:{ all -> 0x0037 }
        r5 = r5.isEmpty();	 Catch:{ all -> 0x0037 }
        if (r5 == 0) goto L_0x001e;
    L_0x0019:
        monitor-exit(r6);
        return;
    L_0x001b:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x001e:
        r2 = new java.util.ArrayList;	 Catch:{ all -> 0x0037 }
        r5 = r9.mPlayers;	 Catch:{ all -> 0x0037 }
        r5 = r5.values();	 Catch:{ all -> 0x0037 }
        r2.<init>(r5);	 Catch:{ all -> 0x0037 }
        monitor-exit(r6);
        r6 = r9.mClients;
        monitor-enter(r6);
        r5 = r9.mClients;	 Catch:{ all -> 0x0092 }
        r5 = r5.isEmpty();	 Catch:{ all -> 0x0092 }
        if (r5 == 0) goto L_0x003a;
    L_0x0035:
        monitor-exit(r6);
        return;
    L_0x0037:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x003a:
        r5 = r9.mHasPublicClients;	 Catch:{ all -> 0x0092 }
        if (r5 == 0) goto L_0x0095;
    L_0x003e:
        r1 = r9.anonymizeForPublicConsumption(r2);	 Catch:{ all -> 0x0092 }
    L_0x0042:
        r5 = r9.mClients;	 Catch:{ all -> 0x0092 }
        r0 = r5.iterator();	 Catch:{ all -> 0x0092 }
    L_0x0048:
        r5 = r0.hasNext();	 Catch:{ all -> 0x0092 }
        if (r5 == 0) goto L_0x009e;
    L_0x004e:
        r4 = r0.next();	 Catch:{ all -> 0x0092 }
        r4 = (com.android.server.audio.PlaybackActivityMonitor.PlayMonitorClient) r4;	 Catch:{ all -> 0x0092 }
        r5 = r4.mErrorCount;	 Catch:{ RemoteException -> 0x0063 }
        r7 = 5;
        if (r5 >= r7) goto L_0x0048;
    L_0x0059:
        r5 = r4.mIsPrivileged;	 Catch:{ RemoteException -> 0x0063 }
        if (r5 == 0) goto L_0x0097;
    L_0x005d:
        r5 = r4.mDispatcherCb;	 Catch:{ RemoteException -> 0x0063 }
        r5.dispatchPlaybackConfigChange(r2, r10);	 Catch:{ RemoteException -> 0x0063 }
        goto L_0x0048;
    L_0x0063:
        r3 = move-exception;
        r5 = r4.mErrorCount;	 Catch:{ all -> 0x0092 }
        r5 = r5 + 1;
        r4.mErrorCount = r5;	 Catch:{ all -> 0x0092 }
        r5 = "AudioService.PlaybackActivityMonitor";
        r7 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0092 }
        r7.<init>();	 Catch:{ all -> 0x0092 }
        r8 = "Error (";
        r7 = r7.append(r8);	 Catch:{ all -> 0x0092 }
        r8 = r4.mErrorCount;	 Catch:{ all -> 0x0092 }
        r7 = r7.append(r8);	 Catch:{ all -> 0x0092 }
        r8 = ") trying to dispatch playback config change to ";
        r7 = r7.append(r8);	 Catch:{ all -> 0x0092 }
        r7 = r7.append(r4);	 Catch:{ all -> 0x0092 }
        r7 = r7.toString();	 Catch:{ all -> 0x0092 }
        android.util.Log.e(r5, r7, r3);	 Catch:{ all -> 0x0092 }
        goto L_0x0048;
    L_0x0092:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x0095:
        r1 = 0;
        goto L_0x0042;
    L_0x0097:
        r5 = r4.mDispatcherCb;	 Catch:{ RemoteException -> 0x0063 }
        r7 = 0;
        r5.dispatchPlaybackConfigChange(r1, r7);	 Catch:{ RemoteException -> 0x0063 }
        goto L_0x0048;
    L_0x009e:
        monitor-exit(r6);
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.audio.PlaybackActivityMonitor.dispatchPlaybackChange(boolean):void");
    }

    private ArrayList<AudioPlaybackConfiguration> anonymizeForPublicConsumption(List<AudioPlaybackConfiguration> sysConfigs) {
        ArrayList<AudioPlaybackConfiguration> publicConfigs = new ArrayList();
        for (AudioPlaybackConfiguration config : sysConfigs) {
            if (config.isActive()) {
                publicConfigs.add(AudioPlaybackConfiguration.anonymizedCopy(config));
            }
        }
        return publicConfigs;
    }

    public boolean duckPlayers(FocusRequester winner, FocusRequester loser) {
        synchronized (this.mPlayerLock) {
            if (this.mPlayers.isEmpty()) {
                return true;
            }
            ArrayList<AudioPlaybackConfiguration> apcsToDuck = new ArrayList();
            for (AudioPlaybackConfiguration apc : this.mPlayers.values()) {
                if (!winner.hasSameUid(apc.getClientUid()) && loser.hasSameUid(apc.getClientUid()) && apc.getPlayerState() == 2) {
                    if (apc.getAudioAttributes().getContentType() == 1) {
                        Log.v(TAG, "not ducking player " + apc.getPlayerInterfaceId() + " uid:" + apc.getClientUid() + " pid:" + apc.getClientPid() + " - SPEECH");
                        return false;
                    } else if (ArrayUtils.contains(UNDUCKABLE_PLAYER_TYPES, apc.getPlayerType())) {
                        Log.v(TAG, "not ducking player " + apc.getPlayerInterfaceId() + " uid:" + apc.getClientUid() + " pid:" + apc.getClientPid() + " due to type:" + AudioPlaybackConfiguration.toLogFriendlyPlayerType(apc.getPlayerType()));
                        return false;
                    } else {
                        apcsToDuck.add(apc);
                    }
                }
            }
            this.mDuckingManager.duckUid(loser.getClientUid(), apcsToDuck);
            return true;
        }
    }

    public void unduckPlayers(FocusRequester winner) {
        synchronized (this.mPlayerLock) {
            this.mDuckingManager.unduckUid(winner.getClientUid(), this.mPlayers);
        }
    }

    public void mutePlayersForCall(int[] usagesToMute) {
        synchronized (this.mPlayerLock) {
            for (Integer piid : this.mPlayers.keySet()) {
                AudioPlaybackConfiguration apc = (AudioPlaybackConfiguration) this.mPlayers.get(piid);
                if (apc != null) {
                    int playerUsage = apc.getAudioAttributes().getUsage();
                    boolean mute = false;
                    for (int usageToMute : usagesToMute) {
                        if (playerUsage == usageToMute) {
                            mute = true;
                            break;
                        }
                    }
                    if (mute) {
                        try {
                            sEventLogger.log(new StringEvent("call: muting piid:" + piid + " uid:" + apc.getClientUid()).printLog(TAG));
                            apc.getPlayerProxy().setVolume(0.0f);
                            this.mMutedPlayers.add(new Integer(piid.intValue()));
                        } catch (Exception e) {
                            Log.e(TAG, "call: error muting player " + piid, e);
                        }
                    } else {
                        continue;
                    }
                }
            }
        }
    }

    public void unmutePlayersForCall() {
        synchronized (this.mPlayerLock) {
            if (this.mMutedPlayers.isEmpty()) {
                return;
            }
            for (Integer intValue : this.mMutedPlayers) {
                int piid = intValue.intValue();
                AudioPlaybackConfiguration apc = (AudioPlaybackConfiguration) this.mPlayers.get(Integer.valueOf(piid));
                if (apc != null) {
                    try {
                        sEventLogger.log(new StringEvent("call: unmuting piid:" + piid).printLog(TAG));
                        apc.getPlayerProxy().setVolume(1.0f);
                    } catch (Exception e) {
                        Log.e(TAG, "call: error unmuting player " + piid + " uid:" + apc.getClientUid(), e);
                    }
                }
            }
            this.mMutedPlayers.clear();
        }
    }

    void registerPlaybackCallback(IPlaybackConfigDispatcher pcdb, boolean isPrivileged) {
        if (pcdb != null) {
            synchronized (this.mClients) {
                PlayMonitorClient pmc = new PlayMonitorClient(pcdb, isPrivileged);
                if (pmc.init()) {
                    if (!isPrivileged) {
                        this.mHasPublicClients = true;
                    }
                    this.mClients.add(pmc);
                }
            }
        }
    }

    void unregisterPlaybackCallback(IPlaybackConfigDispatcher pcdb) {
        if (pcdb != null) {
            synchronized (this.mClients) {
                Iterator<PlayMonitorClient> clientIterator = this.mClients.iterator();
                boolean hasPublicClients = false;
                while (clientIterator.hasNext()) {
                    PlayMonitorClient pmc = (PlayMonitorClient) clientIterator.next();
                    if (pcdb.equals(pmc.mDispatcherCb)) {
                        pmc.release();
                        clientIterator.remove();
                    } else if (!pmc.mIsPrivileged) {
                        hasPublicClients = true;
                    }
                }
                this.mHasPublicClients = hasPublicClients;
            }
        }
    }

    List<AudioPlaybackConfiguration> getActivePlaybackConfigurations(boolean isPrivileged) {
        synchronized (this.mPlayers) {
            if (isPrivileged) {
                List arrayList = new ArrayList(this.mPlayers.values());
                return arrayList;
            }
            List<AudioPlaybackConfiguration> configsPublic;
            synchronized (this.mPlayerLock) {
                configsPublic = anonymizeForPublicConsumption(new ArrayList(this.mPlayers.values()));
            }
            return configsPublic;
        }
    }
}
