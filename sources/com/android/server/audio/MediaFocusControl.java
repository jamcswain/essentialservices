package com.android.server.audio;

import android.app.AppOpsManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusInfo;
import android.media.IAudioFocusDispatcher;
import android.media.audiopolicy.IAudioPolicyCallback;
import android.os.Binder;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.RemoteException;
import android.util.Log;
import com.android.server.SystemService;
import com.android.server.audio.AudioEventLogger.StringEvent;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Stack;

public class MediaFocusControl implements PlayerFocusEnforcer {
    static final boolean DEBUG = false;
    static final int DUCKING_IN_APP_SDK_LEVEL = 25;
    static final boolean ENFORCE_DUCKING = true;
    static final boolean ENFORCE_DUCKING_FOR_NEW = true;
    static final boolean ENFORCE_MUTING_FOR_RING_OR_CALL = true;
    private static final int MAX_STACK_SIZE = 100;
    private static final int RING_CALL_MUTING_ENFORCEMENT_DELAY_MS = 100;
    private static final String TAG = "MediaFocusControl";
    private static final int[] USAGES_TO_MUTE_IN_RING_OR_CALL = new int[]{1, 14};
    private static final Object mAudioFocusLock = new Object();
    private static final AudioEventLogger mEventLogger = new AudioEventLogger(50, "focus commands as seen by MediaFocusControl");
    private final AppOpsManager mAppOps;
    private final Context mContext;
    private PlayerFocusEnforcer mFocusEnforcer;
    private ArrayList<IAudioPolicyCallback> mFocusFollowers = new ArrayList();
    private HashMap<String, FocusRequester> mFocusOwnersForFocusPolicy = new HashMap();
    private IAudioPolicyCallback mFocusPolicy = null;
    private final Stack<FocusRequester> mFocusStack = new Stack();
    private boolean mNotifyFocusOwnerOnDuck = true;
    private boolean mRingOrCallActive = false;

    protected class AudioFocusDeathHandler implements DeathRecipient {
        private IBinder mCb;

        AudioFocusDeathHandler(IBinder cb) {
            this.mCb = cb;
        }

        public void binderDied() {
            synchronized (MediaFocusControl.mAudioFocusLock) {
                if (MediaFocusControl.this.mFocusPolicy != null) {
                    MediaFocusControl.this.removeFocusEntryForExtPolicy(this.mCb);
                } else {
                    MediaFocusControl.this.removeFocusStackEntryOnDeath(this.mCb);
                }
            }
        }
    }

    protected MediaFocusControl(Context cntxt, PlayerFocusEnforcer pfe) {
        this.mContext = cntxt;
        this.mAppOps = (AppOpsManager) this.mContext.getSystemService("appops");
        this.mFocusEnforcer = pfe;
    }

    protected void dump(PrintWriter pw) {
        pw.println("\nMediaFocusControl dump time: " + DateFormat.getTimeInstance().format(new Date()));
        dumpFocusStack(pw);
        pw.println("\n");
        mEventLogger.dump(pw);
    }

    public boolean duckPlayers(FocusRequester winner, FocusRequester loser) {
        return this.mFocusEnforcer.duckPlayers(winner, loser);
    }

    public void unduckPlayers(FocusRequester winner) {
        this.mFocusEnforcer.unduckPlayers(winner);
    }

    public void mutePlayersForCall(int[] usagesToMute) {
        this.mFocusEnforcer.mutePlayersForCall(usagesToMute);
    }

    public void unmutePlayersForCall() {
        this.mFocusEnforcer.unmutePlayersForCall();
    }

    protected void discardAudioFocusOwner() {
        synchronized (mAudioFocusLock) {
            if (!this.mFocusStack.empty()) {
                FocusRequester exFocusOwner = (FocusRequester) this.mFocusStack.pop();
                exFocusOwner.handleFocusLoss(-1, null);
                exFocusOwner.release();
            }
        }
    }

    private void notifyTopOfAudioFocusStack() {
        if (!this.mFocusStack.empty() && canReassignAudioFocus()) {
            ((FocusRequester) this.mFocusStack.peek()).handleFocusGain(1);
        }
    }

    private void propagateFocusLossFromGain_syncAf(int focusGain, FocusRequester fr) {
        Iterator<FocusRequester> stackIterator = this.mFocusStack.iterator();
        while (stackIterator.hasNext()) {
            ((FocusRequester) stackIterator.next()).handleExternalFocusGain(focusGain, fr);
        }
    }

    private void dumpFocusStack(PrintWriter pw) {
        pw.println("\nAudio Focus stack entries (last is top of stack):");
        synchronized (mAudioFocusLock) {
            Iterator<FocusRequester> stackIterator = this.mFocusStack.iterator();
            while (stackIterator.hasNext()) {
                ((FocusRequester) stackIterator.next()).dump(pw);
            }
            pw.println("\n");
            if (this.mFocusPolicy == null) {
                pw.println("No external focus policy\n");
            } else {
                pw.println("External focus policy: " + this.mFocusPolicy + ", focus owners:\n");
                dumpExtFocusPolicyFocusOwners(pw);
            }
        }
        pw.println("\n");
        pw.println(" Notify on duck:  " + this.mNotifyFocusOwnerOnDuck + "\n");
        pw.println(" In ring or call: " + this.mRingOrCallActive + "\n");
    }

    private void removeFocusStackEntry(String clientToRemove, boolean signal, boolean notifyFocusFollowers) {
        FocusRequester fr;
        if (this.mFocusStack.empty() || !((FocusRequester) this.mFocusStack.peek()).hasSameClient(clientToRemove)) {
            Iterator<FocusRequester> stackIterator = this.mFocusStack.iterator();
            while (stackIterator.hasNext()) {
                fr = (FocusRequester) stackIterator.next();
                if (fr.hasSameClient(clientToRemove)) {
                    Log.i(TAG, "AudioFocus  removeFocusStackEntry(): removing entry for " + clientToRemove);
                    stackIterator.remove();
                    fr.release();
                }
            }
            return;
        }
        fr = (FocusRequester) this.mFocusStack.pop();
        fr.release();
        if (notifyFocusFollowers) {
            AudioFocusInfo afi = fr.toAudioFocusInfo();
            afi.clearLossReceived();
            notifyExtPolicyFocusLoss_syncAf(afi, false);
        }
        if (signal) {
            notifyTopOfAudioFocusStack();
        }
    }

    private void removeFocusStackEntryOnDeath(IBinder cb) {
        boolean isTopOfStackForClientToRemove;
        if (this.mFocusStack.isEmpty()) {
            isTopOfStackForClientToRemove = false;
        } else {
            isTopOfStackForClientToRemove = ((FocusRequester) this.mFocusStack.peek()).hasSameBinder(cb);
        }
        Iterator<FocusRequester> stackIterator = this.mFocusStack.iterator();
        while (stackIterator.hasNext()) {
            FocusRequester fr = (FocusRequester) stackIterator.next();
            if (fr.hasSameBinder(cb)) {
                Log.i(TAG, "AudioFocus  removeFocusStackEntryOnDeath(): removing entry for " + cb);
                stackIterator.remove();
                fr.release();
            }
        }
        if (isTopOfStackForClientToRemove) {
            notifyTopOfAudioFocusStack();
        }
    }

    private void removeFocusEntryForExtPolicy(IBinder cb) {
        if (!this.mFocusOwnersForFocusPolicy.isEmpty()) {
            Iterator<Entry<String, FocusRequester>> ownerIterator = this.mFocusOwnersForFocusPolicy.entrySet().iterator();
            while (ownerIterator.hasNext()) {
                FocusRequester fr = (FocusRequester) ((Entry) ownerIterator.next()).getValue();
                if (fr.hasSameBinder(cb)) {
                    ownerIterator.remove();
                    fr.release();
                    notifyExtFocusPolicyFocusAbandon_syncAf(fr.toAudioFocusInfo());
                    break;
                }
            }
        }
    }

    private boolean canReassignAudioFocus() {
        if (this.mFocusStack.isEmpty() || !isLockedFocusOwner((FocusRequester) this.mFocusStack.peek())) {
            return true;
        }
        return false;
    }

    private boolean isLockedFocusOwner(FocusRequester fr) {
        return !fr.hasSameClient("AudioFocus_For_Phone_Ring_And_Calls") ? fr.isLockedFocusOwner() : true;
    }

    private int pushBelowLockedFocusOwners(FocusRequester nfr) {
        int lastLockedFocusOwnerIndex = this.mFocusStack.size();
        for (int index = this.mFocusStack.size() - 1; index >= 0; index--) {
            if (isLockedFocusOwner((FocusRequester) this.mFocusStack.elementAt(index))) {
                lastLockedFocusOwnerIndex = index;
            }
        }
        if (lastLockedFocusOwnerIndex == this.mFocusStack.size()) {
            Log.e(TAG, "No exclusive focus owner found in propagateFocusLossFromGain_syncAf()", new Exception());
            propagateFocusLossFromGain_syncAf(nfr.getGainRequest(), nfr);
            this.mFocusStack.push(nfr);
            return 1;
        }
        this.mFocusStack.insertElementAt(nfr, lastLockedFocusOwnerIndex);
        return 2;
    }

    protected void setDuckingInExtPolicyAvailable(boolean available) {
        this.mNotifyFocusOwnerOnDuck = available ^ 1;
    }

    boolean mustNotifyFocusOwnerOnDuck() {
        return this.mNotifyFocusOwnerOnDuck;
    }

    void addFocusFollower(IAudioPolicyCallback ff) {
        if (ff != null) {
            synchronized (mAudioFocusLock) {
                boolean found = false;
                for (IAudioPolicyCallback pcb : this.mFocusFollowers) {
                    if (pcb.asBinder().equals(ff.asBinder())) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    return;
                }
                this.mFocusFollowers.add(ff);
                notifyExtPolicyCurrentFocusAsync(ff);
            }
        }
    }

    void removeFocusFollower(IAudioPolicyCallback ff) {
        if (ff != null) {
            synchronized (mAudioFocusLock) {
                for (IAudioPolicyCallback pcb : this.mFocusFollowers) {
                    if (pcb.asBinder().equals(ff.asBinder())) {
                        this.mFocusFollowers.remove(pcb);
                        break;
                    }
                }
            }
        }
    }

    void setFocusPolicy(IAudioPolicyCallback policy) {
        if (policy != null) {
            synchronized (mAudioFocusLock) {
                this.mFocusPolicy = policy;
            }
        }
    }

    void unsetFocusPolicy(IAudioPolicyCallback policy) {
        if (policy != null) {
            synchronized (mAudioFocusLock) {
                if (this.mFocusPolicy == policy) {
                    this.mFocusPolicy = null;
                }
            }
        }
    }

    void notifyExtPolicyCurrentFocusAsync(final IAudioPolicyCallback pcb) {
        IAudioPolicyCallback pcb2 = pcb;
        new Thread() {
            public void run() {
                synchronized (MediaFocusControl.mAudioFocusLock) {
                    if (MediaFocusControl.this.mFocusStack.isEmpty()) {
                        return;
                    }
                    try {
                        pcb.notifyAudioFocusGrant(((FocusRequester) MediaFocusControl.this.mFocusStack.peek()).toAudioFocusInfo(), 1);
                    } catch (RemoteException e) {
                        Log.e(MediaFocusControl.TAG, "Can't call notifyAudioFocusGrant() on IAudioPolicyCallback " + pcb.asBinder(), e);
                    }
                }
            }
        }.start();
    }

    void notifyExtPolicyFocusGrant_syncAf(AudioFocusInfo afi, int requestResult) {
        for (IAudioPolicyCallback pcb : this.mFocusFollowers) {
            try {
                pcb.notifyAudioFocusGrant(afi, requestResult);
            } catch (RemoteException e) {
                Log.e(TAG, "Can't call notifyAudioFocusGrant() on IAudioPolicyCallback " + pcb.asBinder(), e);
            }
        }
    }

    void notifyExtPolicyFocusLoss_syncAf(AudioFocusInfo afi, boolean wasDispatched) {
        for (IAudioPolicyCallback pcb : this.mFocusFollowers) {
            try {
                pcb.notifyAudioFocusLoss(afi, wasDispatched);
            } catch (RemoteException e) {
                Log.e(TAG, "Can't call notifyAudioFocusLoss() on IAudioPolicyCallback " + pcb.asBinder(), e);
            }
        }
    }

    boolean notifyExtFocusPolicyFocusRequest_syncAf(AudioFocusInfo afi, int requestResult, IAudioFocusDispatcher fd, IBinder cb) {
        if (this.mFocusPolicy == null) {
            return false;
        }
        FocusRequester existingFr = (FocusRequester) this.mFocusOwnersForFocusPolicy.get(afi.getClientId());
        if (existingFr != null) {
            if (!existingFr.hasSameDispatcher(fd)) {
                existingFr.release();
                this.mFocusOwnersForFocusPolicy.put(afi.getClientId(), new FocusRequester(afi, fd, cb, new AudioFocusDeathHandler(cb), this));
            }
        } else if (requestResult == 1 || requestResult == 2) {
            this.mFocusOwnersForFocusPolicy.put(afi.getClientId(), new FocusRequester(afi, fd, cb, new AudioFocusDeathHandler(cb), this));
        }
        try {
            this.mFocusPolicy.notifyAudioFocusRequest(afi, requestResult);
        } catch (RemoteException e) {
            Log.e(TAG, "Can't call notifyAudioFocusRequest() on IAudioPolicyCallback " + this.mFocusPolicy.asBinder(), e);
        }
        return true;
    }

    boolean notifyExtFocusPolicyFocusAbandon_syncAf(AudioFocusInfo afi) {
        if (this.mFocusPolicy == null) {
            return false;
        }
        FocusRequester fr = (FocusRequester) this.mFocusOwnersForFocusPolicy.remove(afi.getClientId());
        if (fr != null) {
            fr.release();
        }
        try {
            this.mFocusPolicy.notifyAudioFocusAbandon(afi);
        } catch (RemoteException e) {
            Log.e(TAG, "Can't call notifyAudioFocusAbandon() on IAudioPolicyCallback " + this.mFocusPolicy.asBinder(), e);
        }
        return true;
    }

    int dispatchFocusChange(AudioFocusInfo afi, int focusChange) {
        synchronized (mAudioFocusLock) {
            if (this.mFocusPolicy == null) {
                return 0;
            }
            FocusRequester fr = (FocusRequester) this.mFocusOwnersForFocusPolicy.get(afi.getClientId());
            if (fr == null) {
                return 0;
            }
            int dispatchFocusChange = fr.dispatchFocusChange(focusChange);
            return dispatchFocusChange;
        }
    }

    private void dumpExtFocusPolicyFocusOwners(PrintWriter pw) {
        for (Entry<String, FocusRequester> owner : this.mFocusOwnersForFocusPolicy.entrySet()) {
            ((FocusRequester) owner.getValue()).dump(pw);
        }
    }

    protected int getCurrentAudioFocus() {
        synchronized (mAudioFocusLock) {
            if (this.mFocusStack.empty()) {
                return 0;
            }
            int gainRequest = ((FocusRequester) this.mFocusStack.peek()).getGainRequest();
            return gainRequest;
        }
    }

    protected static int getFocusRampTimeMs(int focusGain, AudioAttributes attr) {
        switch (attr.getUsage()) {
            case 1:
            case 14:
                return 1000;
            case 2:
            case 3:
            case 5:
            case 7:
            case 8:
            case 9:
            case 10:
            case 13:
                return SystemService.PHASE_SYSTEM_SERVICES_READY;
            case 4:
            case 6:
            case 11:
            case 12:
            case 16:
                return 700;
            default:
                return 0;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected int requestAudioFocus(android.media.AudioAttributes r25, int r26, android.os.IBinder r27, android.media.IAudioFocusDispatcher r28, java.lang.String r29, java.lang.String r30, int r31, int r32) {
        /*
        r24 = this;
        r5 = mEventLogger;
        r6 = new com.android.server.audio.AudioEventLogger$StringEvent;
        r7 = new java.lang.StringBuilder;
        r7.<init>();
        r8 = "requestAudioFocus() from uid/pid ";
        r7 = r7.append(r8);
        r8 = android.os.Binder.getCallingUid();
        r7 = r7.append(r8);
        r8 = "/";
        r7 = r7.append(r8);
        r8 = android.os.Binder.getCallingPid();
        r7 = r7.append(r8);
        r8 = " clientId=";
        r7 = r7.append(r8);
        r0 = r29;
        r7 = r7.append(r0);
        r8 = " callingPack=";
        r7 = r7.append(r8);
        r0 = r30;
        r7 = r7.append(r0);
        r8 = " req=";
        r7 = r7.append(r8);
        r0 = r26;
        r7 = r7.append(r0);
        r8 = " flags=0x";
        r7 = r7.append(r8);
        r8 = java.lang.Integer.toHexString(r31);
        r7 = r7.append(r8);
        r8 = " sdk=";
        r7 = r7.append(r8);
        r0 = r32;
        r7 = r7.append(r0);
        r7 = r7.toString();
        r6.<init>(r7);
        r7 = "MediaFocusControl";
        r6 = r6.printLog(r7);
        r5.log(r6);
        r5 = r27.pingBinder();
        if (r5 != 0) goto L_0x008c;
    L_0x0081:
        r5 = "MediaFocusControl";
        r6 = " AudioFocus DOA client for requestAudioFocus(), aborting.";
        android.util.Log.e(r5, r6);
        r5 = 0;
        return r5;
    L_0x008c:
        r0 = r24;
        r5 = r0.mAppOps;
        r6 = android.os.Binder.getCallingUid();
        r7 = 32;
        r0 = r30;
        r5 = r5.noteOp(r7, r6, r0);
        if (r5 == 0) goto L_0x00a0;
    L_0x009e:
        r5 = 0;
        return r5;
    L_0x00a0:
        r23 = mAudioFocusLock;
        monitor-enter(r23);
        r0 = r24;
        r5 = r0.mFocusStack;	 Catch:{ all -> 0x021c }
        r5 = r5.size();	 Catch:{ all -> 0x021c }
        r6 = 100;
        if (r5 <= r6) goto L_0x00bb;
    L_0x00af:
        r5 = "MediaFocusControl";
        r6 = "Max AudioFocus stack size reached, failing requestAudioFocus()";
        android.util.Log.e(r5, r6);	 Catch:{ all -> 0x021c }
        r5 = 0;
        monitor-exit(r23);
        return r5;
    L_0x00bb:
        r0 = r24;
        r5 = r0.mRingOrCallActive;	 Catch:{ all -> 0x021c }
        r6 = r5 ^ 1;
        r5 = "AudioFocus_For_Phone_Ring_And_Calls";
        r0 = r29;
        r5 = r5.compareTo(r0);	 Catch:{ all -> 0x021c }
        if (r5 != 0) goto L_0x010d;
    L_0x00cc:
        r5 = 1;
    L_0x00cd:
        r17 = r6 & r5;
        if (r17 == 0) goto L_0x00d6;
    L_0x00d1:
        r5 = 1;
        r0 = r24;
        r0.mRingOrCallActive = r5;	 Catch:{ all -> 0x021c }
    L_0x00d6:
        r0 = r24;
        r5 = r0.mFocusPolicy;	 Catch:{ all -> 0x021c }
        if (r5 == 0) goto L_0x010f;
    L_0x00dc:
        r3 = new android.media.AudioFocusInfo;	 Catch:{ all -> 0x021c }
        r5 = android.os.Binder.getCallingUid();	 Catch:{ all -> 0x021c }
        r9 = 0;
        r4 = r25;
        r6 = r29;
        r7 = r30;
        r8 = r26;
        r10 = r31;
        r11 = r32;
        r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11);	 Catch:{ all -> 0x021c }
    L_0x00f2:
        r18 = 0;
        r5 = r24.canReassignAudioFocus();	 Catch:{ all -> 0x021c }
        if (r5 != 0) goto L_0x0113;
    L_0x00fa:
        r5 = r31 & 1;
        if (r5 != 0) goto L_0x0111;
    L_0x00fe:
        r21 = 0;
        r5 = 0;
        r0 = r24;
        r1 = r28;
        r2 = r27;
        r0.notifyExtFocusPolicyFocusRequest_syncAf(r3, r5, r1, r2);	 Catch:{ all -> 0x021c }
        r5 = 0;
        monitor-exit(r23);
        return r5;
    L_0x010d:
        r5 = 0;
        goto L_0x00cd;
    L_0x010f:
        r3 = 0;
        goto L_0x00f2;
    L_0x0111:
        r18 = 1;
    L_0x0113:
        r22 = 2;
        r5 = 2;
        r0 = r24;
        r1 = r28;
        r2 = r27;
        r5 = r0.notifyExtFocusPolicyFocusRequest_syncAf(r3, r5, r1, r2);	 Catch:{ all -> 0x021c }
        if (r5 == 0) goto L_0x0125;
    L_0x0122:
        r5 = 2;
        monitor-exit(r23);
        return r5;
    L_0x0125:
        r11 = new com.android.server.audio.MediaFocusControl$AudioFocusDeathHandler;	 Catch:{ all -> 0x021c }
        r0 = r24;
        r1 = r27;
        r11.<init>(r1);	 Catch:{ all -> 0x021c }
        r5 = 0;
        r0 = r27;
        r0.linkToDeath(r11, r5);	 Catch:{ RemoteException -> 0x017d }
        r0 = r24;
        r5 = r0.mFocusStack;	 Catch:{ all -> 0x021c }
        r5 = r5.empty();	 Catch:{ all -> 0x021c }
        if (r5 != 0) goto L_0x01b0;
    L_0x013e:
        r0 = r24;
        r5 = r0.mFocusStack;	 Catch:{ all -> 0x021c }
        r5 = r5.peek();	 Catch:{ all -> 0x021c }
        r5 = (com.android.server.audio.FocusRequester) r5;	 Catch:{ all -> 0x021c }
        r0 = r29;
        r5 = r5.hasSameClient(r0);	 Catch:{ all -> 0x021c }
        if (r5 == 0) goto L_0x01b0;
    L_0x0150:
        r0 = r24;
        r5 = r0.mFocusStack;	 Catch:{ all -> 0x021c }
        r19 = r5.peek();	 Catch:{ all -> 0x021c }
        r19 = (com.android.server.audio.FocusRequester) r19;	 Catch:{ all -> 0x021c }
        r5 = r19.getGainRequest();	 Catch:{ all -> 0x021c }
        r0 = r26;
        if (r5 != r0) goto L_0x01a4;
    L_0x0162:
        r5 = r19.getGrantFlags();	 Catch:{ all -> 0x021c }
        r0 = r31;
        if (r5 != r0) goto L_0x01a4;
    L_0x016a:
        r5 = 0;
        r0 = r27;
        r0.unlinkToDeath(r11, r5);	 Catch:{ all -> 0x021c }
        r5 = r19.toAudioFocusInfo();	 Catch:{ all -> 0x021c }
        r6 = 1;
        r0 = r24;
        r0.notifyExtPolicyFocusGrant_syncAf(r5, r6);	 Catch:{ all -> 0x021c }
        r5 = 1;
        monitor-exit(r23);
        return r5;
    L_0x017d:
        r16 = move-exception;
        r5 = "MediaFocusControl";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x021c }
        r6.<init>();	 Catch:{ all -> 0x021c }
        r7 = "AudioFocus  requestAudioFocus() could not link to ";
        r6 = r6.append(r7);	 Catch:{ all -> 0x021c }
        r0 = r27;
        r6 = r6.append(r0);	 Catch:{ all -> 0x021c }
        r7 = " binder death";
        r6 = r6.append(r7);	 Catch:{ all -> 0x021c }
        r6 = r6.toString();	 Catch:{ all -> 0x021c }
        android.util.Log.w(r5, r6);	 Catch:{ all -> 0x021c }
        r5 = 0;
        monitor-exit(r23);
        return r5;
    L_0x01a4:
        if (r18 != 0) goto L_0x01b0;
    L_0x01a6:
        r0 = r24;
        r5 = r0.mFocusStack;	 Catch:{ all -> 0x021c }
        r5.pop();	 Catch:{ all -> 0x021c }
        r19.release();	 Catch:{ all -> 0x021c }
    L_0x01b0:
        r5 = 0;
        r6 = 0;
        r0 = r24;
        r1 = r29;
        r0.removeFocusStackEntry(r1, r5, r6);	 Catch:{ all -> 0x021c }
        r4 = new com.android.server.audio.FocusRequester;	 Catch:{ all -> 0x021c }
        r13 = android.os.Binder.getCallingUid();	 Catch:{ all -> 0x021c }
        r5 = r25;
        r6 = r26;
        r7 = r31;
        r8 = r28;
        r9 = r27;
        r10 = r29;
        r12 = r30;
        r14 = r24;
        r15 = r32;
        r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15);	 Catch:{ all -> 0x021c }
        if (r18 == 0) goto L_0x01eb;
    L_0x01d6:
        r0 = r24;
        r20 = r0.pushBelowLockedFocusOwners(r4);	 Catch:{ all -> 0x021c }
        if (r20 == 0) goto L_0x01e9;
    L_0x01de:
        r5 = r4.toAudioFocusInfo();	 Catch:{ all -> 0x021c }
        r0 = r24;
        r1 = r20;
        r0.notifyExtPolicyFocusGrant_syncAf(r5, r1);	 Catch:{ all -> 0x021c }
    L_0x01e9:
        monitor-exit(r23);
        return r20;
    L_0x01eb:
        r0 = r24;
        r5 = r0.mFocusStack;	 Catch:{ all -> 0x021c }
        r5 = r5.empty();	 Catch:{ all -> 0x021c }
        if (r5 != 0) goto L_0x01fc;
    L_0x01f5:
        r0 = r24;
        r1 = r26;
        r0.propagateFocusLossFromGain_syncAf(r1, r4);	 Catch:{ all -> 0x021c }
    L_0x01fc:
        r0 = r24;
        r5 = r0.mFocusStack;	 Catch:{ all -> 0x021c }
        r5.push(r4);	 Catch:{ all -> 0x021c }
        r5 = 1;
        r4.handleFocusGainFromRequest(r5);	 Catch:{ all -> 0x021c }
        r5 = r4.toAudioFocusInfo();	 Catch:{ all -> 0x021c }
        r6 = 1;
        r0 = r24;
        r0.notifyExtPolicyFocusGrant_syncAf(r5, r6);	 Catch:{ all -> 0x021c }
        if (r17 == 0) goto L_0x0219;
    L_0x0213:
        r5 = 1;
        r0 = r24;
        r0.runAudioCheckerForRingOrCallAsync(r5);	 Catch:{ all -> 0x021c }
    L_0x0219:
        monitor-exit(r23);
        r5 = 1;
        return r5;
    L_0x021c:
        r5 = move-exception;
        monitor-exit(r23);
        throw r5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.audio.MediaFocusControl.requestAudioFocus(android.media.AudioAttributes, int, android.os.IBinder, android.media.IAudioFocusDispatcher, java.lang.String, java.lang.String, int, int):int");
    }

    protected int abandonAudioFocus(IAudioFocusDispatcher fl, String clientId, AudioAttributes aa, String callingPackageName) {
        mEventLogger.log(new StringEvent("abandonAudioFocus() from uid/pid " + Binder.getCallingUid() + "/" + Binder.getCallingPid() + " clientId=" + clientId).printLog(TAG));
        try {
            synchronized (mAudioFocusLock) {
                if (this.mFocusPolicy != null) {
                    if (notifyExtFocusPolicyFocusAbandon_syncAf(new AudioFocusInfo(aa, Binder.getCallingUid(), clientId, callingPackageName, 0, 0, 0, 0))) {
                        return 1;
                    }
                }
                boolean exitingRingOrCall = this.mRingOrCallActive & ("AudioFocus_For_Phone_Ring_And_Calls".compareTo(clientId) == 0 ? 1 : 0);
                if (exitingRingOrCall) {
                    this.mRingOrCallActive = false;
                }
                removeFocusStackEntry(clientId, true, true);
                if (exitingRingOrCall) {
                    runAudioCheckerForRingOrCallAsync(false);
                }
            }
        } catch (ConcurrentModificationException cme) {
            Log.e(TAG, "FATAL EXCEPTION AudioFocus  abandonAudioFocus() caused " + cme);
            cme.printStackTrace();
        }
        return 1;
    }

    protected void unregisterAudioFocusClient(String clientId) {
        synchronized (mAudioFocusLock) {
            removeFocusStackEntry(clientId, false, true);
        }
    }

    private void runAudioCheckerForRingOrCallAsync(final boolean enteringRingOrCall) {
        new Thread() {
            public void run() {
                if (enteringRingOrCall) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (MediaFocusControl.mAudioFocusLock) {
                    if (MediaFocusControl.this.mRingOrCallActive) {
                        MediaFocusControl.this.mFocusEnforcer.mutePlayersForCall(MediaFocusControl.USAGES_TO_MUTE_IN_RING_OR_CALL);
                    } else {
                        MediaFocusControl.this.mFocusEnforcer.unmutePlayersForCall();
                    }
                }
            }
        }.start();
    }
}
