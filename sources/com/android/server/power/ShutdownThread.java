package com.android.server.power;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.IActivityManager;
import android.app.IActivityManager.Stub;
import android.app.ProgressDialog;
import android.bluetooth.IBluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.nfc.INfcAdapter;
import android.os.FileUtils;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RecoverySystem;
import android.os.RecoverySystem.ProgressListener;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.SystemVibrator;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.storage.IStorageManager;
import android.os.storage.IStorageShutdownObserver;
import android.util.ArrayMap;
import android.util.Log;
import android.util.TimingsTraceLog;
import com.android.internal.telephony.ITelephony;
import com.android.server.LocalServices;
import com.android.server.RescueParty;
import com.android.server.job.controllers.JobStatus;
import com.android.server.pm.PackageManagerService;
import com.android.server.statusbar.StatusBarManagerInternal;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public final class ShutdownThread extends Thread {
    private static final int ACTIVITY_MANAGER_STOP_PERCENT = 4;
    private static final int BROADCAST_STOP_PERCENT = 2;
    private static final int MAX_BROADCAST_TIME = 10000;
    private static final int MAX_RADIO_WAIT_TIME = 12000;
    private static final int MAX_SHUTDOWN_WAIT_TIME = 20000;
    private static final int MAX_UNCRYPT_WAIT_TIME = 900000;
    private static final String METRICS_FILE_BASENAME = "/data/system/shutdown-metrics";
    private static String METRIC_AM = "shutdown_activity_manager";
    private static String METRIC_BT = "shutdown_bt";
    private static String METRIC_NFC = "shutdown_nfc";
    private static String METRIC_PM = "shutdown_package_manager";
    private static String METRIC_RADIO = "shutdown_radio";
    private static String METRIC_RADIOS = "shutdown_radios";
    private static String METRIC_SEND_BROADCAST = "shutdown_send_shutdown_broadcast";
    private static String METRIC_SM = "shutdown_storage_manager";
    private static String METRIC_SYSTEM_SERVER = "shutdown_system_server";
    private static final int MOUNT_SERVICE_STOP_PERCENT = 20;
    private static final int PACKAGE_MANAGER_STOP_PERCENT = 6;
    private static final int PHONE_STATE_POLL_SLEEP_MSEC = 500;
    private static final int RADIO_STOP_PERCENT = 18;
    public static final String REBOOT_SAFEMODE_PROPERTY = "persist.sys.safemode";
    public static final String RO_SAFEMODE_PROPERTY = "ro.sys.safemode";
    public static final String SHUTDOWN_ACTION_PROPERTY = "sys.shutdown.requested";
    private static final int SHUTDOWN_VIBRATE_MS = 500;
    private static final String TAG = "ShutdownThread";
    private static final ArrayMap<String, Long> TRON_METRICS = new ArrayMap();
    private static final AudioAttributes VIBRATION_ATTRIBUTES = new Builder().setContentType(4).setUsage(13).build();
    private static String mReason;
    private static boolean mReboot;
    private static boolean mRebootHasProgressBar;
    private static boolean mRebootSafeMode;
    private static AlertDialog sConfirmDialog;
    private static final ShutdownThread sInstance = new ShutdownThread();
    private static boolean sIsStarted = false;
    private static final Object sIsStartedGuard = new Object();
    private boolean mActionDone;
    private final Object mActionDoneSync = new Object();
    private Context mContext;
    private WakeLock mCpuWakeLock;
    private Handler mHandler;
    private PowerManager mPowerManager;
    private ProgressDialog mProgressDialog;
    private WakeLock mScreenWakeLock;

    private static class CloseDialogReceiver extends BroadcastReceiver implements OnDismissListener {
        public Dialog dialog;
        private Context mContext;

        CloseDialogReceiver(Context context) {
            this.mContext = context;
            context.registerReceiver(this, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        }

        public void onReceive(Context context, Intent intent) {
            this.dialog.cancel();
        }

        public void onDismiss(DialogInterface unused) {
            this.mContext.unregisterReceiver(this);
        }
    }

    private ShutdownThread() {
    }

    public static void shutdown(Context context, String reason, boolean confirm) {
        mReboot = false;
        mRebootSafeMode = false;
        mReason = reason;
        shutdownInner(context, confirm);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void shutdownInner(final android.content.Context r7, boolean r8) {
        /*
        r6 = 0;
        r7.assertRuntimeOverlayThemable();
        r4 = sIsStartedGuard;
        monitor-enter(r4);
        r3 = sIsStarted;	 Catch:{ all -> 0x009a }
        if (r3 == 0) goto L_0x0016;
    L_0x000b:
        r3 = "ShutdownThread";
        r5 = "Request to shutdown already running, returning.";
        android.util.Log.d(r3, r5);	 Catch:{ all -> 0x009a }
        monitor-exit(r4);
        return;
    L_0x0016:
        monitor-exit(r4);
        r3 = r7.getResources();
        r4 = 17694799; // 0x10e004f float:2.6081502E-38 double:8.7423923E-317;
        r1 = r3.getInteger(r4);
        r3 = mRebootSafeMode;
        if (r3 == 0) goto L_0x009d;
    L_0x0026:
        r2 = 17040656; // 0x1040510 float:2.4248203E-38 double:8.4192027E-317;
    L_0x0029:
        r3 = "ShutdownThread";
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "Notifying thread to start shutdown longPressBehavior=";
        r4 = r4.append(r5);
        r4 = r4.append(r1);
        r4 = r4.toString();
        android.util.Log.d(r3, r4);
        if (r8 == 0) goto L_0x00ac;
    L_0x0045:
        r0 = new com.android.server.power.ShutdownThread$CloseDialogReceiver;
        r0.<init>(r7);
        r3 = sConfirmDialog;
        if (r3 == 0) goto L_0x0053;
    L_0x004e:
        r3 = sConfirmDialog;
        r3.dismiss();
    L_0x0053:
        r4 = new android.app.AlertDialog$Builder;
        r4.<init>(r7);
        r3 = mRebootSafeMode;
        if (r3 == 0) goto L_0x00a8;
    L_0x005c:
        r3 = 17040657; // 0x1040511 float:2.4248206E-38 double:8.419203E-317;
    L_0x005f:
        r3 = r4.setTitle(r3);
        r3 = r3.setMessage(r2);
        r4 = new com.android.server.power.ShutdownThread$1;
        r4.<init>(r7);
        r5 = 17039379; // 0x1040013 float:2.4244624E-38 double:8.418572E-317;
        r3 = r3.setPositiveButton(r5, r4);
        r4 = 17039369; // 0x1040009 float:2.4244596E-38 double:8.418567E-317;
        r3 = r3.setNegativeButton(r4, r6);
        r3 = r3.create();
        sConfirmDialog = r3;
        r3 = sConfirmDialog;
        r0.dialog = r3;
        r3 = sConfirmDialog;
        r3.setOnDismissListener(r0);
        r3 = sConfirmDialog;
        r3 = r3.getWindow();
        r4 = 2009; // 0x7d9 float:2.815E-42 double:9.926E-321;
        r3.setType(r4);
        r3 = sConfirmDialog;
        r3.show();
    L_0x0099:
        return;
    L_0x009a:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
    L_0x009d:
        r3 = 2;
        if (r1 != r3) goto L_0x00a4;
    L_0x00a0:
        r2 = 17040778; // 0x104058a float:2.4248545E-38 double:8.419263E-317;
        goto L_0x0029;
    L_0x00a4:
        r2 = 17040777; // 0x1040589 float:2.4248542E-38 double:8.4192625E-317;
        goto L_0x0029;
    L_0x00a8:
        r3 = 17040641; // 0x1040501 float:2.424816E-38 double:8.4191953E-317;
        goto L_0x005f;
    L_0x00ac:
        beginShutdownSequence(r7);
        goto L_0x0099;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.power.ShutdownThread.shutdownInner(android.content.Context, boolean):void");
    }

    public static void reboot(Context context, String reason, boolean confirm) {
        mReboot = true;
        mRebootSafeMode = false;
        mRebootHasProgressBar = false;
        mReason = reason;
        shutdownInner(context, confirm);
    }

    public static void rebootSafeMode(Context context, boolean confirm) {
        if (!((UserManager) context.getSystemService("user")).hasUserRestriction("no_safe_boot")) {
            mReboot = true;
            mRebootSafeMode = true;
            mRebootHasProgressBar = false;
            mReason = null;
            shutdownInner(context, confirm);
        }
    }

    private static ProgressDialog showShutdownDialog(Context context) {
        ProgressDialog pd = new ProgressDialog(context);
        if (mReason != null && mReason.startsWith("recovery-update")) {
            boolean exists;
            if (RecoverySystem.UNCRYPT_PACKAGE_FILE.exists()) {
                exists = RecoverySystem.BLOCK_MAP_FILE.exists() ^ 1;
            } else {
                exists = false;
            }
            mRebootHasProgressBar = exists;
            pd.setTitle(context.getText(17040663));
            if (mRebootHasProgressBar) {
                pd.setMax(100);
                pd.setProgress(0);
                pd.setIndeterminate(false);
                pd.setProgressNumberFormat(null);
                pd.setProgressStyle(1);
                pd.setMessage(context.getText(17040661));
            } else if (showSysuiReboot()) {
                return null;
            } else {
                pd.setIndeterminate(true);
                pd.setMessage(context.getText(17040662));
            }
        } else if (mReason == null || !mReason.equals("recovery")) {
            if (showSysuiReboot()) {
                return null;
            }
            pd.setTitle(context.getText(17040641));
            pd.setMessage(context.getText(17040779));
            pd.setIndeterminate(true);
        } else if (RescueParty.isAttemptingFactoryReset()) {
            pd.setTitle(context.getText(17040641));
            pd.setMessage(context.getText(17040779));
            pd.setIndeterminate(true);
        } else {
            pd.setTitle(context.getText(17040659));
            pd.setMessage(context.getText(17040658));
            pd.setIndeterminate(true);
        }
        pd.setCancelable(false);
        pd.getWindow().setType(2009);
        pd.show();
        return pd;
    }

    private static boolean showSysuiReboot() {
        Log.d(TAG, "Attempting to use SysUI shutdown UI");
        try {
            if (((StatusBarManagerInternal) LocalServices.getService(StatusBarManagerInternal.class)).showShutdownUi(mReboot, mReason)) {
                Log.d(TAG, "SysUI handling shutdown UI");
                return true;
            }
        } catch (Exception e) {
        }
        Log.d(TAG, "SysUI is unavailable");
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void beginShutdownSequence(android.content.Context r6) {
        /*
        r5 = 0;
        r2 = sIsStartedGuard;
        monitor-enter(r2);
        r1 = sIsStarted;	 Catch:{ all -> 0x0090 }
        if (r1 == 0) goto L_0x0013;
    L_0x0008:
        r1 = "ShutdownThread";
        r3 = "Shutdown sequence already running, returning.";
        android.util.Log.d(r1, r3);	 Catch:{ all -> 0x0090 }
        monitor-exit(r2);
        return;
    L_0x0013:
        r1 = 1;
        sIsStarted = r1;	 Catch:{ all -> 0x0090 }
        monitor-exit(r2);
        r1 = sInstance;
        r2 = showShutdownDialog(r6);
        r1.mProgressDialog = r2;
        r1 = sInstance;
        r1.mContext = r6;
        r2 = sInstance;
        r1 = "power";
        r1 = r6.getSystemService(r1);
        r1 = (android.os.PowerManager) r1;
        r2.mPowerManager = r1;
        r1 = sInstance;
        r1.mCpuWakeLock = r5;
        r1 = sInstance;	 Catch:{ SecurityException -> 0x0093 }
        r2 = sInstance;	 Catch:{ SecurityException -> 0x0093 }
        r2 = r2.mPowerManager;	 Catch:{ SecurityException -> 0x0093 }
        r3 = "ShutdownThread-cpu";
        r4 = 1;
        r2 = r2.newWakeLock(r4, r3);	 Catch:{ SecurityException -> 0x0093 }
        r1.mCpuWakeLock = r2;	 Catch:{ SecurityException -> 0x0093 }
        r1 = sInstance;	 Catch:{ SecurityException -> 0x0093 }
        r1 = r1.mCpuWakeLock;	 Catch:{ SecurityException -> 0x0093 }
        r2 = 0;
        r1.setReferenceCounted(r2);	 Catch:{ SecurityException -> 0x0093 }
        r1 = sInstance;	 Catch:{ SecurityException -> 0x0093 }
        r1 = r1.mCpuWakeLock;	 Catch:{ SecurityException -> 0x0093 }
        r1.acquire();	 Catch:{ SecurityException -> 0x0093 }
    L_0x0053:
        r1 = sInstance;
        r1.mScreenWakeLock = r5;
        r1 = sInstance;
        r1 = r1.mPowerManager;
        r1 = r1.isScreenOn();
        if (r1 == 0) goto L_0x0081;
    L_0x0061:
        r1 = sInstance;	 Catch:{ SecurityException -> 0x00a2 }
        r2 = sInstance;	 Catch:{ SecurityException -> 0x00a2 }
        r2 = r2.mPowerManager;	 Catch:{ SecurityException -> 0x00a2 }
        r3 = "ShutdownThread-screen";
        r4 = 26;
        r2 = r2.newWakeLock(r4, r3);	 Catch:{ SecurityException -> 0x00a2 }
        r1.mScreenWakeLock = r2;	 Catch:{ SecurityException -> 0x00a2 }
        r1 = sInstance;	 Catch:{ SecurityException -> 0x00a2 }
        r1 = r1.mScreenWakeLock;	 Catch:{ SecurityException -> 0x00a2 }
        r2 = 0;
        r1.setReferenceCounted(r2);	 Catch:{ SecurityException -> 0x00a2 }
        r1 = sInstance;	 Catch:{ SecurityException -> 0x00a2 }
        r1 = r1.mScreenWakeLock;	 Catch:{ SecurityException -> 0x00a2 }
        r1.acquire();	 Catch:{ SecurityException -> 0x00a2 }
    L_0x0081:
        r1 = sInstance;
        r2 = new com.android.server.power.ShutdownThread$2;
        r2.<init>();
        r1.mHandler = r2;
        r1 = sInstance;
        r1.start();
        return;
    L_0x0090:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
    L_0x0093:
        r0 = move-exception;
        r1 = "ShutdownThread";
        r2 = "No permission to acquire wake lock";
        android.util.Log.w(r1, r2, r0);
        r1 = sInstance;
        r1.mCpuWakeLock = r5;
        goto L_0x0053;
    L_0x00a2:
        r0 = move-exception;
        r1 = "ShutdownThread";
        r2 = "No permission to acquire wake lock";
        android.util.Log.w(r1, r2, r0);
        r1 = sInstance;
        r1.mScreenWakeLock = r5;
        goto L_0x0081;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.power.ShutdownThread.beginShutdownSequence(android.content.Context):void");
    }

    void actionDone() {
        synchronized (this.mActionDoneSync) {
            this.mActionDone = true;
            this.mActionDoneSync.notifyAll();
        }
    }

    public void run() {
        String str;
        TimingsTraceLog shutdownTimingLog = newTimingsLog();
        shutdownTimingLog.traceBegin("SystemServerShutdown");
        metricStarted(METRIC_SYSTEM_SERVER);
        BroadcastReceiver br = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                ShutdownThread.this.actionDone();
            }
        };
        StringBuilder append = new StringBuilder().append(mReboot ? "1" : "0");
        if (mReason != null) {
            str = mReason;
        } else {
            str = "";
        }
        SystemProperties.set(SHUTDOWN_ACTION_PROPERTY, append.append(str).toString());
        if (mRebootSafeMode) {
            SystemProperties.set(REBOOT_SAFEMODE_PROPERTY, "1");
        }
        metricStarted(METRIC_SEND_BROADCAST);
        shutdownTimingLog.traceBegin("SendShutdownBroadcast");
        Log.i(TAG, "Sending shutdown broadcast...");
        this.mActionDone = false;
        Intent intent = new Intent("android.intent.action.ACTION_SHUTDOWN");
        intent.addFlags(285212672);
        this.mContext.sendOrderedBroadcastAsUser(intent, UserHandle.ALL, null, br, this.mHandler, 0, null, null);
        long endTime = SystemClock.elapsedRealtime() + JobStatus.DEFAULT_TRIGGER_UPDATE_DELAY;
        synchronized (this.mActionDoneSync) {
            while (!this.mActionDone) {
                long delay = endTime - SystemClock.elapsedRealtime();
                if (delay <= 0) {
                    Log.w(TAG, "Shutdown broadcast timed out");
                    break;
                }
                if (mRebootHasProgressBar) {
                    sInstance.setRebootProgress((int) (((((double) (JobStatus.DEFAULT_TRIGGER_UPDATE_DELAY - delay)) * 1.0d) * 2.0d) / 10000.0d), null);
                }
                try {
                    this.mActionDoneSync.wait(Math.min(delay, 500));
                } catch (InterruptedException e) {
                }
            }
        }
        if (mRebootHasProgressBar) {
            sInstance.setRebootProgress(2, null);
        }
        shutdownTimingLog.traceEnd();
        metricEnded(METRIC_SEND_BROADCAST);
        Log.i(TAG, "Shutting down activity manager...");
        shutdownTimingLog.traceBegin("ShutdownActivityManager");
        metricStarted(METRIC_AM);
        IActivityManager am = Stub.asInterface(ServiceManager.checkService("activity"));
        if (am != null) {
            try {
                am.shutdown(10000);
            } catch (RemoteException e2) {
            }
        }
        if (mRebootHasProgressBar) {
            sInstance.setRebootProgress(4, null);
        }
        shutdownTimingLog.traceEnd();
        metricEnded(METRIC_AM);
        Log.i(TAG, "Shutting down package manager...");
        shutdownTimingLog.traceBegin("ShutdownPackageManager");
        metricStarted(METRIC_PM);
        PackageManagerService pm = (PackageManagerService) ServiceManager.getService("package");
        if (pm != null) {
            pm.shutdown();
        }
        if (mRebootHasProgressBar) {
            sInstance.setRebootProgress(6, null);
        }
        shutdownTimingLog.traceEnd();
        metricEnded(METRIC_PM);
        shutdownTimingLog.traceBegin("ShutdownRadios");
        metricStarted(METRIC_RADIOS);
        shutdownRadios(MAX_RADIO_WAIT_TIME);
        if (mRebootHasProgressBar) {
            sInstance.setRebootProgress(18, null);
        }
        shutdownTimingLog.traceEnd();
        metricEnded(METRIC_RADIOS);
        AnonymousClass4 anonymousClass4 = new IStorageShutdownObserver.Stub() {
            public void onShutDownComplete(int statusCode) throws RemoteException {
                Log.w(ShutdownThread.TAG, "Result code " + statusCode + " from StorageManagerService.shutdown");
                ShutdownThread.this.actionDone();
            }
        };
        Log.i(TAG, "Shutting down StorageManagerService");
        shutdownTimingLog.traceBegin("ShutdownStorageManager");
        metricStarted(METRIC_SM);
        this.mActionDone = false;
        long endShutTime = SystemClock.elapsedRealtime() + 20000;
        synchronized (this.mActionDoneSync) {
            try {
                IStorageManager storageManager = IStorageManager.Stub.asInterface(ServiceManager.checkService("mount"));
                if (storageManager != null) {
                    storageManager.shutdown(anonymousClass4);
                } else {
                    Log.w(TAG, "StorageManagerService unavailable for shutdown");
                }
            } catch (Exception e3) {
                Log.e(TAG, "Exception during StorageManagerService shutdown", e3);
            }
            while (!this.mActionDone) {
                delay = endShutTime - SystemClock.elapsedRealtime();
                if (delay <= 0) {
                    Log.w(TAG, "StorageManager shutdown wait timed out");
                    break;
                }
                if (mRebootHasProgressBar) {
                    sInstance.setRebootProgress(((int) (((((double) (20000 - delay)) * 1.0d) * 2.0d) / 20000.0d)) + 18, null);
                }
                try {
                    this.mActionDoneSync.wait(Math.min(delay, 500));
                } catch (InterruptedException e4) {
                }
            }
        }
        shutdownTimingLog.traceEnd();
        metricEnded(METRIC_SM);
        if (mRebootHasProgressBar) {
            sInstance.setRebootProgress(20, null);
            uncrypt();
        }
        shutdownTimingLog.traceEnd();
        metricEnded(METRIC_SYSTEM_SERVER);
        saveMetrics(mReboot);
        rebootOrShutdown(this.mContext, mReboot, mReason);
    }

    private static TimingsTraceLog newTimingsLog() {
        return new TimingsTraceLog("ShutdownTiming", 524288);
    }

    private static void metricStarted(String metricKey) {
        synchronized (TRON_METRICS) {
            TRON_METRICS.put(metricKey, Long.valueOf(SystemClock.elapsedRealtime() * -1));
        }
    }

    private static void metricEnded(String metricKey) {
        synchronized (TRON_METRICS) {
            TRON_METRICS.put(metricKey, Long.valueOf(SystemClock.elapsedRealtime() + ((Long) TRON_METRICS.get(metricKey)).longValue()));
        }
    }

    private void setRebootProgress(final int progress, final CharSequence message) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (ShutdownThread.this.mProgressDialog != null) {
                    ShutdownThread.this.mProgressDialog.setProgress(progress);
                    if (message != null) {
                        ShutdownThread.this.mProgressDialog.setMessage(message);
                    }
                }
            }
        });
    }

    private void shutdownRadios(int timeout) {
        final long endTime = SystemClock.elapsedRealtime() + ((long) timeout);
        final boolean[] done = new boolean[1];
        final int i = timeout;
        Thread t = new Thread() {
            public void run() {
                boolean nfcOff;
                boolean bluetoothReadyForShutdown;
                int needMobileRadioShutdown;
                TimingsTraceLog shutdownTimingsTraceLog = ShutdownThread.newTimingsLog();
                INfcAdapter nfc = INfcAdapter.Stub.asInterface(ServiceManager.checkService("nfc"));
                ITelephony phone = ITelephony.Stub.asInterface(ServiceManager.checkService("phone"));
                IBluetoothManager bluetooth = IBluetoothManager.Stub.asInterface(ServiceManager.checkService("bluetooth_manager"));
                if (nfc != null) {
                    try {
                        nfcOff = nfc.getState() == 1;
                    } catch (RemoteException ex) {
                        Log.e(ShutdownThread.TAG, "RemoteException during NFC shutdown", ex);
                        nfcOff = true;
                    }
                } else {
                    nfcOff = true;
                }
                if (!nfcOff) {
                    Log.w(ShutdownThread.TAG, "Turning off NFC...");
                    ShutdownThread.metricStarted(ShutdownThread.METRIC_NFC);
                    nfc.disable(false);
                }
                if (bluetooth != null) {
                    try {
                        bluetoothReadyForShutdown = bluetooth.getState() == 10;
                    } catch (RemoteException ex2) {
                        Log.e(ShutdownThread.TAG, "RemoteException during bluetooth shutdown", ex2);
                        bluetoothReadyForShutdown = true;
                    }
                } else {
                    bluetoothReadyForShutdown = true;
                }
                if (!bluetoothReadyForShutdown) {
                    Log.w(ShutdownThread.TAG, "Disabling Bluetooth...");
                    ShutdownThread.metricStarted(ShutdownThread.METRIC_BT);
                    bluetooth.disable(ShutdownThread.this.mContext.getPackageName(), false);
                }
                if (phone != null) {
                    try {
                        needMobileRadioShutdown = phone.needMobileRadioShutdown() ^ 1;
                    } catch (RemoteException ex22) {
                        Log.e(ShutdownThread.TAG, "RemoteException during radio shutdown", ex22);
                        needMobileRadioShutdown = 1;
                    }
                } else {
                    needMobileRadioShutdown = 1;
                }
                if (needMobileRadioShutdown == 0) {
                    Log.w(ShutdownThread.TAG, "Turning off cellular radios...");
                    ShutdownThread.metricStarted(ShutdownThread.METRIC_RADIO);
                    phone.shutdownMobileRadios();
                }
                Log.i(ShutdownThread.TAG, "Waiting for NFC, Bluetooth and Radio...");
                long delay = endTime - SystemClock.elapsedRealtime();
                while (delay > 0) {
                    if (ShutdownThread.mRebootHasProgressBar) {
                        ShutdownThread.sInstance.setRebootProgress(((int) (((((double) (((long) i) - delay)) * 1.0d) * 12.0d) / ((double) i))) + 6, null);
                    }
                    if (!bluetoothReadyForShutdown) {
                        try {
                            if (bluetooth.getState() == 10 || bluetooth.getState() == 16) {
                                bluetoothReadyForShutdown = true;
                                if (bluetoothReadyForShutdown) {
                                    Log.i(ShutdownThread.TAG, "Bluetooth turned off.");
                                    ShutdownThread.metricEnded(ShutdownThread.METRIC_BT);
                                    shutdownTimingsTraceLog.logDuration("ShutdownBt", ((Long) ShutdownThread.TRON_METRICS.get(ShutdownThread.METRIC_BT)).longValue());
                                }
                            } else {
                                bluetoothReadyForShutdown = bluetooth.getState() == 15;
                                if (bluetoothReadyForShutdown) {
                                    Log.i(ShutdownThread.TAG, "Bluetooth turned off.");
                                    ShutdownThread.metricEnded(ShutdownThread.METRIC_BT);
                                    shutdownTimingsTraceLog.logDuration("ShutdownBt", ((Long) ShutdownThread.TRON_METRICS.get(ShutdownThread.METRIC_BT)).longValue());
                                }
                            }
                        } catch (RemoteException ex222) {
                            Log.e(ShutdownThread.TAG, "RemoteException during bluetooth shutdown", ex222);
                            bluetoothReadyForShutdown = true;
                        }
                    }
                    if (needMobileRadioShutdown == 0) {
                        try {
                            needMobileRadioShutdown = phone.needMobileRadioShutdown() ^ 1;
                        } catch (RemoteException ex2222) {
                            Log.e(ShutdownThread.TAG, "RemoteException during radio shutdown", ex2222);
                            needMobileRadioShutdown = 1;
                        }
                        if (needMobileRadioShutdown != 0) {
                            Log.i(ShutdownThread.TAG, "Radio turned off.");
                            ShutdownThread.metricEnded(ShutdownThread.METRIC_RADIO);
                            shutdownTimingsTraceLog.logDuration("ShutdownRadio", ((Long) ShutdownThread.TRON_METRICS.get(ShutdownThread.METRIC_RADIO)).longValue());
                        }
                    }
                    if (!nfcOff) {
                        try {
                            nfcOff = nfc.getState() == 1;
                        } catch (RemoteException ex22222) {
                            Log.e(ShutdownThread.TAG, "RemoteException during NFC shutdown", ex22222);
                            nfcOff = true;
                        }
                        if (nfcOff) {
                            Log.i(ShutdownThread.TAG, "NFC turned off.");
                            ShutdownThread.metricEnded(ShutdownThread.METRIC_NFC);
                            shutdownTimingsTraceLog.logDuration("ShutdownNfc", ((Long) ShutdownThread.TRON_METRICS.get(ShutdownThread.METRIC_NFC)).longValue());
                        }
                    }
                    if (needMobileRadioShutdown != 0 && bluetoothReadyForShutdown && nfcOff) {
                        Log.i(ShutdownThread.TAG, "NFC, Radio and Bluetooth shutdown complete.");
                        done[0] = true;
                        return;
                    }
                    SystemClock.sleep(500);
                    delay = endTime - SystemClock.elapsedRealtime();
                }
            }
        };
        t.start();
        try {
            t.join((long) timeout);
        } catch (InterruptedException e) {
        }
        if (!done[0]) {
            Log.w(TAG, "Timed out waiting for NFC, Radio and Bluetooth shutdown.");
        }
    }

    public static void rebootOrShutdown(Context context, boolean reboot, String reason) {
        if (reboot) {
            Log.i(TAG, "Rebooting, reason: " + reason);
            PowerManagerService.lowLevelReboot(reason);
            Log.e(TAG, "Reboot failed, will attempt shutdown instead");
            reason = null;
        } else if (context != null) {
            try {
                new SystemVibrator(context).vibrate(500, VIBRATION_ATTRIBUTES);
            } catch (Exception e) {
                Log.w(TAG, "Failed to vibrate during shutdown.", e);
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e2) {
            }
        }
        Log.i(TAG, "Performing low-level shutdown...");
        PowerManagerService.lowLevelShutdown(reason);
    }

    private static void saveMetrics(boolean reboot) {
        IOException e;
        Throwable th;
        StringBuilder metricValue = new StringBuilder();
        metricValue.append("reboot:");
        metricValue.append(reboot ? "y" : "n");
        int metricsSize = TRON_METRICS.size();
        for (int i = 0; i < metricsSize; i++) {
            String name = (String) TRON_METRICS.keyAt(i);
            long value = ((Long) TRON_METRICS.valueAt(i)).longValue();
            if (value < 0) {
                Log.e(TAG, "metricEnded wasn't called for " + name);
            } else {
                metricValue.append(',').append(name).append(':').append(value);
            }
        }
        File tmp = new File("/data/system/shutdown-metrics.tmp");
        boolean saved = false;
        Throwable th2 = null;
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fos = new FileOutputStream(tmp);
            try {
                fos.write(metricValue.toString().getBytes(StandardCharsets.UTF_8));
                saved = true;
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (Throwable th3) {
                        th2 = th3;
                    }
                }
                if (th2 != null) {
                    try {
                        throw th2;
                    } catch (IOException e2) {
                        e = e2;
                        fileOutputStream = fos;
                    }
                } else {
                    if (saved) {
                        tmp.renameTo(new File("/data/system/shutdown-metrics.txt"));
                    }
                }
            } catch (Throwable th4) {
                th = th4;
                fileOutputStream = fos;
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (Throwable th5) {
                        if (th2 == null) {
                            th2 = th5;
                        } else if (th2 != th5) {
                            th2.addSuppressed(th5);
                        }
                    }
                }
                if (th2 == null) {
                    try {
                        throw th2;
                    } catch (IOException e3) {
                        e = e3;
                        Log.e(TAG, "Cannot save shutdown metrics", e);
                        if (saved) {
                            tmp.renameTo(new File("/data/system/shutdown-metrics.txt"));
                        }
                    }
                }
                throw th;
            }
        } catch (Throwable th6) {
            th = th6;
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            if (th2 == null) {
                throw th;
            }
            throw th2;
        }
    }

    private void uncrypt() {
        Log.i(TAG, "Calling uncrypt and monitoring the progress...");
        final ProgressListener progressListener = new ProgressListener() {
            public void onProgress(int status) {
                if (status >= 0 && status < 100) {
                    ShutdownThread.sInstance.setRebootProgress(((int) ((((double) status) * 80.0d) / 100.0d)) + 20, ShutdownThread.this.mContext.getText(17040660));
                } else if (status == 100) {
                    ShutdownThread.sInstance.setRebootProgress(status, ShutdownThread.this.mContext.getText(17040662));
                }
            }
        };
        final boolean[] done = new boolean[]{false};
        Thread t = new Thread() {
            public void run() {
                RecoverySystem rs = (RecoverySystem) ShutdownThread.this.mContext.getSystemService("recovery");
                try {
                    RecoverySystem.processPackage(ShutdownThread.this.mContext, new File(FileUtils.readTextFile(RecoverySystem.UNCRYPT_PACKAGE_FILE, 0, null)), progressListener);
                } catch (IOException e) {
                    Log.e(ShutdownThread.TAG, "Error uncrypting file", e);
                }
                done[0] = true;
            }
        };
        t.start();
        try {
            t.join(900000);
        } catch (InterruptedException e) {
        }
        if (!done[0]) {
            Log.w(TAG, "Timed out waiting for uncrypt.");
            try {
                FileUtils.stringToFile(RecoverySystem.UNCRYPT_STATUS_FILE, String.format("uncrypt_time: %d\nuncrypt_error: %d\n", new Object[]{Integer.valueOf(900), Integer.valueOf(100)}));
            } catch (IOException e2) {
                Log.e(TAG, "Failed to write timeout message to uncrypt status", e2);
            }
        }
    }
}
