package com.android.server.clipboard;

import android.app.ActivityManager;
import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.app.IActivityManager;
import android.app.KeyguardManager;
import android.content.ClipData;
import android.content.ClipData.Item;
import android.content.ClipDescription;
import android.content.ContentProvider;
import android.content.Context;
import android.content.IClipboard.Stub;
import android.content.IOnPrimaryClipChangedListener;
import android.content.Intent;
import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.UserInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IUserManager;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.util.Slog;
import android.util.SparseArray;
import com.android.server.SystemService;
import com.android.server.clipboard.HostClipboardMonitor.HostClipboardCallback;
import java.util.HashSet;
import java.util.List;

public class ClipboardService extends SystemService {
    private static final boolean IS_EMULATOR = SystemProperties.getBoolean("ro.kernel.qemu", false);
    private static final String TAG = "ClipboardService";
    private final IActivityManager mAm = ActivityManager.getService();
    private final AppOpsManager mAppOps = ((AppOpsManager) getContext().getSystemService("appops"));
    private final SparseArray<PerUserClipboard> mClipboards = new SparseArray();
    private HostClipboardMonitor mHostClipboardMonitor = null;
    private Thread mHostMonitorThread = null;
    private final IBinder mPermissionOwner;
    private final PackageManager mPm = getContext().getPackageManager();
    private final IUserManager mUm = ((IUserManager) ServiceManager.getService("user"));

    private class ClipboardImpl extends Stub {
        private ClipboardImpl() {
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            try {
                return super.onTransact(code, data, reply, flags);
            } catch (RuntimeException e) {
                if (!(e instanceof SecurityException)) {
                    Slog.wtf("clipboard", "Exception: ", e);
                }
                throw e;
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void setPrimaryClip(android.content.ClipData r14, java.lang.String r15) {
            /*
            r13 = this;
            monitor-enter(r13);
            if (r14 == 0) goto L_0x0015;
        L_0x0003:
            r10 = r14.getItemCount();	 Catch:{ all -> 0x0012 }
            if (r10 > 0) goto L_0x0015;
        L_0x0009:
            r10 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x0012 }
            r11 = "No items";
            r10.<init>(r11);	 Catch:{ all -> 0x0012 }
            throw r10;	 Catch:{ all -> 0x0012 }
        L_0x0012:
            r10 = move-exception;
        L_0x0013:
            monitor-exit(r13);
            throw r10;
        L_0x0015:
            r10 = 0;
            r10 = r14.getItemAt(r10);	 Catch:{ all -> 0x0012 }
            r10 = r10.getText();	 Catch:{ all -> 0x0012 }
            if (r10 == 0) goto L_0x003e;
        L_0x0020:
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r10 = r10.mHostClipboardMonitor;	 Catch:{ all -> 0x0012 }
            if (r10 == 0) goto L_0x003e;
        L_0x0028:
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r10 = r10.mHostClipboardMonitor;	 Catch:{ all -> 0x0012 }
            r11 = 0;
            r11 = r14.getItemAt(r11);	 Catch:{ all -> 0x0012 }
            r11 = r11.getText();	 Catch:{ all -> 0x0012 }
            r11 = r11.toString();	 Catch:{ all -> 0x0012 }
            r10.setHostClipboard(r11);	 Catch:{ all -> 0x0012 }
        L_0x003e:
            r0 = android.os.Binder.getCallingUid();	 Catch:{ all -> 0x0012 }
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r11 = 30;
            r10 = r10.clipboardAccessAllowed(r11, r15, r0);	 Catch:{ all -> 0x0012 }
            if (r10 != 0) goto L_0x004e;
        L_0x004c:
            monitor-exit(r13);
            return;
        L_0x004e:
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r10.checkDataOwnerLocked(r14, r0);	 Catch:{ all -> 0x0012 }
            r9 = android.os.UserHandle.getUserId(r0);	 Catch:{ all -> 0x0012 }
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r3 = r10.getClipboard(r9);	 Catch:{ all -> 0x0012 }
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r10.revokeUris(r3);	 Catch:{ all -> 0x0012 }
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r10.setPrimaryClipInternal(r3, r14);	 Catch:{ all -> 0x0012 }
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r7 = r10.getRelatedProfiles(r9);	 Catch:{ all -> 0x0012 }
            if (r7 == 0) goto L_0x00e5;
        L_0x006f:
            r8 = r7.size();	 Catch:{ all -> 0x0012 }
            r10 = 1;
            if (r8 <= r10) goto L_0x00e5;
        L_0x0076:
            r1 = 0;
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ RemoteException -> 0x00a8 }
            r10 = r10.mUm;	 Catch:{ RemoteException -> 0x00a8 }
            r10 = r10.getUserRestrictions(r9);	 Catch:{ RemoteException -> 0x00a8 }
            r11 = "no_cross_profile_copy_paste";
            r10 = r10.getBoolean(r11);	 Catch:{ RemoteException -> 0x00a8 }
            r1 = r10 ^ 1;
        L_0x008a:
            if (r1 != 0) goto L_0x00c4;
        L_0x008c:
            r14 = 0;
        L_0x008d:
            r5 = 0;
        L_0x008e:
            if (r5 >= r8) goto L_0x00e5;
        L_0x0090:
            r10 = r7.get(r5);	 Catch:{ all -> 0x0012 }
            r10 = (android.content.pm.UserInfo) r10;	 Catch:{ all -> 0x0012 }
            r6 = r10.id;	 Catch:{ all -> 0x0012 }
            if (r6 == r9) goto L_0x00a5;
        L_0x009a:
            r10 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r11 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0012 }
            r11 = r11.getClipboard(r6);	 Catch:{ all -> 0x0012 }
            r10.setPrimaryClipInternal(r11, r14);	 Catch:{ all -> 0x0012 }
        L_0x00a5:
            r5 = r5 + 1;
            goto L_0x008e;
        L_0x00a8:
            r4 = move-exception;
            r10 = "ClipboardService";
            r11 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0012 }
            r11.<init>();	 Catch:{ all -> 0x0012 }
            r12 = "Remote Exception calling UserManager: ";
            r11 = r11.append(r12);	 Catch:{ all -> 0x0012 }
            r11 = r11.append(r4);	 Catch:{ all -> 0x0012 }
            r11 = r11.toString();	 Catch:{ all -> 0x0012 }
            android.util.Slog.e(r10, r11);	 Catch:{ all -> 0x0012 }
            goto L_0x008a;
        L_0x00c4:
            r2 = new android.content.ClipData;	 Catch:{ all -> 0x0012 }
            r2.<init>(r14);	 Catch:{ all -> 0x0012 }
            r10 = r2.getItemCount();	 Catch:{ all -> 0x00e7 }
            r5 = r10 + -1;
        L_0x00cf:
            if (r5 < 0) goto L_0x00e0;
        L_0x00d1:
            r10 = new android.content.ClipData$Item;	 Catch:{ all -> 0x00e7 }
            r11 = r2.getItemAt(r5);	 Catch:{ all -> 0x00e7 }
            r10.<init>(r11);	 Catch:{ all -> 0x00e7 }
            r2.setItemAt(r5, r10);	 Catch:{ all -> 0x00e7 }
            r5 = r5 + -1;
            goto L_0x00cf;
        L_0x00e0:
            r2.fixUrisLight(r9);	 Catch:{ all -> 0x00e7 }
            r14 = r2;
            goto L_0x008d;
        L_0x00e5:
            monitor-exit(r13);
            return;
        L_0x00e7:
            r10 = move-exception;
            r14 = r2;
            goto L_0x0013;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.clipboard.ClipboardService.ClipboardImpl.setPrimaryClip(android.content.ClipData, java.lang.String):void");
        }

        public ClipData getPrimaryClip(String pkg) {
            synchronized (this) {
                if (!ClipboardService.this.clipboardAccessAllowed(29, pkg, Binder.getCallingUid()) || ClipboardService.this.isDeviceLocked()) {
                    return null;
                }
                ClipboardService.this.addActiveOwnerLocked(Binder.getCallingUid(), pkg);
                ClipData clipData = ClipboardService.this.getClipboard().primaryClip;
                return clipData;
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.content.ClipDescription getPrimaryClipDescription(java.lang.String r6) {
            /*
            r5 = this;
            r1 = 0;
            monitor-enter(r5);
            r2 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x002c }
            r3 = android.os.Binder.getCallingUid();	 Catch:{ all -> 0x002c }
            r4 = 29;
            r2 = r2.clipboardAccessAllowed(r4, r6, r3);	 Catch:{ all -> 0x002c }
            if (r2 == 0) goto L_0x0018;
        L_0x0010:
            r2 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x002c }
            r2 = r2.isDeviceLocked();	 Catch:{ all -> 0x002c }
            if (r2 == 0) goto L_0x001a;
        L_0x0018:
            monitor-exit(r5);
            return r1;
        L_0x001a:
            r2 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x002c }
            r0 = r2.getClipboard();	 Catch:{ all -> 0x002c }
            r2 = r0.primaryClip;	 Catch:{ all -> 0x002c }
            if (r2 == 0) goto L_0x002a;
        L_0x0024:
            r1 = r0.primaryClip;	 Catch:{ all -> 0x002c }
            r1 = r1.getDescription();	 Catch:{ all -> 0x002c }
        L_0x002a:
            monitor-exit(r5);
            return r1;
        L_0x002c:
            r1 = move-exception;
            monitor-exit(r5);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.clipboard.ClipboardService.ClipboardImpl.getPrimaryClipDescription(java.lang.String):android.content.ClipDescription");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean hasPrimaryClip(java.lang.String r5) {
            /*
            r4 = this;
            r0 = 0;
            monitor-enter(r4);
            r1 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0027 }
            r2 = android.os.Binder.getCallingUid();	 Catch:{ all -> 0x0027 }
            r3 = 29;
            r1 = r1.clipboardAccessAllowed(r3, r5, r2);	 Catch:{ all -> 0x0027 }
            if (r1 == 0) goto L_0x0018;
        L_0x0010:
            r1 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0027 }
            r1 = r1.isDeviceLocked();	 Catch:{ all -> 0x0027 }
            if (r1 == 0) goto L_0x001a;
        L_0x0018:
            monitor-exit(r4);
            return r0;
        L_0x001a:
            r1 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x0027 }
            r1 = r1.getClipboard();	 Catch:{ all -> 0x0027 }
            r1 = r1.primaryClip;	 Catch:{ all -> 0x0027 }
            if (r1 == 0) goto L_0x0025;
        L_0x0024:
            r0 = 1;
        L_0x0025:
            monitor-exit(r4);
            return r0;
        L_0x0027:
            r0 = move-exception;
            monitor-exit(r4);
            throw r0;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.clipboard.ClipboardService.ClipboardImpl.hasPrimaryClip(java.lang.String):boolean");
        }

        public void addPrimaryClipChangedListener(IOnPrimaryClipChangedListener listener, String callingPackage) {
            synchronized (this) {
                ClipboardService.this.getClipboard().primaryClipListeners.register(listener, new ListenerInfo(Binder.getCallingUid(), callingPackage));
            }
        }

        public void removePrimaryClipChangedListener(IOnPrimaryClipChangedListener listener) {
            synchronized (this) {
                ClipboardService.this.getClipboard().primaryClipListeners.unregister(listener);
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean hasClipboardText(java.lang.String r7) {
            /*
            r6 = this;
            r2 = 0;
            monitor-enter(r6);
            r3 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x003c }
            r4 = android.os.Binder.getCallingUid();	 Catch:{ all -> 0x003c }
            r5 = 29;
            r3 = r3.clipboardAccessAllowed(r5, r7, r4);	 Catch:{ all -> 0x003c }
            if (r3 == 0) goto L_0x0018;
        L_0x0010:
            r3 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x003c }
            r3 = r3.isDeviceLocked();	 Catch:{ all -> 0x003c }
            if (r3 == 0) goto L_0x001a;
        L_0x0018:
            monitor-exit(r6);
            return r2;
        L_0x001a:
            r3 = com.android.server.clipboard.ClipboardService.this;	 Catch:{ all -> 0x003c }
            r0 = r3.getClipboard();	 Catch:{ all -> 0x003c }
            r3 = r0.primaryClip;	 Catch:{ all -> 0x003c }
            if (r3 == 0) goto L_0x003a;
        L_0x0024:
            r3 = r0.primaryClip;	 Catch:{ all -> 0x003c }
            r4 = 0;
            r3 = r3.getItemAt(r4);	 Catch:{ all -> 0x003c }
            r1 = r3.getText();	 Catch:{ all -> 0x003c }
            if (r1 == 0) goto L_0x0038;
        L_0x0031:
            r3 = r1.length();	 Catch:{ all -> 0x003c }
            if (r3 <= 0) goto L_0x0038;
        L_0x0037:
            r2 = 1;
        L_0x0038:
            monitor-exit(r6);
            return r2;
        L_0x003a:
            monitor-exit(r6);
            return r2;
        L_0x003c:
            r2 = move-exception;
            monitor-exit(r6);
            throw r2;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.clipboard.ClipboardService.ClipboardImpl.hasClipboardText(java.lang.String):boolean");
        }
    }

    private class ListenerInfo {
        final String mPackageName;
        final int mUid;

        ListenerInfo(int uid, String packageName) {
            this.mUid = uid;
            this.mPackageName = packageName;
        }
    }

    private class PerUserClipboard {
        final HashSet<String> activePermissionOwners = new HashSet();
        ClipData primaryClip;
        final RemoteCallbackList<IOnPrimaryClipChangedListener> primaryClipListeners = new RemoteCallbackList();
        final int userId;

        PerUserClipboard(int userId) {
            this.userId = userId;
        }
    }

    public ClipboardService(Context context) {
        super(context);
        IBinder permOwner = null;
        try {
            permOwner = this.mAm.newUriPermissionOwner("clipboard");
        } catch (RemoteException e) {
            Slog.w("clipboard", "AM dead", e);
        }
        this.mPermissionOwner = permOwner;
        if (IS_EMULATOR) {
            this.mHostClipboardMonitor = new HostClipboardMonitor(new HostClipboardCallback() {
                public void onHostClipboardUpdated(String contents) {
                    ClipData clip = new ClipData("host clipboard", new String[]{"text/plain"}, new Item(contents));
                    synchronized (ClipboardService.this.mClipboards) {
                        ClipboardService.this.setPrimaryClipInternal(ClipboardService.this.getClipboard(0), clip);
                    }
                }
            });
            this.mHostMonitorThread = new Thread(this.mHostClipboardMonitor);
            this.mHostMonitorThread.start();
        }
    }

    public void onStart() {
        publishBinderService("clipboard", new ClipboardImpl());
    }

    public void onCleanupUser(int userId) {
        synchronized (this.mClipboards) {
            this.mClipboards.remove(userId);
        }
    }

    private PerUserClipboard getClipboard() {
        return getClipboard(UserHandle.getCallingUserId());
    }

    private PerUserClipboard getClipboard(int userId) {
        PerUserClipboard puc;
        synchronized (this.mClipboards) {
            puc = (PerUserClipboard) this.mClipboards.get(userId);
            if (puc == null) {
                puc = new PerUserClipboard(userId);
                this.mClipboards.put(userId, puc);
            }
        }
        return puc;
    }

    List<UserInfo> getRelatedProfiles(int userId) {
        long origId = Binder.clearCallingIdentity();
        try {
            List<UserInfo> related = this.mUm.getProfiles(userId, true);
            Binder.restoreCallingIdentity(origId);
            return related;
        } catch (RemoteException e) {
            Slog.e(TAG, "Remote Exception calling UserManager: " + e);
            Binder.restoreCallingIdentity(origId);
            return null;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(origId);
            throw th;
        }
    }

    void setPrimaryClipInternal(PerUserClipboard clipboard, ClipData clip) {
        clipboard.activePermissionOwners.clear();
        if (clip != null || clipboard.primaryClip != null) {
            clipboard.primaryClip = clip;
            if (clip != null) {
                ClipDescription description = clip.getDescription();
                if (description != null) {
                    description.setTimestamp(System.currentTimeMillis());
                }
            }
            long ident = Binder.clearCallingIdentity();
            int n = clipboard.primaryClipListeners.beginBroadcast();
            for (int i = 0; i < n; i++) {
                try {
                    ListenerInfo li = (ListenerInfo) clipboard.primaryClipListeners.getBroadcastCookie(i);
                    if (clipboardAccessAllowed(29, li.mPackageName, li.mUid)) {
                        ((IOnPrimaryClipChangedListener) clipboard.primaryClipListeners.getBroadcastItem(i)).dispatchPrimaryClipChanged();
                    }
                } catch (RemoteException e) {
                } catch (Throwable th) {
                    clipboard.primaryClipListeners.finishBroadcast();
                    Binder.restoreCallingIdentity(ident);
                }
            }
            clipboard.primaryClipListeners.finishBroadcast();
            Binder.restoreCallingIdentity(ident);
        }
    }

    private boolean isDeviceLocked() {
        int callingUserId = UserHandle.getCallingUserId();
        long token = Binder.clearCallingIdentity();
        try {
            KeyguardManager keyguardManager = (KeyguardManager) getContext().getSystemService(KeyguardManager.class);
            boolean isDeviceLocked = keyguardManager != null ? keyguardManager.isDeviceLocked(callingUserId) : false;
            Binder.restoreCallingIdentity(token);
            return isDeviceLocked;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(token);
        }
    }

    private final void checkUriOwnerLocked(Uri uri, int uid) {
        if ("content".equals(uri.getScheme())) {
            long ident = Binder.clearCallingIdentity();
            try {
                this.mAm.checkGrantUriPermission(uid, null, ContentProvider.getUriWithoutUserId(uri), 1, ContentProvider.getUserIdFromUri(uri, UserHandle.getUserId(uid)));
            } catch (RemoteException e) {
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    private final void checkItemOwnerLocked(Item item, int uid) {
        if (item.getUri() != null) {
            checkUriOwnerLocked(item.getUri(), uid);
        }
        Intent intent = item.getIntent();
        if (intent != null && intent.getData() != null) {
            checkUriOwnerLocked(intent.getData(), uid);
        }
    }

    private final void checkDataOwnerLocked(ClipData data, int uid) {
        int N = data.getItemCount();
        for (int i = 0; i < N; i++) {
            checkItemOwnerLocked(data.getItemAt(i), uid);
        }
    }

    private final void grantUriLocked(Uri uri, String pkg, int userId) {
        long ident = Binder.clearCallingIdentity();
        try {
            int sourceUserId = ContentProvider.getUserIdFromUri(uri, userId);
            this.mAm.grantUriPermissionFromOwner(this.mPermissionOwner, Process.myUid(), pkg, ContentProvider.getUriWithoutUserId(uri), 1, sourceUserId, userId);
        } catch (RemoteException e) {
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    private final void grantItemLocked(Item item, String pkg, int userId) {
        if (item.getUri() != null) {
            grantUriLocked(item.getUri(), pkg, userId);
        }
        Intent intent = item.getIntent();
        if (intent != null && intent.getData() != null) {
            grantUriLocked(intent.getData(), pkg, userId);
        }
    }

    private final void addActiveOwnerLocked(int uid, String pkg) {
        IPackageManager pm = AppGlobals.getPackageManager();
        int targetUserHandle = UserHandle.getCallingUserId();
        long oldIdentity = Binder.clearCallingIdentity();
        try {
            PackageInfo pi = pm.getPackageInfo(pkg, 0, targetUserHandle);
            if (pi == null) {
                throw new IllegalArgumentException("Unknown package " + pkg);
            } else if (UserHandle.isSameApp(pi.applicationInfo.uid, uid)) {
                PerUserClipboard clipboard = getClipboard();
                if (clipboard.primaryClip != null && (clipboard.activePermissionOwners.contains(pkg) ^ 1) != 0) {
                    int N = clipboard.primaryClip.getItemCount();
                    for (int i = 0; i < N; i++) {
                        grantItemLocked(clipboard.primaryClip.getItemAt(i), pkg, UserHandle.getUserId(uid));
                    }
                    clipboard.activePermissionOwners.add(pkg);
                }
            } else {
                throw new SecurityException("Calling uid " + uid + " does not own package " + pkg);
            }
        } catch (RemoteException e) {
        } finally {
            Binder.restoreCallingIdentity(oldIdentity);
        }
    }

    private final void revokeUriLocked(Uri uri) {
        int userId = ContentProvider.getUserIdFromUri(uri, UserHandle.getUserId(Binder.getCallingUid()));
        long ident = Binder.clearCallingIdentity();
        try {
            this.mAm.revokeUriPermissionFromOwner(this.mPermissionOwner, ContentProvider.getUriWithoutUserId(uri), 3, userId);
        } catch (RemoteException e) {
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    private final void revokeItemLocked(Item item) {
        if (item.getUri() != null) {
            revokeUriLocked(item.getUri());
        }
        Intent intent = item.getIntent();
        if (intent != null && intent.getData() != null) {
            revokeUriLocked(intent.getData());
        }
    }

    private final void revokeUris(PerUserClipboard clipboard) {
        if (clipboard.primaryClip != null) {
            int N = clipboard.primaryClip.getItemCount();
            for (int i = 0; i < N; i++) {
                revokeItemLocked(clipboard.primaryClip.getItemAt(i));
            }
        }
    }

    private boolean clipboardAccessAllowed(int op, String callingPackage, int callingUid) {
        if (this.mAppOps.checkOp(op, callingUid, callingPackage) != 0) {
            return false;
        }
        try {
            if (AppGlobals.getPackageManager().isInstantApp(callingPackage, UserHandle.getUserId(callingUid))) {
                return this.mAm.isAppForeground(callingUid);
            }
            return true;
        } catch (RemoteException e) {
            Slog.e("clipboard", "Failed to get Instant App status for package " + callingPackage, e);
            return false;
        }
    }
}
