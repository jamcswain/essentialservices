package com.android.server.timezone;

import android.app.timezone.RulesUpdaterContract;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Environment;
import android.util.Slog;
import java.io.File;
import java.io.PrintWriter;

public class PackageTracker {
    private static final String TAG = "timezone.PackageTracker";
    private int mCheckFailureCount;
    private int mCheckTimeAllowedMillis;
    private boolean mCheckTriggered;
    private final ClockHelper mClockHelper;
    private final ConfigHelper mConfigHelper;
    private String mDataAppPackageName;
    private int mDelayBeforeReliabilityCheckMillis;
    private long mFailedCheckRetryCount;
    private final IntentHelper mIntentHelper;
    private Long mLastTriggerTimestamp = null;
    private final PackageManagerHelper mPackageManagerHelper;
    private final PackageStatusStorage mPackageStatusStorage;
    private boolean mTrackingEnabled;
    private String mUpdateAppPackageName;

    static PackageTracker create(Context context) {
        PackageTrackerHelperImpl helperImpl = new PackageTrackerHelperImpl(context);
        File storageDir = new File(Environment.getDataSystemDirectory(), "timezone");
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        return new PackageTracker(helperImpl, helperImpl, helperImpl, new PackageStatusStorage(storageDir), new IntentHelperImpl(context));
    }

    PackageTracker(ClockHelper clockHelper, ConfigHelper configHelper, PackageManagerHelper packageManagerHelper, PackageStatusStorage packageStatusStorage, IntentHelper intentHelper) {
        this.mClockHelper = clockHelper;
        this.mConfigHelper = configHelper;
        this.mPackageManagerHelper = packageManagerHelper;
        this.mPackageStatusStorage = packageStatusStorage;
        this.mIntentHelper = intentHelper;
    }

    protected synchronized void start() {
        this.mTrackingEnabled = this.mConfigHelper.isTrackingEnabled();
        if (this.mTrackingEnabled) {
            this.mUpdateAppPackageName = this.mConfigHelper.getUpdateAppPackageName();
            this.mDataAppPackageName = this.mConfigHelper.getDataAppPackageName();
            this.mCheckTimeAllowedMillis = this.mConfigHelper.getCheckTimeAllowedMillis();
            this.mFailedCheckRetryCount = (long) this.mConfigHelper.getFailedCheckRetryCount();
            this.mDelayBeforeReliabilityCheckMillis = this.mCheckTimeAllowedMillis + 60000;
            throwIfDeviceSettingsOrAppsAreBad();
            this.mCheckTriggered = false;
            this.mCheckFailureCount = 0;
            this.mIntentHelper.initialize(this.mUpdateAppPackageName, this.mDataAppPackageName, this);
            this.mIntentHelper.scheduleReliabilityTrigger((long) this.mDelayBeforeReliabilityCheckMillis);
            Slog.i(TAG, "Time zone updater / data package tracking enabled");
            return;
        }
        Slog.i(TAG, "Time zone updater / data package tracking explicitly disabled.");
    }

    private void throwIfDeviceSettingsOrAppsAreBad() {
        throwRuntimeExceptionIfNullOrEmpty(this.mUpdateAppPackageName, "Update app package name missing.");
        throwRuntimeExceptionIfNullOrEmpty(this.mDataAppPackageName, "Data app package name missing.");
        if (this.mFailedCheckRetryCount < 1) {
            throw logAndThrowRuntimeException("mFailedRetryCount=" + this.mFailedCheckRetryCount, null);
        } else if (this.mCheckTimeAllowedMillis < 1000) {
            throw logAndThrowRuntimeException("mCheckTimeAllowedMillis=" + this.mCheckTimeAllowedMillis, null);
        } else {
            try {
                if (this.mPackageManagerHelper.isPrivilegedApp(this.mUpdateAppPackageName)) {
                    Slog.d(TAG, "Update app " + this.mUpdateAppPackageName + " is valid.");
                    try {
                        if (this.mPackageManagerHelper.isPrivilegedApp(this.mDataAppPackageName)) {
                            Slog.d(TAG, "Data app " + this.mDataAppPackageName + " is valid.");
                            return;
                        }
                        throw logAndThrowRuntimeException("Data app " + this.mDataAppPackageName + " must be a priv-app.", null);
                    } catch (NameNotFoundException e) {
                        throw logAndThrowRuntimeException("Could not determine data app package details for " + this.mDataAppPackageName, e);
                    }
                }
                throw logAndThrowRuntimeException("Update app " + this.mUpdateAppPackageName + " must be a priv-app.", null);
            } catch (NameNotFoundException e2) {
                throw logAndThrowRuntimeException("Could not determine update app package details for " + this.mUpdateAppPackageName, e2);
            }
        }
    }

    public synchronized void triggerUpdateIfNeeded(boolean packageChanged) {
        if (this.mTrackingEnabled) {
            boolean updaterAppManifestValid = validateUpdaterAppManifest();
            boolean dataAppManifestValid = validateDataAppManifest();
            if (updaterAppManifestValid && (dataAppManifestValid ^ 1) == 0) {
                if (!packageChanged) {
                    if (!this.mCheckTriggered) {
                        Slog.d(TAG, "triggerUpdateIfNeeded: First reliability trigger.");
                    } else if (isCheckInProgress()) {
                        if (!isCheckResponseOverdue()) {
                            Slog.d(TAG, "triggerUpdateIfNeeded: checkComplete call is not yet overdue. Not triggering.");
                            this.mIntentHelper.scheduleReliabilityTrigger((long) this.mDelayBeforeReliabilityCheckMillis);
                            return;
                        }
                    } else if (((long) this.mCheckFailureCount) > this.mFailedCheckRetryCount) {
                        Slog.i(TAG, "triggerUpdateIfNeeded: number of allowed consecutive check failures exceeded. Stopping reliability triggers until next reboot or package update.");
                        this.mIntentHelper.unscheduleReliabilityTrigger();
                        return;
                    } else if (this.mCheckFailureCount == 0) {
                        Slog.i(TAG, "triggerUpdateIfNeeded: No reliability check required. Last check was successful.");
                        this.mIntentHelper.unscheduleReliabilityTrigger();
                        return;
                    }
                }
                PackageVersions currentInstalledVersions = lookupInstalledPackageVersions();
                if (currentInstalledVersions == null) {
                    Slog.e(TAG, "triggerUpdateIfNeeded: currentInstalledVersions was null");
                    this.mIntentHelper.unscheduleReliabilityTrigger();
                    return;
                }
                PackageStatus packageStatus = this.mPackageStatusStorage.getPackageStatus();
                if (packageStatus == null) {
                    Slog.i(TAG, "triggerUpdateIfNeeded: No package status data found. Data check needed.");
                } else if (packageStatus.mVersions.equals(currentInstalledVersions)) {
                    Slog.i(TAG, "triggerUpdateIfNeeded: Stored package versions match currently installed versions, currentInstalledVersions=" + currentInstalledVersions + ", packageStatus.mCheckStatus=" + packageStatus.mCheckStatus);
                    if (packageStatus.mCheckStatus == 2) {
                        Slog.i(TAG, "triggerUpdateIfNeeded: Prior check succeeded. No need to trigger.");
                        this.mIntentHelper.unscheduleReliabilityTrigger();
                        return;
                    }
                } else {
                    Slog.i(TAG, "triggerUpdateIfNeeded: Stored package versions=" + packageStatus.mVersions + ", do not match current package versions=" + currentInstalledVersions + ". Triggering check.");
                }
                CheckToken checkToken = this.mPackageStatusStorage.generateCheckToken(currentInstalledVersions);
                if (checkToken == null) {
                    Slog.w(TAG, "triggerUpdateIfNeeded: Unable to generate check token. Not sending check request.");
                    this.mIntentHelper.scheduleReliabilityTrigger((long) this.mDelayBeforeReliabilityCheckMillis);
                    return;
                }
                this.mIntentHelper.sendTriggerUpdateCheck(checkToken);
                this.mCheckTriggered = true;
                setCheckInProgress();
                this.mIntentHelper.scheduleReliabilityTrigger((long) this.mDelayBeforeReliabilityCheckMillis);
                return;
            }
            Slog.e(TAG, "No update triggered due to invalid application manifest entries. updaterApp=" + updaterAppManifestValid + ", dataApp=" + dataAppManifestValid);
            this.mIntentHelper.unscheduleReliabilityTrigger();
            return;
        }
        throw new IllegalStateException("Unexpected call. Tracking is disabled.");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected synchronized void recordCheckResult(com.android.server.timezone.CheckToken r5, boolean r6) {
        /*
        r4 = this;
        monitor-enter(r4);
        r1 = "timezone.PackageTracker";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x005e }
        r2.<init>();	 Catch:{ all -> 0x005e }
        r3 = "recordOperationResult: checkToken=";
        r2 = r2.append(r3);	 Catch:{ all -> 0x005e }
        r2 = r2.append(r5);	 Catch:{ all -> 0x005e }
        r3 = " success=";
        r2 = r2.append(r3);	 Catch:{ all -> 0x005e }
        r2 = r2.append(r6);	 Catch:{ all -> 0x005e }
        r2 = r2.toString();	 Catch:{ all -> 0x005e }
        android.util.Slog.i(r1, r2);	 Catch:{ all -> 0x005e }
        r1 = r4.mTrackingEnabled;	 Catch:{ all -> 0x005e }
        if (r1 != 0) goto L_0x0061;
    L_0x002a:
        if (r5 != 0) goto L_0x003c;
    L_0x002c:
        r1 = "timezone.PackageTracker";
        r2 = "recordCheckResult: Tracking is disabled and no token has been provided. Resetting tracking state.";
        android.util.Slog.d(r1, r2);	 Catch:{ all -> 0x005e }
    L_0x0035:
        r1 = r4.mPackageStatusStorage;	 Catch:{ all -> 0x005e }
        r1.resetCheckState();	 Catch:{ all -> 0x005e }
        monitor-exit(r4);
        return;
    L_0x003c:
        r1 = "timezone.PackageTracker";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x005e }
        r2.<init>();	 Catch:{ all -> 0x005e }
        r3 = "recordCheckResult: Tracking is disabled and a token ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x005e }
        r2 = r2.append(r5);	 Catch:{ all -> 0x005e }
        r3 = " has been unexpectedly provided. Resetting tracking state.";
        r2 = r2.append(r3);	 Catch:{ all -> 0x005e }
        r2 = r2.toString();	 Catch:{ all -> 0x005e }
        android.util.Slog.w(r1, r2);	 Catch:{ all -> 0x005e }
        goto L_0x0035;
    L_0x005e:
        r1 = move-exception;
        monitor-exit(r4);
        throw r1;
    L_0x0061:
        if (r5 != 0) goto L_0x007e;
    L_0x0063:
        r1 = "timezone.PackageTracker";
        r2 = "recordCheckResult: Unexpectedly missing checkToken, resetting storage state.";
        android.util.Slog.i(r1, r2);	 Catch:{ all -> 0x005e }
        r1 = r4.mPackageStatusStorage;	 Catch:{ all -> 0x005e }
        r1.resetCheckState();	 Catch:{ all -> 0x005e }
        r1 = r4.mIntentHelper;	 Catch:{ all -> 0x005e }
        r2 = r4.mDelayBeforeReliabilityCheckMillis;	 Catch:{ all -> 0x005e }
        r2 = (long) r2;	 Catch:{ all -> 0x005e }
        r1.scheduleReliabilityTrigger(r2);	 Catch:{ all -> 0x005e }
        r1 = 0;
        r4.mCheckFailureCount = r1;	 Catch:{ all -> 0x005e }
    L_0x007c:
        monitor-exit(r4);
        return;
    L_0x007e:
        r1 = r4.mPackageStatusStorage;	 Catch:{ all -> 0x005e }
        r0 = r1.markChecked(r5, r6);	 Catch:{ all -> 0x005e }
        if (r0 == 0) goto L_0x00a3;
    L_0x0086:
        r4.setCheckComplete();	 Catch:{ all -> 0x005e }
        if (r6 == 0) goto L_0x0094;
    L_0x008b:
        r1 = r4.mIntentHelper;	 Catch:{ all -> 0x005e }
        r1.unscheduleReliabilityTrigger();	 Catch:{ all -> 0x005e }
        r1 = 0;
        r4.mCheckFailureCount = r1;	 Catch:{ all -> 0x005e }
        goto L_0x007c;
    L_0x0094:
        r1 = r4.mIntentHelper;	 Catch:{ all -> 0x005e }
        r2 = r4.mDelayBeforeReliabilityCheckMillis;	 Catch:{ all -> 0x005e }
        r2 = (long) r2;	 Catch:{ all -> 0x005e }
        r1.scheduleReliabilityTrigger(r2);	 Catch:{ all -> 0x005e }
        r1 = r4.mCheckFailureCount;	 Catch:{ all -> 0x005e }
        r1 = r1 + 1;
        r4.mCheckFailureCount = r1;	 Catch:{ all -> 0x005e }
        goto L_0x007c;
    L_0x00a3:
        r1 = "timezone.PackageTracker";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x005e }
        r2.<init>();	 Catch:{ all -> 0x005e }
        r3 = "recordCheckResult: could not update token=";
        r2 = r2.append(r3);	 Catch:{ all -> 0x005e }
        r2 = r2.append(r5);	 Catch:{ all -> 0x005e }
        r3 = " with success=";
        r2 = r2.append(r3);	 Catch:{ all -> 0x005e }
        r2 = r2.append(r6);	 Catch:{ all -> 0x005e }
        r3 = ". Optimistic lock failure";
        r2 = r2.append(r3);	 Catch:{ all -> 0x005e }
        r2 = r2.toString();	 Catch:{ all -> 0x005e }
        android.util.Slog.i(r1, r2);	 Catch:{ all -> 0x005e }
        r1 = r4.mIntentHelper;	 Catch:{ all -> 0x005e }
        r2 = r4.mDelayBeforeReliabilityCheckMillis;	 Catch:{ all -> 0x005e }
        r2 = (long) r2;	 Catch:{ all -> 0x005e }
        r1.scheduleReliabilityTrigger(r2);	 Catch:{ all -> 0x005e }
        r1 = r4.mCheckFailureCount;	 Catch:{ all -> 0x005e }
        r1 = r1 + 1;
        r4.mCheckFailureCount = r1;	 Catch:{ all -> 0x005e }
        goto L_0x007c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.timezone.PackageTracker.recordCheckResult(com.android.server.timezone.CheckToken, boolean):void");
    }

    protected int getCheckFailureCountForTests() {
        return this.mCheckFailureCount;
    }

    private void setCheckInProgress() {
        this.mLastTriggerTimestamp = Long.valueOf(this.mClockHelper.currentTimestamp());
    }

    private void setCheckComplete() {
        this.mLastTriggerTimestamp = null;
    }

    private boolean isCheckInProgress() {
        return this.mLastTriggerTimestamp != null;
    }

    private boolean isCheckResponseOverdue() {
        boolean z = false;
        if (this.mLastTriggerTimestamp == null) {
            return false;
        }
        if (this.mClockHelper.currentTimestamp() > this.mLastTriggerTimestamp.longValue() + ((long) this.mCheckTimeAllowedMillis)) {
            z = true;
        }
        return z;
    }

    private PackageVersions lookupInstalledPackageVersions() {
        try {
            return new PackageVersions(this.mPackageManagerHelper.getInstalledPackageVersion(this.mUpdateAppPackageName), this.mPackageManagerHelper.getInstalledPackageVersion(this.mDataAppPackageName));
        } catch (NameNotFoundException e) {
            Slog.w(TAG, "lookupInstalledPackageVersions: Unable to resolve installed package versions", e);
            return null;
        }
    }

    private boolean validateDataAppManifest() {
        if (this.mPackageManagerHelper.contentProviderRegistered("com.android.timezone", this.mDataAppPackageName)) {
            return true;
        }
        Slog.w(TAG, "validateDataAppManifest: Data app " + this.mDataAppPackageName + " does not expose the required provider with authority=" + "com.android.timezone");
        return false;
    }

    private boolean validateUpdaterAppManifest() {
        try {
            if (!this.mPackageManagerHelper.usesPermission(this.mUpdateAppPackageName, "android.permission.UPDATE_TIME_ZONE_RULES")) {
                Slog.w(TAG, "validateUpdaterAppManifest: Updater app " + this.mDataAppPackageName + " does not use permission=" + "android.permission.UPDATE_TIME_ZONE_RULES");
                return false;
            } else if (this.mPackageManagerHelper.receiverRegistered(RulesUpdaterContract.createUpdaterIntent(this.mUpdateAppPackageName), "android.permission.TRIGGER_TIME_ZONE_RULES_CHECK")) {
                return true;
            } else {
                return false;
            }
        } catch (NameNotFoundException e) {
            Slog.w(TAG, "validateUpdaterAppManifest: Updater app " + this.mDataAppPackageName + " does not expose the required broadcast receiver.", e);
            return false;
        }
    }

    private static void throwRuntimeExceptionIfNullOrEmpty(String value, String message) {
        if (value == null || value.trim().isEmpty()) {
            throw logAndThrowRuntimeException(message, null);
        }
    }

    private static RuntimeException logAndThrowRuntimeException(String message, Throwable cause) {
        Slog.wtf(TAG, message, cause);
        throw new RuntimeException(message, cause);
    }

    public void dump(PrintWriter fout) {
        fout.println("PackageTrackerState: " + toString());
        this.mPackageStatusStorage.dump(fout);
    }

    public String toString() {
        return "PackageTracker{mTrackingEnabled=" + this.mTrackingEnabled + ", mUpdateAppPackageName='" + this.mUpdateAppPackageName + '\'' + ", mDataAppPackageName='" + this.mDataAppPackageName + '\'' + ", mCheckTimeAllowedMillis=" + this.mCheckTimeAllowedMillis + ", mDelayBeforeReliabilityCheckMillis=" + this.mDelayBeforeReliabilityCheckMillis + ", mFailedCheckRetryCount=" + this.mFailedCheckRetryCount + ", mLastTriggerTimestamp=" + this.mLastTriggerTimestamp + ", mCheckTriggered=" + this.mCheckTriggered + ", mCheckFailureCount=" + this.mCheckFailureCount + '}';
    }
}
