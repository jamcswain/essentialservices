package com.android.server;

import android.app.ActivityManager;
import android.app.ActivityManagerInternal;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.hardware.input.InputManagerInternal;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.IInterface;
import android.os.LocaleList;
import android.os.Message;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.text.style.SuggestionSpan;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.AtomicFile;
import android.util.EventLog;
import android.util.LruCache;
import android.util.Pair;
import android.util.PrintWriterPrinter;
import android.util.Printer;
import android.util.Slog;
import android.util.Xml;
import android.view.IWindowManager;
import android.view.InputChannel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManagerInternal;
import android.view.WindowManagerInternal.OnHardKeyboardStatusChangeListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManagerInternal;
import android.view.inputmethod.InputMethodSubtype;
import android.view.inputmethod.InputMethodSubtype.InputMethodSubtypeBuilder;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.content.PackageMonitor;
import com.android.internal.inputmethod.IInputContentUriToken;
import com.android.internal.inputmethod.InputMethodSubtypeSwitchingController;
import com.android.internal.inputmethod.InputMethodSubtypeSwitchingController.ImeSubtypeListItem;
import com.android.internal.inputmethod.InputMethodUtils;
import com.android.internal.inputmethod.InputMethodUtils.InputMethodSettings;
import com.android.internal.notification.SystemNotificationChannels;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.SomeArgs;
import com.android.internal.os.TransferPipe;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethod;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodManager.Stub;
import com.android.internal.view.IInputMethodSession;
import com.android.internal.view.IInputSessionCallback;
import com.android.internal.view.InputBindResult;
import com.android.internal.view.InputMethodClient;
import com.android.server.statusbar.StatusBarManagerService;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.charset.StandardCharsets;
import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class InputMethodManagerService extends Stub implements ServiceConnection, Callback {
    private static final String ACTION_SHOW_INPUT_METHOD_PICKER = "com.android.server.InputMethodManagerService.SHOW_INPUT_METHOD_PICKER";
    static final boolean DEBUG = false;
    static final boolean DEBUG_RESTORE = false;
    private static final int IME_CONNECTION_BIND_FLAGS = 1082130437;
    private static final int IME_VISIBLE_BIND_FLAGS = 738197505;
    static final int MSG_ATTACH_TOKEN = 1040;
    static final int MSG_BIND_CLIENT = 3010;
    static final int MSG_BIND_INPUT = 1010;
    static final int MSG_CREATE_SESSION = 1050;
    static final int MSG_HARD_KEYBOARD_SWITCH_CHANGED = 4000;
    static final int MSG_HIDE_CURRENT_INPUT_METHOD = 1035;
    static final int MSG_HIDE_SOFT_INPUT = 1030;
    static final int MSG_REPORT_FULLSCREEN_MODE = 3045;
    static final int MSG_SET_ACTIVE = 3020;
    static final int MSG_SET_INTERACTIVE = 3030;
    static final int MSG_SET_USER_ACTION_NOTIFICATION_SEQUENCE_NUMBER = 3040;
    static final int MSG_SHOW_IM_CONFIG = 3;
    static final int MSG_SHOW_IM_SUBTYPE_ENABLER = 2;
    static final int MSG_SHOW_IM_SUBTYPE_PICKER = 1;
    static final int MSG_SHOW_SOFT_INPUT = 1020;
    static final int MSG_START_INPUT = 2000;
    static final int MSG_SWITCH_IME = 3050;
    static final int MSG_SYSTEM_UNLOCK_USER = 5000;
    static final int MSG_UNBIND_CLIENT = 3000;
    static final int MSG_UNBIND_INPUT = 1000;
    private static final int NOT_A_SUBTYPE_ID = -1;
    static final int SECURE_SUGGESTION_SPANS_MAX_SIZE = 20;
    static final String TAG = "InputMethodManagerService";
    private static final String TAG_TRY_SUPPRESSING_IME_SWITCHER = "TrySuppressingImeSwitcher";
    static final long TIME_TO_RECONNECT = 3000;
    private boolean mAccessibilityRequestingNoSoftKeyboard;
    private final AppOpsManager mAppOpsManager;
    int mBackDisposition = 0;
    boolean mBoundToMethod;
    final HandlerCaller mCaller;
    final HashMap<IBinder, ClientState> mClients = new HashMap();
    final Context mContext;
    EditorInfo mCurAttribute;
    ClientState mCurClient;
    private boolean mCurClientInKeyguard;
    IBinder mCurFocusedWindow;
    ClientState mCurFocusedWindowClient;
    int mCurFocusedWindowSoftInputMode;
    String mCurId;
    IInputContext mCurInputContext;
    int mCurInputContextMissingMethods;
    Intent mCurIntent;
    IInputMethod mCurMethod;
    String mCurMethodId;
    int mCurSeq;
    IBinder mCurToken;
    int mCurUserActionNotificationSequenceNumber = 0;
    private InputMethodSubtype mCurrentSubtype;
    private Builder mDialogBuilder;
    SessionState mEnabledSession;
    private InputMethodFileManager mFileManager;
    final Handler mHandler;
    private final int mHardKeyboardBehavior;
    private final HardKeyboardListener mHardKeyboardListener;
    final boolean mHasFeature;
    boolean mHaveConnection;
    private final IPackageManager mIPackageManager = AppGlobals.getPackageManager();
    final IWindowManager mIWindowManager;
    private PendingIntent mImeSwitchPendingIntent;
    private Notification.Builder mImeSwitcherNotification;
    int mImeWindowVis;
    private InputMethodInfo[] mIms;
    boolean mInFullscreenMode;
    boolean mInputShown;
    boolean mIsInteractive = true;
    private KeyguardManager mKeyguardManager;
    long mLastBindTime;
    private LocaleList mLastSystemLocales;
    final ArrayList<InputMethodInfo> mMethodList = new ArrayList();
    final HashMap<String, InputMethodInfo> mMethodMap = new HashMap();
    @GuardedBy("mMethodMap")
    private int mMethodMapUpdateCount = 0;
    private final MyPackageMonitor mMyPackageMonitor = new MyPackageMonitor();
    final InputBindResult mNoBinding = new InputBindResult(null, null, null, -1, -1);
    private NotificationManager mNotificationManager;
    private boolean mNotificationShown;
    final Resources mRes;
    private final LruCache<SuggestionSpan, InputMethodInfo> mSecureSuggestionSpans = new LruCache(20);
    final InputMethodSettings mSettings;
    final SettingsObserver mSettingsObserver;
    private final HashMap<InputMethodInfo, ArrayList<InputMethodSubtype>> mShortcutInputMethodsAndSubtypes = new HashMap();
    boolean mShowExplicitlyRequested;
    boolean mShowForced;
    private boolean mShowImeWithHardKeyboard;
    private boolean mShowOngoingImeSwitcherForPhones;
    boolean mShowRequested;
    private final String mSlotIme;
    @GuardedBy("mMethodMap")
    private final StartInputHistory mStartInputHistory = new StartInputHistory();
    @GuardedBy("mMethodMap")
    private final WeakHashMap<IBinder, StartInputInfo> mStartInputMap = new WeakHashMap();
    private StatusBarManagerService mStatusBar;
    private int[] mSubtypeIds;
    private Toast mSubtypeSwitchedByShortCutToast;
    private final InputMethodSubtypeSwitchingController mSwitchingController;
    private AlertDialog mSwitchingDialog;
    private View mSwitchingDialogTitleView;
    private IBinder mSwitchingDialogToken = new Binder();
    boolean mSystemReady;
    private final UserManager mUserManager;
    boolean mVisibleBound = false;
    final ServiceConnection mVisibleConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
        }

        public void onServiceDisconnected(ComponentName name) {
        }
    };
    final WindowManagerInternal mWindowManagerInternal;

    static final class ClientState {
        final InputBinding binding = new InputBinding(null, this.inputContext.asBinder(), this.uid, this.pid);
        final IInputMethodClient client;
        SessionState curSession;
        final IInputContext inputContext;
        final int pid;
        boolean sessionRequested;
        final int uid;

        public String toString() {
            return "ClientState{" + Integer.toHexString(System.identityHashCode(this)) + " uid " + this.uid + " pid " + this.pid + "}";
        }

        ClientState(IInputMethodClient _client, IInputContext _inputContext, int _uid, int _pid) {
            this.client = _client;
            this.inputContext = _inputContext;
            this.uid = _uid;
            this.pid = _pid;
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    private @interface HardKeyboardBehavior {
        public static final int WIRED_AFFORDANCE = 1;
        public static final int WIRELESS_AFFORDANCE = 0;
    }

    private class HardKeyboardListener implements OnHardKeyboardStatusChangeListener {
        private HardKeyboardListener() {
        }

        public void onHardKeyboardStatusChange(boolean available) {
            InputMethodManagerService.this.mHandler.sendMessage(InputMethodManagerService.this.mHandler.obtainMessage(InputMethodManagerService.MSG_HARD_KEYBOARD_SWITCH_CHANGED, Integer.valueOf(available ? 1 : 0)));
        }

        public void handleHardKeyboardStatusChange(boolean available) {
            synchronized (InputMethodManagerService.this.mMethodMap) {
                if (!(InputMethodManagerService.this.mSwitchingDialog == null || InputMethodManagerService.this.mSwitchingDialogTitleView == null || !InputMethodManagerService.this.mSwitchingDialog.isShowing())) {
                    InputMethodManagerService.this.mSwitchingDialogTitleView.findViewById(16908919).setVisibility(available ? 0 : 8);
                }
            }
        }
    }

    private static class ImeSubtypeListAdapter extends ArrayAdapter<ImeSubtypeListItem> {
        public int mCheckedItem;
        private final LayoutInflater mInflater;
        private final List<ImeSubtypeListItem> mItemsList;
        private final int mTextViewResourceId;

        public ImeSubtypeListAdapter(Context context, int textViewResourceId, List<ImeSubtypeListItem> itemsList, int checkedItem) {
            super(context, textViewResourceId, itemsList);
            this.mTextViewResourceId = textViewResourceId;
            this.mItemsList = itemsList;
            this.mCheckedItem = checkedItem;
            this.mInflater = (LayoutInflater) context.getSystemService(LayoutInflater.class);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            boolean z = false;
            if (convertView != null) {
                view = convertView;
            } else {
                view = this.mInflater.inflate(this.mTextViewResourceId, null);
            }
            if (position < 0 || position >= this.mItemsList.size()) {
                return view;
            }
            ImeSubtypeListItem item = (ImeSubtypeListItem) this.mItemsList.get(position);
            CharSequence imeName = item.mImeName;
            CharSequence subtypeName = item.mSubtypeName;
            TextView firstTextView = (TextView) view.findViewById(16908308);
            TextView secondTextView = (TextView) view.findViewById(16908309);
            if (TextUtils.isEmpty(subtypeName)) {
                firstTextView.setText(imeName);
                secondTextView.setVisibility(8);
            } else {
                firstTextView.setText(subtypeName);
                secondTextView.setText(imeName);
                secondTextView.setVisibility(0);
            }
            RadioButton radioButton = (RadioButton) view.findViewById(16909186);
            if (position == this.mCheckedItem) {
                z = true;
            }
            radioButton.setChecked(z);
            return view;
        }
    }

    class ImmsBroadcastReceiver extends BroadcastReceiver {
        ImmsBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(action)) {
                InputMethodManagerService.this.hideInputMethodMenu();
            } else if ("android.intent.action.USER_ADDED".equals(action) || "android.intent.action.USER_REMOVED".equals(action)) {
                InputMethodManagerService.this.updateCurrentProfileIds();
            } else {
                if ("android.os.action.SETTING_RESTORED".equals(action)) {
                    if ("enabled_input_methods".equals(intent.getStringExtra("setting_name"))) {
                        InputMethodManagerService.restoreEnabledInputMethods(InputMethodManagerService.this.mContext, intent.getStringExtra("previous_value"), intent.getStringExtra("new_value"));
                    }
                } else if ("android.intent.action.LOCALE_CHANGED".equals(action)) {
                    InputMethodManagerService.this.onActionLocaleChanged();
                } else if (InputMethodManagerService.ACTION_SHOW_INPUT_METHOD_PICKER.equals(action)) {
                    InputMethodManagerService.this.mHandler.obtainMessage(1, 1, 0).sendToTarget();
                } else {
                    Slog.w(InputMethodManagerService.TAG, "Unexpected intent " + intent);
                }
            }
        }
    }

    private static class InputMethodFileManager {
        private static final String ADDITIONAL_SUBTYPES_FILE_NAME = "subtypes.xml";
        private static final String ATTR_ICON = "icon";
        private static final String ATTR_ID = "id";
        private static final String ATTR_IME_SUBTYPE_EXTRA_VALUE = "imeSubtypeExtraValue";
        private static final String ATTR_IME_SUBTYPE_ID = "subtypeId";
        private static final String ATTR_IME_SUBTYPE_LANGUAGE_TAG = "languageTag";
        private static final String ATTR_IME_SUBTYPE_LOCALE = "imeSubtypeLocale";
        private static final String ATTR_IME_SUBTYPE_MODE = "imeSubtypeMode";
        private static final String ATTR_IS_ASCII_CAPABLE = "isAsciiCapable";
        private static final String ATTR_IS_AUXILIARY = "isAuxiliary";
        private static final String ATTR_LABEL = "label";
        private static final String INPUT_METHOD_PATH = "inputmethod";
        private static final String NODE_IMI = "imi";
        private static final String NODE_SUBTYPE = "subtype";
        private static final String NODE_SUBTYPES = "subtypes";
        private static final String SYSTEM_PATH = "system";
        private final AtomicFile mAdditionalInputMethodSubtypeFile;
        private final HashMap<String, List<InputMethodSubtype>> mAdditionalSubtypesMap = new HashMap();
        private final HashMap<String, InputMethodInfo> mMethodMap;

        public InputMethodFileManager(HashMap<String, InputMethodInfo> methodMap, int userId) {
            if (methodMap == null) {
                throw new NullPointerException("methodMap is null");
            }
            File systemDir;
            this.mMethodMap = methodMap;
            if (userId == 0) {
                systemDir = new File(Environment.getDataDirectory(), SYSTEM_PATH);
            } else {
                systemDir = Environment.getUserSystemDirectory(userId);
            }
            File inputMethodDir = new File(systemDir, INPUT_METHOD_PATH);
            if (!(inputMethodDir.exists() || (inputMethodDir.mkdirs() ^ 1) == 0)) {
                Slog.w(InputMethodManagerService.TAG, "Couldn't create dir.: " + inputMethodDir.getAbsolutePath());
            }
            File subtypeFile = new File(inputMethodDir, ADDITIONAL_SUBTYPES_FILE_NAME);
            this.mAdditionalInputMethodSubtypeFile = new AtomicFile(subtypeFile);
            if (subtypeFile.exists()) {
                readAdditionalInputMethodSubtypes(this.mAdditionalSubtypesMap, this.mAdditionalInputMethodSubtypeFile);
            } else {
                writeAdditionalInputMethodSubtypes(this.mAdditionalSubtypesMap, this.mAdditionalInputMethodSubtypeFile, methodMap);
            }
        }

        private void deleteAllInputMethodSubtypes(String imiId) {
            synchronized (this.mMethodMap) {
                this.mAdditionalSubtypesMap.remove(imiId);
                writeAdditionalInputMethodSubtypes(this.mAdditionalSubtypesMap, this.mAdditionalInputMethodSubtypeFile, this.mMethodMap);
            }
        }

        public void addInputMethodSubtypes(InputMethodInfo imi, InputMethodSubtype[] additionalSubtypes) {
            synchronized (this.mMethodMap) {
                ArrayList<InputMethodSubtype> subtypes = new ArrayList();
                for (InputMethodSubtype subtype : additionalSubtypes) {
                    if (subtypes.contains(subtype)) {
                        Slog.w(InputMethodManagerService.TAG, "Duplicated subtype definition found: " + subtype.getLocale() + ", " + subtype.getMode());
                    } else {
                        subtypes.add(subtype);
                    }
                }
                this.mAdditionalSubtypesMap.put(imi.getId(), subtypes);
                writeAdditionalInputMethodSubtypes(this.mAdditionalSubtypesMap, this.mAdditionalInputMethodSubtypeFile, this.mMethodMap);
            }
        }

        public HashMap<String, List<InputMethodSubtype>> getAllAdditionalInputMethodSubtypes() {
            HashMap<String, List<InputMethodSubtype>> hashMap;
            synchronized (this.mMethodMap) {
                hashMap = this.mAdditionalSubtypesMap;
            }
            return hashMap;
        }

        private static void writeAdditionalInputMethodSubtypes(HashMap<String, List<InputMethodSubtype>> allSubtypes, AtomicFile subtypesFile, HashMap<String, InputMethodInfo> methodMap) {
            boolean isSetMethodMap = methodMap != null && methodMap.size() > 0;
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = subtypesFile.startWrite();
                XmlSerializer out = new FastXmlSerializer();
                out.setOutput(fileOutputStream, StandardCharsets.UTF_8.name());
                out.startDocument(null, Boolean.valueOf(true));
                out.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
                out.startTag(null, NODE_SUBTYPES);
                for (String imiId : allSubtypes.keySet()) {
                    if (!isSetMethodMap || (methodMap.containsKey(imiId) ^ 1) == 0) {
                        out.startTag(null, NODE_IMI);
                        out.attribute(null, ATTR_ID, imiId);
                        List<InputMethodSubtype> subtypesList = (List) allSubtypes.get(imiId);
                        int N = subtypesList.size();
                        for (int i = 0; i < N; i++) {
                            InputMethodSubtype subtype = (InputMethodSubtype) subtypesList.get(i);
                            out.startTag(null, NODE_SUBTYPE);
                            if (subtype.hasSubtypeId()) {
                                out.attribute(null, ATTR_IME_SUBTYPE_ID, String.valueOf(subtype.getSubtypeId()));
                            }
                            out.attribute(null, ATTR_ICON, String.valueOf(subtype.getIconResId()));
                            out.attribute(null, ATTR_LABEL, String.valueOf(subtype.getNameResId()));
                            out.attribute(null, ATTR_IME_SUBTYPE_LOCALE, subtype.getLocale());
                            out.attribute(null, ATTR_IME_SUBTYPE_LANGUAGE_TAG, subtype.getLanguageTag());
                            out.attribute(null, ATTR_IME_SUBTYPE_MODE, subtype.getMode());
                            out.attribute(null, ATTR_IME_SUBTYPE_EXTRA_VALUE, subtype.getExtraValue());
                            out.attribute(null, ATTR_IS_AUXILIARY, String.valueOf(subtype.isAuxiliary() ? 1 : 0));
                            out.attribute(null, ATTR_IS_ASCII_CAPABLE, String.valueOf(subtype.isAsciiCapable() ? 1 : 0));
                            out.endTag(null, NODE_SUBTYPE);
                        }
                        out.endTag(null, NODE_IMI);
                    } else {
                        Slog.w(InputMethodManagerService.TAG, "IME uninstalled or not valid.: " + imiId);
                    }
                }
                out.endTag(null, NODE_SUBTYPES);
                out.endDocument();
                subtypesFile.finishWrite(fileOutputStream);
            } catch (IOException e) {
                Slog.w(InputMethodManagerService.TAG, "Error writing subtypes", e);
                if (fileOutputStream != null) {
                    subtypesFile.failWrite(fileOutputStream);
                }
            }
        }

        private static void readAdditionalInputMethodSubtypes(HashMap<String, List<InputMethodSubtype>> allSubtypes, AtomicFile subtypesFile) {
            Throwable th;
            FileInputStream fileInputStream;
            Throwable th2;
            if (allSubtypes != null && subtypesFile != null) {
                allSubtypes.clear();
                th = null;
                fileInputStream = null;
                try {
                    fileInputStream = subtypesFile.openRead();
                    XmlPullParser parser = Xml.newPullParser();
                    parser.setInput(fileInputStream, StandardCharsets.UTF_8.name());
                    int eventType = parser.getEventType();
                    do {
                        eventType = parser.next();
                        if (eventType == 2) {
                            break;
                        }
                    } while (eventType != 1);
                    if (NODE_SUBTYPES.equals(parser.getName())) {
                        int depth = parser.getDepth();
                        Object currentImiId = null;
                        ArrayList<InputMethodSubtype> arrayList = null;
                        while (true) {
                            eventType = parser.next();
                            if ((eventType != 3 || parser.getDepth() > depth) && eventType != 1) {
                                if (eventType == 2) {
                                    String nodeName = parser.getName();
                                    if (NODE_IMI.equals(nodeName)) {
                                        currentImiId = parser.getAttributeValue(null, ATTR_ID);
                                        if (TextUtils.isEmpty(currentImiId)) {
                                            Slog.w(InputMethodManagerService.TAG, "Invalid imi id found in subtypes.xml");
                                        } else {
                                            arrayList = new ArrayList();
                                            allSubtypes.put(currentImiId, arrayList);
                                        }
                                    } else if (NODE_SUBTYPE.equals(nodeName)) {
                                        if (TextUtils.isEmpty(currentImiId) || arrayList == null) {
                                            Slog.w(InputMethodManagerService.TAG, "IME uninstalled or not valid.: " + currentImiId);
                                        } else {
                                            int icon = Integer.parseInt(parser.getAttributeValue(null, ATTR_ICON));
                                            int label = Integer.parseInt(parser.getAttributeValue(null, ATTR_LABEL));
                                            String imeSubtypeLocale = parser.getAttributeValue(null, ATTR_IME_SUBTYPE_LOCALE);
                                            String languageTag = parser.getAttributeValue(null, ATTR_IME_SUBTYPE_LANGUAGE_TAG);
                                            String imeSubtypeMode = parser.getAttributeValue(null, ATTR_IME_SUBTYPE_MODE);
                                            String imeSubtypeExtraValue = parser.getAttributeValue(null, ATTR_IME_SUBTYPE_EXTRA_VALUE);
                                            boolean isAuxiliary = "1".equals(String.valueOf(parser.getAttributeValue(null, ATTR_IS_AUXILIARY)));
                                            InputMethodSubtypeBuilder builder = new InputMethodSubtypeBuilder().setSubtypeNameResId(label).setSubtypeIconResId(icon).setSubtypeLocale(imeSubtypeLocale).setLanguageTag(languageTag).setSubtypeMode(imeSubtypeMode).setSubtypeExtraValue(imeSubtypeExtraValue).setIsAuxiliary(isAuxiliary).setIsAsciiCapable("1".equals(String.valueOf(parser.getAttributeValue(null, ATTR_IS_ASCII_CAPABLE))));
                                            String subtypeIdString = parser.getAttributeValue(null, ATTR_IME_SUBTYPE_ID);
                                            if (subtypeIdString != null) {
                                                builder.setSubtypeId(Integer.parseInt(subtypeIdString));
                                            }
                                            arrayList.add(builder.build());
                                        }
                                    }
                                }
                            }
                        }
                        if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (Throwable th3) {
                                th = th3;
                            }
                        }
                        if (th != null) {
                            throw th;
                        }
                        return;
                    }
                    throw new XmlPullParserException("Xml doesn't start with subtypes");
                } catch (Throwable th4) {
                    Throwable th5 = th4;
                    th4 = th2;
                    th2 = th5;
                }
            } else {
                return;
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (Throwable th6) {
                    if (th4 == null) {
                        th4 = th6;
                    } else if (th4 != th6) {
                        th4.addSuppressed(th6);
                    }
                }
            }
            if (th4 != null) {
                try {
                    throw th4;
                } catch (Exception e) {
                    Slog.w(InputMethodManagerService.TAG, "Error reading subtypes", e);
                    return;
                }
            }
            throw th2;
        }
    }

    public static final class Lifecycle extends SystemService {
        private InputMethodManagerService mService;

        public Lifecycle(Context context) {
            super(context);
            this.mService = new InputMethodManagerService(context);
        }

        public void onStart() {
            LocalServices.addService(InputMethodManagerInternal.class, new LocalServiceImpl(this.mService.mHandler));
            publishBinderService("input_method", this.mService);
        }

        public void onSwitchUser(int userHandle) {
            this.mService.onSwitchUser(userHandle);
        }

        public void onBootPhase(int phase) {
            if (phase == SystemService.PHASE_ACTIVITY_MANAGER_READY) {
                this.mService.systemRunning((StatusBarManagerService) ServiceManager.getService("statusbar"));
            }
        }

        public void onUnlockUser(int userHandle) {
            this.mService.mHandler.sendMessage(this.mService.mHandler.obtainMessage(InputMethodManagerService.MSG_SYSTEM_UNLOCK_USER, userHandle, 0));
        }
    }

    private static final class LocalServiceImpl implements InputMethodManagerInternal {
        private final Handler mHandler;

        LocalServiceImpl(Handler handler) {
            this.mHandler = handler;
        }

        public void setInteractive(boolean interactive) {
            int i;
            Handler handler = this.mHandler;
            Handler handler2 = this.mHandler;
            if (interactive) {
                i = 1;
            } else {
                i = 0;
            }
            handler.sendMessage(handler2.obtainMessage(InputMethodManagerService.MSG_SET_INTERACTIVE, i, 0));
        }

        public void switchInputMethod(boolean forwardDirection) {
            int i;
            Handler handler = this.mHandler;
            Handler handler2 = this.mHandler;
            if (forwardDirection) {
                i = 1;
            } else {
                i = 0;
            }
            handler.sendMessage(handler2.obtainMessage(3050, i, 0));
        }

        public void hideCurrentInputMethod() {
            this.mHandler.removeMessages(InputMethodManagerService.MSG_HIDE_CURRENT_INPUT_METHOD);
            this.mHandler.sendEmptyMessage(InputMethodManagerService.MSG_HIDE_CURRENT_INPUT_METHOD);
        }
    }

    private static final class MethodCallback extends IInputSessionCallback.Stub {
        private final InputChannel mChannel;
        private final IInputMethod mMethod;
        private final InputMethodManagerService mParentIMMS;

        MethodCallback(InputMethodManagerService imms, IInputMethod method, InputChannel channel) {
            this.mParentIMMS = imms;
            this.mMethod = method;
            this.mChannel = channel;
        }

        public void sessionCreated(IInputMethodSession session) {
            long ident = Binder.clearCallingIdentity();
            try {
                this.mParentIMMS.onSessionCreated(this.mMethod, session, this.mChannel);
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    final class MyPackageMonitor extends PackageMonitor {
        private final ArrayList<String> mChangedPackages = new ArrayList();
        private boolean mImePackageAppeared = false;
        @GuardedBy("mMethodMap")
        private final ArraySet<String> mKnownImePackageNames = new ArraySet();

        MyPackageMonitor() {
        }

        @GuardedBy("mMethodMap")
        void clearKnownImePackageNamesLocked() {
            this.mKnownImePackageNames.clear();
        }

        @GuardedBy("mMethodMap")
        final void addKnownImePackageNameLocked(String packageName) {
            this.mKnownImePackageNames.add(packageName);
        }

        @GuardedBy("mMethodMap")
        private boolean isChangingPackagesOfCurrentUserLocked() {
            return getChangingUserId() == InputMethodManagerService.this.mSettings.getCurrentUserId();
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onHandleForceStop(android.content.Intent r12, java.lang.String[] r13, int r14, boolean r15) {
            /*
            r11 = this;
            r10 = 1;
            r6 = 0;
            r5 = com.android.server.InputMethodManagerService.this;
            r7 = r5.mMethodMap;
            monitor-enter(r7);
            r5 = r11.isChangingPackagesOfCurrentUserLocked();	 Catch:{ all -> 0x0063 }
            if (r5 != 0) goto L_0x000f;
        L_0x000d:
            monitor-exit(r7);
            return r6;
        L_0x000f:
            r5 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0063 }
            r5 = r5.mSettings;	 Catch:{ all -> 0x0063 }
            r1 = r5.getSelectedInputMethod();	 Catch:{ all -> 0x0063 }
            r5 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0063 }
            r5 = r5.mMethodList;	 Catch:{ all -> 0x0063 }
            r0 = r5.size();	 Catch:{ all -> 0x0063 }
            if (r1 == 0) goto L_0x0061;
        L_0x0021:
            r2 = 0;
        L_0x0022:
            if (r2 >= r0) goto L_0x0061;
        L_0x0024:
            r5 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0063 }
            r5 = r5.mMethodList;	 Catch:{ all -> 0x0063 }
            r3 = r5.get(r2);	 Catch:{ all -> 0x0063 }
            r3 = (android.view.inputmethod.InputMethodInfo) r3;	 Catch:{ all -> 0x0063 }
            r5 = r3.getId();	 Catch:{ all -> 0x0063 }
            r5 = r5.equals(r1);	 Catch:{ all -> 0x0063 }
            if (r5 == 0) goto L_0x005e;
        L_0x0038:
            r8 = r13.length;	 Catch:{ all -> 0x0063 }
            r5 = r6;
        L_0x003a:
            if (r5 >= r8) goto L_0x005e;
        L_0x003c:
            r4 = r13[r5];	 Catch:{ all -> 0x0063 }
            r9 = r3.getPackageName();	 Catch:{ all -> 0x0063 }
            r9 = r9.equals(r4);	 Catch:{ all -> 0x0063 }
            if (r9 == 0) goto L_0x005b;
        L_0x0048:
            if (r15 != 0) goto L_0x004c;
        L_0x004a:
            monitor-exit(r7);
            return r10;
        L_0x004c:
            r5 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0063 }
            r6 = "";
            r5.resetSelectedInputMethodAndSubtypeLocked(r6);	 Catch:{ all -> 0x0063 }
            r5 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0063 }
            r5.chooseNewDefaultIMELocked();	 Catch:{ all -> 0x0063 }
            monitor-exit(r7);
            return r10;
        L_0x005b:
            r5 = r5 + 1;
            goto L_0x003a;
        L_0x005e:
            r2 = r2 + 1;
            goto L_0x0022;
        L_0x0061:
            monitor-exit(r7);
            return r6;
        L_0x0063:
            r5 = move-exception;
            monitor-exit(r7);
            throw r5;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.MyPackageMonitor.onHandleForceStop(android.content.Intent, java.lang.String[], int, boolean):boolean");
        }

        public void onBeginPackageChanges() {
            clearPackageChangeState();
        }

        public void onPackageAppeared(String packageName, int reason) {
            if (!(this.mImePackageAppeared || InputMethodManagerService.this.mContext.getPackageManager().queryIntentServicesAsUser(new Intent("android.view.InputMethod").setPackage(packageName), 512, getChangingUserId()).isEmpty())) {
                this.mImePackageAppeared = true;
            }
            this.mChangedPackages.add(packageName);
        }

        public void onPackageDisappeared(String packageName, int reason) {
            this.mChangedPackages.add(packageName);
        }

        public void onPackageModified(String packageName) {
            this.mChangedPackages.add(packageName);
        }

        public void onPackagesSuspended(String[] packages) {
            for (String packageName : packages) {
                this.mChangedPackages.add(packageName);
            }
        }

        public void onPackagesUnsuspended(String[] packages) {
            for (String packageName : packages) {
                this.mChangedPackages.add(packageName);
            }
        }

        public void onFinishPackageChanges() {
            onFinishPackageChangesInternal();
            clearPackageChangeState();
        }

        private void clearPackageChangeState() {
            this.mChangedPackages.clear();
            this.mImePackageAppeared = false;
        }

        private boolean shouldRebuildInputMethodListLocked() {
            if (this.mImePackageAppeared) {
                return true;
            }
            int N = this.mChangedPackages.size();
            for (int i = 0; i < N; i++) {
                if (this.mKnownImePackageNames.contains((String) this.mChangedPackages.get(i))) {
                    return true;
                }
            }
            return false;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void onFinishPackageChangesInternal() {
            /*
            r15 = this;
            r10 = com.android.server.InputMethodManagerService.this;
            r11 = r10.mMethodMap;
            monitor-enter(r11);
            r10 = r15.isChangingPackagesOfCurrentUserLocked();	 Catch:{ all -> 0x0120 }
            if (r10 != 0) goto L_0x000d;
        L_0x000b:
            monitor-exit(r11);
            return;
        L_0x000d:
            r10 = r15.shouldRebuildInputMethodListLocked();	 Catch:{ all -> 0x0120 }
            if (r10 != 0) goto L_0x0015;
        L_0x0013:
            monitor-exit(r11);
            return;
        L_0x0015:
            r3 = 0;
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r10 = r10.mSettings;	 Catch:{ all -> 0x0120 }
            r4 = r10.getSelectedInputMethod();	 Catch:{ all -> 0x0120 }
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r10 = r10.mMethodList;	 Catch:{ all -> 0x0120 }
            r0 = r10.size();	 Catch:{ all -> 0x0120 }
            if (r4 == 0) goto L_0x008c;
        L_0x0028:
            r6 = 0;
        L_0x0029:
            if (r6 >= r0) goto L_0x008c;
        L_0x002b:
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r10 = r10.mMethodList;	 Catch:{ all -> 0x0120 }
            r7 = r10.get(r6);	 Catch:{ all -> 0x0120 }
            r7 = (android.view.inputmethod.InputMethodInfo) r7;	 Catch:{ all -> 0x0120 }
            r8 = r7.getId();	 Catch:{ all -> 0x0120 }
            r10 = r8.equals(r4);	 Catch:{ all -> 0x0120 }
            if (r10 == 0) goto L_0x0040;
        L_0x003f:
            r3 = r7;
        L_0x0040:
            r10 = r7.getPackageName();	 Catch:{ all -> 0x0120 }
            r1 = r15.isPackageDisappearing(r10);	 Catch:{ all -> 0x0120 }
            r10 = r7.getPackageName();	 Catch:{ all -> 0x0120 }
            r10 = r15.isPackageModified(r10);	 Catch:{ all -> 0x0120 }
            if (r10 == 0) goto L_0x005b;
        L_0x0052:
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r10 = r10.mFileManager;	 Catch:{ all -> 0x0120 }
            r10.deleteAllInputMethodSubtypes(r8);	 Catch:{ all -> 0x0120 }
        L_0x005b:
            r10 = 2;
            if (r1 == r10) goto L_0x0061;
        L_0x005e:
            r10 = 3;
            if (r1 != r10) goto L_0x0089;
        L_0x0061:
            r10 = "InputMethodManagerService";
            r12 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0120 }
            r12.<init>();	 Catch:{ all -> 0x0120 }
            r13 = "Input method uninstalled, disabling: ";
            r12 = r12.append(r13);	 Catch:{ all -> 0x0120 }
            r13 = r7.getComponent();	 Catch:{ all -> 0x0120 }
            r12 = r12.append(r13);	 Catch:{ all -> 0x0120 }
            r12 = r12.toString();	 Catch:{ all -> 0x0120 }
            android.util.Slog.i(r10, r12);	 Catch:{ all -> 0x0120 }
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r12 = r7.getId();	 Catch:{ all -> 0x0120 }
            r13 = 0;
            r10.setInputMethodEnabledLocked(r12, r13);	 Catch:{ all -> 0x0120 }
        L_0x0089:
            r6 = r6 + 1;
            goto L_0x0029;
        L_0x008c:
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r12 = 0;
            r10.buildInputMethodListLocked(r12);	 Catch:{ all -> 0x0120 }
            r2 = 0;
            if (r3 == 0) goto L_0x0100;
        L_0x0095:
            r10 = r3.getPackageName();	 Catch:{ all -> 0x0120 }
            r1 = r15.isPackageDisappearing(r10);	 Catch:{ all -> 0x0120 }
            r10 = 2;
            if (r1 == r10) goto L_0x00a3;
        L_0x00a0:
            r10 = 3;
            if (r1 != r10) goto L_0x0100;
        L_0x00a3:
            r9 = 0;
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ RemoteException -> 0x0123 }
            r10 = r10.mIPackageManager;	 Catch:{ RemoteException -> 0x0123 }
            r12 = r3.getComponent();	 Catch:{ RemoteException -> 0x0123 }
            r13 = com.android.server.InputMethodManagerService.this;	 Catch:{ RemoteException -> 0x0123 }
            r13 = r13.mSettings;	 Catch:{ RemoteException -> 0x0123 }
            r13 = r13.getCurrentUserId();	 Catch:{ RemoteException -> 0x0123 }
            r14 = 0;
            r9 = r10.getServiceInfo(r12, r14, r13);	 Catch:{ RemoteException -> 0x0123 }
        L_0x00bb:
            if (r9 != 0) goto L_0x0100;
        L_0x00bd:
            r10 = "InputMethodManagerService";
            r12 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0120 }
            r12.<init>();	 Catch:{ all -> 0x0120 }
            r13 = "Current input method removed: ";
            r12 = r12.append(r13);	 Catch:{ all -> 0x0120 }
            r12 = r12.append(r4);	 Catch:{ all -> 0x0120 }
            r12 = r12.toString();	 Catch:{ all -> 0x0120 }
            android.util.Slog.i(r10, r12);	 Catch:{ all -> 0x0120 }
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r12 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r12 = r12.mCurToken;	 Catch:{ all -> 0x0120 }
            r13 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r13 = r13.mBackDisposition;	 Catch:{ all -> 0x0120 }
            r14 = 0;
            r10.updateSystemUiLocked(r12, r14, r13);	 Catch:{ all -> 0x0120 }
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r10 = r10.chooseNewDefaultIMELocked();	 Catch:{ all -> 0x0120 }
            if (r10 != 0) goto L_0x0100;
        L_0x00ed:
            r2 = 1;
            r3 = 0;
            r10 = "InputMethodManagerService";
            r12 = "Unsetting current input method";
            android.util.Slog.i(r10, r12);	 Catch:{ all -> 0x0120 }
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r12 = "";
            r10.resetSelectedInputMethodAndSubtypeLocked(r12);	 Catch:{ all -> 0x0120 }
        L_0x0100:
            if (r3 != 0) goto L_0x0112;
        L_0x0102:
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r2 = r10.chooseNewDefaultIMELocked();	 Catch:{ all -> 0x0120 }
        L_0x0108:
            if (r2 == 0) goto L_0x0110;
        L_0x010a:
            r10 = com.android.server.InputMethodManagerService.this;	 Catch:{ all -> 0x0120 }
            r12 = 0;
            r10.updateFromSettingsLocked(r12);	 Catch:{ all -> 0x0120 }
        L_0x0110:
            monitor-exit(r11);
            return;
        L_0x0112:
            if (r2 != 0) goto L_0x0108;
        L_0x0114:
            r10 = r3.getPackageName();	 Catch:{ all -> 0x0120 }
            r10 = r15.isPackageModified(r10);	 Catch:{ all -> 0x0120 }
            if (r10 == 0) goto L_0x0108;
        L_0x011e:
            r2 = 1;
            goto L_0x0108;
        L_0x0120:
            r10 = move-exception;
            monitor-exit(r11);
            throw r10;
        L_0x0123:
            r5 = move-exception;
            goto L_0x00bb;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.MyPackageMonitor.onFinishPackageChangesInternal():void");
        }
    }

    static class SessionState {
        InputChannel channel;
        final ClientState client;
        final IInputMethod method;
        IInputMethodSession session;

        public String toString() {
            return "SessionState{uid " + this.client.uid + " pid " + this.client.pid + " method " + Integer.toHexString(System.identityHashCode(this.method)) + " session " + Integer.toHexString(System.identityHashCode(this.session)) + " channel " + this.channel + "}";
        }

        SessionState(ClientState _client, IInputMethod _method, IInputMethodSession _session, InputChannel _channel) {
            this.client = _client;
            this.method = _method;
            this.session = _session;
            this.channel = _channel;
        }
    }

    class SettingsObserver extends ContentObserver {
        String mLastEnabled = "";
        boolean mRegistered = false;
        int mUserId;

        SettingsObserver(Handler handler) {
            super(handler);
        }

        public void registerContentObserverLocked(int userId) {
            if (!this.mRegistered || this.mUserId != userId) {
                ContentResolver resolver = InputMethodManagerService.this.mContext.getContentResolver();
                if (this.mRegistered) {
                    InputMethodManagerService.this.mContext.getContentResolver().unregisterContentObserver(this);
                    this.mRegistered = false;
                }
                if (this.mUserId != userId) {
                    this.mLastEnabled = "";
                    this.mUserId = userId;
                }
                resolver.registerContentObserver(Secure.getUriFor("default_input_method"), false, this, userId);
                resolver.registerContentObserver(Secure.getUriFor("enabled_input_methods"), false, this, userId);
                resolver.registerContentObserver(Secure.getUriFor("selected_input_method_subtype"), false, this, userId);
                resolver.registerContentObserver(Secure.getUriFor("show_ime_with_hard_keyboard"), false, this, userId);
                resolver.registerContentObserver(Secure.getUriFor("accessibility_soft_keyboard_mode"), false, this, userId);
                this.mRegistered = true;
            }
        }

        public void onChange(boolean selfChange, Uri uri) {
            boolean z = true;
            Uri showImeUri = Secure.getUriFor("show_ime_with_hard_keyboard");
            Uri accessibilityRequestingNoImeUri = Secure.getUriFor("accessibility_soft_keyboard_mode");
            synchronized (InputMethodManagerService.this.mMethodMap) {
                if (showImeUri.equals(uri)) {
                    InputMethodManagerService.this.updateKeyboardFromSettingsLocked();
                } else if (accessibilityRequestingNoImeUri.equals(uri)) {
                    InputMethodManagerService inputMethodManagerService = InputMethodManagerService.this;
                    if (Secure.getIntForUser(InputMethodManagerService.this.mContext.getContentResolver(), "accessibility_soft_keyboard_mode", 0, this.mUserId) != 1) {
                        z = false;
                    }
                    inputMethodManagerService.mAccessibilityRequestingNoSoftKeyboard = z;
                    if (InputMethodManagerService.this.mAccessibilityRequestingNoSoftKeyboard) {
                        boolean showRequested = InputMethodManagerService.this.mShowRequested;
                        InputMethodManagerService.this.hideCurrentInputLocked(0, null);
                        InputMethodManagerService.this.mShowRequested = showRequested;
                    } else if (InputMethodManagerService.this.mShowRequested) {
                        InputMethodManagerService.this.showCurrentInputLocked(1, null);
                    }
                } else {
                    boolean enabledChanged = false;
                    String newEnabled = InputMethodManagerService.this.mSettings.getEnabledInputMethodsStr();
                    if (!this.mLastEnabled.equals(newEnabled)) {
                        this.mLastEnabled = newEnabled;
                        enabledChanged = true;
                    }
                    InputMethodManagerService.this.updateInputMethodsFromSettingsLocked(enabledChanged);
                }
            }
        }

        public String toString() {
            return "SettingsObserver{mUserId=" + this.mUserId + " mRegistered=" + this.mRegistered + " mLastEnabled=" + this.mLastEnabled + "}";
        }
    }

    private static final class StartInputHistory {
        private static final int ENTRY_SIZE_FOR_HIGH_RAM_DEVICE = 16;
        private static final int ENTRY_SIZE_FOR_LOW_RAM_DEVICE = 5;
        private final Entry[] mEntries;
        private int mNextIndex;

        private static final class Entry {
            int mClientBindSequenceNumber;
            EditorInfo mEditorInfo;
            String mImeId;
            String mImeTokenString;
            boolean mRestarting;
            int mSequenceNumber;
            int mStartInputReason;
            int mTargetWindowSoftInputMode;
            String mTargetWindowString;
            long mTimestamp;
            long mWallTime;

            Entry(StartInputInfo original) {
                set(original);
            }

            void set(StartInputInfo original) {
                this.mSequenceNumber = original.mSequenceNumber;
                this.mTimestamp = original.mTimestamp;
                this.mWallTime = original.mWallTime;
                this.mImeTokenString = String.valueOf(original.mImeToken);
                this.mImeId = original.mImeId;
                this.mStartInputReason = original.mStartInputReason;
                this.mRestarting = original.mRestarting;
                this.mTargetWindowString = String.valueOf(original.mTargetWindow);
                this.mEditorInfo = original.mEditorInfo;
                this.mTargetWindowSoftInputMode = original.mTargetWindowSoftInputMode;
                this.mClientBindSequenceNumber = original.mClientBindSequenceNumber;
            }
        }

        private StartInputHistory() {
            this.mEntries = new Entry[getEntrySize()];
            this.mNextIndex = 0;
        }

        private static int getEntrySize() {
            if (ActivityManager.isLowRamDeviceStatic()) {
                return 5;
            }
            return 16;
        }

        void addEntry(StartInputInfo info) {
            int index = this.mNextIndex;
            if (this.mEntries[index] == null) {
                this.mEntries[index] = new Entry(info);
            } else {
                this.mEntries[index].set(info);
            }
            this.mNextIndex = (this.mNextIndex + 1) % this.mEntries.length;
        }

        void dump(PrintWriter pw, String prefix) {
            SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
            for (int i = 0; i < this.mEntries.length; i++) {
                Entry entry = this.mEntries[(this.mNextIndex + i) % this.mEntries.length];
                if (entry != null) {
                    pw.print(prefix);
                    pw.println("StartInput #" + entry.mSequenceNumber + ":");
                    pw.print(prefix);
                    pw.println(" time=" + dataFormat.format(new Date(entry.mWallTime)) + " (timestamp=" + entry.mTimestamp + ")" + " reason=" + InputMethodClient.getStartInputReason(entry.mStartInputReason) + " restarting=" + entry.mRestarting);
                    pw.print(prefix);
                    pw.println(" imeToken=" + entry.mImeTokenString + " [" + entry.mImeId + "]");
                    pw.print(prefix);
                    pw.println(" targetWin=" + entry.mTargetWindowString + " [" + entry.mEditorInfo.packageName + "]" + " clientBindSeq=" + entry.mClientBindSequenceNumber);
                    pw.print(prefix);
                    pw.println(" softInputMode=" + InputMethodClient.softInputModeToString(entry.mTargetWindowSoftInputMode));
                    pw.print(prefix);
                    pw.println(" inputType=0x" + Integer.toHexString(entry.mEditorInfo.inputType) + " imeOptions=0x" + Integer.toHexString(entry.mEditorInfo.imeOptions) + " fieldId=0x" + Integer.toHexString(entry.mEditorInfo.fieldId) + " fieldName=" + entry.mEditorInfo.fieldName + " actionId=" + entry.mEditorInfo.actionId + " actionLabel=" + entry.mEditorInfo.actionLabel);
                }
            }
        }
    }

    private static class StartInputInfo {
        private static final AtomicInteger sSequenceNumber = new AtomicInteger(0);
        final int mClientBindSequenceNumber;
        final EditorInfo mEditorInfo;
        final String mImeId;
        final IBinder mImeToken;
        final boolean mRestarting;
        final int mSequenceNumber = sSequenceNumber.getAndIncrement();
        final int mStartInputReason;
        final IBinder mTargetWindow;
        final int mTargetWindowSoftInputMode;
        final long mTimestamp = SystemClock.uptimeMillis();
        final long mWallTime = System.currentTimeMillis();

        StartInputInfo(IBinder imeToken, String imeId, int startInputReason, boolean restarting, IBinder targetWindow, EditorInfo editorInfo, int targetWindowSoftInputMode, int clientBindSequenceNumber) {
            this.mImeToken = imeToken;
            this.mImeId = imeId;
            this.mStartInputReason = startInputReason;
            this.mRestarting = restarting;
            this.mTargetWindow = targetWindow;
            this.mEditorInfo = editorInfo;
            this.mTargetWindowSoftInputMode = targetWindowSoftInputMode;
            this.mClientBindSequenceNumber = clientBindSequenceNumber;
        }
    }

    void onActionLocaleChanged() {
        synchronized (this.mMethodMap) {
            LocaleList possibleNewLocale = this.mRes.getConfiguration().getLocales();
            if (possibleNewLocale == null || !possibleNewLocale.equals(this.mLastSystemLocales)) {
                buildInputMethodListLocked(true);
                resetDefaultImeLocked(this.mContext);
                updateFromSettingsLocked(true);
                this.mLastSystemLocales = possibleNewLocale;
                return;
            }
        }
    }

    static void restoreEnabledInputMethods(Context context, String prevValue, String newValue) {
        ArrayMap<String, ArraySet<String>> prevMap = InputMethodUtils.parseInputMethodsAndSubtypesString(prevValue);
        for (Entry<String, ArraySet<String>> entry : InputMethodUtils.parseInputMethodsAndSubtypesString(newValue).entrySet()) {
            String imeId = (String) entry.getKey();
            ArraySet<String> prevSubtypes = (ArraySet) prevMap.get(imeId);
            if (prevSubtypes == null) {
                prevSubtypes = new ArraySet(2);
                prevMap.put(imeId, prevSubtypes);
            }
            prevSubtypes.addAll((ArraySet) entry.getValue());
        }
        Secure.putString(context.getContentResolver(), "enabled_input_methods", InputMethodUtils.buildInputMethodsAndSubtypesString(prevMap));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void onUnlockUser(int r5) {
        /*
        r4 = this;
        r2 = r4.mMethodMap;
        monitor-enter(r2);
        r1 = r4.mSettings;	 Catch:{ all -> 0x0024 }
        r0 = r1.getCurrentUserId();	 Catch:{ all -> 0x0024 }
        if (r5 == r0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r2);
        return;
    L_0x000d:
        r1 = r4.mSettings;	 Catch:{ all -> 0x0024 }
        r3 = r4.mSystemReady;	 Catch:{ all -> 0x0024 }
        r3 = r3 ^ 1;
        r1.switchCurrentUser(r0, r3);	 Catch:{ all -> 0x0024 }
        r1 = r4.mSystemReady;	 Catch:{ all -> 0x0024 }
        if (r1 == 0) goto L_0x0022;
    L_0x001a:
        r1 = 0;
        r4.buildInputMethodListLocked(r1);	 Catch:{ all -> 0x0024 }
        r1 = 1;
        r4.updateInputMethodsFromSettingsLocked(r1);	 Catch:{ all -> 0x0024 }
    L_0x0022:
        monitor-exit(r2);
        return;
    L_0x0024:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.onUnlockUser(int):void");
    }

    void onSwitchUser(int userId) {
        synchronized (this.mMethodMap) {
            switchUserLocked(userId);
        }
    }

    public InputMethodManagerService(Context context) {
        this.mContext = context;
        this.mRes = context.getResources();
        this.mHandler = new Handler(this);
        this.mSettingsObserver = new SettingsObserver(this.mHandler);
        this.mIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
        this.mWindowManagerInternal = (WindowManagerInternal) LocalServices.getService(WindowManagerInternal.class);
        this.mCaller = new HandlerCaller(context, null, new HandlerCaller.Callback() {
            public void executeMessage(Message msg) {
                InputMethodManagerService.this.handleMessage(msg);
            }
        }, true);
        this.mAppOpsManager = (AppOpsManager) this.mContext.getSystemService(AppOpsManager.class);
        this.mUserManager = (UserManager) this.mContext.getSystemService(UserManager.class);
        this.mHardKeyboardListener = new HardKeyboardListener();
        this.mHasFeature = context.getPackageManager().hasSystemFeature("android.software.input_methods");
        this.mSlotIme = this.mContext.getString(17040827);
        this.mHardKeyboardBehavior = this.mContext.getResources().getInteger(17694783);
        Bundle extras = new Bundle();
        extras.putBoolean("android.allowDuringSetup", true);
        this.mImeSwitcherNotification = new Notification.Builder(this.mContext, SystemNotificationChannels.VIRTUAL_KEYBOARD).setSmallIcon(17302679).setWhen(0).setOngoing(true).addExtras(extras).setCategory("sys").setColor(this.mContext.getColor(17170763));
        this.mImeSwitchPendingIntent = PendingIntent.getBroadcast(this.mContext, 0, new Intent(ACTION_SHOW_INPUT_METHOD_PICKER).setPackage(this.mContext.getPackageName()), 0);
        this.mShowOngoingImeSwitcherForPhones = false;
        this.mNotificationShown = false;
        int userId = 0;
        try {
            userId = ActivityManager.getService().getCurrentUser().id;
        } catch (RemoteException e) {
            Slog.w(TAG, "Couldn't get current user ID; guessing it's 0", e);
        }
        this.mSettings = new InputMethodSettings(this.mRes, context.getContentResolver(), this.mMethodMap, this.mMethodList, userId, this.mSystemReady ^ 1);
        updateCurrentProfileIds();
        this.mFileManager = new InputMethodFileManager(this.mMethodMap, userId);
        this.mSwitchingController = InputMethodSubtypeSwitchingController.createInstanceLocked(this.mSettings, context);
    }

    private void resetDefaultImeLocked(Context context) {
        if (this.mCurMethodId == null || (InputMethodUtils.isSystemIme((InputMethodInfo) this.mMethodMap.get(this.mCurMethodId)) ^ 1) == 0) {
            List<InputMethodInfo> suitableImes = InputMethodUtils.getDefaultEnabledImes(context, this.mSettings.getEnabledInputMethodListLocked());
            if (suitableImes.isEmpty()) {
                Slog.i(TAG, "No default found");
            } else {
                setSelectedInputMethodAndSubtypeLocked((InputMethodInfo) suitableImes.get(0), -1, false);
            }
        }
    }

    private void switchUserLocked(int newUserId) {
        this.mSettingsObserver.registerContentObserverLocked(newUserId);
        this.mSettings.switchCurrentUser(newUserId, this.mSystemReady ? this.mUserManager.isUserUnlockingOrUnlocked(newUserId) ^ 1 : true);
        updateCurrentProfileIds();
        this.mFileManager = new InputMethodFileManager(this.mMethodMap, newUserId);
        boolean initialUserSwitch = TextUtils.isEmpty(this.mSettings.getSelectedInputMethod());
        this.mLastSystemLocales = this.mRes.getConfiguration().getLocales();
        if (this.mSystemReady) {
            hideCurrentInputLocked(0, null);
            resetCurrentMethodAndClient(6);
            buildInputMethodListLocked(initialUserSwitch);
            if (TextUtils.isEmpty(this.mSettings.getSelectedInputMethod())) {
                resetDefaultImeLocked(this.mContext);
            }
            updateFromSettingsLocked(true);
            try {
                startInputInnerLocked();
            } catch (RuntimeException e) {
                Slog.w(TAG, "Unexpected exception", e);
            }
        }
        if (initialUserSwitch) {
            InputMethodUtils.setNonSelectedSystemImesDisabledUntilUsed(this.mIPackageManager, this.mSettings.getEnabledInputMethodListLocked(), newUserId, this.mContext.getBasePackageName());
        }
    }

    void updateCurrentProfileIds() {
        this.mSettings.setCurrentProfileIds(this.mUserManager.getProfileIdsWithDisabled(this.mSettings.getCurrentUserId()));
    }

    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        try {
            return super.onTransact(code, data, reply, flags);
        } catch (RuntimeException e) {
            if (!(e instanceof SecurityException)) {
                Slog.wtf(TAG, "Input Method Manager Crash", e);
            }
            throw e;
        }
    }

    public void systemRunning(StatusBarManagerService statusBar) {
        synchronized (this.mMethodMap) {
            if (!this.mSystemReady) {
                this.mSystemReady = true;
                this.mLastSystemLocales = this.mRes.getConfiguration().getLocales();
                int currentUserId = this.mSettings.getCurrentUserId();
                this.mSettings.switchCurrentUser(currentUserId, this.mUserManager.isUserUnlockingOrUnlocked(currentUserId) ^ 1);
                this.mKeyguardManager = (KeyguardManager) this.mContext.getSystemService(KeyguardManager.class);
                this.mNotificationManager = (NotificationManager) this.mContext.getSystemService(NotificationManager.class);
                this.mStatusBar = statusBar;
                if (this.mStatusBar != null) {
                    this.mStatusBar.setIconVisibility(this.mSlotIme, false);
                }
                updateSystemUiLocked(this.mCurToken, this.mImeWindowVis, this.mBackDisposition);
                this.mShowOngoingImeSwitcherForPhones = this.mRes.getBoolean(17957087);
                if (this.mShowOngoingImeSwitcherForPhones) {
                    this.mWindowManagerInternal.setOnHardKeyboardStatusChangeListener(this.mHardKeyboardListener);
                }
                this.mMyPackageMonitor.register(this.mContext, null, UserHandle.ALL, true);
                this.mSettingsObserver.registerContentObserverLocked(currentUserId);
                IntentFilter broadcastFilter = new IntentFilter();
                broadcastFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
                broadcastFilter.addAction("android.intent.action.USER_ADDED");
                broadcastFilter.addAction("android.intent.action.USER_REMOVED");
                broadcastFilter.addAction("android.os.action.SETTING_RESTORED");
                broadcastFilter.addAction("android.intent.action.LOCALE_CHANGED");
                broadcastFilter.addAction(ACTION_SHOW_INPUT_METHOD_PICKER);
                this.mContext.registerReceiver(new ImmsBroadcastReceiver(), broadcastFilter);
                buildInputMethodListLocked(true);
                resetDefaultImeLocked(this.mContext);
                updateFromSettingsLocked(true);
                InputMethodUtils.setNonSelectedSystemImesDisabledUntilUsed(this.mIPackageManager, this.mSettings.getEnabledInputMethodListLocked(), currentUserId, this.mContext.getBasePackageName());
                try {
                    startInputInnerLocked();
                } catch (RuntimeException e) {
                    Slog.w(TAG, "Unexpected exception", e);
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean calledFromValidUser() {
        /*
        r6 = this;
        r4 = 1;
        r5 = 0;
        r0 = android.os.Binder.getCallingUid();
        r1 = android.os.UserHandle.getUserId(r0);
        r2 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        if (r0 == r2) goto L_0x0016;
    L_0x000e:
        r2 = r6.mSettings;
        r2 = r2.isCurrentProfile(r1);
        if (r2 == 0) goto L_0x0017;
    L_0x0016:
        return r4;
    L_0x0017:
        r2 = r6.mContext;
        r3 = "android.permission.INTERACT_ACROSS_USERS_FULL";
        r2 = r2.checkCallingOrSelfPermission(r3);
        if (r2 != 0) goto L_0x0023;
    L_0x0022:
        return r4;
    L_0x0023:
        r2 = "InputMethodManagerService";
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "--- IPC called from background users. Ignore. callers=";
        r3 = r3.append(r4);
        r4 = 10;
        r4 = android.os.Debug.getCallers(r4);
        r3 = r3.append(r4);
        r3 = r3.toString();
        android.util.Slog.w(r2, r3);
        return r5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.calledFromValidUser():boolean");
    }

    private boolean calledWithValidToken(IBinder token) {
        if (token == null && Binder.getCallingPid() == Process.myPid()) {
            return false;
        }
        if (token != null && token == this.mCurToken) {
            return true;
        }
        Slog.e(TAG, "Ignoring " + Debug.getCaller() + " due to an invalid token." + " uid:" + Binder.getCallingUid() + " token:" + token);
        return false;
    }

    private boolean bindCurrentInputMethodService(Intent service, ServiceConnection conn, int flags) {
        if (service != null && conn != null) {
            return this.mContext.bindServiceAsUser(service, conn, flags, new UserHandle(this.mSettings.getCurrentUserId()));
        }
        Slog.e(TAG, "--- bind failed: service = " + service + ", conn = " + conn);
        return false;
    }

    public List<InputMethodInfo> getInputMethodList() {
        if (!calledFromValidUser()) {
            return Collections.emptyList();
        }
        List arrayList;
        synchronized (this.mMethodMap) {
            arrayList = new ArrayList(this.mMethodList);
        }
        return arrayList;
    }

    public List<InputMethodInfo> getEnabledInputMethodList() {
        if (!calledFromValidUser()) {
            return Collections.emptyList();
        }
        List enabledInputMethodListLocked;
        synchronized (this.mMethodMap) {
            enabledInputMethodListLocked = this.mSettings.getEnabledInputMethodListLocked();
        }
        return enabledInputMethodListLocked;
    }

    public List<InputMethodSubtype> getEnabledInputMethodSubtypeList(String imiId, boolean allowsImplicitlySelectedSubtypes) {
        if (!calledFromValidUser()) {
            return Collections.emptyList();
        }
        synchronized (this.mMethodMap) {
            InputMethodInfo imi;
            List<InputMethodSubtype> emptyList;
            if (imiId == null) {
                if (this.mCurMethodId != null) {
                    imi = (InputMethodInfo) this.mMethodMap.get(this.mCurMethodId);
                    if (imi != null) {
                        emptyList = Collections.emptyList();
                        return emptyList;
                    }
                    emptyList = this.mSettings.getEnabledInputMethodSubtypeListLocked(this.mContext, imi, allowsImplicitlySelectedSubtypes);
                    return emptyList;
                }
            }
            imi = (InputMethodInfo) this.mMethodMap.get(imiId);
            if (imi != null) {
                emptyList = this.mSettings.getEnabledInputMethodSubtypeListLocked(this.mContext, imi, allowsImplicitlySelectedSubtypes);
                return emptyList;
            }
            emptyList = Collections.emptyList();
            return emptyList;
        }
    }

    public void addClient(IInputMethodClient client, IInputContext inputContext, int uid, int pid) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                this.mClients.put(client.asBinder(), new ClientState(client, inputContext, uid, pid));
            }
        }
    }

    public void removeClient(IInputMethodClient client) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                ClientState cs = (ClientState) this.mClients.remove(client.asBinder());
                if (cs != null) {
                    clearClientSessionLocked(cs);
                    if (this.mCurClient == cs) {
                        this.mCurClient = null;
                    }
                    if (this.mCurFocusedWindowClient == cs) {
                        this.mCurFocusedWindowClient = null;
                    }
                }
            }
        }
    }

    void executeOrSendMessage(IInterface target, Message msg) {
        if (target.asBinder() instanceof Binder) {
            this.mCaller.sendMessage(msg);
            return;
        }
        handleMessage(msg);
        msg.recycle();
    }

    void unbindCurrentClientLocked(int unbindClientReason) {
        if (this.mCurClient != null) {
            if (this.mBoundToMethod) {
                this.mBoundToMethod = false;
                if (this.mCurMethod != null) {
                    executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageO(1000, this.mCurMethod));
                }
            }
            executeOrSendMessage(this.mCurClient.client, this.mCaller.obtainMessageIIO(MSG_SET_ACTIVE, 0, 0, this.mCurClient));
            executeOrSendMessage(this.mCurClient.client, this.mCaller.obtainMessageIIO(MSG_UNBIND_CLIENT, this.mCurSeq, unbindClientReason, this.mCurClient.client));
            this.mCurClient.sessionRequested = false;
            this.mCurClient = null;
            hideInputMethodMenuLocked();
        }
    }

    private int getImeShowFlags() {
        if (this.mShowForced) {
            return 3;
        }
        if (this.mShowExplicitlyRequested) {
            return 1;
        }
        return 0;
    }

    private int getAppShowFlags() {
        if (this.mShowForced) {
            return 2;
        }
        if (this.mShowExplicitlyRequested) {
            return 0;
        }
        return 1;
    }

    InputBindResult attachNewInputLocked(int startInputReason, boolean initial) {
        if (!this.mBoundToMethod) {
            executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageOO(MSG_BIND_INPUT, this.mCurMethod, this.mCurClient.binding));
            this.mBoundToMethod = true;
        }
        Binder startInputToken = new Binder();
        StartInputInfo info = new StartInputInfo(this.mCurToken, this.mCurId, startInputReason, initial ^ 1, this.mCurFocusedWindow, this.mCurAttribute, this.mCurFocusedWindowSoftInputMode, this.mCurSeq);
        this.mStartInputMap.put(startInputToken, info);
        this.mStartInputHistory.addEntry(info);
        SessionState session = this.mCurClient.curSession;
        executeOrSendMessage(session.method, this.mCaller.obtainMessageIIOOOO(MSG_START_INPUT, this.mCurInputContextMissingMethods, initial ? 0 : 1, startInputToken, session, this.mCurInputContext, this.mCurAttribute));
        if (this.mShowRequested) {
            showCurrentInputLocked(getAppShowFlags(), null);
        }
        return new InputBindResult(session.session, session.channel != null ? session.channel.dup() : null, this.mCurId, this.mCurSeq, this.mCurUserActionNotificationSequenceNumber);
    }

    InputBindResult startInputLocked(int startInputReason, IInputMethodClient client, IInputContext inputContext, int missingMethods, EditorInfo attribute, int controlFlags) {
        if (this.mCurMethodId == null) {
            return this.mNoBinding;
        }
        ClientState cs = (ClientState) this.mClients.get(client.asBinder());
        if (cs == null) {
            throw new IllegalArgumentException("unknown client " + client.asBinder());
        } else if (attribute == null) {
            Slog.w(TAG, "Ignoring startInput with null EditorInfo. uid=" + cs.uid + " pid=" + cs.pid);
            return null;
        } else {
            try {
                if (!this.mIWindowManager.inputMethodClientHasFocus(cs.client)) {
                    return null;
                }
            } catch (RemoteException e) {
            }
            return startInputUncheckedLocked(cs, inputContext, missingMethods, attribute, controlFlags, startInputReason);
        }
    }

    InputBindResult startInputUncheckedLocked(ClientState cs, IInputContext inputContext, int missingMethods, EditorInfo attribute, int controlFlags, int startInputReason) {
        if (this.mCurMethodId == null) {
            return this.mNoBinding;
        }
        if (InputMethodUtils.checkIfPackageBelongsToUid(this.mAppOpsManager, cs.uid, attribute.packageName)) {
            if (this.mCurClient != cs) {
                this.mCurClientInKeyguard = isKeyguardLocked();
                unbindCurrentClientLocked(1);
                if (this.mIsInteractive) {
                    executeOrSendMessage(cs.client, this.mCaller.obtainMessageIO(MSG_SET_ACTIVE, this.mIsInteractive ? 1 : 0, cs));
                }
            }
            this.mCurSeq++;
            if (this.mCurSeq <= 0) {
                this.mCurSeq = 1;
            }
            this.mCurClient = cs;
            this.mCurInputContext = inputContext;
            this.mCurInputContextMissingMethods = missingMethods;
            this.mCurAttribute = attribute;
            if (this.mCurId != null && this.mCurId.equals(this.mCurMethodId)) {
                if (cs.curSession != null) {
                    return attachNewInputLocked(startInputReason, (controlFlags & 256) != 0);
                } else if (this.mHaveConnection) {
                    if (this.mCurMethod != null) {
                        requestClientSessionLocked(cs);
                        return new InputBindResult(null, null, this.mCurId, this.mCurSeq, this.mCurUserActionNotificationSequenceNumber);
                    } else if (SystemClock.uptimeMillis() < this.mLastBindTime + TIME_TO_RECONNECT) {
                        return new InputBindResult(null, null, this.mCurId, this.mCurSeq, this.mCurUserActionNotificationSequenceNumber);
                    } else {
                        EventLog.writeEvent(EventLogTags.IMF_FORCE_RECONNECT_IME, new Object[]{this.mCurMethodId, Long.valueOf(SystemClock.uptimeMillis() - this.mLastBindTime), Integer.valueOf(0)});
                    }
                }
            }
            return startInputInnerLocked();
        }
        Slog.e(TAG, "Rejecting this client as it reported an invalid package name. uid=" + cs.uid + " package=" + attribute.packageName);
        return this.mNoBinding;
    }

    InputBindResult startInputInnerLocked() {
        if (this.mCurMethodId == null) {
            return this.mNoBinding;
        }
        if (this.mSystemReady) {
            InputMethodInfo info = (InputMethodInfo) this.mMethodMap.get(this.mCurMethodId);
            if (info == null) {
                throw new IllegalArgumentException("Unknown id: " + this.mCurMethodId);
            }
            unbindCurrentMethodLocked(true);
            this.mCurIntent = new Intent("android.view.InputMethod");
            this.mCurIntent.setComponent(info.getComponent());
            this.mCurIntent.putExtra("android.intent.extra.client_label", 17039984);
            this.mCurIntent.putExtra("android.intent.extra.client_intent", PendingIntent.getActivity(this.mContext, 0, new Intent("android.settings.INPUT_METHOD_SETTINGS"), 0));
            if (bindCurrentInputMethodService(this.mCurIntent, this, IME_CONNECTION_BIND_FLAGS)) {
                this.mLastBindTime = SystemClock.uptimeMillis();
                this.mHaveConnection = true;
                this.mCurId = info.getId();
                this.mCurToken = new Binder();
                try {
                    this.mIWindowManager.addWindowToken(this.mCurToken, 2011, 0);
                } catch (RemoteException e) {
                }
                return new InputBindResult(null, null, this.mCurId, this.mCurSeq, this.mCurUserActionNotificationSequenceNumber);
            }
            this.mCurIntent = null;
            Slog.w(TAG, "Failure connecting to input method service: " + this.mCurIntent);
            return null;
        }
        return new InputBindResult(null, null, this.mCurMethodId, this.mCurSeq, this.mCurUserActionNotificationSequenceNumber);
    }

    private InputBindResult startInput(int startInputReason, IInputMethodClient client, IInputContext inputContext, int missingMethods, EditorInfo attribute, int controlFlags) {
        if (!calledFromValidUser()) {
            return null;
        }
        InputBindResult startInputLocked;
        synchronized (this.mMethodMap) {
            long ident = Binder.clearCallingIdentity();
            try {
                startInputLocked = startInputLocked(startInputReason, client, inputContext, missingMethods, attribute, controlFlags);
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        }
        return startInputLocked;
    }

    public void finishInput(IInputMethodClient client) {
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onServiceConnected(android.content.ComponentName r7, android.os.IBinder r8) {
        /*
        r6 = this;
        r1 = r6.mMethodMap;
        monitor-enter(r1);
        r0 = r6.mCurIntent;	 Catch:{ all -> 0x004d }
        if (r0 == 0) goto L_0x004b;
    L_0x0007:
        r0 = r6.mCurIntent;	 Catch:{ all -> 0x004d }
        r0 = r0.getComponent();	 Catch:{ all -> 0x004d }
        r0 = r7.equals(r0);	 Catch:{ all -> 0x004d }
        if (r0 == 0) goto L_0x004b;
    L_0x0013:
        r0 = com.android.internal.view.IInputMethod.Stub.asInterface(r8);	 Catch:{ all -> 0x004d }
        r6.mCurMethod = r0;	 Catch:{ all -> 0x004d }
        r0 = r6.mCurToken;	 Catch:{ all -> 0x004d }
        if (r0 != 0) goto L_0x002c;
    L_0x001d:
        r0 = "InputMethodManagerService";
        r2 = "Service connected without a token!";
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x004d }
        r0 = 0;
        r6.unbindCurrentMethodLocked(r0);	 Catch:{ all -> 0x004d }
        monitor-exit(r1);
        return;
    L_0x002c:
        r0 = r6.mCurMethod;	 Catch:{ all -> 0x004d }
        r2 = r6.mCaller;	 Catch:{ all -> 0x004d }
        r3 = r6.mCurMethod;	 Catch:{ all -> 0x004d }
        r4 = r6.mCurToken;	 Catch:{ all -> 0x004d }
        r5 = 1040; // 0x410 float:1.457E-42 double:5.14E-321;
        r2 = r2.obtainMessageOO(r5, r3, r4);	 Catch:{ all -> 0x004d }
        r6.executeOrSendMessage(r0, r2);	 Catch:{ all -> 0x004d }
        r0 = r6.mCurClient;	 Catch:{ all -> 0x004d }
        if (r0 == 0) goto L_0x004b;
    L_0x0041:
        r0 = r6.mCurClient;	 Catch:{ all -> 0x004d }
        r6.clearClientSessionLocked(r0);	 Catch:{ all -> 0x004d }
        r0 = r6.mCurClient;	 Catch:{ all -> 0x004d }
        r6.requestClientSessionLocked(r0);	 Catch:{ all -> 0x004d }
    L_0x004b:
        monitor-exit(r1);
        return;
    L_0x004d:
        r0 = move-exception;
        monitor-exit(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void onSessionCreated(com.android.internal.view.IInputMethod r7, com.android.internal.view.IInputMethodSession r8, android.view.InputChannel r9) {
        /*
        r6 = this;
        r2 = r6.mMethodMap;
        monitor-enter(r2);
        r1 = r6.mCurMethod;	 Catch:{ all -> 0x004e }
        if (r1 == 0) goto L_0x0049;
    L_0x0007:
        if (r7 == 0) goto L_0x0049;
    L_0x0009:
        r1 = r6.mCurMethod;	 Catch:{ all -> 0x004e }
        r1 = r1.asBinder();	 Catch:{ all -> 0x004e }
        r3 = r7.asBinder();	 Catch:{ all -> 0x004e }
        if (r1 != r3) goto L_0x0049;
    L_0x0015:
        r1 = r6.mCurClient;	 Catch:{ all -> 0x004e }
        if (r1 == 0) goto L_0x0049;
    L_0x0019:
        r1 = r6.mCurClient;	 Catch:{ all -> 0x004e }
        r6.clearClientSessionLocked(r1);	 Catch:{ all -> 0x004e }
        r1 = r6.mCurClient;	 Catch:{ all -> 0x004e }
        r3 = new com.android.server.InputMethodManagerService$SessionState;	 Catch:{ all -> 0x004e }
        r4 = r6.mCurClient;	 Catch:{ all -> 0x004e }
        r3.<init>(r4, r7, r8, r9);	 Catch:{ all -> 0x004e }
        r1.curSession = r3;	 Catch:{ all -> 0x004e }
        r1 = 9;
        r3 = 1;
        r0 = r6.attachNewInputLocked(r1, r3);	 Catch:{ all -> 0x004e }
        r1 = r0.method;	 Catch:{ all -> 0x004e }
        if (r1 == 0) goto L_0x0047;
    L_0x0034:
        r1 = r6.mCurClient;	 Catch:{ all -> 0x004e }
        r1 = r1.client;	 Catch:{ all -> 0x004e }
        r3 = r6.mCaller;	 Catch:{ all -> 0x004e }
        r4 = r6.mCurClient;	 Catch:{ all -> 0x004e }
        r4 = r4.client;	 Catch:{ all -> 0x004e }
        r5 = 3010; // 0xbc2 float:4.218E-42 double:1.487E-320;
        r3 = r3.obtainMessageOO(r5, r4, r0);	 Catch:{ all -> 0x004e }
        r6.executeOrSendMessage(r1, r3);	 Catch:{ all -> 0x004e }
    L_0x0047:
        monitor-exit(r2);
        return;
    L_0x0049:
        monitor-exit(r2);
        r9.dispose();
        return;
    L_0x004e:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.onSessionCreated(com.android.internal.view.IInputMethod, com.android.internal.view.IInputMethodSession, android.view.InputChannel):void");
    }

    void unbindCurrentMethodLocked(boolean savePosition) {
        if (this.mVisibleBound) {
            this.mContext.unbindService(this.mVisibleConnection);
            this.mVisibleBound = false;
        }
        if (this.mHaveConnection) {
            this.mContext.unbindService(this);
            this.mHaveConnection = false;
        }
        if (this.mCurToken != null) {
            try {
                if ((this.mImeWindowVis & 1) != 0 && savePosition) {
                    this.mWindowManagerInternal.saveLastInputMethodWindowForTransition();
                }
                this.mIWindowManager.removeWindowToken(this.mCurToken, 0);
            } catch (RemoteException e) {
            }
            this.mCurToken = null;
        }
        this.mCurId = null;
        clearCurMethodLocked();
    }

    void resetCurrentMethodAndClient(int unbindClientReason) {
        this.mCurMethodId = null;
        unbindCurrentMethodLocked(false);
        unbindCurrentClientLocked(unbindClientReason);
    }

    void requestClientSessionLocked(ClientState cs) {
        if (!cs.sessionRequested) {
            InputChannel[] channels = InputChannel.openInputChannelPair(cs.toString());
            cs.sessionRequested = true;
            executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageOOO(MSG_CREATE_SESSION, this.mCurMethod, channels[1], new MethodCallback(this, this.mCurMethod, channels[0])));
        }
    }

    void clearClientSessionLocked(ClientState cs) {
        finishSessionLocked(cs.curSession);
        cs.curSession = null;
        cs.sessionRequested = false;
    }

    private void finishSessionLocked(SessionState sessionState) {
        if (sessionState != null) {
            if (sessionState.session != null) {
                try {
                    sessionState.session.finishSession();
                } catch (RemoteException e) {
                    Slog.w(TAG, "Session failed to close due to remote exception", e);
                    updateSystemUiLocked(this.mCurToken, 0, this.mBackDisposition);
                }
                sessionState.session = null;
            }
            if (sessionState.channel != null) {
                sessionState.channel.dispose();
                sessionState.channel = null;
            }
        }
    }

    void clearCurMethodLocked() {
        if (this.mCurMethod != null) {
            for (ClientState cs : this.mClients.values()) {
                clearClientSessionLocked(cs);
            }
            finishSessionLocked(this.mEnabledSession);
            this.mEnabledSession = null;
            this.mCurMethod = null;
        }
        if (this.mStatusBar != null) {
            this.mStatusBar.setIconVisibility(this.mSlotIme, false);
        }
        this.mInFullscreenMode = false;
    }

    public void onServiceDisconnected(ComponentName name) {
        synchronized (this.mMethodMap) {
            if (!(this.mCurMethod == null || this.mCurIntent == null || !name.equals(this.mCurIntent.getComponent()))) {
                clearCurMethodLocked();
                this.mLastBindTime = SystemClock.uptimeMillis();
                this.mShowRequested = this.mInputShown;
                this.mInputShown = false;
                unbindCurrentClientLocked(3);
            }
        }
    }

    public void updateStatusIcon(IBinder token, String packageName, int iconId) {
        String str = null;
        synchronized (this.mMethodMap) {
            if (calledWithValidToken(token)) {
                long ident = Binder.clearCallingIdentity();
                if (iconId == 0) {
                    try {
                        if (this.mStatusBar != null) {
                            this.mStatusBar.setIconVisibility(this.mSlotIme, false);
                        }
                    } catch (Throwable th) {
                        Binder.restoreCallingIdentity(ident);
                    }
                } else if (packageName != null) {
                    CharSequence contentDescription = null;
                    try {
                        contentDescription = this.mContext.getPackageManager().getApplicationLabel(this.mIPackageManager.getApplicationInfo(packageName, 0, this.mSettings.getCurrentUserId()));
                    } catch (RemoteException e) {
                    }
                    if (this.mStatusBar != null) {
                        StatusBarManagerService statusBarManagerService = this.mStatusBar;
                        String str2 = this.mSlotIme;
                        if (contentDescription != null) {
                            str = contentDescription.toString();
                        }
                        statusBarManagerService.setIcon(str2, packageName, iconId, 0, str);
                        this.mStatusBar.setIconVisibility(this.mSlotIme, true);
                    }
                }
                Binder.restoreCallingIdentity(ident);
                return;
            }
        }
    }

    private boolean shouldShowImeSwitcherLocked(int visibility) {
        if (!this.mShowOngoingImeSwitcherForPhones) {
            return false;
        }
        if (this.mSwitchingDialog != null) {
            return false;
        }
        if (this.mWindowManagerInternal.isKeyguardShowingAndNotOccluded() && this.mKeyguardManager != null && this.mKeyguardManager.isKeyguardSecure()) {
            return false;
        }
        if ((visibility & 1) == 0) {
            return false;
        }
        if (this.mWindowManagerInternal.isHardKeyboardAvailable()) {
            if (this.mHardKeyboardBehavior == 0) {
                return true;
            }
        } else if ((visibility & 2) == 0) {
            return false;
        }
        List<InputMethodInfo> imis = this.mSettings.getEnabledInputMethodListLocked();
        int N = imis.size();
        if (N > 2) {
            return true;
        }
        if (N < 1) {
            return false;
        }
        int nonAuxCount = 0;
        int auxCount = 0;
        InputMethodSubtype nonAuxSubtype = null;
        InputMethodSubtype auxSubtype = null;
        for (int i = 0; i < N; i++) {
            List<InputMethodSubtype> subtypes = this.mSettings.getEnabledInputMethodSubtypeListLocked(this.mContext, (InputMethodInfo) imis.get(i), true);
            int subtypeCount = subtypes.size();
            if (subtypeCount == 0) {
                nonAuxCount++;
            } else {
                for (int j = 0; j < subtypeCount; j++) {
                    InputMethodSubtype subtype = (InputMethodSubtype) subtypes.get(j);
                    if (subtype.isAuxiliary()) {
                        auxCount++;
                        auxSubtype = subtype;
                    } else {
                        nonAuxCount++;
                        nonAuxSubtype = subtype;
                    }
                }
            }
        }
        if (nonAuxCount > 1 || auxCount > 1) {
            return true;
        }
        if (nonAuxCount != 1 || auxCount != 1) {
            return false;
        }
        if (nonAuxSubtype == null || r2 == null || ((!nonAuxSubtype.getLocale().equals(r2.getLocale()) && !r2.overridesImplicitlyEnabledSubtype() && !nonAuxSubtype.overridesImplicitlyEnabledSubtype()) || !nonAuxSubtype.containsExtraValueKey(TAG_TRY_SUPPRESSING_IME_SWITCHER))) {
            return true;
        }
        return false;
    }

    private boolean isKeyguardLocked() {
        return this.mKeyguardManager != null ? this.mKeyguardManager.isKeyguardLocked() : false;
    }

    public void setImeWindowStatus(IBinder token, IBinder startInputToken, int vis, int backDisposition) {
        IBinder iBinder = null;
        boolean z = false;
        if (calledWithValidToken(token)) {
            StartInputInfo info;
            boolean dismissImeOnBackKeyPressed;
            synchronized (this.mMethodMap) {
                info = (StartInputInfo) this.mStartInputMap.get(startInputToken);
                this.mImeWindowVis = vis;
                this.mBackDisposition = backDisposition;
                updateSystemUiLocked(token, vis, backDisposition);
            }
            switch (backDisposition) {
                case 1:
                    dismissImeOnBackKeyPressed = false;
                    break;
                case 2:
                    dismissImeOnBackKeyPressed = true;
                    break;
                default:
                    if ((vis & 2) == 0) {
                        dismissImeOnBackKeyPressed = false;
                        break;
                    } else {
                        dismissImeOnBackKeyPressed = true;
                        break;
                    }
            }
            WindowManagerInternal windowManagerInternal = this.mWindowManagerInternal;
            if ((vis & 2) != 0) {
                z = true;
            }
            if (info != null) {
                iBinder = info.mTargetWindow;
            }
            windowManagerInternal.updateInputMethodWindowStatus(token, z, dismissImeOnBackKeyPressed, iBinder);
        }
    }

    private void updateSystemUi(IBinder token, int vis, int backDisposition) {
        synchronized (this.mMethodMap) {
            updateSystemUiLocked(token, vis, backDisposition);
        }
    }

    private void updateSystemUiLocked(IBinder token, int vis, int backDisposition) {
        if (calledWithValidToken(token)) {
            long ident = Binder.clearCallingIdentity();
            if (vis != 0) {
                try {
                    if (isKeyguardLocked() && (this.mCurClientInKeyguard ^ 1) != 0) {
                        vis = 0;
                    }
                } catch (Throwable th) {
                    Binder.restoreCallingIdentity(ident);
                }
            }
            boolean needsToShowImeSwitcher = shouldShowImeSwitcherLocked(vis);
            if (this.mStatusBar != null) {
                this.mStatusBar.setImeWindowStatus(token, vis, backDisposition, needsToShowImeSwitcher);
            }
            InputMethodInfo imi = (InputMethodInfo) this.mMethodMap.get(this.mCurMethodId);
            if (imi != null && needsToShowImeSwitcher) {
                CharSequence title = this.mRes.getText(17040743);
                this.mImeSwitcherNotification.setContentTitle(title).setContentText(InputMethodUtils.getImeAndSubtypeDisplayName(this.mContext, imi, this.mCurrentSubtype)).setContentIntent(this.mImeSwitchPendingIntent);
                try {
                    if (!(this.mNotificationManager == null || (this.mIWindowManager.hasNavigationBar() ^ 1) == 0)) {
                        this.mNotificationManager.notifyAsUser(null, 8, this.mImeSwitcherNotification.build(), UserHandle.ALL);
                        this.mNotificationShown = true;
                    }
                } catch (RemoteException e) {
                }
            } else if (this.mNotificationShown && this.mNotificationManager != null) {
                this.mNotificationManager.cancelAsUser(null, 8, UserHandle.ALL);
                this.mNotificationShown = false;
            }
            Binder.restoreCallingIdentity(ident);
        }
    }

    public void registerSuggestionSpansForNotification(SuggestionSpan[] spans) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                InputMethodInfo currentImi = (InputMethodInfo) this.mMethodMap.get(this.mCurMethodId);
                for (SuggestionSpan ss : spans) {
                    if (!TextUtils.isEmpty(ss.getNotificationTargetClassName())) {
                        this.mSecureSuggestionSpans.put(ss, currentImi);
                    }
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean notifySuggestionPicked(android.text.style.SuggestionSpan r10, java.lang.String r11, int r12) {
        /*
        r9 = this;
        r8 = 0;
        r6 = r9.calledFromValidUser();
        if (r6 != 0) goto L_0x0008;
    L_0x0007:
        return r8;
    L_0x0008:
        r7 = r9.mMethodMap;
        monitor-enter(r7);
        r6 = r9.mSecureSuggestionSpans;	 Catch:{ all -> 0x0064 }
        r5 = r6.get(r10);	 Catch:{ all -> 0x0064 }
        r5 = (android.view.inputmethod.InputMethodInfo) r5;	 Catch:{ all -> 0x0064 }
        if (r5 == 0) goto L_0x0067;
    L_0x0015:
        r4 = r10.getSuggestions();	 Catch:{ all -> 0x0064 }
        if (r12 < 0) goto L_0x001e;
    L_0x001b:
        r6 = r4.length;	 Catch:{ all -> 0x0064 }
        if (r12 < r6) goto L_0x0020;
    L_0x001e:
        monitor-exit(r7);
        return r8;
    L_0x0020:
        r0 = r10.getNotificationTargetClassName();	 Catch:{ all -> 0x0064 }
        r1 = new android.content.Intent;	 Catch:{ all -> 0x0064 }
        r1.<init>();	 Catch:{ all -> 0x0064 }
        r6 = r5.getPackageName();	 Catch:{ all -> 0x0064 }
        r1.setClassName(r6, r0);	 Catch:{ all -> 0x0064 }
        r6 = "android.text.style.SUGGESTION_PICKED";
        r1.setAction(r6);	 Catch:{ all -> 0x0064 }
        r6 = "before";
        r1.putExtra(r6, r11);	 Catch:{ all -> 0x0064 }
        r6 = "after";
        r8 = r4[r12];	 Catch:{ all -> 0x0064 }
        r1.putExtra(r6, r8);	 Catch:{ all -> 0x0064 }
        r6 = "hashcode";
        r8 = r10.hashCode();	 Catch:{ all -> 0x0064 }
        r1.putExtra(r6, r8);	 Catch:{ all -> 0x0064 }
        r2 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x0064 }
        r6 = r9.mContext;	 Catch:{ all -> 0x005f }
        r8 = android.os.UserHandle.CURRENT;	 Catch:{ all -> 0x005f }
        r6.sendBroadcastAsUser(r1, r8);	 Catch:{ all -> 0x005f }
        android.os.Binder.restoreCallingIdentity(r2);	 Catch:{ all -> 0x0064 }
        r6 = 1;
        monitor-exit(r7);
        return r6;
    L_0x005f:
        r6 = move-exception;
        android.os.Binder.restoreCallingIdentity(r2);	 Catch:{ all -> 0x0064 }
        throw r6;	 Catch:{ all -> 0x0064 }
    L_0x0064:
        r6 = move-exception;
        monitor-exit(r7);
        throw r6;
    L_0x0067:
        monitor-exit(r7);
        return r8;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.notifySuggestionPicked(android.text.style.SuggestionSpan, java.lang.String, int):boolean");
    }

    void updateFromSettingsLocked(boolean enabledMayChange) {
        updateInputMethodsFromSettingsLocked(enabledMayChange);
        updateKeyboardFromSettingsLocked();
    }

    void updateInputMethodsFromSettingsLocked(boolean enabledMayChange) {
        if (enabledMayChange) {
            List<InputMethodInfo> enabled = this.mSettings.getEnabledInputMethodListLocked();
            for (int i = 0; i < enabled.size(); i++) {
                InputMethodInfo imm = (InputMethodInfo) enabled.get(i);
                try {
                    ApplicationInfo ai = this.mIPackageManager.getApplicationInfo(imm.getPackageName(), 32768, this.mSettings.getCurrentUserId());
                    if (ai != null && ai.enabledSetting == 4) {
                        this.mIPackageManager.setApplicationEnabledSetting(imm.getPackageName(), 0, 1, this.mSettings.getCurrentUserId(), this.mContext.getBasePackageName());
                    }
                } catch (RemoteException e) {
                }
            }
        }
        String id = this.mSettings.getSelectedInputMethod();
        if (TextUtils.isEmpty(id) && chooseNewDefaultIMELocked()) {
            id = this.mSettings.getSelectedInputMethod();
        }
        if (TextUtils.isEmpty(id)) {
            resetCurrentMethodAndClient(4);
        } else {
            try {
                setInputMethodLocked(id, this.mSettings.getSelectedInputMethodSubtypeId(id));
            } catch (IllegalArgumentException e2) {
                Slog.w(TAG, "Unknown input method from prefs: " + id, e2);
                resetCurrentMethodAndClient(5);
            }
            this.mShortcutInputMethodsAndSubtypes.clear();
        }
        this.mSwitchingController.resetCircularListLocked(this.mContext);
    }

    public void updateKeyboardFromSettingsLocked() {
        this.mShowImeWithHardKeyboard = this.mSettings.isShowImeWithHardKeyboardEnabled();
        if (this.mSwitchingDialog != null && this.mSwitchingDialogTitleView != null && this.mSwitchingDialog.isShowing()) {
            ((Switch) this.mSwitchingDialogTitleView.findViewById(16908920)).setChecked(this.mShowImeWithHardKeyboard);
        }
    }

    private void notifyInputMethodSubtypeChanged(int userId, InputMethodInfo inputMethodInfo, InputMethodSubtype subtype) {
        InputManagerInternal inputManagerInternal = (InputManagerInternal) LocalServices.getService(InputManagerInternal.class);
        if (inputManagerInternal != null) {
            inputManagerInternal.onInputMethodSubtypeChanged(userId, inputMethodInfo, subtype);
        }
    }

    void setInputMethodLocked(String id, int subtypeId) {
        InputMethodInfo info = (InputMethodInfo) this.mMethodMap.get(id);
        if (info == null) {
            throw new IllegalArgumentException("Unknown id: " + id);
        } else if (id.equals(this.mCurMethodId)) {
            int subtypeCount = info.getSubtypeCount();
            if (subtypeCount > 0) {
                InputMethodSubtype newSubtype;
                InputMethodSubtype oldSubtype = this.mCurrentSubtype;
                if (subtypeId < 0 || subtypeId >= subtypeCount) {
                    newSubtype = getCurrentInputMethodSubtypeLocked();
                } else {
                    newSubtype = info.getSubtypeAt(subtypeId);
                }
                if (newSubtype == null || oldSubtype == null) {
                    Slog.w(TAG, "Illegal subtype state: old subtype = " + oldSubtype + ", new subtype = " + newSubtype);
                    return;
                }
                if (newSubtype != oldSubtype) {
                    setSelectedInputMethodAndSubtypeLocked(info, subtypeId, true);
                    if (this.mCurMethod != null) {
                        try {
                            updateSystemUiLocked(this.mCurToken, this.mImeWindowVis, this.mBackDisposition);
                            this.mCurMethod.changeInputMethodSubtype(newSubtype);
                        } catch (RemoteException e) {
                            Slog.w(TAG, "Failed to call changeInputMethodSubtype");
                            return;
                        }
                    }
                    notifyInputMethodSubtypeChanged(this.mSettings.getCurrentUserId(), info, newSubtype);
                }
            }
        } else {
            long ident = Binder.clearCallingIdentity();
            try {
                setSelectedInputMethodAndSubtypeLocked(info, subtypeId, false);
                this.mCurMethodId = id;
                if (((ActivityManagerInternal) LocalServices.getService(ActivityManagerInternal.class)).isSystemReady()) {
                    Intent intent = new Intent("android.intent.action.INPUT_METHOD_CHANGED");
                    intent.addFlags(536870912);
                    intent.putExtra("input_method_id", id);
                    this.mContext.sendBroadcastAsUser(intent, UserHandle.CURRENT);
                }
                unbindCurrentClientLocked(2);
                notifyInputMethodSubtypeChanged(this.mSettings.getCurrentUserId(), info, getCurrentInputMethodSubtypeLocked());
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean showSoftInput(com.android.internal.view.IInputMethodClient r10, int r11, android.os.ResultReceiver r12) {
        /*
        r9 = this;
        r8 = 0;
        r4 = r9.calledFromValidUser();
        if (r4 != 0) goto L_0x0008;
    L_0x0007:
        return r8;
    L_0x0008:
        r1 = android.os.Binder.getCallingUid();
        r2 = android.os.Binder.clearCallingIdentity();
        r5 = r9.mMethodMap;	 Catch:{ all -> 0x006b }
        monitor-enter(r5);	 Catch:{ all -> 0x006b }
        r4 = r9.mCurClient;	 Catch:{ all -> 0x0068 }
        if (r4 == 0) goto L_0x0019;
    L_0x0017:
        if (r10 != 0) goto L_0x004b;
    L_0x0019:
        r4 = r9.mIWindowManager;	 Catch:{ RemoteException -> 0x0062 }
        r4 = r4.inputMethodClientHasFocus(r10);	 Catch:{ RemoteException -> 0x0062 }
        if (r4 != 0) goto L_0x0059;
    L_0x0021:
        r4 = "InputMethodManagerService";
        r6 = new java.lang.StringBuilder;	 Catch:{ RemoteException -> 0x0062 }
        r6.<init>();	 Catch:{ RemoteException -> 0x0062 }
        r7 = "Ignoring showSoftInput of uid ";
        r6 = r6.append(r7);	 Catch:{ RemoteException -> 0x0062 }
        r6 = r6.append(r1);	 Catch:{ RemoteException -> 0x0062 }
        r7 = ": ";
        r6 = r6.append(r7);	 Catch:{ RemoteException -> 0x0062 }
        r6 = r6.append(r10);	 Catch:{ RemoteException -> 0x0062 }
        r6 = r6.toString();	 Catch:{ RemoteException -> 0x0062 }
        android.util.Slog.w(r4, r6);	 Catch:{ RemoteException -> 0x0062 }
        monitor-exit(r5);	 Catch:{ all -> 0x006b }
        android.os.Binder.restoreCallingIdentity(r2);
        return r8;
    L_0x004b:
        r4 = r9.mCurClient;	 Catch:{ all -> 0x0068 }
        r4 = r4.client;	 Catch:{ all -> 0x0068 }
        r4 = r4.asBinder();	 Catch:{ all -> 0x0068 }
        r6 = r10.asBinder();	 Catch:{ all -> 0x0068 }
        if (r4 != r6) goto L_0x0019;
    L_0x0059:
        r4 = r9.showCurrentInputLocked(r11, r12);	 Catch:{ all -> 0x0068 }
        monitor-exit(r5);	 Catch:{ all -> 0x006b }
        android.os.Binder.restoreCallingIdentity(r2);
        return r4;
    L_0x0062:
        r0 = move-exception;
        monitor-exit(r5);	 Catch:{ all -> 0x006b }
        android.os.Binder.restoreCallingIdentity(r2);
        return r8;
    L_0x0068:
        r4 = move-exception;
        monitor-exit(r5);	 Catch:{ all -> 0x006b }
        throw r4;	 Catch:{ all -> 0x006b }
    L_0x006b:
        r4 = move-exception;
        android.os.Binder.restoreCallingIdentity(r2);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.showSoftInput(com.android.internal.view.IInputMethodClient, int, android.os.ResultReceiver):boolean");
    }

    boolean showCurrentInputLocked(int flags, ResultReceiver resultReceiver) {
        this.mShowRequested = true;
        if (this.mAccessibilityRequestingNoSoftKeyboard) {
            return false;
        }
        if ((flags & 2) != 0) {
            this.mShowExplicitlyRequested = true;
            this.mShowForced = true;
        } else if ((flags & 1) == 0) {
            this.mShowExplicitlyRequested = true;
        }
        if (!this.mSystemReady) {
            return false;
        }
        boolean res = false;
        if (this.mCurMethod != null) {
            executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageIOO(MSG_SHOW_SOFT_INPUT, getImeShowFlags(), this.mCurMethod, resultReceiver));
            this.mInputShown = true;
            if (this.mHaveConnection && (this.mVisibleBound ^ 1) != 0) {
                bindCurrentInputMethodService(this.mCurIntent, this.mVisibleConnection, IME_VISIBLE_BIND_FLAGS);
                this.mVisibleBound = true;
            }
            res = true;
        } else if (this.mHaveConnection && SystemClock.uptimeMillis() >= this.mLastBindTime + TIME_TO_RECONNECT) {
            EventLog.writeEvent(EventLogTags.IMF_FORCE_RECONNECT_IME, new Object[]{this.mCurMethodId, Long.valueOf(SystemClock.uptimeMillis() - this.mLastBindTime), Integer.valueOf(1)});
            Slog.w(TAG, "Force disconnect/connect to the IME in showCurrentInputLocked()");
            this.mContext.unbindService(this);
            bindCurrentInputMethodService(this.mCurIntent, this, IME_CONNECTION_BIND_FLAGS);
        }
        if (res) {
            SystemProperties.set("sys.grip.status", "off");
        }
        return res;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hideSoftInput(com.android.internal.view.IInputMethodClient r9, int r10, android.os.ResultReceiver r11) {
        /*
        r8 = this;
        r7 = 0;
        r4 = r8.calledFromValidUser();
        if (r4 != 0) goto L_0x0008;
    L_0x0007:
        return r7;
    L_0x0008:
        r1 = android.os.Binder.getCallingUid();
        r2 = android.os.Binder.clearCallingIdentity();
        r5 = r8.mMethodMap;	 Catch:{ all -> 0x0046 }
        monitor-enter(r5);	 Catch:{ all -> 0x0046 }
        r4 = r8.mCurClient;	 Catch:{ all -> 0x0043 }
        if (r4 == 0) goto L_0x0019;
    L_0x0017:
        if (r9 != 0) goto L_0x0026;
    L_0x0019:
        r4 = r8.mIWindowManager;	 Catch:{ RemoteException -> 0x003d }
        r4 = r4.inputMethodClientHasFocus(r9);	 Catch:{ RemoteException -> 0x003d }
        if (r4 != 0) goto L_0x0034;
    L_0x0021:
        monitor-exit(r5);	 Catch:{ all -> 0x0046 }
        android.os.Binder.restoreCallingIdentity(r2);
        return r7;
    L_0x0026:
        r4 = r8.mCurClient;	 Catch:{ all -> 0x0043 }
        r4 = r4.client;	 Catch:{ all -> 0x0043 }
        r4 = r4.asBinder();	 Catch:{ all -> 0x0043 }
        r6 = r9.asBinder();	 Catch:{ all -> 0x0043 }
        if (r4 != r6) goto L_0x0019;
    L_0x0034:
        r4 = r8.hideCurrentInputLocked(r10, r11);	 Catch:{ all -> 0x0043 }
        monitor-exit(r5);	 Catch:{ all -> 0x0046 }
        android.os.Binder.restoreCallingIdentity(r2);
        return r4;
    L_0x003d:
        r0 = move-exception;
        monitor-exit(r5);	 Catch:{ all -> 0x0046 }
        android.os.Binder.restoreCallingIdentity(r2);
        return r7;
    L_0x0043:
        r4 = move-exception;
        monitor-exit(r5);	 Catch:{ all -> 0x0046 }
        throw r4;	 Catch:{ all -> 0x0046 }
    L_0x0046:
        r4 = move-exception;
        android.os.Binder.restoreCallingIdentity(r2);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.hideSoftInput(com.android.internal.view.IInputMethodClient, int, android.os.ResultReceiver):boolean");
    }

    boolean hideCurrentInputLocked(int flags, ResultReceiver resultReceiver) {
        if ((flags & 1) != 0 && (this.mShowExplicitlyRequested || this.mShowForced)) {
            return false;
        }
        if (this.mShowForced && (flags & 2) != 0) {
            return false;
        }
        boolean res;
        boolean shouldHideSoftInput = this.mCurMethod != null ? !this.mInputShown ? (this.mImeWindowVis & 1) != 0 : true : false;
        if (shouldHideSoftInput) {
            executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageOO(MSG_HIDE_SOFT_INPUT, this.mCurMethod, resultReceiver));
            res = true;
        } else {
            res = false;
        }
        if (this.mHaveConnection && this.mVisibleBound) {
            this.mContext.unbindService(this.mVisibleConnection);
            this.mVisibleBound = false;
        }
        this.mInputShown = false;
        this.mShowRequested = false;
        this.mShowExplicitlyRequested = false;
        this.mShowForced = false;
        SystemProperties.set("sys.grip.status", "on");
        return res;
    }

    public InputBindResult startInputOrWindowGainedFocus(int startInputReason, IInputMethodClient client, IBinder windowToken, int controlFlags, int softInputMode, int windowFlags, EditorInfo attribute, IInputContext inputContext, int missingMethods) {
        if (windowToken != null) {
            return windowGainedFocus(startInputReason, client, windowToken, controlFlags, softInputMode, windowFlags, attribute, inputContext, missingMethods);
        }
        return startInput(startInputReason, client, inputContext, missingMethods, attribute, controlFlags);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.android.internal.view.InputBindResult windowGainedFocus(int r19, com.android.internal.view.IInputMethodClient r20, android.os.IBinder r21, int r22, int r23, int r24, android.view.inputmethod.EditorInfo r25, com.android.internal.view.IInputContext r26, int r27) {
        /*
        r18 = this;
        r9 = r18.calledFromValidUser();
        r16 = 0;
        r14 = android.os.Binder.clearCallingIdentity();
        r0 = r18;
        r0 = r0.mMethodMap;	 Catch:{ all -> 0x0042 }
        r17 = r0;
        monitor-enter(r17);	 Catch:{ all -> 0x0042 }
        r0 = r18;
        r2 = r0.mClients;	 Catch:{ all -> 0x003f }
        r4 = r20.asBinder();	 Catch:{ all -> 0x003f }
        r3 = r2.get(r4);	 Catch:{ all -> 0x003f }
        r3 = (com.android.server.InputMethodManagerService.ClientState) r3;	 Catch:{ all -> 0x003f }
        if (r3 != 0) goto L_0x0047;
    L_0x0021:
        r2 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x003f }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003f }
        r4.<init>();	 Catch:{ all -> 0x003f }
        r5 = "unknown client ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x003f }
        r5 = r20.asBinder();	 Catch:{ all -> 0x003f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x003f }
        r4 = r4.toString();	 Catch:{ all -> 0x003f }
        r2.<init>(r4);	 Catch:{ all -> 0x003f }
        throw r2;	 Catch:{ all -> 0x003f }
    L_0x003f:
        r2 = move-exception;
        monitor-exit(r17);	 Catch:{ all -> 0x0042 }
        throw r2;	 Catch:{ all -> 0x0042 }
    L_0x0042:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r14);
        throw r2;
    L_0x0047:
        r0 = r18;
        r2 = r0.mIWindowManager;	 Catch:{ RemoteException -> 0x0059 }
        r4 = r3.client;	 Catch:{ RemoteException -> 0x0059 }
        r2 = r2.inputMethodClientHasFocus(r4);	 Catch:{ RemoteException -> 0x0059 }
        if (r2 != 0) goto L_0x005a;
    L_0x0053:
        monitor-exit(r17);	 Catch:{ all -> 0x0042 }
        r2 = 0;
        android.os.Binder.restoreCallingIdentity(r14);
        return r2;
    L_0x0059:
        r12 = move-exception;
    L_0x005a:
        if (r9 != 0) goto L_0x007b;
    L_0x005c:
        r2 = "InputMethodManagerService";
        r4 = "A background user is requesting window. Hiding IME.";
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x003f }
        r2 = "InputMethodManagerService";
        r4 = "If you want to interect with IME, you need android.permission.INTERACT_ACROSS_USERS_FULL";
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x003f }
        r2 = 0;
        r4 = 0;
        r0 = r18;
        r0.hideCurrentInputLocked(r2, r4);	 Catch:{ all -> 0x003f }
        monitor-exit(r17);	 Catch:{ all -> 0x0042 }
        r2 = 0;
        android.os.Binder.restoreCallingIdentity(r14);
        return r2;
    L_0x007b:
        r0 = r18;
        r2 = r0.mCurFocusedWindow;	 Catch:{ all -> 0x003f }
        r0 = r21;
        if (r2 != r0) goto L_0x00a0;
    L_0x0083:
        if (r25 == 0) goto L_0x009a;
    L_0x0085:
        r2 = r18;
        r4 = r26;
        r5 = r27;
        r6 = r25;
        r7 = r22;
        r8 = r19;
        r2 = r2.startInputUncheckedLocked(r3, r4, r5, r6, r7, r8);	 Catch:{ all -> 0x003f }
        monitor-exit(r17);	 Catch:{ all -> 0x0042 }
        android.os.Binder.restoreCallingIdentity(r14);
        return r2;
    L_0x009a:
        monitor-exit(r17);	 Catch:{ all -> 0x0042 }
        r2 = 0;
        android.os.Binder.restoreCallingIdentity(r14);
        return r2;
    L_0x00a0:
        r0 = r21;
        r1 = r18;
        r1.mCurFocusedWindow = r0;	 Catch:{ all -> 0x003f }
        r0 = r23;
        r1 = r18;
        r1.mCurFocusedWindowSoftInputMode = r0;	 Catch:{ all -> 0x003f }
        r0 = r18;
        r0.mCurFocusedWindowClient = r3;	 Catch:{ all -> 0x003f }
        r0 = r23;
        r2 = r0 & 240;
        r4 = 16;
        if (r2 == r4) goto L_0x00e9;
    L_0x00b8:
        r0 = r18;
        r2 = r0.mRes;	 Catch:{ all -> 0x003f }
        r2 = r2.getConfiguration();	 Catch:{ all -> 0x003f }
        r4 = 3;
        r11 = r2.isLayoutSizeAtLeast(r4);	 Catch:{ all -> 0x003f }
    L_0x00c5:
        r2 = r22 & 2;
        if (r2 == 0) goto L_0x00eb;
    L_0x00c9:
        r13 = 1;
    L_0x00ca:
        r10 = 0;
        r2 = r23 & 15;
        switch(r2) {
            case 0: goto L_0x00ed;
            case 1: goto L_0x00d0;
            case 2: goto L_0x0126;
            case 3: goto L_0x0134;
            case 4: goto L_0x013c;
            case 5: goto L_0x015e;
            default: goto L_0x00d0;
        };	 Catch:{ all -> 0x003f }
    L_0x00d0:
        if (r10 != 0) goto L_0x00e4;
    L_0x00d2:
        if (r25 == 0) goto L_0x00e4;
    L_0x00d4:
        r2 = r18;
        r4 = r26;
        r5 = r27;
        r6 = r25;
        r7 = r22;
        r8 = r19;
        r16 = r2.startInputUncheckedLocked(r3, r4, r5, r6, r7, r8);	 Catch:{ all -> 0x003f }
    L_0x00e4:
        monitor-exit(r17);	 Catch:{ all -> 0x0042 }
        android.os.Binder.restoreCallingIdentity(r14);
        return r16;
    L_0x00e9:
        r11 = 1;
        goto L_0x00c5;
    L_0x00eb:
        r13 = 0;
        goto L_0x00ca;
    L_0x00ed:
        if (r13 == 0) goto L_0x00f3;
    L_0x00ef:
        r2 = r11 ^ 1;
        if (r2 == 0) goto L_0x0101;
    L_0x00f3:
        r2 = android.view.WindowManager.LayoutParams.mayUseInputMethod(r24);	 Catch:{ all -> 0x003f }
        if (r2 == 0) goto L_0x00d0;
    L_0x00f9:
        r2 = 2;
        r4 = 0;
        r0 = r18;
        r0.hideCurrentInputLocked(r2, r4);	 Catch:{ all -> 0x003f }
        goto L_0x00d0;
    L_0x0101:
        if (r13 == 0) goto L_0x00d0;
    L_0x0103:
        if (r11 == 0) goto L_0x00d0;
    L_0x0105:
        r0 = r23;
        r2 = r0 & 256;
        if (r2 == 0) goto L_0x00d0;
    L_0x010b:
        if (r25 == 0) goto L_0x011e;
    L_0x010d:
        r2 = r18;
        r4 = r26;
        r5 = r27;
        r6 = r25;
        r7 = r22;
        r8 = r19;
        r16 = r2.startInputUncheckedLocked(r3, r4, r5, r6, r7, r8);	 Catch:{ all -> 0x003f }
        r10 = 1;
    L_0x011e:
        r2 = 1;
        r4 = 0;
        r0 = r18;
        r0.showCurrentInputLocked(r2, r4);	 Catch:{ all -> 0x003f }
        goto L_0x00d0;
    L_0x0126:
        r0 = r23;
        r2 = r0 & 256;
        if (r2 == 0) goto L_0x00d0;
    L_0x012c:
        r2 = 0;
        r4 = 0;
        r0 = r18;
        r0.hideCurrentInputLocked(r2, r4);	 Catch:{ all -> 0x003f }
        goto L_0x00d0;
    L_0x0134:
        r2 = 0;
        r4 = 0;
        r0 = r18;
        r0.hideCurrentInputLocked(r2, r4);	 Catch:{ all -> 0x003f }
        goto L_0x00d0;
    L_0x013c:
        r0 = r23;
        r2 = r0 & 256;
        if (r2 == 0) goto L_0x00d0;
    L_0x0142:
        if (r25 == 0) goto L_0x0155;
    L_0x0144:
        r2 = r18;
        r4 = r26;
        r5 = r27;
        r6 = r25;
        r7 = r22;
        r8 = r19;
        r16 = r2.startInputUncheckedLocked(r3, r4, r5, r6, r7, r8);	 Catch:{ all -> 0x003f }
        r10 = 1;
    L_0x0155:
        r2 = 1;
        r4 = 0;
        r0 = r18;
        r0.showCurrentInputLocked(r2, r4);	 Catch:{ all -> 0x003f }
        goto L_0x00d0;
    L_0x015e:
        if (r25 == 0) goto L_0x0171;
    L_0x0160:
        r2 = r18;
        r4 = r26;
        r5 = r27;
        r6 = r25;
        r7 = r22;
        r8 = r19;
        r16 = r2.startInputUncheckedLocked(r3, r4, r5, r6, r7, r8);	 Catch:{ all -> 0x003f }
        r10 = 1;
    L_0x0171:
        r2 = 1;
        r4 = 0;
        r0 = r18;
        r0.showCurrentInputLocked(r2, r4);	 Catch:{ all -> 0x003f }
        goto L_0x00d0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.windowGainedFocus(int, com.android.internal.view.IInputMethodClient, android.os.IBinder, int, int, int, android.view.inputmethod.EditorInfo, com.android.internal.view.IInputContext, int):com.android.internal.view.InputBindResult");
    }

    private boolean canShowInputMethodPickerLocked(IInputMethodClient client) {
        int uid = Binder.getCallingUid();
        if (UserHandle.getAppId(uid) == 1000) {
            return true;
        }
        if (this.mCurClient == null || client == null || this.mCurClient.client.asBinder() != client.asBinder()) {
            return (this.mCurIntent != null && InputMethodUtils.checkIfPackageBelongsToUid(this.mAppOpsManager, uid, this.mCurIntent.getComponent().getPackageName())) || this.mContext.checkCallingPermission("android.permission.WRITE_SECURE_SETTINGS") == 0;
        } else {
            return true;
        }
    }

    public void showInputMethodPickerFromClient(IInputMethodClient client, int auxiliarySubtypeMode) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                if (canShowInputMethodPickerLocked(client)) {
                    this.mHandler.sendMessage(this.mCaller.obtainMessageI(1, auxiliarySubtypeMode));
                    return;
                }
                Slog.w(TAG, "Ignoring showInputMethodPickerFromClient of uid " + Binder.getCallingUid() + ": " + client);
            }
        }
    }

    public void setInputMethod(IBinder token, String id) {
        if (calledFromValidUser()) {
            setInputMethodWithSubtypeId(token, id, -1);
        }
    }

    public void setInputMethodAndSubtype(IBinder token, String id, InputMethodSubtype subtype) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                if (subtype != null) {
                    setInputMethodWithSubtypeIdLocked(token, id, InputMethodUtils.getSubtypeIdFromHashCode((InputMethodInfo) this.mMethodMap.get(id), subtype.hashCode()));
                } else {
                    setInputMethod(token, id);
                }
            }
        }
    }

    public void showInputMethodAndSubtypeEnablerFromClient(IInputMethodClient client, String inputMethodId) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                executeOrSendMessage(this.mCurMethod, this.mCaller.obtainMessageO(2, inputMethodId));
            }
        }
    }

    public boolean switchToLastInputMethod(IBinder token) {
        if (!calledFromValidUser()) {
            return false;
        }
        synchronized (this.mMethodMap) {
            InputMethodInfo inputMethodInfo;
            Pair<String, String> lastIme = this.mSettings.getLastInputMethodAndSubtypeLocked();
            if (lastIme != null) {
                inputMethodInfo = (InputMethodInfo) this.mMethodMap.get(lastIme.first);
            } else {
                inputMethodInfo = null;
            }
            String str = null;
            int subtypeId = -1;
            if (!(lastIme == null || inputMethodInfo == null)) {
                boolean imiIdIsSame = inputMethodInfo.getId().equals(this.mCurMethodId);
                int lastSubtypeHash = Integer.parseInt((String) lastIme.second);
                int currentSubtypeHash;
                if (this.mCurrentSubtype == null) {
                    currentSubtypeHash = -1;
                } else {
                    currentSubtypeHash = this.mCurrentSubtype.hashCode();
                }
                if (!(imiIdIsSame && lastSubtypeHash == currentSubtypeHash)) {
                    str = lastIme.first;
                    subtypeId = InputMethodUtils.getSubtypeIdFromHashCode(inputMethodInfo, lastSubtypeHash);
                }
            }
            if (TextUtils.isEmpty(str) && (InputMethodUtils.canAddToLastInputMethod(this.mCurrentSubtype) ^ 1) != 0) {
                List<InputMethodInfo> enabled = this.mSettings.getEnabledInputMethodListLocked();
                if (enabled != null) {
                    String locale;
                    int N = enabled.size();
                    if (this.mCurrentSubtype == null) {
                        locale = this.mRes.getConfiguration().locale.toString();
                    } else {
                        locale = this.mCurrentSubtype.getLocale();
                    }
                    for (int i = 0; i < N; i++) {
                        InputMethodInfo imi = (InputMethodInfo) enabled.get(i);
                        if (imi.getSubtypeCount() > 0 && InputMethodUtils.isSystemIme(imi)) {
                            InputMethodSubtype keyboardSubtype = InputMethodUtils.findLastResortApplicableSubtypeLocked(this.mRes, InputMethodUtils.getSubtypes(imi), "keyboard", locale, true);
                            if (keyboardSubtype != null) {
                                CharSequence targetLastImiId = imi.getId();
                                subtypeId = InputMethodUtils.getSubtypeIdFromHashCode(imi, keyboardSubtype.hashCode());
                                if (keyboardSubtype.getLocale().equals(locale)) {
                                    break;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
            }
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            setInputMethodWithSubtypeIdLocked(token, str, subtypeId);
            return true;
        }
    }

    public boolean switchToNextInputMethod(IBinder token, boolean onlyCurrentIme) {
        if (!calledFromValidUser()) {
            return false;
        }
        synchronized (this.mMethodMap) {
            if (calledWithValidToken(token)) {
                ImeSubtypeListItem nextSubtype = this.mSwitchingController.getNextInputMethodLocked(onlyCurrentIme, (InputMethodInfo) this.mMethodMap.get(this.mCurMethodId), this.mCurrentSubtype, true);
                if (nextSubtype == null) {
                    return false;
                }
                setInputMethodWithSubtypeIdLocked(token, nextSubtype.mImi.getId(), nextSubtype.mSubtypeId);
                return true;
            }
            return false;
        }
    }

    public boolean shouldOfferSwitchingToNextInputMethod(IBinder token) {
        if (!calledFromValidUser()) {
            return false;
        }
        synchronized (this.mMethodMap) {
            if (!calledWithValidToken(token)) {
                return false;
            } else if (this.mSwitchingController.getNextInputMethodLocked(false, (InputMethodInfo) this.mMethodMap.get(this.mCurMethodId), this.mCurrentSubtype, true) == null) {
                return false;
            } else {
                return true;
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.inputmethod.InputMethodSubtype getLastInputMethodSubtype() {
        /*
        r9 = this;
        r8 = 0;
        r5 = r9.calledFromValidUser();
        if (r5 != 0) goto L_0x0008;
    L_0x0007:
        return r8;
    L_0x0008:
        r6 = r9.mMethodMap;
        monitor-enter(r6);
        r5 = r9.mSettings;	 Catch:{ all -> 0x0056 }
        r1 = r5.getLastInputMethodAndSubtypeLocked();	 Catch:{ all -> 0x0056 }
        if (r1 == 0) goto L_0x0027;
    L_0x0013:
        r5 = r1.first;	 Catch:{ all -> 0x0056 }
        r5 = (java.lang.CharSequence) r5;	 Catch:{ all -> 0x0056 }
        r5 = android.text.TextUtils.isEmpty(r5);	 Catch:{ all -> 0x0056 }
        if (r5 != 0) goto L_0x0027;
    L_0x001d:
        r5 = r1.second;	 Catch:{ all -> 0x0056 }
        r5 = (java.lang.CharSequence) r5;	 Catch:{ all -> 0x0056 }
        r5 = android.text.TextUtils.isEmpty(r5);	 Catch:{ all -> 0x0056 }
        if (r5 == 0) goto L_0x0029;
    L_0x0027:
        monitor-exit(r6);
        return r8;
    L_0x0029:
        r5 = r9.mMethodMap;	 Catch:{ all -> 0x0056 }
        r7 = r1.first;	 Catch:{ all -> 0x0056 }
        r2 = r5.get(r7);	 Catch:{ all -> 0x0056 }
        r2 = (android.view.inputmethod.InputMethodInfo) r2;	 Catch:{ all -> 0x0056 }
        if (r2 != 0) goto L_0x0037;
    L_0x0035:
        monitor-exit(r6);
        return r8;
    L_0x0037:
        r5 = r1.second;	 Catch:{ NumberFormatException -> 0x0053 }
        r5 = (java.lang.String) r5;	 Catch:{ NumberFormatException -> 0x0053 }
        r3 = java.lang.Integer.parseInt(r5);	 Catch:{ NumberFormatException -> 0x0053 }
        r4 = com.android.internal.inputmethod.InputMethodUtils.getSubtypeIdFromHashCode(r2, r3);	 Catch:{ NumberFormatException -> 0x0053 }
        if (r4 < 0) goto L_0x004b;
    L_0x0045:
        r5 = r2.getSubtypeCount();	 Catch:{ NumberFormatException -> 0x0053 }
        if (r4 < r5) goto L_0x004d;
    L_0x004b:
        monitor-exit(r6);
        return r8;
    L_0x004d:
        r5 = r2.getSubtypeAt(r4);	 Catch:{ NumberFormatException -> 0x0053 }
        monitor-exit(r6);
        return r5;
    L_0x0053:
        r0 = move-exception;
        monitor-exit(r6);
        return r8;
    L_0x0056:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.getLastInputMethodSubtype():android.view.inputmethod.InputMethodSubtype");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setAdditionalInputMethodSubtypes(java.lang.String r11, android.view.inputmethod.InputMethodSubtype[] r12) {
        /*
        r10 = this;
        r7 = r10.calledFromValidUser();
        if (r7 != 0) goto L_0x0007;
    L_0x0006:
        return;
    L_0x0007:
        r7 = android.text.TextUtils.isEmpty(r11);
        if (r7 != 0) goto L_0x000f;
    L_0x000d:
        if (r12 != 0) goto L_0x0010;
    L_0x000f:
        return;
    L_0x0010:
        r8 = r10.mMethodMap;
        monitor-enter(r8);
        r7 = r10.mSystemReady;	 Catch:{ all -> 0x0064 }
        if (r7 != 0) goto L_0x0019;
    L_0x0017:
        monitor-exit(r8);
        return;
    L_0x0019:
        r7 = r10.mMethodMap;	 Catch:{ all -> 0x0064 }
        r4 = r7.get(r11);	 Catch:{ all -> 0x0064 }
        r4 = (android.view.inputmethod.InputMethodInfo) r4;	 Catch:{ all -> 0x0064 }
        if (r4 != 0) goto L_0x0025;
    L_0x0023:
        monitor-exit(r8);
        return;
    L_0x0025:
        r7 = r10.mIPackageManager;	 Catch:{ RemoteException -> 0x0053 }
        r9 = android.os.Binder.getCallingUid();	 Catch:{ RemoteException -> 0x0053 }
        r5 = r7.getPackagesForUid(r9);	 Catch:{ RemoteException -> 0x0053 }
        if (r5 == 0) goto L_0x006a;
    L_0x0031:
        r6 = r5.length;	 Catch:{ all -> 0x0064 }
        r1 = 0;
    L_0x0033:
        if (r1 >= r6) goto L_0x006a;
    L_0x0035:
        r7 = r5[r1];	 Catch:{ all -> 0x0064 }
        r9 = r4.getPackageName();	 Catch:{ all -> 0x0064 }
        r7 = r7.equals(r9);	 Catch:{ all -> 0x0064 }
        if (r7 == 0) goto L_0x0067;
    L_0x0041:
        r7 = r10.mFileManager;	 Catch:{ all -> 0x0064 }
        r7.addInputMethodSubtypes(r4, r12);	 Catch:{ all -> 0x0064 }
        r2 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x0064 }
        r7 = 0;
        r10.buildInputMethodListLocked(r7);	 Catch:{ all -> 0x005f }
        android.os.Binder.restoreCallingIdentity(r2);	 Catch:{ all -> 0x0064 }
        monitor-exit(r8);
        return;
    L_0x0053:
        r0 = move-exception;
        r7 = "InputMethodManagerService";
        r9 = "Failed to get package infos";
        android.util.Slog.e(r7, r9);	 Catch:{ all -> 0x0064 }
        monitor-exit(r8);
        return;
    L_0x005f:
        r7 = move-exception;
        android.os.Binder.restoreCallingIdentity(r2);	 Catch:{ all -> 0x0064 }
        throw r7;	 Catch:{ all -> 0x0064 }
    L_0x0064:
        r7 = move-exception;
        monitor-exit(r8);
        throw r7;
    L_0x0067:
        r1 = r1 + 1;
        goto L_0x0033;
    L_0x006a:
        monitor-exit(r8);
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.setAdditionalInputMethodSubtypes(java.lang.String, android.view.inputmethod.InputMethodSubtype[]):void");
    }

    public int getInputMethodWindowVisibleHeight() {
        return this.mWindowManagerInternal.getInputMethodWindowVisibleHeight();
    }

    public void clearLastInputMethodWindowForTransition(IBinder token) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                if (calledWithValidToken(token)) {
                    this.mWindowManagerInternal.clearLastInputMethodWindowForTransition();
                    return;
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void notifyUserAction(int r5) {
        /*
        r4 = this;
        r2 = r4.mMethodMap;
        monitor-enter(r2);
        r1 = r4.mCurUserActionNotificationSequenceNumber;	 Catch:{ all -> 0x001e }
        if (r1 == r5) goto L_0x0009;
    L_0x0007:
        monitor-exit(r2);
        return;
    L_0x0009:
        r1 = r4.mMethodMap;	 Catch:{ all -> 0x001e }
        r3 = r4.mCurMethodId;	 Catch:{ all -> 0x001e }
        r0 = r1.get(r3);	 Catch:{ all -> 0x001e }
        r0 = (android.view.inputmethod.InputMethodInfo) r0;	 Catch:{ all -> 0x001e }
        if (r0 == 0) goto L_0x001c;
    L_0x0015:
        r1 = r4.mSwitchingController;	 Catch:{ all -> 0x001e }
        r3 = r4.mCurrentSubtype;	 Catch:{ all -> 0x001e }
        r1.onUserActionLocked(r0, r3);	 Catch:{ all -> 0x001e }
    L_0x001c:
        monitor-exit(r2);
        return;
    L_0x001e:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.notifyUserAction(int):void");
    }

    private void setInputMethodWithSubtypeId(IBinder token, String id, int subtypeId) {
        synchronized (this.mMethodMap) {
            setInputMethodWithSubtypeIdLocked(token, id, subtypeId);
        }
    }

    private void setInputMethodWithSubtypeIdLocked(IBinder token, String id, int subtypeId) {
        if (token == null) {
            if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0) {
                throw new SecurityException("Using null token requires permission android.permission.WRITE_SECURE_SETTINGS");
            }
        } else if (this.mCurToken != token) {
            Slog.w(TAG, "Ignoring setInputMethod of uid " + Binder.getCallingUid() + " token: " + token);
            return;
        }
        long ident = Binder.clearCallingIdentity();
        try {
            setInputMethodLocked(id, subtypeId);
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    public void hideMySoftInput(IBinder token, int flags) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                if (calledWithValidToken(token)) {
                    long ident = Binder.clearCallingIdentity();
                    try {
                        hideCurrentInputLocked(flags, null);
                        Binder.restoreCallingIdentity(ident);
                    } catch (Throwable th) {
                        Binder.restoreCallingIdentity(ident);
                    }
                }
            }
        }
    }

    public void showMySoftInput(IBinder token, int flags) {
        if (calledFromValidUser()) {
            synchronized (this.mMethodMap) {
                if (calledWithValidToken(token)) {
                    long ident = Binder.clearCallingIdentity();
                    try {
                        showCurrentInputLocked(flags, null);
                        Binder.restoreCallingIdentity(ident);
                    } catch (Throwable th) {
                        Binder.restoreCallingIdentity(ident);
                    }
                }
            }
        }
    }

    void setEnabledSessionInMainThread(SessionState session) {
        if (this.mEnabledSession != session) {
            if (!(this.mEnabledSession == null || this.mEnabledSession.session == null)) {
                try {
                    this.mEnabledSession.method.setSessionEnabled(this.mEnabledSession.session, false);
                } catch (RemoteException e) {
                }
            }
            this.mEnabledSession = session;
            if (this.mEnabledSession != null && this.mEnabledSession.session != null) {
                try {
                    this.mEnabledSession.method.setSessionEnabled(this.mEnabledSession.session, true);
                } catch (RemoteException e2) {
                }
            }
        }
    }

    public boolean handleMessage(Message msg) {
        SomeArgs args;
        ClientState clientState;
        switch (msg.what) {
            case 1:
                boolean z;
                switch (msg.arg1) {
                    case 0:
                        z = this.mInputShown;
                        break;
                    case 1:
                        z = true;
                        break;
                    case 2:
                        z = false;
                        break;
                    default:
                        Slog.e(TAG, "Unknown subtype picker mode = " + msg.arg1);
                        return false;
                }
                showInputMethodMenu(z);
                return true;
            case 2:
                showInputMethodAndSubtypeEnabler((String) msg.obj);
                return true;
            case 3:
                showConfigureInputMethods();
                return true;
            case 1000:
                try {
                    ((IInputMethod) msg.obj).unbindInput();
                } catch (RemoteException e) {
                }
                return true;
            case MSG_BIND_INPUT /*1010*/:
                args = msg.obj;
                try {
                    ((IInputMethod) args.arg1).bindInput((InputBinding) args.arg2);
                } catch (RemoteException e2) {
                }
                args.recycle();
                return true;
            case MSG_SHOW_SOFT_INPUT /*1020*/:
                args = (SomeArgs) msg.obj;
                try {
                    ((IInputMethod) args.arg1).showSoftInput(msg.arg1, (ResultReceiver) args.arg2);
                } catch (RemoteException e3) {
                }
                args.recycle();
                return true;
            case MSG_HIDE_SOFT_INPUT /*1030*/:
                args = (SomeArgs) msg.obj;
                try {
                    ((IInputMethod) args.arg1).hideSoftInput(0, (ResultReceiver) args.arg2);
                } catch (RemoteException e4) {
                }
                args.recycle();
                return true;
            case MSG_HIDE_CURRENT_INPUT_METHOD /*1035*/:
                synchronized (this.mMethodMap) {
                    hideCurrentInputLocked(0, null);
                }
                return true;
            case MSG_ATTACH_TOKEN /*1040*/:
                args = (SomeArgs) msg.obj;
                try {
                    ((IInputMethod) args.arg1).attachToken((IBinder) args.arg2);
                } catch (RemoteException e5) {
                }
                args.recycle();
                return true;
            case MSG_CREATE_SESSION /*1050*/:
                args = (SomeArgs) msg.obj;
                IInputMethod method = args.arg1;
                InputChannel channel = args.arg2;
                try {
                    method.createSession(channel, (IInputSessionCallback) args.arg3);
                    if (channel != null && Binder.isProxy(method)) {
                        channel.dispose();
                    }
                } catch (RemoteException e6) {
                    if (channel != null && Binder.isProxy(method)) {
                        channel.dispose();
                    }
                } catch (Throwable th) {
                    if (channel != null && Binder.isProxy(method)) {
                        channel.dispose();
                    }
                }
                args.recycle();
                return true;
            case MSG_START_INPUT /*2000*/:
                int missingMethods = msg.arg1;
                boolean restarting = msg.arg2 != 0;
                args = (SomeArgs) msg.obj;
                IBinder startInputToken = args.arg1;
                SessionState session = args.arg2;
                IInputContext inputContext = args.arg3;
                EditorInfo editorInfo = args.arg4;
                try {
                    setEnabledSessionInMainThread(session);
                    session.method.startInput(startInputToken, inputContext, missingMethods, editorInfo, restarting);
                } catch (RemoteException e7) {
                }
                args.recycle();
                return true;
            case MSG_UNBIND_CLIENT /*3000*/:
                try {
                    ((IInputMethodClient) msg.obj).onUnbindMethod(msg.arg1, msg.arg2);
                } catch (RemoteException e8) {
                }
                return true;
            case 3010:
                args = (SomeArgs) msg.obj;
                IInputMethodClient client = args.arg1;
                InputBindResult res = args.arg2;
                try {
                    client.onBindMethod(res);
                    if (res.channel != null && Binder.isProxy(client)) {
                        res.channel.dispose();
                    }
                } catch (RemoteException e9) {
                    Slog.w(TAG, "Client died receiving input method " + args.arg2);
                    if (res.channel != null && Binder.isProxy(client)) {
                        res.channel.dispose();
                    }
                } catch (Throwable th2) {
                    if (res.channel != null && Binder.isProxy(client)) {
                        res.channel.dispose();
                    }
                }
                args.recycle();
                return true;
            case MSG_SET_ACTIVE /*3020*/:
                try {
                    ((ClientState) msg.obj).client.setActive(msg.arg1 != 0, msg.arg2 != 0);
                } catch (RemoteException e10) {
                    Slog.w(TAG, "Got RemoteException sending setActive(false) notification to pid " + ((ClientState) msg.obj).pid + " uid " + ((ClientState) msg.obj).uid);
                }
                return true;
            case MSG_SET_INTERACTIVE /*3030*/:
                handleSetInteractive(msg.arg1 != 0);
                return true;
            case 3040:
                int sequenceNumber = msg.arg1;
                clientState = msg.obj;
                try {
                    clientState.client.setUserActionNotificationSequenceNumber(sequenceNumber);
                } catch (RemoteException e11) {
                    Slog.w(TAG, "Got RemoteException sending setUserActionNotificationSequenceNumber(" + sequenceNumber + ") notification to pid " + clientState.pid + " uid " + clientState.uid);
                }
                return true;
            case MSG_REPORT_FULLSCREEN_MODE /*3045*/:
                boolean fullscreen = msg.arg1 != 0;
                clientState = (ClientState) msg.obj;
                try {
                    clientState.client.reportFullscreenMode(fullscreen);
                } catch (RemoteException e12) {
                    Slog.w(TAG, "Got RemoteException sending reportFullscreen(" + fullscreen + ") notification to pid=" + clientState.pid + " uid=" + clientState.uid);
                }
                return true;
            case 3050:
                handleSwitchInputMethod(msg.arg1 != 0);
                return true;
            case MSG_HARD_KEYBOARD_SWITCH_CHANGED /*4000*/:
                this.mHardKeyboardListener.handleHardKeyboardStatusChange(msg.arg1 == 1);
                return true;
            case MSG_SYSTEM_UNLOCK_USER /*5000*/:
                onUnlockUser(msg.arg1);
                return true;
            default:
                return false;
        }
    }

    private void handleSetInteractive(boolean interactive) {
        int i = 1;
        synchronized (this.mMethodMap) {
            this.mIsInteractive = interactive;
            updateSystemUiLocked(this.mCurToken, interactive ? this.mImeWindowVis : 0, this.mBackDisposition);
            if (!(this.mCurClient == null || this.mCurClient.client == null)) {
                int i2;
                IInterface iInterface = this.mCurClient.client;
                HandlerCaller handlerCaller = this.mCaller;
                if (this.mIsInteractive) {
                    i2 = 1;
                } else {
                    i2 = 0;
                }
                if (!this.mInFullscreenMode) {
                    i = 0;
                }
                executeOrSendMessage(iInterface, handlerCaller.obtainMessageIIO(MSG_SET_ACTIVE, i2, i, this.mCurClient));
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handleSwitchInputMethod(boolean r9) {
        /*
        r8 = this;
        r4 = r8.mMethodMap;
        monitor-enter(r4);
        r5 = r8.mSwitchingController;	 Catch:{ all -> 0x005b }
        r3 = r8.mMethodMap;	 Catch:{ all -> 0x005b }
        r6 = r8.mCurMethodId;	 Catch:{ all -> 0x005b }
        r3 = r3.get(r6);	 Catch:{ all -> 0x005b }
        r3 = (android.view.inputmethod.InputMethodInfo) r3;	 Catch:{ all -> 0x005b }
        r6 = r8.mCurrentSubtype;	 Catch:{ all -> 0x005b }
        r7 = 0;
        r1 = r5.getNextInputMethodLocked(r7, r3, r6, r9);	 Catch:{ all -> 0x005b }
        if (r1 != 0) goto L_0x001a;
    L_0x0018:
        monitor-exit(r4);
        return;
    L_0x001a:
        r3 = r1.mImi;	 Catch:{ all -> 0x005b }
        r3 = r3.getId();	 Catch:{ all -> 0x005b }
        r5 = r1.mSubtypeId;	 Catch:{ all -> 0x005b }
        r8.setInputMethodLocked(r3, r5);	 Catch:{ all -> 0x005b }
        r3 = r8.mMethodMap;	 Catch:{ all -> 0x005b }
        r5 = r8.mCurMethodId;	 Catch:{ all -> 0x005b }
        r0 = r3.get(r5);	 Catch:{ all -> 0x005b }
        r0 = (android.view.inputmethod.InputMethodInfo) r0;	 Catch:{ all -> 0x005b }
        if (r0 != 0) goto L_0x0033;
    L_0x0031:
        monitor-exit(r4);
        return;
    L_0x0033:
        r3 = r8.mContext;	 Catch:{ all -> 0x005b }
        r5 = r8.mCurrentSubtype;	 Catch:{ all -> 0x005b }
        r2 = com.android.internal.inputmethod.InputMethodUtils.getImeAndSubtypeDisplayName(r3, r0, r5);	 Catch:{ all -> 0x005b }
        r3 = android.text.TextUtils.isEmpty(r2);	 Catch:{ all -> 0x005b }
        if (r3 != 0) goto L_0x0053;
    L_0x0041:
        r3 = r8.mSubtypeSwitchedByShortCutToast;	 Catch:{ all -> 0x005b }
        if (r3 != 0) goto L_0x0055;
    L_0x0045:
        r3 = r8.mContext;	 Catch:{ all -> 0x005b }
        r5 = 0;
        r3 = android.widget.Toast.makeText(r3, r2, r5);	 Catch:{ all -> 0x005b }
        r8.mSubtypeSwitchedByShortCutToast = r3;	 Catch:{ all -> 0x005b }
    L_0x004e:
        r3 = r8.mSubtypeSwitchedByShortCutToast;	 Catch:{ all -> 0x005b }
        r3.show();	 Catch:{ all -> 0x005b }
    L_0x0053:
        monitor-exit(r4);
        return;
    L_0x0055:
        r3 = r8.mSubtypeSwitchedByShortCutToast;	 Catch:{ all -> 0x005b }
        r3.setText(r2);	 Catch:{ all -> 0x005b }
        goto L_0x004e;
    L_0x005b:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.handleSwitchInputMethod(boolean):void");
    }

    private boolean chooseNewDefaultIMELocked() {
        InputMethodInfo imi = InputMethodUtils.getMostApplicableDefaultIME(this.mSettings.getEnabledInputMethodListLocked());
        if (imi == null) {
            return false;
        }
        resetSelectedInputMethodAndSubtypeLocked(imi.getId());
        return true;
    }

    void buildInputMethodListLocked(boolean resetDefaultEnabledIme) {
        if (this.mSystemReady) {
            int i;
            ServiceInfo si;
            this.mMethodList.clear();
            this.mMethodMap.clear();
            this.mMethodMapUpdateCount++;
            this.mMyPackageMonitor.clearKnownImePackageNamesLocked();
            PackageManager pm = this.mContext.getPackageManager();
            List<ResolveInfo> services = pm.queryIntentServicesAsUser(new Intent("android.view.InputMethod"), 32896, this.mSettings.getCurrentUserId());
            HashMap<String, List<InputMethodSubtype>> additionalSubtypeMap = this.mFileManager.getAllAdditionalInputMethodSubtypes();
            for (i = 0; i < services.size(); i++) {
                ResolveInfo ri = (ResolveInfo) services.get(i);
                si = ri.serviceInfo;
                String imeId = InputMethodInfo.computeId(ri);
                if ("android.permission.BIND_INPUT_METHOD".equals(si.permission)) {
                    try {
                        InputMethodInfo inputMethodInfo = new InputMethodInfo(this.mContext, ri, (List) additionalSubtypeMap.get(imeId));
                        this.mMethodList.add(inputMethodInfo);
                        this.mMethodMap.put(inputMethodInfo.getId(), inputMethodInfo);
                    } catch (Exception e) {
                        Slog.wtf(TAG, "Unable to load input method " + imeId, e);
                    }
                } else {
                    Slog.w(TAG, "Skipping input method " + imeId + ": it does not require the permission " + "android.permission.BIND_INPUT_METHOD");
                }
            }
            List<ResolveInfo> allInputMethodServices = pm.queryIntentServicesAsUser(new Intent("android.view.InputMethod"), 512, this.mSettings.getCurrentUserId());
            int N = allInputMethodServices.size();
            for (i = 0; i < N; i++) {
                si = ((ResolveInfo) allInputMethodServices.get(i)).serviceInfo;
                if ("android.permission.BIND_INPUT_METHOD".equals(si.permission)) {
                    this.mMyPackageMonitor.addKnownImePackageNameLocked(si.packageName);
                }
            }
            if (!resetDefaultEnabledIme) {
                boolean enabledImeFound = false;
                List<InputMethodInfo> enabledImes = this.mSettings.getEnabledInputMethodListLocked();
                N = enabledImes.size();
                for (i = 0; i < N; i++) {
                    if (this.mMethodList.contains((InputMethodInfo) enabledImes.get(i))) {
                        enabledImeFound = true;
                        break;
                    }
                }
                if (!enabledImeFound) {
                    resetDefaultEnabledIme = true;
                    resetSelectedInputMethodAndSubtypeLocked("");
                }
            }
            if (resetDefaultEnabledIme) {
                ArrayList<InputMethodInfo> defaultEnabledIme = InputMethodUtils.getDefaultEnabledImes(this.mContext, this.mMethodList);
                N = defaultEnabledIme.size();
                for (i = 0; i < N; i++) {
                    setInputMethodEnabledLocked(((InputMethodInfo) defaultEnabledIme.get(i)).getId(), true);
                }
            }
            String defaultImiId = this.mSettings.getSelectedInputMethod();
            if (!TextUtils.isEmpty(defaultImiId)) {
                if (this.mMethodMap.containsKey(defaultImiId)) {
                    setInputMethodEnabledLocked(defaultImiId, true);
                } else {
                    Slog.w(TAG, "Default IME is uninstalled. Choose new default IME.");
                    if (chooseNewDefaultIMELocked()) {
                        updateInputMethodsFromSettingsLocked(true);
                    }
                }
            }
            this.mSwitchingController.resetCircularListLocked(this.mContext);
            return;
        }
        Slog.e(TAG, "buildInputMethodListLocked is not allowed until system is ready");
    }

    private void showInputMethodAndSubtypeEnabler(String inputMethodId) {
        int userId;
        Intent intent = new Intent("android.settings.INPUT_METHOD_SUBTYPE_SETTINGS");
        intent.setFlags(337641472);
        if (!TextUtils.isEmpty(inputMethodId)) {
            intent.putExtra("input_method_id", inputMethodId);
        }
        synchronized (this.mMethodMap) {
            userId = this.mSettings.getCurrentUserId();
        }
        this.mContext.startActivityAsUser(intent, null, UserHandle.of(userId));
    }

    private void showConfigureInputMethods() {
        Intent intent = new Intent("android.settings.INPUT_METHOD_SETTINGS");
        intent.setFlags(337641472);
        this.mContext.startActivityAsUser(intent, null, UserHandle.CURRENT);
    }

    private boolean isScreenLocked() {
        if (this.mKeyguardManager == null || !this.mKeyguardManager.isKeyguardLocked()) {
            return false;
        }
        return this.mKeyguardManager.isKeyguardSecure();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void showInputMethodMenu(boolean r33) {
        /*
        r32 = this;
        r19 = r32.isScreenLocked();
        r0 = r32;
        r0 = r0.mSettings;
        r27 = r0;
        r21 = r27.getSelectedInputMethod();
        r0 = r32;
        r0 = r0.mSettings;
        r27 = r0;
        r0 = r27;
        r1 = r21;
        r22 = r0.getSelectedInputMethodSubtypeId(r1);
        r0 = r32;
        r0 = r0.mMethodMap;
        r28 = r0;
        monitor-enter(r28);
        r0 = r32;
        r0 = r0.mSettings;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r32;
        r0 = r0.mContext;	 Catch:{ all -> 0x0288 }
        r29 = r0;
        r0 = r27;
        r1 = r29;
        r17 = r0.getExplicitlyOrImplicitlyEnabledInputMethodsAndSubtypeListLocked(r1);	 Catch:{ all -> 0x0288 }
        if (r17 == 0) goto L_0x003f;
    L_0x0039:
        r27 = r17.size();	 Catch:{ all -> 0x0288 }
        if (r27 != 0) goto L_0x0041;
    L_0x003f:
        monitor-exit(r28);
        return;
    L_0x0041:
        r32.hideInputMethodMenuLocked();	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mSwitchingController;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r27;
        r1 = r33;
        r2 = r19;
        r16 = r0.getSortedInputMethodAndSubtypeListLocked(r1, r2);	 Catch:{ all -> 0x0288 }
        r27 = -1;
        r0 = r22;
        r1 = r27;
        if (r0 != r1) goto L_0x0082;
    L_0x005c:
        r11 = r32.getCurrentInputMethodSubtypeLocked();	 Catch:{ all -> 0x0288 }
        if (r11 == 0) goto L_0x0082;
    L_0x0062:
        r0 = r32;
        r0 = r0.mMethodMap;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r32;
        r0 = r0.mCurMethodId;	 Catch:{ all -> 0x0288 }
        r29 = r0;
        r0 = r27;
        r1 = r29;
        r10 = r0.get(r1);	 Catch:{ all -> 0x0288 }
        r10 = (android.view.inputmethod.InputMethodInfo) r10;	 Catch:{ all -> 0x0288 }
        r27 = r11.hashCode();	 Catch:{ all -> 0x0288 }
        r0 = r27;
        r22 = com.android.internal.inputmethod.InputMethodUtils.getSubtypeIdFromHashCode(r10, r0);	 Catch:{ all -> 0x0288 }
    L_0x0082:
        r4 = r16.size();	 Catch:{ all -> 0x0288 }
        r0 = new android.view.inputmethod.InputMethodInfo[r4];	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r27;
        r1 = r32;
        r1.mIms = r0;	 Catch:{ all -> 0x0288 }
        r0 = new int[r4];	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r27;
        r1 = r32;
        r1.mSubtypeIds = r0;	 Catch:{ all -> 0x0288 }
        r8 = 0;
        r15 = 0;
    L_0x009c:
        if (r15 >= r4) goto L_0x00fd;
    L_0x009e:
        r0 = r16;
        r20 = r0.get(r15);	 Catch:{ all -> 0x0288 }
        r20 = (com.android.internal.inputmethod.InputMethodSubtypeSwitchingController.ImeSubtypeListItem) r20;	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mIms;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r20;
        r0 = r0.mImi;	 Catch:{ all -> 0x0288 }
        r29 = r0;
        r27[r15] = r29;	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mSubtypeIds;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r20;
        r0 = r0.mSubtypeId;	 Catch:{ all -> 0x0288 }
        r29 = r0;
        r27[r15] = r29;	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mIms;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r27 = r27[r15];	 Catch:{ all -> 0x0288 }
        r27 = r27.getId();	 Catch:{ all -> 0x0288 }
        r0 = r27;
        r1 = r21;
        r27 = r0.equals(r1);	 Catch:{ all -> 0x0288 }
        if (r27 == 0) goto L_0x00f3;
    L_0x00d8:
        r0 = r32;
        r0 = r0.mSubtypeIds;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r24 = r27[r15];	 Catch:{ all -> 0x0288 }
        r27 = -1;
        r0 = r24;
        r1 = r27;
        if (r0 == r1) goto L_0x00f2;
    L_0x00e8:
        r27 = -1;
        r0 = r22;
        r1 = r27;
        if (r0 != r1) goto L_0x00f6;
    L_0x00f0:
        if (r24 != 0) goto L_0x00f6;
    L_0x00f2:
        r8 = r15;
    L_0x00f3:
        r15 = r15 + 1;
        goto L_0x009c;
    L_0x00f6:
        r0 = r24;
        r1 = r22;
        if (r0 != r1) goto L_0x00f3;
    L_0x00fc:
        goto L_0x00f2;
    L_0x00fd:
        r23 = new android.view.ContextThemeWrapper;	 Catch:{ all -> 0x0288 }
        r27 = android.app.ActivityThread.currentActivityThread();	 Catch:{ all -> 0x0288 }
        r27 = r27.getSystemUiContext();	 Catch:{ all -> 0x0288 }
        r29 = 16974371; // 0x1030223 float:2.4062433E-38 double:8.3864536E-317;
        r0 = r23;
        r1 = r27;
        r2 = r29;
        r0.<init>(r1, r2);	 Catch:{ all -> 0x0288 }
        r27 = new android.app.AlertDialog$Builder;	 Catch:{ all -> 0x0288 }
        r0 = r27;
        r1 = r23;
        r0.<init>(r1);	 Catch:{ all -> 0x0288 }
        r0 = r27;
        r1 = r32;
        r1.mDialogBuilder = r0;	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mDialogBuilder;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r29 = new com.android.server.InputMethodManagerService$3;	 Catch:{ all -> 0x0288 }
        r0 = r29;
        r1 = r32;
        r0.<init>();	 Catch:{ all -> 0x0288 }
        r0 = r27;
        r1 = r29;
        r0.setOnCancelListener(r1);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mDialogBuilder;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r12 = r27.getContext();	 Catch:{ all -> 0x0288 }
        r27 = com.android.internal.R.styleable.DialogPreference;	 Catch:{ all -> 0x0288 }
        r29 = 0;
        r30 = 16842845; // 0x101005d float:2.369382E-38 double:8.321471E-317;
        r31 = 0;
        r0 = r29;
        r1 = r27;
        r2 = r30;
        r3 = r31;
        r5 = r12.obtainStyledAttributes(r0, r1, r2, r3);	 Catch:{ all -> 0x0288 }
        r27 = 2;
        r0 = r27;
        r13 = r5.getDrawable(r0);	 Catch:{ all -> 0x0288 }
        r5.recycle();	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mDialogBuilder;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r27;
        r0.setIcon(r13);	 Catch:{ all -> 0x0288 }
        r27 = android.view.LayoutInflater.class;
        r0 = r27;
        r18 = r12.getSystemService(r0);	 Catch:{ all -> 0x0288 }
        r18 = (android.view.LayoutInflater) r18;	 Catch:{ all -> 0x0288 }
        r27 = 17367151; // 0x109006f float:2.5163237E-38 double:8.5805127E-317;
        r29 = 0;
        r0 = r18;
        r1 = r27;
        r2 = r29;
        r25 = r0.inflate(r1, r2);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mDialogBuilder;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r27;
        r1 = r25;
        r0.setCustomTitle(r1);	 Catch:{ all -> 0x0288 }
        r0 = r25;
        r1 = r32;
        r1.mSwitchingDialogTitleView = r0;	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mSwitchingDialogTitleView;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r29 = 16908919; // 0x1020277 float:2.3878997E-38 double:8.354116E-317;
        r0 = r27;
        r1 = r29;
        r29 = r0.findViewById(r1);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mWindowManagerInternal;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r27 = r27.isHardKeyboardAvailable();	 Catch:{ all -> 0x0288 }
        if (r27 == 0) goto L_0x0284;
    L_0x01b6:
        r27 = 0;
    L_0x01b8:
        r0 = r29;
        r1 = r27;
        r0.setVisibility(r1);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mSwitchingDialogTitleView;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r29 = 16908920; // 0x1020278 float:2.3879E-38 double:8.3541165E-317;
        r0 = r27;
        r1 = r29;
        r14 = r0.findViewById(r1);	 Catch:{ all -> 0x0288 }
        r14 = (android.widget.Switch) r14;	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mShowImeWithHardKeyboard;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r27;
        r14.setChecked(r0);	 Catch:{ all -> 0x0288 }
        r27 = new com.android.server.InputMethodManagerService$4;	 Catch:{ all -> 0x0288 }
        r0 = r27;
        r1 = r32;
        r0.<init>();	 Catch:{ all -> 0x0288 }
        r0 = r27;
        r14.setOnCheckedChangeListener(r0);	 Catch:{ all -> 0x0288 }
        r6 = new com.android.server.InputMethodManagerService$ImeSubtypeListAdapter;	 Catch:{ all -> 0x0288 }
        r27 = 17367152; // 0x1090070 float:2.516324E-38 double:8.580513E-317;
        r0 = r27;
        r1 = r16;
        r6.<init>(r12, r0, r1, r8);	 Catch:{ all -> 0x0288 }
        r9 = new com.android.server.InputMethodManagerService$5;	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r9.<init>(r6);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mDialogBuilder;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r27;
        r0.setSingleChoiceItems(r6, r8, r9);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mDialogBuilder;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r27 = r27.create();	 Catch:{ all -> 0x0288 }
        r0 = r27;
        r1 = r32;
        r1.mSwitchingDialog = r0;	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mSwitchingDialog;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r29 = 1;
        r0 = r27;
        r1 = r29;
        r0.setCanceledOnTouchOutside(r1);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mSwitchingDialog;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r26 = r27.getWindow();	 Catch:{ all -> 0x0288 }
        r7 = r26.getAttributes();	 Catch:{ all -> 0x0288 }
        r27 = 2012; // 0x7dc float:2.82E-42 double:9.94E-321;
        r26.setType(r27);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mSwitchingDialogToken;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r27;
        r7.token = r0;	 Catch:{ all -> 0x0288 }
        r0 = r7.privateFlags;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r27 = r27 | 16;
        r0 = r27;
        r7.privateFlags = r0;	 Catch:{ all -> 0x0288 }
        r27 = "Select input method";
        r0 = r27;
        r7.setTitle(r0);	 Catch:{ all -> 0x0288 }
        r0 = r26;
        r0.setAttributes(r7);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mCurToken;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r0 = r32;
        r0 = r0.mImeWindowVis;	 Catch:{ all -> 0x0288 }
        r29 = r0;
        r0 = r32;
        r0 = r0.mBackDisposition;	 Catch:{ all -> 0x0288 }
        r30 = r0;
        r0 = r32;
        r1 = r27;
        r2 = r29;
        r3 = r30;
        r0.updateSystemUi(r1, r2, r3);	 Catch:{ all -> 0x0288 }
        r0 = r32;
        r0 = r0.mSwitchingDialog;	 Catch:{ all -> 0x0288 }
        r27 = r0;
        r27.show();	 Catch:{ all -> 0x0288 }
        monitor-exit(r28);
        return;
    L_0x0284:
        r27 = 8;
        goto L_0x01b8;
    L_0x0288:
        r27 = move-exception;
        monitor-exit(r28);
        throw r27;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.showInputMethodMenu(boolean):void");
    }

    void hideInputMethodMenu() {
        synchronized (this.mMethodMap) {
            hideInputMethodMenuLocked();
        }
    }

    void hideInputMethodMenuLocked() {
        if (this.mSwitchingDialog != null) {
            this.mSwitchingDialog.dismiss();
            this.mSwitchingDialog = null;
        }
        updateSystemUiLocked(this.mCurToken, this.mImeWindowVis, this.mBackDisposition);
        this.mDialogBuilder = null;
        this.mIms = null;
    }

    public boolean setInputMethodEnabled(String id, boolean enabled) {
        if (!calledFromValidUser()) {
            return false;
        }
        boolean inputMethodEnabledLocked;
        synchronized (this.mMethodMap) {
            if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0) {
                throw new SecurityException("Requires permission android.permission.WRITE_SECURE_SETTINGS");
            }
            long ident = Binder.clearCallingIdentity();
            try {
                inputMethodEnabledLocked = setInputMethodEnabledLocked(id, enabled);
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }
        return inputMethodEnabledLocked;
    }

    boolean setInputMethodEnabledLocked(String id, boolean enabled) {
        if (((InputMethodInfo) this.mMethodMap.get(id)) == null) {
            throw new IllegalArgumentException("Unknown id: " + this.mCurMethodId);
        }
        List<Pair<String, ArrayList<String>>> enabledInputMethodsList = this.mSettings.getEnabledInputMethodsAndSubtypeListLocked();
        if (enabled) {
            for (Pair<String, ArrayList<String>> pair : enabledInputMethodsList) {
                if (((String) pair.first).equals(id)) {
                    return true;
                }
            }
            this.mSettings.appendAndPutEnabledInputMethodLocked(id, false);
            return false;
        }
        if (!this.mSettings.buildAndPutEnabledInputMethodsStrRemovingIdLocked(new StringBuilder(), enabledInputMethodsList, id)) {
            return false;
        }
        if (id.equals(this.mSettings.getSelectedInputMethod()) && (chooseNewDefaultIMELocked() ^ 1) != 0) {
            Slog.i(TAG, "Can't find new IME, unsetting the current input method.");
            resetSelectedInputMethodAndSubtypeLocked("");
        }
        return true;
    }

    private void setSelectedInputMethodAndSubtypeLocked(InputMethodInfo imi, int subtypeId, boolean setSubtypeOnly) {
        this.mSettings.saveCurrentInputMethodAndSubtypeToHistory(this.mCurMethodId, this.mCurrentSubtype);
        this.mCurUserActionNotificationSequenceNumber = Math.max(this.mCurUserActionNotificationSequenceNumber + 1, 1);
        if (!(this.mCurClient == null || this.mCurClient.client == null)) {
            executeOrSendMessage(this.mCurClient.client, this.mCaller.obtainMessageIO(3040, this.mCurUserActionNotificationSequenceNumber, this.mCurClient));
        }
        if (imi == null || subtypeId < 0) {
            this.mSettings.putSelectedSubtype(-1);
            this.mCurrentSubtype = null;
        } else if (subtypeId < imi.getSubtypeCount()) {
            InputMethodSubtype subtype = imi.getSubtypeAt(subtypeId);
            this.mSettings.putSelectedSubtype(subtype.hashCode());
            this.mCurrentSubtype = subtype;
        } else {
            this.mSettings.putSelectedSubtype(-1);
            this.mCurrentSubtype = getCurrentInputMethodSubtypeLocked();
        }
        if (!setSubtypeOnly) {
            this.mSettings.putSelectedInputMethod(imi != null ? imi.getId() : "");
        }
    }

    private void resetSelectedInputMethodAndSubtypeLocked(String newDefaultIme) {
        InputMethodInfo imi = (InputMethodInfo) this.mMethodMap.get(newDefaultIme);
        int lastSubtypeId = -1;
        if (!(imi == null || (TextUtils.isEmpty(newDefaultIme) ^ 1) == 0)) {
            String subtypeHashCode = this.mSettings.getLastSubtypeForInputMethodLocked(newDefaultIme);
            if (subtypeHashCode != null) {
                try {
                    lastSubtypeId = InputMethodUtils.getSubtypeIdFromHashCode(imi, Integer.parseInt(subtypeHashCode));
                } catch (NumberFormatException e) {
                    Slog.w(TAG, "HashCode for subtype looks broken: " + subtypeHashCode, e);
                }
            }
        }
        setSelectedInputMethodAndSubtypeLocked(imi, lastSubtypeId, false);
    }

    private Pair<InputMethodInfo, InputMethodSubtype> findLastResortApplicableShortcutInputMethodAndSubtypeLocked(String mode) {
        Object mostApplicableIMI = null;
        Object mostApplicableSubtype = null;
        boolean foundInSystemIME = false;
        for (InputMethodInfo imi : this.mSettings.getEnabledInputMethodListLocked()) {
            String imiId = imi.getId();
            if (!foundInSystemIME || (imiId.equals(this.mCurMethodId) ^ 1) == 0) {
                ArrayList<InputMethodSubtype> subtypesForSearch;
                InputMethodSubtype subtype = null;
                List<InputMethodSubtype> enabledSubtypes = this.mSettings.getEnabledInputMethodSubtypeListLocked(this.mContext, imi, true);
                if (this.mCurrentSubtype != null) {
                    subtype = InputMethodUtils.findLastResortApplicableSubtypeLocked(this.mRes, enabledSubtypes, mode, this.mCurrentSubtype.getLocale(), false);
                }
                if (subtype == null) {
                    subtype = InputMethodUtils.findLastResortApplicableSubtypeLocked(this.mRes, enabledSubtypes, mode, null, true);
                }
                ArrayList<InputMethodSubtype> overridingImplicitlyEnabledSubtypes = InputMethodUtils.getOverridingImplicitlyEnabledSubtypes(imi, mode);
                if (overridingImplicitlyEnabledSubtypes.isEmpty()) {
                    subtypesForSearch = InputMethodUtils.getSubtypes(imi);
                } else {
                    subtypesForSearch = overridingImplicitlyEnabledSubtypes;
                }
                if (subtype == null && this.mCurrentSubtype != null) {
                    subtype = InputMethodUtils.findLastResortApplicableSubtypeLocked(this.mRes, subtypesForSearch, mode, this.mCurrentSubtype.getLocale(), false);
                }
                if (subtype == null) {
                    subtype = InputMethodUtils.findLastResortApplicableSubtypeLocked(this.mRes, subtypesForSearch, mode, null, true);
                }
                if (subtype == null) {
                    continue;
                } else if (imiId.equals(this.mCurMethodId)) {
                    mostApplicableIMI = imi;
                    mostApplicableSubtype = subtype;
                    break;
                } else if (!foundInSystemIME) {
                    mostApplicableIMI = imi;
                    mostApplicableSubtype = subtype;
                    if ((imi.getServiceInfo().applicationInfo.flags & 1) != 0) {
                        foundInSystemIME = true;
                    }
                }
            }
        }
        if (mostApplicableIMI != null) {
            return new Pair(mostApplicableIMI, mostApplicableSubtype);
        }
        return null;
    }

    public InputMethodSubtype getCurrentInputMethodSubtype() {
        if (!calledFromValidUser()) {
            return null;
        }
        InputMethodSubtype currentInputMethodSubtypeLocked;
        synchronized (this.mMethodMap) {
            currentInputMethodSubtypeLocked = getCurrentInputMethodSubtypeLocked();
        }
        return currentInputMethodSubtypeLocked;
    }

    private InputMethodSubtype getCurrentInputMethodSubtypeLocked() {
        if (this.mCurMethodId == null) {
            return null;
        }
        boolean subtypeIsSelected = this.mSettings.isSubtypeSelected();
        InputMethodInfo imi = (InputMethodInfo) this.mMethodMap.get(this.mCurMethodId);
        if (imi == null || imi.getSubtypeCount() == 0) {
            return null;
        }
        if (subtypeIsSelected && this.mCurrentSubtype != null) {
            if ((InputMethodUtils.isValidSubtypeId(imi, this.mCurrentSubtype.hashCode()) ^ 1) != 0) {
            }
            return this.mCurrentSubtype;
        }
        int subtypeId = this.mSettings.getSelectedInputMethodSubtypeId(this.mCurMethodId);
        if (subtypeId == -1) {
            List<InputMethodSubtype> explicitlyOrImplicitlyEnabledSubtypes = this.mSettings.getEnabledInputMethodSubtypeListLocked(this.mContext, imi, true);
            if (explicitlyOrImplicitlyEnabledSubtypes.size() == 1) {
                this.mCurrentSubtype = (InputMethodSubtype) explicitlyOrImplicitlyEnabledSubtypes.get(0);
            } else if (explicitlyOrImplicitlyEnabledSubtypes.size() > 1) {
                this.mCurrentSubtype = InputMethodUtils.findLastResortApplicableSubtypeLocked(this.mRes, explicitlyOrImplicitlyEnabledSubtypes, "keyboard", null, true);
                if (this.mCurrentSubtype == null) {
                    this.mCurrentSubtype = InputMethodUtils.findLastResortApplicableSubtypeLocked(this.mRes, explicitlyOrImplicitlyEnabledSubtypes, null, null, true);
                }
            }
        } else {
            this.mCurrentSubtype = (InputMethodSubtype) InputMethodUtils.getSubtypes(imi).get(subtypeId);
        }
        return this.mCurrentSubtype;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List getShortcutInputMethodsAndSubtypes() {
        /*
        r8 = this;
        r7 = r8.mMethodMap;
        monitor-enter(r7);
        r3 = new java.util.ArrayList;	 Catch:{ all -> 0x005a }
        r3.<init>();	 Catch:{ all -> 0x005a }
        r6 = r8.mShortcutInputMethodsAndSubtypes;	 Catch:{ all -> 0x005a }
        r6 = r6.size();	 Catch:{ all -> 0x005a }
        if (r6 != 0) goto L_0x0025;
    L_0x0010:
        r6 = "voice";
        r2 = r8.findLastResortApplicableShortcutInputMethodAndSubtypeLocked(r6);	 Catch:{ all -> 0x005a }
        if (r2 == 0) goto L_0x0023;
    L_0x0019:
        r6 = r2.first;	 Catch:{ all -> 0x005a }
        r3.add(r6);	 Catch:{ all -> 0x005a }
        r6 = r2.second;	 Catch:{ all -> 0x005a }
        r3.add(r6);	 Catch:{ all -> 0x005a }
    L_0x0023:
        monitor-exit(r7);
        return r3;
    L_0x0025:
        r6 = r8.mShortcutInputMethodsAndSubtypes;	 Catch:{ all -> 0x005a }
        r6 = r6.keySet();	 Catch:{ all -> 0x005a }
        r1 = r6.iterator();	 Catch:{ all -> 0x005a }
    L_0x002f:
        r6 = r1.hasNext();	 Catch:{ all -> 0x005a }
        if (r6 == 0) goto L_0x005d;
    L_0x0035:
        r0 = r1.next();	 Catch:{ all -> 0x005a }
        r0 = (android.view.inputmethod.InputMethodInfo) r0;	 Catch:{ all -> 0x005a }
        r3.add(r0);	 Catch:{ all -> 0x005a }
        r6 = r8.mShortcutInputMethodsAndSubtypes;	 Catch:{ all -> 0x005a }
        r6 = r6.get(r0);	 Catch:{ all -> 0x005a }
        r6 = (java.util.ArrayList) r6;	 Catch:{ all -> 0x005a }
        r5 = r6.iterator();	 Catch:{ all -> 0x005a }
    L_0x004a:
        r6 = r5.hasNext();	 Catch:{ all -> 0x005a }
        if (r6 == 0) goto L_0x002f;
    L_0x0050:
        r4 = r5.next();	 Catch:{ all -> 0x005a }
        r4 = (android.view.inputmethod.InputMethodSubtype) r4;	 Catch:{ all -> 0x005a }
        r3.add(r4);	 Catch:{ all -> 0x005a }
        goto L_0x004a;
    L_0x005a:
        r6 = move-exception;
        monitor-exit(r7);
        throw r6;
    L_0x005d:
        monitor-exit(r7);
        return r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.getShortcutInputMethodsAndSubtypes():java.util.List");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean setCurrentInputMethodSubtype(android.view.inputmethod.InputMethodSubtype r7) {
        /*
        r6 = this;
        r5 = 0;
        r2 = r6.calledFromValidUser();
        if (r2 != 0) goto L_0x0008;
    L_0x0007:
        return r5;
    L_0x0008:
        r3 = r6.mMethodMap;
        monitor-enter(r3);
        if (r7 == 0) goto L_0x002e;
    L_0x000d:
        r2 = r6.mCurMethodId;	 Catch:{ all -> 0x0030 }
        if (r2 == 0) goto L_0x002e;
    L_0x0011:
        r2 = r6.mMethodMap;	 Catch:{ all -> 0x0030 }
        r4 = r6.mCurMethodId;	 Catch:{ all -> 0x0030 }
        r0 = r2.get(r4);	 Catch:{ all -> 0x0030 }
        r0 = (android.view.inputmethod.InputMethodInfo) r0;	 Catch:{ all -> 0x0030 }
        r2 = r7.hashCode();	 Catch:{ all -> 0x0030 }
        r1 = com.android.internal.inputmethod.InputMethodUtils.getSubtypeIdFromHashCode(r0, r2);	 Catch:{ all -> 0x0030 }
        r2 = -1;
        if (r1 == r2) goto L_0x002e;
    L_0x0026:
        r2 = r6.mCurMethodId;	 Catch:{ all -> 0x0030 }
        r6.setInputMethodLocked(r2, r1);	 Catch:{ all -> 0x0030 }
        r2 = 1;
        monitor-exit(r3);
        return r2;
    L_0x002e:
        monitor-exit(r3);
        return r5;
    L_0x0030:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.setCurrentInputMethodSubtype(android.view.inputmethod.InputMethodSubtype):boolean");
    }

    private static String imeWindowStatusToString(int imeWindowVis) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        if ((imeWindowVis & 1) != 0) {
            sb.append("Active");
            first = false;
        }
        if ((imeWindowVis & 2) != 0) {
            if (!first) {
                sb.append("|");
            }
            sb.append("Visible");
        }
        return sb.toString();
    }

    public IInputContentUriToken createInputContentUriToken(IBinder token, Uri contentUri, String packageName) {
        if (!calledFromValidUser()) {
            return null;
        }
        if (token == null) {
            throw new NullPointerException("token");
        } else if (packageName == null) {
            throw new NullPointerException("packageName");
        } else if (contentUri == null) {
            throw new NullPointerException("contentUri");
        } else {
            if ("content".equals(contentUri.getScheme())) {
                synchronized (this.mMethodMap) {
                    int uid = Binder.getCallingUid();
                    if (this.mCurMethodId == null) {
                        return null;
                    } else if (this.mCurToken != token) {
                        Slog.e(TAG, "Ignoring createInputContentUriToken mCurToken=" + this.mCurToken + " token=" + token);
                        return null;
                    } else if (TextUtils.equals(this.mCurAttribute.packageName, packageName)) {
                        int imeUserId = UserHandle.getUserId(uid);
                        int appUserId = UserHandle.getUserId(this.mCurClient.uid);
                        InputContentUriTokenHandler inputContentUriTokenHandler = new InputContentUriTokenHandler(ContentProvider.getUriWithoutUserId(contentUri), uid, packageName, ContentProvider.getUserIdFromUri(contentUri, imeUserId), appUserId);
                        return inputContentUriTokenHandler;
                    } else {
                        Slog.e(TAG, "Ignoring createInputContentUriToken mCurAttribute.packageName=" + this.mCurAttribute.packageName + " packageName=" + packageName);
                        return null;
                    }
                }
            }
            throw new InvalidParameterException("contentUri must have content scheme");
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void reportFullscreenMode(android.os.IBinder r7, boolean r8) {
        /*
        r6 = this;
        r0 = r6.calledFromValidUser();
        if (r0 != 0) goto L_0x0007;
    L_0x0006:
        return;
    L_0x0007:
        r1 = r6.mMethodMap;
        monitor-enter(r1);
        r0 = r6.calledWithValidToken(r7);	 Catch:{ all -> 0x0036 }
        if (r0 != 0) goto L_0x0012;
    L_0x0010:
        monitor-exit(r1);
        return;
    L_0x0012:
        r0 = r6.mCurClient;	 Catch:{ all -> 0x0036 }
        if (r0 == 0) goto L_0x0032;
    L_0x0016:
        r0 = r6.mCurClient;	 Catch:{ all -> 0x0036 }
        r0 = r0.client;	 Catch:{ all -> 0x0036 }
        if (r0 == 0) goto L_0x0032;
    L_0x001c:
        r6.mInFullscreenMode = r8;	 Catch:{ all -> 0x0036 }
        r0 = r6.mCurClient;	 Catch:{ all -> 0x0036 }
        r2 = r0.client;	 Catch:{ all -> 0x0036 }
        r3 = r6.mCaller;	 Catch:{ all -> 0x0036 }
        if (r8 == 0) goto L_0x0034;
    L_0x0026:
        r0 = 1;
    L_0x0027:
        r4 = r6.mCurClient;	 Catch:{ all -> 0x0036 }
        r5 = 3045; // 0xbe5 float:4.267E-42 double:1.5044E-320;
        r0 = r3.obtainMessageIO(r5, r0, r4);	 Catch:{ all -> 0x0036 }
        r6.executeOrSendMessage(r2, r0);	 Catch:{ all -> 0x0036 }
    L_0x0032:
        monitor-exit(r1);
        return;
    L_0x0034:
        r0 = 0;
        goto L_0x0027;
    L_0x0036:
        r0 = move-exception;
        monitor-exit(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.InputMethodManagerService.reportFullscreenMode(android.os.IBinder, boolean):void");
    }

    protected void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        if (DumpUtils.checkDumpPermission(this.mContext, TAG, pw)) {
            ClientState client;
            ClientState focusedWindowClient;
            IInputMethod method;
            Printer p = new PrintWriterPrinter(pw);
            synchronized (this.mMethodMap) {
                p.println("Current Input Method Manager state:");
                int N = this.mMethodList.size();
                p.println("  Input Methods: mMethodMapUpdateCount=" + this.mMethodMapUpdateCount);
                for (int i = 0; i < N; i++) {
                    InputMethodInfo info = (InputMethodInfo) this.mMethodList.get(i);
                    p.println("  InputMethod #" + i + ":");
                    info.dump(p, "    ");
                }
                p.println("  Clients:");
                for (ClientState ci : this.mClients.values()) {
                    p.println("  Client " + ci + ":");
                    p.println("    client=" + ci.client);
                    p.println("    inputContext=" + ci.inputContext);
                    p.println("    sessionRequested=" + ci.sessionRequested);
                    p.println("    curSession=" + ci.curSession);
                }
                p.println("  mCurMethodId=" + this.mCurMethodId);
                client = this.mCurClient;
                p.println("  mCurClient=" + client + " mCurSeq=" + this.mCurSeq);
                p.println("  mCurFocusedWindow=" + this.mCurFocusedWindow + " softInputMode=" + InputMethodClient.softInputModeToString(this.mCurFocusedWindowSoftInputMode) + " client=" + this.mCurFocusedWindowClient);
                focusedWindowClient = this.mCurFocusedWindowClient;
                p.println("  mCurId=" + this.mCurId + " mHaveConnect=" + this.mHaveConnection + " mBoundToMethod=" + this.mBoundToMethod);
                p.println("  mCurToken=" + this.mCurToken);
                p.println("  mCurIntent=" + this.mCurIntent);
                method = this.mCurMethod;
                p.println("  mCurMethod=" + this.mCurMethod);
                p.println("  mEnabledSession=" + this.mEnabledSession);
                p.println("  mImeWindowVis=" + imeWindowStatusToString(this.mImeWindowVis));
                p.println("  mShowRequested=" + this.mShowRequested + " mShowExplicitlyRequested=" + this.mShowExplicitlyRequested + " mShowForced=" + this.mShowForced + " mInputShown=" + this.mInputShown);
                p.println("  mInFullscreenMode=" + this.mInFullscreenMode);
                p.println("  mCurUserActionNotificationSequenceNumber=" + this.mCurUserActionNotificationSequenceNumber);
                p.println("  mSystemReady=" + this.mSystemReady + " mInteractive=" + this.mIsInteractive);
                p.println("  mSettingsObserver=" + this.mSettingsObserver);
                p.println("  mSwitchingController:");
                this.mSwitchingController.dump(p);
                p.println("  mSettings:");
                this.mSettings.dumpLocked(p, "    ");
                p.println("  mStartInputHistory:");
                this.mStartInputHistory.dump(pw, "   ");
            }
            p.println(" ");
            if (client != null) {
                pw.flush();
                try {
                    TransferPipe.dumpAsync(client.client.asBinder(), fd, args);
                } catch (Exception e) {
                    p.println("Failed to dump input method client: " + e);
                }
            } else {
                p.println("No input method client.");
            }
            if (!(focusedWindowClient == null || client == focusedWindowClient)) {
                p.println(" ");
                p.println("Warning: Current input method client doesn't match the last focused. window.");
                p.println("Dumping input method client in the last focused window just in case.");
                p.println(" ");
                pw.flush();
                try {
                    TransferPipe.dumpAsync(focusedWindowClient.client.asBinder(), fd, args);
                } catch (Exception e2) {
                    p.println("Failed to dump input method client in focused window: " + e2);
                }
            }
            p.println(" ");
            if (method != null) {
                pw.flush();
                try {
                    TransferPipe.dumpAsync(method.asBinder(), fd, args);
                } catch (Exception e22) {
                    p.println("Failed to dump input method service: " + e22);
                }
            } else {
                p.println("No input method service.");
            }
        }
    }
}
