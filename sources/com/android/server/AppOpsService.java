package com.android.server;

import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.app.AppOpsManager.OpEntry;
import android.app.AppOpsManager.PackageOps;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManagerInternal;
import android.content.pm.PackageManagerInternal.ExternalSourcesPolicy;
import android.content.pm.UserInfo;
import android.media.AudioAttributes;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Process;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.os.ShellCallback;
import android.os.ShellCommand;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.storage.StorageManagerInternal;
import android.os.storage.StorageManagerInternal.ExternalStorageMountPolicy;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.AtomicFile;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TimeUtils;
import android.util.Xml;
import com.android.internal.app.IAppOpsCallback;
import com.android.internal.app.IAppOpsService;
import com.android.internal.app.IAppOpsService.Stub;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.Preconditions;
import com.android.internal.util.XmlUtils;
import com.android.server.job.controllers.JobStatus;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import libcore.util.EmptyArray;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class AppOpsService extends Stub {
    static final boolean DEBUG = false;
    static final String TAG = "AppOps";
    static final long WRITE_DELAY = 1800000;
    final SparseArray<SparseArray<Restriction>> mAudioRestrictions = new SparseArray();
    final ArrayMap<IBinder, ClientState> mClients = new ArrayMap();
    Context mContext;
    boolean mFastWriteScheduled;
    final AtomicFile mFile;
    final Handler mHandler;
    final ArrayMap<IBinder, Callback> mModeWatchers = new ArrayMap();
    final SparseArray<ArraySet<Callback>> mOpModeWatchers = new SparseArray();
    private final ArrayMap<IBinder, ClientRestrictionState> mOpUserRestrictions = new ArrayMap();
    final ArrayMap<String, ArraySet<Callback>> mPackageModeWatchers = new ArrayMap();
    private final SparseArray<UidState> mUidStates = new SparseArray();
    final Runnable mWriteRunner = new Runnable() {
        public void run() {
            synchronized (AppOpsService.this) {
                AppOpsService.this.mWriteScheduled = false;
                AppOpsService.this.mFastWriteScheduled = false;
                new AsyncTask<Void, Void, Void>() {
                    protected Void doInBackground(Void... params) {
                        AppOpsService.this.writeState();
                        return null;
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
            }
        }
    };
    boolean mWriteScheduled;

    public final class Callback implements DeathRecipient {
        final IAppOpsCallback mCallback;

        public Callback(IAppOpsCallback callback) {
            this.mCallback = callback;
            try {
                this.mCallback.asBinder().linkToDeath(this, 0);
            } catch (RemoteException e) {
            }
        }

        public void unlinkToDeath() {
            this.mCallback.asBinder().unlinkToDeath(this, 0);
        }

        public void binderDied() {
            AppOpsService.this.stopWatchingMode(this.mCallback);
        }
    }

    public static final class Op {
        public int duration;
        public int mode;
        public int nesting;
        public final int op;
        public final String packageName;
        public String proxyPackageName;
        public int proxyUid = -1;
        public long rejectTime;
        public long time;
        public final int uid;

        public Op(int _uid, String _packageName, int _op) {
            this.uid = _uid;
            this.packageName = _packageName;
            this.op = _op;
            this.mode = AppOpsManager.opToDefaultMode(this.op);
        }
    }

    public static final class Ops extends SparseArray<Op> {
        public final boolean isPrivileged;
        public final String packageName;
        public final UidState uidState;

        public Ops(String _packageName, UidState _uidState, boolean _isPrivileged) {
            this.packageName = _packageName;
            this.uidState = _uidState;
            this.isPrivileged = _isPrivileged;
        }
    }

    private static final class UidState {
        public SparseIntArray opModes;
        public ArrayMap<String, Ops> pkgOps;
        public final int uid;

        public UidState(int uid) {
            this.uid = uid;
        }

        public void clear() {
            this.pkgOps = null;
            this.opModes = null;
        }

        public boolean isDefault() {
            if (this.pkgOps != null && !this.pkgOps.isEmpty()) {
                return false;
            }
            if (this.opModes == null || this.opModes.size() <= 0) {
                return true;
            }
            return false;
        }
    }

    static final class ChangeRec {
        final int op;
        final String pkg;
        final int uid;

        ChangeRec(int _op, int _uid, String _pkg) {
            this.op = _op;
            this.uid = _uid;
            this.pkg = _pkg;
        }
    }

    private final class ClientRestrictionState implements DeathRecipient {
        SparseArray<String[]> perUserExcludedPackages;
        SparseArray<boolean[]> perUserRestrictions;
        private final IBinder token;

        public ClientRestrictionState(IBinder token) throws RemoteException {
            token.linkToDeath(this, 0);
            this.token = token;
        }

        public boolean setRestriction(int code, boolean restricted, String[] excludedPackages, int userId) {
            boolean changed = false;
            if (this.perUserRestrictions == null && restricted) {
                this.perUserRestrictions = new SparseArray();
            }
            int[] users;
            if (userId == -1) {
                List<UserInfo> liveUsers = UserManager.get(AppOpsService.this.mContext).getUsers(false);
                users = new int[liveUsers.size()];
                int i;
                for (i = 0; i < liveUsers.size(); i++) {
                    users[i] = ((UserInfo) liveUsers.get(i)).id;
                }
            } else {
                users = new int[]{userId};
            }
            if (this.perUserRestrictions != null) {
                for (int thisUserId : users) {
                    boolean[] userRestrictions = (boolean[]) this.perUserRestrictions.get(thisUserId);
                    if (userRestrictions == null && restricted) {
                        userRestrictions = new boolean[70];
                        this.perUserRestrictions.put(thisUserId, userRestrictions);
                    }
                    if (!(userRestrictions == null || userRestrictions[code] == restricted)) {
                        userRestrictions[code] = restricted;
                        if (!restricted && isDefault(userRestrictions)) {
                            this.perUserRestrictions.remove(thisUserId);
                            userRestrictions = null;
                        }
                        changed = true;
                    }
                    if (userRestrictions != null) {
                        boolean noExcludedPackages = ArrayUtils.isEmpty(excludedPackages);
                        if (this.perUserExcludedPackages == null && (noExcludedPackages ^ 1) != 0) {
                            this.perUserExcludedPackages = new SparseArray();
                        }
                        if (!(this.perUserExcludedPackages == null || (Arrays.equals(excludedPackages, (Object[]) this.perUserExcludedPackages.get(thisUserId)) ^ 1) == 0)) {
                            if (noExcludedPackages) {
                                this.perUserExcludedPackages.remove(thisUserId);
                                if (this.perUserExcludedPackages.size() <= 0) {
                                    this.perUserExcludedPackages = null;
                                }
                            } else {
                                this.perUserExcludedPackages.put(thisUserId, excludedPackages);
                            }
                            changed = true;
                        }
                    }
                }
            }
            return changed;
        }

        public boolean hasRestriction(int restriction, String packageName, int userId) {
            if (this.perUserRestrictions == null) {
                return false;
            }
            boolean[] restrictions = (boolean[]) this.perUserRestrictions.get(userId);
            if (restrictions == null || !restrictions[restriction]) {
                return false;
            }
            if (this.perUserExcludedPackages == null) {
                return true;
            }
            String[] perUserExclusions = (String[]) this.perUserExcludedPackages.get(userId);
            if (perUserExclusions == null) {
                return true;
            }
            return ArrayUtils.contains(perUserExclusions, packageName) ^ 1;
        }

        public void removeUser(int userId) {
            if (this.perUserExcludedPackages != null) {
                this.perUserExcludedPackages.remove(userId);
                if (this.perUserExcludedPackages.size() <= 0) {
                    this.perUserExcludedPackages = null;
                }
            }
            if (this.perUserRestrictions != null) {
                this.perUserRestrictions.remove(userId);
                if (this.perUserRestrictions.size() <= 0) {
                    this.perUserRestrictions = null;
                }
            }
        }

        public boolean isDefault() {
            return this.perUserRestrictions == null || this.perUserRestrictions.size() <= 0;
        }

        public void binderDied() {
            synchronized (AppOpsService.this) {
                AppOpsService.this.mOpUserRestrictions.remove(this.token);
                if (this.perUserRestrictions == null) {
                    return;
                }
                int userCount = this.perUserRestrictions.size();
                for (int i = 0; i < userCount; i++) {
                    boolean[] restrictions = (boolean[]) this.perUserRestrictions.valueAt(i);
                    int restrictionCount = restrictions.length;
                    for (int j = 0; j < restrictionCount; j++) {
                        if (restrictions[j]) {
                            AppOpsService.this.mHandler.post(new -$Lambda$pxHDwRy_8WGgaOZHQ9e3tJlXPqU(j, this));
                        }
                    }
                }
                destroy();
            }
        }

        /* synthetic */ void lambda$-com_android_server_AppOpsService$ClientRestrictionState_109304(int changedCode) {
            AppOpsService.this.notifyWatchersOfChange(changedCode);
        }

        public void destroy() {
            this.token.unlinkToDeath(this, 0);
        }

        private boolean isDefault(boolean[] array) {
            if (ArrayUtils.isEmpty(array)) {
                return true;
            }
            for (boolean value : array) {
                if (value) {
                    return false;
                }
            }
            return true;
        }
    }

    public final class ClientState extends Binder implements DeathRecipient {
        final IBinder mAppToken;
        final int mPid = Binder.getCallingPid();
        final ArrayList<Op> mStartedOps;

        public ClientState(IBinder appToken) {
            this.mAppToken = appToken;
            if (appToken instanceof Binder) {
                this.mStartedOps = null;
                return;
            }
            this.mStartedOps = new ArrayList();
            try {
                this.mAppToken.linkToDeath(this, 0);
            } catch (RemoteException e) {
            }
        }

        public String toString() {
            return "ClientState{mAppToken=" + this.mAppToken + ", " + (this.mStartedOps != null ? "pid=" + this.mPid : "local") + '}';
        }

        public void binderDied() {
            synchronized (AppOpsService.this) {
                for (int i = this.mStartedOps.size() - 1; i >= 0; i--) {
                    AppOpsService.this.finishOperationLocked((Op) this.mStartedOps.get(i));
                }
                AppOpsService.this.mClients.remove(this.mAppToken);
            }
        }
    }

    private static final class Restriction {
        private static final ArraySet<String> NO_EXCEPTIONS = new ArraySet();
        ArraySet<String> exceptionPackages;
        int mode;

        private Restriction() {
            this.exceptionPackages = NO_EXCEPTIONS;
        }
    }

    static class Shell extends ShellCommand {
        final IAppOpsService mInterface;
        final AppOpsService mInternal;
        int mode;
        String modeStr;
        int nonpackageUid;
        int op;
        String opStr;
        String packageName;
        int packageUid;
        int userId = 0;

        Shell(IAppOpsService iface, AppOpsService internal) {
            this.mInterface = iface;
            this.mInternal = internal;
        }

        public int onCommand(String cmd) {
            return AppOpsService.onShellCommand(this, cmd);
        }

        public void onHelp() {
            AppOpsService.dumpCommandHelp(getOutPrintWriter());
        }

        private int strOpToOp(String op, PrintWriter err) {
            try {
                return AppOpsManager.strOpToOp(op);
            } catch (IllegalArgumentException e) {
                try {
                    return Integer.parseInt(op);
                } catch (NumberFormatException e2) {
                    try {
                        return AppOpsManager.strDebugOpToOp(op);
                    } catch (IllegalArgumentException e3) {
                        err.println("Error: " + e3.getMessage());
                        return -1;
                    }
                }
            }
        }

        int strModeToMode(String modeStr, PrintWriter err) {
            if (modeStr.equals("allow")) {
                return 0;
            }
            if (modeStr.equals("deny")) {
                return 2;
            }
            if (modeStr.equals("ignore")) {
                return 1;
            }
            if (modeStr.equals("default")) {
                return 3;
            }
            try {
                return Integer.parseInt(modeStr);
            } catch (NumberFormatException e) {
                err.println("Error: Mode " + modeStr + " is not valid");
                return -1;
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        int parseUserOpMode(int r6, java.io.PrintWriter r7) throws android.os.RemoteException {
            /*
            r5 = this;
            r4 = 0;
            r3 = -1;
            r2 = 0;
            r1 = -2;
            r5.userId = r1;
            r5.opStr = r2;
            r5.modeStr = r2;
        L_0x000a:
            r0 = r5.getNextArg();
            if (r0 == 0) goto L_0x0031;
        L_0x0010:
            r1 = "--user";
            r1 = r1.equals(r0);
            if (r1 == 0) goto L_0x0024;
        L_0x0019:
            r1 = r5.getNextArgRequired();
            r1 = android.os.UserHandle.parseUserArg(r1);
            r5.userId = r1;
            goto L_0x000a;
        L_0x0024:
            r1 = r5.opStr;
            if (r1 != 0) goto L_0x002b;
        L_0x0028:
            r5.opStr = r0;
            goto L_0x000a;
        L_0x002b:
            r1 = r5.modeStr;
            if (r1 != 0) goto L_0x000a;
        L_0x002f:
            r5.modeStr = r0;
        L_0x0031:
            r1 = r5.opStr;
            if (r1 != 0) goto L_0x003c;
        L_0x0035:
            r1 = "Error: Operation not specified.";
            r7.println(r1);
            return r3;
        L_0x003c:
            r1 = r5.opStr;
            r1 = r5.strOpToOp(r1, r7);
            r5.op = r1;
            r1 = r5.op;
            if (r1 >= 0) goto L_0x0049;
        L_0x0048:
            return r3;
        L_0x0049:
            r1 = r5.modeStr;
            if (r1 == 0) goto L_0x0058;
        L_0x004d:
            r1 = r5.modeStr;
            r1 = r5.strModeToMode(r1, r7);
            r5.mode = r1;
            if (r1 >= 0) goto L_0x005a;
        L_0x0057:
            return r3;
        L_0x0058:
            r5.mode = r6;
        L_0x005a:
            return r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.AppOpsService.Shell.parseUserOpMode(int, java.io.PrintWriter):int");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        int parseUserPackageOp(boolean r14, java.io.PrintWriter r15) throws android.os.RemoteException {
            /*
            r13 = this;
            r9 = -2;
            r13.userId = r9;
            r9 = 0;
            r13.packageName = r9;
            r9 = 0;
            r13.opStr = r9;
        L_0x0009:
            r0 = r13.getNextArg();
            if (r0 == 0) goto L_0x0030;
        L_0x000f:
            r9 = "--user";
            r9 = r9.equals(r0);
            if (r9 == 0) goto L_0x0023;
        L_0x0018:
            r9 = r13.getNextArgRequired();
            r9 = android.os.UserHandle.parseUserArg(r9);
            r13.userId = r9;
            goto L_0x0009;
        L_0x0023:
            r9 = r13.packageName;
            if (r9 != 0) goto L_0x002a;
        L_0x0027:
            r13.packageName = r0;
            goto L_0x0009;
        L_0x002a:
            r9 = r13.opStr;
            if (r9 != 0) goto L_0x0009;
        L_0x002e:
            r13.opStr = r0;
        L_0x0030:
            r9 = r13.packageName;
            if (r9 != 0) goto L_0x003c;
        L_0x0034:
            r9 = "Error: Package name not specified.";
            r15.println(r9);
            r9 = -1;
            return r9;
        L_0x003c:
            r9 = r13.opStr;
            if (r9 != 0) goto L_0x004a;
        L_0x0040:
            if (r14 == 0) goto L_0x004a;
        L_0x0042:
            r9 = "Error: Operation not specified.";
            r15.println(r9);
            r9 = -1;
            return r9;
        L_0x004a:
            r9 = r13.opStr;
            if (r9 == 0) goto L_0x005c;
        L_0x004e:
            r9 = r13.opStr;
            r9 = r13.strOpToOp(r9, r15);
            r13.op = r9;
            r9 = r13.op;
            if (r9 >= 0) goto L_0x005f;
        L_0x005a:
            r9 = -1;
            return r9;
        L_0x005c:
            r9 = -1;
            r13.op = r9;
        L_0x005f:
            r9 = r13.userId;
            r10 = -2;
            if (r9 != r10) goto L_0x006a;
        L_0x0064:
            r9 = android.app.ActivityManager.getCurrentUser();
            r13.userId = r9;
        L_0x006a:
            r9 = -1;
            r13.nonpackageUid = r9;
            r9 = r13.packageName;	 Catch:{ NumberFormatException -> 0x0172 }
            r9 = java.lang.Integer.parseInt(r9);	 Catch:{ NumberFormatException -> 0x0172 }
            r13.nonpackageUid = r9;	 Catch:{ NumberFormatException -> 0x0172 }
        L_0x0075:
            r9 = r13.nonpackageUid;
            r10 = -1;
            if (r9 != r10) goto L_0x010e;
        L_0x007a:
            r9 = r13.packageName;
            r9 = r9.length();
            r10 = 1;
            if (r9 <= r10) goto L_0x010e;
        L_0x0083:
            r9 = r13.packageName;
            r10 = 0;
            r9 = r9.charAt(r10);
            r10 = 117; // 0x75 float:1.64E-43 double:5.8E-322;
            if (r9 != r10) goto L_0x010e;
        L_0x008e:
            r9 = r13.packageName;
            r10 = 46;
            r9 = r9.indexOf(r10);
            if (r9 >= 0) goto L_0x010e;
        L_0x0098:
            r2 = 1;
        L_0x0099:
            r9 = r13.packageName;
            r9 = r9.length();
            if (r2 >= r9) goto L_0x00b8;
        L_0x00a1:
            r9 = r13.packageName;
            r9 = r9.charAt(r2);
            r10 = 48;
            if (r9 < r10) goto L_0x00b8;
        L_0x00ab:
            r9 = r13.packageName;
            r9 = r9.charAt(r2);
            r10 = 57;
            if (r9 > r10) goto L_0x00b8;
        L_0x00b5:
            r2 = r2 + 1;
            goto L_0x0099;
        L_0x00b8:
            r9 = 1;
            if (r2 <= r9) goto L_0x010e;
        L_0x00bb:
            r9 = r13.packageName;
            r9 = r9.length();
            if (r2 >= r9) goto L_0x010e;
        L_0x00c3:
            r9 = r13.packageName;
            r10 = 1;
            r8 = r9.substring(r10, r2);
            r7 = java.lang.Integer.parseInt(r8);	 Catch:{ NumberFormatException -> 0x0170 }
            r9 = r13.packageName;	 Catch:{ NumberFormatException -> 0x0170 }
            r4 = r9.charAt(r2);	 Catch:{ NumberFormatException -> 0x0170 }
            r2 = r2 + 1;
            r3 = r2;
        L_0x00d7:
            r9 = r13.packageName;	 Catch:{ NumberFormatException -> 0x0170 }
            r9 = r9.length();	 Catch:{ NumberFormatException -> 0x0170 }
            if (r2 >= r9) goto L_0x00f6;
        L_0x00df:
            r9 = r13.packageName;	 Catch:{ NumberFormatException -> 0x0170 }
            r9 = r9.charAt(r2);	 Catch:{ NumberFormatException -> 0x0170 }
            r10 = 48;
            if (r9 < r10) goto L_0x00f6;
        L_0x00e9:
            r9 = r13.packageName;	 Catch:{ NumberFormatException -> 0x0170 }
            r9 = r9.charAt(r2);	 Catch:{ NumberFormatException -> 0x0170 }
            r10 = 57;
            if (r9 > r10) goto L_0x00f6;
        L_0x00f3:
            r2 = r2 + 1;
            goto L_0x00d7;
        L_0x00f6:
            if (r2 <= r3) goto L_0x010e;
        L_0x00f8:
            r9 = r13.packageName;	 Catch:{ NumberFormatException -> 0x0170 }
            r6 = r9.substring(r3, r2);	 Catch:{ NumberFormatException -> 0x0170 }
            r5 = java.lang.Integer.parseInt(r6);	 Catch:{ NumberFormatException -> 0x0123 }
            r9 = 97;
            if (r4 != r9) goto L_0x0118;
        L_0x0106:
            r9 = r5 + 10000;
            r9 = android.os.UserHandle.getUid(r7, r9);	 Catch:{ NumberFormatException -> 0x0123 }
            r13.nonpackageUid = r9;	 Catch:{ NumberFormatException -> 0x0123 }
        L_0x010e:
            r9 = r13.nonpackageUid;
            r10 = -1;
            if (r9 == r10) goto L_0x0125;
        L_0x0113:
            r9 = 0;
            r13.packageName = r9;
        L_0x0116:
            r9 = 0;
            return r9;
        L_0x0118:
            r9 = 115; // 0x73 float:1.61E-43 double:5.7E-322;
            if (r4 != r9) goto L_0x010e;
        L_0x011c:
            r9 = android.os.UserHandle.getUid(r7, r5);	 Catch:{ NumberFormatException -> 0x0123 }
            r13.nonpackageUid = r9;	 Catch:{ NumberFormatException -> 0x0123 }
            goto L_0x010e;
        L_0x0123:
            r1 = move-exception;
            goto L_0x010e;
        L_0x0125:
            r9 = "root";
            r10 = r13.packageName;
            r9 = r9.equals(r10);
            if (r9 == 0) goto L_0x015f;
        L_0x0130:
            r9 = 0;
            r13.packageUid = r9;
        L_0x0133:
            r9 = r13.packageUid;
            if (r9 >= 0) goto L_0x0116;
        L_0x0137:
            r9 = new java.lang.StringBuilder;
            r9.<init>();
            r10 = "Error: No UID for ";
            r9 = r9.append(r10);
            r10 = r13.packageName;
            r9 = r9.append(r10);
            r10 = " in user ";
            r9 = r9.append(r10);
            r10 = r13.userId;
            r9 = r9.append(r10);
            r9 = r9.toString();
            r15.println(r9);
            r9 = -1;
            return r9;
        L_0x015f:
            r9 = android.app.AppGlobals.getPackageManager();
            r10 = r13.packageName;
            r11 = r13.userId;
            r12 = 8192; // 0x2000 float:1.14794E-41 double:4.0474E-320;
            r9 = r9.getPackageUid(r10, r12, r11);
            r13.packageUid = r9;
            goto L_0x0133;
        L_0x0170:
            r1 = move-exception;
            goto L_0x010e;
        L_0x0172:
            r1 = move-exception;
            goto L_0x0075;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.AppOpsService.Shell.parseUserPackageOp(boolean, java.io.PrintWriter):int");
        }
    }

    public AppOpsService(File storagePath, Handler handler) {
        LockGuard.installLock((Object) this, 0);
        this.mFile = new AtomicFile(storagePath);
        this.mHandler = handler;
        readState();
    }

    public void publish(Context context) {
        this.mContext = context;
        ServiceManager.addService("appops", asBinder());
    }

    public void systemReady() {
        synchronized (this) {
            boolean changed = false;
            for (int i = this.mUidStates.size() - 1; i >= 0; i--) {
                UidState uidState = (UidState) this.mUidStates.valueAt(i);
                if (ArrayUtils.isEmpty(getPackagesForUid(uidState.uid))) {
                    uidState.clear();
                    this.mUidStates.removeAt(i);
                    changed = true;
                } else {
                    ArrayMap<String, Ops> pkgs = uidState.pkgOps;
                    if (pkgs != null) {
                        Iterator<Ops> it = pkgs.values().iterator();
                        while (it.hasNext()) {
                            Ops ops = (Ops) it.next();
                            int curUid = -1;
                            try {
                                curUid = AppGlobals.getPackageManager().getPackageUid(ops.packageName, 8192, UserHandle.getUserId(ops.uidState.uid));
                            } catch (RemoteException e) {
                            }
                            if (curUid != ops.uidState.uid) {
                                Slog.i(TAG, "Pruning old package " + ops.packageName + "/" + ops.uidState + ": new uid=" + curUid);
                                it.remove();
                                changed = true;
                            }
                        }
                        if (uidState.isDefault()) {
                            this.mUidStates.removeAt(i);
                        }
                    } else {
                        continue;
                    }
                }
            }
            if (changed) {
                scheduleFastWriteLocked();
            }
        }
        ((PackageManagerInternal) LocalServices.getService(PackageManagerInternal.class)).setExternalSourcesPolicy(new ExternalSourcesPolicy() {
            public int getPackageTrustedToInstallApps(String packageName, int uid) {
                switch (AppOpsService.this.checkOperation(66, uid, packageName)) {
                    case 0:
                        return 0;
                    case 2:
                        return 1;
                    default:
                        return 2;
                }
            }
        });
        ((StorageManagerInternal) LocalServices.getService(StorageManagerInternal.class)).addExternalStoragePolicy(new ExternalStorageMountPolicy() {
            public int getMountMode(int uid, String packageName) {
                if (Process.isIsolated(uid) || AppOpsService.this.noteOperation(59, uid, packageName) != 0) {
                    return 0;
                }
                if (AppOpsService.this.noteOperation(60, uid, packageName) != 0) {
                    return 2;
                }
                return 3;
            }

            public boolean hasExternalStorage(int uid, String packageName) {
                int mountMode = getMountMode(uid, packageName);
                if (mountMode == 2 || mountMode == 3) {
                    return true;
                }
                return false;
            }
        });
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void packageRemoved(int r4, java.lang.String r5) {
        /*
        r3 = this;
        monitor-enter(r3);
        r2 = r3.mUidStates;	 Catch:{ all -> 0x0038 }
        r1 = r2.get(r4);	 Catch:{ all -> 0x0038 }
        r1 = (com.android.server.AppOpsService.UidState) r1;	 Catch:{ all -> 0x0038 }
        if (r1 != 0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r3);
        return;
    L_0x000d:
        r0 = 0;
        r2 = r1.pkgOps;	 Catch:{ all -> 0x0038 }
        if (r2 == 0) goto L_0x001b;
    L_0x0012:
        r2 = r1.pkgOps;	 Catch:{ all -> 0x0038 }
        r2 = r2.remove(r5);	 Catch:{ all -> 0x0038 }
        if (r2 == 0) goto L_0x001b;
    L_0x001a:
        r0 = 1;
    L_0x001b:
        if (r0 == 0) goto L_0x0031;
    L_0x001d:
        r2 = r1.pkgOps;	 Catch:{ all -> 0x0038 }
        r2 = r2.isEmpty();	 Catch:{ all -> 0x0038 }
        if (r2 == 0) goto L_0x0031;
    L_0x0025:
        r2 = getPackagesForUid(r4);	 Catch:{ all -> 0x0038 }
        r2 = r2.length;	 Catch:{ all -> 0x0038 }
        if (r2 > 0) goto L_0x0031;
    L_0x002c:
        r2 = r3.mUidStates;	 Catch:{ all -> 0x0038 }
        r2.remove(r4);	 Catch:{ all -> 0x0038 }
    L_0x0031:
        if (r0 == 0) goto L_0x0036;
    L_0x0033:
        r3.scheduleFastWriteLocked();	 Catch:{ all -> 0x0038 }
    L_0x0036:
        monitor-exit(r3);
        return;
    L_0x0038:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.AppOpsService.packageRemoved(int, java.lang.String):void");
    }

    public void uidRemoved(int uid) {
        synchronized (this) {
            if (this.mUidStates.indexOfKey(uid) >= 0) {
                this.mUidStates.remove(uid);
                scheduleFastWriteLocked();
            }
        }
    }

    public void shutdown() {
        Slog.w(TAG, "Writing app ops before shutdown...");
        boolean doWrite = false;
        synchronized (this) {
            if (this.mWriteScheduled) {
                this.mWriteScheduled = false;
                doWrite = true;
            }
        }
        if (doWrite) {
            writeState();
        }
    }

    private ArrayList<OpEntry> collectOps(Ops pkgOps, int[] ops) {
        ArrayList<OpEntry> arrayList = null;
        int j;
        Op curOp;
        if (ops == null) {
            arrayList = new ArrayList();
            for (j = 0; j < pkgOps.size(); j++) {
                curOp = (Op) pkgOps.valueAt(j);
                arrayList.add(new OpEntry(curOp.op, curOp.mode, curOp.time, curOp.rejectTime, curOp.duration, curOp.proxyUid, curOp.proxyPackageName));
            }
        } else {
            for (int i : ops) {
                curOp = (Op) pkgOps.get(i);
                if (curOp != null) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(new OpEntry(curOp.op, curOp.mode, curOp.time, curOp.rejectTime, curOp.duration, curOp.proxyUid, curOp.proxyPackageName));
                }
            }
        }
        return arrayList;
    }

    private ArrayList<OpEntry> collectOps(SparseIntArray uidOps, int[] ops) {
        ArrayList<OpEntry> arrayList = null;
        int j;
        if (ops == null) {
            arrayList = new ArrayList();
            for (j = 0; j < uidOps.size(); j++) {
                arrayList.add(new OpEntry(uidOps.keyAt(j), uidOps.valueAt(j), 0, 0, 0, -1, null));
            }
        } else {
            for (int indexOfKey : ops) {
                int index = uidOps.indexOfKey(indexOfKey);
                if (index >= 0) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(new OpEntry(uidOps.keyAt(index), uidOps.valueAt(index), 0, 0, 0, -1, null));
                }
            }
        }
        return arrayList;
    }

    public List<PackageOps> getPackagesForOps(int[] ops) {
        Throwable th;
        this.mContext.enforcePermission("android.permission.GET_APP_OPS_STATS", Binder.getCallingPid(), Binder.getCallingUid(), null);
        List<PackageOps> res = null;
        synchronized (this) {
            try {
                int uidStateCount = this.mUidStates.size();
                for (int i = 0; i < uidStateCount; i++) {
                    UidState uidState = (UidState) this.mUidStates.valueAt(i);
                    if (!(uidState.pkgOps == null || uidState.pkgOps.isEmpty())) {
                        ArrayMap<String, Ops> packages = uidState.pkgOps;
                        int packageCount = packages.size();
                        int j = 0;
                        ArrayList<PackageOps> res2 = res;
                        while (j < packageCount) {
                            ArrayList<PackageOps> res3;
                            try {
                                Ops pkgOps = (Ops) packages.valueAt(j);
                                ArrayList<OpEntry> resOps = collectOps(pkgOps, ops);
                                if (resOps != null) {
                                    if (res2 == null) {
                                        res3 = new ArrayList();
                                    } else {
                                        res3 = res2;
                                    }
                                    res3.add(new PackageOps(pkgOps.packageName, pkgOps.uidState.uid, resOps));
                                } else {
                                    res3 = res2;
                                }
                                j++;
                                res2 = res3;
                            } catch (Throwable th2) {
                                th = th2;
                                res3 = res2;
                            }
                        }
                        Object res4 = res2;
                    }
                }
                return res;
            } catch (Throwable th3) {
                th = th3;
            }
        }
        throw th;
    }

    public List<PackageOps> getOpsForPackage(int uid, String packageName, int[] ops) {
        this.mContext.enforcePermission("android.permission.GET_APP_OPS_STATS", Binder.getCallingPid(), Binder.getCallingUid(), null);
        String resolvedPackageName = resolvePackageName(uid, packageName);
        if (resolvedPackageName == null) {
            return Collections.emptyList();
        }
        synchronized (this) {
            Ops pkgOps = getOpsRawLocked(uid, resolvedPackageName, false);
            if (pkgOps == null) {
                return null;
            }
            ArrayList<OpEntry> resOps = collectOps(pkgOps, ops);
            if (resOps == null) {
                return null;
            }
            ArrayList<PackageOps> res = new ArrayList();
            res.add(new PackageOps(pkgOps.packageName, pkgOps.uidState.uid, resOps));
            return res;
        }
    }

    public List<PackageOps> getUidOps(int uid, int[] ops) {
        this.mContext.enforcePermission("android.permission.GET_APP_OPS_STATS", Binder.getCallingPid(), Binder.getCallingUid(), null);
        synchronized (this) {
            UidState uidState = getUidStateLocked(uid, false);
            if (uidState == null) {
                return null;
            }
            ArrayList<OpEntry> resOps = collectOps(uidState.opModes, ops);
            if (resOps == null) {
                return null;
            }
            ArrayList<PackageOps> res = new ArrayList();
            res.add(new PackageOps(null, uidState.uid, resOps));
            return res;
        }
    }

    private void pruneOp(Op op, int uid, String packageName) {
        if (op.time == 0 && op.rejectTime == 0) {
            Ops ops = getOpsRawLocked(uid, packageName, false);
            if (ops != null) {
                ops.remove(op.op);
                if (ops.size() <= 0) {
                    UidState uidState = ops.uidState;
                    ArrayMap<String, Ops> pkgOps = uidState.pkgOps;
                    if (pkgOps != null) {
                        pkgOps.remove(ops.packageName);
                        if (pkgOps.isEmpty()) {
                            uidState.pkgOps = null;
                        }
                        if (uidState.isDefault()) {
                            this.mUidStates.remove(uid);
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setUidMode(int r28, int r29, int r30) {
        /*
        r27 = this;
        r22 = android.os.Binder.getCallingPid();
        r23 = android.os.Process.myPid();
        r0 = r22;
        r1 = r23;
        if (r0 == r1) goto L_0x0024;
    L_0x000e:
        r0 = r27;
        r0 = r0.mContext;
        r22 = r0;
        r23 = "android.permission.UPDATE_APP_OPS_STATS";
        r24 = android.os.Binder.getCallingPid();
        r25 = android.os.Binder.getCallingUid();
        r26 = 0;
        r22.enforcePermission(r23, r24, r25, r26);
    L_0x0024:
        r27.verifyIncomingOp(r28);
        r28 = android.app.AppOpsManager.opToSwitch(r28);
        monitor-enter(r27);
        r10 = android.app.AppOpsManager.opToDefaultMode(r28);	 Catch:{ all -> 0x00e0 }
        r22 = 0;
        r0 = r27;
        r1 = r29;
        r2 = r22;
        r21 = r0.getUidStateLocked(r1, r2);	 Catch:{ all -> 0x00e0 }
        if (r21 != 0) goto L_0x00b6;
    L_0x003e:
        r0 = r30;
        if (r0 != r10) goto L_0x0044;
    L_0x0042:
        monitor-exit(r27);
        return;
    L_0x0044:
        r21 = new com.android.server.AppOpsService$UidState;	 Catch:{ all -> 0x00e0 }
        r0 = r21;
        r1 = r29;
        r0.<init>(r1);	 Catch:{ all -> 0x00e0 }
        r22 = new android.util.SparseIntArray;	 Catch:{ all -> 0x00e0 }
        r22.<init>();	 Catch:{ all -> 0x00e0 }
        r0 = r22;
        r1 = r21;
        r1.opModes = r0;	 Catch:{ all -> 0x00e0 }
        r0 = r21;
        r0 = r0.opModes;	 Catch:{ all -> 0x00e0 }
        r22 = r0;
        r0 = r22;
        r1 = r28;
        r2 = r30;
        r0.put(r1, r2);	 Catch:{ all -> 0x00e0 }
        r0 = r27;
        r0 = r0.mUidStates;	 Catch:{ all -> 0x00e0 }
        r22 = r0;
        r0 = r22;
        r1 = r29;
        r2 = r21;
        r0.put(r1, r2);	 Catch:{ all -> 0x00e0 }
        r27.scheduleWriteLocked();	 Catch:{ all -> 0x00e0 }
    L_0x0079:
        monitor-exit(r27);
        r20 = getPackagesForUid(r29);
        r6 = 0;
        monitor-enter(r27);
        r0 = r27;
        r0 = r0.mOpModeWatchers;	 Catch:{ all -> 0x018b }
        r22 = r0;
        r0 = r22;
        r1 = r28;
        r8 = r0.get(r1);	 Catch:{ all -> 0x018b }
        r8 = (android.util.ArraySet) r8;	 Catch:{ all -> 0x018b }
        if (r8 == 0) goto L_0x0134;
    L_0x0092:
        r5 = r8.size();	 Catch:{ all -> 0x018b }
        r12 = 0;
        r7 = r6;
    L_0x0098:
        if (r12 >= r5) goto L_0x0133;
    L_0x009a:
        r4 = r8.valueAt(r12);	 Catch:{ all -> 0x01f8 }
        r4 = (com.android.server.AppOpsService.Callback) r4;	 Catch:{ all -> 0x01f8 }
        r9 = new android.util.ArraySet;	 Catch:{ all -> 0x01f8 }
        r9.<init>();	 Catch:{ all -> 0x01f8 }
        r0 = r20;
        java.util.Collections.addAll(r9, r0);	 Catch:{ all -> 0x01f8 }
        r6 = new android.util.ArrayMap;	 Catch:{ all -> 0x01f8 }
        r6.<init>();	 Catch:{ all -> 0x01f8 }
        r6.put(r4, r9);	 Catch:{ all -> 0x018b }
        r12 = r12 + 1;
        r7 = r6;
        goto L_0x0098;
    L_0x00b6:
        r0 = r21;
        r0 = r0.opModes;	 Catch:{ all -> 0x00e0 }
        r22 = r0;
        if (r22 != 0) goto L_0x00e3;
    L_0x00be:
        r0 = r30;
        if (r0 == r10) goto L_0x0079;
    L_0x00c2:
        r22 = new android.util.SparseIntArray;	 Catch:{ all -> 0x00e0 }
        r22.<init>();	 Catch:{ all -> 0x00e0 }
        r0 = r22;
        r1 = r21;
        r1.opModes = r0;	 Catch:{ all -> 0x00e0 }
        r0 = r21;
        r0 = r0.opModes;	 Catch:{ all -> 0x00e0 }
        r22 = r0;
        r0 = r22;
        r1 = r28;
        r2 = r30;
        r0.put(r1, r2);	 Catch:{ all -> 0x00e0 }
        r27.scheduleWriteLocked();	 Catch:{ all -> 0x00e0 }
        goto L_0x0079;
    L_0x00e0:
        r22 = move-exception;
        monitor-exit(r27);
        throw r22;
    L_0x00e3:
        r0 = r21;
        r0 = r0.opModes;	 Catch:{ all -> 0x00e0 }
        r22 = r0;
        r0 = r22;
        r1 = r28;
        r22 = r0.get(r1);	 Catch:{ all -> 0x00e0 }
        r0 = r22;
        r1 = r30;
        if (r0 != r1) goto L_0x00f9;
    L_0x00f7:
        monitor-exit(r27);
        return;
    L_0x00f9:
        r0 = r30;
        if (r0 != r10) goto L_0x0123;
    L_0x00fd:
        r0 = r21;
        r0 = r0.opModes;	 Catch:{ all -> 0x00e0 }
        r22 = r0;
        r0 = r22;
        r1 = r28;
        r0.delete(r1);	 Catch:{ all -> 0x00e0 }
        r0 = r21;
        r0 = r0.opModes;	 Catch:{ all -> 0x00e0 }
        r22 = r0;
        r22 = r22.size();	 Catch:{ all -> 0x00e0 }
        if (r22 > 0) goto L_0x011e;
    L_0x0116:
        r22 = 0;
        r0 = r22;
        r1 = r21;
        r1.opModes = r0;	 Catch:{ all -> 0x00e0 }
    L_0x011e:
        r27.scheduleWriteLocked();	 Catch:{ all -> 0x00e0 }
        goto L_0x0079;
    L_0x0123:
        r0 = r21;
        r0 = r0.opModes;	 Catch:{ all -> 0x00e0 }
        r22 = r0;
        r0 = r22;
        r1 = r28;
        r2 = r30;
        r0.put(r1, r2);	 Catch:{ all -> 0x00e0 }
        goto L_0x011e;
    L_0x0133:
        r6 = r7;
    L_0x0134:
        r22 = 0;
        r0 = r20;
        r0 = r0.length;	 Catch:{ all -> 0x018b }
        r23 = r0;
        r7 = r6;
    L_0x013c:
        r0 = r22;
        r1 = r23;
        if (r0 >= r1) goto L_0x0187;
    L_0x0142:
        r19 = r20[r22];	 Catch:{ all -> 0x01f8 }
        r0 = r27;
        r0 = r0.mPackageModeWatchers;	 Catch:{ all -> 0x01f8 }
        r24 = r0;
        r0 = r24;
        r1 = r19;
        r8 = r0.get(r1);	 Catch:{ all -> 0x01f8 }
        r8 = (android.util.ArraySet) r8;	 Catch:{ all -> 0x01f8 }
        if (r8 == 0) goto L_0x0182;
    L_0x0156:
        if (r7 != 0) goto L_0x01fb;
    L_0x0158:
        r6 = new android.util.ArrayMap;	 Catch:{ all -> 0x01f8 }
        r6.<init>();	 Catch:{ all -> 0x01f8 }
    L_0x015d:
        r5 = r8.size();	 Catch:{ all -> 0x018b }
        r12 = 0;
    L_0x0162:
        if (r12 >= r5) goto L_0x0183;
    L_0x0164:
        r4 = r8.valueAt(r12);	 Catch:{ all -> 0x018b }
        r4 = (com.android.server.AppOpsService.Callback) r4;	 Catch:{ all -> 0x018b }
        r9 = r6.get(r4);	 Catch:{ all -> 0x018b }
        r9 = (android.util.ArraySet) r9;	 Catch:{ all -> 0x018b }
        if (r9 != 0) goto L_0x017a;
    L_0x0172:
        r9 = new android.util.ArraySet;	 Catch:{ all -> 0x018b }
        r9.<init>();	 Catch:{ all -> 0x018b }
        r6.put(r4, r9);	 Catch:{ all -> 0x018b }
    L_0x017a:
        r0 = r19;
        r9.add(r0);	 Catch:{ all -> 0x018b }
        r12 = r12 + 1;
        goto L_0x0162;
    L_0x0182:
        r6 = r7;
    L_0x0183:
        r22 = r22 + 1;
        r7 = r6;
        goto L_0x013c;
    L_0x0187:
        monitor-exit(r27);
        if (r7 != 0) goto L_0x018e;
    L_0x018a:
        return;
    L_0x018b:
        r22 = move-exception;
    L_0x018c:
        monitor-exit(r27);
        throw r22;
    L_0x018e:
        r14 = android.os.Binder.clearCallingIdentity();
        r12 = 0;
    L_0x0193:
        r22 = r7.size();	 Catch:{ all -> 0x01ef }
        r0 = r22;
        if (r12 >= r0) goto L_0x01f4;
    L_0x019b:
        r4 = r7.keyAt(r12);	 Catch:{ all -> 0x01ef }
        r4 = (com.android.server.AppOpsService.Callback) r4;	 Catch:{ all -> 0x01ef }
        r18 = r7.valueAt(r12);	 Catch:{ all -> 0x01ef }
        r18 = (android.util.ArraySet) r18;	 Catch:{ all -> 0x01ef }
        if (r18 != 0) goto L_0x01bd;
    L_0x01a9:
        r0 = r4.mCallback;	 Catch:{ RemoteException -> 0x01e0 }
        r22 = r0;
        r23 = 0;
        r0 = r22;
        r1 = r28;
        r2 = r29;
        r3 = r23;
        r0.opChanged(r1, r2, r3);	 Catch:{ RemoteException -> 0x01e0 }
    L_0x01ba:
        r12 = r12 + 1;
        goto L_0x0193;
    L_0x01bd:
        r16 = r18.size();	 Catch:{ RemoteException -> 0x01e0 }
        r13 = 0;
    L_0x01c2:
        r0 = r16;
        if (r13 >= r0) goto L_0x01ba;
    L_0x01c6:
        r0 = r18;
        r17 = r0.valueAt(r13);	 Catch:{ RemoteException -> 0x01e0 }
        r17 = (java.lang.String) r17;	 Catch:{ RemoteException -> 0x01e0 }
        r0 = r4.mCallback;	 Catch:{ RemoteException -> 0x01e0 }
        r22 = r0;
        r0 = r22;
        r1 = r28;
        r2 = r29;
        r3 = r17;
        r0.opChanged(r1, r2, r3);	 Catch:{ RemoteException -> 0x01e0 }
        r13 = r13 + 1;
        goto L_0x01c2;
    L_0x01e0:
        r11 = move-exception;
        r22 = "AppOps";
        r23 = "Error dispatching op op change";
        r0 = r22;
        r1 = r23;
        android.util.Log.w(r0, r1, r11);	 Catch:{ all -> 0x01ef }
        goto L_0x01ba;
    L_0x01ef:
        r22 = move-exception;
        android.os.Binder.restoreCallingIdentity(r14);
        throw r22;
    L_0x01f4:
        android.os.Binder.restoreCallingIdentity(r14);
        return;
    L_0x01f8:
        r22 = move-exception;
        r6 = r7;
        goto L_0x018c;
    L_0x01fb:
        r6 = r7;
        goto L_0x015d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.AppOpsService.setUidMode(int, int, int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setMode(int r19, int r20, java.lang.String r21, int r22) {
        /*
        r18 = this;
        r13 = android.os.Binder.getCallingPid();
        r14 = android.os.Process.myPid();
        if (r13 == r14) goto L_0x001e;
    L_0x000a:
        r0 = r18;
        r13 = r0.mContext;
        r14 = "android.permission.UPDATE_APP_OPS_STATS";
        r15 = android.os.Binder.getCallingPid();
        r16 = android.os.Binder.getCallingUid();
        r17 = 0;
        r13.enforcePermission(r14, r15, r16, r17);
    L_0x001e:
        r18.verifyIncomingOp(r19);
        r10 = 0;
        r19 = android.app.AppOpsManager.opToSwitch(r19);
        monitor-enter(r18);
        r13 = 0;
        r0 = r18;
        r1 = r20;
        r12 = r0.getUidStateLocked(r1, r13);	 Catch:{ all -> 0x00af }
        r13 = 1;
        r0 = r18;
        r1 = r19;
        r2 = r20;
        r3 = r21;
        r7 = r0.getOpLocked(r1, r2, r3, r13);	 Catch:{ all -> 0x00af }
        if (r7 == 0) goto L_0x008d;
    L_0x003f:
        r13 = r7.mode;	 Catch:{ all -> 0x00af }
        r0 = r22;
        if (r13 == r0) goto L_0x008d;
    L_0x0045:
        r0 = r22;
        r7.mode = r0;	 Catch:{ all -> 0x00af }
        r0 = r18;
        r13 = r0.mOpModeWatchers;	 Catch:{ all -> 0x00af }
        r0 = r19;
        r4 = r13.get(r0);	 Catch:{ all -> 0x00af }
        r4 = (android.util.ArraySet) r4;	 Catch:{ all -> 0x00af }
        if (r4 == 0) goto L_0x00c4;
    L_0x0057:
        r11 = new java.util.ArrayList;	 Catch:{ all -> 0x00af }
        r11.<init>();	 Catch:{ all -> 0x00af }
        r11.addAll(r4);	 Catch:{ all -> 0x00bd }
    L_0x005f:
        r0 = r18;
        r13 = r0.mPackageModeWatchers;	 Catch:{ all -> 0x00bd }
        r0 = r21;
        r4 = r13.get(r0);	 Catch:{ all -> 0x00bd }
        r4 = (android.util.ArraySet) r4;	 Catch:{ all -> 0x00bd }
        if (r4 == 0) goto L_0x00c2;
    L_0x006d:
        if (r11 != 0) goto L_0x00c0;
    L_0x006f:
        r10 = new java.util.ArrayList;	 Catch:{ all -> 0x00bd }
        r10.<init>();	 Catch:{ all -> 0x00bd }
    L_0x0074:
        r10.addAll(r4);	 Catch:{ all -> 0x00af }
    L_0x0077:
        r13 = r7.op;	 Catch:{ all -> 0x00af }
        r13 = android.app.AppOpsManager.opToDefaultMode(r13);	 Catch:{ all -> 0x00af }
        r0 = r22;
        if (r0 != r13) goto L_0x008a;
    L_0x0081:
        r0 = r18;
        r1 = r20;
        r2 = r21;
        r0.pruneOp(r7, r1, r2);	 Catch:{ all -> 0x00af }
    L_0x008a:
        r18.scheduleFastWriteLocked();	 Catch:{ all -> 0x00af }
    L_0x008d:
        monitor-exit(r18);
        if (r10 == 0) goto L_0x00b5;
    L_0x0090:
        r8 = android.os.Binder.clearCallingIdentity();
        r6 = 0;
    L_0x0095:
        r13 = r10.size();	 Catch:{ all -> 0x00b6 }
        if (r6 >= r13) goto L_0x00b2;
    L_0x009b:
        r13 = r10.get(r6);	 Catch:{ RemoteException -> 0x00bb }
        r13 = (com.android.server.AppOpsService.Callback) r13;	 Catch:{ RemoteException -> 0x00bb }
        r13 = r13.mCallback;	 Catch:{ RemoteException -> 0x00bb }
        r0 = r19;
        r1 = r20;
        r2 = r21;
        r13.opChanged(r0, r1, r2);	 Catch:{ RemoteException -> 0x00bb }
    L_0x00ac:
        r6 = r6 + 1;
        goto L_0x0095;
    L_0x00af:
        r13 = move-exception;
    L_0x00b0:
        monitor-exit(r18);
        throw r13;
    L_0x00b2:
        android.os.Binder.restoreCallingIdentity(r8);
    L_0x00b5:
        return;
    L_0x00b6:
        r13 = move-exception;
        android.os.Binder.restoreCallingIdentity(r8);
        throw r13;
    L_0x00bb:
        r5 = move-exception;
        goto L_0x00ac;
    L_0x00bd:
        r13 = move-exception;
        r10 = r11;
        goto L_0x00b0;
    L_0x00c0:
        r10 = r11;
        goto L_0x0074;
    L_0x00c2:
        r10 = r11;
        goto L_0x0077;
    L_0x00c4:
        r11 = r10;
        goto L_0x005f;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.AppOpsService.setMode(int, int, java.lang.String, int):void");
    }

    private static HashMap<Callback, ArrayList<ChangeRec>> addCallbacks(HashMap<Callback, ArrayList<ChangeRec>> callbacks, int op, int uid, String packageName, ArraySet<Callback> cbs) {
        if (cbs == null) {
            return callbacks;
        }
        if (callbacks == null) {
            callbacks = new HashMap();
        }
        boolean duplicate = false;
        int N = cbs.size();
        for (int i = 0; i < N; i++) {
            Callback cb = (Callback) cbs.valueAt(i);
            ArrayList<ChangeRec> reports = (ArrayList) callbacks.get(cb);
            if (reports != null) {
                int reportCount = reports.size();
                for (int j = 0; j < reportCount; j++) {
                    ChangeRec report = (ChangeRec) reports.get(j);
                    if (report.op == op && report.pkg.equals(packageName)) {
                        duplicate = true;
                        break;
                    }
                }
            } else {
                reports = new ArrayList();
                callbacks.put(cb, reports);
            }
            if (!duplicate) {
                reports.add(new ChangeRec(op, uid, packageName));
            }
        }
        return callbacks;
    }

    public void resetAllModes(int reqUserId, String reqPackageName) {
        int callingPid = Binder.getCallingPid();
        int callingUid = Binder.getCallingUid();
        this.mContext.enforcePermission("android.permission.UPDATE_APP_OPS_STATS", callingPid, callingUid, null);
        reqUserId = ActivityManager.handleIncomingUser(callingPid, callingUid, reqUserId, true, true, "resetAllModes", null);
        int reqUid = -1;
        if (reqPackageName != null) {
            try {
                reqUid = AppGlobals.getPackageManager().getPackageUid(reqPackageName, 8192, reqUserId);
            } catch (RemoteException e) {
            }
        }
        HashMap<Callback, ArrayList<ChangeRec>> callbacks = null;
        synchronized (this) {
            int i;
            boolean changed = false;
            for (i = this.mUidStates.size() - 1; i >= 0; i--) {
                int j;
                UidState uidState = (UidState) this.mUidStates.valueAt(i);
                SparseIntArray opModes = uidState.opModes;
                if (opModes != null && (uidState.uid == reqUid || reqUid == -1)) {
                    for (j = opModes.size() - 1; j >= 0; j--) {
                        int code = opModes.keyAt(j);
                        if (AppOpsManager.opAllowsReset(code)) {
                            opModes.removeAt(j);
                            if (opModes.size() <= 0) {
                                uidState.opModes = null;
                            }
                            for (String packageName : getPackagesForUid(uidState.uid)) {
                                String packageName2;
                                callbacks = addCallbacks(addCallbacks(callbacks, code, uidState.uid, packageName2, (ArraySet) this.mOpModeWatchers.get(code)), code, uidState.uid, packageName2, (ArraySet) this.mPackageModeWatchers.get(packageName2));
                            }
                        }
                    }
                }
                if (uidState.pkgOps != null && (reqUserId == -1 || reqUserId == UserHandle.getUserId(uidState.uid))) {
                    Iterator<Entry<String, Ops>> it = uidState.pkgOps.entrySet().iterator();
                    while (it.hasNext()) {
                        Entry<String, Ops> ent = (Entry) it.next();
                        packageName2 = (String) ent.getKey();
                        if (reqPackageName == null || (reqPackageName.equals(packageName2) ^ 1) == 0) {
                            Ops pkgOps = (Ops) ent.getValue();
                            for (j = pkgOps.size() - 1; j >= 0; j--) {
                                Op curOp = (Op) pkgOps.valueAt(j);
                                if (AppOpsManager.opAllowsReset(curOp.op) && curOp.mode != AppOpsManager.opToDefaultMode(curOp.op)) {
                                    curOp.mode = AppOpsManager.opToDefaultMode(curOp.op);
                                    changed = true;
                                    callbacks = addCallbacks(addCallbacks(callbacks, curOp.op, curOp.uid, packageName2, (ArraySet) this.mOpModeWatchers.get(curOp.op)), curOp.op, curOp.uid, packageName2, (ArraySet) this.mPackageModeWatchers.get(packageName2));
                                    if (curOp.time == 0 && curOp.rejectTime == 0) {
                                        pkgOps.removeAt(j);
                                    }
                                }
                            }
                            if (pkgOps.size() == 0) {
                                it.remove();
                            }
                        }
                    }
                    if (uidState.isDefault()) {
                        this.mUidStates.remove(uidState.uid);
                    }
                }
            }
            if (changed) {
                scheduleFastWriteLocked();
            }
        }
        if (callbacks != null) {
            for (Entry<Callback, ArrayList<ChangeRec>> ent2 : callbacks.entrySet()) {
                Callback cb = (Callback) ent2.getKey();
                ArrayList<ChangeRec> reports = (ArrayList) ent2.getValue();
                for (i = 0; i < reports.size(); i++) {
                    ChangeRec rep = (ChangeRec) reports.get(i);
                    try {
                        cb.mCallback.opChanged(rep.op, rep.uid, rep.pkg);
                    } catch (RemoteException e2) {
                    }
                }
            }
        }
    }

    public void startWatchingMode(int op, String packageName, IAppOpsCallback callback) {
        if (callback != null) {
            synchronized (this) {
                ArraySet<Callback> cbs;
                if (op != -1) {
                    op = AppOpsManager.opToSwitch(op);
                }
                Callback cb = (Callback) this.mModeWatchers.get(callback.asBinder());
                if (cb == null) {
                    cb = new Callback(callback);
                    this.mModeWatchers.put(callback.asBinder(), cb);
                }
                if (op != -1) {
                    cbs = (ArraySet) this.mOpModeWatchers.get(op);
                    if (cbs == null) {
                        cbs = new ArraySet();
                        this.mOpModeWatchers.put(op, cbs);
                    }
                    cbs.add(cb);
                }
                if (packageName != null) {
                    cbs = (ArraySet) this.mPackageModeWatchers.get(packageName);
                    if (cbs == null) {
                        cbs = new ArraySet();
                        this.mPackageModeWatchers.put(packageName, cbs);
                    }
                    cbs.add(cb);
                }
            }
        }
    }

    public void stopWatchingMode(IAppOpsCallback callback) {
        if (callback != null) {
            synchronized (this) {
                Callback cb = (Callback) this.mModeWatchers.remove(callback.asBinder());
                if (cb != null) {
                    int i;
                    ArraySet<Callback> cbs;
                    cb.unlinkToDeath();
                    for (i = this.mOpModeWatchers.size() - 1; i >= 0; i--) {
                        cbs = (ArraySet) this.mOpModeWatchers.valueAt(i);
                        cbs.remove(cb);
                        if (cbs.size() <= 0) {
                            this.mOpModeWatchers.removeAt(i);
                        }
                    }
                    for (i = this.mPackageModeWatchers.size() - 1; i >= 0; i--) {
                        cbs = (ArraySet) this.mPackageModeWatchers.valueAt(i);
                        cbs.remove(cb);
                        if (cbs.size() <= 0) {
                            this.mPackageModeWatchers.removeAt(i);
                        }
                    }
                }
            }
        }
    }

    public IBinder getToken(IBinder clientToken) {
        ClientState cs;
        synchronized (this) {
            cs = (ClientState) this.mClients.get(clientToken);
            if (cs == null) {
                cs = new ClientState(clientToken);
                this.mClients.put(clientToken, cs);
            }
        }
        return cs;
    }

    public int checkOperation(int code, int uid, String packageName) {
        verifyIncomingUid(uid);
        verifyIncomingOp(code);
        String resolvedPackageName = resolvePackageName(uid, packageName);
        if (resolvedPackageName == null) {
            return 1;
        }
        synchronized (this) {
            if (isOpRestrictedLocked(uid, code, resolvedPackageName)) {
                return 1;
            }
            code = AppOpsManager.opToSwitch(code);
            UidState uidState = getUidStateLocked(uid, false);
            int opToDefaultMode;
            if (uidState == null || uidState.opModes == null || uidState.opModes.indexOfKey(code) < 0) {
                Op op = getOpLocked(code, uid, resolvedPackageName, false);
                if (op == null) {
                    opToDefaultMode = AppOpsManager.opToDefaultMode(code);
                    return opToDefaultMode;
                }
                opToDefaultMode = op.mode;
                return opToDefaultMode;
            }
            opToDefaultMode = uidState.opModes.get(code);
            return opToDefaultMode;
        }
    }

    public int checkAudioOperation(int code, int usage, int uid, String packageName) {
        boolean isPackageSuspendedForUser;
        try {
            isPackageSuspendedForUser = isPackageSuspendedForUser(packageName, uid);
        } catch (IllegalArgumentException e) {
            isPackageSuspendedForUser = false;
        }
        if (isPackageSuspendedForUser) {
            Log.i(TAG, "Audio disabled for suspended package=" + packageName + " for uid=" + uid);
            return 1;
        }
        synchronized (this) {
            int mode = checkRestrictionLocked(code, usage, uid, packageName);
            if (mode != 0) {
                return mode;
            }
            return checkOperation(code, uid, packageName);
        }
    }

    private boolean isPackageSuspendedForUser(String pkg, int uid) {
        try {
            return AppGlobals.getPackageManager().isPackageSuspendedForUser(pkg, UserHandle.getUserId(uid));
        } catch (RemoteException e) {
            throw new SecurityException("Could not talk to package manager service");
        }
    }

    private int checkRestrictionLocked(int code, int usage, int uid, String packageName) {
        SparseArray<Restriction> usageRestrictions = (SparseArray) this.mAudioRestrictions.get(code);
        if (usageRestrictions != null) {
            Restriction r = (Restriction) usageRestrictions.get(usage);
            if (!(r == null || (r.exceptionPackages.contains(packageName) ^ 1) == 0)) {
                return r.mode;
            }
        }
        return 0;
    }

    public void setAudioRestriction(int code, int usage, int uid, int mode, String[] exceptionPackages) {
        verifyIncomingUid(uid);
        verifyIncomingOp(code);
        synchronized (this) {
            SparseArray<Restriction> usageRestrictions = (SparseArray) this.mAudioRestrictions.get(code);
            if (usageRestrictions == null) {
                usageRestrictions = new SparseArray();
                this.mAudioRestrictions.put(code, usageRestrictions);
            }
            usageRestrictions.remove(usage);
            if (mode != 0) {
                Restriction r = new Restriction();
                r.mode = mode;
                if (exceptionPackages != null) {
                    r.exceptionPackages = new ArraySet(N);
                    for (String pkg : exceptionPackages) {
                        if (pkg != null) {
                            r.exceptionPackages.add(pkg.trim());
                        }
                    }
                }
                usageRestrictions.put(usage, r);
            }
        }
        notifyWatchersOfChange(code);
    }

    public int checkPackage(int uid, String packageName) {
        Preconditions.checkNotNull(packageName);
        synchronized (this) {
            if (getOpsRawLocked(uid, packageName, true) != null) {
                return 0;
            }
            return 2;
        }
    }

    public int noteProxyOperation(int code, String proxyPackageName, int proxiedUid, String proxiedPackageName) {
        verifyIncomingOp(code);
        int proxyUid = Binder.getCallingUid();
        String resolveProxyPackageName = resolvePackageName(proxyUid, proxyPackageName);
        if (resolveProxyPackageName == null) {
            return 1;
        }
        int proxyMode = noteOperationUnchecked(code, proxyUid, resolveProxyPackageName, -1, null);
        if (proxyMode != 0 || Binder.getCallingUid() == proxiedUid) {
            return proxyMode;
        }
        String resolveProxiedPackageName = resolvePackageName(proxiedUid, proxiedPackageName);
        if (resolveProxiedPackageName == null) {
            return 1;
        }
        return noteOperationUnchecked(code, proxiedUid, resolveProxiedPackageName, proxyMode, resolveProxyPackageName);
    }

    public int noteOperation(int code, int uid, String packageName) {
        verifyIncomingUid(uid);
        verifyIncomingOp(code);
        String resolvedPackageName = resolvePackageName(uid, packageName);
        if (resolvedPackageName == null) {
            return 1;
        }
        return noteOperationUnchecked(code, uid, resolvedPackageName, 0, null);
    }

    private int noteOperationUnchecked(int code, int uid, String packageName, int proxyUid, String proxyPackageName) {
        synchronized (this) {
            Ops ops = getOpsRawLocked(uid, packageName, true);
            if (ops == null) {
                return 2;
            }
            Op op = getOpLocked(ops, code, true);
            if (isOpRestrictedLocked(uid, code, packageName)) {
                return 1;
            }
            if (op.duration == -1) {
                Slog.w(TAG, "Noting op not finished: uid " + uid + " pkg " + packageName + " code " + code + " time=" + op.time + " duration=" + op.duration);
            }
            op.duration = 0;
            int switchCode = AppOpsManager.opToSwitch(code);
            UidState uidState = ops.uidState;
            if (uidState.opModes == null || uidState.opModes.indexOfKey(switchCode) < 0) {
                Op switchOp = switchCode != code ? getOpLocked(ops, switchCode, true) : op;
                if (switchOp.mode != 0) {
                    op.rejectTime = System.currentTimeMillis();
                    int i = switchOp.mode;
                    return i;
                }
            }
            int uidMode = uidState.opModes.get(switchCode);
            if (uidMode != 0) {
                op.rejectTime = System.currentTimeMillis();
                return uidMode;
            }
            op.time = System.currentTimeMillis();
            op.rejectTime = 0;
            op.proxyUid = proxyUid;
            op.proxyPackageName = proxyPackageName;
            return 0;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int startOperation(android.os.IBinder r12, int r13, int r14, java.lang.String r15) {
        /*
        r11 = this;
        r10 = 0;
        r9 = 1;
        r11.verifyIncomingUid(r14);
        r11.verifyIncomingOp(r13);
        r3 = resolvePackageName(r14, r15);
        if (r3 != 0) goto L_0x000f;
    L_0x000e:
        return r9;
    L_0x000f:
        r0 = r12;
        r0 = (com.android.server.AppOpsService.ClientState) r0;
        monitor-enter(r11);
        r8 = 1;
        r2 = r11.getOpsRawLocked(r14, r3, r8);	 Catch:{ all -> 0x007d }
        if (r2 != 0) goto L_0x001d;
    L_0x001a:
        r8 = 2;
        monitor-exit(r11);
        return r8;
    L_0x001d:
        r8 = 1;
        r1 = r11.getOpLocked(r2, r13, r8);	 Catch:{ all -> 0x007d }
        r8 = r11.isOpRestrictedLocked(r14, r13, r3);	 Catch:{ all -> 0x007d }
        if (r8 == 0) goto L_0x002a;
    L_0x0028:
        monitor-exit(r11);
        return r9;
    L_0x002a:
        r4 = android.app.AppOpsManager.opToSwitch(r13);	 Catch:{ all -> 0x007d }
        r7 = r2.uidState;	 Catch:{ all -> 0x007d }
        r8 = r7.opModes;	 Catch:{ all -> 0x007d }
        if (r8 == 0) goto L_0x0044;
    L_0x0034:
        r8 = r7.opModes;	 Catch:{ all -> 0x007d }
        r6 = r8.get(r4);	 Catch:{ all -> 0x007d }
        if (r6 == 0) goto L_0x0044;
    L_0x003c:
        r8 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x007d }
        r1.rejectTime = r8;	 Catch:{ all -> 0x007d }
        monitor-exit(r11);
        return r6;
    L_0x0044:
        if (r4 == r13) goto L_0x0059;
    L_0x0046:
        r8 = 1;
        r5 = r11.getOpLocked(r2, r4, r8);	 Catch:{ all -> 0x007d }
    L_0x004b:
        r8 = r5.mode;	 Catch:{ all -> 0x007d }
        if (r8 == 0) goto L_0x005b;
    L_0x004f:
        r8 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x007d }
        r1.rejectTime = r8;	 Catch:{ all -> 0x007d }
        r8 = r5.mode;	 Catch:{ all -> 0x007d }
        monitor-exit(r11);
        return r8;
    L_0x0059:
        r5 = r1;
        goto L_0x004b;
    L_0x005b:
        r8 = r1.nesting;	 Catch:{ all -> 0x007d }
        if (r8 != 0) goto L_0x006c;
    L_0x005f:
        r8 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x007d }
        r1.time = r8;	 Catch:{ all -> 0x007d }
        r8 = 0;
        r1.rejectTime = r8;	 Catch:{ all -> 0x007d }
        r8 = -1;
        r1.duration = r8;	 Catch:{ all -> 0x007d }
    L_0x006c:
        r8 = r1.nesting;	 Catch:{ all -> 0x007d }
        r8 = r8 + 1;
        r1.nesting = r8;	 Catch:{ all -> 0x007d }
        r8 = r0.mStartedOps;	 Catch:{ all -> 0x007d }
        if (r8 == 0) goto L_0x007b;
    L_0x0076:
        r8 = r0.mStartedOps;	 Catch:{ all -> 0x007d }
        r8.add(r1);	 Catch:{ all -> 0x007d }
    L_0x007b:
        monitor-exit(r11);
        return r10;
    L_0x007d:
        r8 = move-exception;
        monitor-exit(r11);
        throw r8;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.AppOpsService.startOperation(android.os.IBinder, int, int, java.lang.String):int");
    }

    public void finishOperation(IBinder token, int code, int uid, String packageName) {
        verifyIncomingUid(uid);
        verifyIncomingOp(code);
        String resolvedPackageName = resolvePackageName(uid, packageName);
        if (resolvedPackageName != null && (token instanceof ClientState)) {
            ClientState client = (ClientState) token;
            synchronized (this) {
                Op op = getOpLocked(code, uid, resolvedPackageName, true);
                if (op == null) {
                } else if (client.mStartedOps == null || client.mStartedOps.remove(op)) {
                    finishOperationLocked(op);
                } else {
                    throw new IllegalStateException("Operation not started: uid" + op.uid + " pkg=" + op.packageName + " op=" + op.op);
                }
            }
        }
    }

    public int permissionToOpCode(String permission) {
        if (permission == null) {
            return -1;
        }
        return AppOpsManager.permissionToOpCode(permission);
    }

    void finishOperationLocked(Op op) {
        if (op.nesting <= 1) {
            if (op.nesting == 1) {
                op.duration = (int) (System.currentTimeMillis() - op.time);
                op.time += (long) op.duration;
            } else {
                Slog.w(TAG, "Finishing op nesting under-run: uid " + op.uid + " pkg " + op.packageName + " code " + op.op + " time=" + op.time + " duration=" + op.duration + " nesting=" + op.nesting);
            }
            op.nesting = 0;
            return;
        }
        op.nesting--;
    }

    private void verifyIncomingUid(int uid) {
        if (uid != Binder.getCallingUid() && Binder.getCallingPid() != Process.myPid()) {
            this.mContext.enforcePermission("android.permission.UPDATE_APP_OPS_STATS", Binder.getCallingPid(), Binder.getCallingUid(), null);
        }
    }

    private void verifyIncomingOp(int op) {
        if (op < 0 || op >= 70) {
            throw new IllegalArgumentException("Bad operation #" + op);
        }
    }

    private UidState getUidStateLocked(int uid, boolean edit) {
        UidState uidState = (UidState) this.mUidStates.get(uid);
        if (uidState == null) {
            if (!edit) {
                return null;
            }
            uidState = new UidState(uid);
            this.mUidStates.put(uid, uidState);
        }
        return uidState;
    }

    private Ops getOpsRawLocked(int uid, String packageName, boolean edit) {
        UidState uidState = getUidStateLocked(uid, edit);
        if (uidState == null) {
            return null;
        }
        if (uidState.pkgOps == null) {
            if (!edit) {
                return null;
            }
            uidState.pkgOps = new ArrayMap();
        }
        Ops ops = (Ops) uidState.pkgOps.get(packageName);
        if (ops == null) {
            if (!edit) {
                return null;
            }
            boolean isPrivileged = false;
            if (uid != 0) {
                long ident = Binder.clearCallingIdentity();
                int pkgUid = -1;
                try {
                    ApplicationInfo appInfo = ActivityThread.getPackageManager().getApplicationInfo(packageName, 268435456, UserHandle.getUserId(uid));
                    if (appInfo != null) {
                        pkgUid = appInfo.uid;
                        isPrivileged = (appInfo.privateFlags & 8) != 0;
                    } else if ("media".equals(packageName)) {
                        pkgUid = 1013;
                        isPrivileged = false;
                    } else if ("audioserver".equals(packageName)) {
                        pkgUid = 1041;
                        isPrivileged = false;
                    } else if ("cameraserver".equals(packageName)) {
                        pkgUid = 1047;
                        isPrivileged = false;
                    }
                } catch (RemoteException e) {
                    Slog.w(TAG, "Could not contact PackageManager", e);
                } catch (Throwable th) {
                    Binder.restoreCallingIdentity(ident);
                }
                if (pkgUid != uid) {
                    RuntimeException ex = new RuntimeException("here");
                    ex.fillInStackTrace();
                    Slog.w(TAG, "Bad call: specified package " + packageName + " under uid " + uid + " but it is really " + pkgUid, ex);
                    Binder.restoreCallingIdentity(ident);
                    return null;
                }
                Binder.restoreCallingIdentity(ident);
            }
            ops = new Ops(packageName, uidState, isPrivileged);
            uidState.pkgOps.put(packageName, ops);
        }
        return ops;
    }

    private void scheduleWriteLocked() {
        if (!this.mWriteScheduled) {
            this.mWriteScheduled = true;
            this.mHandler.postDelayed(this.mWriteRunner, 1800000);
        }
    }

    private void scheduleFastWriteLocked() {
        if (!this.mFastWriteScheduled) {
            this.mWriteScheduled = true;
            this.mFastWriteScheduled = true;
            this.mHandler.removeCallbacks(this.mWriteRunner);
            this.mHandler.postDelayed(this.mWriteRunner, JobStatus.DEFAULT_TRIGGER_UPDATE_DELAY);
        }
    }

    private Op getOpLocked(int code, int uid, String packageName, boolean edit) {
        Ops ops = getOpsRawLocked(uid, packageName, edit);
        if (ops == null) {
            return null;
        }
        return getOpLocked(ops, code, edit);
    }

    private Op getOpLocked(Ops ops, int code, boolean edit) {
        Op op = (Op) ops.get(code);
        if (op == null) {
            if (!edit) {
                return null;
            }
            op = new Op(ops.uidState.uid, ops.packageName, code);
            ops.put(code, op);
        }
        if (edit) {
            scheduleWriteLocked();
        }
        return op;
    }

    private boolean isOpRestrictedLocked(int uid, int code, String packageName) {
        int userHandle = UserHandle.getUserId(uid);
        int restrictionSetCount = this.mOpUserRestrictions.size();
        for (int i = 0; i < restrictionSetCount; i++) {
            if (((ClientRestrictionState) this.mOpUserRestrictions.valueAt(i)).hasRestriction(code, packageName, userHandle)) {
                if (AppOpsManager.opAllowSystemBypassRestriction(code)) {
                    synchronized (this) {
                        Ops ops = getOpsRawLocked(uid, packageName, true);
                        if (ops == null || !ops.isPrivileged) {
                        } else {
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    void readState() {
        synchronized (this.mFile) {
            synchronized (this) {
                try {
                    FileInputStream stream = this.mFile.openRead();
                    this.mUidStates.clear();
                    try {
                        int type;
                        XmlPullParser parser = Xml.newPullParser();
                        parser.setInput(stream, StandardCharsets.UTF_8.name());
                        do {
                            type = parser.next();
                            if (type == 2) {
                                break;
                            }
                        } while (type != 1);
                        if (type != 2) {
                            throw new IllegalStateException("no start tag found");
                        }
                        int outerDepth = parser.getDepth();
                        while (true) {
                            type = parser.next();
                            if (type != 1 && (type != 3 || parser.getDepth() > outerDepth)) {
                                if (!(type == 3 || type == 4)) {
                                    String tagName = parser.getName();
                                    if (tagName.equals("pkg")) {
                                        readPackage(parser);
                                    } else if (tagName.equals("uid")) {
                                        readUidOps(parser);
                                    } else {
                                        Slog.w(TAG, "Unknown element under <app-ops>: " + parser.getName());
                                        XmlUtils.skipCurrentTag(parser);
                                    }
                                }
                            }
                        }
                        if (!true) {
                            this.mUidStates.clear();
                        }
                        try {
                            stream.close();
                        } catch (IOException e) {
                        }
                    } catch (IllegalStateException e2) {
                        Slog.w(TAG, "Failed parsing " + e2);
                        if (null == null) {
                            this.mUidStates.clear();
                        }
                        try {
                            stream.close();
                        } catch (IOException e3) {
                        }
                    } catch (NullPointerException e4) {
                        Slog.w(TAG, "Failed parsing " + e4);
                        if (null == null) {
                            this.mUidStates.clear();
                        }
                        try {
                            stream.close();
                        } catch (IOException e5) {
                        }
                    } catch (NumberFormatException e6) {
                        Slog.w(TAG, "Failed parsing " + e6);
                        if (null == null) {
                            this.mUidStates.clear();
                        }
                        try {
                            stream.close();
                        } catch (IOException e7) {
                        }
                    } catch (XmlPullParserException e8) {
                        Slog.w(TAG, "Failed parsing " + e8);
                        if (null == null) {
                            this.mUidStates.clear();
                        }
                        try {
                            stream.close();
                        } catch (IOException e9) {
                        }
                    } catch (IOException e10) {
                        Slog.w(TAG, "Failed parsing " + e10);
                        if (null == null) {
                            this.mUidStates.clear();
                        }
                        try {
                            stream.close();
                        } catch (IOException e11) {
                        }
                    } catch (IndexOutOfBoundsException e12) {
                        Slog.w(TAG, "Failed parsing " + e12);
                        if (null == null) {
                            this.mUidStates.clear();
                        }
                        try {
                            stream.close();
                        } catch (IOException e13) {
                        }
                    } catch (Throwable th) {
                        if (null == null) {
                            this.mUidStates.clear();
                        }
                        try {
                            stream.close();
                        } catch (IOException e14) {
                        }
                    }
                } catch (FileNotFoundException e15) {
                    Slog.i(TAG, "No existing app ops " + this.mFile.getBaseFile() + "; starting empty");
                    return;
                }
            }
        }
        return;
    }

    void readUidOps(XmlPullParser parser) throws NumberFormatException, XmlPullParserException, IOException {
        int uid = Integer.parseInt(parser.getAttributeValue(null, "n"));
        int outerDepth = parser.getDepth();
        while (true) {
            int type = parser.next();
            if (type == 1) {
                return;
            }
            if (type == 3 && parser.getDepth() <= outerDepth) {
                return;
            }
            if (!(type == 3 || type == 4)) {
                if (parser.getName().equals("op")) {
                    int code = Integer.parseInt(parser.getAttributeValue(null, "n"));
                    int mode = Integer.parseInt(parser.getAttributeValue(null, "m"));
                    UidState uidState = getUidStateLocked(uid, true);
                    if (uidState.opModes == null) {
                        uidState.opModes = new SparseIntArray();
                    }
                    uidState.opModes.put(code, mode);
                } else {
                    Slog.w(TAG, "Unknown element under <uid-ops>: " + parser.getName());
                    XmlUtils.skipCurrentTag(parser);
                }
            }
        }
    }

    void readPackage(XmlPullParser parser) throws NumberFormatException, XmlPullParserException, IOException {
        String pkgName = parser.getAttributeValue(null, "n");
        int outerDepth = parser.getDepth();
        while (true) {
            int type = parser.next();
            if (type == 1) {
                return;
            }
            if (type == 3 && parser.getDepth() <= outerDepth) {
                return;
            }
            if (!(type == 3 || type == 4)) {
                if (parser.getName().equals("uid")) {
                    readUid(parser, pkgName);
                } else {
                    Slog.w(TAG, "Unknown element under <pkg>: " + parser.getName());
                    XmlUtils.skipCurrentTag(parser);
                }
            }
        }
    }

    void readUid(XmlPullParser parser, String pkgName) throws NumberFormatException, XmlPullParserException, IOException {
        int uid = Integer.parseInt(parser.getAttributeValue(null, "n"));
        String isPrivilegedString = parser.getAttributeValue(null, "p");
        boolean isPrivileged = false;
        if (isPrivilegedString == null) {
            try {
                if (ActivityThread.getPackageManager() != null) {
                    ApplicationInfo appInfo = ActivityThread.getPackageManager().getApplicationInfo(pkgName, 0, UserHandle.getUserId(uid));
                    if (appInfo != null) {
                        isPrivileged = (appInfo.privateFlags & 8) != 0;
                    }
                } else {
                    return;
                }
            } catch (RemoteException e) {
                Slog.w(TAG, "Could not contact PackageManager", e);
            }
        } else {
            isPrivileged = Boolean.parseBoolean(isPrivilegedString);
        }
        int outerDepth = parser.getDepth();
        while (true) {
            int type = parser.next();
            if (type != 1 && (type != 3 || parser.getDepth() > outerDepth)) {
                if (!(type == 3 || type == 4)) {
                    if (parser.getName().equals("op")) {
                        Op op = new Op(uid, pkgName, Integer.parseInt(parser.getAttributeValue(null, "n")));
                        String mode = parser.getAttributeValue(null, "m");
                        if (mode != null) {
                            op.mode = Integer.parseInt(mode);
                        }
                        String time = parser.getAttributeValue(null, "t");
                        if (time != null) {
                            op.time = Long.parseLong(time);
                        }
                        time = parser.getAttributeValue(null, "r");
                        if (time != null) {
                            op.rejectTime = Long.parseLong(time);
                        }
                        String dur = parser.getAttributeValue(null, "d");
                        if (dur != null) {
                            op.duration = Integer.parseInt(dur);
                        }
                        String proxyUid = parser.getAttributeValue(null, "pu");
                        if (proxyUid != null) {
                            op.proxyUid = Integer.parseInt(proxyUid);
                        }
                        String proxyPackageName = parser.getAttributeValue(null, "pp");
                        if (proxyPackageName != null) {
                            op.proxyPackageName = proxyPackageName;
                        }
                        UidState uidState = getUidStateLocked(uid, true);
                        if (uidState.pkgOps == null) {
                            uidState.pkgOps = new ArrayMap();
                        }
                        Ops ops = (Ops) uidState.pkgOps.get(pkgName);
                        if (ops == null) {
                            ops = new Ops(pkgName, uidState, isPrivileged);
                            uidState.pkgOps.put(pkgName, ops);
                        }
                        ops.put(op.op, op);
                    } else {
                        Slog.w(TAG, "Unknown element under <pkg>: " + parser.getName());
                        XmlUtils.skipCurrentTag(parser);
                    }
                }
            }
        }
    }

    void writeState() {
        synchronized (this.mFile) {
            List<PackageOps> allOps = getPackagesForOps(null);
            try {
                int i;
                int j;
                OutputStream stream = this.mFile.startWrite();
                XmlSerializer out = new FastXmlSerializer();
                out.setOutput(stream, StandardCharsets.UTF_8.name());
                out.startDocument(null, Boolean.valueOf(true));
                out.startTag(null, "app-ops");
                int uidStateCount = this.mUidStates.size();
                for (i = 0; i < uidStateCount; i++) {
                    UidState uidState = (UidState) this.mUidStates.valueAt(i);
                    if (uidState.opModes != null && uidState.opModes.size() > 0) {
                        out.startTag(null, "uid");
                        out.attribute(null, "n", Integer.toString(uidState.uid));
                        SparseIntArray uidOpModes = uidState.opModes;
                        int opCount = uidOpModes.size();
                        for (j = 0; j < opCount; j++) {
                            int op = uidOpModes.keyAt(j);
                            int mode = uidOpModes.valueAt(j);
                            out.startTag(null, "op");
                            out.attribute(null, "n", Integer.toString(op));
                            out.attribute(null, "m", Integer.toString(mode));
                            out.endTag(null, "op");
                        }
                        try {
                            out.endTag(null, "uid");
                        } catch (IOException e) {
                            Slog.w(TAG, "Failed to write state, restoring backup.", e);
                            this.mFile.failWrite(stream);
                        }
                    }
                }
                if (allOps != null) {
                    Object lastPkg = null;
                    for (i = 0; i < allOps.size(); i++) {
                        PackageOps pkg = (PackageOps) allOps.get(i);
                        if (!pkg.getPackageName().equals(lastPkg)) {
                            if (lastPkg != null) {
                                out.endTag(null, "pkg");
                            }
                            lastPkg = pkg.getPackageName();
                            out.startTag(null, "pkg");
                            out.attribute(null, "n", lastPkg);
                        }
                        out.startTag(null, "uid");
                        out.attribute(null, "n", Integer.toString(pkg.getUid()));
                        synchronized (this) {
                            Ops ops = getOpsRawLocked(pkg.getUid(), pkg.getPackageName(), false);
                            if (ops != null) {
                                out.attribute(null, "p", Boolean.toString(ops.isPrivileged));
                            } else {
                                out.attribute(null, "p", Boolean.toString(false));
                            }
                        }
                        List<OpEntry> ops2 = pkg.getOps();
                        for (j = 0; j < ops2.size(); j++) {
                            OpEntry op2 = (OpEntry) ops2.get(j);
                            out.startTag(null, "op");
                            out.attribute(null, "n", Integer.toString(op2.getOp()));
                            if (op2.getMode() != AppOpsManager.opToDefaultMode(op2.getOp())) {
                                out.attribute(null, "m", Integer.toString(op2.getMode()));
                            }
                            long time = op2.getTime();
                            if (time != 0) {
                                out.attribute(null, "t", Long.toString(time));
                            }
                            time = op2.getRejectTime();
                            if (time != 0) {
                                out.attribute(null, "r", Long.toString(time));
                            }
                            int dur = op2.getDuration();
                            if (dur != 0) {
                                out.attribute(null, "d", Integer.toString(dur));
                            }
                            int proxyUid = op2.getProxyUid();
                            if (proxyUid != -1) {
                                out.attribute(null, "pu", Integer.toString(proxyUid));
                            }
                            String proxyPackageName = op2.getProxyPackageName();
                            if (proxyPackageName != null) {
                                out.attribute(null, "pp", proxyPackageName);
                            }
                            out.endTag(null, "op");
                        }
                        out.endTag(null, "uid");
                    }
                    if (lastPkg != null) {
                        out.endTag(null, "pkg");
                    }
                }
                out.endTag(null, "app-ops");
                out.endDocument();
                this.mFile.finishWrite(stream);
            } catch (IOException e2) {
                Slog.w(TAG, "Failed to write state: " + e2);
                return;
            }
        }
        return;
    }

    public void onShellCommand(FileDescriptor in, FileDescriptor out, FileDescriptor err, String[] args, ShellCallback callback, ResultReceiver resultReceiver) {
        new Shell(this, this).exec(this, in, out, err, args, callback, resultReceiver);
    }

    static void dumpCommandHelp(PrintWriter pw) {
        pw.println("AppOps service (appops) commands:");
        pw.println("  help");
        pw.println("    Print this help text.");
        pw.println("  set [--user <USER_ID>] <PACKAGE | UID> <OP> <MODE>");
        pw.println("    Set the mode for a particular application and operation.");
        pw.println("  get [--user <USER_ID>] <PACKAGE | UID> [<OP>]");
        pw.println("    Return the mode for a particular application and optional operation.");
        pw.println("  query-op [--user <USER_ID>] <OP> [<MODE>]");
        pw.println("    Print all packages that currently have the given op in the given mode.");
        pw.println("  reset [--user <USER_ID>] [<PACKAGE>]");
        pw.println("    Reset the given application or all applications to default modes.");
        pw.println("  write-settings");
        pw.println("    Immediately write pending changes to storage.");
        pw.println("  read-settings");
        pw.println("    Read the last written settings, replacing current state in RAM.");
        pw.println("  options:");
        pw.println("    <PACKAGE> an Android package name.");
        pw.println("    <OP>      an AppOps operation.");
        pw.println("    <MODE>    one of allow, ignore, deny, or default");
        pw.println("    <USER_ID> the user id under which the package is installed. If --user is not");
        pw.println("              specified, the current user is assumed.");
    }

    static int onShellCommand(Shell shell, String cmd) {
        long token;
        if (cmd == null) {
            return shell.handleDefaultCommands(cmd);
        }
        PrintWriter pw = shell.getOutPrintWriter();
        PrintWriter err = shell.getErrPrintWriter();
        try {
            int res;
            if (cmd.equals("set")) {
                res = shell.parseUserPackageOp(true, err);
                if (res < 0) {
                    return res;
                }
                String modeStr = shell.getNextArg();
                if (modeStr == null) {
                    err.println("Error: Mode not specified.");
                    return -1;
                }
                int mode = shell.strModeToMode(modeStr, err);
                if (mode < 0) {
                    return -1;
                }
                if (shell.packageName != null) {
                    shell.mInterface.setMode(shell.op, shell.packageUid, shell.packageName, mode);
                } else {
                    shell.mInterface.setUidMode(shell.op, shell.nonpackageUid, mode);
                }
                return 0;
            } else if (cmd.equals("get")) {
                res = shell.parseUserPackageOp(false, err);
                if (res < 0) {
                    return res;
                }
                if (shell.packageName != null) {
                    ops = shell.mInterface.getOpsForPackage(shell.packageUid, shell.packageName, shell.op != -1 ? new int[]{shell.op} : null);
                } else {
                    ops = shell.mInterface.getUidOps(shell.nonpackageUid, shell.op != -1 ? new int[]{shell.op} : null);
                }
                if (ops == null || ops.size() <= 0) {
                    pw.println("No operations.");
                    return 0;
                }
                long now = System.currentTimeMillis();
                for (i = 0; i < ops.size(); i++) {
                    entries = ((PackageOps) ops.get(i)).getOps();
                    for (j = 0; j < entries.size(); j++) {
                        ent = (OpEntry) entries.get(j);
                        pw.print(AppOpsManager.opToName(ent.getOp()));
                        pw.print(": ");
                        switch (ent.getMode()) {
                            case 0:
                                pw.print("allow");
                                break;
                            case 1:
                                pw.print("ignore");
                                break;
                            case 2:
                                pw.print("deny");
                                break;
                            case 3:
                                pw.print("default");
                                break;
                            default:
                                pw.print("mode=");
                                pw.print(ent.getMode());
                                break;
                        }
                        if (ent.getTime() != 0) {
                            pw.print("; time=");
                            TimeUtils.formatDuration(now - ent.getTime(), pw);
                            pw.print(" ago");
                        }
                        if (ent.getRejectTime() != 0) {
                            pw.print("; rejectTime=");
                            TimeUtils.formatDuration(now - ent.getRejectTime(), pw);
                            pw.print(" ago");
                        }
                        if (ent.getDuration() == -1) {
                            pw.print(" (running)");
                        } else if (ent.getDuration() != 0) {
                            pw.print("; duration=");
                            TimeUtils.formatDuration((long) ent.getDuration(), pw);
                        }
                        pw.println();
                    }
                }
                return 0;
            } else if (cmd.equals("query-op")) {
                res = shell.parseUserOpMode(1, err);
                if (res < 0) {
                    return res;
                }
                ops = shell.mInterface.getPackagesForOps(new int[]{shell.op});
                if (ops == null || ops.size() <= 0) {
                    pw.println("No operations.");
                    return 0;
                }
                for (i = 0; i < ops.size(); i++) {
                    PackageOps pkg = (PackageOps) ops.get(i);
                    boolean hasMatch = false;
                    entries = ((PackageOps) ops.get(i)).getOps();
                    for (j = 0; j < entries.size(); j++) {
                        ent = (OpEntry) entries.get(j);
                        if (ent.getOp() == shell.op && ent.getMode() == shell.mode) {
                            hasMatch = true;
                            break;
                        }
                    }
                    if (hasMatch) {
                        pw.println(pkg.getPackageName());
                    }
                }
                return 0;
            } else if (cmd.equals("reset")) {
                String packageName = null;
                int userId = -2;
                while (true) {
                    String argument = shell.getNextArg();
                    if (argument == null) {
                        break;
                    } else if ("--user".equals(argument)) {
                        userId = UserHandle.parseUserArg(shell.getNextArgRequired());
                    } else if (packageName == null) {
                        packageName = argument;
                    } else {
                        err.println("Error: Unsupported argument: " + argument);
                        return -1;
                    }
                }
                if (userId == -2) {
                    userId = ActivityManager.getCurrentUser();
                }
                shell.mInterface.resetAllModes(userId, packageName);
                pw.print("Reset all modes for: ");
                if (userId == -1) {
                    pw.print("all users");
                } else {
                    pw.print("user ");
                    pw.print(userId);
                }
                pw.print(", ");
                if (packageName == null) {
                    pw.println("all packages");
                } else {
                    pw.print("package ");
                    pw.println(packageName);
                }
                return 0;
            } else if (cmd.equals("write-settings")) {
                shell.mInternal.mContext.enforcePermission("android.permission.UPDATE_APP_OPS_STATS", Binder.getCallingPid(), Binder.getCallingUid(), null);
                token = Binder.clearCallingIdentity();
                try {
                    synchronized (shell.mInternal) {
                        shell.mInternal.mHandler.removeCallbacks(shell.mInternal.mWriteRunner);
                    }
                    shell.mInternal.writeState();
                    pw.println("Current settings written.");
                    return 0;
                } finally {
                    Binder.restoreCallingIdentity(token);
                }
            } else if (!cmd.equals("read-settings")) {
                return shell.handleDefaultCommands(cmd);
            } else {
                shell.mInternal.mContext.enforcePermission("android.permission.UPDATE_APP_OPS_STATS", Binder.getCallingPid(), Binder.getCallingUid(), null);
                token = Binder.clearCallingIdentity();
                shell.mInternal.readState();
                pw.println("Last settings read.");
                Binder.restoreCallingIdentity(token);
                return 0;
            }
        } catch (RemoteException e) {
            pw.println("Remote exception: " + e);
            return -1;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(token);
        }
    }

    private void dumpHelp(PrintWriter pw) {
        pw.println("AppOps service (appops) dump options:");
        pw.println("  none");
    }

    protected void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        if (DumpUtils.checkDumpAndUsageStatsPermission(this.mContext, TAG, pw)) {
            int i;
            if (args != null) {
                i = 0;
                while (i < args.length) {
                    String arg = args[i];
                    if ("-h".equals(arg)) {
                        dumpHelp(pw);
                        return;
                    } else if ("-a".equals(arg)) {
                        i++;
                    } else if (arg.length() <= 0 || arg.charAt(0) != '-') {
                        pw.println("Unknown command: " + arg);
                        return;
                    } else {
                        pw.println("Unknown option: " + arg);
                        return;
                    }
                }
            }
            synchronized (this) {
                ArraySet<Callback> callbacks;
                int j;
                Op op;
                pw.println("Current AppOps Service state:");
                long now = System.currentTimeMillis();
                boolean needSep = false;
                if (this.mOpModeWatchers.size() > 0) {
                    needSep = true;
                    pw.println("  Op mode watchers:");
                    for (i = 0; i < this.mOpModeWatchers.size(); i++) {
                        pw.print("    Op ");
                        pw.print(AppOpsManager.opToName(this.mOpModeWatchers.keyAt(i)));
                        pw.println(":");
                        callbacks = (ArraySet) this.mOpModeWatchers.valueAt(i);
                        for (j = 0; j < callbacks.size(); j++) {
                            pw.print("      #");
                            pw.print(j);
                            pw.print(": ");
                            pw.println(callbacks.valueAt(j));
                        }
                    }
                }
                if (this.mPackageModeWatchers.size() > 0) {
                    needSep = true;
                    pw.println("  Package mode watchers:");
                    for (i = 0; i < this.mPackageModeWatchers.size(); i++) {
                        pw.print("    Pkg ");
                        pw.print((String) this.mPackageModeWatchers.keyAt(i));
                        pw.println(":");
                        callbacks = (ArraySet) this.mPackageModeWatchers.valueAt(i);
                        for (j = 0; j < callbacks.size(); j++) {
                            pw.print("      #");
                            pw.print(j);
                            pw.print(": ");
                            pw.println(callbacks.valueAt(j));
                        }
                    }
                }
                if (this.mModeWatchers.size() > 0) {
                    needSep = true;
                    pw.println("  All mode watchers:");
                    for (i = 0; i < this.mModeWatchers.size(); i++) {
                        pw.print("    ");
                        pw.print(this.mModeWatchers.keyAt(i));
                        pw.print(" -> ");
                        pw.println(this.mModeWatchers.valueAt(i));
                    }
                }
                if (this.mClients.size() > 0) {
                    needSep = true;
                    pw.println("  Clients:");
                    for (i = 0; i < this.mClients.size(); i++) {
                        pw.print("    ");
                        pw.print(this.mClients.keyAt(i));
                        pw.println(":");
                        ClientState cs = (ClientState) this.mClients.valueAt(i);
                        pw.print("      ");
                        pw.println(cs);
                        if (cs.mStartedOps != null && cs.mStartedOps.size() > 0) {
                            pw.println("      Started ops:");
                            for (j = 0; j < cs.mStartedOps.size(); j++) {
                                op = (Op) cs.mStartedOps.get(j);
                                pw.print("        ");
                                pw.print("uid=");
                                pw.print(op.uid);
                                pw.print(" pkg=");
                                pw.print(op.packageName);
                                pw.print(" op=");
                                pw.println(AppOpsManager.opToName(op.op));
                            }
                        }
                    }
                }
                if (this.mAudioRestrictions.size() > 0) {
                    boolean printedHeader = false;
                    for (int o = 0; o < this.mAudioRestrictions.size(); o++) {
                        String op2 = AppOpsManager.opToName(this.mAudioRestrictions.keyAt(o));
                        SparseArray<Restriction> restrictions = (SparseArray) this.mAudioRestrictions.valueAt(o);
                        for (i = 0; i < restrictions.size(); i++) {
                            if (!printedHeader) {
                                pw.println("  Audio Restrictions:");
                                printedHeader = true;
                                needSep = true;
                            }
                            int usage = restrictions.keyAt(i);
                            pw.print("    ");
                            pw.print(op2);
                            pw.print(" usage=");
                            pw.print(AudioAttributes.usageToString(usage));
                            Restriction r = (Restriction) restrictions.valueAt(i);
                            pw.print(": mode=");
                            pw.println(r.mode);
                            if (!r.exceptionPackages.isEmpty()) {
                                pw.println("      Exceptions:");
                                for (j = 0; j < r.exceptionPackages.size(); j++) {
                                    pw.print("        ");
                                    pw.println((String) r.exceptionPackages.valueAt(j));
                                }
                            }
                        }
                    }
                }
                if (needSep) {
                    pw.println();
                }
                for (i = 0; i < this.mUidStates.size(); i++) {
                    UidState uidState = (UidState) this.mUidStates.valueAt(i);
                    pw.print("  Uid ");
                    UserHandle.formatUid(pw, uidState.uid);
                    pw.println(":");
                    needSep = true;
                    SparseIntArray opModes = uidState.opModes;
                    if (opModes != null) {
                        int opModeCount = opModes.size();
                        for (j = 0; j < opModeCount; j++) {
                            int code = opModes.keyAt(j);
                            int mode = opModes.valueAt(j);
                            pw.print("      ");
                            pw.print(AppOpsManager.opToName(code));
                            pw.print(": mode=");
                            pw.println(mode);
                        }
                    }
                    ArrayMap<String, Ops> pkgOps = uidState.pkgOps;
                    if (pkgOps != null) {
                        for (Ops ops : pkgOps.values()) {
                            pw.print("    Package ");
                            pw.print(ops.packageName);
                            pw.println(":");
                            for (j = 0; j < ops.size(); j++) {
                                op = (Op) ops.valueAt(j);
                                pw.print("      ");
                                pw.print(AppOpsManager.opToName(op.op));
                                pw.print(": mode=");
                                pw.print(op.mode);
                                if (op.time != 0) {
                                    pw.print("; time=");
                                    TimeUtils.formatDuration(now - op.time, pw);
                                    pw.print(" ago");
                                }
                                if (op.rejectTime != 0) {
                                    pw.print("; rejectTime=");
                                    TimeUtils.formatDuration(now - op.rejectTime, pw);
                                    pw.print(" ago");
                                }
                                if (op.duration == -1) {
                                    pw.print(" (running)");
                                } else if (op.duration != 0) {
                                    pw.print("; duration=");
                                    TimeUtils.formatDuration((long) op.duration, pw);
                                }
                                pw.println();
                            }
                        }
                    }
                }
                if (needSep) {
                    pw.println();
                }
                int userRestrictionCount = this.mOpUserRestrictions.size();
                for (i = 0; i < userRestrictionCount; i++) {
                    int userId;
                    ClientRestrictionState restrictionState = (ClientRestrictionState) this.mOpUserRestrictions.valueAt(i);
                    pw.println("  User restrictions for token " + ((IBinder) this.mOpUserRestrictions.keyAt(i)) + ":");
                    int restrictionCount = restrictionState.perUserRestrictions != null ? restrictionState.perUserRestrictions.size() : 0;
                    if (restrictionCount > 0) {
                        pw.println("      Restricted ops:");
                        for (j = 0; j < restrictionCount; j++) {
                            userId = restrictionState.perUserRestrictions.keyAt(j);
                            boolean[] restrictedOps = (boolean[]) restrictionState.perUserRestrictions.valueAt(j);
                            if (restrictedOps != null) {
                                StringBuilder restrictedOpsValue = new StringBuilder();
                                restrictedOpsValue.append("[");
                                int restrictedOpCount = restrictedOps.length;
                                for (int k = 0; k < restrictedOpCount; k++) {
                                    if (restrictedOps[k]) {
                                        if (restrictedOpsValue.length() > 1) {
                                            restrictedOpsValue.append(", ");
                                        }
                                        restrictedOpsValue.append(AppOpsManager.opToName(k));
                                    }
                                }
                                restrictedOpsValue.append("]");
                                pw.print("        ");
                                pw.print("user: ");
                                pw.print(userId);
                                pw.print(" restricted ops: ");
                                pw.println(restrictedOpsValue);
                            }
                        }
                    }
                    int excludedPackageCount = restrictionState.perUserExcludedPackages != null ? restrictionState.perUserExcludedPackages.size() : 0;
                    if (excludedPackageCount > 0) {
                        pw.println("      Excluded packages:");
                        for (j = 0; j < excludedPackageCount; j++) {
                            userId = restrictionState.perUserExcludedPackages.keyAt(j);
                            String[] packageNames = (String[]) restrictionState.perUserExcludedPackages.valueAt(j);
                            pw.print("        ");
                            pw.print("user: ");
                            pw.print(userId);
                            pw.print(" packages: ");
                            pw.println(Arrays.toString(packageNames));
                        }
                    }
                }
            }
        }
    }

    public void setUserRestrictions(Bundle restrictions, IBinder token, int userHandle) {
        checkSystemUid("setUserRestrictions");
        Preconditions.checkNotNull(restrictions);
        Preconditions.checkNotNull(token);
        for (int i = 0; i < 70; i++) {
            String restriction = AppOpsManager.opToRestriction(i);
            if (restriction != null) {
                setUserRestrictionNoCheck(i, restrictions.getBoolean(restriction, false), token, userHandle, null);
            }
        }
    }

    public void setUserRestriction(int code, boolean restricted, IBinder token, int userHandle, String[] exceptionPackages) {
        if (Binder.getCallingPid() != Process.myPid()) {
            this.mContext.enforcePermission("android.permission.MANAGE_APP_OPS_RESTRICTIONS", Binder.getCallingPid(), Binder.getCallingUid(), null);
        }
        if (userHandle == UserHandle.getCallingUserId() || this.mContext.checkCallingOrSelfPermission("android.permission.INTERACT_ACROSS_USERS_FULL") == 0 || this.mContext.checkCallingOrSelfPermission("android.permission.INTERACT_ACROSS_USERS") == 0) {
            verifyIncomingOp(code);
            Preconditions.checkNotNull(token);
            setUserRestrictionNoCheck(code, restricted, token, userHandle, exceptionPackages);
            return;
        }
        throw new SecurityException("Need INTERACT_ACROSS_USERS_FULL or INTERACT_ACROSS_USERS to interact cross user ");
    }

    private void setUserRestrictionNoCheck(int code, boolean restricted, IBinder token, int userHandle, String[] exceptionPackages) {
        boolean notifyChange = false;
        synchronized (this) {
            ClientRestrictionState restrictionState = (ClientRestrictionState) this.mOpUserRestrictions.get(token);
            if (restrictionState == null) {
                try {
                    restrictionState = new ClientRestrictionState(token);
                    this.mOpUserRestrictions.put(token, restrictionState);
                } catch (RemoteException e) {
                    return;
                }
            }
            if (restrictionState.setRestriction(code, restricted, exceptionPackages, userHandle)) {
                notifyChange = true;
            }
            if (restrictionState.isDefault()) {
                this.mOpUserRestrictions.remove(token);
                restrictionState.destroy();
            }
        }
        if (notifyChange) {
            notifyWatchersOfChange(code);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void notifyWatchersOfChange(int r12) {
        /*
        r11 = this;
        monitor-enter(r11);
        r8 = r11.mOpModeWatchers;	 Catch:{ all -> 0x002e }
        r2 = r8.get(r12);	 Catch:{ all -> 0x002e }
        r2 = (android.util.ArraySet) r2;	 Catch:{ all -> 0x002e }
        if (r2 != 0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r11);
        return;
    L_0x000d:
        r3 = new android.util.ArraySet;	 Catch:{ all -> 0x002e }
        r3.<init>(r2);	 Catch:{ all -> 0x002e }
        monitor-exit(r11);
        r6 = android.os.Binder.clearCallingIdentity();
        r1 = r3.size();	 Catch:{ all -> 0x003c }
        r5 = 0;
    L_0x001c:
        if (r5 >= r1) goto L_0x0041;
    L_0x001e:
        r0 = r3.valueAt(r5);	 Catch:{ all -> 0x003c }
        r0 = (com.android.server.AppOpsService.Callback) r0;	 Catch:{ all -> 0x003c }
        r8 = r0.mCallback;	 Catch:{ RemoteException -> 0x0031 }
        r9 = -1;
        r10 = 0;
        r8.opChanged(r12, r9, r10);	 Catch:{ RemoteException -> 0x0031 }
    L_0x002b:
        r5 = r5 + 1;
        goto L_0x001c;
    L_0x002e:
        r8 = move-exception;
        monitor-exit(r11);
        throw r8;
    L_0x0031:
        r4 = move-exception;
        r8 = "AppOps";
        r9 = "Error dispatching op op change";
        android.util.Log.w(r8, r9, r4);	 Catch:{ all -> 0x003c }
        goto L_0x002b;
    L_0x003c:
        r8 = move-exception;
        android.os.Binder.restoreCallingIdentity(r6);
        throw r8;
    L_0x0041:
        android.os.Binder.restoreCallingIdentity(r6);
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.AppOpsService.notifyWatchersOfChange(int):void");
    }

    public void removeUser(int userHandle) throws RemoteException {
        checkSystemUid("removeUser");
        synchronized (this) {
            for (int i = this.mOpUserRestrictions.size() - 1; i >= 0; i--) {
                ((ClientRestrictionState) this.mOpUserRestrictions.valueAt(i)).removeUser(userHandle);
            }
            removeUidsForUserLocked(userHandle);
        }
    }

    public boolean isOperationActive(int code, int uid, String packageName) {
        verifyIncomingUid(uid);
        verifyIncomingOp(code);
        if (resolvePackageName(uid, packageName) == null) {
            return false;
        }
        synchronized (this) {
            for (int i = this.mClients.size() - 1; i >= 0; i--) {
                ClientState client = (ClientState) this.mClients.valueAt(i);
                if (client.mStartedOps != null) {
                    for (int j = client.mStartedOps.size() - 1; j >= 0; j--) {
                        Op op = (Op) client.mStartedOps.get(j);
                        if (op.op == code && op.uid == uid) {
                            return true;
                        }
                    }
                    continue;
                }
            }
            return false;
        }
    }

    private void removeUidsForUserLocked(int userHandle) {
        for (int i = this.mUidStates.size() - 1; i >= 0; i--) {
            if (UserHandle.getUserId(this.mUidStates.keyAt(i)) == userHandle) {
                this.mUidStates.removeAt(i);
            }
        }
    }

    private void checkSystemUid(String function) {
        if (Binder.getCallingUid() != 1000) {
            throw new SecurityException(function + " must by called by the system");
        }
    }

    private static String resolvePackageName(int uid, String packageName) {
        if (uid == 0) {
            return "root";
        }
        if (uid == 2000) {
            return "com.android.shell";
        }
        if (uid == 1000 && packageName == null) {
            return "android";
        }
        return packageName;
    }

    private static String[] getPackagesForUid(int uid) {
        String[] packageNames = null;
        try {
            packageNames = AppGlobals.getPackageManager().getPackagesForUid(uid);
        } catch (RemoteException e) {
        }
        if (packageNames == null) {
            return EmptyArray.STRING;
        }
        return packageNames;
    }
}
