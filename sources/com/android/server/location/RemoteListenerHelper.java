package com.android.server.location;

import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.HashMap;
import java.util.Map;

abstract class RemoteListenerHelper<TListener extends IInterface> {
    protected static final int RESULT_GPS_LOCATION_DISABLED = 3;
    protected static final int RESULT_INTERNAL_ERROR = 4;
    protected static final int RESULT_NOT_AVAILABLE = 1;
    protected static final int RESULT_NOT_SUPPORTED = 2;
    protected static final int RESULT_SUCCESS = 0;
    protected static final int RESULT_UNKNOWN = 5;
    private final Handler mHandler;
    private boolean mHasIsSupported;
    private boolean mIsRegistered;
    private boolean mIsSupported;
    private int mLastReportedResult = 5;
    private final Map<IBinder, LinkedListener> mListenerMap = new HashMap();
    private final String mTag;

    protected interface ListenerOperation<TListener extends IInterface> {
        void execute(TListener tListener) throws RemoteException;
    }

    private class HandlerRunnable implements Runnable {
        private final TListener mListener;
        private final ListenerOperation<TListener> mOperation;

        public HandlerRunnable(TListener listener, ListenerOperation<TListener> operation) {
            this.mListener = listener;
            this.mOperation = operation;
        }

        public void run() {
            try {
                this.mOperation.execute(this.mListener);
            } catch (RemoteException e) {
                Log.v(RemoteListenerHelper.this.mTag, "Error in monitored listener.", e);
            }
        }
    }

    private class LinkedListener implements DeathRecipient {
        private final TListener mListener;

        public LinkedListener(TListener listener) {
            this.mListener = listener;
        }

        public TListener getUnderlyingListener() {
            return this.mListener;
        }

        public void binderDied() {
            Log.d(RemoteListenerHelper.this.mTag, "Remote Listener died: " + this.mListener);
            RemoteListenerHelper.this.removeListener(this.mListener);
        }
    }

    protected abstract ListenerOperation<TListener> getHandlerOperation(int i);

    protected abstract boolean isAvailableInPlatform();

    protected abstract boolean isGpsEnabled();

    protected abstract boolean registerWithService();

    protected abstract void unregisterFromService();

    protected RemoteListenerHelper(Handler handler, String name) {
        Preconditions.checkNotNull(name);
        this.mHandler = handler;
        this.mTag = name;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean addListener(TListener r9) {
        /*
        r8 = this;
        r7 = 0;
        r6 = 1;
        r4 = "Attempted to register a 'null' listener.";
        com.android.internal.util.Preconditions.checkNotNull(r9, r4);
        r0 = r9.asBinder();
        r1 = new com.android.server.location.RemoteListenerHelper$LinkedListener;
        r1.<init>(r9);
        r5 = r8.mListenerMap;
        monitor-enter(r5);
        r4 = r8.mListenerMap;	 Catch:{ all -> 0x0065 }
        r4 = r4.containsKey(r0);	 Catch:{ all -> 0x0065 }
        if (r4 == 0) goto L_0x001e;
    L_0x001c:
        monitor-exit(r5);
        return r6;
    L_0x001e:
        r4 = 0;
        r0.linkToDeath(r1, r4);	 Catch:{ RemoteException -> 0x0037 }
        r4 = r8.mListenerMap;	 Catch:{ all -> 0x0065 }
        r4.put(r0, r1);	 Catch:{ all -> 0x0065 }
        r4 = r8.isAvailableInPlatform();	 Catch:{ all -> 0x0065 }
        if (r4 != 0) goto L_0x0042;
    L_0x002d:
        r3 = 1;
    L_0x002e:
        r4 = r8.getHandlerOperation(r3);	 Catch:{ all -> 0x0065 }
        r8.post(r9, r4);	 Catch:{ all -> 0x0065 }
        monitor-exit(r5);
        return r6;
    L_0x0037:
        r2 = move-exception;
        r4 = r8.mTag;	 Catch:{ all -> 0x0065 }
        r6 = "Remote listener already died.";
        android.util.Log.v(r4, r6, r2);	 Catch:{ all -> 0x0065 }
        monitor-exit(r5);
        return r7;
    L_0x0042:
        r4 = r8.mHasIsSupported;	 Catch:{ all -> 0x0065 }
        if (r4 == 0) goto L_0x004e;
    L_0x0046:
        r4 = r8.mIsSupported;	 Catch:{ all -> 0x0065 }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x004e;
    L_0x004c:
        r3 = 2;
        goto L_0x002e;
    L_0x004e:
        r4 = r8.isGpsEnabled();	 Catch:{ all -> 0x0065 }
        if (r4 != 0) goto L_0x0056;
    L_0x0054:
        r3 = 3;
        goto L_0x002e;
    L_0x0056:
        r4 = r8.mHasIsSupported;	 Catch:{ all -> 0x0065 }
        if (r4 == 0) goto L_0x0063;
    L_0x005a:
        r4 = r8.mIsSupported;	 Catch:{ all -> 0x0065 }
        if (r4 == 0) goto L_0x0063;
    L_0x005e:
        r8.tryRegister();	 Catch:{ all -> 0x0065 }
        r3 = 0;
        goto L_0x002e;
    L_0x0063:
        monitor-exit(r5);
        return r6;
    L_0x0065:
        r4 = move-exception;
        monitor-exit(r5);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.location.RemoteListenerHelper.addListener(android.os.IInterface):boolean");
    }

    public void removeListener(TListener listener) {
        Preconditions.checkNotNull(listener, "Attempted to remove a 'null' listener.");
        IBinder binder = listener.asBinder();
        synchronized (this.mListenerMap) {
            LinkedListener linkedListener = (LinkedListener) this.mListenerMap.remove(binder);
            if (this.mListenerMap.isEmpty()) {
                tryUnregister();
            }
        }
        if (linkedListener != null) {
            binder.unlinkToDeath(linkedListener, 0);
        }
    }

    protected void foreach(ListenerOperation<TListener> operation) {
        synchronized (this.mListenerMap) {
            foreachUnsafe(operation);
        }
    }

    protected void setSupported(boolean value) {
        synchronized (this.mListenerMap) {
            this.mHasIsSupported = true;
            this.mIsSupported = value;
        }
    }

    protected void tryUpdateRegistrationWithService() {
        synchronized (this.mListenerMap) {
            if (!isGpsEnabled()) {
                tryUnregister();
            } else if (this.mListenerMap.isEmpty()) {
            } else {
                tryRegister();
            }
        }
    }

    protected void updateResult() {
        synchronized (this.mListenerMap) {
            int newResult = calculateCurrentResultUnsafe();
            if (this.mLastReportedResult == newResult) {
                return;
            }
            foreachUnsafe(getHandlerOperation(newResult));
            this.mLastReportedResult = newResult;
        }
    }

    private void foreachUnsafe(ListenerOperation<TListener> operation) {
        for (LinkedListener linkedListener : this.mListenerMap.values()) {
            post(linkedListener.getUnderlyingListener(), operation);
        }
    }

    private void post(TListener listener, ListenerOperation<TListener> operation) {
        if (operation != null) {
            this.mHandler.post(new HandlerRunnable(listener, operation));
        }
    }

    private void tryRegister() {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (!RemoteListenerHelper.this.mIsRegistered) {
                    RemoteListenerHelper.this.mIsRegistered = RemoteListenerHelper.this.registerWithService();
                }
                if (!RemoteListenerHelper.this.mIsRegistered) {
                    RemoteListenerHelper.this.mHandler.post(new Runnable() {
                        public void run() {
                            synchronized (RemoteListenerHelper.this.mListenerMap) {
                                RemoteListenerHelper.this.foreachUnsafe(RemoteListenerHelper.this.getHandlerOperation(4));
                            }
                        }
                    });
                }
            }
        });
    }

    private void tryUnregister() {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (RemoteListenerHelper.this.mIsRegistered) {
                    RemoteListenerHelper.this.unregisterFromService();
                    RemoteListenerHelper.this.mIsRegistered = false;
                }
            }
        });
    }

    private int calculateCurrentResultUnsafe() {
        if (!isAvailableInPlatform()) {
            return 1;
        }
        if (!this.mHasIsSupported || this.mListenerMap.isEmpty()) {
            return 5;
        }
        if (!this.mIsSupported) {
            return 2;
        }
        if (isGpsEnabled()) {
            return 0;
        }
        return 3;
    }
}
