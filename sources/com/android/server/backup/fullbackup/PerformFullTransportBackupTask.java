package com.android.server.backup.fullbackup;

import android.app.IBackupAgent;
import android.app.backup.IBackupManagerMonitor;
import android.app.backup.IBackupObserver;
import android.app.backup.IFullBackupRestoreObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Slog;
import com.android.internal.backup.IBackupTransport;
import com.android.server.backup.BackupRestoreTask;
import com.android.server.backup.FullBackupJob;
import com.android.server.backup.RefactoredBackupManagerService;
import com.android.server.backup.internal.Operation;
import com.android.server.backup.utils.AppBackupUtils;
import com.android.server.backup.utils.BackupManagerMonitorUtils;
import com.android.server.backup.utils.BackupObserverUtils;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class PerformFullTransportBackupTask extends FullBackupTask implements BackupRestoreTask {
    private static final String TAG = "PFTBT";
    private RefactoredBackupManagerService backupManagerService;
    IBackupObserver mBackupObserver;
    SinglePackageBackupRunner mBackupRunner;
    private final int mBackupRunnerOpToken;
    private volatile boolean mCancelAll;
    private final Object mCancelLock = new Object();
    private final int mCurrentOpToken;
    PackageInfo mCurrentPackage;
    private volatile boolean mIsDoingBackup;
    FullBackupJob mJob;
    CountDownLatch mLatch;
    IBackupManagerMonitor mMonitor;
    ArrayList<PackageInfo> mPackages;
    private volatile IBackupTransport mTransport;
    boolean mUpdateSchedule;
    boolean mUserInitiated;

    class SinglePackageBackupPreflight implements BackupRestoreTask, FullBackupPreflight {
        private final int mCurrentOpToken;
        final CountDownLatch mLatch = new CountDownLatch(1);
        final long mQuota;
        final AtomicLong mResult = new AtomicLong(-1003);
        final IBackupTransport mTransport;

        SinglePackageBackupPreflight(IBackupTransport transport, long quota, int currentOpToken) {
            this.mTransport = transport;
            this.mQuota = quota;
            this.mCurrentOpToken = currentOpToken;
        }

        public int preflightFullBackup(PackageInfo pkg, IBackupAgent agent) {
            int result;
            try {
                PerformFullTransportBackupTask.this.backupManagerService.prepareOperationTimeout(this.mCurrentOpToken, RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL, this, 0);
                PerformFullTransportBackupTask.this.backupManagerService.addBackupTrace("preflighting");
                agent.doMeasureFullBackup(this.mQuota, this.mCurrentOpToken, PerformFullTransportBackupTask.this.backupManagerService.getBackupManagerBinder());
                this.mLatch.await(RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL, TimeUnit.MILLISECONDS);
                long totalSize = this.mResult.get();
                if (totalSize < 0) {
                    return (int) totalSize;
                }
                result = this.mTransport.checkFullBackupSize(totalSize);
                if (result == -1005) {
                    agent.doQuotaExceeded(totalSize, this.mQuota);
                }
                return result;
            } catch (Exception e) {
                Slog.w(PerformFullTransportBackupTask.TAG, "Exception preflighting " + pkg.packageName + ": " + e.getMessage());
                result = -1003;
            }
        }

        public void execute() {
        }

        public void operationComplete(long result) {
            this.mResult.set(result);
            this.mLatch.countDown();
            PerformFullTransportBackupTask.this.backupManagerService.removeOperation(this.mCurrentOpToken);
        }

        public void handleCancel(boolean cancelAll) {
            this.mResult.set(-1003);
            this.mLatch.countDown();
            PerformFullTransportBackupTask.this.backupManagerService.removeOperation(this.mCurrentOpToken);
        }

        public long getExpectedSizeOrErrorCode() {
            try {
                this.mLatch.await(RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL, TimeUnit.MILLISECONDS);
                return this.mResult.get();
            } catch (InterruptedException e) {
                return -1;
            }
        }
    }

    class SinglePackageBackupRunner implements Runnable, BackupRestoreTask {
        final CountDownLatch mBackupLatch = new CountDownLatch(1);
        private volatile int mBackupResult = -1003;
        private final int mCurrentOpToken;
        private FullBackupEngine mEngine;
        private final int mEphemeralToken;
        private volatile boolean mIsCancelled;
        final ParcelFileDescriptor mOutput;
        final SinglePackageBackupPreflight mPreflight;
        final CountDownLatch mPreflightLatch = new CountDownLatch(1);
        private volatile int mPreflightResult = -1003;
        private final long mQuota;
        final PackageInfo mTarget;

        SinglePackageBackupRunner(ParcelFileDescriptor output, PackageInfo target, IBackupTransport transport, long quota, int currentOpToken) throws IOException {
            this.mOutput = ParcelFileDescriptor.dup(output.getFileDescriptor());
            this.mTarget = target;
            this.mCurrentOpToken = currentOpToken;
            this.mEphemeralToken = PerformFullTransportBackupTask.this.backupManagerService.generateRandomIntegerToken();
            this.mPreflight = new SinglePackageBackupPreflight(transport, quota, this.mEphemeralToken);
            this.mQuota = quota;
            registerTask();
        }

        void registerTask() {
            synchronized (PerformFullTransportBackupTask.this.backupManagerService.getCurrentOpLock()) {
                PerformFullTransportBackupTask.this.backupManagerService.getCurrentOperations().put(this.mCurrentOpToken, new Operation(0, this, 0));
            }
        }

        void unregisterTask() {
            synchronized (PerformFullTransportBackupTask.this.backupManagerService.getCurrentOpLock()) {
                PerformFullTransportBackupTask.this.backupManagerService.getCurrentOperations().remove(this.mCurrentOpToken);
            }
        }

        public void run() {
            this.mEngine = new FullBackupEngine(PerformFullTransportBackupTask.this.backupManagerService, new FileOutputStream(this.mOutput.getFileDescriptor()), this.mPreflight, this.mTarget, false, this, this.mQuota, this.mCurrentOpToken);
            try {
                if (!this.mIsCancelled) {
                    this.mPreflightResult = this.mEngine.preflightCheck();
                }
                this.mPreflightLatch.countDown();
                if (this.mPreflightResult == 0 && !this.mIsCancelled) {
                    this.mBackupResult = this.mEngine.backupOnePackage();
                }
                unregisterTask();
                this.mBackupLatch.countDown();
                try {
                    this.mOutput.close();
                } catch (IOException e) {
                    Slog.w(PerformFullTransportBackupTask.TAG, "Error closing transport pipe in runner");
                }
            } catch (Exception e2) {
                try {
                    Slog.e(PerformFullTransportBackupTask.TAG, "Exception during full package backup of " + this.mTarget.packageName);
                    try {
                        this.mOutput.close();
                    } catch (IOException e3) {
                        Slog.w(PerformFullTransportBackupTask.TAG, "Error closing transport pipe in runner");
                    }
                } finally {
                    unregisterTask();
                    this.mBackupLatch.countDown();
                    try {
                        this.mOutput.close();
                    } catch (IOException e4) {
                        Slog.w(PerformFullTransportBackupTask.TAG, "Error closing transport pipe in runner");
                    }
                }
            } catch (Throwable th) {
                this.mPreflightLatch.countDown();
            }
        }

        public void sendQuotaExceeded(long backupDataBytes, long quotaBytes) {
            this.mEngine.sendQuotaExceeded(backupDataBytes, quotaBytes);
        }

        long getPreflightResultBlocking() {
            try {
                this.mPreflightLatch.await(RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL, TimeUnit.MILLISECONDS);
                if (this.mIsCancelled) {
                    return -2003;
                }
                if (this.mPreflightResult == 0) {
                    return this.mPreflight.getExpectedSizeOrErrorCode();
                }
                return (long) this.mPreflightResult;
            } catch (InterruptedException e) {
                return -1003;
            }
        }

        int getBackupResultBlocking() {
            try {
                this.mBackupLatch.await(RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL, TimeUnit.MILLISECONDS);
                if (this.mIsCancelled) {
                    return -2003;
                }
                return this.mBackupResult;
            } catch (InterruptedException e) {
                return -1003;
            }
        }

        public void execute() {
        }

        public void operationComplete(long result) {
        }

        public void handleCancel(boolean cancelAll) {
            Slog.w(PerformFullTransportBackupTask.TAG, "Full backup cancel of " + this.mTarget.packageName);
            PerformFullTransportBackupTask.this.mMonitor = BackupManagerMonitorUtils.monitorEvent(PerformFullTransportBackupTask.this.mMonitor, 4, PerformFullTransportBackupTask.this.mCurrentPackage, 2, null);
            this.mIsCancelled = true;
            PerformFullTransportBackupTask.this.backupManagerService.handleCancel(this.mEphemeralToken, cancelAll);
            PerformFullTransportBackupTask.this.backupManagerService.tearDownAgentAndKill(this.mTarget.applicationInfo);
            this.mPreflightLatch.countDown();
            this.mBackupLatch.countDown();
            PerformFullTransportBackupTask.this.backupManagerService.removeOperation(this.mCurrentOpToken);
        }
    }

    public PerformFullTransportBackupTask(RefactoredBackupManagerService backupManagerService, IFullBackupRestoreObserver observer, String[] whichPackages, boolean updateSchedule, FullBackupJob runningJob, CountDownLatch latch, IBackupObserver backupObserver, IBackupManagerMonitor monitor, boolean userInitiated) {
        super(observer);
        this.backupManagerService = backupManagerService;
        this.mUpdateSchedule = updateSchedule;
        this.mLatch = latch;
        this.mJob = runningJob;
        this.mPackages = new ArrayList(whichPackages.length);
        this.mBackupObserver = backupObserver;
        this.mMonitor = monitor;
        this.mUserInitiated = userInitiated;
        this.mCurrentOpToken = backupManagerService.generateRandomIntegerToken();
        this.mBackupRunnerOpToken = backupManagerService.generateRandomIntegerToken();
        if (backupManagerService.isBackupOperationInProgress()) {
            Slog.d(TAG, "Skipping full backup. A backup is already in progress.");
            this.mCancelAll = true;
            return;
        }
        registerTask();
        for (String pkg : whichPackages) {
            try {
                PackageInfo info = backupManagerService.getPackageManager().getPackageInfo(pkg, 64);
                this.mCurrentPackage = info;
                if (!AppBackupUtils.appIsEligibleForBackup(info.applicationInfo)) {
                    this.mMonitor = BackupManagerMonitorUtils.monitorEvent(this.mMonitor, 9, this.mCurrentPackage, 3, null);
                    BackupObserverUtils.sendBackupOnPackageResult(this.mBackupObserver, pkg, -2001);
                } else if (!AppBackupUtils.appGetsFullBackup(info)) {
                    this.mMonitor = BackupManagerMonitorUtils.monitorEvent(this.mMonitor, 10, this.mCurrentPackage, 3, null);
                    BackupObserverUtils.sendBackupOnPackageResult(this.mBackupObserver, pkg, -2001);
                } else if (AppBackupUtils.appIsStopped(info.applicationInfo)) {
                    this.mMonitor = BackupManagerMonitorUtils.monitorEvent(this.mMonitor, 11, this.mCurrentPackage, 3, null);
                    BackupObserverUtils.sendBackupOnPackageResult(this.mBackupObserver, pkg, -2001);
                } else {
                    this.mPackages.add(info);
                }
            } catch (NameNotFoundException e) {
                Slog.i(TAG, "Requested package " + pkg + " not found; ignoring");
                this.mMonitor = BackupManagerMonitorUtils.monitorEvent(this.mMonitor, 12, this.mCurrentPackage, 3, null);
            }
        }
    }

    private void registerTask() {
        synchronized (this.backupManagerService.getCurrentOpLock()) {
            Slog.d(TAG, "backupmanager pftbt token=" + Integer.toHexString(this.mCurrentOpToken));
            this.backupManagerService.getCurrentOperations().put(this.mCurrentOpToken, new Operation(0, this, 2));
        }
    }

    public void unregisterTask() {
        this.backupManagerService.removeOperation(this.mCurrentOpToken);
    }

    public void execute() {
    }

    public void handleCancel(boolean cancelAll) {
        synchronized (this.mCancelLock) {
            if (!cancelAll) {
                Slog.wtf(TAG, "Expected cancelAll to be true.");
            }
            if (this.mCancelAll) {
                Slog.d(TAG, "Ignoring duplicate cancel call.");
                return;
            }
            this.mCancelAll = true;
            if (this.mIsDoingBackup) {
                this.backupManagerService.handleCancel(this.mBackupRunnerOpToken, cancelAll);
                try {
                    this.mTransport.cancelFullBackup();
                } catch (RemoteException e) {
                    Slog.w(TAG, "Error calling cancelFullBackup() on transport: " + e);
                }
            }
        }
    }

    public void operationComplete(long result) {
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        r36 = this;
        r21 = 0;
        r34 = 0;
        r14 = 0;
        r16 = 0;
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r5 = r5.isEnabled();	 Catch:{ Exception -> 0x04a0 }
        if (r5 == 0) goto L_0x001e;
    L_0x0012:
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r5 = r5.isProvisioned();	 Catch:{ Exception -> 0x04a0 }
        r5 = r5 ^ 1;
        if (r5 == 0) goto L_0x010b;
    L_0x001e:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "full backup requested but enabled=";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r7 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r7 = r7.isEnabled();	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r7 = " provisioned=";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r7 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r7 = r7.isProvisioned();	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r7 = "; ignoring";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.i(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r5 = r5.isProvisioned();	 Catch:{ Exception -> 0x04a0 }
        if (r5 == 0) goto L_0x0104;
    L_0x0064:
        r26 = 13;
    L_0x0066:
        r0 = r36;
        r5 = r0.mMonitor;	 Catch:{ Exception -> 0x04a0 }
        r6 = 0;
        r7 = 3;
        r9 = 0;
        r0 = r26;
        r5 = com.android.server.backup.utils.BackupManagerMonitorUtils.monitorEvent(r5, r0, r6, r7, r9);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r0.mMonitor = r5;	 Catch:{ Exception -> 0x04a0 }
        r5 = 0;
        r0 = r36;
        r0.mUpdateSchedule = r5;	 Catch:{ Exception -> 0x04a0 }
        r16 = -2001; // 0xfffffffffffff82f float:NaN double:NaN;
        r0 = r36;
        r5 = r0.mCancelAll;
        if (r5 == 0) goto L_0x0086;
    L_0x0084:
        r16 = -2003; // 0xfffffffffffff82d float:NaN double:NaN;
    L_0x0086:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Full backup completed with status: ";
        r6 = r6.append(r7);
        r0 = r16;
        r6 = r6.append(r0);
        r6 = r6.toString();
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.mBackupObserver;
        r0 = r16;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupFinished(r5, r0);
        r0 = r36;
        r1 = r34;
        r0.cleanUpPipes(r1);
        r0 = r36;
        r1 = r21;
        r0.cleanUpPipes(r1);
        r36.unregisterTask();
        r0 = r36;
        r5 = r0.mJob;
        if (r5 == 0) goto L_0x00c9;
    L_0x00c2:
        r0 = r36;
        r5 = r0.mJob;
        r5.finishBackupPass();
    L_0x00c9:
        r0 = r36;
        r5 = r0.backupManagerService;
        r6 = r5.getQueueLock();
        monitor-enter(r6);
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ all -> 0x0108 }
        r7 = 0;
        r5.setRunningFullBackupTask(r7);	 Catch:{ all -> 0x0108 }
        monitor-exit(r6);
        r0 = r36;
        r5 = r0.mLatch;
        r5.countDown();
        r0 = r36;
        r5 = r0.mUpdateSchedule;
        if (r5 == 0) goto L_0x00ef;
    L_0x00e8:
        r0 = r36;
        r5 = r0.backupManagerService;
        r5.scheduleNextFullBackupJob(r14);
    L_0x00ef:
        r5 = "PFTBT";
        r6 = "Full data backup pass finished.";
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.backupManagerService;
        r5 = r5.getWakelock();
        r5.release();
        return;
    L_0x0104:
        r26 = 14;
        goto L_0x0066;
    L_0x0108:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x010b:
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r5 = r5.getTransportManager();	 Catch:{ Exception -> 0x04a0 }
        r5 = r5.getCurrentTransportBinder();	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r0.mTransport = r5;	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.mTransport;	 Catch:{ Exception -> 0x04a0 }
        if (r5 != 0) goto L_0x01c9;
    L_0x0121:
        r5 = "PFTBT";
        r6 = "Transport not present; full data backup not performed";
        android.util.Slog.w(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r16 = -1000; // 0xfffffffffffffc18 float:NaN double:NaN;
        r0 = r36;
        r5 = r0.mMonitor;	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r6 = r0.mCurrentPackage;	 Catch:{ Exception -> 0x04a0 }
        r7 = 15;
        r9 = 1;
        r12 = 0;
        r5 = com.android.server.backup.utils.BackupManagerMonitorUtils.monitorEvent(r5, r7, r6, r9, r12);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r0.mMonitor = r5;	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.mCancelAll;
        if (r5 == 0) goto L_0x0148;
    L_0x0146:
        r16 = -2003; // 0xfffffffffffff82d float:NaN double:NaN;
    L_0x0148:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Full backup completed with status: ";
        r6 = r6.append(r7);
        r0 = r16;
        r6 = r6.append(r0);
        r6 = r6.toString();
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.mBackupObserver;
        r0 = r16;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupFinished(r5, r0);
        r0 = r36;
        r1 = r34;
        r0.cleanUpPipes(r1);
        r0 = r36;
        r1 = r21;
        r0.cleanUpPipes(r1);
        r36.unregisterTask();
        r0 = r36;
        r5 = r0.mJob;
        if (r5 == 0) goto L_0x018b;
    L_0x0184:
        r0 = r36;
        r5 = r0.mJob;
        r5.finishBackupPass();
    L_0x018b:
        r0 = r36;
        r5 = r0.backupManagerService;
        r6 = r5.getQueueLock();
        monitor-enter(r6);
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ all -> 0x01c6 }
        r7 = 0;
        r5.setRunningFullBackupTask(r7);	 Catch:{ all -> 0x01c6 }
        monitor-exit(r6);
        r0 = r36;
        r5 = r0.mLatch;
        r5.countDown();
        r0 = r36;
        r5 = r0.mUpdateSchedule;
        if (r5 == 0) goto L_0x01b1;
    L_0x01aa:
        r0 = r36;
        r5 = r0.backupManagerService;
        r5.scheduleNextFullBackupJob(r14);
    L_0x01b1:
        r5 = "PFTBT";
        r6 = "Full data backup pass finished.";
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.backupManagerService;
        r5 = r5.getWakelock();
        r5.release();
        return;
    L_0x01c6:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x01c9:
        r0 = r36;
        r5 = r0.mPackages;	 Catch:{ Exception -> 0x04a0 }
        r4 = r5.size();	 Catch:{ Exception -> 0x04a0 }
        r5 = 8192; // 0x2000 float:1.14794E-41 double:4.0474E-320;
        r0 = new byte[r5];	 Catch:{ Exception -> 0x04a0 }
        r18 = r0;
        r24 = 0;
    L_0x01d9:
        r0 = r24;
        if (r0 >= r4) goto L_0x023e;
    L_0x01dd:
        r0 = r36;
        r5 = r0.mPackages;	 Catch:{ Exception -> 0x04a0 }
        r0 = r24;
        r8 = r5.get(r0);	 Catch:{ Exception -> 0x04a0 }
        r8 = (android.content.pm.PackageInfo) r8;	 Catch:{ Exception -> 0x04a0 }
        r0 = r8.packageName;	 Catch:{ Exception -> 0x04a0 }
        r29 = r0;
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Initiating full-data transport backup of ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r7 = " token: ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r7 = r0.mCurrentOpToken;	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.i(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = 2840; // 0xb18 float:3.98E-42 double:1.403E-320;
        r0 = r29;
        android.util.EventLog.writeEvent(r5, r0);	 Catch:{ Exception -> 0x04a0 }
        r34 = android.os.ParcelFileDescriptor.createPipe();	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.mUserInitiated;	 Catch:{ Exception -> 0x04a0 }
        if (r5 == 0) goto L_0x02c4;
    L_0x0229:
        r23 = 1;
    L_0x022b:
        r10 = 9223372036854775807; // 0x7fffffffffffffff float:NaN double:NaN;
        r0 = r36;
        r0 = r0.mCancelLock;	 Catch:{ Exception -> 0x04a0 }
        r35 = r0;
        monitor-enter(r35);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.mCancelAll;	 Catch:{ all -> 0x049d }
        if (r5 == 0) goto L_0x02c8;
    L_0x023d:
        monitor-exit(r35);	 Catch:{ Exception -> 0x04a0 }
    L_0x023e:
        r0 = r36;
        r5 = r0.mCancelAll;
        if (r5 == 0) goto L_0x0246;
    L_0x0244:
        r16 = -2003; // 0xfffffffffffff82d float:NaN double:NaN;
    L_0x0246:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Full backup completed with status: ";
        r6 = r6.append(r7);
        r0 = r16;
        r6 = r6.append(r0);
        r6 = r6.toString();
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.mBackupObserver;
        r0 = r16;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupFinished(r5, r0);
        r0 = r36;
        r1 = r34;
        r0.cleanUpPipes(r1);
        r0 = r36;
        r1 = r21;
        r0.cleanUpPipes(r1);
        r36.unregisterTask();
        r0 = r36;
        r5 = r0.mJob;
        if (r5 == 0) goto L_0x0289;
    L_0x0282:
        r0 = r36;
        r5 = r0.mJob;
        r5.finishBackupPass();
    L_0x0289:
        r0 = r36;
        r5 = r0.backupManagerService;
        r6 = r5.getQueueLock();
        monitor-enter(r6);
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ all -> 0x0827 }
        r7 = 0;
        r5.setRunningFullBackupTask(r7);	 Catch:{ all -> 0x0827 }
        monitor-exit(r6);
        r0 = r36;
        r5 = r0.mLatch;
        r5.countDown();
        r0 = r36;
        r5 = r0.mUpdateSchedule;
        if (r5 == 0) goto L_0x02af;
    L_0x02a8:
        r0 = r36;
        r5 = r0.backupManagerService;
        r5.scheduleNextFullBackupJob(r14);
    L_0x02af:
        r5 = "PFTBT";
        r6 = "Full data backup pass finished.";
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.backupManagerService;
        r5 = r5.getWakelock();
        r5.release();
    L_0x02c3:
        return;
    L_0x02c4:
        r23 = 0;
        goto L_0x022b;
    L_0x02c8:
        r0 = r36;
        r5 = r0.mTransport;	 Catch:{ all -> 0x049d }
        r6 = 0;
        r6 = r34[r6];	 Catch:{ all -> 0x049d }
        r0 = r23;
        r13 = r5.performFullBackup(r8, r6, r0);	 Catch:{ all -> 0x049d }
        if (r13 != 0) goto L_0x030b;
    L_0x02d7:
        r0 = r36;
        r5 = r0.mTransport;	 Catch:{ all -> 0x049d }
        r6 = r8.packageName;	 Catch:{ all -> 0x049d }
        r7 = 1;
        r10 = r5.getBackupQuota(r6, r7);	 Catch:{ all -> 0x049d }
        r21 = android.os.ParcelFileDescriptor.createPipe();	 Catch:{ all -> 0x049d }
        r5 = new com.android.server.backup.fullbackup.PerformFullTransportBackupTask$SinglePackageBackupRunner;	 Catch:{ all -> 0x049d }
        r6 = 1;
        r7 = r21[r6];	 Catch:{ all -> 0x049d }
        r0 = r36;
        r9 = r0.mTransport;	 Catch:{ all -> 0x049d }
        r0 = r36;
        r12 = r0.mBackupRunnerOpToken;	 Catch:{ all -> 0x049d }
        r6 = r36;
        r5.<init>(r7, r8, r9, r10, r12);	 Catch:{ all -> 0x049d }
        r0 = r36;
        r0.mBackupRunner = r5;	 Catch:{ all -> 0x049d }
        r5 = 1;
        r5 = r21[r5];	 Catch:{ all -> 0x049d }
        r5.close();	 Catch:{ all -> 0x049d }
        r5 = 0;
        r6 = 1;
        r21[r6] = r5;	 Catch:{ all -> 0x049d }
        r5 = 1;
        r0 = r36;
        r0.mIsDoingBackup = r5;	 Catch:{ all -> 0x049d }
    L_0x030b:
        monitor-exit(r35);	 Catch:{ Exception -> 0x04a0 }
        if (r13 != 0) goto L_0x03eb;
    L_0x030e:
        r5 = 0;
        r5 = r34[r5];	 Catch:{ Exception -> 0x04a0 }
        r5.close();	 Catch:{ Exception -> 0x04a0 }
        r5 = 0;
        r6 = 0;
        r34[r6] = r5;	 Catch:{ Exception -> 0x04a0 }
        r5 = new java.lang.Thread;	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r6 = r0.mBackupRunner;	 Catch:{ Exception -> 0x04a0 }
        r7 = "package-backup-bridge";
        r5.<init>(r6, r7);	 Catch:{ Exception -> 0x04a0 }
        r5.start();	 Catch:{ Exception -> 0x04a0 }
        r25 = new java.io.FileInputStream;	 Catch:{ Exception -> 0x04a0 }
        r5 = 0;
        r5 = r21[r5];	 Catch:{ Exception -> 0x04a0 }
        r5 = r5.getFileDescriptor();	 Catch:{ Exception -> 0x04a0 }
        r0 = r25;
        r0.<init>(r5);	 Catch:{ Exception -> 0x04a0 }
        r28 = new java.io.FileOutputStream;	 Catch:{ Exception -> 0x04a0 }
        r5 = 1;
        r5 = r34[r5];	 Catch:{ Exception -> 0x04a0 }
        r5 = r5.getFileDescriptor();	 Catch:{ Exception -> 0x04a0 }
        r0 = r28;
        r0.<init>(r5);	 Catch:{ Exception -> 0x04a0 }
        r32 = 0;
        r0 = r36;
        r5 = r0.mBackupRunner;	 Catch:{ Exception -> 0x04a0 }
        r30 = r5.getPreflightResultBlocking();	 Catch:{ Exception -> 0x04a0 }
        r6 = 0;
        r5 = (r30 > r6 ? 1 : (r30 == r6 ? 0 : -1));
        if (r5 >= 0) goto L_0x0554;
    L_0x0353:
        r0 = r36;
        r5 = r0.mMonitor;	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r6 = r0.mCurrentPackage;	 Catch:{ Exception -> 0x04a0 }
        r7 = "android.app.backup.extra.LOG_PREFLIGHT_ERROR";
        r9 = 0;
        r0 = r30;
        r7 = com.android.server.backup.utils.BackupManagerMonitorUtils.putMonitoringExtra(r9, r7, r0);	 Catch:{ Exception -> 0x04a0 }
        r9 = 16;
        r12 = 3;
        r5 = com.android.server.backup.utils.BackupManagerMonitorUtils.monitorEvent(r5, r9, r6, r12, r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r0.mMonitor = r5;	 Catch:{ Exception -> 0x04a0 }
        r0 = r30;
        r13 = (int) r0;	 Catch:{ Exception -> 0x04a0 }
    L_0x0373:
        r0 = r36;
        r5 = r0.mBackupRunner;	 Catch:{ Exception -> 0x04a0 }
        r17 = r5.getBackupResultBlocking();	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r6 = r0.mCancelLock;	 Catch:{ Exception -> 0x04a0 }
        monitor-enter(r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = 0;
        r0 = r36;
        r0.mIsDoingBackup = r5;	 Catch:{ all -> 0x0691 }
        r0 = r36;
        r5 = r0.mCancelAll;	 Catch:{ all -> 0x0691 }
        if (r5 != 0) goto L_0x0399;
    L_0x038b:
        if (r17 != 0) goto L_0x0688;
    L_0x038d:
        r0 = r36;
        r5 = r0.mTransport;	 Catch:{ all -> 0x0691 }
        r22 = r5.finishBackup();	 Catch:{ all -> 0x0691 }
        if (r13 != 0) goto L_0x0399;
    L_0x0397:
        r13 = r22;
    L_0x0399:
        monitor-exit(r6);	 Catch:{ Exception -> 0x04a0 }
        if (r13 != 0) goto L_0x03a0;
    L_0x039c:
        if (r17 == 0) goto L_0x03a0;
    L_0x039e:
        r13 = r17;
    L_0x03a0:
        if (r13 == 0) goto L_0x03c9;
    L_0x03a2:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Error ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.append(r13);	 Catch:{ Exception -> 0x04a0 }
        r7 = " backing up ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.e(r5, r6);	 Catch:{ Exception -> 0x04a0 }
    L_0x03c9:
        r0 = r36;
        r5 = r0.mTransport;	 Catch:{ Exception -> 0x04a0 }
        r14 = r5.requestFullBackupTime();	 Catch:{ Exception -> 0x04a0 }
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Transport suggested backoff=";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.append(r14);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.i(r5, r6);	 Catch:{ Exception -> 0x04a0 }
    L_0x03eb:
        r0 = r36;
        r5 = r0.mUpdateSchedule;	 Catch:{ Exception -> 0x04a0 }
        if (r5 == 0) goto L_0x03fe;
    L_0x03f1:
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r6 = java.lang.System.currentTimeMillis();	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r5.enqueueFullBackup(r0, r6);	 Catch:{ Exception -> 0x04a0 }
    L_0x03fe:
        r5 = -1002; // 0xfffffffffffffc16 float:NaN double:NaN;
        if (r13 != r5) goto L_0x0694;
    L_0x0402:
        r0 = r36;
        r5 = r0.mBackupObserver;	 Catch:{ Exception -> 0x04a0 }
        r6 = -1002; // 0xfffffffffffffc16 float:NaN double:NaN;
        r0 = r29;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r5, r0, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Transport rejected backup of ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r7 = ", skipping";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.i(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = 2;
        r5 = new java.lang.Object[r5];	 Catch:{ Exception -> 0x04a0 }
        r6 = 0;
        r5[r6] = r29;	 Catch:{ Exception -> 0x04a0 }
        r6 = "transport rejected";
        r7 = 1;
        r5[r7] = r6;	 Catch:{ Exception -> 0x04a0 }
        r6 = 2841; // 0xb19 float:3.981E-42 double:1.4036E-320;
        android.util.EventLog.writeEvent(r6, r5);	 Catch:{ Exception -> 0x04a0 }
    L_0x0441:
        r0 = r36;
        r1 = r34;
        r0.cleanUpPipes(r1);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r1 = r21;
        r0.cleanUpPipes(r1);	 Catch:{ Exception -> 0x04a0 }
        r5 = r8.applicationInfo;	 Catch:{ Exception -> 0x04a0 }
        if (r5 == 0) goto L_0x0499;
    L_0x0453:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Unbinding agent in ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.i(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "unbinding ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        r5.addBackupTrace(r6);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ RemoteException -> 0x0830 }
        r5 = r5.getActivityManager();	 Catch:{ RemoteException -> 0x0830 }
        r6 = r8.applicationInfo;	 Catch:{ RemoteException -> 0x0830 }
        r5.unbindBackupAgent(r6);	 Catch:{ RemoteException -> 0x0830 }
    L_0x0499:
        r24 = r24 + 1;
        goto L_0x01d9;
    L_0x049d:
        r5 = move-exception;
        monitor-exit(r35);	 Catch:{ Exception -> 0x04a0 }
        throw r5;	 Catch:{ Exception -> 0x04a0 }
    L_0x04a0:
        r20 = move-exception;
        r16 = -1000; // 0xfffffffffffffc18 float:NaN double:NaN;
        r5 = "PFTBT";
        r6 = "Exception trying full transport backup";
        r0 = r20;
        android.util.Slog.w(r5, r6, r0);	 Catch:{ all -> 0x05fe }
        r0 = r36;
        r5 = r0.mMonitor;	 Catch:{ all -> 0x05fe }
        r0 = r36;
        r6 = r0.mCurrentPackage;	 Catch:{ all -> 0x05fe }
        r7 = "android.app.backup.extra.LOG_EXCEPTION_FULL_BACKUP";
        r9 = android.util.Log.getStackTraceString(r20);	 Catch:{ all -> 0x05fe }
        r12 = 0;
        r7 = com.android.server.backup.utils.BackupManagerMonitorUtils.putMonitoringExtra(r12, r7, r9);	 Catch:{ all -> 0x05fe }
        r9 = 19;
        r12 = 3;
        r5 = com.android.server.backup.utils.BackupManagerMonitorUtils.monitorEvent(r5, r9, r6, r12, r7);	 Catch:{ all -> 0x05fe }
        r0 = r36;
        r0.mMonitor = r5;	 Catch:{ all -> 0x05fe }
        r0 = r36;
        r5 = r0.mCancelAll;
        if (r5 == 0) goto L_0x04d5;
    L_0x04d3:
        r16 = -2003; // 0xfffffffffffff82d float:NaN double:NaN;
    L_0x04d5:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Full backup completed with status: ";
        r6 = r6.append(r7);
        r0 = r16;
        r6 = r6.append(r0);
        r6 = r6.toString();
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.mBackupObserver;
        r0 = r16;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupFinished(r5, r0);
        r0 = r36;
        r1 = r34;
        r0.cleanUpPipes(r1);
        r0 = r36;
        r1 = r21;
        r0.cleanUpPipes(r1);
        r36.unregisterTask();
        r0 = r36;
        r5 = r0.mJob;
        if (r5 == 0) goto L_0x0518;
    L_0x0511:
        r0 = r36;
        r5 = r0.mJob;
        r5.finishBackupPass();
    L_0x0518:
        r0 = r36;
        r5 = r0.backupManagerService;
        r6 = r5.getQueueLock();
        monitor-enter(r6);
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ all -> 0x082a }
        r7 = 0;
        r5.setRunningFullBackupTask(r7);	 Catch:{ all -> 0x082a }
        monitor-exit(r6);
        r0 = r36;
        r5 = r0.mLatch;
        r5.countDown();
        r0 = r36;
        r5 = r0.mUpdateSchedule;
        if (r5 == 0) goto L_0x053e;
    L_0x0537:
        r0 = r36;
        r5 = r0.backupManagerService;
        r5.scheduleNextFullBackupJob(r14);
    L_0x053e:
        r5 = "PFTBT";
        r6 = "Full data backup pass finished.";
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.backupManagerService;
        r5 = r5.getWakelock();
        r5.release();
        goto L_0x02c3;
    L_0x0554:
        r27 = 0;
    L_0x0556:
        r0 = r25;
        r1 = r18;
        r27 = r0.read(r1);	 Catch:{ Exception -> 0x04a0 }
        if (r27 <= 0) goto L_0x05a3;
    L_0x0560:
        r5 = 0;
        r0 = r28;
        r1 = r18;
        r2 = r27;
        r0.write(r1, r5, r2);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r6 = r0.mCancelLock;	 Catch:{ Exception -> 0x04a0 }
        monitor-enter(r6);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.mCancelAll;	 Catch:{ all -> 0x0685 }
        if (r5 != 0) goto L_0x057f;
    L_0x0575:
        r0 = r36;
        r5 = r0.mTransport;	 Catch:{ all -> 0x0685 }
        r0 = r27;
        r13 = r5.sendBackupData(r0);	 Catch:{ all -> 0x0685 }
    L_0x057f:
        monitor-exit(r6);	 Catch:{ Exception -> 0x04a0 }
        r0 = r27;
        r6 = (long) r0;	 Catch:{ Exception -> 0x04a0 }
        r32 = r32 + r6;
        r0 = r36;
        r5 = r0.mBackupObserver;	 Catch:{ Exception -> 0x04a0 }
        if (r5 == 0) goto L_0x05a3;
    L_0x058b:
        r6 = 0;
        r5 = (r30 > r6 ? 1 : (r30 == r6 ? 0 : -1));
        if (r5 <= 0) goto L_0x05a3;
    L_0x0591:
        r0 = r36;
        r5 = r0.mBackupObserver;	 Catch:{ Exception -> 0x04a0 }
        r6 = new android.app.backup.BackupProgress;	 Catch:{ Exception -> 0x04a0 }
        r0 = r30;
        r2 = r32;
        r6.<init>(r0, r2);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnUpdate(r5, r0, r6);	 Catch:{ Exception -> 0x04a0 }
    L_0x05a3:
        if (r27 <= 0) goto L_0x05a7;
    L_0x05a5:
        if (r13 == 0) goto L_0x0556;
    L_0x05a7:
        r5 = -1005; // 0xfffffffffffffc13 float:NaN double:NaN;
        if (r13 != r5) goto L_0x0373;
    L_0x05ab:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Package hit quota limit in-flight ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r7 = ": ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r32;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r7 = " of ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.append(r10);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.w(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.mMonitor;	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r6 = r0.mCurrentPackage;	 Catch:{ Exception -> 0x04a0 }
        r7 = 18;
        r9 = 1;
        r12 = 0;
        r5 = com.android.server.backup.utils.BackupManagerMonitorUtils.monitorEvent(r5, r7, r6, r9, r12);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r0.mMonitor = r5;	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.mBackupRunner;	 Catch:{ Exception -> 0x04a0 }
        r0 = r32;
        r5.sendQuotaExceeded(r0, r10);	 Catch:{ Exception -> 0x04a0 }
        goto L_0x0373;
    L_0x05fe:
        r5 = move-exception;
        r0 = r36;
        r6 = r0.mCancelAll;
        if (r6 == 0) goto L_0x0607;
    L_0x0605:
        r16 = -2003; // 0xfffffffffffff82d float:NaN double:NaN;
    L_0x0607:
        r6 = "PFTBT";
        r7 = new java.lang.StringBuilder;
        r7.<init>();
        r9 = "Full backup completed with status: ";
        r7 = r7.append(r9);
        r0 = r16;
        r7 = r7.append(r0);
        r7 = r7.toString();
        android.util.Slog.i(r6, r7);
        r0 = r36;
        r6 = r0.mBackupObserver;
        r0 = r16;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupFinished(r6, r0);
        r0 = r36;
        r1 = r34;
        r0.cleanUpPipes(r1);
        r0 = r36;
        r1 = r21;
        r0.cleanUpPipes(r1);
        r36.unregisterTask();
        r0 = r36;
        r6 = r0.mJob;
        if (r6 == 0) goto L_0x064a;
    L_0x0643:
        r0 = r36;
        r6 = r0.mJob;
        r6.finishBackupPass();
    L_0x064a:
        r0 = r36;
        r6 = r0.backupManagerService;
        r6 = r6.getQueueLock();
        monitor-enter(r6);
        r0 = r36;
        r7 = r0.backupManagerService;	 Catch:{ all -> 0x082d }
        r9 = 0;
        r7.setRunningFullBackupTask(r9);	 Catch:{ all -> 0x082d }
        monitor-exit(r6);
        r0 = r36;
        r6 = r0.mLatch;
        r6.countDown();
        r0 = r36;
        r6 = r0.mUpdateSchedule;
        if (r6 == 0) goto L_0x0670;
    L_0x0669:
        r0 = r36;
        r6 = r0.backupManagerService;
        r6.scheduleNextFullBackupJob(r14);
    L_0x0670:
        r6 = "PFTBT";
        r7 = "Full data backup pass finished.";
        android.util.Slog.i(r6, r7);
        r0 = r36;
        r6 = r0.backupManagerService;
        r6 = r6.getWakelock();
        r6.release();
        throw r5;
    L_0x0685:
        r5 = move-exception;
        monitor-exit(r6);	 Catch:{ Exception -> 0x04a0 }
        throw r5;	 Catch:{ Exception -> 0x04a0 }
    L_0x0688:
        r0 = r36;
        r5 = r0.mTransport;	 Catch:{ all -> 0x0691 }
        r5.cancelFullBackup();	 Catch:{ all -> 0x0691 }
        goto L_0x0399;
    L_0x0691:
        r5 = move-exception;
        monitor-exit(r6);	 Catch:{ Exception -> 0x04a0 }
        throw r5;	 Catch:{ Exception -> 0x04a0 }
    L_0x0694:
        r5 = -1005; // 0xfffffffffffffc13 float:NaN double:NaN;
        if (r13 != r5) goto L_0x06c8;
    L_0x0698:
        r0 = r36;
        r5 = r0.mBackupObserver;	 Catch:{ Exception -> 0x04a0 }
        r6 = -1005; // 0xfffffffffffffc13 float:NaN double:NaN;
        r0 = r29;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r5, r0, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Transport quota exceeded for package: ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.i(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = 2845; // 0xb1d float:3.987E-42 double:1.4056E-320;
        r0 = r29;
        android.util.EventLog.writeEvent(r5, r0);	 Catch:{ Exception -> 0x04a0 }
        goto L_0x0441;
    L_0x06c8:
        r5 = -1003; // 0xfffffffffffffc15 float:NaN double:NaN;
        if (r13 != r5) goto L_0x0705;
    L_0x06cc:
        r0 = r36;
        r5 = r0.mBackupObserver;	 Catch:{ Exception -> 0x04a0 }
        r6 = -1003; // 0xfffffffffffffc15 float:NaN double:NaN;
        r0 = r29;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r5, r0, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Application failure for package: ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.w(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = 2823; // 0xb07 float:3.956E-42 double:1.3947E-320;
        r0 = r29;
        android.util.EventLog.writeEvent(r5, r0);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r6 = r8.applicationInfo;	 Catch:{ Exception -> 0x04a0 }
        r5.tearDownAgentAndKill(r6);	 Catch:{ Exception -> 0x04a0 }
        goto L_0x0441;
    L_0x0705:
        r5 = -2003; // 0xfffffffffffff82d float:NaN double:NaN;
        if (r13 != r5) goto L_0x0751;
    L_0x0709:
        r0 = r36;
        r5 = r0.mBackupObserver;	 Catch:{ Exception -> 0x04a0 }
        r6 = -2003; // 0xfffffffffffff82d float:NaN double:NaN;
        r0 = r29;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r5, r0, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Backup cancelled. package=";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r6 = r6.append(r0);	 Catch:{ Exception -> 0x04a0 }
        r7 = ", cancelAll=";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r7 = r0.mCancelAll;	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.w(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = 2846; // 0xb1e float:3.988E-42 double:1.406E-320;
        r0 = r29;
        android.util.EventLog.writeEvent(r5, r0);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r6 = r8.applicationInfo;	 Catch:{ Exception -> 0x04a0 }
        r5.tearDownAgentAndKill(r6);	 Catch:{ Exception -> 0x04a0 }
        goto L_0x0441;
    L_0x0751:
        if (r13 == 0) goto L_0x080b;
    L_0x0753:
        r0 = r36;
        r5 = r0.mBackupObserver;	 Catch:{ Exception -> 0x04a0 }
        r6 = -1000; // 0xfffffffffffffc18 float:NaN double:NaN;
        r0 = r29;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r5, r0, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04a0 }
        r6.<init>();	 Catch:{ Exception -> 0x04a0 }
        r7 = "Transport failed; aborting backup: ";
        r6 = r6.append(r7);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.append(r13);	 Catch:{ Exception -> 0x04a0 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x04a0 }
        android.util.Slog.w(r5, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = 0;
        r5 = new java.lang.Object[r5];	 Catch:{ Exception -> 0x04a0 }
        r6 = 2842; // 0xb1a float:3.982E-42 double:1.404E-320;
        android.util.EventLog.writeEvent(r6, r5);	 Catch:{ Exception -> 0x04a0 }
        r16 = -1000; // 0xfffffffffffffc18 float:NaN double:NaN;
        r0 = r36;
        r5 = r0.mCancelAll;
        if (r5 == 0) goto L_0x078a;
    L_0x0788:
        r16 = -2003; // 0xfffffffffffff82d float:NaN double:NaN;
    L_0x078a:
        r5 = "PFTBT";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Full backup completed with status: ";
        r6 = r6.append(r7);
        r0 = r16;
        r6 = r6.append(r0);
        r6 = r6.toString();
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.mBackupObserver;
        r0 = r16;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupFinished(r5, r0);
        r0 = r36;
        r1 = r34;
        r0.cleanUpPipes(r1);
        r0 = r36;
        r1 = r21;
        r0.cleanUpPipes(r1);
        r36.unregisterTask();
        r0 = r36;
        r5 = r0.mJob;
        if (r5 == 0) goto L_0x07cd;
    L_0x07c6:
        r0 = r36;
        r5 = r0.mJob;
        r5.finishBackupPass();
    L_0x07cd:
        r0 = r36;
        r5 = r0.backupManagerService;
        r6 = r5.getQueueLock();
        monitor-enter(r6);
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ all -> 0x0808 }
        r7 = 0;
        r5.setRunningFullBackupTask(r7);	 Catch:{ all -> 0x0808 }
        monitor-exit(r6);
        r0 = r36;
        r5 = r0.mLatch;
        r5.countDown();
        r0 = r36;
        r5 = r0.mUpdateSchedule;
        if (r5 == 0) goto L_0x07f3;
    L_0x07ec:
        r0 = r36;
        r5 = r0.backupManagerService;
        r5.scheduleNextFullBackupJob(r14);
    L_0x07f3:
        r5 = "PFTBT";
        r6 = "Full data backup pass finished.";
        android.util.Slog.i(r5, r6);
        r0 = r36;
        r5 = r0.backupManagerService;
        r5 = r5.getWakelock();
        r5.release();
        return;
    L_0x0808:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x080b:
        r0 = r36;
        r5 = r0.mBackupObserver;	 Catch:{ Exception -> 0x04a0 }
        r6 = 0;
        r0 = r29;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r5, r0, r6);	 Catch:{ Exception -> 0x04a0 }
        r5 = 2843; // 0xb1b float:3.984E-42 double:1.4046E-320;
        r0 = r29;
        android.util.EventLog.writeEvent(r5, r0);	 Catch:{ Exception -> 0x04a0 }
        r0 = r36;
        r5 = r0.backupManagerService;	 Catch:{ Exception -> 0x04a0 }
        r0 = r29;
        r5.logBackupComplete(r0);	 Catch:{ Exception -> 0x04a0 }
        goto L_0x0441;
    L_0x0827:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x082a:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x082d:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x0830:
        r19 = move-exception;
        goto L_0x0499;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.backup.fullbackup.PerformFullTransportBackupTask.run():void");
    }

    void cleanUpPipes(ParcelFileDescriptor[] pipes) {
        if (pipes != null) {
            ParcelFileDescriptor fd;
            if (pipes[0] != null) {
                fd = pipes[0];
                pipes[0] = null;
                try {
                    fd.close();
                } catch (IOException e) {
                    Slog.w(TAG, "Unable to close pipe!");
                }
            }
            if (pipes[1] != null) {
                fd = pipes[1];
                pipes[1] = null;
                try {
                    fd.close();
                } catch (IOException e2) {
                    Slog.w(TAG, "Unable to close pipe!");
                }
            }
        }
    }
}
