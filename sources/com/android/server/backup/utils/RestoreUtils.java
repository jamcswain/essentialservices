package com.android.server.backup.utils;

public class RestoreUtils {
    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean installApk(java.io.InputStream r22, android.content.pm.PackageManager r23, com.android.server.backup.restore.RestoreInstallObserver r24, com.android.server.backup.restore.RestoreDeleteObserver r25, java.util.HashMap<java.lang.String, android.content.pm.Signature[]> r26, java.util.HashMap<java.lang.String, com.android.server.backup.restore.RestorePolicy> r27, com.android.server.backup.FileMetadata r28, java.lang.String r29, com.android.server.backup.utils.BytesReadListener r30, java.io.File r31) {
        /*
        r10 = 1;
        r19 = "BackupManagerService";
        r20 = new java.lang.StringBuilder;
        r20.<init>();
        r21 = "Installing from backup: ";
        r20 = r20.append(r21);
        r0 = r28;
        r0 = r0.packageName;
        r21 = r0;
        r20 = r20.append(r21);
        r20 = r20.toString();
        android.util.Slog.d(r19, r20);
        r4 = new java.io.File;
        r0 = r28;
        r0 = r0.packageName;
        r19 = r0;
        r0 = r31;
        r1 = r19;
        r4.<init>(r0, r1);
        r5 = new java.io.FileOutputStream;	 Catch:{ IOException -> 0x0131 }
        r5.<init>(r4);	 Catch:{ IOException -> 0x0131 }
        r19 = 32768; // 0x8000 float:4.5918E-41 double:1.61895E-319;
        r0 = r19;
        r6 = new byte[r0];	 Catch:{ IOException -> 0x0131 }
        r0 = r28;
        r14 = r0.size;	 Catch:{ IOException -> 0x0131 }
    L_0x0040:
        r20 = 0;
        r19 = (r14 > r20 ? 1 : (r14 == r20 ? 0 : -1));
        if (r19 <= 0) goto L_0x0087;
    L_0x0046:
        r0 = r6.length;	 Catch:{ IOException -> 0x0131 }
        r19 = r0;
        r0 = r19;
        r0 = (long) r0;	 Catch:{ IOException -> 0x0131 }
        r20 = r0;
        r19 = (r20 > r14 ? 1 : (r20 == r14 ? 0 : -1));
        if (r19 >= 0) goto L_0x0084;
    L_0x0052:
        r0 = r6.length;	 Catch:{ IOException -> 0x0131 }
        r19 = r0;
        r0 = r19;
        r0 = (long) r0;	 Catch:{ IOException -> 0x0131 }
        r16 = r0;
    L_0x005a:
        r0 = r16;
        r0 = (int) r0;	 Catch:{ IOException -> 0x0131 }
        r19 = r0;
        r20 = 0;
        r0 = r22;
        r1 = r20;
        r2 = r19;
        r7 = r0.read(r6, r1, r2);	 Catch:{ IOException -> 0x0131 }
        if (r7 < 0) goto L_0x0077;
    L_0x006d:
        r0 = (long) r7;	 Catch:{ IOException -> 0x0131 }
        r20 = r0;
        r0 = r30;
        r1 = r20;
        r0.onBytesRead(r1);	 Catch:{ IOException -> 0x0131 }
    L_0x0077:
        r19 = 0;
        r0 = r19;
        r5.write(r6, r0, r7);	 Catch:{ IOException -> 0x0131 }
        r0 = (long) r7;	 Catch:{ IOException -> 0x0131 }
        r20 = r0;
        r14 = r14 - r20;
        goto L_0x0040;
    L_0x0084:
        r16 = r14;
        goto L_0x005a;
    L_0x0087:
        r5.close();	 Catch:{ IOException -> 0x0131 }
        r19 = 1;
        r20 = 0;
        r0 = r19;
        r1 = r20;
        r4.setReadable(r0, r1);	 Catch:{ IOException -> 0x0131 }
        r11 = android.net.Uri.fromFile(r4);	 Catch:{ IOException -> 0x0131 }
        r24.reset();	 Catch:{ IOException -> 0x0131 }
        r19 = 34;
        r0 = r23;
        r1 = r24;
        r2 = r19;
        r3 = r29;
        r0.installPackage(r11, r1, r2, r3);	 Catch:{ IOException -> 0x0131 }
        r24.waitForCompletion();	 Catch:{ IOException -> 0x0131 }
        r19 = r24.getResult();	 Catch:{ IOException -> 0x0131 }
        r20 = 1;
        r0 = r19;
        r1 = r20;
        if (r0 == r1) goto L_0x00d3;
    L_0x00b8:
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ IOException -> 0x0131 }
        r19 = r0;
        r0 = r27;
        r1 = r19;
        r19 = r0.get(r1);	 Catch:{ IOException -> 0x0131 }
        r20 = com.android.server.backup.restore.RestorePolicy.ACCEPT;	 Catch:{ IOException -> 0x0131 }
        r0 = r19;
        r1 = r20;
        if (r0 == r1) goto L_0x00cf;
    L_0x00ce:
        r10 = 0;
    L_0x00cf:
        r4.delete();
    L_0x00d2:
        return r10;
    L_0x00d3:
        r18 = 0;
        r19 = r24.getPackageName();	 Catch:{ IOException -> 0x0131 }
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ IOException -> 0x0131 }
        r20 = r0;
        r19 = r19.equals(r20);	 Catch:{ IOException -> 0x0131 }
        if (r19 != 0) goto L_0x0140;
    L_0x00e5:
        r19 = "BackupManagerService";
        r20 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x0131 }
        r20.<init>();	 Catch:{ IOException -> 0x0131 }
        r21 = "Restore stream claimed to include apk for ";
        r20 = r20.append(r21);	 Catch:{ IOException -> 0x0131 }
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ IOException -> 0x0131 }
        r21 = r0;
        r20 = r20.append(r21);	 Catch:{ IOException -> 0x0131 }
        r21 = " but apk was really ";
        r20 = r20.append(r21);	 Catch:{ IOException -> 0x0131 }
        r21 = r24.getPackageName();	 Catch:{ IOException -> 0x0131 }
        r20 = r20.append(r21);	 Catch:{ IOException -> 0x0131 }
        r20 = r20.toString();	 Catch:{ IOException -> 0x0131 }
        android.util.Slog.w(r19, r20);	 Catch:{ IOException -> 0x0131 }
        r10 = 0;
        r18 = 1;
    L_0x0117:
        if (r18 == 0) goto L_0x00cf;
    L_0x0119:
        r25.reset();	 Catch:{ IOException -> 0x0131 }
        r19 = r24.getPackageName();	 Catch:{ IOException -> 0x0131 }
        r20 = 0;
        r0 = r23;
        r1 = r19;
        r2 = r25;
        r3 = r20;
        r0.deletePackage(r1, r2, r3);	 Catch:{ IOException -> 0x0131 }
        r25.waitForCompletion();	 Catch:{ IOException -> 0x0131 }
        goto L_0x00cf;
    L_0x0131:
        r9 = move-exception;
        r19 = "BackupManagerService";
        r20 = "Unable to transcribe restored apk for install";
        android.util.Slog.e(r19, r20);	 Catch:{ all -> 0x0241 }
        r10 = 0;
        r4.delete();
        goto L_0x00d2;
    L_0x0140:
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = r0;
        r20 = 64;
        r0 = r23;
        r1 = r19;
        r2 = r20;
        r12 = r0.getPackageInfo(r1, r2);	 Catch:{ NameNotFoundException -> 0x0216 }
        r0 = r12.applicationInfo;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = r0;
        r0 = r19;
        r0 = r0.flags;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = r0;
        r20 = 32768; // 0x8000 float:4.5918E-41 double:1.61895E-319;
        r19 = r19 & r20;
        if (r19 != 0) goto L_0x018c;
    L_0x0163:
        r19 = "BackupManagerService";
        r20 = new java.lang.StringBuilder;	 Catch:{ NameNotFoundException -> 0x0216 }
        r20.<init>();	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = "Restore stream contains apk of package ";
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = r0;
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = " but it disallows backup/restore";
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r20 = r20.toString();	 Catch:{ NameNotFoundException -> 0x0216 }
        android.util.Slog.w(r19, r20);	 Catch:{ NameNotFoundException -> 0x0216 }
        r10 = 0;
        goto L_0x0117;
    L_0x018c:
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = r0;
        r0 = r26;
        r1 = r19;
        r13 = r0.get(r1);	 Catch:{ NameNotFoundException -> 0x0216 }
        r13 = (android.content.pm.Signature[]) r13;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = com.android.server.backup.utils.AppBackupUtils.signaturesMatch(r13, r12);	 Catch:{ NameNotFoundException -> 0x0216 }
        if (r19 == 0) goto L_0x01ea;
    L_0x01a2:
        r0 = r12.applicationInfo;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = r0;
        r0 = r19;
        r0 = r0.uid;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = r0;
        r20 = 10000; // 0x2710 float:1.4013E-41 double:4.9407E-320;
        r0 = r19;
        r1 = r20;
        if (r0 >= r1) goto L_0x0117;
    L_0x01b4:
        r0 = r12.applicationInfo;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = r0;
        r0 = r19;
        r0 = r0.backupAgentName;	 Catch:{ NameNotFoundException -> 0x0216 }
        r19 = r0;
        if (r19 != 0) goto L_0x0117;
    L_0x01c0:
        r19 = "BackupManagerService";
        r20 = new java.lang.StringBuilder;	 Catch:{ NameNotFoundException -> 0x0216 }
        r20.<init>();	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = "Installed app ";
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = r0;
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = " has restricted uid and no agent";
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r20 = r20.toString();	 Catch:{ NameNotFoundException -> 0x0216 }
        android.util.Slog.w(r19, r20);	 Catch:{ NameNotFoundException -> 0x0216 }
        r10 = 0;
        goto L_0x0117;
    L_0x01ea:
        r19 = "BackupManagerService";
        r20 = new java.lang.StringBuilder;	 Catch:{ NameNotFoundException -> 0x0216 }
        r20.<init>();	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = "Installed app ";
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = r0;
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r21 = " signatures do not match restore manifest";
        r20 = r20.append(r21);	 Catch:{ NameNotFoundException -> 0x0216 }
        r20 = r20.toString();	 Catch:{ NameNotFoundException -> 0x0216 }
        android.util.Slog.w(r19, r20);	 Catch:{ NameNotFoundException -> 0x0216 }
        r10 = 0;
        r18 = 1;
        goto L_0x0117;
    L_0x0216:
        r8 = move-exception;
        r19 = "BackupManagerService";
        r20 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x0131 }
        r20.<init>();	 Catch:{ IOException -> 0x0131 }
        r21 = "Install of package ";
        r20 = r20.append(r21);	 Catch:{ IOException -> 0x0131 }
        r0 = r28;
        r0 = r0.packageName;	 Catch:{ IOException -> 0x0131 }
        r21 = r0;
        r20 = r20.append(r21);	 Catch:{ IOException -> 0x0131 }
        r21 = " succeeded but now not found";
        r20 = r20.append(r21);	 Catch:{ IOException -> 0x0131 }
        r20 = r20.toString();	 Catch:{ IOException -> 0x0131 }
        android.util.Slog.w(r19, r20);	 Catch:{ IOException -> 0x0131 }
        r10 = 0;
        goto L_0x0117;
    L_0x0241:
        r19 = move-exception;
        r4.delete();
        throw r19;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.backup.utils.RestoreUtils.installApk(java.io.InputStream, android.content.pm.PackageManager, com.android.server.backup.restore.RestoreInstallObserver, com.android.server.backup.restore.RestoreDeleteObserver, java.util.HashMap, java.util.HashMap, com.android.server.backup.FileMetadata, java.lang.String, com.android.server.backup.utils.BytesReadListener, java.io.File):boolean");
    }
}
