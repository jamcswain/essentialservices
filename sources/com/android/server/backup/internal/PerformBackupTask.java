package com.android.server.backup.internal;

import android.app.IBackupAgent;
import android.app.IBackupAgent.Stub;
import android.app.backup.BackupDataOutput;
import android.app.backup.IBackupManagerMonitor;
import android.app.backup.IBackupObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.SELinux;
import android.os.WorkSource;
import android.util.EventLog;
import android.util.Slog;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.backup.IBackupTransport;
import com.android.server.AppWidgetBackupBridge;
import com.android.server.EventLogTags;
import com.android.server.backup.BackupRestoreTask;
import com.android.server.backup.DataChangedJournal;
import com.android.server.backup.KeyValueBackupJob;
import com.android.server.backup.PackageManagerBackupAgent;
import com.android.server.backup.RefactoredBackupManagerService;
import com.android.server.backup.fullbackup.PerformFullTransportBackupTask;
import com.android.server.backup.utils.AppBackupUtils;
import com.android.server.backup.utils.BackupObserverUtils;
import com.android.server.job.JobSchedulerShellCommand;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

public class PerformBackupTask implements BackupRestoreTask {
    private static final /* synthetic */ int[] -com-android-server-backup-internal-BackupStateSwitchesValues = null;
    private static final String TAG = "PerformBackupTask";
    private RefactoredBackupManagerService backupManagerService;
    IBackupAgent mAgentBinder;
    ParcelFileDescriptor mBackupData;
    File mBackupDataName;
    private volatile boolean mCancelAll;
    private final Object mCancelLock = new Object();
    private final int mCurrentOpToken;
    PackageInfo mCurrentPackage;
    BackupState mCurrentState;
    private volatile int mEphemeralOpToken;
    boolean mFinished;
    private final PerformFullTransportBackupTask mFullBackupTask;
    DataChangedJournal mJournal;
    IBackupManagerMonitor mMonitor;
    ParcelFileDescriptor mNewState;
    File mNewStateName;
    final boolean mNonIncremental;
    IBackupObserver mObserver;
    ArrayList<BackupRequest> mOriginalQueue;
    List<String> mPendingFullBackups;
    ArrayList<BackupRequest> mQueue;
    ParcelFileDescriptor mSavedState;
    File mSavedStateName;
    File mStateDir;
    int mStatus;
    IBackupTransport mTransport;
    final boolean mUserInitiated;

    private static /* synthetic */ int[] -getcom-android-server-backup-internal-BackupStateSwitchesValues() {
        if (-com-android-server-backup-internal-BackupStateSwitchesValues != null) {
            return -com-android-server-backup-internal-BackupStateSwitchesValues;
        }
        int[] iArr = new int[BackupState.values().length];
        try {
            iArr[BackupState.FINAL.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            iArr[BackupState.INITIAL.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            iArr[BackupState.RUNNING_QUEUE.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        -com-android-server-backup-internal-BackupStateSwitchesValues = iArr;
        return iArr;
    }

    public PerformBackupTask(RefactoredBackupManagerService backupManagerService, IBackupTransport transport, String dirName, ArrayList<BackupRequest> queue, DataChangedJournal journal, IBackupObserver observer, IBackupManagerMonitor monitor, List<String> pendingFullBackups, boolean userInitiated, boolean nonIncremental) {
        this.backupManagerService = backupManagerService;
        this.mTransport = transport;
        this.mOriginalQueue = queue;
        this.mQueue = new ArrayList();
        this.mJournal = journal;
        this.mObserver = observer;
        this.mMonitor = monitor;
        this.mPendingFullBackups = pendingFullBackups;
        this.mUserInitiated = userInitiated;
        this.mNonIncremental = nonIncremental;
        this.mStateDir = new File(backupManagerService.getBaseStateDir(), dirName);
        this.mCurrentOpToken = backupManagerService.generateRandomIntegerToken();
        this.mFinished = false;
        synchronized (backupManagerService.getCurrentOpLock()) {
            if (backupManagerService.isBackupOperationInProgress()) {
                Slog.d(TAG, "Skipping backup since one is already in progress.");
                this.mCancelAll = true;
                this.mFullBackupTask = null;
                this.mCurrentState = BackupState.FINAL;
                backupManagerService.addBackupTrace("Skipped. Backup already in progress.");
            } else {
                this.mCurrentState = BackupState.INITIAL;
                RefactoredBackupManagerService refactoredBackupManagerService = backupManagerService;
                this.mFullBackupTask = new PerformFullTransportBackupTask(refactoredBackupManagerService, null, (String[]) this.mPendingFullBackups.toArray(new String[this.mPendingFullBackups.size()]), false, null, new CountDownLatch(1), this.mObserver, this.mMonitor, this.mUserInitiated);
                registerTask();
                backupManagerService.addBackupTrace("STATE => INITIAL");
            }
        }
    }

    private void registerTask() {
        synchronized (this.backupManagerService.getCurrentOpLock()) {
            this.backupManagerService.getCurrentOperations().put(this.mCurrentOpToken, new Operation(0, this, 2));
        }
    }

    private void unregisterTask() {
        this.backupManagerService.removeOperation(this.mCurrentOpToken);
    }

    @GuardedBy("mCancelLock")
    public void execute() {
        synchronized (this.mCancelLock) {
            switch (-getcom-android-server-backup-internal-BackupStateSwitchesValues()[this.mCurrentState.ordinal()]) {
                case 1:
                    if (this.mFinished) {
                        Slog.e(TAG, "Duplicate finish");
                    } else {
                        finalizeBackup();
                    }
                    this.mFinished = true;
                    break;
                case 2:
                    beginBackup();
                    break;
                case 3:
                    invokeNextAgent();
                    break;
            }
        }
    }

    void beginBackup() {
        this.backupManagerService.clearBackupTrace();
        StringBuilder b = new StringBuilder(256);
        b.append("beginBackup: [");
        for (BackupRequest req : this.mOriginalQueue) {
            b.append(' ');
            b.append(req.packageName);
        }
        b.append(" ]");
        this.backupManagerService.addBackupTrace(b.toString());
        this.mAgentBinder = null;
        this.mStatus = 0;
        if (this.mOriginalQueue.isEmpty() && this.mPendingFullBackups.isEmpty()) {
            Slog.w(TAG, "Backup begun with an empty queue - nothing to do.");
            this.backupManagerService.addBackupTrace("queue empty at begin");
            BackupObserverUtils.sendBackupFinished(this.mObserver, 0);
            executeNextState(BackupState.FINAL);
            return;
        }
        this.mQueue = (ArrayList) this.mOriginalQueue.clone();
        boolean skipPm = this.mNonIncremental;
        for (int i = 0; i < this.mQueue.size(); i++) {
            if (RefactoredBackupManagerService.PACKAGE_MANAGER_SENTINEL.equals(((BackupRequest) this.mQueue.get(i)).packageName)) {
                this.mQueue.remove(i);
                skipPm = false;
                break;
            }
        }
        Slog.v(TAG, "Beginning backup of " + this.mQueue.size() + " targets");
        File pmState = new File(this.mStateDir, RefactoredBackupManagerService.PACKAGE_MANAGER_SENTINEL);
        try {
            String transportName = this.mTransport.transportDirName();
            EventLog.writeEvent(EventLogTags.BACKUP_START, transportName);
            if (this.mStatus == 0 && pmState.length() <= 0) {
                Slog.i(TAG, "Initializing (wiping) backup state and transport storage");
                this.backupManagerService.addBackupTrace("initializing transport " + transportName);
                this.backupManagerService.resetBackupState(this.mStateDir);
                this.mStatus = this.mTransport.initializeDevice();
                this.backupManagerService.addBackupTrace("transport.initializeDevice() == " + this.mStatus);
                if (this.mStatus == 0) {
                    EventLog.writeEvent(EventLogTags.BACKUP_INITIALIZE, new Object[0]);
                } else {
                    EventLog.writeEvent(EventLogTags.BACKUP_TRANSPORT_FAILURE, "(initialize)");
                    Slog.e(TAG, "Transport error in initializeDevice()");
                }
            }
            if (skipPm) {
                Slog.d(TAG, "Skipping backup of package metadata.");
                executeNextState(BackupState.RUNNING_QUEUE);
            } else if (this.mStatus == 0) {
                this.mStatus = invokeAgentForBackup(RefactoredBackupManagerService.PACKAGE_MANAGER_SENTINEL, Stub.asInterface(new PackageManagerBackupAgent(this.backupManagerService.getPackageManager()).onBind()), this.mTransport);
                this.backupManagerService.addBackupTrace("PMBA invoke: " + this.mStatus);
                this.backupManagerService.getBackupHandler().removeMessages(17);
            }
            if (this.mStatus == JobSchedulerShellCommand.CMD_ERR_NO_JOB) {
                EventLog.writeEvent(EventLogTags.BACKUP_RESET, this.mTransport.transportDirName());
            }
            this.backupManagerService.addBackupTrace("exiting prelim: " + this.mStatus);
            if (this.mStatus != 0) {
                this.backupManagerService.resetBackupState(this.mStateDir);
                BackupObserverUtils.sendBackupFinished(this.mObserver, JobSchedulerShellCommand.CMD_ERR_NO_PACKAGE);
                executeNextState(BackupState.FINAL);
            }
        } catch (Exception e) {
            Slog.e(TAG, "Error in backup thread", e);
            this.backupManagerService.addBackupTrace("Exception in backup thread: " + e);
            this.mStatus = JobSchedulerShellCommand.CMD_ERR_NO_PACKAGE;
            this.backupManagerService.addBackupTrace("exiting prelim: " + this.mStatus);
            if (this.mStatus != 0) {
                this.backupManagerService.resetBackupState(this.mStateDir);
                BackupObserverUtils.sendBackupFinished(this.mObserver, JobSchedulerShellCommand.CMD_ERR_NO_PACKAGE);
                executeNextState(BackupState.FINAL);
            }
        } catch (Throwable th) {
            this.backupManagerService.addBackupTrace("exiting prelim: " + this.mStatus);
            if (this.mStatus != 0) {
                this.backupManagerService.resetBackupState(this.mStateDir);
                BackupObserverUtils.sendBackupFinished(this.mObserver, JobSchedulerShellCommand.CMD_ERR_NO_PACKAGE);
                executeNextState(BackupState.FINAL);
            }
        }
    }

    void invokeNextAgent() {
        this.mStatus = 0;
        this.backupManagerService.addBackupTrace("invoke q=" + this.mQueue.size());
        if (this.mQueue.isEmpty()) {
            executeNextState(BackupState.FINAL);
            return;
        }
        BackupRequest request = (BackupRequest) this.mQueue.get(0);
        this.mQueue.remove(0);
        Slog.d(TAG, "starting key/value backup of " + request);
        this.backupManagerService.addBackupTrace("launch agent for " + request.packageName);
        BackupState nextState;
        try {
            this.mCurrentPackage = this.backupManagerService.getPackageManager().getPackageInfo(request.packageName, 64);
            if (!AppBackupUtils.appIsEligibleForBackup(this.mCurrentPackage.applicationInfo)) {
                Slog.i(TAG, "Package " + request.packageName + " no longer supports backup; skipping");
                this.backupManagerService.addBackupTrace("skipping - not eligible, completion is noop");
                BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2001);
                executeNextState(BackupState.RUNNING_QUEUE);
                this.backupManagerService.getWakelock().setWorkSource(null);
                if (this.mStatus != 0) {
                    nextState = BackupState.RUNNING_QUEUE;
                    this.mAgentBinder = null;
                    if (this.mStatus == -1003) {
                        this.backupManagerService.dataChangedImpl(request.packageName);
                        this.mStatus = 0;
                        if (this.mQueue.isEmpty()) {
                            nextState = BackupState.FINAL;
                        }
                        BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -1003);
                    } else if (this.mStatus == -1004) {
                        this.mStatus = 0;
                        BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2002);
                    } else {
                        revertAndEndBackup();
                        nextState = BackupState.FINAL;
                    }
                    executeNextState(nextState);
                } else {
                    this.backupManagerService.addBackupTrace("expecting completion/timeout callback");
                }
            } else if (AppBackupUtils.appGetsFullBackup(this.mCurrentPackage)) {
                Slog.i(TAG, "Package " + request.packageName + " requests full-data rather than key/value; skipping");
                this.backupManagerService.addBackupTrace("skipping - fullBackupOnly, completion is noop");
                BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2001);
                executeNextState(BackupState.RUNNING_QUEUE);
                this.backupManagerService.getWakelock().setWorkSource(null);
                if (this.mStatus != 0) {
                    nextState = BackupState.RUNNING_QUEUE;
                    this.mAgentBinder = null;
                    if (this.mStatus == -1003) {
                        this.backupManagerService.dataChangedImpl(request.packageName);
                        this.mStatus = 0;
                        if (this.mQueue.isEmpty()) {
                            nextState = BackupState.FINAL;
                        }
                        BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -1003);
                    } else if (this.mStatus == -1004) {
                        this.mStatus = 0;
                        BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2002);
                    } else {
                        revertAndEndBackup();
                        nextState = BackupState.FINAL;
                    }
                    executeNextState(nextState);
                } else {
                    this.backupManagerService.addBackupTrace("expecting completion/timeout callback");
                }
            } else if (AppBackupUtils.appIsStopped(this.mCurrentPackage.applicationInfo)) {
                this.backupManagerService.addBackupTrace("skipping - stopped");
                BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2001);
                executeNextState(BackupState.RUNNING_QUEUE);
                this.backupManagerService.getWakelock().setWorkSource(null);
                if (this.mStatus != 0) {
                    nextState = BackupState.RUNNING_QUEUE;
                    this.mAgentBinder = null;
                    if (this.mStatus == -1003) {
                        this.backupManagerService.dataChangedImpl(request.packageName);
                        this.mStatus = 0;
                        if (this.mQueue.isEmpty()) {
                            nextState = BackupState.FINAL;
                        }
                        BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -1003);
                    } else if (this.mStatus == -1004) {
                        this.mStatus = 0;
                        BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2002);
                    } else {
                        revertAndEndBackup();
                        nextState = BackupState.FINAL;
                    }
                    executeNextState(nextState);
                } else {
                    this.backupManagerService.addBackupTrace("expecting completion/timeout callback");
                }
            } else {
                try {
                    boolean z;
                    this.backupManagerService.getWakelock().setWorkSource(new WorkSource(this.mCurrentPackage.applicationInfo.uid));
                    IBackupAgent agent = this.backupManagerService.bindToAgentSynchronous(this.mCurrentPackage.applicationInfo, 0);
                    RefactoredBackupManagerService refactoredBackupManagerService = this.backupManagerService;
                    StringBuilder append = new StringBuilder().append("agent bound; a? = ");
                    if (agent != null) {
                        z = true;
                    } else {
                        z = false;
                    }
                    refactoredBackupManagerService.addBackupTrace(append.append(z).toString());
                    if (agent != null) {
                        this.mAgentBinder = agent;
                        this.mStatus = invokeAgentForBackup(request.packageName, agent, this.mTransport);
                    } else {
                        this.mStatus = -1003;
                    }
                } catch (SecurityException ex) {
                    Slog.d(TAG, "error in bind/backup", ex);
                    this.mStatus = -1003;
                    this.backupManagerService.addBackupTrace("agent SE");
                }
                this.backupManagerService.getWakelock().setWorkSource(null);
                if (this.mStatus != 0) {
                    nextState = BackupState.RUNNING_QUEUE;
                    this.mAgentBinder = null;
                    if (this.mStatus == -1003) {
                        this.backupManagerService.dataChangedImpl(request.packageName);
                        this.mStatus = 0;
                        if (this.mQueue.isEmpty()) {
                            nextState = BackupState.FINAL;
                        }
                        BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -1003);
                    } else if (this.mStatus == -1004) {
                        this.mStatus = 0;
                        BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2002);
                    } else {
                        revertAndEndBackup();
                        nextState = BackupState.FINAL;
                    }
                    executeNextState(nextState);
                } else {
                    this.backupManagerService.addBackupTrace("expecting completion/timeout callback");
                }
            }
        } catch (NameNotFoundException e) {
            Slog.d(TAG, "Package does not exist; skipping");
            this.backupManagerService.addBackupTrace("no such package");
            this.mStatus = -1004;
            this.backupManagerService.getWakelock().setWorkSource(null);
            if (this.mStatus != 0) {
                nextState = BackupState.RUNNING_QUEUE;
                this.mAgentBinder = null;
                if (this.mStatus == -1003) {
                    this.backupManagerService.dataChangedImpl(request.packageName);
                    this.mStatus = 0;
                    if (this.mQueue.isEmpty()) {
                        nextState = BackupState.FINAL;
                    }
                    BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -1003);
                } else if (this.mStatus == -1004) {
                    this.mStatus = 0;
                    BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2002);
                } else {
                    revertAndEndBackup();
                    nextState = BackupState.FINAL;
                }
                executeNextState(nextState);
            } else {
                this.backupManagerService.addBackupTrace("expecting completion/timeout callback");
            }
        } catch (Throwable th) {
            this.backupManagerService.getWakelock().setWorkSource(null);
            if (this.mStatus != 0) {
                nextState = BackupState.RUNNING_QUEUE;
                this.mAgentBinder = null;
                if (this.mStatus == -1003) {
                    this.backupManagerService.dataChangedImpl(request.packageName);
                    this.mStatus = 0;
                    if (this.mQueue.isEmpty()) {
                        nextState = BackupState.FINAL;
                    }
                    BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -1003);
                } else if (this.mStatus == -1004) {
                    this.mStatus = 0;
                    BackupObserverUtils.sendBackupOnPackageResult(this.mObserver, this.mCurrentPackage.packageName, -2002);
                } else {
                    revertAndEndBackup();
                    nextState = BackupState.FINAL;
                }
                executeNextState(nextState);
            } else {
                this.backupManagerService.addBackupTrace("expecting completion/timeout callback");
            }
        }
    }

    void finalizeBackup() {
        this.backupManagerService.addBackupTrace("finishing");
        for (BackupRequest req : this.mQueue) {
            this.backupManagerService.dataChangedImpl(req.packageName);
        }
        if (!(this.mJournal == null || (this.mJournal.delete() ^ 1) == 0)) {
            Slog.e(TAG, "Unable to remove backup journal file " + this.mJournal);
        }
        if (this.backupManagerService.getCurrentToken() == 0 && this.mStatus == 0) {
            this.backupManagerService.addBackupTrace("success; recording token");
            try {
                this.backupManagerService.setCurrentToken(this.mTransport.getCurrentRestoreSet());
                this.backupManagerService.writeRestoreTokens();
            } catch (Exception e) {
                Slog.e(TAG, "Transport threw reporting restore set: " + e.getMessage());
                this.backupManagerService.addBackupTrace("transport threw returning token");
            }
        }
        synchronized (this.backupManagerService.getQueueLock()) {
            this.backupManagerService.setBackupRunning(false);
            if (this.mStatus == JobSchedulerShellCommand.CMD_ERR_NO_JOB) {
                this.backupManagerService.addBackupTrace("init required; rerunning");
                try {
                    String name = this.backupManagerService.getTransportManager().getTransportName(this.mTransport);
                    if (name != null) {
                        this.backupManagerService.getPendingInits().add(name);
                    } else {
                        Slog.w(TAG, "Couldn't find name of transport " + this.mTransport + " for init");
                    }
                } catch (Exception e2) {
                    Slog.w(TAG, "Failed to query transport name for init: " + e2.getMessage());
                }
                clearMetadata();
                this.backupManagerService.backupNow();
            }
        }
        this.backupManagerService.clearBackupTrace();
        unregisterTask();
        if (this.mCancelAll || this.mStatus != 0 || this.mPendingFullBackups == null || (this.mPendingFullBackups.isEmpty() ^ 1) == 0) {
            if (!this.mCancelAll) {
                this.mFullBackupTask.unregisterTask();
                switch (this.mStatus) {
                    case JobSchedulerShellCommand.CMD_ERR_NO_JOB /*-1001*/:
                        BackupObserverUtils.sendBackupFinished(this.mObserver, JobSchedulerShellCommand.CMD_ERR_NO_PACKAGE);
                        break;
                    case 0:
                        BackupObserverUtils.sendBackupFinished(this.mObserver, 0);
                        break;
                    default:
                        BackupObserverUtils.sendBackupFinished(this.mObserver, JobSchedulerShellCommand.CMD_ERR_NO_PACKAGE);
                        break;
                }
            }
            if (this.mFullBackupTask != null) {
                this.mFullBackupTask.unregisterTask();
            }
            BackupObserverUtils.sendBackupFinished(this.mObserver, -2003);
        } else {
            Slog.d(TAG, "Starting full backups for: " + this.mPendingFullBackups);
            this.backupManagerService.getWakelock().acquire();
            new Thread(this.mFullBackupTask, "full-transport-requested").start();
        }
        Slog.i(TAG, "K/V backup pass finished.");
        this.backupManagerService.getWakelock().release();
    }

    void clearMetadata() {
        File pmState = new File(this.mStateDir, RefactoredBackupManagerService.PACKAGE_MANAGER_SENTINEL);
        if (pmState.exists()) {
            pmState.delete();
        }
    }

    int invokeAgentForBackup(String packageName, IBackupAgent agent, IBackupTransport transport) {
        Slog.d(TAG, "invokeAgentForBackup on " + packageName);
        this.backupManagerService.addBackupTrace("invoking " + packageName);
        File blankStateName = new File(this.mStateDir, "blank_state");
        this.mSavedStateName = new File(this.mStateDir, packageName);
        this.mBackupDataName = new File(this.backupManagerService.getDataDir(), packageName + ".data");
        this.mNewStateName = new File(this.mStateDir, packageName + ".new");
        this.mSavedState = null;
        this.mBackupData = null;
        this.mNewState = null;
        this.mEphemeralOpToken = this.backupManagerService.generateRandomIntegerToken();
        try {
            if (packageName.equals(RefactoredBackupManagerService.PACKAGE_MANAGER_SENTINEL)) {
                this.mCurrentPackage = new PackageInfo();
                this.mCurrentPackage.packageName = packageName;
            }
            this.mSavedState = ParcelFileDescriptor.open(this.mNonIncremental ? blankStateName : this.mSavedStateName, 402653184);
            this.mBackupData = ParcelFileDescriptor.open(this.mBackupDataName, 1006632960);
            if (!SELinux.restorecon(this.mBackupDataName)) {
                Slog.e(TAG, "SELinux restorecon failed on " + this.mBackupDataName);
            }
            this.mNewState = ParcelFileDescriptor.open(this.mNewStateName, 1006632960);
            long quota = this.mTransport.getBackupQuota(packageName, false);
            this.backupManagerService.addBackupTrace("setting timeout");
            this.backupManagerService.prepareOperationTimeout(this.mEphemeralOpToken, 30000, this, 0);
            this.backupManagerService.addBackupTrace("calling agent doBackup()");
            agent.doBackup(this.mSavedState, this.mBackupData, this.mNewState, quota, this.mEphemeralOpToken, this.backupManagerService.getBackupManagerBinder());
            if (this.mNonIncremental) {
                blankStateName.delete();
            }
            this.backupManagerService.addBackupTrace("invoke success");
            return 0;
        } catch (Exception e) {
            int i;
            Slog.e(TAG, "Error invoking for backup on " + packageName + ". " + e);
            this.backupManagerService.addBackupTrace("exception: " + e);
            EventLog.writeEvent(EventLogTags.BACKUP_AGENT_FAILURE, new Object[]{packageName, e.toString()});
            errorCleanup();
            if (false) {
                i = -1003;
            } else {
                i = JobSchedulerShellCommand.CMD_ERR_NO_PACKAGE;
            }
            if (this.mNonIncremental) {
                blankStateName.delete();
            }
            return i;
        } catch (Throwable th) {
            if (this.mNonIncremental) {
                blankStateName.delete();
            }
        }
    }

    public void failAgent(IBackupAgent agent, String message) {
        try {
            agent.fail(message);
        } catch (Exception e) {
            Slog.w(TAG, "Error conveying failure to " + this.mCurrentPackage.packageName);
        }
    }

    private String SHA1Checksum(byte[] input) {
        try {
            byte[] checksum = MessageDigest.getInstance("SHA-1").digest(input);
            StringBuffer sb = new StringBuffer(checksum.length * 2);
            for (byte toHexString : checksum) {
                sb.append(Integer.toHexString(toHexString));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            Slog.e(TAG, "Unable to use SHA-1!");
            return "00";
        }
    }

    private void writeWidgetPayloadIfAppropriate(FileDescriptor fd, String pkgName) throws IOException {
        Throwable th;
        Throwable th2;
        Throwable th3;
        Throwable th4;
        byte[] widgetState = AppWidgetBackupBridge.getWidgetState(pkgName, 0);
        File widgetFile = new File(this.mStateDir, pkgName + "_widget");
        boolean priorStateExists = widgetFile.exists();
        if (priorStateExists || widgetState != null) {
            String str = null;
            if (widgetState != null) {
                str = SHA1Checksum(widgetState);
                if (priorStateExists) {
                    th = null;
                    FileInputStream fileInputStream = null;
                    DataInputStream dataInputStream = null;
                    try {
                        FileInputStream fin = new FileInputStream(widgetFile);
                        try {
                            DataInputStream in = new DataInputStream(fin);
                            try {
                                String priorChecksum = in.readUTF();
                                if (in != null) {
                                    try {
                                        in.close();
                                    } catch (Throwable th5) {
                                        th = th5;
                                    }
                                }
                                if (fin != null) {
                                    try {
                                        fin.close();
                                    } catch (Throwable th6) {
                                        th2 = th6;
                                        if (th != null) {
                                            if (th != th2) {
                                                th.addSuppressed(th2);
                                                th2 = th;
                                            }
                                        }
                                    }
                                }
                                th2 = th;
                                if (th2 != null) {
                                    throw th2;
                                } else if (Objects.equals(str, priorChecksum)) {
                                    return;
                                }
                            } catch (Throwable th7) {
                                th2 = th7;
                                dataInputStream = in;
                                fileInputStream = fin;
                                if (dataInputStream != null) {
                                    try {
                                        dataInputStream.close();
                                    } catch (Throwable th8) {
                                        th4 = th8;
                                        if (th != null) {
                                            if (th != th4) {
                                                th.addSuppressed(th4);
                                                th4 = th;
                                            }
                                        }
                                    }
                                }
                                th4 = th;
                                if (fileInputStream != null) {
                                    try {
                                        fileInputStream.close();
                                    } catch (Throwable th9) {
                                        th = th9;
                                        if (th4 != null) {
                                            if (th4 != th) {
                                                th4.addSuppressed(th);
                                                th = th4;
                                            }
                                        }
                                    }
                                }
                                th = th4;
                                if (th != null) {
                                    throw th;
                                }
                                throw th2;
                            }
                        } catch (Throwable th10) {
                            th2 = th10;
                            fileInputStream = fin;
                            if (dataInputStream != null) {
                                dataInputStream.close();
                            }
                            th4 = th;
                            if (fileInputStream != null) {
                                fileInputStream.close();
                            }
                            th = th4;
                            if (th != null) {
                                throw th2;
                            }
                            throw th;
                        }
                    } catch (Throwable th11) {
                        th2 = th11;
                        if (dataInputStream != null) {
                            dataInputStream.close();
                        }
                        th4 = th;
                        if (fileInputStream != null) {
                            fileInputStream.close();
                        }
                        th = th4;
                        if (th != null) {
                            throw th;
                        }
                        throw th2;
                    }
                }
            }
            BackupDataOutput out = new BackupDataOutput(fd);
            if (widgetState != null) {
                th = null;
                FileOutputStream fileOutputStream = null;
                DataOutputStream dataOutputStream = null;
                try {
                    FileOutputStream fout = new FileOutputStream(widgetFile);
                    try {
                        DataOutputStream stateOut = new DataOutputStream(fout);
                        try {
                            stateOut.writeUTF(str);
                            if (stateOut != null) {
                                try {
                                    stateOut.close();
                                } catch (Throwable th12) {
                                    th = th12;
                                }
                            }
                            if (fout != null) {
                                try {
                                    fout.close();
                                } catch (Throwable th13) {
                                    th2 = th13;
                                    if (th != null) {
                                        if (th != th2) {
                                            th.addSuppressed(th2);
                                            th2 = th;
                                        }
                                    }
                                }
                            }
                            th2 = th;
                            if (th2 != null) {
                                throw th2;
                            }
                            out.writeEntityHeader(RefactoredBackupManagerService.KEY_WIDGET_STATE, widgetState.length);
                            out.writeEntityData(widgetState, widgetState.length);
                        } catch (Throwable th14) {
                            th2 = th14;
                            dataOutputStream = stateOut;
                            fileOutputStream = fout;
                            if (dataOutputStream != null) {
                                try {
                                    dataOutputStream.close();
                                } catch (Throwable th15) {
                                    th4 = th15;
                                    if (th != null) {
                                        if (th != th4) {
                                            th.addSuppressed(th4);
                                            th4 = th;
                                        }
                                    }
                                }
                            }
                            th4 = th;
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (Throwable th16) {
                                    th = th16;
                                    if (th4 != null) {
                                        if (th4 != th) {
                                            th4.addSuppressed(th);
                                            th = th4;
                                        }
                                    }
                                }
                            }
                            th = th4;
                            if (th != null) {
                                throw th;
                            }
                            throw th2;
                        }
                    } catch (Throwable th17) {
                        th2 = th17;
                        fileOutputStream = fout;
                        if (dataOutputStream != null) {
                            dataOutputStream.close();
                        }
                        th4 = th;
                        if (fileOutputStream != null) {
                            fileOutputStream.close();
                        }
                        th = th4;
                        if (th != null) {
                            throw th2;
                        }
                        throw th;
                    }
                } catch (Throwable th18) {
                    th2 = th18;
                    if (dataOutputStream != null) {
                        dataOutputStream.close();
                    }
                    th4 = th;
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                    th = th4;
                    if (th != null) {
                        throw th;
                    }
                    throw th2;
                }
            }
            out.writeEntityHeader(RefactoredBackupManagerService.KEY_WIDGET_STATE, -1);
            widgetFile.delete();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @com.android.internal.annotations.GuardedBy("mCancelLock")
    public void operationComplete(long r31) {
        /*
        r30 = this;
        r0 = r30;
        r0 = r0.backupManagerService;
        r24 = r0;
        r0 = r30;
        r0 = r0.mEphemeralOpToken;
        r25 = r0;
        r24.removeOperation(r25);
        r0 = r30;
        r0 = r0.mCancelLock;
        r25 = r0;
        monitor-enter(r25);
        r0 = r30;
        r0 = r0.mFinished;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        if (r24 == 0) goto L_0x002d;
    L_0x001e:
        r24 = "PerformBackupTask";
        r26 = "operationComplete received after task finished.";
        r0 = r24;
        r1 = r26;
        android.util.Slog.d(r0, r1);	 Catch:{ all -> 0x0395 }
        monitor-exit(r25);
        return;
    L_0x002d:
        r0 = r30;
        r0 = r0.mBackupData;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        if (r24 != 0) goto L_0x0074;
    L_0x0035:
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        if (r24 == 0) goto L_0x0070;
    L_0x003d:
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r0 = r24;
        r0 = r0.packageName;	 Catch:{ all -> 0x0395 }
        r17 = r0;
    L_0x0049:
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r26 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0395 }
        r26.<init>();	 Catch:{ all -> 0x0395 }
        r27 = "late opComplete; curPkg = ";
        r26 = r26.append(r27);	 Catch:{ all -> 0x0395 }
        r0 = r26;
        r1 = r17;
        r26 = r0.append(r1);	 Catch:{ all -> 0x0395 }
        r26 = r26.toString();	 Catch:{ all -> 0x0395 }
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ all -> 0x0395 }
        monitor-exit(r25);
        return;
    L_0x0070:
        r17 = "[none]";
        goto L_0x0049;
    L_0x0074:
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r0 = r24;
        r0 = r0.packageName;	 Catch:{ all -> 0x0395 }
        r18 = r0;
        r0 = r30;
        r0 = r0.mBackupDataName;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r12 = r24.length();	 Catch:{ all -> 0x0395 }
        r0 = r30;
        r0 = r0.mBackupData;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r10 = r24.getFileDescriptor();	 Catch:{ all -> 0x0395 }
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ IOException -> 0x01ce }
        r24 = r0;
        r0 = r24;
        r0 = r0.applicationInfo;	 Catch:{ IOException -> 0x01ce }
        r24 = r0;
        if (r24 == 0) goto L_0x038c;
    L_0x00a2:
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ IOException -> 0x01ce }
        r24 = r0;
        r0 = r24;
        r0 = r0.applicationInfo;	 Catch:{ IOException -> 0x01ce }
        r24 = r0;
        r0 = r24;
        r0 = r0.flags;	 Catch:{ IOException -> 0x01ce }
        r24 = r0;
        r24 = r24 & 1;
        if (r24 != 0) goto L_0x038c;
    L_0x00b8:
        r0 = r30;
        r0 = r0.mBackupDataName;	 Catch:{ IOException -> 0x01ce }
        r24 = r0;
        r26 = 268435456; // 0x10000000 float:2.5243549E-29 double:1.32624737E-315;
        r0 = r24;
        r1 = r26;
        r19 = android.os.ParcelFileDescriptor.open(r0, r1);	 Catch:{ IOException -> 0x01ce }
        r14 = new android.app.backup.BackupDataInput;	 Catch:{ IOException -> 0x01ce }
        r24 = r19.getFileDescriptor();	 Catch:{ IOException -> 0x01ce }
        r0 = r24;
        r14.<init>(r0);	 Catch:{ IOException -> 0x01ce }
    L_0x00d3:
        r24 = r14.readNextHeader();	 Catch:{ all -> 0x01c7 }
        if (r24 == 0) goto L_0x0387;
    L_0x00d9:
        r15 = r14.getKey();	 Catch:{ all -> 0x01c7 }
        if (r15 == 0) goto L_0x01c2;
    L_0x00df:
        r24 = 0;
        r0 = r24;
        r24 = r15.charAt(r0);	 Catch:{ all -> 0x01c7 }
        r26 = 65280; // 0xff00 float:9.1477E-41 double:3.22526E-319;
        r0 = r24;
        r1 = r26;
        if (r0 < r1) goto L_0x01c2;
    L_0x00f0:
        r0 = r30;
        r0 = r0.mAgentBinder;	 Catch:{ all -> 0x01c7 }
        r24 = r0;
        r26 = new java.lang.StringBuilder;	 Catch:{ all -> 0x01c7 }
        r26.<init>();	 Catch:{ all -> 0x01c7 }
        r27 = "Illegal backup key: ";
        r26 = r26.append(r27);	 Catch:{ all -> 0x01c7 }
        r0 = r26;
        r26 = r0.append(r15);	 Catch:{ all -> 0x01c7 }
        r26 = r26.toString();	 Catch:{ all -> 0x01c7 }
        r0 = r30;
        r1 = r24;
        r2 = r26;
        r0.failAgent(r1, r2);	 Catch:{ all -> 0x01c7 }
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ all -> 0x01c7 }
        r24 = r0;
        r26 = new java.lang.StringBuilder;	 Catch:{ all -> 0x01c7 }
        r26.<init>();	 Catch:{ all -> 0x01c7 }
        r27 = "illegal key ";
        r26 = r26.append(r27);	 Catch:{ all -> 0x01c7 }
        r0 = r26;
        r26 = r0.append(r15);	 Catch:{ all -> 0x01c7 }
        r27 = " from ";
        r26 = r26.append(r27);	 Catch:{ all -> 0x01c7 }
        r0 = r26;
        r1 = r18;
        r26 = r0.append(r1);	 Catch:{ all -> 0x01c7 }
        r26 = r26.toString();	 Catch:{ all -> 0x01c7 }
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ all -> 0x01c7 }
        r24 = 2;
        r0 = r24;
        r0 = new java.lang.Object[r0];	 Catch:{ all -> 0x01c7 }
        r24 = r0;
        r26 = 0;
        r24[r26] = r18;	 Catch:{ all -> 0x01c7 }
        r26 = "bad key";
        r27 = 1;
        r24[r27] = r26;	 Catch:{ all -> 0x01c7 }
        r26 = 2823; // 0xb07 float:3.956E-42 double:1.3947E-320;
        r0 = r26;
        r1 = r24;
        android.util.EventLog.writeEvent(r0, r1);	 Catch:{ all -> 0x01c7 }
        r0 = r30;
        r0 = r0.mMonitor;	 Catch:{ all -> 0x01c7 }
        r24 = r0;
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ all -> 0x01c7 }
        r26 = r0;
        r27 = "android.app.backup.extra.LOG_ILLEGAL_KEY";
        r28 = 0;
        r0 = r28;
        r1 = r27;
        r27 = com.android.server.backup.utils.BackupManagerMonitorUtils.putMonitoringExtra(r0, r1, r15);	 Catch:{ all -> 0x01c7 }
        r28 = 5;
        r29 = 3;
        r0 = r24;
        r1 = r28;
        r2 = r26;
        r3 = r29;
        r4 = r27;
        r24 = com.android.server.backup.utils.BackupManagerMonitorUtils.monitorEvent(r0, r1, r2, r3, r4);	 Catch:{ all -> 0x01c7 }
        r0 = r24;
        r1 = r30;
        r1.mMonitor = r0;	 Catch:{ all -> 0x01c7 }
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ all -> 0x01c7 }
        r24 = r0;
        r24 = r24.getBackupHandler();	 Catch:{ all -> 0x01c7 }
        r26 = 17;
        r0 = r24;
        r1 = r26;
        r0.removeMessages(r1);	 Catch:{ all -> 0x01c7 }
        r0 = r30;
        r0 = r0.mObserver;	 Catch:{ all -> 0x01c7 }
        r24 = r0;
        r26 = -1003; // 0xfffffffffffffc15 float:NaN double:NaN;
        r0 = r24;
        r1 = r18;
        r2 = r26;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r0, r1, r2);	 Catch:{ all -> 0x01c7 }
        r30.errorCleanup();	 Catch:{ all -> 0x01c7 }
        if (r19 == 0) goto L_0x01c0;
    L_0x01bd:
        r19.close();	 Catch:{ IOException -> 0x01ce }
    L_0x01c0:
        monitor-exit(r25);
        return;
    L_0x01c2:
        r14.skipEntityData();	 Catch:{ all -> 0x01c7 }
        goto L_0x00d3;
    L_0x01c7:
        r24 = move-exception;
        if (r19 == 0) goto L_0x01cd;
    L_0x01ca:
        r19.close();	 Catch:{ IOException -> 0x01ce }
    L_0x01cd:
        throw r24;	 Catch:{ IOException -> 0x01ce }
    L_0x01ce:
        r7 = move-exception;
        r24 = "PerformBackupTask";
        r26 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0395 }
        r26.<init>();	 Catch:{ all -> 0x0395 }
        r27 = "Unable to save widget state for ";
        r26 = r26.append(r27);	 Catch:{ all -> 0x0395 }
        r0 = r26;
        r1 = r18;
        r26 = r0.append(r1);	 Catch:{ all -> 0x0395 }
        r26 = r26.toString();	 Catch:{ all -> 0x0395 }
        r0 = r24;
        r1 = r26;
        android.util.Slog.w(r0, r1);	 Catch:{ all -> 0x0395 }
        android.system.Os.ftruncate(r10, r12);	 Catch:{ ErrnoException -> 0x0398 }
    L_0x01f4:
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r24 = r24.getBackupHandler();	 Catch:{ all -> 0x0395 }
        r26 = 17;
        r0 = r24;
        r1 = r26;
        r0.removeMessages(r1);	 Catch:{ all -> 0x0395 }
        r30.clearAgentState();	 Catch:{ all -> 0x0395 }
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r26 = "operation complete";
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ all -> 0x0395 }
        r6 = 0;
        r24 = 0;
        r0 = r24;
        r1 = r30;
        r1.mStatus = r0;	 Catch:{ all -> 0x0395 }
        r22 = 0;
        r0 = r30;
        r0 = r0.mBackupDataName;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r22 = r24.length();	 Catch:{ Exception -> 0x03cb }
        r26 = 0;
        r24 = (r22 > r26 ? 1 : (r22 == r26 ? 0 : -1));
        if (r24 <= 0) goto L_0x041a;
    L_0x0235:
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        if (r24 != 0) goto L_0x0280;
    L_0x023d:
        r0 = r30;
        r0 = r0.mBackupDataName;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = 268435456; // 0x10000000 float:2.5243549E-29 double:1.32624737E-315;
        r0 = r24;
        r1 = r26;
        r6 = android.os.ParcelFileDescriptor.open(r0, r1);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = "sending data to transport";
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mUserInitiated;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        if (r24 == 0) goto L_0x03a8;
    L_0x0265:
        r11 = 1;
    L_0x0266:
        r0 = r30;
        r0 = r0.mTransport;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ Exception -> 0x03cb }
        r26 = r0;
        r0 = r24;
        r1 = r26;
        r24 = r0.performBackup(r1, r6, r11);	 Catch:{ Exception -> 0x03cb }
        r0 = r24;
        r1 = r30;
        r1.mStatus = r0;	 Catch:{ Exception -> 0x03cb }
    L_0x0280:
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x03cb }
        r26.<init>();	 Catch:{ Exception -> 0x03cb }
        r27 = "data delivered: ";
        r26 = r26.append(r27);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ Exception -> 0x03cb }
        r27 = r0;
        r26 = r26.append(r27);	 Catch:{ Exception -> 0x03cb }
        r26 = r26.toString();	 Catch:{ Exception -> 0x03cb }
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        if (r24 != 0) goto L_0x03ab;
    L_0x02af:
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = "finishing op on transport";
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mTransport;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r24 = r24.finishBackup();	 Catch:{ Exception -> 0x03cb }
        r0 = r24;
        r1 = r30;
        r1.mStatus = r0;	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x03cb }
        r26.<init>();	 Catch:{ Exception -> 0x03cb }
        r27 = "finished: ";
        r26 = r26.append(r27);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ Exception -> 0x03cb }
        r27 = r0;
        r26 = r26.append(r27);	 Catch:{ Exception -> 0x03cb }
        r26 = r26.toString();	 Catch:{ Exception -> 0x03cb }
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ Exception -> 0x03cb }
    L_0x02f6:
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        if (r24 != 0) goto L_0x0459;
    L_0x02fe:
        r0 = r30;
        r0 = r0.mBackupDataName;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r24.delete();	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mNewStateName;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r0 = r30;
        r0 = r0.mSavedStateName;	 Catch:{ Exception -> 0x03cb }
        r26 = r0;
        r0 = r24;
        r1 = r26;
        r0.renameTo(r1);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mObserver;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = 0;
        r0 = r24;
        r1 = r18;
        r2 = r26;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r0, r1, r2);	 Catch:{ Exception -> 0x03cb }
        r24 = 2;
        r0 = r24;
        r0 = new java.lang.Object[r0];	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = 0;
        r24[r26] = r18;	 Catch:{ Exception -> 0x03cb }
        r26 = java.lang.Long.valueOf(r22);	 Catch:{ Exception -> 0x03cb }
        r27 = 1;
        r24[r27] = r26;	 Catch:{ Exception -> 0x03cb }
        r26 = 2824; // 0xb08 float:3.957E-42 double:1.395E-320;
        r0 = r26;
        r1 = r24;
        android.util.EventLog.writeEvent(r0, r1);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r0 = r24;
        r1 = r18;
        r0.logBackupComplete(r1);	 Catch:{ Exception -> 0x03cb }
    L_0x0355:
        if (r6 == 0) goto L_0x035a;
    L_0x0357:
        r6.close();	 Catch:{ IOException -> 0x04dc }
    L_0x035a:
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        if (r24 == 0) goto L_0x0370;
    L_0x0362:
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r26 = -1002; // 0xfffffffffffffc16 float:NaN double:NaN;
        r0 = r24;
        r1 = r26;
        if (r0 != r1) goto L_0x04e6;
    L_0x0370:
        r0 = r30;
        r0 = r0.mQueue;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r24 = r24.isEmpty();	 Catch:{ all -> 0x0395 }
        if (r24 == 0) goto L_0x04e2;
    L_0x037c:
        r16 = com.android.server.backup.internal.BackupState.FINAL;	 Catch:{ all -> 0x0395 }
    L_0x037e:
        r0 = r30;
        r1 = r16;
        r0.executeNextState(r1);	 Catch:{ all -> 0x0395 }
        monitor-exit(r25);
        return;
    L_0x0387:
        if (r19 == 0) goto L_0x038c;
    L_0x0389:
        r19.close();	 Catch:{ IOException -> 0x01ce }
    L_0x038c:
        r0 = r30;
        r1 = r18;
        r0.writeWidgetPayloadIfAppropriate(r10, r1);	 Catch:{ IOException -> 0x01ce }
        goto L_0x01f4;
    L_0x0395:
        r24 = move-exception;
        monitor-exit(r25);
        throw r24;
    L_0x0398:
        r9 = move-exception;
        r24 = "PerformBackupTask";
        r26 = "Unable to roll back!";
        r0 = r24;
        r1 = r26;
        android.util.Slog.w(r0, r1);	 Catch:{ all -> 0x0395 }
        goto L_0x01f4;
    L_0x03a8:
        r11 = 0;
        goto L_0x0266;
    L_0x03ab:
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = -1002; // 0xfffffffffffffc16 float:NaN double:NaN;
        r0 = r24;
        r1 = r26;
        if (r0 != r1) goto L_0x02f6;
    L_0x03b9:
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = "transport rejected package";
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ Exception -> 0x03cb }
        goto L_0x02f6;
    L_0x03cb:
        r8 = move-exception;
        r0 = r30;
        r0 = r0.mObserver;	 Catch:{ all -> 0x0452 }
        r24 = r0;
        r26 = -1000; // 0xfffffffffffffc18 float:NaN double:NaN;
        r0 = r24;
        r1 = r18;
        r2 = r26;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r0, r1, r2);	 Catch:{ all -> 0x0452 }
        r24 = "PerformBackupTask";
        r26 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0452 }
        r26.<init>();	 Catch:{ all -> 0x0452 }
        r27 = "Transport error backing up ";
        r26 = r26.append(r27);	 Catch:{ all -> 0x0452 }
        r0 = r26;
        r1 = r18;
        r26 = r0.append(r1);	 Catch:{ all -> 0x0452 }
        r26 = r26.toString();	 Catch:{ all -> 0x0452 }
        r0 = r24;
        r1 = r26;
        android.util.Slog.e(r0, r1, r8);	 Catch:{ all -> 0x0452 }
        r24 = 2822; // 0xb06 float:3.954E-42 double:1.3943E-320;
        r0 = r24;
        r1 = r18;
        android.util.EventLog.writeEvent(r0, r1);	 Catch:{ all -> 0x0452 }
        r24 = -1000; // 0xfffffffffffffc18 float:NaN double:NaN;
        r0 = r24;
        r1 = r30;
        r1.mStatus = r0;	 Catch:{ all -> 0x0452 }
        if (r6 == 0) goto L_0x035a;
    L_0x0412:
        r6.close();	 Catch:{ IOException -> 0x0417 }
        goto L_0x035a;
    L_0x0417:
        r7 = move-exception;
        goto L_0x035a;
    L_0x041a:
        r0 = r30;
        r0 = r0.backupManagerService;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = "no data to send";
        r0 = r24;
        r1 = r26;
        r0.addBackupTrace(r1);	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mMonitor;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ Exception -> 0x03cb }
        r26 = r0;
        r27 = 7;
        r28 = 3;
        r29 = 0;
        r0 = r24;
        r1 = r27;
        r2 = r26;
        r3 = r28;
        r4 = r29;
        r24 = com.android.server.backup.utils.BackupManagerMonitorUtils.monitorEvent(r0, r1, r2, r3, r4);	 Catch:{ Exception -> 0x03cb }
        r0 = r24;
        r1 = r30;
        r1.mMonitor = r0;	 Catch:{ Exception -> 0x03cb }
        goto L_0x02f6;
    L_0x0452:
        r24 = move-exception;
        if (r6 == 0) goto L_0x0458;
    L_0x0455:
        r6.close();	 Catch:{ IOException -> 0x04df }
    L_0x0458:
        throw r24;	 Catch:{ all -> 0x0395 }
    L_0x0459:
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = -1002; // 0xfffffffffffffc16 float:NaN double:NaN;
        r0 = r24;
        r1 = r26;
        if (r0 != r1) goto L_0x0496;
    L_0x0467:
        r0 = r30;
        r0 = r0.mBackupDataName;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r24.delete();	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mNewStateName;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r24.delete();	 Catch:{ Exception -> 0x03cb }
        r0 = r30;
        r0 = r0.mObserver;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = -1002; // 0xfffffffffffffc16 float:NaN double:NaN;
        r0 = r24;
        r1 = r18;
        r2 = r26;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r0, r1, r2);	 Catch:{ Exception -> 0x03cb }
        r24 = "Transport rejected";
        r0 = r18;
        r1 = r24;
        com.android.server.EventLogTags.writeBackupAgentFailure(r0, r1);	 Catch:{ Exception -> 0x03cb }
        goto L_0x0355;
    L_0x0496:
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = -1005; // 0xfffffffffffffc13 float:NaN double:NaN;
        r0 = r24;
        r1 = r26;
        if (r0 != r1) goto L_0x04c0;
    L_0x04a4:
        r0 = r30;
        r0 = r0.mObserver;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = -1005; // 0xfffffffffffffc13 float:NaN double:NaN;
        r0 = r24;
        r1 = r18;
        r2 = r26;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r0, r1, r2);	 Catch:{ Exception -> 0x03cb }
        r24 = 2829; // 0xb0d float:3.964E-42 double:1.3977E-320;
        r0 = r24;
        r1 = r18;
        android.util.EventLog.writeEvent(r0, r1);	 Catch:{ Exception -> 0x03cb }
        goto L_0x0355;
    L_0x04c0:
        r0 = r30;
        r0 = r0.mObserver;	 Catch:{ Exception -> 0x03cb }
        r24 = r0;
        r26 = -1000; // 0xfffffffffffffc18 float:NaN double:NaN;
        r0 = r24;
        r1 = r18;
        r2 = r26;
        com.android.server.backup.utils.BackupObserverUtils.sendBackupOnPackageResult(r0, r1, r2);	 Catch:{ Exception -> 0x03cb }
        r24 = 2822; // 0xb06 float:3.954E-42 double:1.3943E-320;
        r0 = r24;
        r1 = r18;
        android.util.EventLog.writeEvent(r0, r1);	 Catch:{ Exception -> 0x03cb }
        goto L_0x0355;
    L_0x04dc:
        r7 = move-exception;
        goto L_0x035a;
    L_0x04df:
        r7 = move-exception;
        goto L_0x0458;
    L_0x04e2:
        r16 = com.android.server.backup.internal.BackupState.RUNNING_QUEUE;	 Catch:{ all -> 0x0395 }
        goto L_0x037e;
    L_0x04e6:
        r0 = r30;
        r0 = r0.mStatus;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r26 = -1005; // 0xfffffffffffffc13 float:NaN double:NaN;
        r0 = r24;
        r1 = r26;
        if (r0 != r1) goto L_0x0561;
    L_0x04f4:
        r0 = r30;
        r0 = r0.mAgentBinder;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        if (r24 == 0) goto L_0x0529;
    L_0x04fc:
        r0 = r30;
        r0 = r0.mTransport;	 Catch:{ Exception -> 0x0539 }
        r24 = r0;
        r0 = r30;
        r0 = r0.mCurrentPackage;	 Catch:{ Exception -> 0x0539 }
        r26 = r0;
        r0 = r26;
        r0 = r0.packageName;	 Catch:{ Exception -> 0x0539 }
        r26 = r0;
        r27 = 0;
        r0 = r24;
        r1 = r26;
        r2 = r27;
        r20 = r0.getBackupQuota(r1, r2);	 Catch:{ Exception -> 0x0539 }
        r0 = r30;
        r0 = r0.mAgentBinder;	 Catch:{ Exception -> 0x0539 }
        r24 = r0;
        r0 = r24;
        r1 = r22;
        r3 = r20;
        r0.doQuotaExceeded(r1, r3);	 Catch:{ Exception -> 0x0539 }
    L_0x0529:
        r0 = r30;
        r0 = r0.mQueue;	 Catch:{ all -> 0x0395 }
        r24 = r0;
        r24 = r24.isEmpty();	 Catch:{ all -> 0x0395 }
        if (r24 == 0) goto L_0x055d;
    L_0x0535:
        r16 = com.android.server.backup.internal.BackupState.FINAL;	 Catch:{ all -> 0x0395 }
        goto L_0x037e;
    L_0x0539:
        r8 = move-exception;
        r24 = "PerformBackupTask";
        r26 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0395 }
        r26.<init>();	 Catch:{ all -> 0x0395 }
        r27 = "Unable to notify about quota exceeded: ";
        r26 = r26.append(r27);	 Catch:{ all -> 0x0395 }
        r27 = r8.getMessage();	 Catch:{ all -> 0x0395 }
        r26 = r26.append(r27);	 Catch:{ all -> 0x0395 }
        r26 = r26.toString();	 Catch:{ all -> 0x0395 }
        r0 = r24;
        r1 = r26;
        android.util.Slog.e(r0, r1);	 Catch:{ all -> 0x0395 }
        goto L_0x0529;
    L_0x055d:
        r16 = com.android.server.backup.internal.BackupState.RUNNING_QUEUE;	 Catch:{ all -> 0x0395 }
        goto L_0x037e;
    L_0x0561:
        r30.revertAndEndBackup();	 Catch:{ all -> 0x0395 }
        r16 = com.android.server.backup.internal.BackupState.FINAL;	 Catch:{ all -> 0x0395 }
        goto L_0x037e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.backup.internal.PerformBackupTask.operationComplete(long):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @com.android.internal.annotations.GuardedBy("mCancelLock")
    public void handleCancel(boolean r8) {
        /*
        r7 = this;
        r1 = r7.backupManagerService;
        r2 = r7.mEphemeralOpToken;
        r1.removeOperation(r2);
        r2 = r7.mCancelLock;
        monitor-enter(r2);
        r1 = r7.mFinished;	 Catch:{ all -> 0x009c }
        if (r1 == 0) goto L_0x0010;
    L_0x000e:
        monitor-exit(r2);
        return;
    L_0x0010:
        r7.mCancelAll = r8;	 Catch:{ all -> 0x009c }
        r1 = r7.mCurrentPackage;	 Catch:{ all -> 0x009c }
        if (r1 == 0) goto L_0x0091;
    L_0x0016:
        r1 = r7.mCurrentPackage;	 Catch:{ all -> 0x009c }
        r0 = r1.packageName;	 Catch:{ all -> 0x009c }
    L_0x001a:
        r1 = "PerformBackupTask";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009c }
        r3.<init>();	 Catch:{ all -> 0x009c }
        r4 = "Cancel backing up ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x009c }
        r3 = r3.append(r0);	 Catch:{ all -> 0x009c }
        r3 = r3.toString();	 Catch:{ all -> 0x009c }
        android.util.Slog.i(r1, r3);	 Catch:{ all -> 0x009c }
        r1 = 2823; // 0xb07 float:3.956E-42 double:1.3947E-320;
        android.util.EventLog.writeEvent(r1, r0);	 Catch:{ all -> 0x009c }
        r1 = r7.backupManagerService;	 Catch:{ all -> 0x009c }
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009c }
        r3.<init>();	 Catch:{ all -> 0x009c }
        r4 = "cancel of ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x009c }
        r3 = r3.append(r0);	 Catch:{ all -> 0x009c }
        r4 = ", cancelAll=";
        r3 = r3.append(r4);	 Catch:{ all -> 0x009c }
        r3 = r3.append(r8);	 Catch:{ all -> 0x009c }
        r3 = r3.toString();	 Catch:{ all -> 0x009c }
        r1.addBackupTrace(r3);	 Catch:{ all -> 0x009c }
        r1 = r7.mMonitor;	 Catch:{ all -> 0x009c }
        r3 = r7.mCurrentPackage;	 Catch:{ all -> 0x009c }
        r4 = "android.app.backup.extra.LOG_CANCEL_ALL";
        r5 = r7.mCancelAll;	 Catch:{ all -> 0x009c }
        r6 = 0;
        r4 = com.android.server.backup.utils.BackupManagerMonitorUtils.putMonitoringExtra(r6, r4, r5);	 Catch:{ all -> 0x009c }
        r5 = 21;
        r6 = 2;
        r1 = com.android.server.backup.utils.BackupManagerMonitorUtils.monitorEvent(r1, r5, r3, r6, r4);	 Catch:{ all -> 0x009c }
        r7.mMonitor = r1;	 Catch:{ all -> 0x009c }
        r7.errorCleanup();	 Catch:{ all -> 0x009c }
        if (r8 != 0) goto L_0x0098;
    L_0x0079:
        r1 = r7.mQueue;	 Catch:{ all -> 0x009c }
        r1 = r1.isEmpty();	 Catch:{ all -> 0x009c }
        if (r1 == 0) goto L_0x0095;
    L_0x0081:
        r1 = com.android.server.backup.internal.BackupState.FINAL;	 Catch:{ all -> 0x009c }
    L_0x0083:
        r7.executeNextState(r1);	 Catch:{ all -> 0x009c }
        r1 = r7.backupManagerService;	 Catch:{ all -> 0x009c }
        r3 = r7.mCurrentPackage;	 Catch:{ all -> 0x009c }
        r3 = r3.packageName;	 Catch:{ all -> 0x009c }
        r1.dataChangedImpl(r3);	 Catch:{ all -> 0x009c }
    L_0x008f:
        monitor-exit(r2);
        return;
    L_0x0091:
        r0 = "no_package_yet";
        goto L_0x001a;
    L_0x0095:
        r1 = com.android.server.backup.internal.BackupState.RUNNING_QUEUE;	 Catch:{ all -> 0x009c }
        goto L_0x0083;
    L_0x0098:
        r7.finalizeBackup();	 Catch:{ all -> 0x009c }
        goto L_0x008f;
    L_0x009c:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.backup.internal.PerformBackupTask.handleCancel(boolean):void");
    }

    void revertAndEndBackup() {
        long delay;
        this.backupManagerService.addBackupTrace("transport error; reverting");
        try {
            delay = this.mTransport.requestBackupTime();
        } catch (Exception e) {
            Slog.w(TAG, "Unable to contact transport for recommended backoff: " + e.getMessage());
            delay = 0;
        }
        KeyValueBackupJob.schedule(this.backupManagerService.getContext(), delay);
        for (BackupRequest request : this.mOriginalQueue) {
            this.backupManagerService.dataChangedImpl(request.packageName);
        }
    }

    void errorCleanup() {
        this.mBackupDataName.delete();
        this.mNewStateName.delete();
        clearAgentState();
    }

    void clearAgentState() {
        try {
            if (this.mSavedState != null) {
                this.mSavedState.close();
            }
        } catch (IOException e) {
        }
        try {
            if (this.mBackupData != null) {
                this.mBackupData.close();
            }
        } catch (IOException e2) {
        }
        try {
            if (this.mNewState != null) {
                this.mNewState.close();
            }
        } catch (IOException e3) {
        }
        synchronized (this.backupManagerService.getCurrentOpLock()) {
            this.backupManagerService.getCurrentOperations().remove(this.mEphemeralOpToken);
            this.mNewState = null;
            this.mBackupData = null;
            this.mSavedState = null;
        }
        if (this.mCurrentPackage.applicationInfo != null) {
            this.backupManagerService.addBackupTrace("unbinding " + this.mCurrentPackage.packageName);
            try {
                this.backupManagerService.getActivityManager().unbindBackupAgent(this.mCurrentPackage.applicationInfo);
            } catch (RemoteException e4) {
            }
        }
    }

    void executeNextState(BackupState nextState) {
        this.backupManagerService.addBackupTrace("executeNextState => " + nextState);
        this.mCurrentState = nextState;
        this.backupManagerService.getBackupHandler().sendMessage(this.backupManagerService.getBackupHandler().obtainMessage(20, this));
    }
}
