package com.android.server.backup.restore;

import android.app.backup.IBackupManagerMonitor;
import android.app.backup.IRestoreObserver;
import android.app.backup.IRestoreSession.Stub;
import android.app.backup.RestoreSet;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Binder;
import android.os.Message;
import android.util.Slog;
import com.android.internal.backup.IBackupTransport;
import com.android.server.backup.RefactoredBackupManagerService;
import com.android.server.backup.params.RestoreParams;

public class ActiveRestoreSession extends Stub {
    private static final String TAG = "RestoreSession";
    private RefactoredBackupManagerService backupManagerService;
    boolean mEnded = false;
    private String mPackageName;
    public RestoreSet[] mRestoreSets = null;
    private IBackupTransport mRestoreTransport = null;
    boolean mTimedOut = false;

    public class EndRestoreRunnable implements Runnable {
        RefactoredBackupManagerService mBackupManager;
        ActiveRestoreSession mSession;

        public EndRestoreRunnable(RefactoredBackupManagerService manager, ActiveRestoreSession session) {
            this.mBackupManager = manager;
            this.mSession = session;
        }

        public void run() {
            synchronized (this.mSession) {
                this.mSession.mRestoreTransport = null;
                this.mSession.mEnded = true;
            }
            this.mBackupManager.clearRestoreSession(this.mSession);
        }
    }

    public ActiveRestoreSession(RefactoredBackupManagerService backupManagerService, String packageName, String transport) {
        this.backupManagerService = backupManagerService;
        this.mPackageName = packageName;
        this.mRestoreTransport = backupManagerService.getTransportManager().getTransportBinder(transport);
    }

    public void markTimedOut() {
        this.mTimedOut = true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int getAvailableRestoreSets(android.app.backup.IRestoreObserver r9, android.app.backup.IBackupManagerMonitor r10) {
        /*
        r8 = this;
        r7 = -1;
        monitor-enter(r8);
        r4 = r8.backupManagerService;	 Catch:{ all -> 0x001c }
        r4 = r4.getContext();	 Catch:{ all -> 0x001c }
        r5 = "android.permission.BACKUP";
        r6 = "getAvailableRestoreSets";
        r4.enforceCallingOrSelfPermission(r5, r6);	 Catch:{ all -> 0x001c }
        if (r9 != 0) goto L_0x001f;
    L_0x0013:
        r4 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x001c }
        r5 = "Observer must not be null";
        r4.<init>(r5);	 Catch:{ all -> 0x001c }
        throw r4;	 Catch:{ all -> 0x001c }
    L_0x001c:
        r4 = move-exception;
        monitor-exit(r8);
        throw r4;
    L_0x001f:
        r4 = r8.mEnded;	 Catch:{ all -> 0x001c }
        if (r4 == 0) goto L_0x002c;
    L_0x0023:
        r4 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x001c }
        r5 = "Restore session already ended";
        r4.<init>(r5);	 Catch:{ all -> 0x001c }
        throw r4;	 Catch:{ all -> 0x001c }
    L_0x002c:
        r4 = r8.mTimedOut;	 Catch:{ all -> 0x001c }
        if (r4 == 0) goto L_0x003b;
    L_0x0030:
        r4 = "RestoreSession";
        r5 = "Session already timed out";
        android.util.Slog.i(r4, r5);	 Catch:{ all -> 0x001c }
        monitor-exit(r8);
        return r7;
    L_0x003b:
        r2 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x001c }
        r4 = r8.mRestoreTransport;	 Catch:{ Exception -> 0x0086 }
        if (r4 != 0) goto L_0x0051;
    L_0x0043:
        r4 = "RestoreSession";
        r5 = "Null transport getting restore sets";
        android.util.Slog.w(r4, r5);	 Catch:{ Exception -> 0x0086 }
        android.os.Binder.restoreCallingIdentity(r2);	 Catch:{ all -> 0x001c }
        monitor-exit(r8);
        return r7;
    L_0x0051:
        r4 = r8.backupManagerService;	 Catch:{ Exception -> 0x0086 }
        r4 = r4.getBackupHandler();	 Catch:{ Exception -> 0x0086 }
        r5 = 8;
        r4.removeMessages(r5);	 Catch:{ Exception -> 0x0086 }
        r4 = r8.backupManagerService;	 Catch:{ Exception -> 0x0086 }
        r4 = r4.getWakelock();	 Catch:{ Exception -> 0x0086 }
        r4.acquire();	 Catch:{ Exception -> 0x0086 }
        r4 = r8.backupManagerService;	 Catch:{ Exception -> 0x0086 }
        r4 = r4.getBackupHandler();	 Catch:{ Exception -> 0x0086 }
        r5 = new com.android.server.backup.params.RestoreGetSetsParams;	 Catch:{ Exception -> 0x0086 }
        r6 = r8.mRestoreTransport;	 Catch:{ Exception -> 0x0086 }
        r5.<init>(r6, r8, r9, r10);	 Catch:{ Exception -> 0x0086 }
        r6 = 6;
        r1 = r4.obtainMessage(r6, r5);	 Catch:{ Exception -> 0x0086 }
        r4 = r8.backupManagerService;	 Catch:{ Exception -> 0x0086 }
        r4 = r4.getBackupHandler();	 Catch:{ Exception -> 0x0086 }
        r4.sendMessage(r1);	 Catch:{ Exception -> 0x0086 }
        android.os.Binder.restoreCallingIdentity(r2);	 Catch:{ all -> 0x001c }
        r4 = 0;
        monitor-exit(r8);
        return r4;
    L_0x0086:
        r0 = move-exception;
        r4 = "RestoreSession";
        r5 = "Error in getAvailableRestoreSets";
        android.util.Slog.e(r4, r5, r0);	 Catch:{ all -> 0x0095 }
        android.os.Binder.restoreCallingIdentity(r2);	 Catch:{ all -> 0x001c }
        monitor-exit(r8);
        return r7;
    L_0x0095:
        r4 = move-exception;
        android.os.Binder.restoreCallingIdentity(r2);	 Catch:{ all -> 0x001c }
        throw r4;	 Catch:{ all -> 0x001c }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.backup.restore.ActiveRestoreSession.getAvailableRestoreSets(android.app.backup.IRestoreObserver, android.app.backup.IBackupManagerMonitor):int");
    }

    public synchronized int restoreAll(long token, IRestoreObserver observer, IBackupManagerMonitor monitor) {
        this.backupManagerService.getContext().enforceCallingOrSelfPermission("android.permission.BACKUP", "performRestore");
        Slog.d(TAG, "restoreAll token=" + Long.toHexString(token) + " observer=" + observer);
        if (this.mEnded) {
            throw new IllegalStateException("Restore session already ended");
        } else if (this.mTimedOut) {
            Slog.i(TAG, "Session already timed out");
            return -1;
        } else if (this.mRestoreTransport == null || this.mRestoreSets == null) {
            Slog.e(TAG, "Ignoring restoreAll() with no restore set");
            return -1;
        } else if (this.mPackageName != null) {
            Slog.e(TAG, "Ignoring restoreAll() on single-package session");
            return -1;
        } else {
            try {
                String dirName = this.mRestoreTransport.transportDirName();
                synchronized (this.backupManagerService.getQueueLock()) {
                    for (RestoreSet restoreSet : this.mRestoreSets) {
                        if (token == restoreSet.token) {
                            this.backupManagerService.getBackupHandler().removeMessages(8);
                            long oldId = Binder.clearCallingIdentity();
                            this.backupManagerService.getWakelock().acquire();
                            Message msg = this.backupManagerService.getBackupHandler().obtainMessage(3);
                            msg.obj = new RestoreParams(this.mRestoreTransport, dirName, observer, monitor, token);
                            this.backupManagerService.getBackupHandler().sendMessage(msg);
                            Binder.restoreCallingIdentity(oldId);
                            return 0;
                        }
                    }
                    Slog.w(TAG, "Restore token " + Long.toHexString(token) + " not found");
                    return -1;
                }
            } catch (Exception e) {
                Slog.e(TAG, "Unable to get transport dir for restore: " + e.getMessage());
                return -1;
            }
        }
    }

    public synchronized int restoreSome(long token, IRestoreObserver observer, IBackupManagerMonitor monitor, String[] packages) {
        this.backupManagerService.getContext().enforceCallingOrSelfPermission("android.permission.BACKUP", "performRestore");
        StringBuilder b = new StringBuilder(128);
        b.append("restoreSome token=");
        b.append(Long.toHexString(token));
        b.append(" observer=");
        b.append(observer.toString());
        b.append(" monitor=");
        if (monitor == null) {
            b.append("null");
        } else {
            b.append(monitor.toString());
        }
        b.append(" packages=");
        if (packages == null) {
            b.append("null");
        } else {
            b.append('{');
            boolean first = true;
            for (String s : packages) {
                if (first) {
                    first = false;
                } else {
                    b.append(", ");
                }
                b.append(s);
            }
            b.append('}');
        }
        Slog.d(TAG, b.toString());
        if (this.mEnded) {
            throw new IllegalStateException("Restore session already ended");
        } else if (this.mTimedOut) {
            Slog.i(TAG, "Session already timed out");
            return -1;
        } else if (this.mRestoreTransport == null || this.mRestoreSets == null) {
            Slog.e(TAG, "Ignoring restoreAll() with no restore set");
            return -1;
        } else if (this.mPackageName != null) {
            Slog.e(TAG, "Ignoring restoreAll() on single-package session");
            return -1;
        } else {
            try {
                String dirName = this.mRestoreTransport.transportDirName();
                synchronized (this.backupManagerService.getQueueLock()) {
                    for (RestoreSet restoreSet : this.mRestoreSets) {
                        if (token == restoreSet.token) {
                            this.backupManagerService.getBackupHandler().removeMessages(8);
                            long oldId = Binder.clearCallingIdentity();
                            this.backupManagerService.getWakelock().acquire();
                            Message msg = this.backupManagerService.getBackupHandler().obtainMessage(3);
                            msg.obj = new RestoreParams(this.mRestoreTransport, dirName, observer, monitor, token, packages, packages.length > 1);
                            this.backupManagerService.getBackupHandler().sendMessage(msg);
                            Binder.restoreCallingIdentity(oldId);
                            return 0;
                        }
                    }
                    Slog.w(TAG, "Restore token " + Long.toHexString(token) + " not found");
                    return -1;
                }
            } catch (Exception e) {
                Slog.e(TAG, "Unable to get transport name for restoreSome: " + e.getMessage());
                return -1;
            }
        }
    }

    public synchronized int restorePackage(String packageName, IRestoreObserver observer, IBackupManagerMonitor monitor) {
        Slog.v(TAG, "restorePackage pkg=" + packageName + " obs=" + observer + "monitor=" + monitor);
        if (this.mEnded) {
            throw new IllegalStateException("Restore session already ended");
        } else if (this.mTimedOut) {
            Slog.i(TAG, "Session already timed out");
            return -1;
        } else if (this.mPackageName == null || this.mPackageName.equals(packageName)) {
            try {
                PackageInfo app = this.backupManagerService.getPackageManager().getPackageInfo(packageName, 0);
                if (this.backupManagerService.getContext().checkPermission("android.permission.BACKUP", Binder.getCallingPid(), Binder.getCallingUid()) != -1 || app.applicationInfo.uid == Binder.getCallingUid()) {
                    long oldId = Binder.clearCallingIdentity();
                    try {
                        long token = this.backupManagerService.getAvailableRestoreToken(packageName);
                        Slog.v(TAG, "restorePackage pkg=" + packageName + " token=" + Long.toHexString(token));
                        if (token == 0) {
                            Slog.w(TAG, "No data available for this package; not restoring");
                            return -1;
                        }
                        String dirName = this.mRestoreTransport.transportDirName();
                        this.backupManagerService.getBackupHandler().removeMessages(8);
                        this.backupManagerService.getWakelock().acquire();
                        Message msg = this.backupManagerService.getBackupHandler().obtainMessage(3);
                        msg.obj = new RestoreParams(this.mRestoreTransport, dirName, observer, monitor, token, app);
                        this.backupManagerService.getBackupHandler().sendMessage(msg);
                        return 0;
                    } catch (Exception e) {
                        Slog.e(TAG, "Unable to get transport dir for restorePackage: " + e.getMessage());
                        return -1;
                    } finally {
                        Binder.restoreCallingIdentity(oldId);
                    }
                } else {
                    Slog.w(TAG, "restorePackage: bad packageName=" + packageName + " or calling uid=" + Binder.getCallingUid());
                    throw new SecurityException("No permission to restore other packages");
                }
            } catch (NameNotFoundException e2) {
                Slog.w(TAG, "Asked to restore nonexistent pkg " + packageName);
                return -1;
            }
        } else {
            Slog.e(TAG, "Ignoring attempt to restore pkg=" + packageName + " on session for package " + this.mPackageName);
            return -1;
        }
    }

    public synchronized void endRestoreSession() {
        Slog.d(TAG, "endRestoreSession");
        if (this.mTimedOut) {
            Slog.i(TAG, "Session already timed out");
        } else if (this.mEnded) {
            throw new IllegalStateException("Restore session already ended");
        } else {
            this.backupManagerService.getBackupHandler().post(new EndRestoreRunnable(this.backupManagerService, this));
        }
    }
}
