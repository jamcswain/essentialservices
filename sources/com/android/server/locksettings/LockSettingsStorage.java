package com.android.server.locksettings;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.UserInfo;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Slog;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import com.android.server.LocalServices;
import com.android.server.PersistentDataBlockManagerInternal;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class LockSettingsStorage {
    private static final String BASE_ZERO_LOCK_PATTERN_FILE = "gatekeeper.gesture.key";
    private static final String CHILD_PROFILE_LOCK_FILE = "gatekeeper.profile.key";
    private static final String[] COLUMNS_FOR_PREFETCH = new String[]{COLUMN_KEY, COLUMN_VALUE};
    private static final String[] COLUMNS_FOR_QUERY = new String[]{COLUMN_VALUE};
    private static final String COLUMN_KEY = "name";
    private static final String COLUMN_USERID = "user";
    private static final String COLUMN_VALUE = "value";
    private static final boolean DEBUG = false;
    private static final Object DEFAULT = new Object();
    private static final String LEGACY_LOCK_PASSWORD_FILE = "password.key";
    private static final String LEGACY_LOCK_PATTERN_FILE = "gesture.key";
    private static final String LOCK_PASSWORD_FILE = "gatekeeper.password.key";
    private static final String LOCK_PATTERN_FILE = "gatekeeper.pattern.key";
    private static final String SYNTHETIC_PASSWORD_DIRECTORY = "spblob/";
    private static final String SYSTEM_DIRECTORY = "/system/";
    private static final String TABLE = "locksettings";
    private static final String TAG = "LockSettingsStorage";
    private final Cache mCache = new Cache();
    private final Context mContext;
    private final Object mFileWriteLock = new Object();
    private final DatabaseHelper mOpenHelper;
    private PersistentDataBlockManagerInternal mPersistentDataBlockManagerInternal;

    public interface Callback {
        void initialize(SQLiteDatabase sQLiteDatabase);
    }

    private static class Cache {
        private final ArrayMap<CacheKey, Object> mCache;
        private final CacheKey mCacheKey;
        private int mVersion;

        private static final class CacheKey {
            static final int TYPE_FETCHED = 2;
            static final int TYPE_FILE = 1;
            static final int TYPE_KEY_VALUE = 0;
            String key;
            int type;
            int userId;

            private CacheKey() {
            }

            public CacheKey set(int type, String key, int userId) {
                this.type = type;
                this.key = key;
                this.userId = userId;
                return this;
            }

            public boolean equals(Object obj) {
                boolean z = false;
                if (!(obj instanceof CacheKey)) {
                    return false;
                }
                CacheKey o = (CacheKey) obj;
                if (this.userId == o.userId && this.type == o.type) {
                    z = this.key.equals(o.key);
                }
                return z;
            }

            public int hashCode() {
                return (this.key.hashCode() ^ this.userId) ^ this.type;
            }
        }

        private Cache() {
            this.mCache = new ArrayMap();
            this.mCacheKey = new CacheKey();
            this.mVersion = 0;
        }

        String peekKeyValue(String key, String defaultValue, int userId) {
            Object cached = peek(0, key, userId);
            return cached == LockSettingsStorage.DEFAULT ? defaultValue : (String) cached;
        }

        boolean hasKeyValue(String key, int userId) {
            return contains(0, key, userId);
        }

        void putKeyValue(String key, String value, int userId) {
            put(0, key, value, userId);
        }

        void putKeyValueIfUnchanged(String key, Object value, int userId, int version) {
            putIfUnchanged(0, key, value, userId, version);
        }

        byte[] peekFile(String fileName) {
            return (byte[]) peek(1, fileName, -1);
        }

        boolean hasFile(String fileName) {
            return contains(1, fileName, -1);
        }

        void putFile(String key, byte[] value) {
            put(1, key, value, -1);
        }

        void putFileIfUnchanged(String key, byte[] value, int version) {
            putIfUnchanged(1, key, value, -1, version);
        }

        void setFetched(int userId) {
            put(2, "isFetched", "true", userId);
        }

        boolean isFetched(int userId) {
            return contains(2, "", userId);
        }

        private synchronized void put(int type, String key, Object value, int userId) {
            this.mCache.put(new CacheKey().set(type, key, userId), value);
            this.mVersion++;
        }

        private synchronized void putIfUnchanged(int type, String key, Object value, int userId, int version) {
            if (!contains(type, key, userId) && this.mVersion == version) {
                put(type, key, value, userId);
            }
        }

        private synchronized boolean contains(int type, String key, int userId) {
            return this.mCache.containsKey(this.mCacheKey.set(type, key, userId));
        }

        private synchronized Object peek(int type, String key, int userId) {
            return this.mCache.get(this.mCacheKey.set(type, key, userId));
        }

        private synchronized int getVersion() {
            return this.mVersion;
        }

        synchronized void removeUser(int userId) {
            for (int i = this.mCache.size() - 1; i >= 0; i--) {
                if (((CacheKey) this.mCache.keyAt(i)).userId == userId) {
                    this.mCache.removeAt(i);
                }
            }
            this.mVersion++;
        }

        synchronized void purgePath(String path) {
            for (int i = this.mCache.size() - 1; i >= 0; i--) {
                CacheKey entry = (CacheKey) this.mCache.keyAt(i);
                if (entry.type == 1 && entry.key.startsWith(path)) {
                    this.mCache.removeAt(i);
                }
            }
            this.mVersion++;
        }

        synchronized void clear() {
            this.mCache.clear();
            this.mVersion++;
        }
    }

    public static class CredentialHash {
        static final int VERSION_GATEKEEPER = 1;
        static final int VERSION_LEGACY = 0;
        byte[] hash;
        boolean isBaseZeroPattern;
        int type;
        int version;

        private CredentialHash(byte[] hash, int type, int version) {
            this(hash, type, version, false);
        }

        private CredentialHash(byte[] hash, int type, int version, boolean isBaseZeroPattern) {
            if (type != -1) {
                if (hash == null) {
                    throw new RuntimeException("Empty hash for CredentialHash");
                }
            } else if (hash != null) {
                throw new RuntimeException("None type CredentialHash should not have hash");
            }
            this.hash = hash;
            this.type = type;
            this.version = version;
            this.isBaseZeroPattern = isBaseZeroPattern;
        }

        private static CredentialHash createBaseZeroPattern(byte[] hash) {
            return new CredentialHash(hash, 1, 1, true);
        }

        static CredentialHash create(byte[] hash, int type) {
            if (type != -1) {
                return new CredentialHash(hash, type, 1);
            }
            throw new RuntimeException("Bad type for CredentialHash");
        }

        static CredentialHash createEmptyHash() {
            return new CredentialHash(null, -1, 1);
        }

        public byte[] toBytes() {
            Preconditions.checkState(this.isBaseZeroPattern ^ 1, "base zero patterns are not serializable");
            try {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(os);
                dos.write(this.version);
                dos.write(this.type);
                if (this.hash == null || this.hash.length <= 0) {
                    dos.writeInt(0);
                } else {
                    dos.writeInt(this.hash.length);
                    dos.write(this.hash);
                }
                dos.close();
                return os.toByteArray();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public static CredentialHash fromBytes(byte[] bytes) {
            try {
                DataInputStream is = new DataInputStream(new ByteArrayInputStream(bytes));
                int version = is.read();
                int type = is.read();
                int hashSize = is.readInt();
                byte[] bArr = null;
                if (hashSize > 0) {
                    bArr = new byte[hashSize];
                    is.readFully(bArr);
                }
                return new CredentialHash(bArr, type, version);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String DATABASE_NAME = "locksettings.db";
        private static final int DATABASE_VERSION = 2;
        private static final int IDLE_CONNECTION_TIMEOUT_MS = 30000;
        private static final String TAG = "LockSettingsDB";
        private Callback mCallback;

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, 2);
            setWriteAheadLoggingEnabled(true);
            setIdleConnectionTimeout(30000);
        }

        public void setCallback(Callback callback) {
            this.mCallback = callback;
        }

        private void createTable(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE locksettings (_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,user INTEGER,value TEXT);");
        }

        public void onCreate(SQLiteDatabase db) {
            createTable(db);
            if (this.mCallback != null) {
                this.mCallback.initialize(db);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int currentVersion) {
            int upgradeVersion = oldVersion;
            if (oldVersion == 1) {
                upgradeVersion = 2;
            }
            if (upgradeVersion != 2) {
                Log.w(TAG, "Failed to upgrade database!");
            }
        }
    }

    public static class PersistentData {
        public static final PersistentData NONE = new PersistentData(0, -10000, 0, null);
        public static final int TYPE_NONE = 0;
        public static final int TYPE_SP = 1;
        public static final int TYPE_SP_WEAVER = 2;
        static final byte VERSION_1 = (byte) 1;
        static final int VERSION_1_HEADER_SIZE = 10;
        final byte[] payload;
        final int qualityForUi;
        final int type;
        final int userId;

        private PersistentData(int type, int userId, int qualityForUi, byte[] payload) {
            this.type = type;
            this.userId = userId;
            this.qualityForUi = qualityForUi;
            this.payload = payload;
        }

        public static PersistentData fromBytes(byte[] frpData) {
            if (frpData == null || frpData.length == 0) {
                return NONE;
            }
            DataInputStream is = new DataInputStream(new ByteArrayInputStream(frpData));
            try {
                byte version = is.readByte();
                if (version == (byte) 1) {
                    int type = is.readByte() & 255;
                    int userId = is.readInt();
                    int qualityForUi = is.readInt();
                    byte[] payload = new byte[(frpData.length - 10)];
                    System.arraycopy(frpData, 10, payload, 0, payload.length);
                    return new PersistentData(type, userId, qualityForUi, payload);
                }
                Slog.wtf(LockSettingsStorage.TAG, "Unknown PersistentData version code: " + version);
                return null;
            } catch (IOException e) {
                Slog.wtf(LockSettingsStorage.TAG, "Could not parse PersistentData", e);
                return null;
            }
        }

        public static byte[] toBytes(int persistentType, int userId, int qualityForUi, byte[] payload) {
            boolean z = true;
            boolean z2 = false;
            if (persistentType == 0) {
                if (payload != null) {
                    z = false;
                }
                Preconditions.checkArgument(z, "TYPE_NONE must have empty payload");
                return null;
            }
            if (payload != null && payload.length > 0) {
                z2 = true;
            }
            Preconditions.checkArgument(z2, "empty payload must only be used with TYPE_NONE");
            ByteArrayOutputStream os = new ByteArrayOutputStream(payload.length + 10);
            DataOutputStream dos = new DataOutputStream(os);
            try {
                dos.writeByte(1);
                dos.writeByte(persistentType);
                dos.writeInt(userId);
                dos.writeInt(qualityForUi);
                dos.write(payload);
                return os.toByteArray();
            } catch (IOException e) {
                throw new RuntimeException("ByteArrayOutputStream cannot throw IOException");
            }
        }
    }

    public LockSettingsStorage(Context context) {
        this.mContext = context;
        this.mOpenHelper = new DatabaseHelper(context);
    }

    public void setDatabaseOnCreateCallback(Callback callback) {
        this.mOpenHelper.setCallback(callback);
    }

    public void writeKeyValue(String key, String value, int userId) {
        writeKeyValue(this.mOpenHelper.getWritableDatabase(), key, value, userId);
    }

    public void writeKeyValue(SQLiteDatabase db, String key, String value, int userId) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_KEY, key);
        cv.put(COLUMN_USERID, Integer.valueOf(userId));
        cv.put(COLUMN_VALUE, value);
        db.beginTransaction();
        try {
            db.delete(TABLE, "name=? AND user=?", new String[]{key, Integer.toString(userId)});
            db.insert(TABLE, null, cv);
            db.setTransactionSuccessful();
            this.mCache.putKeyValue(key, value, userId);
        } finally {
            db.endTransaction();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String readKeyValue(java.lang.String r13, java.lang.String r14, int r15) {
        /*
        r12 = this;
        r11 = 0;
        r5 = 0;
        r2 = r12.mCache;
        monitor-enter(r2);
        r1 = r12.mCache;	 Catch:{ all -> 0x0057 }
        r1 = r1.hasKeyValue(r13, r15);	 Catch:{ all -> 0x0057 }
        if (r1 == 0) goto L_0x0015;
    L_0x000d:
        r1 = r12.mCache;	 Catch:{ all -> 0x0057 }
        r1 = r1.peekKeyValue(r13, r14, r15);	 Catch:{ all -> 0x0057 }
        monitor-exit(r2);
        return r1;
    L_0x0015:
        r1 = r12.mCache;	 Catch:{ all -> 0x0057 }
        r10 = r1.getVersion();	 Catch:{ all -> 0x0057 }
        monitor-exit(r2);
        r9 = DEFAULT;
        r1 = r12.mOpenHelper;
        r0 = r1.getReadableDatabase();
        r1 = "locksettings";
        r2 = COLUMNS_FOR_QUERY;
        r3 = "user=? AND name=?";
        r4 = 2;
        r4 = new java.lang.String[r4];
        r6 = java.lang.Integer.toString(r15);
        r4[r11] = r6;
        r6 = 1;
        r4[r6] = r13;
        r6 = r5;
        r7 = r5;
        r8 = r0.query(r1, r2, r3, r4, r5, r6, r7);
        if (r8 == 0) goto L_0x004d;
    L_0x0040:
        r1 = r8.moveToFirst();
        if (r1 == 0) goto L_0x004a;
    L_0x0046:
        r9 = r8.getString(r11);
    L_0x004a:
        r8.close();
    L_0x004d:
        r1 = r12.mCache;
        r1.putKeyValueIfUnchanged(r13, r9, r15, r10);
        r1 = DEFAULT;
        if (r9 != r1) goto L_0x005a;
    L_0x0056:
        return r14;
    L_0x0057:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
    L_0x005a:
        r9 = (java.lang.String) r9;
        r14 = r9;
        goto L_0x0056;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsStorage.readKeyValue(java.lang.String, java.lang.String, int):java.lang.String");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void prefetchUser(int r15) {
        /*
        r14 = this;
        r13 = 1;
        r12 = 0;
        r5 = 0;
        r2 = r14.mCache;
        monitor-enter(r2);
        r1 = r14.mCache;	 Catch:{ all -> 0x004e }
        r1 = r1.isFetched(r15);	 Catch:{ all -> 0x004e }
        if (r1 == 0) goto L_0x0010;
    L_0x000e:
        monitor-exit(r2);
        return;
    L_0x0010:
        r1 = r14.mCache;	 Catch:{ all -> 0x004e }
        r1.setFetched(r15);	 Catch:{ all -> 0x004e }
        r1 = r14.mCache;	 Catch:{ all -> 0x004e }
        r11 = r1.getVersion();	 Catch:{ all -> 0x004e }
        monitor-exit(r2);
        r1 = r14.mOpenHelper;
        r0 = r1.getReadableDatabase();
        r1 = "locksettings";
        r2 = COLUMNS_FOR_PREFETCH;
        r3 = "user=?";
        r4 = new java.lang.String[r13];
        r6 = java.lang.Integer.toString(r15);
        r4[r12] = r6;
        r6 = r5;
        r7 = r5;
        r8 = r0.query(r1, r2, r3, r4, r5, r6, r7);
        if (r8 == 0) goto L_0x0054;
    L_0x003a:
        r1 = r8.moveToNext();
        if (r1 == 0) goto L_0x0051;
    L_0x0040:
        r9 = r8.getString(r12);
        r10 = r8.getString(r13);
        r1 = r14.mCache;
        r1.putKeyValueIfUnchanged(r9, r10, r15, r11);
        goto L_0x003a;
    L_0x004e:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
    L_0x0051:
        r8.close();
    L_0x0054:
        r14.readCredentialHash(r15);
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsStorage.prefetchUser(int):void");
    }

    private CredentialHash readPasswordHashIfExists(int userId) {
        byte[] stored = readFile(getLockPasswordFilename(userId));
        if (!ArrayUtils.isEmpty(stored)) {
            return new CredentialHash(stored, 2, 1);
        }
        stored = readFile(getLegacyLockPasswordFilename(userId));
        if (ArrayUtils.isEmpty(stored)) {
            return null;
        }
        return new CredentialHash(stored, 2, 0);
    }

    private CredentialHash readPatternHashIfExists(int userId) {
        byte[] stored = readFile(getLockPatternFilename(userId));
        if (!ArrayUtils.isEmpty(stored)) {
            return new CredentialHash(stored, 1, 1);
        }
        stored = readFile(getBaseZeroLockPatternFilename(userId));
        if (!ArrayUtils.isEmpty(stored)) {
            return CredentialHash.createBaseZeroPattern(stored);
        }
        stored = readFile(getLegacyLockPatternFilename(userId));
        if (ArrayUtils.isEmpty(stored)) {
            return null;
        }
        return new CredentialHash(stored, 1, 0);
    }

    public CredentialHash readCredentialHash(int userId) {
        CredentialHash passwordHash = readPasswordHashIfExists(userId);
        CredentialHash patternHash = readPatternHashIfExists(userId);
        if (passwordHash == null || patternHash == null) {
            if (passwordHash != null) {
                return passwordHash;
            }
            if (patternHash != null) {
                return patternHash;
            }
            return CredentialHash.createEmptyHash();
        } else if (passwordHash.version == 1) {
            return passwordHash;
        } else {
            return patternHash;
        }
    }

    public void removeChildProfileLock(int userId) {
        try {
            deleteFile(getChildProfileLockFile(userId));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeChildProfileLock(int userId, byte[] lock) {
        writeFile(getChildProfileLockFile(userId), lock);
    }

    public byte[] readChildProfileLock(int userId) {
        return readFile(getChildProfileLockFile(userId));
    }

    public boolean hasChildProfileLock(int userId) {
        return hasFile(getChildProfileLockFile(userId));
    }

    public boolean hasPassword(int userId) {
        if (hasFile(getLockPasswordFilename(userId))) {
            return true;
        }
        return hasFile(getLegacyLockPasswordFilename(userId));
    }

    public boolean hasPattern(int userId) {
        if (hasFile(getLockPatternFilename(userId)) || hasFile(getBaseZeroLockPatternFilename(userId))) {
            return true;
        }
        return hasFile(getLegacyLockPatternFilename(userId));
    }

    public boolean hasCredential(int userId) {
        return !hasPassword(userId) ? hasPattern(userId) : true;
    }

    private boolean hasFile(String name) {
        byte[] contents = readFile(name);
        if (contents == null || contents.length <= 0) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] readFile(java.lang.String r10) {
        /*
        r9 = this;
        r6 = r9.mCache;
        monitor-enter(r6);
        r5 = r9.mCache;	 Catch:{ all -> 0x003f }
        r5 = r5.hasFile(r10);	 Catch:{ all -> 0x003f }
        if (r5 == 0) goto L_0x0013;
    L_0x000b:
        r5 = r9.mCache;	 Catch:{ all -> 0x003f }
        r5 = r5.peekFile(r10);	 Catch:{ all -> 0x003f }
        monitor-exit(r6);
        return r5;
    L_0x0013:
        r5 = r9.mCache;	 Catch:{ all -> 0x003f }
        r4 = r5.getVersion();	 Catch:{ all -> 0x003f }
        monitor-exit(r6);
        r1 = 0;
        r3 = 0;
        r2 = new java.io.RandomAccessFile;	 Catch:{ IOException -> 0x005e }
        r5 = "r";
        r2.<init>(r10, r5);	 Catch:{ IOException -> 0x005e }
        r6 = r2.length();	 Catch:{ IOException -> 0x00c1, all -> 0x00be }
        r5 = (int) r6;	 Catch:{ IOException -> 0x00c1, all -> 0x00be }
        r3 = new byte[r5];	 Catch:{ IOException -> 0x00c1, all -> 0x00be }
        r5 = r3.length;	 Catch:{ IOException -> 0x00c1, all -> 0x00be }
        r6 = 0;
        r2.readFully(r3, r6, r5);	 Catch:{ IOException -> 0x00c1, all -> 0x00be }
        r2.close();	 Catch:{ IOException -> 0x00c1, all -> 0x00be }
        if (r2 == 0) goto L_0x0038;
    L_0x0035:
        r2.close();	 Catch:{ IOException -> 0x0042 }
    L_0x0038:
        r1 = r2;
    L_0x0039:
        r5 = r9.mCache;
        r5.putFileIfUnchanged(r10, r3, r4);
        return r3;
    L_0x003f:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x0042:
        r0 = move-exception;
        r5 = "LockSettingsStorage";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Error closing file ";
        r6 = r6.append(r7);
        r6 = r6.append(r0);
        r6 = r6.toString();
        android.util.Slog.e(r5, r6);
        goto L_0x0038;
    L_0x005e:
        r0 = move-exception;
    L_0x005f:
        r5 = "LockSettingsStorage";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r6.<init>();	 Catch:{ all -> 0x009b }
        r7 = "Cannot read file ";
        r6 = r6.append(r7);	 Catch:{ all -> 0x009b }
        r6 = r6.append(r0);	 Catch:{ all -> 0x009b }
        r6 = r6.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.e(r5, r6);	 Catch:{ all -> 0x009b }
        if (r1 == 0) goto L_0x0039;
    L_0x007b:
        r1.close();	 Catch:{ IOException -> 0x007f }
        goto L_0x0039;
    L_0x007f:
        r0 = move-exception;
        r5 = "LockSettingsStorage";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Error closing file ";
        r6 = r6.append(r7);
        r6 = r6.append(r0);
        r6 = r6.toString();
        android.util.Slog.e(r5, r6);
        goto L_0x0039;
    L_0x009b:
        r5 = move-exception;
    L_0x009c:
        if (r1 == 0) goto L_0x00a1;
    L_0x009e:
        r1.close();	 Catch:{ IOException -> 0x00a2 }
    L_0x00a1:
        throw r5;
    L_0x00a2:
        r0 = move-exception;
        r6 = "LockSettingsStorage";
        r7 = new java.lang.StringBuilder;
        r7.<init>();
        r8 = "Error closing file ";
        r7 = r7.append(r8);
        r7 = r7.append(r0);
        r7 = r7.toString();
        android.util.Slog.e(r6, r7);
        goto L_0x00a1;
    L_0x00be:
        r5 = move-exception;
        r1 = r2;
        goto L_0x009c;
    L_0x00c1:
        r0 = move-exception;
        r1 = r2;
        goto L_0x005f;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsStorage.readFile(java.lang.String):byte[]");
    }

    private void writeFile(String name, byte[] hash) {
        IOException e;
        Throwable th;
        synchronized (this.mFileWriteLock) {
            RandomAccessFile randomAccessFile = null;
            try {
                RandomAccessFile raf = new RandomAccessFile(name, "rws");
                if (hash != null) {
                    try {
                        if (hash.length != 0) {
                            raf.write(hash, 0, hash.length);
                            raf.close();
                            if (raf != null) {
                                try {
                                    raf.close();
                                } catch (IOException e2) {
                                    Slog.e(TAG, "Error closing file " + e2);
                                } catch (Throwable th2) {
                                    th = th2;
                                    throw th;
                                }
                            }
                            randomAccessFile = raf;
                            this.mCache.putFile(name, hash);
                        }
                    } catch (IOException e3) {
                        e2 = e3;
                        randomAccessFile = raf;
                        try {
                            Slog.e(TAG, "Error writing to file " + e2);
                            if (randomAccessFile != null) {
                                try {
                                    randomAccessFile.close();
                                } catch (IOException e22) {
                                    Slog.e(TAG, "Error closing file " + e22);
                                } catch (Throwable th3) {
                                    th = th3;
                                    throw th;
                                }
                            }
                            this.mCache.putFile(name, hash);
                        } catch (Throwable th4) {
                            th = th4;
                            if (randomAccessFile != null) {
                                try {
                                    randomAccessFile.close();
                                } catch (IOException e222) {
                                    Slog.e(TAG, "Error closing file " + e222);
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th5) {
                        th = th5;
                        randomAccessFile = raf;
                        if (randomAccessFile != null) {
                            randomAccessFile.close();
                        }
                        throw th;
                    }
                }
                raf.setLength(0);
                raf.close();
                if (raf != null) {
                    raf.close();
                }
                randomAccessFile = raf;
            } catch (IOException e4) {
                e222 = e4;
                Slog.e(TAG, "Error writing to file " + e222);
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
                this.mCache.putFile(name, hash);
            }
            this.mCache.putFile(name, hash);
        }
    }

    private void deleteFile(String name) {
        synchronized (this.mFileWriteLock) {
            File file = new File(name);
            if (file.exists()) {
                file.delete();
                this.mCache.putFile(name, null);
            }
        }
    }

    public void writeCredentialHash(CredentialHash hash, int userId) {
        byte[] patternHash = null;
        byte[] passwordHash = null;
        if (hash.type == 2) {
            passwordHash = hash.hash;
        } else if (hash.type == 1) {
            patternHash = hash.hash;
        }
        writeFile(getLockPasswordFilename(userId), passwordHash);
        writeFile(getLockPatternFilename(userId), patternHash);
    }

    String getLockPatternFilename(int userId) {
        return getLockCredentialFilePathForUser(userId, LOCK_PATTERN_FILE);
    }

    String getLockPasswordFilename(int userId) {
        return getLockCredentialFilePathForUser(userId, LOCK_PASSWORD_FILE);
    }

    String getLegacyLockPatternFilename(int userId) {
        return getLockCredentialFilePathForUser(userId, LEGACY_LOCK_PATTERN_FILE);
    }

    String getLegacyLockPasswordFilename(int userId) {
        return getLockCredentialFilePathForUser(userId, LEGACY_LOCK_PASSWORD_FILE);
    }

    private String getBaseZeroLockPatternFilename(int userId) {
        return getLockCredentialFilePathForUser(userId, BASE_ZERO_LOCK_PATTERN_FILE);
    }

    String getChildProfileLockFile(int userId) {
        return getLockCredentialFilePathForUser(userId, CHILD_PROFILE_LOCK_FILE);
    }

    private String getLockCredentialFilePathForUser(int userId, String basename) {
        String dataSystemDirectory = Environment.getDataDirectory().getAbsolutePath() + SYSTEM_DIRECTORY;
        if (userId == 0) {
            return dataSystemDirectory + basename;
        }
        return new File(Environment.getUserSystemDirectory(userId), basename).getAbsolutePath();
    }

    public void writeSyntheticPasswordState(int userId, long handle, String name, byte[] data) {
        writeFile(getSynthenticPasswordStateFilePathForUser(userId, handle, name), data);
    }

    public byte[] readSyntheticPasswordState(int userId, long handle, String name) {
        return readFile(getSynthenticPasswordStateFilePathForUser(userId, handle, name));
    }

    public void deleteSyntheticPasswordState(int userId, long handle, String name) {
        String path = getSynthenticPasswordStateFilePathForUser(userId, handle, name);
        File file = new File(path);
        if (file.exists()) {
            try {
                ((StorageManager) this.mContext.getSystemService(StorageManager.class)).secdiscard(file.getAbsolutePath());
            } catch (Exception e) {
                Slog.w(TAG, "Failed to secdiscard " + path, e);
            } finally {
                file.delete();
            }
            this.mCache.putFile(path, null);
        }
    }

    public Map<Integer, List<Long>> listSyntheticPasswordHandlesForAllUsers(String stateName) {
        Map<Integer, List<Long>> result = new ArrayMap();
        for (UserInfo user : UserManager.get(this.mContext).getUsers(false)) {
            result.put(Integer.valueOf(user.id), listSyntheticPasswordHandlesForUser(stateName, user.id));
        }
        return result;
    }

    public List<Long> listSyntheticPasswordHandlesForUser(String stateName, int userId) {
        File baseDir = getSyntheticPasswordDirectoryForUser(userId);
        List<Long> result = new ArrayList();
        File[] files = baseDir.listFiles();
        if (files == null) {
            return result;
        }
        for (File file : files) {
            String[] parts = file.getName().split("\\.");
            if (parts.length == 2 && parts[1].equals(stateName)) {
                try {
                    result.add(Long.valueOf(Long.parseUnsignedLong(parts[0], 16)));
                } catch (NumberFormatException e) {
                    Slog.e(TAG, "Failed to parse handle " + parts[0]);
                }
            }
        }
        return result;
    }

    protected File getSyntheticPasswordDirectoryForUser(int userId) {
        return new File(Environment.getDataSystemDeDirectory(userId), SYNTHETIC_PASSWORD_DIRECTORY);
    }

    protected String getSynthenticPasswordStateFilePathForUser(int userId, long handle, String name) {
        File baseDir = getSyntheticPasswordDirectoryForUser(userId);
        String baseName = String.format("%016x.%s", new Object[]{Long.valueOf(handle), name});
        if (!baseDir.exists()) {
            baseDir.mkdir();
        }
        return new File(baseDir, baseName).getAbsolutePath();
    }

    public void removeUser(int userId) {
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        if (((UserManager) this.mContext.getSystemService(COLUMN_USERID)).getProfileParent(userId) == null) {
            synchronized (this.mFileWriteLock) {
                String name = getLockPasswordFilename(userId);
                File file = new File(name);
                if (file.exists()) {
                    file.delete();
                    this.mCache.putFile(name, null);
                }
                name = getLockPatternFilename(userId);
                file = new File(name);
                if (file.exists()) {
                    file.delete();
                    this.mCache.putFile(name, null);
                }
            }
        } else {
            removeChildProfileLock(userId);
        }
        File spStateDir = getSyntheticPasswordDirectoryForUser(userId);
        try {
            db.beginTransaction();
            db.delete(TABLE, "user='" + userId + "'", null);
            db.setTransactionSuccessful();
            this.mCache.removeUser(userId);
            this.mCache.purgePath(spStateDir.getAbsolutePath());
        } finally {
            db.endTransaction();
        }
    }

    void closeDatabase() {
        this.mOpenHelper.close();
    }

    void clearCache() {
        this.mCache.clear();
    }

    public PersistentDataBlockManagerInternal getPersistentDataBlock() {
        if (this.mPersistentDataBlockManagerInternal == null) {
            this.mPersistentDataBlockManagerInternal = (PersistentDataBlockManagerInternal) LocalServices.getService(PersistentDataBlockManagerInternal.class);
        }
        return this.mPersistentDataBlockManagerInternal;
    }

    public void writePersistentDataBlock(int persistentType, int userId, int qualityForUi, byte[] payload) {
        PersistentDataBlockManagerInternal persistentDataBlock = getPersistentDataBlock();
        if (persistentDataBlock != null) {
            persistentDataBlock.setFrpCredentialHandle(PersistentData.toBytes(persistentType, userId, qualityForUi, payload));
        }
    }

    public PersistentData readPersistentDataBlock() {
        PersistentDataBlockManagerInternal persistentDataBlock = getPersistentDataBlock();
        if (persistentDataBlock == null) {
            return PersistentData.NONE;
        }
        return PersistentData.fromBytes(persistentDataBlock.getFrpCredentialHandle());
    }
}
