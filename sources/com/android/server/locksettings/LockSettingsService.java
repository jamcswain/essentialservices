package com.android.server.locksettings;

import android.app.ActivityManager;
import android.app.IActivityManager;
import android.app.KeyguardManager;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.app.admin.PasswordMetrics;
import android.app.backup.BackupManager;
import android.app.trust.IStrongAuthTracker;
import android.app.trust.TrustManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.UserInfo;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.IProgressListener;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.os.ShellCallback;
import android.os.StrictMode;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.storage.IStorageManager;
import android.os.storage.StorageManager;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.security.KeyStore;
import android.security.KeyStore.State;
import android.security.keystore.AndroidKeyStoreProvider;
import android.security.keystore.KeyProtection;
import android.service.gatekeeper.GateKeeperResponse;
import android.service.gatekeeper.IGateKeeperService;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.EventLog;
import android.util.Log;
import android.util.Slog;
import com.android.internal.notification.SystemNotificationChannels;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.Preconditions;
import com.android.internal.widget.ICheckCredentialProgressCallback;
import com.android.internal.widget.ILockSettings.Stub;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockPatternUtils.StrongAuthTracker;
import com.android.internal.widget.VerifyCredentialResponse;
import com.android.server.SystemService;
import com.android.server.backup.RefactoredBackupManagerService;
import com.android.server.locksettings.LockSettingsStorage.Callback;
import com.android.server.locksettings.LockSettingsStorage.CredentialHash;
import com.android.server.locksettings.LockSettingsStorage.PersistentData;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore.SecretKeyEntry;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import libcore.util.HexEncoding;

public class LockSettingsService extends Stub {
    private static final boolean DEBUG = false;
    private static final String PERMISSION = "android.permission.ACCESS_KEYGUARD_SECURE_STORAGE";
    private static final int PROFILE_KEY_IV_SIZE = 12;
    private static final String[] READ_CONTACTS_PROTECTED_SETTINGS = new String[]{"lock_screen_owner_info_enabled", "lock_screen_owner_info"};
    private static final String[] READ_PASSWORD_PROTECTED_SETTINGS = new String[]{"lockscreen.password_salt", "lockscreen.passwordhistory", "lockscreen.password_type", SEPARATE_PROFILE_CHALLENGE_KEY};
    private static final String SEPARATE_PROFILE_CHALLENGE_KEY = "lockscreen.profilechallenge";
    private static final String[] SETTINGS_TO_BACKUP = new String[]{"lock_screen_owner_info_enabled", "lock_screen_owner_info", "lock_pattern_visible_pattern", "lockscreen.power_button_instantly_locks"};
    private static final int SYNTHETIC_PASSWORD_ENABLED_BY_DEFAULT = 1;
    private static final int[] SYSTEM_CREDENTIAL_UIDS = new int[]{1010, 1016, 0, 1000};
    private static final String TAG = "LockSettingsService";
    private static final String[] VALID_SETTINGS = new String[]{"lockscreen.lockedoutpermanently", "lockscreen.lockoutattemptdeadline", "lockscreen.patterneverchosen", "lockscreen.password_type", "lockscreen.password_type_alternate", "lockscreen.password_salt", "lockscreen.disabled", "lockscreen.options", "lockscreen.biometric_weak_fallback", "lockscreen.biometricweakeverchosen", "lockscreen.power_button_instantly_locks", "lockscreen.passwordhistory", "lock_pattern_autolock", "lock_biometric_weak_flags", "lock_pattern_visible_pattern", "lock_pattern_tactile_feedback_enabled"};
    private final IActivityManager mActivityManager;
    private final BroadcastReceiver mBroadcastReceiver;
    private final Context mContext;
    private final DeviceProvisionedObserver mDeviceProvisionedObserver;
    private boolean mFirstCallToVold;
    protected IGateKeeperService mGateKeeperService;
    protected final Handler mHandler;
    private final Injector mInjector;
    private final KeyStore mKeyStore;
    private final LockPatternUtils mLockPatternUtils;
    private final NotificationManager mNotificationManager;
    private final Object mSeparateChallengeLock;
    private final SyntheticPasswordManager mSpManager;
    protected final LockSettingsStorage mStorage;
    private final LockSettingsStrongAuth mStrongAuth;
    private final SynchronizedStrongAuthTracker mStrongAuthTracker;
    private final UserManager mUserManager;

    private class DeviceProvisionedObserver extends ContentObserver {
        private final Uri mDeviceProvisionedUri = Global.getUriFor("device_provisioned");
        private boolean mRegistered;

        public DeviceProvisionedObserver() {
            super(null);
        }

        public void onChange(boolean selfChange, Uri uri) {
            if (this.mDeviceProvisionedUri.equals(uri)) {
                updateRegistration();
                if (isProvisioned()) {
                    Slog.i(LockSettingsService.TAG, "Reporting device setup complete to IGateKeeperService");
                    reportDeviceSetupComplete();
                    clearFrpCredentialIfOwnerNotSecure();
                }
            }
        }

        public void onSystemReady() {
            if (LockPatternUtils.frpCredentialEnabled(LockSettingsService.this.mContext)) {
                updateRegistration();
            } else if (!isProvisioned()) {
                Slog.i(LockSettingsService.TAG, "FRP credential disabled, reporting device setup complete to Gatekeeper immediately");
                reportDeviceSetupComplete();
            }
        }

        private void reportDeviceSetupComplete() {
            try {
                LockSettingsService.this.getGateKeeperService().reportDeviceSetupComplete();
            } catch (RemoteException e) {
                Slog.e(LockSettingsService.TAG, "Failure reporting to IGateKeeperService", e);
            }
        }

        private void clearFrpCredentialIfOwnerNotSecure() {
            for (UserInfo user : LockSettingsService.this.mUserManager.getUsers()) {
                if (LockPatternUtils.userOwnsFrpCredential(LockSettingsService.this.mContext, user)) {
                    if (!LockSettingsService.this.isUserSecure(user.id)) {
                        LockSettingsService.this.mStorage.writePersistentDataBlock(0, user.id, 0, null);
                    }
                    return;
                }
            }
        }

        private void updateRegistration() {
            boolean register = isProvisioned() ^ 1;
            if (register != this.mRegistered) {
                if (register) {
                    LockSettingsService.this.mContext.getContentResolver().registerContentObserver(this.mDeviceProvisionedUri, false, this);
                } else {
                    LockSettingsService.this.mContext.getContentResolver().unregisterContentObserver(this);
                }
                this.mRegistered = register;
            }
        }

        private boolean isProvisioned() {
            return Global.getInt(LockSettingsService.this.mContext.getContentResolver(), "device_provisioned", 0) != 0;
        }
    }

    private class GateKeeperDiedRecipient implements DeathRecipient {
        private GateKeeperDiedRecipient() {
        }

        public void binderDied() {
            LockSettingsService.this.mGateKeeperService.asBinder().unlinkToDeath(this, 0);
            LockSettingsService.this.mGateKeeperService = null;
        }
    }

    static class Injector {
        protected Context mContext;

        public Injector(Context context) {
            this.mContext = context;
        }

        public Context getContext() {
            return this.mContext;
        }

        public Handler getHandler() {
            return new Handler();
        }

        public LockSettingsStorage getStorage() {
            final LockSettingsStorage storage = new LockSettingsStorage(this.mContext);
            storage.setDatabaseOnCreateCallback(new Callback() {
                public void initialize(SQLiteDatabase db) {
                    if (SystemProperties.getBoolean("ro.lockscreen.disable.default", false)) {
                        storage.writeKeyValue(db, "lockscreen.disabled", "1", 0);
                    }
                }
            });
            return storage;
        }

        public LockSettingsStrongAuth getStrongAuth() {
            return new LockSettingsStrongAuth(this.mContext);
        }

        public SynchronizedStrongAuthTracker getStrongAuthTracker() {
            return new SynchronizedStrongAuthTracker(this.mContext);
        }

        public IActivityManager getActivityManager() {
            return ActivityManager.getService();
        }

        public LockPatternUtils getLockPatternUtils() {
            return new LockPatternUtils(this.mContext);
        }

        public NotificationManager getNotificationManager() {
            return (NotificationManager) this.mContext.getSystemService("notification");
        }

        public UserManager getUserManager() {
            return (UserManager) this.mContext.getSystemService("user");
        }

        public DevicePolicyManager getDevicePolicyManager() {
            return (DevicePolicyManager) this.mContext.getSystemService("device_policy");
        }

        public KeyStore getKeyStore() {
            return KeyStore.getInstance();
        }

        public IStorageManager getStorageManager() {
            IBinder service = ServiceManager.getService("mount");
            if (service != null) {
                return IStorageManager.Stub.asInterface(service);
            }
            return null;
        }

        public SyntheticPasswordManager getSyntheticPasswordManager(LockSettingsStorage storage) {
            return new SyntheticPasswordManager(getContext(), storage, getUserManager());
        }

        public int binderGetCallingUid() {
            return Binder.getCallingUid();
        }
    }

    public static final class Lifecycle extends SystemService {
        private LockSettingsService mLockSettingsService;

        public Lifecycle(Context context) {
            super(context);
        }

        public void onStart() {
            AndroidKeyStoreProvider.install();
            this.mLockSettingsService = new LockSettingsService(getContext());
            publishBinderService("lock_settings", this.mLockSettingsService);
        }

        public void onBootPhase(int phase) {
            super.onBootPhase(phase);
            if (phase == SystemService.PHASE_ACTIVITY_MANAGER_READY) {
                this.mLockSettingsService.migrateOldDataAfterSystemReady();
            }
        }

        public void onStartUser(int userHandle) {
            this.mLockSettingsService.onStartUser(userHandle);
        }

        public void onUnlockUser(int userHandle) {
            this.mLockSettingsService.onUnlockUser(userHandle);
        }

        public void onCleanupUser(int userHandle) {
            this.mLockSettingsService.onCleanupUser(userHandle);
        }
    }

    protected static class SynchronizedStrongAuthTracker extends StrongAuthTracker {
        public SynchronizedStrongAuthTracker(Context context) {
            super(context);
        }

        protected void handleStrongAuthRequiredChanged(int strongAuthFlags, int userId) {
            synchronized (this) {
                super.handleStrongAuthRequiredChanged(strongAuthFlags, userId);
            }
        }

        public int getStrongAuthForUser(int userId) {
            int strongAuthForUser;
            synchronized (this) {
                strongAuthForUser = super.getStrongAuthForUser(userId);
            }
            return strongAuthForUser;
        }

        void register(LockSettingsStrongAuth strongAuth) {
            strongAuth.registerStrongAuthTracker(this.mStub);
        }
    }

    public void tieManagedProfileLockIfNecessary(int managedUserId, String managedUserPassword) {
        if (this.mUserManager.getUserInfo(managedUserId).isManagedProfile() && !this.mLockPatternUtils.isSeparateProfileChallengeEnabled(managedUserId) && !this.mStorage.hasChildProfileLock(managedUserId)) {
            int parentId = this.mUserManager.getProfileParent(managedUserId).id;
            if (isUserSecure(parentId)) {
                try {
                    if (getGateKeeperService().getSecureUserId(parentId) != 0) {
                        byte[] randomLockSeed = new byte[0];
                        try {
                            String newPassword = String.valueOf(HexEncoding.encode(SecureRandom.getInstance("SHA1PRNG").generateSeed(40)));
                            setLockCredentialInternal(newPassword, 2, managedUserPassword, 327680, managedUserId);
                            setLong("lockscreen.password_type", 327680, managedUserId);
                            tieProfileLockToParent(managedUserId, newPassword);
                        } catch (Exception e) {
                            Slog.e(TAG, "Fail to tie managed profile", e);
                        }
                    }
                } catch (RemoteException e2) {
                    Slog.e(TAG, "Failed to talk to GateKeeper service", e2);
                }
            }
        }
    }

    public LockSettingsService(Context context) {
        this(new Injector(context));
    }

    protected LockSettingsService(Injector injector) {
        this.mSeparateChallengeLock = new Object();
        this.mDeviceProvisionedObserver = new DeviceProvisionedObserver();
        this.mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                int userHandle;
                if ("android.intent.action.USER_ADDED".equals(intent.getAction())) {
                    userHandle = intent.getIntExtra("android.intent.extra.user_handle", 0);
                    if (userHandle > 0) {
                        LockSettingsService.this.removeUser(userHandle, true);
                    }
                    KeyStore ks = KeyStore.getInstance();
                    UserInfo parentInfo = LockSettingsService.this.mUserManager.getProfileParent(userHandle);
                    ks.onUserAdded(userHandle, parentInfo != null ? parentInfo.id : -1);
                } else if ("android.intent.action.USER_STARTING".equals(intent.getAction())) {
                    LockSettingsService.this.mStorage.prefetchUser(intent.getIntExtra("android.intent.extra.user_handle", 0));
                } else if ("android.intent.action.USER_REMOVED".equals(intent.getAction())) {
                    userHandle = intent.getIntExtra("android.intent.extra.user_handle", 0);
                    if (userHandle > 0) {
                        LockSettingsService.this.removeUser(userHandle, false);
                    }
                }
            }
        };
        this.mInjector = injector;
        this.mContext = injector.getContext();
        this.mKeyStore = injector.getKeyStore();
        this.mHandler = injector.getHandler();
        this.mStrongAuth = injector.getStrongAuth();
        this.mActivityManager = injector.getActivityManager();
        this.mLockPatternUtils = injector.getLockPatternUtils();
        this.mFirstCallToVold = true;
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.USER_ADDED");
        filter.addAction("android.intent.action.USER_STARTING");
        filter.addAction("android.intent.action.USER_REMOVED");
        injector.getContext().registerReceiverAsUser(this.mBroadcastReceiver, UserHandle.ALL, filter, null, null);
        this.mStorage = injector.getStorage();
        this.mNotificationManager = injector.getNotificationManager();
        this.mUserManager = injector.getUserManager();
        this.mStrongAuthTracker = injector.getStrongAuthTracker();
        this.mStrongAuthTracker.register(this.mStrongAuth);
        this.mSpManager = injector.getSyntheticPasswordManager(this.mStorage);
    }

    private void maybeShowEncryptionNotificationForUser(int userId) {
        UserInfo user = this.mUserManager.getUserInfo(userId);
        if (user.isManagedProfile()) {
            UserHandle userHandle = user.getUserHandle();
            if (isUserSecure(userId) && (this.mUserManager.isUserUnlockingOrUnlocked(userHandle) ^ 1) != 0) {
                UserInfo parent = this.mUserManager.getProfileParent(userId);
                if (!(parent == null || !this.mUserManager.isUserUnlockingOrUnlocked(parent.getUserHandle()) || (this.mUserManager.isQuietModeEnabled(userHandle) ^ 1) == 0)) {
                    showEncryptionNotificationForProfile(userHandle);
                }
            }
        }
    }

    private void showEncryptionNotificationForProfile(UserHandle user) {
        Resources r = this.mContext.getResources();
        CharSequence title = r.getText(17040921);
        CharSequence message = r.getText(17040649);
        CharSequence detail = r.getText(17040648);
        Intent unlockIntent = ((KeyguardManager) this.mContext.getSystemService("keyguard")).createConfirmDeviceCredentialIntent(null, null, user.getIdentifier());
        if (unlockIntent != null) {
            unlockIntent.setFlags(276824064);
            showEncryptionNotification(user, title, message, detail, PendingIntent.getActivity(this.mContext, 0, unlockIntent, 134217728));
        }
    }

    private void showEncryptionNotification(UserHandle user, CharSequence title, CharSequence message, CharSequence detail, PendingIntent intent) {
        if (StorageManager.isFileEncryptedNativeOrEmulated()) {
            this.mNotificationManager.notifyAsUser(null, 9, new Builder(this.mContext, SystemNotificationChannels.SECURITY).setSmallIcon(17302750).setWhen(0).setOngoing(true).setTicker(title).setColor(this.mContext.getColor(17170763)).setContentTitle(title).setContentText(message).setSubText(detail).setVisibility(1).setContentIntent(intent).build(), user);
        }
    }

    private void hideEncryptionNotification(UserHandle userHandle) {
        this.mNotificationManager.cancelAsUser(null, 9, userHandle);
    }

    public void onCleanupUser(int userId) {
        hideEncryptionNotification(new UserHandle(userId));
    }

    public void onStartUser(int userId) {
        maybeShowEncryptionNotificationForUser(userId);
    }

    private void ensureProfileKeystoreUnlocked(int userId) {
        if (KeyStore.getInstance().state(userId) == State.LOCKED && tiedManagedProfileReadyToUnlock(this.mUserManager.getUserInfo(userId))) {
            Slog.i(TAG, "Managed profile got unlocked, will unlock its keystore");
            try {
                unlockChildProfile(userId, true);
            } catch (RemoteException e) {
                Slog.e(TAG, "Failed to unlock child profile");
            }
        }
    }

    public void onUnlockUser(final int userId) {
        this.mHandler.post(new Runnable() {
            public void run() {
                LockSettingsService.this.ensureProfileKeystoreUnlocked(userId);
                LockSettingsService.this.hideEncryptionNotification(new UserHandle(userId));
                List<UserInfo> profiles = LockSettingsService.this.mUserManager.getProfiles(userId);
                for (int i = 0; i < profiles.size(); i++) {
                    UserInfo profile = (UserInfo) profiles.get(i);
                    if (LockSettingsService.this.isUserSecure(profile.id) && profile.isManagedProfile()) {
                        UserHandle userHandle = profile.getUserHandle();
                        if (!(LockSettingsService.this.mUserManager.isUserUnlockingOrUnlocked(userHandle) || (LockSettingsService.this.mUserManager.isQuietModeEnabled(userHandle) ^ 1) == 0)) {
                            LockSettingsService.this.showEncryptionNotificationForProfile(userHandle);
                        }
                    }
                }
                if (LockSettingsService.this.mUserManager.getUserInfo(userId).isManagedProfile()) {
                    LockSettingsService.this.tieManagedProfileLockIfNecessary(userId, null);
                }
            }
        });
    }

    public void systemReady() {
        if (this.mContext.checkCallingOrSelfPermission(PERMISSION) != 0) {
            EventLog.writeEvent(1397638484, new Object[]{"28251513", Integer.valueOf(getCallingUid()), ""});
        }
        checkWritePermission(0);
        migrateOldData();
        try {
            getGateKeeperService();
            this.mSpManager.initWeaverService();
        } catch (RemoteException e) {
            Slog.e(TAG, "Failure retrieving IGateKeeperService", e);
        }
        this.mDeviceProvisionedObserver.onSystemReady();
        this.mStorage.prefetchUser(0);
        this.mStrongAuth.systemReady();
    }

    private void migrateOldData() {
        try {
            ContentResolver cr;
            List<UserInfo> users;
            int userId;
            int i;
            int userCount;
            if (getString("migrated", null, 0) == null) {
                cr = this.mContext.getContentResolver();
                for (String validSetting : VALID_SETTINGS) {
                    String value = Secure.getString(cr, validSetting);
                    if (value != null) {
                        setString(validSetting, value, 0);
                    }
                }
                setString("migrated", "true", 0);
                Slog.i(TAG, "Migrated lock settings to new location");
            }
            if (getString("migrated_user_specific", null, 0) == null) {
                cr = this.mContext.getContentResolver();
                users = this.mUserManager.getUsers();
                for (int user = 0; user < users.size(); user++) {
                    userId = ((UserInfo) users.get(user)).id;
                    String OWNER_INFO = "lock_screen_owner_info";
                    String ownerInfo = Secure.getStringForUser(cr, "lock_screen_owner_info", userId);
                    if (!TextUtils.isEmpty(ownerInfo)) {
                        setString("lock_screen_owner_info", ownerInfo, userId);
                        Secure.putStringForUser(cr, "lock_screen_owner_info", "", userId);
                    }
                    String OWNER_INFO_ENABLED = "lock_screen_owner_info_enabled";
                    try {
                        setLong("lock_screen_owner_info_enabled", (long) (Secure.getIntForUser(cr, "lock_screen_owner_info_enabled", userId) != 0 ? 1 : 0), userId);
                    } catch (SettingNotFoundException e) {
                        if (!TextUtils.isEmpty(ownerInfo)) {
                            setLong("lock_screen_owner_info_enabled", 1, userId);
                        }
                    }
                    Secure.putIntForUser(cr, "lock_screen_owner_info_enabled", 0, userId);
                }
                setString("migrated_user_specific", "true", 0);
                Slog.i(TAG, "Migrated per-user lock settings to new location");
            }
            if (getString("migrated_biometric_weak", null, 0) == null) {
                users = this.mUserManager.getUsers();
                for (i = 0; i < users.size(); i++) {
                    userId = ((UserInfo) users.get(i)).id;
                    long type = getLong("lockscreen.password_type", 0, userId);
                    long alternateType = getLong("lockscreen.password_type_alternate", 0, userId);
                    if (type == 32768) {
                        setLong("lockscreen.password_type", alternateType, userId);
                    }
                    setLong("lockscreen.password_type_alternate", 0, userId);
                }
                setString("migrated_biometric_weak", "true", 0);
                Slog.i(TAG, "Migrated biometric weak to use the fallback instead");
            }
            if (getString("migrated_lockscreen_disabled", null, 0) == null) {
                users = this.mUserManager.getUsers();
                userCount = users.size();
                int switchableUsers = 0;
                for (i = 0; i < userCount; i++) {
                    if (((UserInfo) users.get(i)).supportsSwitchTo()) {
                        switchableUsers++;
                    }
                }
                if (switchableUsers > 1) {
                    for (i = 0; i < userCount; i++) {
                        int id = ((UserInfo) users.get(i)).id;
                        if (getBoolean("lockscreen.disabled", false, id)) {
                            setBoolean("lockscreen.disabled", false, id);
                        }
                    }
                }
                setString("migrated_lockscreen_disabled", "true", 0);
                Slog.i(TAG, "Migrated lockscreen disabled flag");
            }
            users = this.mUserManager.getUsers();
            for (i = 0; i < users.size(); i++) {
                UserInfo userInfo = (UserInfo) users.get(i);
                if (userInfo.isManagedProfile() && this.mStorage.hasChildProfileLock(userInfo.id)) {
                    long quality = getLong("lockscreen.password_type", 0, userInfo.id);
                    if (quality == 0) {
                        Slog.i(TAG, "Migrated tied profile lock type");
                        setLong("lockscreen.password_type", 327680, userInfo.id);
                    } else if (quality != 327680) {
                        Slog.e(TAG, "Invalid tied profile lock type: " + quality);
                    }
                }
                try {
                    String alias = "profile_key_name_encrypt_" + userInfo.id;
                    java.security.KeyStore keyStore = java.security.KeyStore.getInstance("AndroidKeyStore");
                    keyStore.load(null);
                    if (keyStore.containsAlias(alias)) {
                        keyStore.deleteEntry(alias);
                    }
                } catch (Exception e2) {
                    Slog.e(TAG, "Unable to remove tied profile key", e2);
                }
            }
            if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.type.watch") && getString("migrated_wear_lockscreen_disabled", null, 0) == null) {
                userCount = users.size();
                for (i = 0; i < userCount; i++) {
                    setBoolean("lockscreen.disabled", false, ((UserInfo) users.get(i)).id);
                }
                setString("migrated_wear_lockscreen_disabled", "true", 0);
                Slog.i(TAG, "Migrated lockscreen_disabled for Wear devices");
            }
        } catch (Throwable re) {
            Slog.e(TAG, "Unable to migrate old data", re);
        }
    }

    private void migrateOldDataAfterSystemReady() {
        try {
            if (LockPatternUtils.frpCredentialEnabled(this.mContext) && (getBoolean("migrated_frp", false, 0) ^ 1) != 0) {
                migrateFrpCredential();
                setBoolean("migrated_frp", true, 0);
                Slog.i(TAG, "Migrated migrated_frp.");
            }
        } catch (RemoteException e) {
            Slog.e(TAG, "Unable to migrateOldDataAfterSystemReady", e);
        }
    }

    private void migrateFrpCredential() throws RemoteException {
        if (this.mStorage.readPersistentDataBlock() == PersistentData.NONE) {
            for (UserInfo userInfo : this.mUserManager.getUsers()) {
                if (LockPatternUtils.userOwnsFrpCredential(this.mContext, userInfo) && isUserSecure(userInfo.id)) {
                    synchronized (this.mSpManager) {
                        if (isSyntheticPasswordBasedCredentialLocked(userInfo.id)) {
                            this.mSpManager.migrateFrpPasswordLocked(getSyntheticPasswordHandleLocked(userInfo.id), userInfo, redactActualQualityToMostLenientEquivalentQuality((int) getLong("lockscreen.password_type", 0, userInfo.id)));
                        }
                    }
                    return;
                }
            }
        }
    }

    private int redactActualQualityToMostLenientEquivalentQuality(int quality) {
        switch (quality) {
            case DumpState.DUMP_INTENT_FILTER_VERIFIERS /*131072*/:
            case 196608:
                return DumpState.DUMP_INTENT_FILTER_VERIFIERS;
            case DumpState.DUMP_DOMAIN_PREFERRED /*262144*/:
            case 327680:
            case 393216:
                return DumpState.DUMP_DOMAIN_PREFERRED;
            default:
                return quality;
        }
    }

    private final void checkWritePermission(int userId) {
        this.mContext.enforceCallingOrSelfPermission(PERMISSION, "LockSettingsWrite");
    }

    private final void checkPasswordReadPermission(int userId) {
        this.mContext.enforceCallingOrSelfPermission(PERMISSION, "LockSettingsRead");
    }

    private final void checkPasswordHavePermission(int userId) {
        if (this.mContext.checkCallingOrSelfPermission(PERMISSION) != 0) {
            EventLog.writeEvent(1397638484, new Object[]{"28251513", Integer.valueOf(getCallingUid()), ""});
        }
        this.mContext.enforceCallingOrSelfPermission(PERMISSION, "LockSettingsHave");
    }

    private final void checkReadPermission(String requestedKey, int userId) {
        int callingUid = Binder.getCallingUid();
        int i = 0;
        while (i < READ_CONTACTS_PROTECTED_SETTINGS.length) {
            if (!READ_CONTACTS_PROTECTED_SETTINGS[i].equals(requestedKey) || this.mContext.checkCallingOrSelfPermission("android.permission.READ_CONTACTS") == 0) {
                i++;
            } else {
                throw new SecurityException("uid=" + callingUid + " needs permission " + "android.permission.READ_CONTACTS" + " to read " + requestedKey + " for user " + userId);
            }
        }
        i = 0;
        while (i < READ_PASSWORD_PROTECTED_SETTINGS.length) {
            if (!READ_PASSWORD_PROTECTED_SETTINGS[i].equals(requestedKey) || this.mContext.checkCallingOrSelfPermission(PERMISSION) == 0) {
                i++;
            } else {
                throw new SecurityException("uid=" + callingUid + " needs permission " + PERMISSION + " to read " + requestedKey + " for user " + userId);
            }
        }
    }

    public boolean getSeparateProfileChallengeEnabled(int userId) throws RemoteException {
        boolean z;
        checkReadPermission(SEPARATE_PROFILE_CHALLENGE_KEY, userId);
        synchronized (this.mSeparateChallengeLock) {
            z = getBoolean(SEPARATE_PROFILE_CHALLENGE_KEY, false, userId);
        }
        return z;
    }

    public void setSeparateProfileChallengeEnabled(int userId, boolean enabled, String managedUserPassword) throws RemoteException {
        checkWritePermission(userId);
        synchronized (this.mSeparateChallengeLock) {
            setBoolean(SEPARATE_PROFILE_CHALLENGE_KEY, enabled, userId);
            if (enabled) {
                this.mStorage.removeChildProfileLock(userId);
                removeKeystoreProfileKey(userId);
            } else {
                tieManagedProfileLockIfNecessary(userId, managedUserPassword);
            }
        }
    }

    public void setBoolean(String key, boolean value, int userId) throws RemoteException {
        checkWritePermission(userId);
        setStringUnchecked(key, userId, value ? "1" : "0");
    }

    public void setLong(String key, long value, int userId) throws RemoteException {
        checkWritePermission(userId);
        setStringUnchecked(key, userId, Long.toString(value));
    }

    public void setString(String key, String value, int userId) throws RemoteException {
        checkWritePermission(userId);
        setStringUnchecked(key, userId, value);
    }

    private void setStringUnchecked(String key, int userId, String value) {
        Preconditions.checkArgument(userId != -9999, "cannot store lock settings for FRP user");
        this.mStorage.writeKeyValue(key, value, userId);
        if (ArrayUtils.contains(SETTINGS_TO_BACKUP, key)) {
            BackupManager.dataChanged(RefactoredBackupManagerService.SETTINGS_PACKAGE);
        }
    }

    public boolean getBoolean(String key, boolean defaultValue, int userId) {
        checkReadPermission(key, userId);
        String value = getStringUnchecked(key, null, userId);
        if (TextUtils.isEmpty(value)) {
            return defaultValue;
        }
        return !value.equals("1") ? value.equals("true") : true;
    }

    public long getLong(String key, long defaultValue, int userId) {
        checkReadPermission(key, userId);
        String value = getStringUnchecked(key, null, userId);
        return TextUtils.isEmpty(value) ? defaultValue : Long.parseLong(value);
    }

    public String getString(String key, String defaultValue, int userId) {
        checkReadPermission(key, userId);
        return getStringUnchecked(key, defaultValue, userId);
    }

    public String getStringUnchecked(String key, String defaultValue, int userId) {
        if ("lock_pattern_autolock".equals(key)) {
            long ident = Binder.clearCallingIdentity();
            try {
                String str = this.mLockPatternUtils.isLockPatternEnabled(userId) ? "1" : "0";
                Binder.restoreCallingIdentity(ident);
                return str;
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        } else if (userId == -9999) {
            return getFrpStringUnchecked(key);
        } else {
            if ("legacy_lock_pattern_enabled".equals(key)) {
                key = "lock_pattern_autolock";
            }
            return this.mStorage.readKeyValue(key, defaultValue, userId);
        }
    }

    private String getFrpStringUnchecked(String key) {
        if ("lockscreen.password_type".equals(key)) {
            return String.valueOf(readFrpPasswordQuality());
        }
        return null;
    }

    private int readFrpPasswordQuality() {
        return this.mStorage.readPersistentDataBlock().qualityForUi;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean havePassword(int r6) throws android.os.RemoteException {
        /*
        r5 = this;
        r5.checkPasswordHavePermission(r6);
        r3 = r5.mSpManager;
        monitor-enter(r3);
        r2 = r5.isSyntheticPasswordBasedCredentialLocked(r6);	 Catch:{ all -> 0x0026 }
        if (r2 == 0) goto L_0x001e;
    L_0x000c:
        r0 = r5.getSyntheticPasswordHandleLocked(r6);	 Catch:{ all -> 0x0026 }
        r2 = r5.mSpManager;	 Catch:{ all -> 0x0026 }
        r2 = r2.getCredentialType(r0, r6);	 Catch:{ all -> 0x0026 }
        r4 = 2;
        if (r2 != r4) goto L_0x001c;
    L_0x0019:
        r2 = 1;
    L_0x001a:
        monitor-exit(r3);
        return r2;
    L_0x001c:
        r2 = 0;
        goto L_0x001a;
    L_0x001e:
        monitor-exit(r3);
        r2 = r5.mStorage;
        r2 = r2.hasPassword(r6);
        return r2;
    L_0x0026:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsService.havePassword(int):boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean havePattern(int r6) throws android.os.RemoteException {
        /*
        r5 = this;
        r2 = 1;
        r5.checkPasswordHavePermission(r6);
        r3 = r5.mSpManager;
        monitor-enter(r3);
        r4 = r5.isSyntheticPasswordBasedCredentialLocked(r6);	 Catch:{ all -> 0x0025 }
        if (r4 == 0) goto L_0x001d;
    L_0x000d:
        r0 = r5.getSyntheticPasswordHandleLocked(r6);	 Catch:{ all -> 0x0025 }
        r4 = r5.mSpManager;	 Catch:{ all -> 0x0025 }
        r4 = r4.getCredentialType(r0, r6);	 Catch:{ all -> 0x0025 }
        if (r4 != r2) goto L_0x001b;
    L_0x0019:
        monitor-exit(r3);
        return r2;
    L_0x001b:
        r2 = 0;
        goto L_0x0019;
    L_0x001d:
        monitor-exit(r3);
        r2 = r5.mStorage;
        r2 = r2.hasPattern(r6);
        return r2;
    L_0x0025:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsService.havePattern(int):boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean isUserSecure(int r7) {
        /*
        r6 = this;
        r4 = r6.mSpManager;
        monitor-enter(r4);
        r1 = r6.isSyntheticPasswordBasedCredentialLocked(r7);	 Catch:{ RemoteException -> 0x001b, all -> 0x0024 }
        if (r1 == 0) goto L_0x001c;
    L_0x0009:
        r2 = r6.getSyntheticPasswordHandleLocked(r7);	 Catch:{ RemoteException -> 0x001b, all -> 0x0024 }
        r1 = r6.mSpManager;	 Catch:{ RemoteException -> 0x001b, all -> 0x0024 }
        r1 = r1.getCredentialType(r2, r7);	 Catch:{ RemoteException -> 0x001b, all -> 0x0024 }
        r5 = -1;
        if (r1 == r5) goto L_0x0019;
    L_0x0016:
        r1 = 1;
    L_0x0017:
        monitor-exit(r4);
        return r1;
    L_0x0019:
        r1 = 0;
        goto L_0x0017;
    L_0x001b:
        r0 = move-exception;
    L_0x001c:
        monitor-exit(r4);
        r1 = r6.mStorage;
        r1 = r1.hasCredential(r7);
        return r1;
    L_0x0024:
        r1 = move-exception;
        monitor-exit(r4);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsService.isUserSecure(int):boolean");
    }

    private void setKeystorePassword(String password, int userHandle) {
        KeyStore.getInstance().onUserPasswordChanged(userHandle, password);
    }

    private void unlockKeystore(String password, int userHandle) {
        KeyStore.getInstance().unlock(userHandle, password);
    }

    protected String getDecryptedPasswordForTiedProfile(int userId) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, CertificateException, IOException {
        byte[] storedData = this.mStorage.readChildProfileLock(userId);
        if (storedData == null) {
            throw new FileNotFoundException("Child profile lock file not found");
        }
        byte[] iv = Arrays.copyOfRange(storedData, 0, 12);
        byte[] encryptedPassword = Arrays.copyOfRange(storedData, 12, storedData.length);
        java.security.KeyStore keyStore = java.security.KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        SecretKey decryptionKey = (SecretKey) keyStore.getKey("profile_key_name_decrypt_" + userId, null);
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        cipher.init(2, decryptionKey, new GCMParameterSpec(128, iv));
        return new String(cipher.doFinal(encryptedPassword), StandardCharsets.UTF_8);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void unlockChildProfile(int r10, boolean r11) throws android.os.RemoteException {
        /*
        r9 = this;
        r1 = r9.getDecryptedPasswordForTiedProfile(r10);	 Catch:{ UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f }
        r4 = 0;
        r2 = 2;
        r3 = 0;
        r7 = 0;
        r0 = r9;
        r6 = r10;
        r0.doVerifyCredential(r1, r2, r3, r4, r6, r7);	 Catch:{ UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f, UnrecoverableKeyException -> 0x000f }
    L_0x000e:
        return;
    L_0x000f:
        r8 = move-exception;
        r0 = r8 instanceof java.io.FileNotFoundException;
        if (r0 == 0) goto L_0x001e;
    L_0x0014:
        r0 = "LockSettingsService";
        r1 = "Child profile key not found";
        android.util.Slog.i(r0, r1);
        goto L_0x000e;
    L_0x001e:
        if (r11 == 0) goto L_0x002e;
    L_0x0020:
        r0 = r8 instanceof android.security.keystore.UserNotAuthenticatedException;
        if (r0 == 0) goto L_0x002e;
    L_0x0024:
        r0 = "LockSettingsService";
        r1 = "Parent keystore seems locked, ignoring";
        android.util.Slog.i(r0, r1);
        goto L_0x000e;
    L_0x002e:
        r0 = "LockSettingsService";
        r1 = "Failed to decrypt child profile key";
        android.util.Slog.e(r0, r1, r8);
        goto L_0x000e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsService.unlockChildProfile(int, boolean):void");
    }

    private void unlockUser(int userId, byte[] token, byte[] secret) {
        final CountDownLatch latch = new CountDownLatch(1);
        try {
            this.mActivityManager.unlockUser(userId, token, secret, new IProgressListener.Stub() {
                public void onStarted(int id, Bundle extras) throws RemoteException {
                    Log.d(LockSettingsService.TAG, "unlockUser started");
                }

                public void onProgress(int id, int progress, Bundle extras) throws RemoteException {
                    Log.d(LockSettingsService.TAG, "unlockUser progress " + progress);
                }

                public void onFinished(int id, Bundle extras) throws RemoteException {
                    Log.d(LockSettingsService.TAG, "unlockUser finished");
                    latch.countDown();
                }
            });
            try {
                latch.await(15, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            try {
                if (!this.mUserManager.getUserInfo(userId).isManagedProfile()) {
                    for (UserInfo pi : this.mUserManager.getProfiles(userId)) {
                        if (tiedManagedProfileReadyToUnlock(pi)) {
                            unlockChildProfile(pi.id, false);
                        }
                    }
                }
            } catch (RemoteException e2) {
                Log.d(TAG, "Failed to unlock child profile", e2);
            }
        } catch (RemoteException e22) {
            throw e22.rethrowAsRuntimeException();
        }
    }

    private boolean tiedManagedProfileReadyToUnlock(UserInfo userInfo) {
        if (userInfo.isManagedProfile() && (this.mLockPatternUtils.isSeparateProfileChallengeEnabled(userInfo.id) ^ 1) != 0 && this.mStorage.hasChildProfileLock(userInfo.id)) {
            return this.mUserManager.isUserRunning(userInfo.id);
        }
        return false;
    }

    private Map<Integer, String> getDecryptedPasswordsForAllTiedProfiles(int userId) {
        if (this.mUserManager.getUserInfo(userId).isManagedProfile()) {
            return null;
        }
        Map<Integer, String> result = new ArrayMap();
        List<UserInfo> profiles = this.mUserManager.getProfiles(userId);
        int size = profiles.size();
        for (int i = 0; i < size; i++) {
            UserInfo profile = (UserInfo) profiles.get(i);
            if (profile.isManagedProfile()) {
                int managedUserId = profile.id;
                if (!this.mLockPatternUtils.isSeparateProfileChallengeEnabled(managedUserId)) {
                    try {
                        result.put(Integer.valueOf(managedUserId), getDecryptedPasswordForTiedProfile(managedUserId));
                    } catch (Exception e) {
                        Slog.e(TAG, "getDecryptedPasswordsForAllTiedProfiles failed for user " + managedUserId, e);
                    }
                }
            }
        }
        return result;
    }

    private void synchronizeUnifiedWorkChallengeForProfiles(int userId, Map<Integer, String> profilePasswordMap) throws RemoteException {
        if (!this.mUserManager.getUserInfo(userId).isManagedProfile()) {
            boolean isSecure = isUserSecure(userId);
            List<UserInfo> profiles = this.mUserManager.getProfiles(userId);
            int size = profiles.size();
            for (int i = 0; i < size; i++) {
                UserInfo profile = (UserInfo) profiles.get(i);
                if (profile.isManagedProfile()) {
                    int managedUserId = profile.id;
                    if (!this.mLockPatternUtils.isSeparateProfileChallengeEnabled(managedUserId)) {
                        if (isSecure) {
                            tieManagedProfileLockIfNecessary(managedUserId, null);
                        } else {
                            if (profilePasswordMap == null || !profilePasswordMap.containsKey(Integer.valueOf(managedUserId))) {
                                Slog.wtf(TAG, "clear tied profile challenges, but no password supplied.");
                                setLockCredentialInternal(null, -1, null, 0, managedUserId);
                            } else {
                                setLockCredentialInternal(null, -1, (String) profilePasswordMap.get(Integer.valueOf(managedUserId)), 0, managedUserId);
                            }
                            this.mStorage.removeChildProfileLock(managedUserId);
                            removeKeystoreProfileKey(managedUserId);
                        }
                    }
                }
            }
        }
    }

    private boolean isManagedProfileWithUnifiedLock(int userId) {
        if (this.mUserManager.getUserInfo(userId).isManagedProfile()) {
            return this.mLockPatternUtils.isSeparateProfileChallengeEnabled(userId) ^ 1;
        }
        return false;
    }

    private boolean isManagedProfileWithSeparatedLock(int userId) {
        if (this.mUserManager.getUserInfo(userId).isManagedProfile()) {
            return this.mLockPatternUtils.isSeparateProfileChallengeEnabled(userId);
        }
        return false;
    }

    public void setLockCredential(String credential, int type, String savedCredential, int requestedQuality, int userId) throws RemoteException {
        checkWritePermission(userId);
        synchronized (this.mSeparateChallengeLock) {
            setLockCredentialInternal(credential, type, savedCredential, requestedQuality, userId);
            setSeparateProfileChallengeEnabled(userId, true, null);
            notifyPasswordChanged(userId);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setLockCredentialInternal(java.lang.String r19, int r20, java.lang.String r21, int r22, int r23) throws android.os.RemoteException {
        /*
        r18 = this;
        r4 = android.text.TextUtils.isEmpty(r21);
        if (r4 == 0) goto L_0x0008;
    L_0x0006:
        r21 = 0;
    L_0x0008:
        r4 = android.text.TextUtils.isEmpty(r19);
        if (r4 == 0) goto L_0x0010;
    L_0x000e:
        r19 = 0;
    L_0x0010:
        r0 = r18;
        r5 = r0.mSpManager;
        monitor-enter(r5);
        r0 = r18;
        r1 = r23;
        r4 = r0.isSyntheticPasswordBasedCredentialLocked(r1);	 Catch:{ all -> 0x0072 }
        if (r4 == 0) goto L_0x0024;
    L_0x001f:
        r18.spBasedSetLockCredentialInternalLocked(r19, r20, r21, r22, r23);	 Catch:{ all -> 0x0072 }
        monitor-exit(r5);
        return;
    L_0x0024:
        monitor-exit(r5);
        r4 = -1;
        r0 = r20;
        if (r0 != r4) goto L_0x0075;
    L_0x002a:
        if (r19 == 0) goto L_0x0035;
    L_0x002c:
        r4 = "LockSettingsService";
        r5 = "CredentialType is none, but credential is non-null.";
        android.util.Slog.wtf(r4, r5);
    L_0x0035:
        r0 = r18;
        r1 = r23;
        r0.clearUserKeyProtection(r1);
        r4 = r18.getGateKeeperService();
        r0 = r23;
        r4.clearSecureUserId(r0);
        r0 = r18;
        r4 = r0.mStorage;
        r5 = com.android.server.locksettings.LockSettingsStorage.CredentialHash.createEmptyHash();
        r0 = r23;
        r4.writeCredentialHash(r5, r0);
        r4 = 0;
        r0 = r18;
        r1 = r23;
        r0.setKeystorePassword(r4, r1);
        r0 = r18;
        r1 = r23;
        r0.fixateNewestUserKeyAuth(r1);
        r4 = 0;
        r0 = r18;
        r1 = r23;
        r0.synchronizeUnifiedWorkChallengeForProfiles(r1, r4);
        r4 = 0;
        r0 = r18;
        r1 = r23;
        r0.notifyActivePasswordMetricsAvailable(r4, r1);
        return;
    L_0x0072:
        r4 = move-exception;
        monitor-exit(r5);
        throw r4;
    L_0x0075:
        if (r19 != 0) goto L_0x0080;
    L_0x0077:
        r4 = new android.os.RemoteException;
        r5 = "Null credential with mismatched credential type";
        r4.<init>(r5);
        throw r4;
    L_0x0080:
        r0 = r18;
        r4 = r0.mStorage;
        r0 = r23;
        r12 = r4.readCredentialHash(r0);
        r0 = r18;
        r1 = r23;
        r4 = r0.isManagedProfileWithUnifiedLock(r1);
        if (r4 == 0) goto L_0x00d7;
    L_0x0094:
        if (r21 != 0) goto L_0x009e;
    L_0x0096:
        r0 = r18;
        r1 = r23;
        r21 = r0.getDecryptedPasswordForTiedProfile(r1);	 Catch:{ FileNotFoundException -> 0x00cc, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1, UnrecoverableKeyException -> 0x00c1 }
    L_0x009e:
        r0 = r18;
        r10 = r0.mSpManager;
        monitor-enter(r10);
        r0 = r18;
        r1 = r23;
        r4 = r0.shouldMigrateToSyntheticPasswordLocked(r1);	 Catch:{ all -> 0x014f }
        if (r4 == 0) goto L_0x00e9;
    L_0x00ad:
        r5 = r12.hash;	 Catch:{ all -> 0x014f }
        r7 = r12.type;	 Catch:{ all -> 0x014f }
        r4 = r18;
        r6 = r21;
        r8 = r22;
        r9 = r23;
        r4.initializeSyntheticPasswordLocked(r5, r6, r7, r8, r9);	 Catch:{ all -> 0x014f }
        r18.spBasedSetLockCredentialInternalLocked(r19, r20, r21, r22, r23);	 Catch:{ all -> 0x014f }
        monitor-exit(r10);
        return;
    L_0x00c1:
        r14 = move-exception;
        r4 = "LockSettingsService";
        r5 = "Failed to decrypt child profile key";
        android.util.Slog.e(r4, r5, r14);
        goto L_0x009e;
    L_0x00cc:
        r13 = move-exception;
        r4 = "LockSettingsService";
        r5 = "Child profile key not found";
        android.util.Slog.i(r4, r5);
        goto L_0x009e;
    L_0x00d7:
        r4 = r12.hash;
        if (r4 != 0) goto L_0x009e;
    L_0x00db:
        if (r21 == 0) goto L_0x00e6;
    L_0x00dd:
        r4 = "LockSettingsService";
        r5 = "Saved credential provided, but none stored";
        android.util.Slog.w(r4, r5);
    L_0x00e6:
        r21 = 0;
        goto L_0x009e;
    L_0x00e9:
        monitor-exit(r10);
        r4 = r12.hash;
        r0 = r18;
        r1 = r21;
        r2 = r19;
        r3 = r23;
        r15 = r0.enrollCredential(r4, r1, r2, r3);
        if (r15 == 0) goto L_0x0152;
    L_0x00fa:
        r0 = r20;
        r17 = com.android.server.locksettings.LockSettingsStorage.CredentialHash.create(r15, r0);
        r0 = r18;
        r4 = r0.mStorage;
        r0 = r17;
        r1 = r23;
        r4.writeCredentialHash(r0, r1);
        r4 = r18.getGateKeeperService();
        r6 = 0;
        r0 = r17;
        r8 = r0.hash;
        r9 = r19.getBytes();
        r5 = r23;
        r16 = r4.verifyChallenge(r5, r6, r8, r9);
        r0 = r18;
        r1 = r16;
        r4 = r0.convertResponse(r1);
        r0 = r18;
        r1 = r23;
        r2 = r19;
        r0.setUserKeyProtection(r1, r2, r4);
        r0 = r18;
        r1 = r23;
        r0.fixateNewestUserKeyAuth(r1);
        r8 = 0;
        r7 = 1;
        r11 = 0;
        r4 = r18;
        r5 = r19;
        r6 = r20;
        r10 = r23;
        r4.doVerifyCredential(r5, r6, r7, r8, r10, r11);
        r4 = 0;
        r0 = r18;
        r1 = r23;
        r0.synchronizeUnifiedWorkChallengeForProfiles(r1, r4);
        return;
    L_0x014f:
        r4 = move-exception;
        monitor-exit(r10);
        throw r4;
    L_0x0152:
        r5 = new android.os.RemoteException;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r6 = "Failed to enroll ";
        r6 = r4.append(r6);
        r4 = 2;
        r0 = r20;
        if (r0 != r4) goto L_0x0174;
    L_0x0165:
        r4 = "password";
    L_0x0168:
        r4 = r6.append(r4);
        r4 = r4.toString();
        r5.<init>(r4);
        throw r5;
    L_0x0174:
        r4 = "pattern";
        goto L_0x0168;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsService.setLockCredentialInternal(java.lang.String, int, java.lang.String, int, int):void");
    }

    private VerifyCredentialResponse convertResponse(GateKeeperResponse gateKeeperResponse) {
        return VerifyCredentialResponse.fromGateKeeperResponse(gateKeeperResponse);
    }

    protected void tieProfileLockToParent(int userId, String password) {
        byte[] randomLockSeed = password.getBytes(StandardCharsets.UTF_8);
        java.security.KeyStore keyStore;
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(new SecureRandom());
            SecretKey secretKey = keyGenerator.generateKey();
            keyStore = java.security.KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            keyStore.setEntry("profile_key_name_encrypt_" + userId, new SecretKeyEntry(secretKey), new KeyProtection.Builder(1).setBlockModes(new String[]{"GCM"}).setEncryptionPaddings(new String[]{"NoPadding"}).build());
            keyStore.setEntry("profile_key_name_decrypt_" + userId, new SecretKeyEntry(secretKey), new KeyProtection.Builder(2).setBlockModes(new String[]{"GCM"}).setEncryptionPaddings(new String[]{"NoPadding"}).setUserAuthenticationRequired(true).setUserAuthenticationValidityDurationSeconds(30).setCriticalToDeviceEncryption(true).build());
            SecretKey keyStoreEncryptionKey = (SecretKey) keyStore.getKey("profile_key_name_encrypt_" + userId, null);
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            cipher.init(1, keyStoreEncryptionKey);
            byte[] encryptionResult = cipher.doFinal(randomLockSeed);
            byte[] iv = cipher.getIV();
            keyStore.deleteEntry("profile_key_name_encrypt_" + userId);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                if (iv.length != 12) {
                    throw new RuntimeException("Invalid iv length: " + iv.length);
                }
                outputStream.write(iv);
                outputStream.write(encryptionResult);
                this.mStorage.writeChildProfileLock(userId, outputStream.toByteArray());
            } catch (IOException e) {
                throw new RuntimeException("Failed to concatenate byte arrays", e);
            }
        } catch (Exception e2) {
            throw new RuntimeException("Failed to encrypt key", e2);
        } catch (Throwable th) {
            keyStore.deleteEntry("profile_key_name_encrypt_" + userId);
        }
    }

    private byte[] enrollCredential(byte[] enrolledHandle, String enrolledCredential, String toEnroll, int userId) throws RemoteException {
        byte[] bArr;
        byte[] bArr2;
        checkWritePermission(userId);
        if (enrolledCredential == null) {
            bArr = null;
        } else {
            bArr = enrolledCredential.getBytes();
        }
        if (toEnroll == null) {
            bArr2 = null;
        } else {
            bArr2 = toEnroll.getBytes();
        }
        GateKeeperResponse response = getGateKeeperService().enroll(userId, enrolledHandle, bArr, bArr2);
        if (response == null) {
            return null;
        }
        byte[] hash = response.getPayload();
        if (hash != null) {
            setKeystorePassword(toEnroll, userId);
        } else {
            Slog.e(TAG, "Throttled while enrolling a password");
        }
        return hash;
    }

    private void setAuthlessUserKeyProtection(int userId, byte[] key) throws RemoteException {
        addUserKeyAuth(userId, null, key);
    }

    private void setUserKeyProtection(int userId, String credential, VerifyCredentialResponse vcr) throws RemoteException {
        if (vcr == null) {
            throw new RemoteException("Null response verifying a credential we just set");
        } else if (vcr.getResponseCode() != 0) {
            throw new RemoteException("Non-OK response verifying a credential we just set: " + vcr.getResponseCode());
        } else {
            byte[] token = vcr.getPayload();
            if (token == null) {
                throw new RemoteException("Empty payload verifying a credential we just set");
            }
            addUserKeyAuth(userId, token, secretFromCredential(credential));
        }
    }

    private void clearUserKeyProtection(int userId) throws RemoteException {
        addUserKeyAuth(userId, null, null);
    }

    private static byte[] secretFromCredential(String credential) throws RemoteException {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            digest.update(Arrays.copyOf("Android FBE credential hash".getBytes(StandardCharsets.UTF_8), 128));
            digest.update(credential.getBytes(StandardCharsets.UTF_8));
            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("NoSuchAlgorithmException for SHA-512");
        }
    }

    private void addUserKeyAuth(int userId, byte[] token, byte[] secret) throws RemoteException {
        UserInfo userInfo = this.mUserManager.getUserInfo(userId);
        IStorageManager storageManager = this.mInjector.getStorageManager();
        long callingId = Binder.clearCallingIdentity();
        try {
            storageManager.addUserKeyAuth(userId, userInfo.serialNumber, token, secret);
        } finally {
            Binder.restoreCallingIdentity(callingId);
        }
    }

    private void fixateNewestUserKeyAuth(int userId) throws RemoteException {
        IStorageManager storageManager = this.mInjector.getStorageManager();
        long callingId = Binder.clearCallingIdentity();
        try {
            storageManager.fixateNewestUserKeyAuth(userId);
        } finally {
            Binder.restoreCallingIdentity(callingId);
        }
    }

    public void resetKeyStore(int userId) throws RemoteException {
        checkWritePermission(userId);
        int managedUserId = -1;
        String managedUserDecryptedPassword = null;
        for (UserInfo pi : this.mUserManager.getProfiles(userId)) {
            if (pi.isManagedProfile() && (this.mLockPatternUtils.isSeparateProfileChallengeEnabled(pi.id) ^ 1) != 0 && this.mStorage.hasChildProfileLock(pi.id)) {
                if (managedUserId == -1) {
                    try {
                        managedUserDecryptedPassword = getDecryptedPasswordForTiedProfile(pi.id);
                        managedUserId = pi.id;
                    } catch (Exception e) {
                        Slog.e(TAG, "Failed to decrypt child profile key", e);
                    }
                } else {
                    Slog.e(TAG, "More than one managed profile, uid1:" + managedUserId + ", uid2:" + pi.id);
                }
            }
        }
        try {
            for (int profileId : this.mUserManager.getProfileIdsWithDisabled(userId)) {
                for (int uid : SYSTEM_CREDENTIAL_UIDS) {
                    this.mKeyStore.clearUid(UserHandle.getUid(profileId, uid));
                }
            }
        } finally {
            if (!(managedUserId == -1 || managedUserDecryptedPassword == null)) {
                tieProfileLockToParent(managedUserId, managedUserDecryptedPassword);
            }
        }
    }

    public VerifyCredentialResponse checkCredential(String credential, int type, int userId, ICheckCredentialProgressCallback progressCallback) throws RemoteException {
        checkPasswordReadPermission(userId);
        return doVerifyCredential(credential, type, false, 0, userId, progressCallback);
    }

    public VerifyCredentialResponse verifyCredential(String credential, int type, long challenge, int userId) throws RemoteException {
        checkPasswordReadPermission(userId);
        return doVerifyCredential(credential, type, true, challenge, userId, null);
    }

    private VerifyCredentialResponse doVerifyCredential(String credential, int credentialType, boolean hasChallenge, long challenge, int userId, ICheckCredentialProgressCallback progressCallback) throws RemoteException {
        if (TextUtils.isEmpty(credential)) {
            throw new IllegalArgumentException("Credential can't be null or empty");
        } else if (userId != -9999 || Global.getInt(this.mContext.getContentResolver(), "device_provisioned", 0) == 0) {
            VerifyCredentialResponse response = spBasedDoVerifyCredential(credential, credentialType, hasChallenge, challenge, userId, progressCallback);
            if (response != null) {
                return response;
            }
            if (userId == -9999) {
                Slog.wtf(TAG, "Unexpected FRP credential type, should be SP based.");
                return VerifyCredentialResponse.ERROR;
            }
            CredentialHash storedHash = this.mStorage.readCredentialHash(userId);
            if (storedHash.type != credentialType) {
                Slog.wtf(TAG, "doVerifyCredential type mismatch with stored credential?? stored: " + storedHash.type + " passed in: " + credentialType);
                return VerifyCredentialResponse.ERROR;
            }
            boolean z;
            String credentialToVerify;
            if (storedHash.type == 1) {
                z = storedHash.isBaseZeroPattern;
            } else {
                z = false;
            }
            if (z) {
                credentialToVerify = LockPatternUtils.patternStringToBaseZero(credential);
            } else {
                credentialToVerify = credential;
            }
            response = verifyCredential(userId, storedHash, credentialToVerify, hasChallenge, challenge, progressCallback);
            if (response.getResponseCode() == 0) {
                this.mStrongAuth.reportSuccessfulStrongAuthUnlock(userId);
                if (z) {
                    setLockCredentialInternal(credential, storedHash.type, credentialToVerify, 65536, userId);
                }
            }
            return response;
        } else {
            Slog.e(TAG, "FRP credential can only be verified prior to provisioning.");
            return VerifyCredentialResponse.ERROR;
        }
    }

    public VerifyCredentialResponse verifyTiedProfileChallenge(String credential, int type, long challenge, int userId) throws RemoteException {
        checkPasswordReadPermission(userId);
        if (isManagedProfileWithUnifiedLock(userId)) {
            VerifyCredentialResponse parentResponse = doVerifyCredential(credential, type, true, challenge, this.mUserManager.getProfileParent(userId).id, null);
            if (parentResponse.getResponseCode() != 0) {
                return parentResponse;
            }
            try {
                return doVerifyCredential(getDecryptedPasswordForTiedProfile(userId), 2, true, challenge, userId, null);
            } catch (Throwable e) {
                Slog.e(TAG, "Failed to decrypt child profile key", e);
                throw new RemoteException("Unable to get tied profile token");
            }
        }
        throw new RemoteException("User id must be managed profile with unified lock");
    }

    private VerifyCredentialResponse verifyCredential(int userId, CredentialHash storedHash, String credential, boolean hasChallenge, long challenge, ICheckCredentialProgressCallback progressCallback) throws RemoteException {
        if ((storedHash == null || storedHash.hash.length == 0) && TextUtils.isEmpty(credential)) {
            return VerifyCredentialResponse.OK;
        }
        if (storedHash == null || TextUtils.isEmpty(credential)) {
            return VerifyCredentialResponse.ERROR;
        }
        int i;
        StrictMode.noteDiskRead();
        if (storedHash.version == 0) {
            byte[] hash;
            if (storedHash.type == 1) {
                hash = LockPatternUtils.patternToHash(LockPatternUtils.stringToPattern(credential));
            } else {
                hash = this.mLockPatternUtils.passwordToHash(credential, userId);
            }
            if (!Arrays.equals(hash, storedHash.hash)) {
                return VerifyCredentialResponse.ERROR;
            }
            if (storedHash.type == 1) {
                unlockKeystore(LockPatternUtils.patternStringToBaseZero(credential), userId);
            } else {
                unlockKeystore(credential, userId);
            }
            Slog.i(TAG, "Unlocking user with fake token: " + userId);
            byte[] fakeToken = String.valueOf(userId).getBytes();
            unlockUser(userId, fakeToken, fakeToken);
            int i2 = storedHash.type;
            if (storedHash.type == 1) {
                i = 65536;
            } else {
                i = 327680;
            }
            setLockCredentialInternal(credential, i2, null, i, userId);
            if (!hasChallenge) {
                notifyActivePasswordMetricsAvailable(credential, userId);
                return VerifyCredentialResponse.OK;
            }
        }
        GateKeeperResponse gateKeeperResponse = getGateKeeperService().verifyChallenge(userId, challenge, storedHash.hash, credential.getBytes());
        VerifyCredentialResponse response = convertResponse(gateKeeperResponse);
        boolean shouldReEnroll = gateKeeperResponse.getShouldReEnroll();
        if (response.getResponseCode() == 0) {
            if (progressCallback != null) {
                progressCallback.onCredentialVerified();
            }
            notifyActivePasswordMetricsAvailable(credential, userId);
            unlockKeystore(credential, userId);
            Slog.i(TAG, "Unlocking user " + userId + " with token length " + response.getPayload().length);
            unlockUser(userId, response.getPayload(), secretFromCredential(credential));
            if (isManagedProfileWithSeparatedLock(userId)) {
                ((TrustManager) this.mContext.getSystemService("trust")).setDeviceLockedForUser(userId, false);
            }
            if (storedHash.type == 1) {
                i = 65536;
            } else {
                i = 327680;
            }
            if (shouldReEnroll) {
                setLockCredentialInternal(credential, storedHash.type, credential, i, userId);
            } else {
                synchronized (this.mSpManager) {
                    if (shouldMigrateToSyntheticPasswordLocked(userId)) {
                        activateEscrowTokens(initializeSyntheticPasswordLocked(storedHash.hash, credential, storedHash.type, i, userId), userId);
                    }
                }
            }
        } else if (response.getResponseCode() == 1 && response.getTimeout() > 0) {
            requireStrongAuth(8, userId);
        }
        return response;
    }

    private void notifyActivePasswordMetricsAvailable(String password, int userId) {
        PasswordMetrics metrics;
        if (password == null) {
            metrics = new PasswordMetrics();
        } else {
            metrics = PasswordMetrics.computeForPassword(password);
            metrics.quality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(userId);
        }
        this.mHandler.post(new com.android.server.locksettings.-$Lambda$uuAdbltCNvfImff6TxhVt9IC9Qw.AnonymousClass1(userId, this, metrics));
    }

    /* synthetic */ void lambda$-com_android_server_locksettings_LockSettingsService_80863(PasswordMetrics metrics, int userId) {
        ((DevicePolicyManager) this.mContext.getSystemService("device_policy")).setActivePasswordState(metrics, userId);
    }

    private void notifyPasswordChanged(int userId) {
        this.mHandler.post(new -$Lambda$uuAdbltCNvfImff6TxhVt9IC9Qw(userId, this));
    }

    /* synthetic */ void lambda$-com_android_server_locksettings_LockSettingsService_81411(int userId) {
        ((DevicePolicyManager) this.mContext.getSystemService("device_policy")).reportPasswordChanged(userId);
    }

    public boolean checkVoldPassword(int userId) throws RemoteException {
        if (!this.mFirstCallToVold) {
            return false;
        }
        this.mFirstCallToVold = false;
        checkPasswordReadPermission(userId);
        IStorageManager service = this.mInjector.getStorageManager();
        long identity = Binder.clearCallingIdentity();
        try {
            String password = service.getPassword();
            service.clearPassword();
            if (password == null) {
                return false;
            }
            try {
                if (this.mLockPatternUtils.isLockPatternEnabled(userId) && checkCredential(password, 1, userId, null).getResponseCode() == 0) {
                    return true;
                }
            } catch (Exception e) {
            }
            try {
                return this.mLockPatternUtils.isLockPasswordEnabled(userId) && checkCredential(password, 2, userId, null).getResponseCode() == 0;
            } catch (Exception e2) {
            }
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    private void removeUser(int userId, boolean unknownUser) {
        this.mSpManager.removeUser(userId);
        this.mStorage.removeUser(userId);
        this.mStrongAuth.removeUser(userId);
        KeyStore.getInstance().onUserRemoved(userId);
        try {
            IGateKeeperService gk = getGateKeeperService();
            if (gk != null) {
                gk.clearSecureUserId(userId);
            }
        } catch (RemoteException e) {
            Slog.w(TAG, "unable to clear GK secure user id");
        }
        if (unknownUser || this.mUserManager.getUserInfo(userId).isManagedProfile()) {
            removeKeystoreProfileKey(userId);
        }
    }

    private void removeKeystoreProfileKey(int targetUserId) {
        try {
            java.security.KeyStore keyStore = java.security.KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            keyStore.deleteEntry("profile_key_name_encrypt_" + targetUserId);
            keyStore.deleteEntry("profile_key_name_decrypt_" + targetUserId);
        } catch (Exception e) {
            Slog.e(TAG, "Unable to remove keystore profile key for user:" + targetUserId, e);
        }
    }

    public void registerStrongAuthTracker(IStrongAuthTracker tracker) {
        checkPasswordReadPermission(-1);
        this.mStrongAuth.registerStrongAuthTracker(tracker);
    }

    public void unregisterStrongAuthTracker(IStrongAuthTracker tracker) {
        checkPasswordReadPermission(-1);
        this.mStrongAuth.unregisterStrongAuthTracker(tracker);
    }

    public void requireStrongAuth(int strongAuthReason, int userId) {
        checkWritePermission(userId);
        this.mStrongAuth.requireStrongAuth(strongAuthReason, userId);
    }

    public void userPresent(int userId) {
        checkWritePermission(userId);
        this.mStrongAuth.reportUnlock(userId);
    }

    public int getStrongAuthForUser(int userId) {
        checkPasswordReadPermission(userId);
        return this.mStrongAuthTracker.getStrongAuthForUser(userId);
    }

    private boolean isCallerShell() {
        int callingUid = Binder.getCallingUid();
        if (callingUid == 2000 || callingUid == 0) {
            return true;
        }
        return false;
    }

    private void enforceShell() {
        if (!isCallerShell()) {
            throw new SecurityException("Caller must be shell");
        }
    }

    public void onShellCommand(FileDescriptor in, FileDescriptor out, FileDescriptor err, String[] args, ShellCallback callback, ResultReceiver resultReceiver) throws RemoteException {
        enforceShell();
        long origId = Binder.clearCallingIdentity();
        try {
            new LockSettingsShellCommand(this.mContext, new LockPatternUtils(this.mContext)).exec(this, in, out, err, args, callback, resultReceiver);
        } finally {
            Binder.restoreCallingIdentity(origId);
        }
    }

    protected synchronized IGateKeeperService getGateKeeperService() throws RemoteException {
        if (this.mGateKeeperService != null) {
            return this.mGateKeeperService;
        }
        IBinder service = ServiceManager.getService("android.service.gatekeeper.IGateKeeperService");
        if (service != null) {
            service.linkToDeath(new GateKeeperDiedRecipient(), 0);
            this.mGateKeeperService = IGateKeeperService.Stub.asInterface(service);
            return this.mGateKeeperService;
        }
        Slog.e(TAG, "Unable to acquire GateKeeperService");
        return null;
    }

    protected AuthenticationToken initializeSyntheticPasswordLocked(byte[] credentialHash, String credential, int credentialType, int requestedQuality, int userId) throws RemoteException {
        Slog.i(TAG, "Initialize SyntheticPassword for user: " + userId);
        AuthenticationToken auth = this.mSpManager.newSyntheticPasswordAndSid(getGateKeeperService(), credentialHash, credential, userId);
        if (auth == null) {
            Slog.wtf(TAG, "initializeSyntheticPasswordLocked returns null auth token");
            return null;
        }
        long handle = this.mSpManager.createPasswordBasedSyntheticPassword(getGateKeeperService(), credential, credentialType, auth, requestedQuality, userId);
        if (credential != null) {
            if (credentialHash == null) {
                this.mSpManager.newSidForUser(getGateKeeperService(), auth, userId);
            }
            this.mSpManager.verifyChallenge(getGateKeeperService(), auth, 0, userId);
            setAuthlessUserKeyProtection(userId, auth.deriveDiskEncryptionKey());
            setKeystorePassword(auth.deriveKeyStorePassword(), userId);
        } else {
            clearUserKeyProtection(userId);
            setKeystorePassword(null, userId);
            getGateKeeperService().clearSecureUserId(userId);
        }
        fixateNewestUserKeyAuth(userId);
        setLong("sp-handle", handle, userId);
        return auth;
    }

    private long getSyntheticPasswordHandleLocked(int userId) {
        return getLong("sp-handle", 0, userId);
    }

    private boolean isSyntheticPasswordBasedCredentialLocked(int userId) throws RemoteException {
        boolean z = true;
        boolean z2 = false;
        if (userId == -9999) {
            int type = this.mStorage.readPersistentDataBlock().type;
            if (!(type == 1 || type == 2)) {
                z = false;
            }
            return z;
        }
        long handle = getSyntheticPasswordHandleLocked(userId);
        if (!(getLong("enable-sp", 1, 0) == 0 || handle == 0)) {
            z2 = true;
        }
        return z2;
    }

    protected boolean shouldMigrateToSyntheticPasswordLocked(int userId) throws RemoteException {
        long handle = getSyntheticPasswordHandleLocked(userId);
        if (getLong("enable-sp", 1, 0) == 0 || handle != 0) {
            return false;
        }
        return true;
    }

    private void enableSyntheticPasswordLocked() throws RemoteException {
        setLong("enable-sp", 1, 0);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.android.internal.widget.VerifyCredentialResponse spBasedDoVerifyCredential(java.lang.String r21, int r22, boolean r23, long r24, int r26, com.android.internal.widget.ICheckCredentialProgressCallback r27) throws android.os.RemoteException {
        /*
        r20 = this;
        r4 = -1;
        r0 = r22;
        if (r0 != r4) goto L_0x0007;
    L_0x0005:
        r21 = 0;
    L_0x0007:
        r0 = r20;
        r0 = r0.mSpManager;
        r19 = r0;
        monitor-enter(r19);
        r0 = r20;
        r1 = r26;
        r4 = r0.isSyntheticPasswordBasedCredentialLocked(r1);	 Catch:{ all -> 0x011d }
        if (r4 != 0) goto L_0x001b;
    L_0x0018:
        r4 = 0;
        monitor-exit(r19);
        return r4;
    L_0x001b:
        r4 = -9999; // 0xffffffffffffd8f1 float:NaN double:NaN;
        r0 = r26;
        if (r0 != r4) goto L_0x0035;
    L_0x0021:
        r0 = r20;
        r4 = r0.mSpManager;	 Catch:{ all -> 0x011d }
        r5 = r20.getGateKeeperService();	 Catch:{ all -> 0x011d }
        r0 = r21;
        r1 = r22;
        r2 = r27;
        r4 = r4.verifyFrpCredential(r5, r0, r1, r2);	 Catch:{ all -> 0x011d }
        monitor-exit(r19);
        return r4;
    L_0x0035:
        r0 = r20;
        r1 = r26;
        r6 = r0.getSyntheticPasswordHandleLocked(r1);	 Catch:{ all -> 0x011d }
        r0 = r20;
        r4 = r0.mSpManager;	 Catch:{ all -> 0x011d }
        r5 = r20.getGateKeeperService();	 Catch:{ all -> 0x011d }
        r8 = r21;
        r9 = r26;
        r15 = r4.unwrapPasswordBasedSyntheticPassword(r5, r6, r8, r9);	 Catch:{ all -> 0x011d }
        r4 = r15.credentialType;	 Catch:{ all -> 0x011d }
        r0 = r22;
        if (r4 == r0) goto L_0x0060;
    L_0x0053:
        r4 = "LockSettingsService";
        r5 = "Credential type mismatch.";
        android.util.Slog.e(r4, r5);	 Catch:{ all -> 0x011d }
        r4 = com.android.internal.widget.VerifyCredentialResponse.ERROR;	 Catch:{ all -> 0x011d }
        monitor-exit(r19);
        return r4;
    L_0x0060:
        r0 = r15.gkResponse;	 Catch:{ all -> 0x011d }
        r16 = r0;
        r4 = r16.getResponseCode();	 Catch:{ all -> 0x011d }
        if (r4 != 0) goto L_0x008f;
    L_0x006a:
        r0 = r20;
        r9 = r0.mSpManager;	 Catch:{ all -> 0x011d }
        r10 = r20.getGateKeeperService();	 Catch:{ all -> 0x011d }
        r11 = r15.authToken;	 Catch:{ all -> 0x011d }
        r12 = r24;
        r14 = r26;
        r16 = r9.verifyChallenge(r10, r11, r12, r14);	 Catch:{ all -> 0x011d }
        r4 = r16.getResponseCode();	 Catch:{ all -> 0x011d }
        if (r4 == 0) goto L_0x008f;
    L_0x0082:
        r4 = "LockSettingsService";
        r5 = "verifyChallenge with SP failed.";
        android.util.Slog.wtf(r4, r5);	 Catch:{ all -> 0x011d }
        r4 = com.android.internal.widget.VerifyCredentialResponse.ERROR;	 Catch:{ all -> 0x011d }
        monitor-exit(r19);
        return r4;
    L_0x008f:
        monitor-exit(r19);
        r4 = r16.getResponseCode();
        if (r4 != 0) goto L_0x0120;
    L_0x0096:
        if (r27 == 0) goto L_0x009b;
    L_0x0098:
        r27.onCredentialVerified();
    L_0x009b:
        r0 = r20;
        r1 = r21;
        r2 = r26;
        r0.notifyActivePasswordMetricsAvailable(r1, r2);
        r4 = r15.authToken;
        r4 = r4.deriveKeyStorePassword();
        r0 = r20;
        r1 = r26;
        r0.unlockKeystore(r4, r1);
        r4 = r15.authToken;
        r17 = r4.deriveDiskEncryptionKey();
        r4 = "LockSettingsService";
        r5 = new java.lang.StringBuilder;
        r5.<init>();
        r8 = "Unlocking user ";
        r5 = r5.append(r8);
        r0 = r26;
        r5 = r5.append(r0);
        r8 = " with secret only, length ";
        r5 = r5.append(r8);
        r0 = r17;
        r8 = r0.length;
        r5 = r5.append(r8);
        r5 = r5.toString();
        android.util.Slog.i(r4, r5);
        r4 = 0;
        r0 = r20;
        r1 = r26;
        r2 = r17;
        r0.unlockUser(r1, r4, r2);
        r4 = r15.authToken;
        r0 = r20;
        r1 = r26;
        r0.activateEscrowTokens(r4, r1);
        r0 = r20;
        r1 = r26;
        r4 = r0.isManagedProfileWithSeparatedLock(r1);
        if (r4 == 0) goto L_0x0113;
    L_0x00fe:
        r0 = r20;
        r4 = r0.mContext;
        r5 = "trust";
        r18 = r4.getSystemService(r5);
        r18 = (android.app.trust.TrustManager) r18;
        r4 = 0;
        r0 = r18;
        r1 = r26;
        r0.setDeviceLockedForUser(r1, r4);
    L_0x0113:
        r0 = r20;
        r4 = r0.mStrongAuth;
        r0 = r26;
        r4.reportSuccessfulStrongAuthUnlock(r0);
    L_0x011c:
        return r16;
    L_0x011d:
        r4 = move-exception;
        monitor-exit(r19);
        throw r4;
    L_0x0120:
        r4 = r16.getResponseCode();
        r5 = 1;
        if (r4 != r5) goto L_0x011c;
    L_0x0127:
        r4 = r16.getTimeout();
        if (r4 <= 0) goto L_0x011c;
    L_0x012d:
        r4 = 8;
        r0 = r20;
        r1 = r26;
        r0.requireStrongAuth(r4, r1);
        goto L_0x011c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.locksettings.LockSettingsService.spBasedDoVerifyCredential(java.lang.String, int, boolean, long, int, com.android.internal.widget.ICheckCredentialProgressCallback):com.android.internal.widget.VerifyCredentialResponse");
    }

    private long setLockCredentialWithAuthTokenLocked(String credential, int credentialType, AuthenticationToken auth, int requestedQuality, int userId) throws RemoteException {
        Map profilePasswords;
        long newHandle = this.mSpManager.createPasswordBasedSyntheticPassword(getGateKeeperService(), credential, credentialType, auth, requestedQuality, userId);
        if (credential != null) {
            profilePasswords = null;
            if (this.mSpManager.hasSidForUser(userId)) {
                this.mSpManager.verifyChallenge(getGateKeeperService(), auth, 0, userId);
            } else {
                this.mSpManager.newSidForUser(getGateKeeperService(), auth, userId);
                this.mSpManager.verifyChallenge(getGateKeeperService(), auth, 0, userId);
                setAuthlessUserKeyProtection(userId, auth.deriveDiskEncryptionKey());
                fixateNewestUserKeyAuth(userId);
                setKeystorePassword(auth.deriveKeyStorePassword(), userId);
            }
        } else {
            profilePasswords = getDecryptedPasswordsForAllTiedProfiles(userId);
            this.mSpManager.clearSidForUser(userId);
            getGateKeeperService().clearSecureUserId(userId);
            clearUserKeyProtection(userId);
            fixateNewestUserKeyAuth(userId);
            setKeystorePassword(null, userId);
        }
        setLong("sp-handle", newHandle, userId);
        synchronizeUnifiedWorkChallengeForProfiles(userId, profilePasswords);
        notifyActivePasswordMetricsAvailable(credential, userId);
        return newHandle;
    }

    private void spBasedSetLockCredentialInternalLocked(String credential, int credentialType, String savedCredential, int requestedQuality, int userId) throws RemoteException {
        if (isManagedProfileWithUnifiedLock(userId)) {
            try {
                savedCredential = getDecryptedPasswordForTiedProfile(userId);
            } catch (FileNotFoundException e) {
                Slog.i(TAG, "Child profile key not found");
            } catch (Throwable e2) {
                Slog.e(TAG, "Failed to decrypt child profile key", e2);
            }
        }
        long handle = getSyntheticPasswordHandleLocked(userId);
        AuthenticationResult authResult = this.mSpManager.unwrapPasswordBasedSyntheticPassword(getGateKeeperService(), handle, savedCredential, userId);
        VerifyCredentialResponse response = authResult.gkResponse;
        AuthenticationToken auth = authResult.authToken;
        if (savedCredential == null || auth != null) {
            if (auth != null) {
                setLockCredentialWithAuthTokenLocked(credential, credentialType, auth, requestedQuality, userId);
                this.mSpManager.destroyPasswordBasedSyntheticPassword(handle, userId);
            } else if (response == null || response.getResponseCode() != -1) {
                Slog.w(TAG, "spBasedSetLockCredentialInternalLocked: " + (response != null ? "rate limit exceeded" : "failed"));
                return;
            } else {
                Slog.w(TAG, "Untrusted credential change invoked");
                initializeSyntheticPasswordLocked(null, credential, credentialType, requestedQuality, userId);
                synchronizeUnifiedWorkChallengeForProfiles(userId, null);
                this.mSpManager.destroyPasswordBasedSyntheticPassword(handle, userId);
                notifyActivePasswordMetricsAvailable(credential, userId);
            }
            return;
        }
        String str;
        StringBuilder append = new StringBuilder().append("Failed to enroll ");
        if (credentialType == 2) {
            str = "password";
        } else {
            str = "pattern";
        }
        throw new RemoteException(append.append(str).toString());
    }

    public long addEscrowToken(byte[] token, int userId) throws RemoteException {
        long handle;
        ensureCallerSystemUid();
        synchronized (this.mSpManager) {
            enableSyntheticPasswordLocked();
            AuthenticationToken auth = null;
            if (!isUserSecure(userId)) {
                if (shouldMigrateToSyntheticPasswordLocked(userId)) {
                    auth = initializeSyntheticPasswordLocked(null, null, -1, 0, userId);
                } else {
                    auth = this.mSpManager.unwrapPasswordBasedSyntheticPassword(getGateKeeperService(), getSyntheticPasswordHandleLocked(userId), null, userId).authToken;
                }
            }
            if (isSyntheticPasswordBasedCredentialLocked(userId)) {
                disableEscrowTokenOnNonManagedDevicesIfNeeded(userId);
                if (!this.mSpManager.hasEscrowData(userId)) {
                    throw new SecurityException("Escrow token is disabled on the current user");
                }
            }
            handle = this.mSpManager.createTokenBasedSyntheticPassword(token, userId);
            if (auth != null) {
                this.mSpManager.activateTokenBasedSyntheticPassword(handle, auth, userId);
            }
        }
        return handle;
    }

    private void activateEscrowTokens(AuthenticationToken auth, int userId) throws RemoteException {
        synchronized (this.mSpManager) {
            disableEscrowTokenOnNonManagedDevicesIfNeeded(userId);
            for (Long longValue : this.mSpManager.getPendingTokensForUser(userId)) {
                long handle = longValue.longValue();
                Slog.i(TAG, String.format("activateEscrowTokens: %x %d ", new Object[]{Long.valueOf(handle), Integer.valueOf(userId)}));
                this.mSpManager.activateTokenBasedSyntheticPassword(handle, auth, userId);
            }
        }
    }

    public boolean isEscrowTokenActive(long handle, int userId) throws RemoteException {
        boolean existsHandle;
        ensureCallerSystemUid();
        synchronized (this.mSpManager) {
            existsHandle = this.mSpManager.existsHandle(handle, userId);
        }
        return existsHandle;
    }

    public boolean removeEscrowToken(long handle, int userId) throws RemoteException {
        ensureCallerSystemUid();
        synchronized (this.mSpManager) {
            if (handle == getSyntheticPasswordHandleLocked(userId)) {
                Slog.w(TAG, "Cannot remove password handle");
                return false;
            } else if (this.mSpManager.removePendingToken(handle, userId)) {
                return true;
            } else if (this.mSpManager.existsHandle(handle, userId)) {
                this.mSpManager.destroyTokenBasedSyntheticPassword(handle, userId);
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean setLockCredentialWithToken(String credential, int type, long tokenHandle, byte[] token, int requestedQuality, int userId) throws RemoteException {
        boolean result;
        ensureCallerSystemUid();
        synchronized (this.mSpManager) {
            if (this.mSpManager.hasEscrowData(userId)) {
                result = setLockCredentialWithTokenInternal(credential, type, tokenHandle, token, requestedQuality, userId);
            } else {
                throw new SecurityException("Escrow token is disabled on the current user");
            }
        }
        if (result) {
            synchronized (this.mSeparateChallengeLock) {
                setSeparateProfileChallengeEnabled(userId, true, null);
            }
            notifyPasswordChanged(userId);
        }
        return result;
    }

    private boolean setLockCredentialWithTokenInternal(String credential, int type, long tokenHandle, byte[] token, int requestedQuality, int userId) throws RemoteException {
        synchronized (this.mSpManager) {
            AuthenticationResult result = this.mSpManager.unwrapTokenBasedSyntheticPassword(getGateKeeperService(), tokenHandle, token, userId);
            if (result.authToken == null) {
                Slog.w(TAG, "Invalid escrow token supplied");
                return false;
            } else if (result.gkResponse.getResponseCode() != 0) {
                Slog.e(TAG, "Obsolete token: synthetic password derived but it fails GK verification.");
                return false;
            } else {
                setLong("lockscreen.password_type", (long) requestedQuality, userId);
                long oldHandle = getSyntheticPasswordHandleLocked(userId);
                setLockCredentialWithAuthTokenLocked(credential, type, result.authToken, requestedQuality, userId);
                this.mSpManager.destroyPasswordBasedSyntheticPassword(oldHandle, userId);
                return true;
            }
        }
    }

    public void unlockUserWithToken(long tokenHandle, byte[] token, int userId) throws RemoteException {
        ensureCallerSystemUid();
        synchronized (this.mSpManager) {
            if (this.mSpManager.hasEscrowData(userId)) {
                AuthenticationResult authResult = this.mSpManager.unwrapTokenBasedSyntheticPassword(getGateKeeperService(), tokenHandle, token, userId);
                if (authResult.authToken == null) {
                    Slog.w(TAG, "Invalid escrow token supplied");
                    return;
                }
                unlockUser(userId, null, authResult.authToken.deriveDiskEncryptionKey());
                return;
            }
            throw new SecurityException("Escrow token is disabled on the current user");
        }
    }

    protected void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        if (DumpUtils.checkDumpPermission(this.mContext, TAG, pw)) {
            pw.println("Current lock settings service state:");
            pw.println(String.format("SP Enabled = %b", new Object[]{Boolean.valueOf(this.mLockPatternUtils.isSyntheticPasswordEnabled())}));
            List<UserInfo> users = this.mUserManager.getUsers();
            for (int user = 0; user < users.size(); user++) {
                pw.println("    User " + ((UserInfo) users.get(user)).id);
                synchronized (this.mSpManager) {
                    pw.println(String.format("        SP Handle = %x", new Object[]{Long.valueOf(getSyntheticPasswordHandleLocked(userId))}));
                }
                try {
                    pw.println(String.format("        SID = %x", new Object[]{Long.valueOf(getGateKeeperService().getSecureUserId(userId))}));
                } catch (RemoteException e) {
                }
            }
        }
    }

    private void disableEscrowTokenOnNonManagedDevicesIfNeeded(int userId) {
        long ident = Binder.clearCallingIdentity();
        try {
            if (this.mUserManager.getUserInfo(userId).isManagedProfile()) {
                Slog.i(TAG, "Managed profile can have escrow token");
                return;
            }
            DevicePolicyManager dpm = this.mInjector.getDevicePolicyManager();
            if (dpm.getDeviceOwnerComponentOnAnyUser() != null) {
                Slog.i(TAG, "Corp-owned device can have escrow token");
                Binder.restoreCallingIdentity(ident);
            } else if (dpm.getProfileOwnerAsUser(userId) != null) {
                Slog.i(TAG, "User with profile owner can have escrow token");
                Binder.restoreCallingIdentity(ident);
            } else if (!dpm.isDeviceProvisioned()) {
                Slog.i(TAG, "Postpone disabling escrow tokens until device is provisioned");
                Binder.restoreCallingIdentity(ident);
            } else if (this.mContext.getPackageManager().hasSystemFeature("android.hardware.type.automotive")) {
                Binder.restoreCallingIdentity(ident);
            } else {
                Slog.i(TAG, "Disabling escrow token on user " + userId);
                if (isSyntheticPasswordBasedCredentialLocked(userId)) {
                    this.mSpManager.destroyEscrowData(userId);
                }
                Binder.restoreCallingIdentity(ident);
            }
        } catch (RemoteException e) {
            Slog.e(TAG, "disableEscrowTokenOnNonManagedDevices", e);
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    private void ensureCallerSystemUid() throws SecurityException {
        if (this.mInjector.binderGetCallingUid() != 1000) {
            throw new SecurityException("Only system can call this API.");
        }
    }
}
