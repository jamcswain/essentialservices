package com.android.server.accessibility;

import android.hardware.fingerprint.IFingerprintClientActiveCallback.Stub;
import android.hardware.fingerprint.IFingerprintService;
import android.os.Binder;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.List;

public class FingerprintGestureDispatcher extends Stub implements Callback {
    private static final String LOG_TAG = "FingerprintGestureDispatcher";
    private static final int MSG_REGISTER = 1;
    private static final int MSG_UNREGISTER = 2;
    private final List<FingerprintGestureClient> mCapturingClients = new ArrayList(0);
    private final IFingerprintService mFingerprintService;
    private final Handler mHandler;
    private final Object mLock;
    private boolean mRegisteredReadOnlyExceptInHandler;

    public interface FingerprintGestureClient {
        boolean isCapturingFingerprintGestures();

        void onFingerprintGesture(int i);

        void onFingerprintGestureDetectionActiveChanged(boolean z);
    }

    public FingerprintGestureDispatcher(IFingerprintService fingerprintService, Object lock) {
        this.mFingerprintService = fingerprintService;
        this.mLock = lock;
        this.mHandler = new Handler(this);
    }

    public FingerprintGestureDispatcher(IFingerprintService fingerprintService, Object lock, Handler handler) {
        this.mFingerprintService = fingerprintService;
        this.mLock = lock;
        this.mHandler = handler;
    }

    public void updateClientList(List<? extends FingerprintGestureClient> clientList) {
        synchronized (this.mLock) {
            this.mCapturingClients.clear();
            for (int i = 0; i < clientList.size(); i++) {
                FingerprintGestureClient client = (FingerprintGestureClient) clientList.get(i);
                if (client.isCapturingFingerprintGestures()) {
                    this.mCapturingClients.add(client);
                }
            }
            if (this.mCapturingClients.isEmpty()) {
                if (this.mRegisteredReadOnlyExceptInHandler) {
                    this.mHandler.obtainMessage(2).sendToTarget();
                }
            } else if (!this.mRegisteredReadOnlyExceptInHandler) {
                this.mHandler.obtainMessage(1).sendToTarget();
            }
        }
    }

    public void onClientActiveChanged(boolean nonGestureFingerprintClientActive) {
        synchronized (this.mLock) {
            for (int i = 0; i < this.mCapturingClients.size(); i++) {
                ((FingerprintGestureClient) this.mCapturingClients.get(i)).onFingerprintGestureDetectionActiveChanged(nonGestureFingerprintClientActive ^ 1);
            }
        }
    }

    public boolean isFingerprintGestureDetectionAvailable() {
        boolean isClientActive;
        try {
            isClientActive = this.mFingerprintService.isClientActive() ^ 1;
            return isClientActive;
        } catch (RemoteException e) {
            isClientActive = false;
            return isClientActive;
        } finally {
            Binder.restoreCallingIdentity(Binder.clearCallingIdentity());
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onFingerprintGesture(int r7) {
        /*
        r6 = this;
        r5 = 0;
        r4 = r6.mLock;
        monitor-enter(r4);
        r3 = r6.mCapturingClients;	 Catch:{ all -> 0x0036 }
        r3 = r3.isEmpty();	 Catch:{ all -> 0x0036 }
        if (r3 == 0) goto L_0x000e;
    L_0x000c:
        monitor-exit(r4);
        return r5;
    L_0x000e:
        switch(r7) {
            case 280: goto L_0x0013;
            case 281: goto L_0x002f;
            case 282: goto L_0x0034;
            case 283: goto L_0x0032;
            default: goto L_0x0011;
        };
    L_0x0011:
        monitor-exit(r4);
        return r5;
    L_0x0013:
        r2 = 4;
    L_0x0014:
        r0 = new java.util.ArrayList;	 Catch:{ all -> 0x0036 }
        r3 = r6.mCapturingClients;	 Catch:{ all -> 0x0036 }
        r0.<init>(r3);	 Catch:{ all -> 0x0036 }
        monitor-exit(r4);
        r1 = 0;
    L_0x001d:
        r3 = r0.size();
        if (r1 >= r3) goto L_0x0039;
    L_0x0023:
        r3 = r0.get(r1);
        r3 = (com.android.server.accessibility.FingerprintGestureDispatcher.FingerprintGestureClient) r3;
        r3.onFingerprintGesture(r2);
        r1 = r1 + 1;
        goto L_0x001d;
    L_0x002f:
        r2 = 8;
        goto L_0x0014;
    L_0x0032:
        r2 = 1;
        goto L_0x0014;
    L_0x0034:
        r2 = 2;
        goto L_0x0014;
    L_0x0036:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
    L_0x0039:
        r3 = 1;
        return r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.accessibility.FingerprintGestureDispatcher.onFingerprintGesture(int):boolean");
    }

    public boolean handleMessage(Message message) {
        long identity;
        if (message.what == 1) {
            identity = Binder.clearCallingIdentity();
            try {
                this.mFingerprintService.addClientActiveCallback(this);
                this.mRegisteredReadOnlyExceptInHandler = true;
            } catch (RemoteException e) {
                Slog.e(LOG_TAG, "Failed to register for fingerprint activity callbacks");
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
            return false;
        } else if (message.what == 2) {
            identity = Binder.clearCallingIdentity();
            try {
                this.mFingerprintService.removeClientActiveCallback(this);
            } catch (RemoteException e2) {
                Slog.e(LOG_TAG, "Failed to unregister for fingerprint activity callbacks");
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
            this.mRegisteredReadOnlyExceptInHandler = false;
            return true;
        } else {
            Slog.e(LOG_TAG, "Unknown message: " + message.what);
            return false;
        }
    }
}
