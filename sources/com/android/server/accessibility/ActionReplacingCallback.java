package com.android.server.accessibility;

import android.os.Binder;
import android.os.RemoteException;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction;
import android.view.accessibility.IAccessibilityInteractionConnection;
import android.view.accessibility.IAccessibilityInteractionConnectionCallback;
import android.view.accessibility.IAccessibilityInteractionConnectionCallback.Stub;
import com.android.internal.annotations.GuardedBy;
import java.util.List;

public class ActionReplacingCallback extends Stub {
    private static final boolean DEBUG = false;
    private static final String LOG_TAG = "ActionReplacingCallback";
    private final IAccessibilityInteractionConnection mConnectionWithReplacementActions;
    @GuardedBy("mLock")
    boolean mDone;
    private final int mInteractionId;
    private final Object mLock = new Object();
    @GuardedBy("mLock")
    boolean mMultiNodeCallbackHappened;
    @GuardedBy("mLock")
    AccessibilityNodeInfo mNodeFromOriginalWindow;
    @GuardedBy("mLock")
    List<AccessibilityNodeInfo> mNodesFromOriginalWindow;
    @GuardedBy("mLock")
    List<AccessibilityNodeInfo> mNodesWithReplacementActions;
    private final IAccessibilityInteractionConnectionCallback mServiceCallback;
    @GuardedBy("mLock")
    boolean mSingleNodeCallbackHappened;

    public ActionReplacingCallback(IAccessibilityInteractionConnectionCallback serviceCallback, IAccessibilityInteractionConnection connectionWithReplacementActions, int interactionId, int interrogatingPid, long interrogatingTid) {
        this.mServiceCallback = serviceCallback;
        this.mConnectionWithReplacementActions = connectionWithReplacementActions;
        this.mInteractionId = interactionId;
        long identityToken = Binder.clearCallingIdentity();
        try {
            this.mConnectionWithReplacementActions.findAccessibilityNodeInfoByAccessibilityId(AccessibilityNodeInfo.ROOT_NODE_ID, null, interactionId + 1, this, 0, interrogatingPid, interrogatingTid, null, null);
        } catch (RemoteException e) {
            this.mMultiNodeCallbackHappened = true;
        } finally {
            Binder.restoreCallingIdentity(identityToken);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setFindAccessibilityNodeInfoResult(android.view.accessibility.AccessibilityNodeInfo r5, int r6) {
        /*
        r4 = this;
        r2 = r4.mLock;
        monitor-enter(r2);
        r1 = r4.mInteractionId;	 Catch:{ all -> 0x0020 }
        if (r6 != r1) goto L_0x0015;
    L_0x0007:
        r4.mNodeFromOriginalWindow = r5;	 Catch:{ all -> 0x0020 }
        r1 = 1;
        r4.mSingleNodeCallbackHappened = r1;	 Catch:{ all -> 0x0020 }
        r0 = r4.mMultiNodeCallbackHappened;	 Catch:{ all -> 0x0020 }
        monitor-exit(r2);
        if (r0 == 0) goto L_0x0014;
    L_0x0011:
        r4.replaceInfoActionsAndCallService();
    L_0x0014:
        return;
    L_0x0015:
        r1 = "ActionReplacingCallback";
        r3 = "Callback with unexpected interactionId";
        android.util.Slog.e(r1, r3);	 Catch:{ all -> 0x0020 }
        monitor-exit(r2);
        return;
    L_0x0020:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.accessibility.ActionReplacingCallback.setFindAccessibilityNodeInfoResult(android.view.accessibility.AccessibilityNodeInfo, int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setFindAccessibilityNodeInfosResult(java.util.List<android.view.accessibility.AccessibilityNodeInfo> r6, int r7) {
        /*
        r5 = this;
        r3 = r5.mLock;
        monitor-enter(r3);
        r2 = r5.mInteractionId;	 Catch:{ all -> 0x0025 }
        if (r7 != r2) goto L_0x001c;
    L_0x0007:
        r5.mNodesFromOriginalWindow = r6;	 Catch:{ all -> 0x0025 }
    L_0x0009:
        r1 = r5.mSingleNodeCallbackHappened;	 Catch:{ all -> 0x0025 }
        r0 = r5.mMultiNodeCallbackHappened;	 Catch:{ all -> 0x0025 }
        r2 = 1;
        r5.mMultiNodeCallbackHappened = r2;	 Catch:{ all -> 0x0025 }
        monitor-exit(r3);
        if (r1 == 0) goto L_0x0016;
    L_0x0013:
        r5.replaceInfoActionsAndCallService();
    L_0x0016:
        if (r0 == 0) goto L_0x001b;
    L_0x0018:
        r5.replaceInfosActionsAndCallService();
    L_0x001b:
        return;
    L_0x001c:
        r2 = r5.mInteractionId;	 Catch:{ all -> 0x0025 }
        r2 = r2 + 1;
        if (r7 != r2) goto L_0x0028;
    L_0x0022:
        r5.mNodesWithReplacementActions = r6;	 Catch:{ all -> 0x0025 }
        goto L_0x0009;
    L_0x0025:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
    L_0x0028:
        r2 = "ActionReplacingCallback";
        r4 = "Callback with unexpected interactionId";
        android.util.Slog.e(r2, r4);	 Catch:{ all -> 0x0025 }
        monitor-exit(r3);
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.accessibility.ActionReplacingCallback.setFindAccessibilityNodeInfosResult(java.util.List, int):void");
    }

    public void setPerformAccessibilityActionResult(boolean succeeded, int interactionId) throws RemoteException {
        this.mServiceCallback.setPerformAccessibilityActionResult(succeeded, interactionId);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void replaceInfoActionsAndCallService() {
        /*
        r4 = this;
        r3 = r4.mLock;
        monitor-enter(r3);
        r2 = r4.mDone;	 Catch:{ all -> 0x0023 }
        if (r2 == 0) goto L_0x0009;
    L_0x0007:
        monitor-exit(r3);
        return;
    L_0x0009:
        r2 = r4.mNodeFromOriginalWindow;	 Catch:{ all -> 0x0023 }
        if (r2 == 0) goto L_0x0012;
    L_0x000d:
        r2 = r4.mNodeFromOriginalWindow;	 Catch:{ all -> 0x0023 }
        r4.replaceActionsOnInfoLocked(r2);	 Catch:{ all -> 0x0023 }
    L_0x0012:
        r4.recycleReplaceActionNodesLocked();	 Catch:{ all -> 0x0023 }
        r0 = r4.mNodeFromOriginalWindow;	 Catch:{ all -> 0x0023 }
        r2 = 1;
        r4.mDone = r2;	 Catch:{ all -> 0x0023 }
        monitor-exit(r3);
        r2 = r4.mServiceCallback;	 Catch:{ RemoteException -> 0x0026 }
        r3 = r4.mInteractionId;	 Catch:{ RemoteException -> 0x0026 }
        r2.setFindAccessibilityNodeInfoResult(r0, r3);	 Catch:{ RemoteException -> 0x0026 }
    L_0x0022:
        return;
    L_0x0023:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
    L_0x0026:
        r1 = move-exception;
        goto L_0x0022;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.accessibility.ActionReplacingCallback.replaceInfoActionsAndCallService():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void replaceInfosActionsAndCallService() {
        /*
        r5 = this;
        r4 = r5.mLock;
        monitor-enter(r4);
        r3 = r5.mDone;	 Catch:{ all -> 0x0040 }
        if (r3 == 0) goto L_0x0009;
    L_0x0007:
        monitor-exit(r4);
        return;
    L_0x0009:
        r3 = r5.mNodesFromOriginalWindow;	 Catch:{ all -> 0x0040 }
        if (r3 == 0) goto L_0x0024;
    L_0x000d:
        r0 = 0;
    L_0x000e:
        r3 = r5.mNodesFromOriginalWindow;	 Catch:{ all -> 0x0040 }
        r3 = r3.size();	 Catch:{ all -> 0x0040 }
        if (r0 >= r3) goto L_0x0024;
    L_0x0016:
        r3 = r5.mNodesFromOriginalWindow;	 Catch:{ all -> 0x0040 }
        r3 = r3.get(r0);	 Catch:{ all -> 0x0040 }
        r3 = (android.view.accessibility.AccessibilityNodeInfo) r3;	 Catch:{ all -> 0x0040 }
        r5.replaceActionsOnInfoLocked(r3);	 Catch:{ all -> 0x0040 }
        r0 = r0 + 1;
        goto L_0x000e;
    L_0x0024:
        r5.recycleReplaceActionNodesLocked();	 Catch:{ all -> 0x0040 }
        r3 = r5.mNodesFromOriginalWindow;	 Catch:{ all -> 0x0040 }
        if (r3 != 0) goto L_0x0038;
    L_0x002b:
        r1 = 0;
    L_0x002c:
        r3 = 1;
        r5.mDone = r3;	 Catch:{ all -> 0x0040 }
        monitor-exit(r4);
        r3 = r5.mServiceCallback;	 Catch:{ RemoteException -> 0x0043 }
        r4 = r5.mInteractionId;	 Catch:{ RemoteException -> 0x0043 }
        r3.setFindAccessibilityNodeInfosResult(r1, r4);	 Catch:{ RemoteException -> 0x0043 }
    L_0x0037:
        return;
    L_0x0038:
        r1 = new java.util.ArrayList;	 Catch:{ all -> 0x0040 }
        r3 = r5.mNodesFromOriginalWindow;	 Catch:{ all -> 0x0040 }
        r1.<init>(r3);	 Catch:{ all -> 0x0040 }
        goto L_0x002c;
    L_0x0040:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
    L_0x0043:
        r2 = move-exception;
        goto L_0x0037;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.accessibility.ActionReplacingCallback.replaceInfosActionsAndCallService():void");
    }

    @GuardedBy("mLock")
    private void replaceActionsOnInfoLocked(AccessibilityNodeInfo info) {
        info.removeAllActions();
        info.setClickable(false);
        info.setFocusable(false);
        info.setContextClickable(false);
        info.setScrollable(false);
        info.setLongClickable(false);
        info.setDismissable(false);
        if (info.getSourceNodeId() == AccessibilityNodeInfo.ROOT_NODE_ID && this.mNodesWithReplacementActions != null) {
            for (int i = 0; i < this.mNodesWithReplacementActions.size(); i++) {
                AccessibilityNodeInfo nodeWithReplacementActions = (AccessibilityNodeInfo) this.mNodesWithReplacementActions.get(i);
                if (nodeWithReplacementActions.getSourceNodeId() == AccessibilityNodeInfo.ROOT_NODE_ID) {
                    List<AccessibilityAction> actions = nodeWithReplacementActions.getActionList();
                    if (actions != null) {
                        for (int j = 0; j < actions.size(); j++) {
                            info.addAction((AccessibilityAction) actions.get(j));
                        }
                        info.addAction(AccessibilityAction.ACTION_ACCESSIBILITY_FOCUS);
                        info.addAction(AccessibilityAction.ACTION_CLEAR_ACCESSIBILITY_FOCUS);
                    }
                    info.setClickable(nodeWithReplacementActions.isClickable());
                    info.setFocusable(nodeWithReplacementActions.isFocusable());
                    info.setContextClickable(nodeWithReplacementActions.isContextClickable());
                    info.setScrollable(nodeWithReplacementActions.isScrollable());
                    info.setLongClickable(nodeWithReplacementActions.isLongClickable());
                    info.setDismissable(nodeWithReplacementActions.isDismissable());
                }
            }
        }
    }

    @GuardedBy("mLock")
    private void recycleReplaceActionNodesLocked() {
        if (this.mNodesWithReplacementActions != null) {
            for (int i = this.mNodesWithReplacementActions.size() - 1; i >= 0; i--) {
                ((AccessibilityNodeInfo) this.mNodesWithReplacementActions.get(i)).recycle();
            }
            this.mNodesWithReplacementActions = null;
        }
    }
}
