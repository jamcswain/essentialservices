package com.android.server.notification;

import android.app.AppOpsManager;
import android.app.AutomaticZenRule;
import android.app.NotificationManager;
import android.app.NotificationManager.Policy;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.media.AudioAttributes;
import android.media.AudioManagerInternal;
import android.media.VolumePolicy;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.UserHandle;
import android.provider.Settings.Global;
import android.service.notification.ZenModeConfig;
import android.service.notification.ZenModeConfig.EventInfo;
import android.service.notification.ZenModeConfig.ScheduleInfo;
import android.service.notification.ZenModeConfig.ZenRule;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.util.proto.ProtoOutputStream;
import com.android.internal.logging.MetricsLogger;
import com.android.server.LocalServices;
import com.android.server.notification.ManagedServices.Config;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class ZenModeHelper {
    static final boolean DEBUG = Log.isLoggable(TAG, 3);
    private static final int RULE_INSTANCE_GRACE_PERIOD = 259200000;
    public static final long SUPPRESSED_EFFECT_ALL = 3;
    public static final long SUPPRESSED_EFFECT_CALLS = 2;
    public static final long SUPPRESSED_EFFECT_NOTIFICATIONS = 1;
    static final String TAG = "ZenModeHelper";
    private final String EVENTS_DEFAULT_RULE = "EVENTS_DEFAULT_RULE";
    private final String SCHEDULED_DEFAULT_RULE_1 = "SCHEDULED_DEFAULT_RULE_1";
    private final String SCHEDULED_DEFAULT_RULE_2 = "SCHEDULED_DEFAULT_RULE_2";
    private final AppOpsManager mAppOps;
    private AudioManagerInternal mAudioManager;
    private final ArrayList<Callback> mCallbacks = new ArrayList();
    private final ZenModeConditions mConditions;
    protected ZenModeConfig mConfig;
    private final SparseArray<ZenModeConfig> mConfigs = new SparseArray();
    private final Context mContext;
    protected ZenModeConfig mDefaultConfig;
    protected String mDefaultRuleEventsName;
    protected final ArrayList<String> mDefaultRuleIds = new ArrayList();
    protected String mDefaultRuleWeekendsName;
    protected String mDefaultRuleWeeknightsName;
    private final ZenModeFiltering mFiltering;
    private final H mHandler;
    private final Metrics mMetrics = new Metrics();
    protected PackageManager mPm;
    private final RingerModeDelegate mRingerModeDelegate = new RingerModeDelegate();
    private final Config mServiceConfig;
    private final SettingsObserver mSettingsObserver;
    private long mSuppressedEffects;
    private int mUser = 0;
    private int mZenMode;

    public static class Callback {
        void onConfigChanged() {
        }

        void onZenModeChanged() {
        }

        void onPolicyChanged() {
        }
    }

    private final class H extends Handler {
        private static final long METRICS_PERIOD_MS = 21600000;
        private static final int MSG_APPLY_CONFIG = 4;
        private static final int MSG_DISPATCH = 1;
        private static final int MSG_METRICS = 2;

        private final class ConfigMessageData {
            public final ZenModeConfig config;
            public final String reason;
            public final boolean setRingerMode;

            ConfigMessageData(ZenModeConfig config, String reason, boolean setRingerMode) {
                this.config = config;
                this.reason = reason;
                this.setRingerMode = setRingerMode;
            }
        }

        private H(Looper looper) {
            super(looper);
        }

        private void postDispatchOnZenModeChanged() {
            removeMessages(1);
            sendEmptyMessage(1);
        }

        private void postMetricsTimer() {
            removeMessages(2);
            sendEmptyMessageDelayed(2, METRICS_PERIOD_MS);
        }

        private void postApplyConfig(ZenModeConfig config, String reason, boolean setRingerMode) {
            sendMessage(obtainMessage(4, new ConfigMessageData(config, reason, setRingerMode)));
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    ZenModeHelper.this.dispatchOnZenModeChanged();
                    return;
                case 2:
                    ZenModeHelper.this.mMetrics.emit();
                    return;
                case 4:
                    ConfigMessageData applyConfigData = msg.obj;
                    ZenModeHelper.this.applyConfig(applyConfigData.config, applyConfigData.reason, applyConfigData.setRingerMode);
                    return;
                default:
                    return;
            }
        }
    }

    private final class Metrics extends Callback {
        private static final String COUNTER_PREFIX = "dnd_mode_";
        private static final long MINIMUM_LOG_PERIOD_MS = 60000;
        private long mBeginningMs;
        private int mPreviousZenMode;

        private Metrics() {
            this.mPreviousZenMode = -1;
            this.mBeginningMs = 0;
        }

        void onZenModeChanged() {
            emit();
        }

        private void emit() {
            ZenModeHelper.this.mHandler.postMetricsTimer();
            long now = SystemClock.elapsedRealtime();
            long since = now - this.mBeginningMs;
            if (this.mPreviousZenMode != ZenModeHelper.this.mZenMode || since > 60000) {
                if (this.mPreviousZenMode != -1) {
                    MetricsLogger.count(ZenModeHelper.this.mContext, COUNTER_PREFIX + this.mPreviousZenMode, (int) since);
                }
                this.mPreviousZenMode = ZenModeHelper.this.mZenMode;
                this.mBeginningMs = now;
            }
        }
    }

    private final class RingerModeDelegate implements android.media.AudioManagerInternal.RingerModeDelegate {
        private RingerModeDelegate() {
        }

        public String toString() {
            return ZenModeHelper.TAG;
        }

        public int onSetRingerModeInternal(int ringerModeOld, int ringerModeNew, String caller, int ringerModeExternal, VolumePolicy policy) {
            boolean isChange = ringerModeOld != ringerModeNew;
            int ringerModeExternalOut = ringerModeNew;
            int newZen = -1;
            switch (ringerModeNew) {
                case 0:
                    if (isChange && policy.doNotDisturbWhenSilent) {
                        if (!(ZenModeHelper.this.mZenMode == 2 || ZenModeHelper.this.mZenMode == 3)) {
                            newZen = 3;
                        }
                        ZenModeHelper.this.setPreviousRingerModeSetting(Integer.valueOf(ringerModeOld));
                        break;
                    }
                case 1:
                case 2:
                    if (!isChange || ringerModeOld != 0 || (ZenModeHelper.this.mZenMode != 2 && ZenModeHelper.this.mZenMode != 3)) {
                        if (ZenModeHelper.this.mZenMode != 0) {
                            ringerModeExternalOut = 0;
                            break;
                        }
                    }
                    newZen = 0;
                    break;
                    break;
            }
            if (newZen != -1) {
                ZenModeHelper.this.setManualZenMode(newZen, null, "ringerModeInternal", null, false);
            }
            if (!isChange && newZen == -1) {
                if (ringerModeExternal != ringerModeExternalOut) {
                }
                return ringerModeExternalOut;
            }
            ZenLog.traceSetRingerModeInternal(ringerModeOld, ringerModeNew, caller, ringerModeExternal, ringerModeExternalOut);
            return ringerModeExternalOut;
        }

        public int onSetRingerModeExternal(int ringerModeOld, int ringerModeNew, String caller, int ringerModeInternal, VolumePolicy policy) {
            int ringerModeInternalOut = ringerModeNew;
            boolean isChange = ringerModeOld != ringerModeNew;
            boolean isVibrate = ringerModeInternal == 1;
            int newZen = -1;
            switch (ringerModeNew) {
                case 0:
                    if (!isChange) {
                        ringerModeInternalOut = ringerModeInternal;
                        break;
                    }
                    if (ZenModeHelper.this.mZenMode == 0) {
                        newZen = 3;
                    }
                    if (!isVibrate) {
                        ringerModeInternalOut = 0;
                        break;
                    }
                    ringerModeInternalOut = 1;
                    break;
                case 1:
                case 2:
                    if (ZenModeHelper.this.mZenMode != 0) {
                        newZen = 0;
                        break;
                    }
                    break;
            }
            if (newZen != -1) {
                ZenModeHelper.this.setManualZenMode(newZen, null, "ringerModeExternal", caller, false);
            }
            ZenLog.traceSetRingerModeExternal(ringerModeOld, ringerModeNew, caller, ringerModeInternal, ringerModeInternalOut);
            return ringerModeInternalOut;
        }

        public boolean canVolumeDownEnterSilent() {
            return ZenModeHelper.this.mZenMode == 0;
        }

        public int getRingerModeAffectedStreams(int streams) {
            streams |= 38;
            if (ZenModeHelper.this.mZenMode == 2) {
                return streams | 24;
            }
            return streams & -25;
        }
    }

    private final class SettingsObserver extends ContentObserver {
        private final Uri ZEN_MODE = Global.getUriFor("zen_mode");

        public SettingsObserver(Handler handler) {
            super(handler);
        }

        public void observe() {
            ZenModeHelper.this.mContext.getContentResolver().registerContentObserver(this.ZEN_MODE, false, this);
            update(null);
        }

        public void onChange(boolean selfChange, Uri uri) {
            update(uri);
        }

        public void update(Uri uri) {
            if (this.ZEN_MODE.equals(uri) && ZenModeHelper.this.mZenMode != ZenModeHelper.this.getZenModeSetting()) {
                if (ZenModeHelper.DEBUG) {
                    Log.d(ZenModeHelper.TAG, "Fixing zen mode setting");
                }
                ZenModeHelper.this.setZenModeSetting(ZenModeHelper.this.mZenMode);
            }
        }
    }

    public ZenModeHelper(Context context, Looper looper, ConditionProviders conditionProviders) {
        this.mContext = context;
        this.mHandler = new H(looper);
        addCallback(this.mMetrics);
        this.mAppOps = (AppOpsManager) context.getSystemService("appops");
        this.mDefaultConfig = new ZenModeConfig();
        this.mDefaultRuleWeeknightsName = this.mContext.getResources().getString(17041037);
        this.mDefaultRuleWeekendsName = this.mContext.getResources().getString(17041036);
        this.mDefaultRuleEventsName = this.mContext.getResources().getString(17041035);
        setDefaultZenRules(this.mContext);
        this.mConfig = this.mDefaultConfig;
        this.mConfigs.put(0, this.mConfig);
        this.mSettingsObserver = new SettingsObserver(this.mHandler);
        this.mSettingsObserver.observe();
        this.mFiltering = new ZenModeFiltering(this.mContext);
        this.mConditions = new ZenModeConditions(this, conditionProviders);
        this.mServiceConfig = conditionProviders.getConfig();
    }

    public Looper getLooper() {
        return this.mHandler.getLooper();
    }

    public String toString() {
        return TAG;
    }

    public boolean matchesCallFilter(UserHandle userHandle, Bundle extras, ValidateNotificationPeople validator, int contactsTimeoutMs, float timeoutAffinity) {
        boolean matchesCallFilter;
        synchronized (this.mConfig) {
            matchesCallFilter = ZenModeFiltering.matchesCallFilter(this.mContext, this.mZenMode, this.mConfig, userHandle, extras, validator, contactsTimeoutMs, timeoutAffinity);
        }
        return matchesCallFilter;
    }

    public boolean isCall(NotificationRecord record) {
        return this.mFiltering.isCall(record);
    }

    public void recordCaller(NotificationRecord record) {
        this.mFiltering.recordCall(record);
    }

    public boolean shouldIntercept(NotificationRecord record) {
        boolean shouldIntercept;
        synchronized (this.mConfig) {
            shouldIntercept = this.mFiltering.shouldIntercept(this.mZenMode, this.mConfig, record);
        }
        return shouldIntercept;
    }

    public boolean shouldSuppressWhenScreenOff() {
        boolean z;
        synchronized (this.mConfig) {
            z = this.mConfig.allowWhenScreenOff ^ 1;
        }
        return z;
    }

    public boolean shouldSuppressWhenScreenOn() {
        boolean z;
        synchronized (this.mConfig) {
            z = this.mConfig.allowWhenScreenOn ^ 1;
        }
        return z;
    }

    public void addCallback(Callback callback) {
        this.mCallbacks.add(callback);
    }

    public void removeCallback(Callback callback) {
        this.mCallbacks.remove(callback);
    }

    public void initZenMode() {
        if (DEBUG) {
            Log.d(TAG, "initZenMode");
        }
        evaluateZenMode("init", true);
    }

    public void onSystemReady() {
        if (DEBUG) {
            Log.d(TAG, "onSystemReady");
        }
        this.mAudioManager = (AudioManagerInternal) LocalServices.getService(AudioManagerInternal.class);
        if (this.mAudioManager != null) {
            this.mAudioManager.setRingerModeDelegate(this.mRingerModeDelegate);
        }
        this.mPm = this.mContext.getPackageManager();
        this.mHandler.postMetricsTimer();
        cleanUpZenRules();
        evaluateZenMode("onSystemReady", true);
    }

    public void onUserSwitched(int user) {
        loadConfigForUser(user, "onUserSwitched");
    }

    public void onUserRemoved(int user) {
        if (user >= 0) {
            if (DEBUG) {
                Log.d(TAG, "onUserRemoved u=" + user);
            }
            this.mConfigs.remove(user);
        }
    }

    public void onUserUnlocked(int user) {
        loadConfigForUser(user, "onUserUnlocked");
    }

    private void loadConfigForUser(int user, String reason) {
        if (this.mUser != user && user >= 0) {
            this.mUser = user;
            if (DEBUG) {
                Log.d(TAG, reason + " u=" + user);
            }
            ZenModeConfig config = (ZenModeConfig) this.mConfigs.get(user);
            if (config == null) {
                if (DEBUG) {
                    Log.d(TAG, reason + " generating default config for user " + user);
                }
                config = this.mDefaultConfig.copy();
                config.user = user;
            }
            synchronized (this.mConfig) {
                setConfigLocked(config, reason);
            }
            cleanUpZenRules();
        }
    }

    public int getZenModeListenerInterruptionFilter() {
        return NotificationManager.zenModeToInterruptionFilter(this.mZenMode);
    }

    public void requestFromListener(ComponentName name, int filter) {
        int newZen = NotificationManager.zenModeFromInterruptionFilter(filter, -1);
        if (newZen != -1) {
            String packageName;
            String flattenToShortString;
            if (name != null) {
                packageName = name.getPackageName();
            } else {
                packageName = null;
            }
            StringBuilder append = new StringBuilder().append("listener:");
            if (name != null) {
                flattenToShortString = name.flattenToShortString();
            } else {
                flattenToShortString = null;
            }
            setManualZenMode(newZen, null, packageName, append.append(flattenToShortString).toString());
        }
    }

    public void setSuppressedEffects(long suppressedEffects) {
        if (this.mSuppressedEffects != suppressedEffects) {
            this.mSuppressedEffects = suppressedEffects;
            applyRestrictions();
        }
    }

    public long getSuppressedEffects() {
        return this.mSuppressedEffects;
    }

    public int getZenMode() {
        return this.mZenMode;
    }

    public List<ZenRule> getZenRules() {
        List<ZenRule> rules = new ArrayList();
        synchronized (this.mConfig) {
            if (this.mConfig == null) {
                return rules;
            }
            for (ZenRule rule : this.mConfig.automaticRules.values()) {
                if (canManageAutomaticZenRule(rule)) {
                    rules.add(rule);
                }
            }
            return rules;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.AutomaticZenRule getAutomaticZenRule(java.lang.String r5) {
        /*
        r4 = this;
        r3 = 0;
        r2 = r4.mConfig;
        monitor-enter(r2);
        r1 = r4.mConfig;	 Catch:{ all -> 0x0018 }
        if (r1 != 0) goto L_0x000a;
    L_0x0008:
        monitor-exit(r2);
        return r3;
    L_0x000a:
        r1 = r4.mConfig;	 Catch:{ all -> 0x0018 }
        r1 = r1.automaticRules;	 Catch:{ all -> 0x0018 }
        r0 = r1.get(r5);	 Catch:{ all -> 0x0018 }
        r0 = (android.service.notification.ZenModeConfig.ZenRule) r0;	 Catch:{ all -> 0x0018 }
        monitor-exit(r2);
        if (r0 != 0) goto L_0x001b;
    L_0x0017:
        return r3;
    L_0x0018:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
    L_0x001b:
        r1 = r4.canManageAutomaticZenRule(r0);
        if (r1 == 0) goto L_0x0026;
    L_0x0021:
        r1 = r4.createAutomaticZenRule(r0);
        return r1;
    L_0x0026:
        return r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.ZenModeHelper.getAutomaticZenRule(java.lang.String):android.app.AutomaticZenRule");
    }

    public String addAutomaticZenRule(AutomaticZenRule automaticZenRule, String reason) {
        String str;
        if (!isSystemRule(automaticZenRule)) {
            ServiceInfo owner = getServiceInfo(automaticZenRule.getOwner());
            if (owner == null) {
                throw new IllegalArgumentException("Owner is not a condition provider service");
            }
            int ruleInstanceLimit = -1;
            if (owner.metaData != null) {
                ruleInstanceLimit = owner.metaData.getInt("android.service.zen.automatic.ruleInstanceLimit", -1);
            }
            if (ruleInstanceLimit > 0 && ruleInstanceLimit < getCurrentInstanceCount(automaticZenRule.getOwner()) + 1) {
                throw new IllegalArgumentException("Rule instance limit exceeded");
            }
        }
        synchronized (this.mConfig) {
            if (this.mConfig == null) {
                throw new AndroidRuntimeException("Could not create rule");
            }
            if (DEBUG) {
                Log.d(TAG, "addAutomaticZenRule rule= " + automaticZenRule + " reason=" + reason);
            }
            ZenModeConfig newConfig = this.mConfig.copy();
            ZenRule rule = new ZenRule();
            populateZenRule(automaticZenRule, rule, true);
            newConfig.automaticRules.put(rule.id, rule);
            if (setConfigLocked(newConfig, reason, true)) {
                str = rule.id;
            } else {
                throw new AndroidRuntimeException("Could not create rule");
            }
        }
        return str;
    }

    public boolean updateAutomaticZenRule(String ruleId, AutomaticZenRule automaticZenRule, String reason) {
        synchronized (this.mConfig) {
            if (this.mConfig == null) {
                return false;
            }
            if (DEBUG) {
                Log.d(TAG, "updateAutomaticZenRule zenRule=" + automaticZenRule + " reason=" + reason);
            }
            ZenModeConfig newConfig = this.mConfig.copy();
            if (ruleId == null) {
                throw new IllegalArgumentException("Rule doesn't exist");
            }
            ZenRule rule = (ZenRule) newConfig.automaticRules.get(ruleId);
            if (rule == null || (canManageAutomaticZenRule(rule) ^ 1) != 0) {
                throw new SecurityException("Cannot update rules not owned by your condition provider");
            }
            populateZenRule(automaticZenRule, rule, false);
            newConfig.automaticRules.put(ruleId, rule);
            boolean configLocked = setConfigLocked(newConfig, reason, true);
            return configLocked;
        }
    }

    public boolean removeAutomaticZenRule(String id, String reason) {
        synchronized (this.mConfig) {
            if (this.mConfig == null) {
                return false;
            }
            ZenModeConfig newConfig = this.mConfig.copy();
            ZenRule rule = (ZenRule) newConfig.automaticRules.get(id);
            if (rule == null) {
                return false;
            } else if (canManageAutomaticZenRule(rule)) {
                newConfig.automaticRules.remove(id);
                if (DEBUG) {
                    Log.d(TAG, "removeZenRule zenRule=" + id + " reason=" + reason);
                }
                boolean configLocked = setConfigLocked(newConfig, reason, true);
                return configLocked;
            } else {
                throw new SecurityException("Cannot delete rules not owned by your condition provider");
            }
        }
    }

    public boolean removeAutomaticZenRules(String packageName, String reason) {
        synchronized (this.mConfig) {
            if (this.mConfig == null) {
                return false;
            }
            ZenModeConfig newConfig = this.mConfig.copy();
            for (int i = newConfig.automaticRules.size() - 1; i >= 0; i--) {
                ZenRule rule = (ZenRule) newConfig.automaticRules.get(newConfig.automaticRules.keyAt(i));
                if (rule.component.getPackageName().equals(packageName) && canManageAutomaticZenRule(rule)) {
                    newConfig.automaticRules.removeAt(i);
                }
            }
            boolean configLocked = setConfigLocked(newConfig, reason, true);
            return configLocked;
        }
    }

    public int getCurrentInstanceCount(ComponentName owner) {
        int count = 0;
        synchronized (this.mConfig) {
            for (ZenRule rule : this.mConfig.automaticRules.values()) {
                if (rule.component != null && rule.component.equals(owner)) {
                    count++;
                }
            }
        }
        return count;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean canManageAutomaticZenRule(android.service.notification.ZenModeConfig.ZenRule r9) {
        /*
        r8 = this;
        r7 = 1;
        r6 = 0;
        r0 = android.os.Binder.getCallingUid();
        if (r0 == 0) goto L_0x000c;
    L_0x0008:
        r4 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        if (r0 != r4) goto L_0x000d;
    L_0x000c:
        return r7;
    L_0x000d:
        r4 = r8.mContext;
        r5 = "android.permission.MANAGE_NOTIFICATIONS";
        r4 = r4.checkCallingPermission(r5);
        if (r4 != 0) goto L_0x0019;
    L_0x0018:
        return r7;
    L_0x0019:
        r4 = r8.mPm;
        r5 = android.os.Binder.getCallingUid();
        r3 = r4.getPackagesForUid(r5);
        if (r3 == 0) goto L_0x003b;
    L_0x0025:
        r2 = r3.length;
        r1 = 0;
    L_0x0027:
        if (r1 >= r2) goto L_0x003b;
    L_0x0029:
        r4 = r3[r1];
        r5 = r9.component;
        r5 = r5.getPackageName();
        r4 = r4.equals(r5);
        if (r4 == 0) goto L_0x0038;
    L_0x0037:
        return r7;
    L_0x0038:
        r1 = r1 + 1;
        goto L_0x0027;
    L_0x003b:
        return r6;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.ZenModeHelper.canManageAutomaticZenRule(android.service.notification.ZenModeConfig$ZenRule):boolean");
    }

    public void setDefaultZenRules(Context context) {
        this.mDefaultConfig = readDefaultConfig(context.getResources());
        this.mDefaultRuleIds.add("EVENTS_DEFAULT_RULE");
        this.mDefaultRuleIds.add("SCHEDULED_DEFAULT_RULE_1");
        this.mDefaultRuleIds.add("SCHEDULED_DEFAULT_RULE_2");
        appendDefaultRules(this.mDefaultConfig);
    }

    private void appendDefaultRules(ZenModeConfig config) {
        appendDefaultScheduleRules(config);
        appendDefaultEventRules(config);
    }

    private boolean ruleValuesEqual(AutomaticZenRule rule, ZenRule defaultRule) {
        boolean z = false;
        if (rule == null || defaultRule == null) {
            return false;
        }
        if (rule.getInterruptionFilter() == NotificationManager.zenModeToInterruptionFilter(defaultRule.zenMode) && rule.getConditionId().equals(defaultRule.conditionId)) {
            z = rule.getOwner().equals(defaultRule.component);
        }
        return z;
    }

    protected void updateDefaultZenRules() {
        ZenModeConfig configDefaultRules = new ZenModeConfig();
        appendDefaultRules(configDefaultRules);
        for (String ruleId : this.mDefaultRuleIds) {
            AutomaticZenRule currRule = getAutomaticZenRule(ruleId);
            ZenRule defaultRule = (ZenRule) configDefaultRules.automaticRules.get(ruleId);
            if (ruleValuesEqual(currRule, defaultRule) && (defaultRule.name.equals(currRule.getName()) ^ 1) != 0 && canManageAutomaticZenRule(defaultRule)) {
                if (DEBUG) {
                    Slog.d(TAG, "Locale change - updating default zen rule name from " + currRule.getName() + " to " + defaultRule.name);
                }
                AutomaticZenRule defaultAutoRule = createAutomaticZenRule(defaultRule);
                defaultAutoRule.setEnabled(currRule.isEnabled());
                updateAutomaticZenRule(ruleId, defaultAutoRule, "locale changed");
            }
        }
    }

    private boolean isSystemRule(AutomaticZenRule rule) {
        return "android".equals(rule.getOwner().getPackageName());
    }

    private ServiceInfo getServiceInfo(ComponentName owner) {
        Intent queryIntent = new Intent();
        queryIntent.setComponent(owner);
        List<ResolveInfo> installedServices = this.mPm.queryIntentServicesAsUser(queryIntent, 132, UserHandle.getCallingUserId());
        if (installedServices != null) {
            int count = installedServices.size();
            for (int i = 0; i < count; i++) {
                ServiceInfo info = ((ResolveInfo) installedServices.get(i)).serviceInfo;
                if (this.mServiceConfig.bindPermission.equals(info.permission)) {
                    return info;
                }
            }
        }
        return null;
    }

    private void populateZenRule(AutomaticZenRule automaticZenRule, ZenRule rule, boolean isNew) {
        if (isNew) {
            rule.id = ZenModeConfig.newRuleId();
            rule.creationTime = System.currentTimeMillis();
            rule.component = automaticZenRule.getOwner();
        }
        if (rule.enabled != automaticZenRule.isEnabled()) {
            rule.snoozing = false;
        }
        rule.name = automaticZenRule.getName();
        rule.condition = null;
        rule.conditionId = automaticZenRule.getConditionId();
        rule.enabled = automaticZenRule.isEnabled();
        rule.zenMode = NotificationManager.zenModeFromInterruptionFilter(automaticZenRule.getInterruptionFilter(), 0);
    }

    protected AutomaticZenRule createAutomaticZenRule(ZenRule rule) {
        return new AutomaticZenRule(rule.name, rule.component, rule.conditionId, NotificationManager.zenModeToInterruptionFilter(rule.zenMode), rule.enabled, rule.creationTime);
    }

    public void setManualZenMode(int zenMode, Uri conditionId, String caller, String reason) {
        setManualZenMode(zenMode, conditionId, reason, caller, true);
    }

    private void setManualZenMode(int zenMode, Uri conditionId, String reason, String caller, boolean setRingerMode) {
        synchronized (this.mConfig) {
            if (this.mConfig == null) {
            } else if (Global.isValidZenMode(zenMode)) {
                if (DEBUG) {
                    Log.d(TAG, "setManualZenMode " + Global.zenModeToString(zenMode) + " conditionId=" + conditionId + " reason=" + reason + " setRingerMode=" + setRingerMode);
                }
                ZenModeConfig newConfig = this.mConfig.copy();
                if (zenMode == 0) {
                    newConfig.manualRule = null;
                    for (ZenRule automaticRule : newConfig.automaticRules.values()) {
                        if (automaticRule.isAutomaticActive()) {
                            automaticRule.snoozing = true;
                        }
                    }
                } else {
                    ZenRule newRule = new ZenRule();
                    newRule.enabled = true;
                    newRule.zenMode = zenMode;
                    newRule.conditionId = conditionId;
                    newRule.enabler = caller;
                    newConfig.manualRule = newRule;
                }
                setConfigLocked(newConfig, reason, setRingerMode);
            }
        }
    }

    void dump(ProtoOutputStream proto) {
        proto.write(1168231104513L, this.mZenMode);
        synchronized (this.mConfig) {
            if (this.mConfig.manualRule != null) {
                proto.write(2259152797698L, this.mConfig.manualRule.toString());
            }
            for (ZenRule rule : this.mConfig.automaticRules.values()) {
                if (rule.enabled && rule.condition.state == 1 && (rule.snoozing ^ 1) != 0) {
                    proto.write(2259152797698L, rule.toString());
                }
            }
            proto.write(1159641169925L, this.mConfig.toNotificationPolicy().toString());
            proto.write(1112396529667L, this.mSuppressedEffects);
        }
    }

    public void dump(PrintWriter pw, String prefix) {
        pw.print(prefix);
        pw.print("mZenMode=");
        pw.println(Global.zenModeToString(this.mZenMode));
        int N = this.mConfigs.size();
        for (int i = 0; i < N; i++) {
            dump(pw, prefix, "mConfigs[u=" + this.mConfigs.keyAt(i) + "]", (ZenModeConfig) this.mConfigs.valueAt(i));
        }
        pw.print(prefix);
        pw.print("mUser=");
        pw.println(this.mUser);
        synchronized (this.mConfig) {
            dump(pw, prefix, "mConfig", this.mConfig);
        }
        pw.print(prefix);
        pw.print("mSuppressedEffects=");
        pw.println(this.mSuppressedEffects);
        this.mFiltering.dump(pw, prefix);
        this.mConditions.dump(pw, prefix);
    }

    private static void dump(PrintWriter pw, String prefix, String var, ZenModeConfig config) {
        pw.print(prefix);
        pw.print(var);
        pw.print('=');
        if (config == null) {
            pw.println(config);
            return;
        }
        pw.printf("allow(calls=%b,callsFrom=%s,repeatCallers=%b,messages=%b,messagesFrom=%s,events=%b,reminders=%b,whenScreenOff=%b,whenScreenOn=%b)\n", new Object[]{Boolean.valueOf(config.allowCalls), ZenModeConfig.sourceToString(config.allowCallsFrom), Boolean.valueOf(config.allowRepeatCallers), Boolean.valueOf(config.allowMessages), ZenModeConfig.sourceToString(config.allowMessagesFrom), Boolean.valueOf(config.allowEvents), Boolean.valueOf(config.allowReminders), Boolean.valueOf(config.allowWhenScreenOff), Boolean.valueOf(config.allowWhenScreenOn)});
        pw.print(prefix);
        pw.print("  manualRule=");
        pw.println(config.manualRule);
        if (!config.automaticRules.isEmpty()) {
            int N = config.automaticRules.size();
            int i = 0;
            while (i < N) {
                pw.print(prefix);
                pw.print(i == 0 ? "  automaticRules=" : "                 ");
                pw.println(config.automaticRules.valueAt(i));
                i++;
            }
        }
    }

    public void readXml(XmlPullParser parser, boolean forRestore) throws XmlPullParserException, IOException {
        ZenModeConfig config = ZenModeConfig.readXml(parser);
        if (config != null) {
            if (forRestore) {
                if (config.user == 0) {
                    config.manualRule = null;
                    long time = System.currentTimeMillis();
                    if (config.automaticRules != null) {
                        for (ZenRule automaticRule : config.automaticRules.values()) {
                            automaticRule.snoozing = false;
                            automaticRule.condition = null;
                            automaticRule.creationTime = time;
                        }
                    }
                } else {
                    return;
                }
            }
            if (DEBUG) {
                Log.d(TAG, "readXml");
            }
            synchronized (this.mConfig) {
                setConfigLocked(config, "readXml");
            }
        }
    }

    public void writeXml(XmlSerializer out, boolean forBackup) throws IOException {
        int N = this.mConfigs.size();
        int i = 0;
        while (i < N) {
            if (!forBackup || this.mConfigs.keyAt(i) == 0) {
                ((ZenModeConfig) this.mConfigs.valueAt(i)).writeXml(out);
            }
            i++;
        }
    }

    public Policy getNotificationPolicy() {
        return getNotificationPolicy(this.mConfig);
    }

    private static Policy getNotificationPolicy(ZenModeConfig config) {
        return config == null ? null : config.toNotificationPolicy();
    }

    public void setNotificationPolicy(Policy policy) {
        if (policy != null && this.mConfig != null) {
            synchronized (this.mConfig) {
                ZenModeConfig newConfig = this.mConfig.copy();
                newConfig.applyNotificationPolicy(policy);
                setConfigLocked(newConfig, "setNotificationPolicy");
            }
        }
    }

    private void cleanUpZenRules() {
        long currentTime = System.currentTimeMillis();
        synchronized (this.mConfig) {
            ZenModeConfig newConfig = this.mConfig.copy();
            if (newConfig.automaticRules != null) {
                for (int i = newConfig.automaticRules.size() - 1; i >= 0; i--) {
                    ZenRule rule = (ZenRule) newConfig.automaticRules.get(newConfig.automaticRules.keyAt(i));
                    if (259200000 < currentTime - rule.creationTime) {
                        try {
                            this.mPm.getPackageInfo(rule.component.getPackageName(), DumpState.DUMP_CHANGES);
                        } catch (NameNotFoundException e) {
                            newConfig.automaticRules.removeAt(i);
                        }
                    }
                }
            }
            setConfigLocked(newConfig, "cleanUpZenRules");
        }
    }

    public ZenModeConfig getConfig() {
        ZenModeConfig copy;
        synchronized (this.mConfig) {
            copy = this.mConfig.copy();
        }
        return copy;
    }

    public boolean setConfigLocked(ZenModeConfig config, String reason) {
        return setConfigLocked(config, reason, true);
    }

    public void setConfig(ZenModeConfig config, String reason) {
        synchronized (this.mConfig) {
            setConfigLocked(config, reason);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean setConfigLocked(android.service.notification.ZenModeConfig r10, java.lang.String r11, boolean r12) {
        /*
        r9 = this;
        r8 = 1;
        r7 = 0;
        r2 = android.os.Binder.clearCallingIdentity();
        if (r10 == 0) goto L_0x0010;
    L_0x0008:
        r4 = r10.isValid();	 Catch:{ SecurityException -> 0x00bf }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x002e;
    L_0x0010:
        r4 = "ZenModeHelper";
        r5 = new java.lang.StringBuilder;	 Catch:{ SecurityException -> 0x00bf }
        r5.<init>();	 Catch:{ SecurityException -> 0x00bf }
        r6 = "Invalid config in setConfigLocked; ";
        r5 = r5.append(r6);	 Catch:{ SecurityException -> 0x00bf }
        r5 = r5.append(r10);	 Catch:{ SecurityException -> 0x00bf }
        r5 = r5.toString();	 Catch:{ SecurityException -> 0x00bf }
        android.util.Log.w(r4, r5);	 Catch:{ SecurityException -> 0x00bf }
        android.os.Binder.restoreCallingIdentity(r2);
        return r7;
    L_0x002e:
        r4 = r10.user;	 Catch:{ SecurityException -> 0x00bf }
        r5 = r9.mUser;	 Catch:{ SecurityException -> 0x00bf }
        if (r4 == r5) goto L_0x005f;
    L_0x0034:
        r4 = r9.mConfigs;	 Catch:{ SecurityException -> 0x00bf }
        r5 = r10.user;	 Catch:{ SecurityException -> 0x00bf }
        r4.put(r5, r10);	 Catch:{ SecurityException -> 0x00bf }
        r4 = DEBUG;	 Catch:{ SecurityException -> 0x00bf }
        if (r4 == 0) goto L_0x005b;
    L_0x003f:
        r4 = "ZenModeHelper";
        r5 = new java.lang.StringBuilder;	 Catch:{ SecurityException -> 0x00bf }
        r5.<init>();	 Catch:{ SecurityException -> 0x00bf }
        r6 = "setConfigLocked: store config for user ";
        r5 = r5.append(r6);	 Catch:{ SecurityException -> 0x00bf }
        r6 = r10.user;	 Catch:{ SecurityException -> 0x00bf }
        r5 = r5.append(r6);	 Catch:{ SecurityException -> 0x00bf }
        r5 = r5.toString();	 Catch:{ SecurityException -> 0x00bf }
        android.util.Log.d(r4, r5);	 Catch:{ SecurityException -> 0x00bf }
    L_0x005b:
        android.os.Binder.restoreCallingIdentity(r2);
        return r8;
    L_0x005f:
        r4 = r9.mConditions;	 Catch:{ SecurityException -> 0x00bf }
        r5 = 0;
        r4.evaluateConfig(r10, r5);	 Catch:{ SecurityException -> 0x00bf }
        r4 = r9.mConfigs;	 Catch:{ SecurityException -> 0x00bf }
        r5 = r10.user;	 Catch:{ SecurityException -> 0x00bf }
        r4.put(r5, r10);	 Catch:{ SecurityException -> 0x00bf }
        r4 = DEBUG;	 Catch:{ SecurityException -> 0x00bf }
        if (r4 == 0) goto L_0x008f;
    L_0x0070:
        r4 = "ZenModeHelper";
        r5 = new java.lang.StringBuilder;	 Catch:{ SecurityException -> 0x00bf }
        r5.<init>();	 Catch:{ SecurityException -> 0x00bf }
        r6 = "setConfigLocked reason=";
        r5 = r5.append(r6);	 Catch:{ SecurityException -> 0x00bf }
        r5 = r5.append(r11);	 Catch:{ SecurityException -> 0x00bf }
        r5 = r5.toString();	 Catch:{ SecurityException -> 0x00bf }
        r6 = new java.lang.Throwable;	 Catch:{ SecurityException -> 0x00bf }
        r6.<init>();	 Catch:{ SecurityException -> 0x00bf }
        android.util.Log.d(r4, r5, r6);	 Catch:{ SecurityException -> 0x00bf }
    L_0x008f:
        r4 = r9.mConfig;	 Catch:{ SecurityException -> 0x00bf }
        com.android.server.notification.ZenLog.traceConfig(r11, r4, r10);	 Catch:{ SecurityException -> 0x00bf }
        r4 = r9.mConfig;	 Catch:{ SecurityException -> 0x00bf }
        r4 = getNotificationPolicy(r4);	 Catch:{ SecurityException -> 0x00bf }
        r5 = getNotificationPolicy(r10);	 Catch:{ SecurityException -> 0x00bf }
        r4 = java.util.Objects.equals(r4, r5);	 Catch:{ SecurityException -> 0x00bf }
        r1 = r4 ^ 1;
        r4 = r9.mConfig;	 Catch:{ SecurityException -> 0x00bf }
        r4 = r10.equals(r4);	 Catch:{ SecurityException -> 0x00bf }
        if (r4 != 0) goto L_0x00af;
    L_0x00ac:
        r9.dispatchOnConfigChanged();	 Catch:{ SecurityException -> 0x00bf }
    L_0x00af:
        if (r1 == 0) goto L_0x00b4;
    L_0x00b1:
        r9.dispatchOnPolicyChanged();	 Catch:{ SecurityException -> 0x00bf }
    L_0x00b4:
        r9.mConfig = r10;	 Catch:{ SecurityException -> 0x00bf }
        r4 = r9.mHandler;	 Catch:{ SecurityException -> 0x00bf }
        r4.postApplyConfig(r10, r11, r12);	 Catch:{ SecurityException -> 0x00bf }
        android.os.Binder.restoreCallingIdentity(r2);
        return r8;
    L_0x00bf:
        r0 = move-exception;
        r4 = "ZenModeHelper";
        r5 = "Invalid rule in config";
        android.util.Log.wtf(r4, r5, r0);	 Catch:{ all -> 0x00cd }
        android.os.Binder.restoreCallingIdentity(r2);
        return r7;
    L_0x00cd:
        r4 = move-exception;
        android.os.Binder.restoreCallingIdentity(r2);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.ZenModeHelper.setConfigLocked(android.service.notification.ZenModeConfig, java.lang.String, boolean):boolean");
    }

    private void applyConfig(ZenModeConfig config, String reason, boolean setRingerMode) {
        Global.putString(this.mContext.getContentResolver(), "zen_mode_config_etag", Integer.toString(config.hashCode()));
        if (!evaluateZenMode(reason, setRingerMode)) {
            applyRestrictions();
        }
        this.mConditions.evaluateConfig(config, true);
    }

    private int getZenModeSetting() {
        return Global.getInt(this.mContext.getContentResolver(), "zen_mode", 0);
    }

    private void setZenModeSetting(int zen) {
        Global.putInt(this.mContext.getContentResolver(), "zen_mode", zen);
    }

    private int getPreviousRingerModeSetting() {
        return Global.getInt(this.mContext.getContentResolver(), "zen_mode_ringer_level", 2);
    }

    private void setPreviousRingerModeSetting(Integer previousRingerLevel) {
        String str = null;
        ContentResolver contentResolver = this.mContext.getContentResolver();
        String str2 = "zen_mode_ringer_level";
        if (previousRingerLevel != null) {
            str = Integer.toString(previousRingerLevel.intValue());
        }
        Global.putString(contentResolver, str2, str);
    }

    private boolean evaluateZenMode(String reason, boolean setRingerMode) {
        if (DEBUG) {
            Log.d(TAG, "evaluateZenMode");
        }
        int zenBefore = this.mZenMode;
        int zen = computeZenMode();
        ZenLog.traceSetZenMode(zen, reason);
        this.mZenMode = zen;
        updateRingerModeAffectedStreams();
        setZenModeSetting(this.mZenMode);
        if (setRingerMode) {
            applyZenToRingerMode();
        }
        applyRestrictions();
        if (zen != zenBefore) {
            this.mHandler.postDispatchOnZenModeChanged();
        }
        return true;
    }

    private void updateRingerModeAffectedStreams() {
        if (this.mAudioManager != null) {
            this.mAudioManager.updateRingerModeAffectedStreamsInternal();
        }
    }

    private int computeZenMode() {
        synchronized (this.mConfig) {
            if (this.mConfig == null) {
                return 0;
            } else if (this.mConfig.manualRule != null) {
                int i = this.mConfig.manualRule.zenMode;
                return i;
            } else {
                int zen = 0;
                for (ZenRule automaticRule : this.mConfig.automaticRules.values()) {
                    if (automaticRule.isAutomaticActive() && zenSeverity(automaticRule.zenMode) > zenSeverity(zen)) {
                        zen = automaticRule.zenMode;
                    }
                }
                return zen;
            }
        }
    }

    private void applyRestrictions() {
        boolean zen = this.mZenMode != 0;
        boolean muteNotifications = (this.mSuppressedEffects & 1) != 0;
        boolean muteCalls = (!zen || (this.mConfig.allowCalls ^ 1) == 0 || (this.mConfig.allowRepeatCallers ^ 1) == 0) ? (this.mSuppressedEffects & 2) != 0 : true;
        boolean muteEverything = this.mZenMode == 2;
        for (int usage : AudioAttributes.SDK_USAGES) {
            int suppressionBehavior = AudioAttributes.SUPPRESSIBLE_USAGES.get(usage);
            if (suppressionBehavior == 3) {
                applyRestrictions(false, usage);
            } else if (suppressionBehavior == 1) {
                applyRestrictions(!muteNotifications ? muteEverything : true, usage);
            } else if (suppressionBehavior == 2) {
                applyRestrictions(!muteCalls ? muteEverything : true, usage);
            } else {
                applyRestrictions(muteEverything, usage);
            }
        }
    }

    private void applyRestrictions(boolean mute, int usage) {
        int i = 1;
        this.mAppOps.setRestriction(3, usage, mute ? 1 : 0, null);
        AppOpsManager appOpsManager = this.mAppOps;
        if (!mute) {
            i = 0;
        }
        appOpsManager.setRestriction(28, usage, i, null);
    }

    private void applyZenToRingerMode() {
        if (this.mAudioManager != null) {
            int ringerModeInternal = this.mAudioManager.getRingerModeInternal();
            int newRingerModeInternal = ringerModeInternal;
            switch (this.mZenMode) {
                case 0:
                case 1:
                    if (ringerModeInternal == 0) {
                        newRingerModeInternal = getPreviousRingerModeSetting();
                        setPreviousRingerModeSetting(null);
                        break;
                    }
                    break;
                case 2:
                case 3:
                    if (ringerModeInternal != 0) {
                        setPreviousRingerModeSetting(Integer.valueOf(ringerModeInternal));
                        newRingerModeInternal = 0;
                        break;
                    }
                    break;
            }
            if (newRingerModeInternal != -1) {
                this.mAudioManager.setRingerModeInternal(newRingerModeInternal, TAG);
            }
        }
    }

    private void dispatchOnConfigChanged() {
        for (Callback callback : this.mCallbacks) {
            callback.onConfigChanged();
        }
    }

    private void dispatchOnPolicyChanged() {
        for (Callback callback : this.mCallbacks) {
            callback.onPolicyChanged();
        }
    }

    private void dispatchOnZenModeChanged() {
        for (Callback callback : this.mCallbacks) {
            callback.onZenModeChanged();
        }
    }

    private ZenModeConfig readDefaultConfig(Resources resources) {
        AutoCloseable autoCloseable = null;
        try {
            autoCloseable = resources.getXml(18284550);
            while (autoCloseable.next() != 1) {
                ZenModeConfig config = ZenModeConfig.readXml(autoCloseable);
                if (config != null) {
                    return config;
                }
            }
            IoUtils.closeQuietly(autoCloseable);
        } catch (Exception e) {
            Log.w(TAG, "Error reading default zen mode config from resource", e);
        } finally {
            IoUtils.closeQuietly(autoCloseable);
        }
        return new ZenModeConfig();
    }

    private void appendDefaultScheduleRules(ZenModeConfig config) {
        if (config != null) {
            ScheduleInfo weeknights = new ScheduleInfo();
            weeknights.days = ZenModeConfig.WEEKNIGHT_DAYS;
            weeknights.startHour = 22;
            weeknights.endHour = 7;
            ZenRule rule1 = new ZenRule();
            rule1.enabled = false;
            rule1.name = this.mDefaultRuleWeeknightsName;
            rule1.conditionId = ZenModeConfig.toScheduleConditionId(weeknights);
            rule1.zenMode = 3;
            rule1.component = ScheduleConditionProvider.COMPONENT;
            rule1.id = "SCHEDULED_DEFAULT_RULE_1";
            rule1.creationTime = System.currentTimeMillis();
            config.automaticRules.put(rule1.id, rule1);
            ScheduleInfo weekends = new ScheduleInfo();
            weekends.days = ZenModeConfig.WEEKEND_DAYS;
            weekends.startHour = 23;
            weekends.startMinute = 30;
            weekends.endHour = 10;
            ZenRule rule2 = new ZenRule();
            rule2.enabled = false;
            rule2.name = this.mDefaultRuleWeekendsName;
            rule2.conditionId = ZenModeConfig.toScheduleConditionId(weekends);
            rule2.zenMode = 3;
            rule2.component = ScheduleConditionProvider.COMPONENT;
            rule2.id = "SCHEDULED_DEFAULT_RULE_2";
            rule2.creationTime = System.currentTimeMillis();
            config.automaticRules.put(rule2.id, rule2);
        }
    }

    private void appendDefaultEventRules(ZenModeConfig config) {
        if (config != null) {
            EventInfo events = new EventInfo();
            events.calendar = null;
            events.reply = 1;
            ZenRule rule = new ZenRule();
            rule.enabled = false;
            rule.name = this.mDefaultRuleEventsName;
            rule.conditionId = ZenModeConfig.toEventConditionId(events);
            rule.zenMode = 3;
            rule.component = EventConditionProvider.COMPONENT;
            rule.id = "EVENTS_DEFAULT_RULE";
            rule.creationTime = System.currentTimeMillis();
            config.automaticRules.put(rule.id, rule);
        }
    }

    private static int zenSeverity(int zen) {
        switch (zen) {
            case 1:
                return 1;
            case 2:
                return 3;
            case 3:
                return 2;
            default:
                return 0;
        }
    }
}
