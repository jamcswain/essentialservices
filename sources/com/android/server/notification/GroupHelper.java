package com.android.server.notification;

import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.util.Slog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

public class GroupHelper {
    protected static final int AUTOGROUP_AT_COUNT = 4;
    protected static final String AUTOGROUP_KEY = "ranker_group";
    private static final boolean DEBUG = Log.isLoggable(TAG, 3);
    private static final String TAG = "GroupHelper";
    private final Callback mCallback;
    Map<Integer, Map<String, LinkedHashSet<String>>> mUngroupedNotifications = new HashMap();

    protected interface Callback {
        void addAutoGroup(String str);

        void addAutoGroupSummary(int i, String str, String str2);

        void removeAutoGroup(String str);

        void removeAutoGroupSummary(int i, String str);
    }

    public GroupHelper(Callback callback) {
        this.mCallback = callback;
    }

    public void onNotificationPosted(StatusBarNotification sbn, boolean autogroupSummaryExists) {
        if (DEBUG) {
            Log.i(TAG, "POSTED " + sbn.getKey());
        }
        try {
            List<String> notificationsToGroup = new ArrayList();
            if (sbn.isAppGroup()) {
                maybeUngroup(sbn, false, sbn.getUserId());
                return;
            }
            synchronized (this.mUngroupedNotifications) {
                Map<String, LinkedHashSet<String>> ungroupedNotificationsByUser = (Map) this.mUngroupedNotifications.get(Integer.valueOf(sbn.getUserId()));
                if (ungroupedNotificationsByUser == null) {
                    ungroupedNotificationsByUser = new HashMap();
                }
                this.mUngroupedNotifications.put(Integer.valueOf(sbn.getUserId()), ungroupedNotificationsByUser);
                LinkedHashSet<String> notificationsForPackage = (LinkedHashSet) ungroupedNotificationsByUser.get(sbn.getPackageName());
                if (notificationsForPackage == null) {
                    notificationsForPackage = new LinkedHashSet();
                }
                notificationsForPackage.add(sbn.getKey());
                ungroupedNotificationsByUser.put(sbn.getPackageName(), notificationsForPackage);
                if (notificationsForPackage.size() >= 4 || autogroupSummaryExists) {
                    notificationsToGroup.addAll(notificationsForPackage);
                }
            }
            if (notificationsToGroup.size() > 0) {
                adjustAutogroupingSummary(sbn.getUserId(), sbn.getPackageName(), (String) notificationsToGroup.get(0), true);
                adjustNotificationBundling(notificationsToGroup, true);
            }
        } catch (Exception e) {
            Slog.e(TAG, "Failure processing new notification", e);
        }
    }

    public void onNotificationRemoved(StatusBarNotification sbn) {
        try {
            maybeUngroup(sbn, true, sbn.getUserId());
        } catch (Exception e) {
            Slog.e(TAG, "Error processing canceled notification", e);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void maybeUngroup(android.service.notification.StatusBarNotification r10, boolean r11, int r12) {
        /*
        r9 = this;
        r8 = 0;
        r7 = 0;
        r1 = new java.util.ArrayList;
        r1.<init>();
        r2 = 0;
        r5 = r9.mUngroupedNotifications;
        monitor-enter(r5);
        r4 = r9.mUngroupedNotifications;	 Catch:{ all -> 0x006e }
        r6 = r10.getUserId();	 Catch:{ all -> 0x006e }
        r6 = java.lang.Integer.valueOf(r6);	 Catch:{ all -> 0x006e }
        r3 = r4.get(r6);	 Catch:{ all -> 0x006e }
        r3 = (java.util.Map) r3;	 Catch:{ all -> 0x006e }
        if (r3 == 0) goto L_0x0023;
    L_0x001d:
        r4 = r3.size();	 Catch:{ all -> 0x006e }
        if (r4 != 0) goto L_0x0025;
    L_0x0023:
        monitor-exit(r5);
        return;
    L_0x0025:
        r4 = r10.getPackageName();	 Catch:{ all -> 0x006e }
        r0 = r3.get(r4);	 Catch:{ all -> 0x006e }
        r0 = (java.util.LinkedHashSet) r0;	 Catch:{ all -> 0x006e }
        if (r0 == 0) goto L_0x0037;
    L_0x0031:
        r4 = r0.size();	 Catch:{ all -> 0x006e }
        if (r4 != 0) goto L_0x0039;
    L_0x0037:
        monitor-exit(r5);
        return;
    L_0x0039:
        r4 = r10.getKey();	 Catch:{ all -> 0x006e }
        r4 = r0.remove(r4);	 Catch:{ all -> 0x006e }
        if (r4 == 0) goto L_0x004c;
    L_0x0043:
        if (r11 != 0) goto L_0x004c;
    L_0x0045:
        r4 = r10.getKey();	 Catch:{ all -> 0x006e }
        r1.add(r4);	 Catch:{ all -> 0x006e }
    L_0x004c:
        r4 = r0.size();	 Catch:{ all -> 0x006e }
        if (r4 != 0) goto L_0x005a;
    L_0x0052:
        r4 = r10.getPackageName();	 Catch:{ all -> 0x006e }
        r3.remove(r4);	 Catch:{ all -> 0x006e }
        r2 = 1;
    L_0x005a:
        monitor-exit(r5);
        if (r2 == 0) goto L_0x0064;
    L_0x005d:
        r4 = r10.getPackageName();
        r9.adjustAutogroupingSummary(r12, r4, r8, r7);
    L_0x0064:
        r4 = r1.size();
        if (r4 <= 0) goto L_0x006d;
    L_0x006a:
        r9.adjustNotificationBundling(r1, r7);
    L_0x006d:
        return;
    L_0x006e:
        r4 = move-exception;
        monitor-exit(r5);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.GroupHelper.maybeUngroup(android.service.notification.StatusBarNotification, boolean, int):void");
    }

    private void adjustAutogroupingSummary(int userId, String packageName, String triggeringKey, boolean summaryNeeded) {
        if (summaryNeeded) {
            this.mCallback.addAutoGroupSummary(userId, packageName, triggeringKey);
        } else {
            this.mCallback.removeAutoGroupSummary(userId, packageName);
        }
    }

    private void adjustNotificationBundling(List<String> keys, boolean group) {
        for (String key : keys) {
            if (DEBUG) {
                Log.i(TAG, "Sending grouping adjustment for: " + key + " group? " + group);
            }
            if (group) {
                this.mCallback.addAutoGroup(key);
            } else {
                this.mCallback.removeAutoGroup(key);
            }
        }
    }
}
