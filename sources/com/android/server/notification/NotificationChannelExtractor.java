package com.android.server.notification;

import android.content.Context;

public class NotificationChannelExtractor implements NotificationSignalExtractor {
    private static final boolean DBG = false;
    private static final String TAG = "BadgeExtractor";
    private RankingConfig mConfig;

    public void initialize(Context ctx, NotificationUsageStats usageStats) {
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.android.server.notification.RankingReconsideration process(com.android.server.notification.NotificationRecord r7) {
        /*
        r6 = this;
        r5 = 0;
        if (r7 == 0) goto L_0x0009;
    L_0x0003:
        r0 = r7.getNotification();
        if (r0 != 0) goto L_0x000a;
    L_0x0009:
        return r5;
    L_0x000a:
        r0 = r6.mConfig;
        if (r0 != 0) goto L_0x000f;
    L_0x000e:
        return r5;
    L_0x000f:
        r0 = r6.mConfig;
        r1 = r7.sbn;
        r1 = r1.getPackageName();
        r2 = r7.sbn;
        r2 = r2.getUid();
        r3 = r7.getChannel();
        r3 = r3.getId();
        r4 = 0;
        r0 = r0.getNotificationChannel(r1, r2, r3, r4);
        r7.updateNotificationChannel(r0);
        return r5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.NotificationChannelExtractor.process(com.android.server.notification.NotificationRecord):com.android.server.notification.RankingReconsideration");
    }

    public void setConfig(RankingConfig config) {
        this.mConfig = config;
    }
}
