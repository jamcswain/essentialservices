package com.android.server.notification;

import android.app.INotificationManager;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.IPackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.service.notification.Condition;
import android.service.notification.IConditionProvider;
import android.service.notification.IConditionProvider.Stub;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Slog;
import com.android.server.notification.ManagedServices.Config;
import com.android.server.notification.ManagedServices.ManagedServiceInfo;
import com.android.server.notification.ManagedServices.UserProfiles;
import com.android.server.notification.NotificationManagerService.DumpFilter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class ConditionProviders extends ManagedServices {
    static final String TAG_ENABLED_DND_APPS = "dnd_apps";
    private Callback mCallback;
    private final ArrayList<ConditionRecord> mRecords;
    private final ArraySet<String> mSystemConditionProviderNames;
    private final ArraySet<SystemConditionProviderService> mSystemConditionProviders;

    public interface Callback {
        void onBootComplete();

        void onConditionChanged(Uri uri, Condition condition);

        void onServiceAdded(ComponentName componentName);

        void onUserSwitched();
    }

    private static class ConditionRecord {
        public final ComponentName component;
        public Condition condition;
        public final Uri id;
        public ManagedServiceInfo info;
        public boolean subscribed;

        private ConditionRecord(Uri id, ComponentName component) {
            this.id = id;
            this.component = component;
        }

        public String toString() {
            return "ConditionRecord[id=" + this.id + ",component=" + this.component + ",subscribed=" + this.subscribed + ']';
        }
    }

    public ConditionProviders(Context context, UserProfiles userProfiles, IPackageManager pm) {
        super(context, new Object(), userProfiles, pm);
        this.mRecords = new ArrayList();
        this.mSystemConditionProviders = new ArraySet();
        this.mSystemConditionProviderNames = safeSet(PropConfig.getStringArray(this.mContext, "system.condition.providers", 17236030));
        this.mApprovalLevel = 0;
    }

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    public boolean isSystemProviderEnabled(String path) {
        return this.mSystemConditionProviderNames.contains(path);
    }

    public void addSystemProvider(SystemConditionProviderService service) {
        this.mSystemConditionProviders.add(service);
        service.attachBase(this.mContext);
        registerService(service.asInterface(), service.getComponent(), 0);
    }

    public Iterable<SystemConditionProviderService> getSystemProviders() {
        return this.mSystemConditionProviders;
    }

    protected Config getConfig() {
        Config c = new Config();
        c.caption = "condition provider";
        c.serviceInterface = "android.service.notification.ConditionProviderService";
        c.secureSettingName = "enabled_notification_policy_access_packages";
        c.xmlTag = TAG_ENABLED_DND_APPS;
        c.secondarySettingName = "enabled_notification_listeners";
        c.bindPermission = "android.permission.BIND_CONDITION_PROVIDER_SERVICE";
        c.settingsAction = "android.settings.ACTION_CONDITION_PROVIDER_SETTINGS";
        c.clientLabel = 17039621;
        return c;
    }

    public void dump(PrintWriter pw, DumpFilter filter) {
        super.dump(pw, filter);
        synchronized (this.mMutex) {
            int i;
            pw.print("    mRecords(");
            pw.print(this.mRecords.size());
            pw.println("):");
            for (i = 0; i < this.mRecords.size(); i++) {
                ConditionRecord r = (ConditionRecord) this.mRecords.get(i);
                if (filter == null || (filter.matches(r.component) ^ 1) == 0) {
                    pw.print("      ");
                    pw.println(r);
                    String countdownDesc = CountdownConditionProvider.tryParseDescription(r.id);
                    if (countdownDesc != null) {
                        pw.print("        (");
                        pw.print(countdownDesc);
                        pw.println(")");
                    }
                }
            }
        }
        pw.print("    mSystemConditionProviders: ");
        pw.println(this.mSystemConditionProviderNames);
        for (i = 0; i < this.mSystemConditionProviders.size(); i++) {
            ((SystemConditionProviderService) this.mSystemConditionProviders.valueAt(i)).dump(pw, filter);
        }
    }

    protected IInterface asInterface(IBinder binder) {
        return Stub.asInterface(binder);
    }

    protected boolean checkType(IInterface service) {
        return service instanceof IConditionProvider;
    }

    public void onBootPhaseAppsCanStart() {
        super.onBootPhaseAppsCanStart();
        for (int i = 0; i < this.mSystemConditionProviders.size(); i++) {
            ((SystemConditionProviderService) this.mSystemConditionProviders.valueAt(i)).onBootComplete();
        }
        if (this.mCallback != null) {
            this.mCallback.onBootComplete();
        }
    }

    public void onUserSwitched(int user) {
        super.onUserSwitched(user);
        if (this.mCallback != null) {
            this.mCallback.onUserSwitched();
        }
    }

    protected void onServiceAdded(ManagedServiceInfo info) {
        try {
            provider(info).onConnected();
        } catch (RemoteException e) {
        }
        if (this.mCallback != null) {
            this.mCallback.onServiceAdded(info.component);
        }
    }

    protected void onServiceRemovedLocked(ManagedServiceInfo removed) {
        if (removed != null) {
            for (int i = this.mRecords.size() - 1; i >= 0; i--) {
                if (((ConditionRecord) this.mRecords.get(i)).component.equals(removed.component)) {
                    this.mRecords.remove(i);
                }
            }
        }
    }

    public void onPackagesChanged(boolean removingPackage, String[] pkgList, int[] uid) {
        if (removingPackage) {
            INotificationManager inm = NotificationManager.getService();
            if (pkgList != null && pkgList.length > 0) {
                for (String pkgName : pkgList) {
                    try {
                        inm.removeAutomaticZenRules(pkgName);
                        inm.setNotificationPolicyAccessGranted(pkgName, false);
                    } catch (Exception e) {
                        Slog.e(this.TAG, "Failed to clean up rules for " + pkgName, e);
                    }
                }
            }
        }
        super.onPackagesChanged(removingPackage, pkgList, uid);
    }

    protected boolean isValidEntry(String packageOrComponent, int userId) {
        return true;
    }

    public ManagedServiceInfo checkServiceToken(IConditionProvider provider) {
        ManagedServiceInfo checkServiceTokenLocked;
        synchronized (this.mMutex) {
            checkServiceTokenLocked = checkServiceTokenLocked(provider);
        }
        return checkServiceTokenLocked;
    }

    private Condition[] removeDuplicateConditions(String pkg, Condition[] conditions) {
        if (conditions == null || conditions.length == 0) {
            return null;
        }
        int i;
        int N = conditions.length;
        ArrayMap<Uri, Condition> valid = new ArrayMap(N);
        for (i = 0; i < N; i++) {
            Uri id = conditions[i].id;
            if (valid.containsKey(id)) {
                Slog.w(this.TAG, "Ignoring condition from " + pkg + " for duplicate id: " + id);
            } else {
                valid.put(id, conditions[i]);
            }
        }
        if (valid.size() == 0) {
            return null;
        }
        if (valid.size() == N) {
            return conditions;
        }
        Condition[] rt = new Condition[valid.size()];
        for (i = 0; i < rt.length; i++) {
            rt[i] = (Condition) valid.valueAt(i);
        }
        return rt;
    }

    private ConditionRecord getRecordLocked(Uri id, ComponentName component, boolean create) {
        if (id == null || component == null) {
            return null;
        }
        int N = this.mRecords.size();
        for (int i = 0; i < N; i++) {
            ConditionRecord r = (ConditionRecord) this.mRecords.get(i);
            if (r.id.equals(id) && r.component.equals(component)) {
                return r;
            }
        }
        if (!create) {
            return null;
        }
        r = new ConditionRecord(id, component);
        this.mRecords.add(r);
        return r;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void notifyConditions(java.lang.String r10, com.android.server.notification.ManagedServices.ManagedServiceInfo r11, android.service.notification.Condition[] r12) {
        /*
        r9 = this;
        r4 = 0;
        r5 = r9.mMutex;
        monitor-enter(r5);
        r6 = r9.DEBUG;	 Catch:{ all -> 0x0074 }
        if (r6 == 0) goto L_0x0039;
    L_0x0008:
        r6 = r9.TAG;	 Catch:{ all -> 0x0074 }
        r7 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0074 }
        r7.<init>();	 Catch:{ all -> 0x0074 }
        r8 = "notifyConditions pkg=";
        r7 = r7.append(r8);	 Catch:{ all -> 0x0074 }
        r7 = r7.append(r10);	 Catch:{ all -> 0x0074 }
        r8 = " info=";
        r7 = r7.append(r8);	 Catch:{ all -> 0x0074 }
        r7 = r7.append(r11);	 Catch:{ all -> 0x0074 }
        r8 = " conditions=";
        r7 = r7.append(r8);	 Catch:{ all -> 0x0074 }
        if (r12 != 0) goto L_0x0044;
    L_0x002e:
        r4 = r7.append(r4);	 Catch:{ all -> 0x0074 }
        r4 = r4.toString();	 Catch:{ all -> 0x0074 }
        android.util.Slog.d(r6, r4);	 Catch:{ all -> 0x0074 }
    L_0x0039:
        r12 = r9.removeDuplicateConditions(r10, r12);	 Catch:{ all -> 0x0074 }
        if (r12 == 0) goto L_0x0042;
    L_0x003f:
        r4 = r12.length;	 Catch:{ all -> 0x0074 }
        if (r4 != 0) goto L_0x0049;
    L_0x0042:
        monitor-exit(r5);
        return;
    L_0x0044:
        r4 = java.util.Arrays.asList(r12);	 Catch:{ all -> 0x0074 }
        goto L_0x002e;
    L_0x0049:
        r0 = r12.length;	 Catch:{ all -> 0x0074 }
        r2 = 0;
    L_0x004b:
        if (r2 >= r0) goto L_0x005f;
    L_0x004d:
        r1 = r12[r2];	 Catch:{ all -> 0x0074 }
        r4 = r1.id;	 Catch:{ all -> 0x0074 }
        r6 = r11.component;	 Catch:{ all -> 0x0074 }
        r7 = 1;
        r3 = r9.getRecordLocked(r4, r6, r7);	 Catch:{ all -> 0x0074 }
        r3.info = r11;	 Catch:{ all -> 0x0074 }
        r3.condition = r1;	 Catch:{ all -> 0x0074 }
        r2 = r2 + 1;
        goto L_0x004b;
    L_0x005f:
        monitor-exit(r5);
        r0 = r12.length;
        r2 = 0;
    L_0x0062:
        if (r2 >= r0) goto L_0x0077;
    L_0x0064:
        r1 = r12[r2];
        r4 = r9.mCallback;
        if (r4 == 0) goto L_0x0071;
    L_0x006a:
        r4 = r9.mCallback;
        r5 = r1.id;
        r4.onConditionChanged(r5, r1);
    L_0x0071:
        r2 = r2 + 1;
        goto L_0x0062;
    L_0x0074:
        r4 = move-exception;
        monitor-exit(r5);
        throw r4;
    L_0x0077:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.ConditionProviders.notifyConditions(java.lang.String, com.android.server.notification.ManagedServices$ManagedServiceInfo, android.service.notification.Condition[]):void");
    }

    public IConditionProvider findConditionProvider(ComponentName component) {
        if (component == null) {
            return null;
        }
        for (ManagedServiceInfo service : getServices()) {
            if (component.equals(service.component)) {
                return provider(service);
            }
        }
        return null;
    }

    public Condition findCondition(ComponentName component, Uri conditionId) {
        Condition condition = null;
        if (component == null || conditionId == null) {
            return null;
        }
        synchronized (this.mMutex) {
            ConditionRecord r = getRecordLocked(conditionId, component, false);
            if (r != null) {
                condition = r.condition;
            }
        }
        return condition;
    }

    public void ensureRecordExists(ComponentName component, Uri conditionId, IConditionProvider provider) {
        ConditionRecord r = getRecordLocked(conditionId, component, true);
        if (r.info == null) {
            r.info = checkServiceTokenLocked(provider);
        }
    }

    public boolean subscribeIfNecessary(ComponentName component, Uri conditionId) {
        synchronized (this.mMutex) {
            ConditionRecord r = getRecordLocked(conditionId, component, false);
            if (r == null) {
                Slog.w(this.TAG, "Unable to subscribe to " + component + " " + conditionId);
                return false;
            } else if (r.subscribed) {
                return true;
            } else {
                subscribeLocked(r);
                boolean z = r.subscribed;
                return z;
            }
        }
    }

    public void unsubscribeIfNecessary(ComponentName component, Uri conditionId) {
        synchronized (this.mMutex) {
            ConditionRecord r = getRecordLocked(conditionId, component, false);
            if (r == null) {
                Slog.w(this.TAG, "Unable to unsubscribe to " + component + " " + conditionId);
            } else if (r.subscribed) {
                unsubscribeLocked(r);
            }
        }
    }

    private void subscribeLocked(ConditionRecord r) {
        Uri uri = null;
        if (this.DEBUG) {
            Slog.d(this.TAG, "subscribeLocked " + r);
        }
        IConditionProvider provider = provider(r);
        RemoteException re = null;
        if (provider != null) {
            try {
                Slog.d(this.TAG, "Subscribing to " + r.id + " with " + r.component);
                provider.onSubscribe(r.id);
                r.subscribed = true;
            } catch (RemoteException e) {
                Slog.w(this.TAG, "Error subscribing to " + r, e);
                re = e;
            }
        }
        if (r != null) {
            uri = r.id;
        }
        ZenLog.traceSubscribe(uri, provider, re);
    }

    @SafeVarargs
    private static <T> ArraySet<T> safeSet(T... items) {
        ArraySet<T> rt = new ArraySet();
        if (items == null || items.length == 0) {
            return rt;
        }
        for (T item : items) {
            if (item != null) {
                rt.add(item);
            }
        }
        return rt;
    }

    private void unsubscribeLocked(ConditionRecord r) {
        Uri uri = null;
        if (this.DEBUG) {
            Slog.d(this.TAG, "unsubscribeLocked " + r);
        }
        IConditionProvider provider = provider(r);
        RemoteException remoteException = null;
        if (provider != null) {
            try {
                provider.onUnsubscribe(r.id);
            } catch (RemoteException e) {
                Slog.w(this.TAG, "Error unsubscribing to " + r, e);
                remoteException = e;
            }
            r.subscribed = false;
        }
        if (r != null) {
            uri = r.id;
        }
        ZenLog.traceUnsubscribe(uri, provider, remoteException);
    }

    private static IConditionProvider provider(ConditionRecord r) {
        return r == null ? null : provider(r.info);
    }

    private static IConditionProvider provider(ManagedServiceInfo info) {
        return info == null ? null : (IConditionProvider) info.service;
    }
}
