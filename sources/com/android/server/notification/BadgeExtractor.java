package com.android.server.notification;

import android.content.Context;

public class BadgeExtractor implements NotificationSignalExtractor {
    private static final boolean DBG = false;
    private static final String TAG = "BadgeExtractor";
    private RankingConfig mConfig;

    public void initialize(Context ctx, NotificationUsageStats usageStats) {
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.android.server.notification.RankingReconsideration process(com.android.server.notification.NotificationRecord r8) {
        /*
        r7 = this;
        r2 = 0;
        r6 = 0;
        if (r8 == 0) goto L_0x000a;
    L_0x0004:
        r3 = r8.getNotification();
        if (r3 != 0) goto L_0x000b;
    L_0x000a:
        return r6;
    L_0x000b:
        r3 = r7.mConfig;
        if (r3 != 0) goto L_0x0010;
    L_0x000f:
        return r6;
    L_0x0010:
        r3 = r7.mConfig;
        r4 = r8.sbn;
        r4 = r4.getUser();
        r1 = r3.badgingEnabled(r4);
        r3 = r7.mConfig;
        r4 = r8.sbn;
        r4 = r4.getPackageName();
        r5 = r8.sbn;
        r5 = r5.getUid();
        r0 = r3.canShowBadge(r4, r5);
        if (r1 == 0) goto L_0x0034;
    L_0x0030:
        r3 = r0 ^ 1;
        if (r3 == 0) goto L_0x0038;
    L_0x0034:
        r8.setShowBadge(r2);
    L_0x0037:
        return r6;
    L_0x0038:
        r3 = r8.getChannel();
        if (r3 == 0) goto L_0x004e;
    L_0x003e:
        r3 = r8.getChannel();
        r3 = r3.canShowBadge();
        if (r3 == 0) goto L_0x004c;
    L_0x0048:
        r8.setShowBadge(r0);
        goto L_0x0037;
    L_0x004c:
        r0 = r2;
        goto L_0x0048;
    L_0x004e:
        r8.setShowBadge(r0);
        goto L_0x0037;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.BadgeExtractor.process(com.android.server.notification.NotificationRecord):com.android.server.notification.RankingReconsideration");
    }

    public void setConfig(RankingConfig config) {
        this.mConfig = config;
    }
}
