package com.android.server.notification;

import android.content.Context;

public class ImportanceExtractor implements NotificationSignalExtractor {
    private static final boolean DBG = false;
    private static final String TAG = "ImportantTopicExtractor";
    private RankingConfig mConfig;

    public void initialize(Context ctx, NotificationUsageStats usageStats) {
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.android.server.notification.RankingReconsideration process(com.android.server.notification.NotificationRecord r3) {
        /*
        r2 = this;
        r1 = 0;
        if (r3 == 0) goto L_0x0009;
    L_0x0003:
        r0 = r3.getNotification();
        if (r0 != 0) goto L_0x000a;
    L_0x0009:
        return r1;
    L_0x000a:
        r0 = r2.mConfig;
        if (r0 != 0) goto L_0x000f;
    L_0x000e:
        return r1;
    L_0x000f:
        r0 = r3.getChannel();
        r0 = r0.getImportance();
        r3.setUserImportance(r0);
        return r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.ImportanceExtractor.process(com.android.server.notification.NotificationRecord):com.android.server.notification.RankingReconsideration");
    }

    public void setConfig(RankingConfig config) {
        this.mConfig = config;
    }
}
