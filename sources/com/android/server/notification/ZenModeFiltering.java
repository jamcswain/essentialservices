package com.android.server.notification;

import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.UserHandle;
import android.provider.Settings.Secure;
import android.service.notification.ZenModeConfig;
import android.telecom.TelecomManager;
import android.util.ArrayMap;
import android.util.Slog;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Objects;

public class ZenModeFiltering {
    private static final boolean DEBUG = ZenModeHelper.DEBUG;
    static final RepeatCallers REPEAT_CALLERS = new RepeatCallers();
    private static final String TAG = "ZenModeHelper";
    private final Context mContext;
    private ComponentName mDefaultPhoneApp;

    private static class RepeatCallers {
        private final ArrayMap<String, Long> mCalls;
        private int mThresholdMinutes;

        private RepeatCallers() {
            this.mCalls = new ArrayMap();
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private synchronized void recordCall(android.content.Context r6, android.os.Bundle r7) {
            /*
            r5 = this;
            monitor-enter(r5);
            r5.setThresholdMinutes(r6);	 Catch:{ all -> 0x0028 }
            r3 = r5.mThresholdMinutes;	 Catch:{ all -> 0x0028 }
            if (r3 <= 0) goto L_0x000a;
        L_0x0008:
            if (r7 != 0) goto L_0x000c;
        L_0x000a:
            monitor-exit(r5);
            return;
        L_0x000c:
            r2 = peopleString(r7);	 Catch:{ all -> 0x0028 }
            if (r2 != 0) goto L_0x0014;
        L_0x0012:
            monitor-exit(r5);
            return;
        L_0x0014:
            r0 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x0028 }
            r3 = r5.mCalls;	 Catch:{ all -> 0x0028 }
            r5.cleanUp(r3, r0);	 Catch:{ all -> 0x0028 }
            r3 = r5.mCalls;	 Catch:{ all -> 0x0028 }
            r4 = java.lang.Long.valueOf(r0);	 Catch:{ all -> 0x0028 }
            r3.put(r2, r4);	 Catch:{ all -> 0x0028 }
            monitor-exit(r5);
            return;
        L_0x0028:
            r3 = move-exception;
            monitor-exit(r5);
            throw r3;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.ZenModeFiltering.RepeatCallers.recordCall(android.content.Context, android.os.Bundle):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private synchronized boolean isRepeat(android.content.Context r6, android.os.Bundle r7) {
            /*
            r5 = this;
            r4 = 0;
            monitor-enter(r5);
            r5.setThresholdMinutes(r6);	 Catch:{ all -> 0x0026 }
            r3 = r5.mThresholdMinutes;	 Catch:{ all -> 0x0026 }
            if (r3 <= 0) goto L_0x000b;
        L_0x0009:
            if (r7 != 0) goto L_0x000d;
        L_0x000b:
            monitor-exit(r5);
            return r4;
        L_0x000d:
            r2 = peopleString(r7);	 Catch:{ all -> 0x0026 }
            if (r2 != 0) goto L_0x0015;
        L_0x0013:
            monitor-exit(r5);
            return r4;
        L_0x0015:
            r0 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x0026 }
            r3 = r5.mCalls;	 Catch:{ all -> 0x0026 }
            r5.cleanUp(r3, r0);	 Catch:{ all -> 0x0026 }
            r3 = r5.mCalls;	 Catch:{ all -> 0x0026 }
            r3 = r3.containsKey(r2);	 Catch:{ all -> 0x0026 }
            monitor-exit(r5);
            return r3;
        L_0x0026:
            r3 = move-exception;
            monitor-exit(r5);
            throw r3;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.notification.ZenModeFiltering.RepeatCallers.isRepeat(android.content.Context, android.os.Bundle):boolean");
        }

        private synchronized void cleanUp(ArrayMap<String, Long> calls, long now) {
            for (int i = calls.size() - 1; i >= 0; i--) {
                long time = ((Long) this.mCalls.valueAt(i)).longValue();
                if (time > now || now - time > ((long) ((this.mThresholdMinutes * 1000) * 60))) {
                    calls.removeAt(i);
                }
            }
        }

        private void setThresholdMinutes(Context context) {
            if (this.mThresholdMinutes <= 0) {
                this.mThresholdMinutes = context.getResources().getInteger(17694919);
            }
        }

        private static String peopleString(Bundle extras) {
            String str = null;
            String[] extraPeople = ValidateNotificationPeople.getExtraPeople(extras);
            if (extraPeople == null || extraPeople.length == 0) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            for (String extraPerson : extraPeople) {
                String extraPerson2;
                if (extraPerson2 != null) {
                    extraPerson2 = extraPerson2.trim();
                    if (!extraPerson2.isEmpty()) {
                        if (sb.length() > 0) {
                            sb.append('|');
                        }
                        sb.append(extraPerson2);
                    }
                }
            }
            if (sb.length() != 0) {
                str = sb.toString();
            }
            return str;
        }
    }

    public ZenModeFiltering(Context context) {
        this.mContext = context;
    }

    public void dump(PrintWriter pw, String prefix) {
        pw.print(prefix);
        pw.print("mDefaultPhoneApp=");
        pw.println(this.mDefaultPhoneApp);
        pw.print(prefix);
        pw.print("RepeatCallers.mThresholdMinutes=");
        pw.println(REPEAT_CALLERS.mThresholdMinutes);
        synchronized (REPEAT_CALLERS) {
            if (!REPEAT_CALLERS.mCalls.isEmpty()) {
                pw.print(prefix);
                pw.println("RepeatCallers.mCalls=");
                for (int i = 0; i < REPEAT_CALLERS.mCalls.size(); i++) {
                    pw.print(prefix);
                    pw.print("  ");
                    pw.print((String) REPEAT_CALLERS.mCalls.keyAt(i));
                    pw.print(" at ");
                    pw.println(ts(((Long) REPEAT_CALLERS.mCalls.valueAt(i)).longValue()));
                }
            }
        }
    }

    private static String ts(long time) {
        return new Date(time) + " (" + time + ")";
    }

    public static boolean matchesCallFilter(Context context, int zen, ZenModeConfig config, UserHandle userHandle, Bundle extras, ValidateNotificationPeople validator, int contactsTimeoutMs, float timeoutAffinity) {
        if (zen == 2 || zen == 3) {
            return false;
        }
        if (zen != 1 || (config.allowRepeatCallers && REPEAT_CALLERS.isRepeat(context, extras))) {
            return true;
        }
        if (!config.allowCalls) {
            return false;
        }
        if (validator != null) {
            return audienceMatches(config.allowCallsFrom, validator.getContactAffinity(userHandle, extras, contactsTimeoutMs, timeoutAffinity));
        }
        return true;
    }

    private static Bundle extras(NotificationRecord record) {
        if (record == null || record.sbn == null || record.sbn.getNotification() == null) {
            return null;
        }
        return record.sbn.getNotification().extras;
    }

    protected void recordCall(NotificationRecord record) {
        REPEAT_CALLERS.recordCall(this.mContext, extras(record));
    }

    public boolean shouldIntercept(int zen, ZenModeConfig config, NotificationRecord record) {
        switch (zen) {
            case 1:
                if (isAlarm(record)) {
                    return false;
                }
                if (record.getPackagePriority() == 2) {
                    ZenLog.traceNotIntercepted(record, "priorityApp");
                    return false;
                } else if (isCall(record)) {
                    if (config.allowRepeatCallers && REPEAT_CALLERS.isRepeat(this.mContext, extras(record))) {
                        ZenLog.traceNotIntercepted(record, "repeatCaller");
                        return false;
                    } else if (config.allowCalls) {
                        return shouldInterceptAudience(config.allowCallsFrom, record);
                    } else {
                        ZenLog.traceIntercepted(record, "!allowCalls");
                        return true;
                    }
                } else if (isMessage(record)) {
                    if (config.allowMessages) {
                        return shouldInterceptAudience(config.allowMessagesFrom, record);
                    }
                    ZenLog.traceIntercepted(record, "!allowMessages");
                    return true;
                } else if (isEvent(record)) {
                    if (config.allowEvents) {
                        return false;
                    }
                    ZenLog.traceIntercepted(record, "!allowEvents");
                    return true;
                } else if (!isReminder(record)) {
                    ZenLog.traceIntercepted(record, "!priority");
                    return true;
                } else if (config.allowReminders) {
                    return false;
                } else {
                    ZenLog.traceIntercepted(record, "!allowReminders");
                    return true;
                }
            case 2:
                ZenLog.traceIntercepted(record, "none");
                return true;
            case 3:
                if (isAlarm(record)) {
                    return false;
                }
                ZenLog.traceIntercepted(record, "alarmsOnly");
                return true;
            default:
                return false;
        }
    }

    private static boolean shouldInterceptAudience(int source, NotificationRecord record) {
        if (audienceMatches(source, record.getContactAffinity())) {
            return false;
        }
        ZenLog.traceIntercepted(record, "!audienceMatches");
        return true;
    }

    private static boolean isAlarm(NotificationRecord record) {
        if (record.isCategory("alarm") || record.isAudioStream(4)) {
            return true;
        }
        return record.isAudioAttributesUsage(4);
    }

    private static boolean isEvent(NotificationRecord record) {
        return record.isCategory("event");
    }

    private static boolean isReminder(NotificationRecord record) {
        return record.isCategory("reminder");
    }

    public boolean isCall(NotificationRecord record) {
        if (record == null) {
            return false;
        }
        if (isDefaultPhoneApp(record.sbn.getPackageName())) {
            return true;
        }
        return record.isCategory("call");
    }

    private boolean isDefaultPhoneApp(String pkg) {
        ComponentName componentName = null;
        if (this.mDefaultPhoneApp == null) {
            TelecomManager telecomm = (TelecomManager) this.mContext.getSystemService("telecom");
            if (telecomm != null) {
                componentName = telecomm.getDefaultPhoneApp();
            }
            this.mDefaultPhoneApp = componentName;
            if (DEBUG) {
                Slog.d(TAG, "Default phone app: " + this.mDefaultPhoneApp);
            }
        }
        if (pkg == null || this.mDefaultPhoneApp == null) {
            return false;
        }
        return pkg.equals(this.mDefaultPhoneApp.getPackageName());
    }

    private boolean isDefaultMessagingApp(NotificationRecord record) {
        int userId = record.getUserId();
        if (userId == -10000 || userId == -1) {
            return false;
        }
        return Objects.equals(Secure.getStringForUser(this.mContext.getContentResolver(), "sms_default_application", userId), record.sbn.getPackageName());
    }

    private boolean isMessage(NotificationRecord record) {
        return !record.isCategory("msg") ? isDefaultMessagingApp(record) : true;
    }

    private static boolean audienceMatches(int source, float contactAffinity) {
        boolean z = true;
        switch (source) {
            case 0:
                return true;
            case 1:
                if (contactAffinity < 0.5f) {
                    z = false;
                }
                return z;
            case 2:
                if (contactAffinity < 1.0f) {
                    z = false;
                }
                return z;
            default:
                Slog.w(TAG, "Encountered unknown source: " + source);
                return true;
        }
    }
}
