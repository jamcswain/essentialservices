package com.android.server.lights;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Trace;
import android.provider.Settings.Secure;
import android.util.Slog;
import com.android.server.SystemService;
import com.android.server.usb.UsbAudioDevice;

public class LightsService extends SystemService {
    static final boolean DEBUG = false;
    static final String TAG = "LightsService";
    private Handler mH = new Handler() {
        public void handleMessage(Message msg) {
            msg.obj.stopFlashing();
        }
    };
    final LightImpl[] mLights = new LightImpl[8];
    private final LightsManager mService = new LightsManager() {
        public Light getLight(int id) {
            if (id < 0 || id >= 8) {
                return null;
            }
            return LightsService.this.mLights[id];
        }
    };

    private final class LightImpl extends Light {
        private int mBrightnessMode;
        private int mColor;
        private boolean mFlashing;
        private int mId;
        private boolean mInitialized;
        private int mLastBrightnessMode;
        private int mLastColor;
        private int mMode;
        private int mOffMS;
        private int mOnMS;
        private boolean mUseLowPersistenceForVR;
        private boolean mVrModeEnabled;

        private LightImpl(int id) {
            this.mId = id;
        }

        public void setBrightness(int brightness) {
            setBrightness(brightness, 0);
        }

        public void setBrightness(int brightness, int brightnessMode) {
            synchronized (this) {
                if (brightnessMode == 2) {
                    Slog.w(LightsService.TAG, "setBrightness with LOW_PERSISTENCE unexpected #" + this.mId + ": brightness=0x" + Integer.toHexString(brightness));
                    return;
                }
                int color = brightness & 255;
                setLightLocked(color | (((color << 16) | UsbAudioDevice.kAudioDeviceMetaMask) | (color << 8)), 0, 0, 0, brightnessMode);
            }
        }

        public void setColor(int color) {
            synchronized (this) {
                setLightLocked(color, 0, 0, 0, 0);
            }
        }

        public void setFlashing(int color, int mode, int onMS, int offMS) {
            synchronized (this) {
                setLightLocked(color, mode, onMS, offMS, 0);
            }
        }

        public void pulse() {
            pulse(UsbAudioDevice.kAudioDeviceClassMask, 7);
        }

        public void pulse(int color, int onMS) {
            synchronized (this) {
                if (this.mColor == 0 && (this.mFlashing ^ 1) != 0) {
                    setLightLocked(color, 2, onMS, 1000, 0);
                    this.mColor = 0;
                    LightsService.this.mH.sendMessageDelayed(Message.obtain(LightsService.this.mH, 1, this), (long) onMS);
                }
            }
        }

        public void turnOff() {
            synchronized (this) {
                setLightLocked(0, 0, 0, 0, 0);
            }
        }

        public void setVrMode(boolean enabled) {
            boolean z = false;
            synchronized (this) {
                if (this.mVrModeEnabled != enabled) {
                    this.mVrModeEnabled = enabled;
                    if (LightsService.this.getVrDisplayMode() == 0) {
                        z = true;
                    }
                    this.mUseLowPersistenceForVR = z;
                    if (shouldBeInLowPersistenceMode()) {
                        this.mLastBrightnessMode = this.mBrightnessMode;
                    }
                }
            }
        }

        private void stopFlashing() {
            synchronized (this) {
                setLightLocked(this.mColor, 0, 0, 0, 0);
            }
        }

        private void setLightLocked(int color, int mode, int onMS, int offMS, int brightnessMode) {
            if (shouldBeInLowPersistenceMode()) {
                brightnessMode = 2;
            } else if (brightnessMode == 2) {
                brightnessMode = this.mLastBrightnessMode;
            }
            if (this.mInitialized && color == this.mColor && mode == this.mMode && onMS == this.mOnMS && offMS == this.mOffMS) {
                if (this.mBrightnessMode == brightnessMode) {
                    return;
                }
            }
            this.mInitialized = true;
            this.mLastColor = this.mColor;
            this.mColor = color;
            this.mMode = mode;
            this.mOnMS = onMS;
            this.mOffMS = offMS;
            this.mBrightnessMode = brightnessMode;
            Trace.traceBegin(131072, "setLight(" + this.mId + ", 0x" + Integer.toHexString(color) + ")");
            try {
                LightsService.setLight_native(this.mId, color, mode, onMS, offMS, brightnessMode);
            } finally {
                Trace.traceEnd(131072);
            }
        }

        private boolean shouldBeInLowPersistenceMode() {
            return this.mVrModeEnabled ? this.mUseLowPersistenceForVR : false;
        }
    }

    static native void setLight_native(int i, int i2, int i3, int i4, int i5, int i6);

    public LightsService(Context context) {
        super(context);
        for (int i = 0; i < 8; i++) {
            this.mLights[i] = new LightImpl(i);
        }
    }

    public void onStart() {
        publishLocalService(LightsManager.class, this.mService);
    }

    public void onBootPhase(int phase) {
    }

    private int getVrDisplayMode() {
        return Secure.getIntForUser(getContext().getContentResolver(), "vr_display_mode", 0, ActivityManager.getCurrentUser());
    }
}
