package com.android.server.firewall;

import android.app.AppGlobals;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Slog;
import com.android.internal.util.ArrayUtils;
import com.android.server.EventLogTags;
import com.android.server.IntentResolver;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class IntentFirewall {
    private static final int LOG_PACKAGES_MAX_LENGTH = 150;
    private static final int LOG_PACKAGES_SUFFICIENT_LENGTH = 125;
    private static final File RULES_DIR = new File(Environment.getDataSystemDirectory(), "ifw");
    static final String TAG = "IntentFirewall";
    private static final String TAG_ACTIVITY = "activity";
    private static final String TAG_BROADCAST = "broadcast";
    private static final String TAG_RULES = "rules";
    private static final String TAG_SERVICE = "service";
    private static final int TYPE_ACTIVITY = 0;
    private static final int TYPE_BROADCAST = 1;
    private static final int TYPE_SERVICE = 2;
    private static final HashMap<String, FilterFactory> factoryMap;
    private FirewallIntentResolver mActivityResolver = new FirewallIntentResolver();
    private final AMSInterface mAms;
    private FirewallIntentResolver mBroadcastResolver = new FirewallIntentResolver();
    final FirewallHandler mHandler;
    private final RuleObserver mObserver;
    private FirewallIntentResolver mServiceResolver = new FirewallIntentResolver();

    public interface AMSInterface {
        int checkComponentPermission(String str, int i, int i2, int i3, boolean z);

        Object getAMSLock();
    }

    private final class FirewallHandler extends Handler {
        public FirewallHandler(Looper looper) {
            super(looper, null, true);
        }

        public void handleMessage(Message msg) {
            IntentFirewall.this.readRulesDir(IntentFirewall.getRulesDir());
        }
    }

    private static class FirewallIntentResolver extends IntentResolver<FirewallIntentFilter, Rule> {
        private final ArrayMap<ComponentName, Rule[]> mRulesByComponent;

        private FirewallIntentResolver() {
            this.mRulesByComponent = new ArrayMap(0);
        }

        protected boolean allowFilterResult(FirewallIntentFilter filter, List<Rule> dest) {
            return dest.contains(filter.rule) ^ 1;
        }

        protected boolean isPackageForFilter(String packageName, FirewallIntentFilter filter) {
            return true;
        }

        protected FirewallIntentFilter[] newArray(int size) {
            return new FirewallIntentFilter[size];
        }

        protected Rule newResult(FirewallIntentFilter filter, int match, int userId) {
            return filter.rule;
        }

        protected void sortResults(List<Rule> list) {
        }

        public void queryByComponent(ComponentName componentName, List<Rule> candidateRules) {
            Rule[] rules = (Rule[]) this.mRulesByComponent.get(componentName);
            if (rules != null) {
                candidateRules.addAll(Arrays.asList(rules));
            }
        }

        public void addComponentFilter(ComponentName componentName, Rule rule) {
            this.mRulesByComponent.put(componentName, (Rule[]) ArrayUtils.appendElement(Rule.class, (Rule[]) this.mRulesByComponent.get(componentName), rule));
        }
    }

    private class RuleObserver extends FileObserver {
        private static final int MONITORED_EVENTS = 968;

        public RuleObserver(File monitoredDir) {
            super(monitoredDir.getAbsolutePath(), MONITORED_EVENTS);
        }

        public void onEvent(int event, String path) {
            if (path.endsWith(".xml")) {
                IntentFirewall.this.mHandler.removeMessages(0);
                IntentFirewall.this.mHandler.sendEmptyMessageDelayed(0, 250);
            }
        }
    }

    private static class FirewallIntentFilter extends IntentFilter {
        private final Rule rule;

        public FirewallIntentFilter(Rule rule) {
            this.rule = rule;
        }
    }

    private static class Rule extends AndFilter {
        private static final String ATTR_BLOCK = "block";
        private static final String ATTR_LOG = "log";
        private static final String ATTR_NAME = "name";
        private static final String TAG_COMPONENT_FILTER = "component-filter";
        private static final String TAG_INTENT_FILTER = "intent-filter";
        private boolean block;
        private boolean log;
        private final ArrayList<ComponentName> mComponentFilters;
        private final ArrayList<FirewallIntentFilter> mIntentFilters;

        private Rule() {
            this.mIntentFilters = new ArrayList(1);
            this.mComponentFilters = new ArrayList(0);
        }

        public Rule readFromXml(XmlPullParser parser) throws IOException, XmlPullParserException {
            this.block = Boolean.parseBoolean(parser.getAttributeValue(null, ATTR_BLOCK));
            this.log = Boolean.parseBoolean(parser.getAttributeValue(null, ATTR_LOG));
            super.readFromXml(parser);
            return this;
        }

        protected void readChild(XmlPullParser parser) throws IOException, XmlPullParserException {
            String currentTag = parser.getName();
            if (currentTag.equals(TAG_INTENT_FILTER)) {
                FirewallIntentFilter intentFilter = new FirewallIntentFilter(this);
                intentFilter.readFromXml(parser);
                this.mIntentFilters.add(intentFilter);
            } else if (currentTag.equals(TAG_COMPONENT_FILTER)) {
                String componentStr = parser.getAttributeValue(null, ATTR_NAME);
                if (componentStr == null) {
                    throw new XmlPullParserException("Component name must be specified.", parser, null);
                }
                ComponentName componentName = ComponentName.unflattenFromString(componentStr);
                if (componentName == null) {
                    throw new XmlPullParserException("Invalid component name: " + componentStr);
                }
                this.mComponentFilters.add(componentName);
            } else {
                super.readChild(parser);
            }
        }

        public int getIntentFilterCount() {
            return this.mIntentFilters.size();
        }

        public FirewallIntentFilter getIntentFilter(int index) {
            return (FirewallIntentFilter) this.mIntentFilters.get(index);
        }

        public int getComponentFilterCount() {
            return this.mComponentFilters.size();
        }

        public ComponentName getComponentFilter(int index) {
            return (ComponentName) this.mComponentFilters.get(index);
        }

        public boolean getBlock() {
            return this.block;
        }

        public boolean getLog() {
            return this.log;
        }
    }

    static {
        FilterFactory[] factories = new FilterFactory[]{AndFilter.FACTORY, OrFilter.FACTORY, NotFilter.FACTORY, StringFilter.ACTION, StringFilter.COMPONENT, StringFilter.COMPONENT_NAME, StringFilter.COMPONENT_PACKAGE, StringFilter.DATA, StringFilter.HOST, StringFilter.MIME_TYPE, StringFilter.SCHEME, StringFilter.PATH, StringFilter.SSP, CategoryFilter.FACTORY, SenderFilter.FACTORY, SenderPackageFilter.FACTORY, SenderPermissionFilter.FACTORY, PortFilter.FACTORY};
        factoryMap = new HashMap((factories.length * 4) / 3);
        for (FilterFactory factory : factories) {
            factoryMap.put(factory.getTagName(), factory);
        }
    }

    public IntentFirewall(AMSInterface ams, Handler handler) {
        this.mAms = ams;
        this.mHandler = new FirewallHandler(handler.getLooper());
        File rulesDir = getRulesDir();
        rulesDir.mkdirs();
        readRulesDir(rulesDir);
        this.mObserver = new RuleObserver(rulesDir);
        this.mObserver.startWatching();
    }

    public boolean checkStartActivity(Intent intent, int callerUid, int callerPid, String resolvedType, ApplicationInfo resolvedApp) {
        return checkIntent(this.mActivityResolver, intent.getComponent(), 0, intent, callerUid, callerPid, resolvedType, resolvedApp.uid);
    }

    public boolean checkService(ComponentName resolvedService, Intent intent, int callerUid, int callerPid, String resolvedType, ApplicationInfo resolvedApp) {
        return checkIntent(this.mServiceResolver, resolvedService, 2, intent, callerUid, callerPid, resolvedType, resolvedApp.uid);
    }

    public boolean checkBroadcast(Intent intent, int callerUid, int callerPid, String resolvedType, int receivingUid) {
        return checkIntent(this.mBroadcastResolver, intent.getComponent(), 1, intent, callerUid, callerPid, resolvedType, receivingUid);
    }

    public boolean checkIntent(FirewallIntentResolver resolver, ComponentName resolvedComponent, int intentType, Intent intent, int callerUid, int callerPid, String resolvedType, int receivingUid) {
        int log = 0;
        int block = 0;
        List<Rule> candidateRules = resolver.queryIntent(intent, resolvedType, false, 0);
        if (candidateRules == null) {
            candidateRules = new ArrayList();
        }
        resolver.queryByComponent(resolvedComponent, candidateRules);
        for (int i = 0; i < candidateRules.size(); i++) {
            Rule rule = (Rule) candidateRules.get(i);
            if (rule.matches(this, resolvedComponent, intent, callerUid, callerPid, resolvedType, receivingUid)) {
                block |= rule.getBlock();
                log |= rule.getLog();
                if (!(block == 0 || log == 0)) {
                    break;
                }
            }
        }
        if (log) {
            logIntent(intentType, intent, callerUid, resolvedType);
        }
        return block ^ 1;
    }

    private static void logIntent(int intentType, Intent intent, int callerUid, String resolvedType) {
        ComponentName cn = intent.getComponent();
        String shortComponent = null;
        if (cn != null) {
            shortComponent = cn.flattenToShortString();
        }
        String callerPackages = null;
        int callerPackageCount = 0;
        IPackageManager pm = AppGlobals.getPackageManager();
        if (pm != null) {
            try {
                String[] callerPackagesArray = pm.getPackagesForUid(callerUid);
                if (callerPackagesArray != null) {
                    callerPackageCount = callerPackagesArray.length;
                    callerPackages = joinPackages(callerPackagesArray);
                }
            } catch (RemoteException ex) {
                Slog.e(TAG, "Remote exception while retrieving packages", ex);
            }
        }
        EventLogTags.writeIfwIntentMatched(intentType, shortComponent, callerUid, callerPackageCount, callerPackages, intent.getAction(), resolvedType, intent.getDataString(), intent.getFlags());
    }

    private static String joinPackages(String[] packages) {
        boolean first = true;
        StringBuilder sb = new StringBuilder();
        for (String pkg : packages) {
            String pkg2;
            if ((sb.length() + pkg2.length()) + 1 < LOG_PACKAGES_MAX_LENGTH) {
                if (first) {
                    first = false;
                } else {
                    sb.append(',');
                }
                sb.append(pkg2);
            } else if (sb.length() >= LOG_PACKAGES_SUFFICIENT_LENGTH) {
                return sb.toString();
            }
        }
        if (sb.length() != 0 || packages.length <= 0) {
            return null;
        }
        pkg2 = packages[0];
        return pkg2.substring((pkg2.length() - 150) + 1) + '-';
    }

    public static File getRulesDir() {
        return RULES_DIR;
    }

    private void readRulesDir(File rulesDir) {
        FirewallIntentResolver[] resolvers = new FirewallIntentResolver[3];
        for (int i = 0; i < resolvers.length; i++) {
            resolvers[i] = new FirewallIntentResolver();
        }
        File[] files = rulesDir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.getName().endsWith(".xml")) {
                    readRules(file, resolvers);
                }
            }
        }
        Slog.i(TAG, "Read new rules (A:" + resolvers[0].filterSet().size() + " B:" + resolvers[1].filterSet().size() + " S:" + resolvers[2].filterSet().size() + ")");
        synchronized (this.mAms.getAMSLock()) {
            this.mActivityResolver = resolvers[0];
            this.mBroadcastResolver = resolvers[1];
            this.mServiceResolver = resolvers[2];
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void readRules(java.io.File r21, com.android.server.firewall.IntentFirewall.FirewallIntentResolver[] r22) {
        /*
        r20 = this;
        r14 = new java.util.ArrayList;
        r16 = 3;
        r0 = r16;
        r14.<init>(r0);
        r6 = 0;
    L_0x000a:
        r16 = 3;
        r0 = r16;
        if (r6 >= r0) goto L_0x001d;
    L_0x0010:
        r16 = new java.util.ArrayList;
        r16.<init>();
        r0 = r16;
        r14.add(r0);
        r6 = r6 + 1;
        goto L_0x000a;
    L_0x001d:
        r5 = new java.io.FileInputStream;	 Catch:{ FileNotFoundException -> 0x0093 }
        r0 = r21;
        r5.<init>(r0);	 Catch:{ FileNotFoundException -> 0x0093 }
        r8 = android.util.Xml.newPullParser();	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r16 = 0;
        r0 = r16;
        r8.setInput(r5, r0);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r16 = "rules";
        r0 = r16;
        com.android.internal.util.XmlUtils.beginDocument(r8, r0);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r7 = r8.getDepth();	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
    L_0x003b:
        r16 = com.android.internal.util.XmlUtils.nextElementWithin(r8, r7);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        if (r16 == 0) goto L_0x00f7;
    L_0x0041:
        r12 = -1;
        r15 = r8.getName();	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r16 = "activity";
        r16 = r15.equals(r16);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        if (r16 == 0) goto L_0x0095;
    L_0x004f:
        r12 = 0;
    L_0x0050:
        r16 = -1;
        r0 = r16;
        if (r12 == r0) goto L_0x003b;
    L_0x0056:
        r10 = new com.android.server.firewall.IntentFirewall$Rule;	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r16 = 0;
        r0 = r16;
        r10.<init>();	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r13 = r14.get(r12);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r13 = (java.util.List) r13;	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r10.readFromXml(r8);	 Catch:{ XmlPullParserException -> 0x00ab, IOException -> 0x00d0 }
        r13.add(r10);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        goto L_0x003b;
    L_0x006c:
        r4 = move-exception;
        r16 = "IntentFirewall";
        r17 = new java.lang.StringBuilder;	 Catch:{ all -> 0x019c }
        r17.<init>();	 Catch:{ all -> 0x019c }
        r18 = "Error reading intent firewall rules from ";
        r17 = r17.append(r18);	 Catch:{ all -> 0x019c }
        r0 = r17;
        r1 = r21;
        r17 = r0.append(r1);	 Catch:{ all -> 0x019c }
        r17 = r17.toString();	 Catch:{ all -> 0x019c }
        r0 = r16;
        r1 = r17;
        android.util.Slog.e(r0, r1, r4);	 Catch:{ all -> 0x019c }
        r5.close();	 Catch:{ IOException -> 0x0177 }
    L_0x0092:
        return;
    L_0x0093:
        r2 = move-exception;
        return;
    L_0x0095:
        r16 = "broadcast";
        r16 = r15.equals(r16);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        if (r16 == 0) goto L_0x00a0;
    L_0x009e:
        r12 = 1;
        goto L_0x0050;
    L_0x00a0:
        r16 = "service";
        r16 = r15.equals(r16);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        if (r16 == 0) goto L_0x0050;
    L_0x00a9:
        r12 = 2;
        goto L_0x0050;
    L_0x00ab:
        r4 = move-exception;
        r16 = "IntentFirewall";
        r17 = new java.lang.StringBuilder;	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r17.<init>();	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r18 = "Error reading an intent firewall rule from ";
        r17 = r17.append(r18);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r0 = r17;
        r1 = r21;
        r17 = r0.append(r1);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r17 = r17.toString();	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        r0 = r16;
        r1 = r17;
        android.util.Slog.e(r0, r1, r4);	 Catch:{ XmlPullParserException -> 0x006c, IOException -> 0x00d0 }
        goto L_0x003b;
    L_0x00d0:
        r3 = move-exception;
        r16 = "IntentFirewall";
        r17 = new java.lang.StringBuilder;	 Catch:{ all -> 0x019c }
        r17.<init>();	 Catch:{ all -> 0x019c }
        r18 = "Error reading intent firewall rules from ";
        r17 = r17.append(r18);	 Catch:{ all -> 0x019c }
        r0 = r17;
        r1 = r21;
        r17 = r0.append(r1);	 Catch:{ all -> 0x019c }
        r17 = r17.toString();	 Catch:{ all -> 0x019c }
        r0 = r16;
        r1 = r17;
        android.util.Slog.e(r0, r1, r3);	 Catch:{ all -> 0x019c }
        r5.close();	 Catch:{ IOException -> 0x0153 }
    L_0x00f6:
        return;
    L_0x00f7:
        r5.close();	 Catch:{ IOException -> 0x012f }
    L_0x00fa:
        r12 = 0;
    L_0x00fb:
        r16 = r14.size();
        r0 = r16;
        if (r12 >= r0) goto L_0x01e2;
    L_0x0103:
        r13 = r14.get(r12);
        r13 = (java.util.List) r13;
        r9 = r22[r12];
        r11 = 0;
    L_0x010c:
        r16 = r13.size();
        r0 = r16;
        if (r11 >= r0) goto L_0x01de;
    L_0x0114:
        r10 = r13.get(r11);
        r10 = (com.android.server.firewall.IntentFirewall.Rule) r10;
        r6 = 0;
    L_0x011b:
        r16 = r10.getIntentFilterCount();
        r0 = r16;
        if (r6 >= r0) goto L_0x01c5;
    L_0x0123:
        r16 = r10.getIntentFilter(r6);
        r0 = r16;
        r9.addFilter(r0);
        r6 = r6 + 1;
        goto L_0x011b;
    L_0x012f:
        r3 = move-exception;
        r16 = "IntentFirewall";
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r18 = "Error while closing ";
        r17 = r17.append(r18);
        r0 = r17;
        r1 = r21;
        r17 = r0.append(r1);
        r17 = r17.toString();
        r0 = r16;
        r1 = r17;
        android.util.Slog.e(r0, r1, r3);
        goto L_0x00fa;
    L_0x0153:
        r3 = move-exception;
        r16 = "IntentFirewall";
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r18 = "Error while closing ";
        r17 = r17.append(r18);
        r0 = r17;
        r1 = r21;
        r17 = r0.append(r1);
        r17 = r17.toString();
        r0 = r16;
        r1 = r17;
        android.util.Slog.e(r0, r1, r3);
        goto L_0x00f6;
    L_0x0177:
        r3 = move-exception;
        r16 = "IntentFirewall";
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r18 = "Error while closing ";
        r17 = r17.append(r18);
        r0 = r17;
        r1 = r21;
        r17 = r0.append(r1);
        r17 = r17.toString();
        r0 = r16;
        r1 = r17;
        android.util.Slog.e(r0, r1, r3);
        goto L_0x0092;
    L_0x019c:
        r16 = move-exception;
        r5.close();	 Catch:{ IOException -> 0x01a1 }
    L_0x01a0:
        throw r16;
    L_0x01a1:
        r3 = move-exception;
        r17 = "IntentFirewall";
        r18 = new java.lang.StringBuilder;
        r18.<init>();
        r19 = "Error while closing ";
        r18 = r18.append(r19);
        r0 = r18;
        r1 = r21;
        r18 = r0.append(r1);
        r18 = r18.toString();
        r0 = r17;
        r1 = r18;
        android.util.Slog.e(r0, r1, r3);
        goto L_0x01a0;
    L_0x01c5:
        r6 = 0;
    L_0x01c6:
        r16 = r10.getComponentFilterCount();
        r0 = r16;
        if (r6 >= r0) goto L_0x01da;
    L_0x01ce:
        r16 = r10.getComponentFilter(r6);
        r0 = r16;
        r9.addComponentFilter(r0, r10);
        r6 = r6 + 1;
        goto L_0x01c6;
    L_0x01da:
        r11 = r11 + 1;
        goto L_0x010c;
    L_0x01de:
        r12 = r12 + 1;
        goto L_0x00fb;
    L_0x01e2:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.firewall.IntentFirewall.readRules(java.io.File, com.android.server.firewall.IntentFirewall$FirewallIntentResolver[]):void");
    }

    static Filter parseFilter(XmlPullParser parser) throws IOException, XmlPullParserException {
        String elementName = parser.getName();
        FilterFactory factory = (FilterFactory) factoryMap.get(elementName);
        if (factory != null) {
            return factory.newFilter(parser);
        }
        throw new XmlPullParserException("Unknown element in filter list: " + elementName);
    }

    boolean checkComponentPermission(String permission, int pid, int uid, int owningUid, boolean exported) {
        return this.mAms.checkComponentPermission(permission, pid, uid, owningUid, exported) == 0;
    }

    boolean signaturesMatch(int uid1, int uid2) {
        boolean z = false;
        try {
            if (AppGlobals.getPackageManager().checkUidSignatures(uid1, uid2) == 0) {
                z = true;
            }
            return z;
        } catch (RemoteException ex) {
            Slog.e(TAG, "Remote exception while checking signatures", ex);
            return false;
        }
    }
}
