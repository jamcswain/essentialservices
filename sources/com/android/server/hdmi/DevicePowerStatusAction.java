package com.android.server.hdmi;

import android.hardware.hdmi.IHdmiControlCallback;
import android.os.RemoteException;
import android.util.Slog;
import java.util.ArrayList;
import java.util.List;

final class DevicePowerStatusAction extends HdmiCecFeatureAction {
    private static final int STATE_WAITING_FOR_REPORT_POWER_STATUS = 1;
    private static final String TAG = "DevicePowerStatusAction";
    private final List<IHdmiControlCallback> mCallbacks = new ArrayList();
    private final int mTargetAddress;

    static DevicePowerStatusAction create(HdmiCecLocalDevice source, int targetAddress, IHdmiControlCallback callback) {
        if (source != null && callback != null) {
            return new DevicePowerStatusAction(source, targetAddress, callback);
        }
        Slog.e(TAG, "Wrong arguments");
        return null;
    }

    private DevicePowerStatusAction(HdmiCecLocalDevice localDevice, int targetAddress, IHdmiControlCallback callback) {
        super(localDevice);
        this.mTargetAddress = targetAddress;
        addCallback(callback);
    }

    boolean start() {
        queryDevicePowerStatus();
        this.mState = 1;
        addTimer(this.mState, 2000);
        return true;
    }

    private void queryDevicePowerStatus() {
        sendCommand(HdmiCecMessageBuilder.buildGiveDevicePowerStatus(getSourceAddress(), this.mTargetAddress));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    boolean processCommand(com.android.server.hdmi.HdmiCecMessage r6) {
        /*
        r5 = this;
        r4 = 1;
        r3 = 0;
        r1 = r5.mState;
        if (r1 != r4) goto L_0x000e;
    L_0x0006:
        r1 = r5.mTargetAddress;
        r2 = r6.getSource();
        if (r1 == r2) goto L_0x000f;
    L_0x000e:
        return r3;
    L_0x000f:
        r1 = r6.getOpcode();
        r2 = 144; // 0x90 float:2.02E-43 double:7.1E-322;
        if (r1 != r2) goto L_0x0024;
    L_0x0017:
        r1 = r6.getParams();
        r0 = r1[r3];
        r5.invokeCallback(r0);
        r5.finish();
        return r4;
    L_0x0024:
        return r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.hdmi.DevicePowerStatusAction.processCommand(com.android.server.hdmi.HdmiCecMessage):boolean");
    }

    void handleTimerEvent(int state) {
        if (this.mState == state && state == 1) {
            invokeCallback(-1);
            finish();
        }
    }

    public void addCallback(IHdmiControlCallback callback) {
        this.mCallbacks.add(callback);
    }

    private void invokeCallback(int result) {
        try {
            for (IHdmiControlCallback callback : this.mCallbacks) {
                callback.onComplete(result);
            }
        } catch (RemoteException e) {
            Slog.e(TAG, "Callback failed:" + e);
        }
    }
}
