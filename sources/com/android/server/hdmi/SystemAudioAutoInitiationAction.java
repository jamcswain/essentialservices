package com.android.server.hdmi;

final class SystemAudioAutoInitiationAction extends HdmiCecFeatureAction {
    private static final int STATE_WAITING_FOR_SYSTEM_AUDIO_MODE_STATUS = 1;
    private final int mAvrAddress;

    SystemAudioAutoInitiationAction(HdmiCecLocalDevice source, int avrAddress) {
        super(source);
        this.mAvrAddress = avrAddress;
    }

    boolean start() {
        this.mState = 1;
        addTimer(this.mState, 2000);
        sendGiveSystemAudioModeStatus();
        return true;
    }

    private void sendGiveSystemAudioModeStatus() {
        sendCommand(HdmiCecMessageBuilder.buildGiveSystemAudioModeStatus(getSourceAddress(), this.mAvrAddress), new SendMessageCallback() {
            public void onSendCompleted(int error) {
                if (error != 0) {
                    SystemAudioAutoInitiationAction.this.tv().setSystemAudioMode(false);
                    SystemAudioAutoInitiationAction.this.finish();
                }
            }
        });
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    boolean processCommand(com.android.server.hdmi.HdmiCecMessage r5) {
        /*
        r4 = this;
        r3 = 1;
        r2 = 0;
        r0 = r4.mState;
        if (r0 != r3) goto L_0x000e;
    L_0x0006:
        r0 = r4.mAvrAddress;
        r1 = r5.getSource();
        if (r0 == r1) goto L_0x000f;
    L_0x000e:
        return r2;
    L_0x000f:
        r0 = r5.getOpcode();
        r1 = 126; // 0x7e float:1.77E-43 double:6.23E-322;
        if (r0 != r1) goto L_0x001f;
    L_0x0017:
        r0 = com.android.server.hdmi.HdmiUtils.parseCommandParamSystemAudioStatus(r5);
        r4.handleSystemAudioModeStatusMessage(r0);
        return r3;
    L_0x001f:
        return r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.hdmi.SystemAudioAutoInitiationAction.processCommand(com.android.server.hdmi.HdmiCecMessage):boolean");
    }

    private void handleSystemAudioModeStatusMessage(boolean currentSystemAudioMode) {
        if (canChangeSystemAudio()) {
            boolean targetSystemAudioMode = tv().isSystemAudioControlFeatureEnabled();
            if (currentSystemAudioMode != targetSystemAudioMode) {
                addAndStartAction(new SystemAudioActionFromTv(tv(), this.mAvrAddress, targetSystemAudioMode, null));
            } else {
                tv().setSystemAudioMode(targetSystemAudioMode);
            }
            finish();
            return;
        }
        HdmiLogger.debug("Cannot change system audio mode in auto initiation action.", new Object[0]);
        finish();
    }

    void handleTimerEvent(int state) {
        if (this.mState == state) {
            switch (this.mState) {
                case 1:
                    handleSystemAudioModeStatusTimeout();
                    break;
            }
        }
    }

    private void handleSystemAudioModeStatusTimeout() {
        if (canChangeSystemAudio()) {
            addAndStartAction(new SystemAudioActionFromTv(tv(), this.mAvrAddress, tv().isSystemAudioControlFeatureEnabled(), null));
            finish();
            return;
        }
        HdmiLogger.debug("Cannot change system audio mode in auto initiation action.", new Object[0]);
        finish();
    }

    private boolean canChangeSystemAudio() {
        int i;
        if (tv().hasAction(SystemAudioActionFromTv.class)) {
            i = 1;
        } else {
            i = tv().hasAction(SystemAudioActionFromAvr.class);
        }
        return i ^ 1;
    }
}
