package com.android.server.pm;

import android.content.pm.ShortcutInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Icon;
import android.os.SystemClock;
import android.util.Log;
import android.util.Slog;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.util.Preconditions;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Deque;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ShortcutBitmapSaver {
    private static final boolean ADD_DELAY_BEFORE_SAVE_FOR_TEST = false;
    private static final boolean DEBUG = false;
    private static final long SAVE_DELAY_MS_FOR_TEST = 1000;
    private static final String TAG = "ShortcutService";
    private final long SAVE_WAIT_TIMEOUT_MS = 30000;
    private final Executor mExecutor = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue());
    @GuardedBy("mPendingItems")
    private final Deque<PendingItem> mPendingItems = new LinkedBlockingDeque();
    private final Runnable mRunnable = new -$Lambda$iCTRLJcHnavjRcatPDKSIvElD0U((byte) 1, this);
    private final ShortcutService mService;

    private static class PendingItem {
        public final byte[] bytes;
        private final long mInstantiatedUptimeMillis;
        public final ShortcutInfo shortcut;

        private PendingItem(ShortcutInfo shortcut, byte[] bytes) {
            this.shortcut = shortcut;
            this.bytes = bytes;
            this.mInstantiatedUptimeMillis = SystemClock.uptimeMillis();
        }

        public String toString() {
            return "PendingItem{size=" + this.bytes.length + " age=" + (SystemClock.uptimeMillis() - this.mInstantiatedUptimeMillis) + "ms" + " shortcut=" + this.shortcut.toInsecureString() + "}";
        }
    }

    public ShortcutBitmapSaver(ShortcutService service) {
        this.mService = service;
    }

    public boolean waitForAllSavesLocked() {
        CountDownLatch latch = new CountDownLatch(1);
        this.mExecutor.execute(new -$Lambda$iCTRLJcHnavjRcatPDKSIvElD0U((byte) 2, latch));
        try {
            if (latch.await(30000, TimeUnit.MILLISECONDS)) {
                return true;
            }
            this.mService.wtf("Timed out waiting on saving bitmaps.");
            return false;
        } catch (InterruptedException e) {
            Slog.w(TAG, "interrupted");
        }
    }

    public String getBitmapPathMayWaitLocked(ShortcutInfo shortcut) {
        if (waitForAllSavesLocked() && shortcut.hasIconFile()) {
            return shortcut.getBitmapPath();
        }
        return null;
    }

    public void removeIcon(ShortcutInfo shortcut) {
        shortcut.setIconResourceId(0);
        shortcut.setIconResName(null);
        shortcut.setBitmapPath(null);
        shortcut.clearFlags(2572);
    }

    public void saveBitmapLocked(ShortcutInfo shortcut, int maxDimension, CompressFormat format, int quality) {
        Throwable th;
        Icon icon = shortcut.getIcon();
        Preconditions.checkNotNull(icon);
        Bitmap original = icon.getBitmap();
        if (original == null) {
            Log.e(TAG, "Missing icon: " + shortcut);
            return;
        }
        try {
            ShortcutService shortcutService = this.mService;
            Bitmap shrunk = ShortcutService.shrinkBitmap(original, maxDimension);
            Throwable th2 = null;
            ByteArrayOutputStream byteArrayOutputStream = null;
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream(65536);
                try {
                    if (!shrunk.compress(format, quality, out)) {
                        Slog.wtf(TAG, "Unable to compress bitmap");
                    }
                    out.flush();
                    byte[] bytes = out.toByteArray();
                    out.close();
                    if (out != null) {
                        try {
                            out.close();
                        } catch (Throwable th3) {
                            th2 = th3;
                        }
                    }
                    if (th2 != null) {
                        try {
                            throw th2;
                        } catch (Throwable th4) {
                            th = th4;
                            byteArrayOutputStream = out;
                        }
                    } else {
                        if (shrunk != original) {
                            shrunk.recycle();
                        }
                        shortcut.addFlags(2056);
                        if (icon.getType() == 5) {
                            shortcut.addFlags(512);
                        }
                        PendingItem item = new PendingItem(shortcut, bytes);
                        synchronized (this.mPendingItems) {
                            this.mPendingItems.add(item);
                        }
                        this.mExecutor.execute(this.mRunnable);
                    }
                } catch (Throwable th5) {
                    th = th5;
                    byteArrayOutputStream = out;
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (Throwable th6) {
                            th = th6;
                            if (shrunk != original) {
                                shrunk.recycle();
                            }
                            throw th;
                        }
                    }
                    if (th2 == null) {
                        throw th2;
                    }
                    throw th;
                }
            } catch (Throwable th7) {
                th = th7;
                if (byteArrayOutputStream != null) {
                    byteArrayOutputStream.close();
                }
                if (th2 == null) {
                    throw th;
                }
                throw th2;
            }
        } catch (Throwable e) {
            Slog.wtf(TAG, "Unable to write bitmap to file", e);
        }
    }

    /* synthetic */ void lambda$-com_android_server_pm_ShortcutBitmapSaver_7645() {
        do {
        } while (processPendingItems());
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean processPendingItems() {
        /*
        r10 = this;
        r9 = 0;
        r8 = 1;
        r7 = 2048; // 0x800 float:2.87E-42 double:1.0118E-320;
        r4 = 0;
        r6 = r10.mPendingItems;	 Catch:{ all -> 0x0035 }
        monitor-enter(r6);	 Catch:{ all -> 0x0035 }
        r5 = r10.mPendingItems;	 Catch:{ all -> 0x0032 }
        r5 = r5.size();	 Catch:{ all -> 0x0032 }
        if (r5 != 0) goto L_0x0012;
    L_0x0010:
        monitor-exit(r6);	 Catch:{ all -> 0x0035 }
        return r9;
    L_0x0012:
        r5 = r10.mPendingItems;	 Catch:{ all -> 0x0032 }
        r2 = r5.pop();	 Catch:{ all -> 0x0032 }
        r2 = (com.android.server.pm.ShortcutBitmapSaver.PendingItem) r2;	 Catch:{ all -> 0x0032 }
        monitor-exit(r6);	 Catch:{ all -> 0x0035 }
        r4 = r2.shortcut;	 Catch:{ all -> 0x0035 }
        r5 = r4.isIconPendingSave();	 Catch:{ all -> 0x0035 }
        if (r5 != 0) goto L_0x0045;
    L_0x0023:
        if (r4 == 0) goto L_0x0031;
    L_0x0025:
        r5 = r4.getBitmapPath();
        if (r5 != 0) goto L_0x002e;
    L_0x002b:
        r10.removeIcon(r4);
    L_0x002e:
        r4.clearFlags(r7);
    L_0x0031:
        return r8;
    L_0x0032:
        r5 = move-exception;
        monitor-exit(r6);	 Catch:{ all -> 0x0035 }
        throw r5;	 Catch:{ all -> 0x0035 }
    L_0x0035:
        r5 = move-exception;
        if (r4 == 0) goto L_0x0044;
    L_0x0038:
        r6 = r4.getBitmapPath();
        if (r6 != 0) goto L_0x0041;
    L_0x003e:
        r10.removeIcon(r4);
    L_0x0041:
        r4.clearFlags(r7);
    L_0x0044:
        throw r5;
    L_0x0045:
        r1 = 0;
        r5 = r10.mService;	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
        r6 = r4.getUserId();	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
        r3 = r5.openIconFileForWrite(r6, r4);	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
        r1 = r3.getFile();	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
        r5 = r2.bytes;	 Catch:{ all -> 0x0072 }
        r3.write(r5);	 Catch:{ all -> 0x0072 }
        libcore.io.IoUtils.closeQuietly(r3);	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
        r5 = r1.getAbsolutePath();	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
        r4.setBitmapPath(r5);	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
        if (r4 == 0) goto L_0x0071;
    L_0x0065:
        r5 = r4.getBitmapPath();
        if (r5 != 0) goto L_0x006e;
    L_0x006b:
        r10.removeIcon(r4);
    L_0x006e:
        r4.clearFlags(r7);
    L_0x0071:
        return r8;
    L_0x0072:
        r5 = move-exception;
        libcore.io.IoUtils.closeQuietly(r3);	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
        throw r5;	 Catch:{ IOException -> 0x0077, IOException -> 0x0077 }
    L_0x0077:
        r0 = move-exception;
        r5 = "ShortcutService";
        r6 = "Unable to write bitmap to file";
        android.util.Slog.e(r5, r6, r0);	 Catch:{ all -> 0x0035 }
        if (r1 == 0) goto L_0x008c;
    L_0x0083:
        r5 = r1.exists();	 Catch:{ all -> 0x0035 }
        if (r5 == 0) goto L_0x008c;
    L_0x0089:
        r1.delete();	 Catch:{ all -> 0x0035 }
    L_0x008c:
        if (r4 == 0) goto L_0x009a;
    L_0x008e:
        r5 = r4.getBitmapPath();
        if (r5 != 0) goto L_0x0097;
    L_0x0094:
        r10.removeIcon(r4);
    L_0x0097:
        r4.clearFlags(r7);
    L_0x009a:
        return r8;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.ShortcutBitmapSaver.processPendingItems():boolean");
    }

    public void dumpLocked(PrintWriter pw, String prefix) {
        synchronized (this.mPendingItems) {
            int N = this.mPendingItems.size();
            pw.print(prefix);
            pw.println("Pending saves: Num=" + N + " Executor=" + this.mExecutor);
            for (PendingItem item : this.mPendingItems) {
                pw.print(prefix);
                pw.print("  ");
                pw.println(item);
            }
        }
    }
}
