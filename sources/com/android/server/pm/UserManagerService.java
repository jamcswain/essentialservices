package com.android.server.pm;

import android.app.ActivityManager;
import android.app.ActivityManagerInternal;
import android.app.ActivityManagerNative;
import android.app.IActivityManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.IntentSender.SendIntentException;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.UserInfo;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IBinder;
import android.os.IUserManager.Stub;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.SELinux;
import android.os.ServiceManager;
import android.os.ShellCallback;
import android.os.ShellCommand;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.UserManager.EnforcingUser;
import android.os.UserManagerInternal;
import android.os.UserManagerInternal.UserRestrictionsListener;
import android.os.storage.StorageManager;
import android.provider.Settings.Secure;
import android.security.GateKeeper;
import android.service.gatekeeper.IGateKeeperService;
import android.util.AtomicFile;
import android.util.IntArray;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;
import android.util.TimeUtils;
import android.util.Xml;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.app.IAppOpsService;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.Preconditions;
import com.android.internal.util.XmlUtils;
import com.android.internal.widget.LockPatternUtils;
import com.android.server.LocalServices;
import com.android.server.LockGuard;
import com.android.server.SystemService;
import com.android.server.am.UserState;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class UserManagerService extends Stub {
    private static final int ALLOWED_FLAGS_FOR_CREATE_USERS_PERMISSION = 812;
    private static final String ATTR_CREATION_TIME = "created";
    private static final String ATTR_FLAGS = "flags";
    private static final String ATTR_GUEST_TO_REMOVE = "guestToRemove";
    private static final String ATTR_ICON_PATH = "icon";
    private static final String ATTR_ID = "id";
    private static final String ATTR_KEY = "key";
    private static final String ATTR_LAST_LOGGED_IN_FINGERPRINT = "lastLoggedInFingerprint";
    private static final String ATTR_LAST_LOGGED_IN_TIME = "lastLoggedIn";
    private static final String ATTR_MULTIPLE = "m";
    private static final String ATTR_NEXT_SERIAL_NO = "nextSerialNumber";
    private static final String ATTR_PARTIAL = "partial";
    private static final String ATTR_PROFILE_BADGE = "profileBadge";
    private static final String ATTR_PROFILE_GROUP_ID = "profileGroupId";
    private static final String ATTR_RESTRICTED_PROFILE_PARENT_ID = "restrictedProfileParentId";
    private static final String ATTR_SEED_ACCOUNT_NAME = "seedAccountName";
    private static final String ATTR_SEED_ACCOUNT_TYPE = "seedAccountType";
    private static final String ATTR_SERIAL_NO = "serialNumber";
    private static final String ATTR_TYPE_BOOLEAN = "b";
    private static final String ATTR_TYPE_BUNDLE = "B";
    private static final String ATTR_TYPE_BUNDLE_ARRAY = "BA";
    private static final String ATTR_TYPE_INTEGER = "i";
    private static final String ATTR_TYPE_STRING = "s";
    private static final String ATTR_TYPE_STRING_ARRAY = "sa";
    private static final String ATTR_USER_VERSION = "version";
    private static final String ATTR_VALUE_TYPE = "type";
    static final boolean DBG = false;
    private static final boolean DBG_WITH_STACKTRACE = false;
    private static final long EPOCH_PLUS_30_YEARS = 946080000000L;
    private static final String LOG_TAG = "UserManagerService";
    static final int MAX_MANAGED_PROFILES = 1;
    static final int MAX_RECENTLY_REMOVED_IDS_SIZE = 100;
    static final int MAX_USER_ID = 21474;
    static final int MIN_USER_ID = 10;
    private static final boolean RELEASE_DELETED_USER_ID = false;
    private static final String RESTRICTIONS_FILE_PREFIX = "res_";
    private static final String TAG_ACCOUNT = "account";
    private static final String TAG_DEVICE_OWNER_USER_ID = "deviceOwnerUserId";
    private static final String TAG_DEVICE_POLICY_GLOBAL_RESTRICTIONS = "device_policy_global_restrictions";
    private static final String TAG_DEVICE_POLICY_RESTRICTIONS = "device_policy_restrictions";
    private static final String TAG_ENTRY = "entry";
    private static final String TAG_GLOBAL_RESTRICTION_OWNER_ID = "globalRestrictionOwnerUserId";
    private static final String TAG_GUEST_RESTRICTIONS = "guestRestrictions";
    private static final String TAG_NAME = "name";
    private static final String TAG_RESTRICTIONS = "restrictions";
    private static final String TAG_SEED_ACCOUNT_OPTIONS = "seedAccountOptions";
    private static final String TAG_USER = "user";
    private static final String TAG_USERS = "users";
    private static final String TAG_VALUE = "value";
    private static final String TRON_DEMO_CREATED = "users_demo_created";
    private static final String TRON_GUEST_CREATED = "users_guest_created";
    private static final String TRON_USER_CREATED = "users_user_created";
    private static final String USER_INFO_DIR = ("system" + File.separator + "users");
    private static final String USER_LIST_FILENAME = "userlist.xml";
    private static final String USER_PHOTO_FILENAME = "photo.png";
    private static final String USER_PHOTO_FILENAME_TMP = "photo.png.tmp";
    private static final int USER_VERSION = 7;
    static final int WRITE_USER_DELAY = 2000;
    static final int WRITE_USER_MSG = 1;
    private static final String XML_SUFFIX = ".xml";
    private static final IBinder mUserRestriconToken = new Binder();
    private static UserManagerService sInstance;
    private final String ACTION_DISABLE_QUIET_MODE_AFTER_UNLOCK;
    private IAppOpsService mAppOpsService;
    private final Object mAppRestrictionsLock;
    @GuardedBy("mRestrictionsLock")
    private final SparseArray<Bundle> mAppliedUserRestrictions;
    @GuardedBy("mRestrictionsLock")
    private final SparseArray<Bundle> mBaseUserRestrictions;
    @GuardedBy("mRestrictionsLock")
    private final SparseArray<Bundle> mCachedEffectiveUserRestrictions;
    private final Context mContext;
    @GuardedBy("mRestrictionsLock")
    private int mDeviceOwnerUserId;
    @GuardedBy("mRestrictionsLock")
    private final SparseArray<Bundle> mDevicePolicyGlobalUserRestrictions;
    @GuardedBy("mRestrictionsLock")
    private final SparseArray<Bundle> mDevicePolicyLocalUserRestrictions;
    private final BroadcastReceiver mDisableQuietModeCallback;
    @GuardedBy("mUsersLock")
    private boolean mForceEphemeralUsers;
    @GuardedBy("mGuestRestrictions")
    private final Bundle mGuestRestrictions;
    private final Handler mHandler;
    @GuardedBy("mUsersLock")
    private boolean mIsDeviceManaged;
    @GuardedBy("mUsersLock")
    private final SparseBooleanArray mIsUserManaged;
    private final LocalService mLocalService;
    private final LockPatternUtils mLockPatternUtils;
    @GuardedBy("mPackagesLock")
    private int mNextSerialNumber;
    private final Object mPackagesLock;
    private final PackageManagerService mPm;
    @GuardedBy("mUsersLock")
    private final LinkedList<Integer> mRecentlyRemovedIds;
    @GuardedBy("mUsersLock")
    private final SparseBooleanArray mRemovingUserIds;
    private final Object mRestrictionsLock;
    private final UserDataPreparer mUserDataPreparer;
    @GuardedBy("mUsersLock")
    private int[] mUserIds;
    private final File mUserListFile;
    @GuardedBy("mUserRestrictionsListeners")
    private final ArrayList<UserRestrictionsListener> mUserRestrictionsListeners;
    @GuardedBy("mUserStates")
    private final SparseIntArray mUserStates;
    private int mUserVersion;
    @GuardedBy("mUsersLock")
    private final SparseArray<UserData> mUsers;
    private final File mUsersDir;
    private final Object mUsersLock;

    private class LocalService extends UserManagerInternal {
        private LocalService() {
        }

        public void setDevicePolicyUserRestrictions(int userId, Bundle restrictions, boolean isDeviceOwner, int cameraRestrictionScope) {
            UserManagerService.this.setDevicePolicyUserRestrictionsInner(userId, restrictions, isDeviceOwner, cameraRestrictionScope);
        }

        public Bundle getBaseUserRestrictions(int userId) {
            Bundle bundle;
            synchronized (UserManagerService.this.mRestrictionsLock) {
                bundle = (Bundle) UserManagerService.this.mBaseUserRestrictions.get(userId);
            }
            return bundle;
        }

        public void setBaseUserRestrictionsByDpmsForMigration(int userId, Bundle baseRestrictions) {
            synchronized (UserManagerService.this.mRestrictionsLock) {
                if (UserManagerService.this.updateRestrictionsIfNeededLR(userId, new Bundle(baseRestrictions), UserManagerService.this.mBaseUserRestrictions)) {
                    UserManagerService.this.invalidateEffectiveUserRestrictionsLR(userId);
                }
            }
            UserData userData = UserManagerService.this.getUserDataNoChecks(userId);
            synchronized (UserManagerService.this.mPackagesLock) {
                if (userData != null) {
                    UserManagerService.this.writeUserLP(userData);
                } else {
                    Slog.w(UserManagerService.LOG_TAG, "UserInfo not found for " + userId);
                }
            }
        }

        public boolean getUserRestriction(int userId, String key) {
            return UserManagerService.this.getUserRestrictions(userId).getBoolean(key);
        }

        public void addUserRestrictionsListener(UserRestrictionsListener listener) {
            synchronized (UserManagerService.this.mUserRestrictionsListeners) {
                UserManagerService.this.mUserRestrictionsListeners.add(listener);
            }
        }

        public void removeUserRestrictionsListener(UserRestrictionsListener listener) {
            synchronized (UserManagerService.this.mUserRestrictionsListeners) {
                UserManagerService.this.mUserRestrictionsListeners.remove(listener);
            }
        }

        public void setDeviceManaged(boolean isManaged) {
            synchronized (UserManagerService.this.mUsersLock) {
                UserManagerService.this.mIsDeviceManaged = isManaged;
            }
        }

        public void setUserManaged(int userId, boolean isManaged) {
            synchronized (UserManagerService.this.mUsersLock) {
                UserManagerService.this.mIsUserManaged.put(userId, isManaged);
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void setUserIcon(int r8, android.graphics.Bitmap r9) {
            /*
            r7 = this;
            r0 = android.os.Binder.clearCallingIdentity();
            r3 = com.android.server.pm.UserManagerService.this;	 Catch:{ all -> 0x0051 }
            r4 = r3.mPackagesLock;	 Catch:{ all -> 0x0051 }
            monitor-enter(r4);	 Catch:{ all -> 0x0051 }
            r3 = com.android.server.pm.UserManagerService.this;	 Catch:{ all -> 0x004e }
            r2 = r3.getUserDataNoChecks(r8);	 Catch:{ all -> 0x004e }
            if (r2 == 0) goto L_0x0019;
        L_0x0013:
            r3 = r2.info;	 Catch:{ all -> 0x004e }
            r3 = r3.partial;	 Catch:{ all -> 0x004e }
            if (r3 == 0) goto L_0x0038;
        L_0x0019:
            r3 = "UserManagerService";
            r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x004e }
            r5.<init>();	 Catch:{ all -> 0x004e }
            r6 = "setUserIcon: unknown user #";
            r5 = r5.append(r6);	 Catch:{ all -> 0x004e }
            r5 = r5.append(r8);	 Catch:{ all -> 0x004e }
            r5 = r5.toString();	 Catch:{ all -> 0x004e }
            android.util.Slog.w(r3, r5);	 Catch:{ all -> 0x004e }
            monitor-exit(r4);	 Catch:{ all -> 0x0051 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x0038:
            r3 = com.android.server.pm.UserManagerService.this;	 Catch:{ all -> 0x004e }
            r5 = r2.info;	 Catch:{ all -> 0x004e }
            r3.writeBitmapLP(r5, r9);	 Catch:{ all -> 0x004e }
            r3 = com.android.server.pm.UserManagerService.this;	 Catch:{ all -> 0x004e }
            r3.writeUserLP(r2);	 Catch:{ all -> 0x004e }
            monitor-exit(r4);	 Catch:{ all -> 0x0051 }
            r3 = com.android.server.pm.UserManagerService.this;	 Catch:{ all -> 0x0051 }
            r3.sendUserInfoChangedBroadcast(r8);	 Catch:{ all -> 0x0051 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x004e:
            r3 = move-exception;
            monitor-exit(r4);	 Catch:{ all -> 0x0051 }
            throw r3;	 Catch:{ all -> 0x0051 }
        L_0x0051:
            r3 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r3;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.LocalService.setUserIcon(int, android.graphics.Bitmap):void");
        }

        public void setForceEphemeralUsers(boolean forceEphemeralUsers) {
            synchronized (UserManagerService.this.mUsersLock) {
                UserManagerService.this.mForceEphemeralUsers = forceEphemeralUsers;
            }
        }

        public void removeAllUsers() {
            if (ActivityManager.getCurrentUser() == 0) {
                UserManagerService.this.removeNonSystemUsers();
                return;
            }
            BroadcastReceiver userSwitchedReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getIntExtra("android.intent.extra.user_handle", -10000) == 0) {
                        UserManagerService.this.mContext.unregisterReceiver(this);
                        UserManagerService.this.removeNonSystemUsers();
                    }
                }
            };
            IntentFilter userSwitchedFilter = new IntentFilter();
            userSwitchedFilter.addAction("android.intent.action.USER_SWITCHED");
            UserManagerService.this.mContext.registerReceiver(userSwitchedReceiver, userSwitchedFilter, null, UserManagerService.this.mHandler);
            ((ActivityManager) UserManagerService.this.mContext.getSystemService("activity")).switchUser(0);
        }

        public void onEphemeralUserStop(int userId) {
            synchronized (UserManagerService.this.mUsersLock) {
                UserInfo userInfo = UserManagerService.this.getUserInfoLU(userId);
                if (userInfo != null && userInfo.isEphemeral()) {
                    userInfo.flags |= 64;
                    if (userInfo.isGuest()) {
                        userInfo.guestToRemove = true;
                    }
                }
            }
        }

        public UserInfo createUserEvenWhenDisallowed(String name, int flags) {
            UserInfo user = UserManagerService.this.createUserInternalUnchecked(name, flags, -10000, null);
            if (!(user == null || (user.isAdmin() ^ 1) == 0 || (user.isDemo() ^ 1) == 0)) {
                UserManagerService.this.setUserRestriction("no_sms", true, user.id);
                UserManagerService.this.setUserRestriction("no_outgoing_calls", true, user.id);
            }
            return user;
        }

        public boolean removeUserEvenWhenDisallowed(int userId) {
            return UserManagerService.this.removeUserUnchecked(userId);
        }

        public boolean isUserRunning(int userId) {
            boolean z = false;
            synchronized (UserManagerService.this.mUserStates) {
                if (UserManagerService.this.mUserStates.get(userId, -1) >= 0) {
                    z = true;
                }
            }
            return z;
        }

        public void setUserState(int userId, int userState) {
            synchronized (UserManagerService.this.mUserStates) {
                UserManagerService.this.mUserStates.put(userId, userState);
            }
        }

        public void removeUserState(int userId) {
            synchronized (UserManagerService.this.mUserStates) {
                UserManagerService.this.mUserStates.delete(userId);
            }
        }

        public int[] getUserIds() {
            return UserManagerService.this.getUserIds();
        }

        public boolean isUserUnlockingOrUnlocked(int userId) {
            boolean z = true;
            synchronized (UserManagerService.this.mUserStates) {
                int state = UserManagerService.this.mUserStates.get(userId, -1);
            }
            if (state == 4 || state == 5) {
                return StorageManager.isUserKeyUnlocked(userId);
            }
            if (!(state == 2 || state == 3)) {
                z = false;
            }
            return z;
        }

        public boolean isUserUnlocked(int userId) {
            synchronized (UserManagerService.this.mUserStates) {
                int state = UserManagerService.this.mUserStates.get(userId, -1);
            }
            if (state == 4 || state == 5) {
                return StorageManager.isUserKeyUnlocked(userId);
            }
            return state == 3;
        }
    }

    final class MainHandler extends Handler {
        MainHandler() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    removeMessages(1, msg.obj);
                    synchronized (UserManagerService.this.mPackagesLock) {
                        UserData userData = UserManagerService.this.getUserDataNoChecks(((UserData) msg.obj).info.id);
                        if (userData != null) {
                            UserManagerService.this.writeUserLP(userData);
                        }
                    }
                    return;
                default:
                    return;
            }
        }
    }

    static class UserData {
        String account;
        UserInfo info;
        boolean persistSeedData;
        String seedAccountName;
        PersistableBundle seedAccountOptions;
        String seedAccountType;

        UserData() {
        }

        void clearSeedAccountData() {
            this.seedAccountName = null;
            this.seedAccountType = null;
            this.seedAccountOptions = null;
            this.persistSeedData = false;
        }
    }

    public static class LifeCycle extends SystemService {
        private UserManagerService mUms;

        public LifeCycle(Context context) {
            super(context);
        }

        public void onStart() {
            this.mUms = UserManagerService.getInstance();
            publishBinderService(UserManagerService.TAG_USER, this.mUms);
        }

        public void onBootPhase(int phase) {
            if (phase == SystemService.PHASE_ACTIVITY_MANAGER_READY) {
                this.mUms.cleanupPartialUsers();
            }
        }
    }

    private class Shell extends ShellCommand {
        private Shell() {
        }

        public int onCommand(String cmd) {
            return UserManagerService.this.onShellCommand(this, cmd);
        }

        public void onHelp() {
            PrintWriter pw = getOutPrintWriter();
            pw.println("User manager (user) commands:");
            pw.println("  help");
            pw.println("    Print this help text.");
            pw.println("");
            pw.println("  list");
            pw.println("    Prints all users on the system.");
        }
    }

    public static UserManagerService getInstance() {
        UserManagerService userManagerService;
        synchronized (UserManagerService.class) {
            userManagerService = sInstance;
        }
        return userManagerService;
    }

    UserManagerService(Context context) {
        this(context, null, null, new Object(), context.getCacheDir());
    }

    UserManagerService(Context context, PackageManagerService pm, UserDataPreparer userDataPreparer, Object packagesLock) {
        this(context, pm, userDataPreparer, packagesLock, Environment.getDataDirectory());
    }

    private UserManagerService(Context context, PackageManagerService pm, UserDataPreparer userDataPreparer, Object packagesLock, File dataDir) {
        this.mUsersLock = LockGuard.installNewLock(2);
        this.mRestrictionsLock = new Object();
        this.mAppRestrictionsLock = new Object();
        this.mUsers = new SparseArray();
        this.mBaseUserRestrictions = new SparseArray();
        this.mCachedEffectiveUserRestrictions = new SparseArray();
        this.mAppliedUserRestrictions = new SparseArray();
        this.mDevicePolicyGlobalUserRestrictions = new SparseArray();
        this.mDeviceOwnerUserId = -10000;
        this.mDevicePolicyLocalUserRestrictions = new SparseArray();
        this.mGuestRestrictions = new Bundle();
        this.mRemovingUserIds = new SparseBooleanArray();
        this.mRecentlyRemovedIds = new LinkedList();
        this.mUserVersion = 0;
        this.mIsUserManaged = new SparseBooleanArray();
        this.mUserRestrictionsListeners = new ArrayList();
        this.ACTION_DISABLE_QUIET_MODE_AFTER_UNLOCK = "com.android.server.pm.DISABLE_QUIET_MODE_AFTER_UNLOCK";
        this.mDisableQuietModeCallback = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if ("com.android.server.pm.DISABLE_QUIET_MODE_AFTER_UNLOCK".equals(intent.getAction())) {
                    IntentSender target = (IntentSender) intent.getParcelableExtra("android.intent.extra.INTENT");
                    UserManagerService.this.setQuietModeEnabled(intent.getIntExtra("android.intent.extra.USER_ID", 0), false);
                    if (target != null) {
                        try {
                            UserManagerService.this.mContext.startIntentSender(target, null, 0, 0, 0);
                        } catch (SendIntentException e) {
                        }
                    }
                }
            }
        };
        this.mUserStates = new SparseIntArray();
        this.mContext = context;
        this.mPm = pm;
        this.mPackagesLock = packagesLock;
        this.mHandler = new MainHandler();
        this.mUserDataPreparer = userDataPreparer;
        synchronized (this.mPackagesLock) {
            this.mUsersDir = new File(dataDir, USER_INFO_DIR);
            this.mUsersDir.mkdirs();
            new File(this.mUsersDir, String.valueOf(0)).mkdirs();
            FileUtils.setPermissions(this.mUsersDir.toString(), 509, -1, -1);
            this.mUserListFile = new File(this.mUsersDir, USER_LIST_FILENAME);
            initDefaultGuestRestrictions();
            readUserListLP();
            sInstance = this;
        }
        this.mLocalService = new LocalService();
        LocalServices.addService(UserManagerInternal.class, this.mLocalService);
        this.mLockPatternUtils = new LockPatternUtils(this.mContext);
        this.mUserStates.put(0, 0);
    }

    void systemReady() {
        this.mAppOpsService = IAppOpsService.Stub.asInterface(ServiceManager.getService("appops"));
        synchronized (this.mRestrictionsLock) {
            applyUserRestrictionsLR(0);
        }
        UserInfo currentGuestUser = findCurrentGuestUser();
        if (!(currentGuestUser == null || (hasUserRestriction("no_config_wifi", currentGuestUser.id) ^ 1) == 0)) {
            setUserRestriction("no_config_wifi", true, currentGuestUser.id);
        }
        this.mContext.registerReceiver(this.mDisableQuietModeCallback, new IntentFilter("com.android.server.pm.DISABLE_QUIET_MODE_AFTER_UNLOCK"), null, this.mHandler);
    }

    void cleanupPartialUsers() {
        ArrayList<UserInfo> partials = new ArrayList();
        synchronized (this.mUsersLock) {
            int userSize = this.mUsers.size();
            int i = 0;
            while (i < userSize) {
                UserInfo ui = ((UserData) this.mUsers.valueAt(i)).info;
                if ((ui.partial || ui.guestToRemove || ui.isEphemeral()) && i != 0) {
                    partials.add(ui);
                    addRemovingUserIdLocked(ui.id);
                    ui.partial = true;
                }
                i++;
            }
        }
        int partialsSize = partials.size();
        for (i = 0; i < partialsSize; i++) {
            ui = (UserInfo) partials.get(i);
            Slog.w(LOG_TAG, "Removing partially created user " + ui.id + " (name=" + ui.name + ")");
            removeUserState(ui.id);
        }
    }

    public String getUserAccount(int userId) {
        String str;
        checkManageUserAndAcrossUsersFullPermission("get user account");
        synchronized (this.mUsersLock) {
            str = ((UserData) this.mUsers.get(userId)).account;
        }
        return str;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setUserAccount(int r9, java.lang.String r10) {
        /*
        r8 = this;
        r3 = "set user account";
        checkManageUserAndAcrossUsersFullPermission(r3);
        r2 = 0;
        r4 = r8.mPackagesLock;
        monitor-enter(r4);
        r5 = r8.mUsersLock;	 Catch:{ all -> 0x004a }
        monitor-enter(r5);	 Catch:{ all -> 0x004a }
        r3 = r8.mUsers;	 Catch:{ all -> 0x0047 }
        r1 = r3.get(r9);	 Catch:{ all -> 0x0047 }
        r1 = (com.android.server.pm.UserManagerService.UserData) r1;	 Catch:{ all -> 0x0047 }
        if (r1 != 0) goto L_0x0034;
    L_0x0017:
        r3 = "UserManagerService";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0047 }
        r6.<init>();	 Catch:{ all -> 0x0047 }
        r7 = "User not found for setting user account: u";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0047 }
        r6 = r6.append(r9);	 Catch:{ all -> 0x0047 }
        r6 = r6.toString();	 Catch:{ all -> 0x0047 }
        android.util.Slog.e(r3, r6);	 Catch:{ all -> 0x0047 }
        monitor-exit(r5);	 Catch:{ all -> 0x004a }
        monitor-exit(r4);
        return;
    L_0x0034:
        r0 = r1.account;	 Catch:{ all -> 0x0047 }
        r3 = libcore.util.Objects.equal(r0, r10);	 Catch:{ all -> 0x0047 }
        if (r3 != 0) goto L_0x003f;
    L_0x003c:
        r1.account = r10;	 Catch:{ all -> 0x0047 }
        r2 = r1;
    L_0x003f:
        monitor-exit(r5);	 Catch:{ all -> 0x004a }
        if (r2 == 0) goto L_0x0045;
    L_0x0042:
        r8.writeUserLP(r2);	 Catch:{ all -> 0x004a }
    L_0x0045:
        monitor-exit(r4);
        return;
    L_0x0047:
        r3 = move-exception;
        monitor-exit(r5);	 Catch:{ all -> 0x004a }
        throw r3;	 Catch:{ all -> 0x004a }
    L_0x004a:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.setUserAccount(int, java.lang.String):void");
    }

    public UserInfo getPrimaryUser() {
        checkManageUsersPermission("query users");
        synchronized (this.mUsersLock) {
            int userSize = this.mUsers.size();
            int i = 0;
            while (i < userSize) {
                UserInfo ui = ((UserData) this.mUsers.valueAt(i)).info;
                if (!ui.isPrimary() || (this.mRemovingUserIds.get(ui.id) ^ 1) == 0) {
                    i++;
                } else {
                    return ui;
                }
            }
            return null;
        }
    }

    public List<UserInfo> getUsers(boolean excludeDying) {
        ArrayList<UserInfo> users;
        checkManageOrCreateUsersPermission("query users");
        synchronized (this.mUsersLock) {
            users = new ArrayList(this.mUsers.size());
            int userSize = this.mUsers.size();
            for (int i = 0; i < userSize; i++) {
                UserInfo ui = ((UserData) this.mUsers.valueAt(i)).info;
                if (!(ui.partial || (excludeDying && (this.mRemovingUserIds.get(ui.id) ^ 1) == 0))) {
                    users.add(userWithName(ui));
                }
            }
        }
        return users;
    }

    public List<UserInfo> getProfiles(int userId, boolean enabledOnly) {
        boolean returnFullInfo = true;
        if (userId != UserHandle.getCallingUserId()) {
            List<UserInfo> list = "getting profiles related to user ";
            checkManageOrCreateUsersPermission(list + userId);
        } else {
            returnFullInfo = hasManageUsersPermission();
        }
        long ident = Binder.clearCallingIdentity();
        try {
            synchronized (this.mUsersLock) {
                list = getProfilesLU(userId, enabledOnly, returnFullInfo);
            }
            return list;
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    public int[] getProfileIds(int userId, boolean enabledOnly) {
        if (userId != UserHandle.getCallingUserId()) {
            checkManageOrCreateUsersPermission("getting profiles related to user " + userId);
        }
        long ident = Binder.clearCallingIdentity();
        try {
            int[] toArray;
            synchronized (this.mUsersLock) {
                toArray = getProfileIdsLU(userId, enabledOnly).toArray();
            }
            return toArray;
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    private List<UserInfo> getProfilesLU(int userId, boolean enabledOnly, boolean fullInfo) {
        IntArray profileIds = getProfileIdsLU(userId, enabledOnly);
        ArrayList<UserInfo> users = new ArrayList(profileIds.size());
        for (int i = 0; i < profileIds.size(); i++) {
            UserInfo userInfo = ((UserData) this.mUsers.get(profileIds.get(i))).info;
            if (fullInfo) {
                userInfo = userWithName(userInfo);
            } else {
                UserInfo userInfo2 = new UserInfo(userInfo);
                userInfo2.name = null;
                userInfo2.iconPath = null;
                userInfo = userInfo2;
            }
            users.add(userInfo);
        }
        return users;
    }

    private IntArray getProfileIdsLU(int userId, boolean enabledOnly) {
        UserInfo user = getUserInfoLU(userId);
        IntArray result = new IntArray(this.mUsers.size());
        if (user == null) {
            return result;
        }
        int userSize = this.mUsers.size();
        for (int i = 0; i < userSize; i++) {
            UserInfo profile = ((UserData) this.mUsers.valueAt(i)).info;
            if (isProfileOf(user, profile) && !((enabledOnly && (profile.isEnabled() ^ 1) != 0) || this.mRemovingUserIds.get(profile.id) || profile.partial)) {
                result.add(profile.id);
            }
        }
        return result;
    }

    public int getCredentialOwnerProfile(int userHandle) {
        checkManageUsersPermission("get the credential owner");
        if (!this.mLockPatternUtils.isSeparateProfileChallengeEnabled(userHandle)) {
            synchronized (this.mUsersLock) {
                UserInfo profileParent = getProfileParentLU(userHandle);
                if (profileParent != null) {
                    int i = profileParent.id;
                    return i;
                }
            }
        }
        return userHandle;
    }

    public boolean isSameProfileGroup(int userId, int otherUserId) {
        if (userId == otherUserId) {
            return true;
        }
        checkManageUsersPermission("check if in the same profile group");
        return isSameProfileGroupNoChecks(userId, otherUserId);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean isSameProfileGroupNoChecks(int r7, int r8) {
        /*
        r6 = this;
        r5 = -10000; // 0xffffffffffffd8f0 float:NaN double:NaN;
        r2 = 0;
        r3 = r6.mUsersLock;
        monitor-enter(r3);
        r1 = r6.getUserInfoLU(r7);	 Catch:{ all -> 0x0027 }
        if (r1 == 0) goto L_0x0010;
    L_0x000c:
        r4 = r1.profileGroupId;	 Catch:{ all -> 0x0027 }
        if (r4 != r5) goto L_0x0012;
    L_0x0010:
        monitor-exit(r3);
        return r2;
    L_0x0012:
        r0 = r6.getUserInfoLU(r8);	 Catch:{ all -> 0x0027 }
        if (r0 == 0) goto L_0x001c;
    L_0x0018:
        r4 = r0.profileGroupId;	 Catch:{ all -> 0x0027 }
        if (r4 != r5) goto L_0x001e;
    L_0x001c:
        monitor-exit(r3);
        return r2;
    L_0x001e:
        r4 = r1.profileGroupId;	 Catch:{ all -> 0x0027 }
        r5 = r0.profileGroupId;	 Catch:{ all -> 0x0027 }
        if (r4 != r5) goto L_0x0025;
    L_0x0024:
        r2 = 1;
    L_0x0025:
        monitor-exit(r3);
        return r2;
    L_0x0027:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.isSameProfileGroupNoChecks(int, int):boolean");
    }

    public UserInfo getProfileParent(int userHandle) {
        UserInfo profileParentLU;
        checkManageUsersPermission("get the profile parent");
        synchronized (this.mUsersLock) {
            profileParentLU = getProfileParentLU(userHandle);
        }
        return profileParentLU;
    }

    private UserInfo getProfileParentLU(int userHandle) {
        UserInfo profile = getUserInfoLU(userHandle);
        if (profile == null) {
            return null;
        }
        int parentUserId = profile.profileGroupId;
        if (parentUserId == userHandle || parentUserId == -10000) {
            return null;
        }
        return getUserInfoLU(parentUserId);
    }

    private static boolean isProfileOf(UserInfo user, UserInfo profile) {
        if (user.id == profile.id) {
            return true;
        }
        if (user.profileGroupId != -10000) {
            return user.profileGroupId == profile.profileGroupId;
        } else {
            return false;
        }
    }

    private void broadcastProfileAvailabilityChanges(UserHandle profileHandle, UserHandle parentHandle, boolean inQuietMode) {
        Intent intent = new Intent();
        if (inQuietMode) {
            intent.setAction("android.intent.action.MANAGED_PROFILE_UNAVAILABLE");
        } else {
            intent.setAction("android.intent.action.MANAGED_PROFILE_AVAILABLE");
        }
        intent.putExtra("android.intent.extra.QUIET_MODE", inQuietMode);
        intent.putExtra("android.intent.extra.USER", profileHandle);
        intent.putExtra("android.intent.extra.user_handle", profileHandle.getIdentifier());
        intent.addFlags(1073741824);
        this.mContext.sendBroadcastAsUser(intent, parentHandle);
    }

    public void setQuietModeEnabled(int userHandle, boolean enableQuietMode) {
        checkManageUsersPermission("silence profile");
        boolean changed = false;
        synchronized (this.mPackagesLock) {
            synchronized (this.mUsersLock) {
                UserInfo profile = getUserInfoLU(userHandle);
                UserInfo parent = getProfileParentLU(userHandle);
            }
            if (profile == null || (profile.isManagedProfile() ^ 1) != 0) {
                throw new IllegalArgumentException("User " + userHandle + " is not a profile");
            }
            if (profile.isQuietModeEnabled() != enableQuietMode) {
                profile.flags ^= 128;
                writeUserLP(getUserDataLU(profile.id));
                changed = true;
            }
        }
        if (changed) {
            long identity = Binder.clearCallingIdentity();
            if (enableQuietMode) {
                try {
                    ActivityManager.getService().stopUser(userHandle, true, null);
                    ((ActivityManagerInternal) LocalServices.getService(ActivityManagerInternal.class)).killForegroundAppsForUser(userHandle);
                } catch (RemoteException e) {
                    Slog.e(LOG_TAG, "fail to start/stop user for quiet mode", e);
                } finally {
                    Binder.restoreCallingIdentity(identity);
                }
            } else {
                ActivityManager.getService().startUserInBackground(userHandle);
            }
            Binder.restoreCallingIdentity(identity);
            broadcastProfileAvailabilityChanges(profile.getUserHandle(), parent.getUserHandle(), enableQuietMode);
        }
    }

    public boolean isQuietModeEnabled(int userHandle) {
        synchronized (this.mPackagesLock) {
            synchronized (this.mUsersLock) {
                UserInfo info = getUserInfoLU(userHandle);
            }
            if (info == null || (info.isManagedProfile() ^ 1) != 0) {
                return false;
            }
            boolean isQuietModeEnabled = info.isQuietModeEnabled();
            return isQuietModeEnabled;
        }
    }

    public boolean trySetQuietModeDisabled(int userHandle, IntentSender target) {
        checkManageUsersPermission("silence profile");
        if (StorageManager.isUserKeyUnlocked(userHandle) || (this.mLockPatternUtils.isSecure(userHandle) ^ 1) != 0) {
            setQuietModeEnabled(userHandle, false);
            return true;
        }
        long identity = Binder.clearCallingIdentity();
        try {
            Intent unlockIntent = ((KeyguardManager) this.mContext.getSystemService("keyguard")).createConfirmDeviceCredentialIntent(null, null, userHandle);
            if (unlockIntent == null) {
                return false;
            }
            Intent callBackIntent = new Intent("com.android.server.pm.DISABLE_QUIET_MODE_AFTER_UNLOCK");
            if (target != null) {
                callBackIntent.putExtra("android.intent.extra.INTENT", target);
            }
            callBackIntent.putExtra("android.intent.extra.USER_ID", userHandle);
            callBackIntent.setPackage(this.mContext.getPackageName());
            callBackIntent.addFlags(268435456);
            unlockIntent.putExtra("android.intent.extra.INTENT", PendingIntent.getBroadcast(this.mContext, 0, callBackIntent, 1409286144).getIntentSender());
            unlockIntent.setFlags(276824064);
            this.mContext.startActivity(unlockIntent);
            Binder.restoreCallingIdentity(identity);
            return false;
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    public void setUserEnabled(int userId) {
        checkManageUsersPermission("enable user");
        synchronized (this.mPackagesLock) {
            synchronized (this.mUsersLock) {
                UserInfo info = getUserInfoLU(userId);
            }
            if (!(info == null || (info.isEnabled() ^ 1) == 0)) {
                info.flags ^= 64;
                writeUserLP(getUserDataLU(info.id));
            }
        }
    }

    public void evictCredentialEncryptionKey(int userId) {
        checkManageUsersPermission("evict CE key");
        IActivityManager am = ActivityManagerNative.getDefault();
        long identity = Binder.clearCallingIdentity();
        try {
            am.restartUserInBackground(userId);
            Binder.restoreCallingIdentity(identity);
        } catch (RemoteException re) {
            throw re.rethrowAsRuntimeException();
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(identity);
        }
    }

    public UserInfo getUserInfo(int userId) {
        UserInfo userWithName;
        checkManageOrCreateUsersPermission("query user");
        synchronized (this.mUsersLock) {
            userWithName = userWithName(getUserInfoLU(userId));
        }
        return userWithName;
    }

    private UserInfo userWithName(UserInfo orig) {
        if (orig == null || orig.name != null || orig.id != 0) {
            return orig;
        }
        UserInfo withName = new UserInfo(orig);
        withName.name = getOwnerName();
        return withName;
    }

    public int getManagedProfileBadge(int userId) {
        int callingUserId = UserHandle.getCallingUserId();
        if (callingUserId == userId || (hasManageUsersPermission() ^ 1) == 0 || isSameProfileGroupNoChecks(callingUserId, userId)) {
            int i;
            synchronized (this.mUsersLock) {
                UserInfo userInfo = getUserInfoLU(userId);
                i = userInfo != null ? userInfo.profileBadge : 0;
            }
            return i;
        }
        throw new SecurityException("You need MANAGE_USERS permission to: check if specified user a managed profile outside your profile group");
    }

    public boolean isManagedProfile(int userId) {
        int callingUserId = UserHandle.getCallingUserId();
        if (callingUserId == userId || (hasManageUsersPermission() ^ 1) == 0 || isSameProfileGroupNoChecks(callingUserId, userId)) {
            boolean isManagedProfile;
            synchronized (this.mUsersLock) {
                UserInfo userInfo = getUserInfoLU(userId);
                isManagedProfile = userInfo != null ? userInfo.isManagedProfile() : false;
            }
            return isManagedProfile;
        }
        throw new SecurityException("You need MANAGE_USERS permission to: check if specified user a managed profile outside your profile group");
    }

    public boolean isUserUnlockingOrUnlocked(int userId) {
        checkManageOrInteractPermIfCallerInOtherProfileGroup(userId, "isUserUnlockingOrUnlocked");
        return this.mLocalService.isUserUnlockingOrUnlocked(userId);
    }

    public boolean isUserUnlocked(int userId) {
        checkManageOrInteractPermIfCallerInOtherProfileGroup(userId, "isUserUnlocked");
        return this.mLocalService.isUserUnlocked(userId);
    }

    public boolean isUserRunning(int userId) {
        checkManageOrInteractPermIfCallerInOtherProfileGroup(userId, "isUserRunning");
        return this.mLocalService.isUserRunning(userId);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void checkManageOrInteractPermIfCallerInOtherProfileGroup(int r6, java.lang.String r7) {
        /*
        r5 = this;
        r0 = android.os.UserHandle.getCallingUserId();
        if (r0 == r6) goto L_0x0012;
    L_0x0006:
        r1 = r5.isSameProfileGroupNoChecks(r0, r6);
        if (r1 != 0) goto L_0x0012;
    L_0x000c:
        r1 = hasManageUsersPermission();
        if (r1 == 0) goto L_0x0013;
    L_0x0012:
        return;
    L_0x0013:
        r1 = "android.permission.INTERACT_ACROSS_USERS";
        r2 = android.os.Binder.getCallingUid();
        r3 = -1;
        r4 = 1;
        r1 = android.app.ActivityManager.checkComponentPermission(r1, r2, r3, r4);
        if (r1 == 0) goto L_0x003c;
    L_0x0022:
        r1 = new java.lang.SecurityException;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r3 = "You need INTERACT_ACROSS_USERS or MANAGE_USERS permission to: check ";
        r2 = r2.append(r3);
        r2 = r2.append(r7);
        r2 = r2.toString();
        r1.<init>(r2);
        throw r1;
    L_0x003c:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.checkManageOrInteractPermIfCallerInOtherProfileGroup(int, java.lang.String):void");
    }

    public boolean isDemoUser(int userId) {
        if (UserHandle.getCallingUserId() == userId || (hasManageUsersPermission() ^ 1) == 0) {
            boolean isDemo;
            synchronized (this.mUsersLock) {
                UserInfo userInfo = getUserInfoLU(userId);
                isDemo = userInfo != null ? userInfo.isDemo() : false;
            }
            return isDemo;
        }
        throw new SecurityException("You need MANAGE_USERS permission to query if u=" + userId + " is a demo user");
    }

    public boolean isRestricted() {
        boolean isRestricted;
        synchronized (this.mUsersLock) {
            isRestricted = getUserInfoLU(UserHandle.getCallingUserId()).isRestricted();
        }
        return isRestricted;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean canHaveRestrictedProfile(int r5) {
        /*
        r4 = this;
        r1 = 0;
        r2 = "canHaveRestrictedProfile";
        checkManageUsersPermission(r2);
        r2 = r4.mUsersLock;
        monitor-enter(r2);
        r0 = r4.getUserInfoLU(r5);	 Catch:{ all -> 0x0030 }
        if (r0 == 0) goto L_0x0018;
    L_0x0010:
        r3 = r0.canHaveProfile();	 Catch:{ all -> 0x0030 }
        r3 = r3 ^ 1;
        if (r3 == 0) goto L_0x001a;
    L_0x0018:
        monitor-exit(r2);
        return r1;
    L_0x001a:
        r3 = r0.isAdmin();	 Catch:{ all -> 0x0030 }
        if (r3 != 0) goto L_0x0022;
    L_0x0020:
        monitor-exit(r2);
        return r1;
    L_0x0022:
        r3 = r4.mIsDeviceManaged;	 Catch:{ all -> 0x0030 }
        if (r3 != 0) goto L_0x002e;
    L_0x0026:
        r1 = r4.mIsUserManaged;	 Catch:{ all -> 0x0030 }
        r1 = r1.get(r5);	 Catch:{ all -> 0x0030 }
        r1 = r1 ^ 1;
    L_0x002e:
        monitor-exit(r2);
        return r1;
    L_0x0030:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.canHaveRestrictedProfile(int):boolean");
    }

    private UserInfo getUserInfoLU(int userId) {
        UserInfo userInfo = null;
        UserData userData = (UserData) this.mUsers.get(userId);
        if (userData == null || !userData.info.partial || (this.mRemovingUserIds.get(userId) ^ 1) == 0) {
            if (userData != null) {
                userInfo = userData.info;
            }
            return userInfo;
        }
        Slog.w(LOG_TAG, "getUserInfo: unknown user #" + userId);
        return null;
    }

    private UserData getUserDataLU(int userId) {
        UserData userData = (UserData) this.mUsers.get(userId);
        if (userData == null || !userData.info.partial || (this.mRemovingUserIds.get(userId) ^ 1) == 0) {
            return userData;
        }
        return null;
    }

    private UserInfo getUserInfoNoChecks(int userId) {
        UserInfo userInfo = null;
        synchronized (this.mUsersLock) {
            UserData userData = (UserData) this.mUsers.get(userId);
            if (userData != null) {
                userInfo = userData.info;
            }
        }
        return userInfo;
    }

    private UserData getUserDataNoChecks(int userId) {
        UserData userData;
        synchronized (this.mUsersLock) {
            userData = (UserData) this.mUsers.get(userId);
        }
        return userData;
    }

    public boolean exists(int userId) {
        return getUserInfoNoChecks(userId) != null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setUserName(int r7, java.lang.String r8) {
        /*
        r6 = this;
        r2 = "rename users";
        checkManageUsersPermission(r2);
        r0 = 0;
        r3 = r6.mPackagesLock;
        monitor-enter(r3);
        r1 = r6.getUserDataNoChecks(r7);	 Catch:{ all -> 0x004f }
        if (r1 == 0) goto L_0x0016;
    L_0x0010:
        r2 = r1.info;	 Catch:{ all -> 0x004f }
        r2 = r2.partial;	 Catch:{ all -> 0x004f }
        if (r2 == 0) goto L_0x0032;
    L_0x0016:
        r2 = "UserManagerService";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x004f }
        r4.<init>();	 Catch:{ all -> 0x004f }
        r5 = "setUserName: unknown user #";
        r4 = r4.append(r5);	 Catch:{ all -> 0x004f }
        r4 = r4.append(r7);	 Catch:{ all -> 0x004f }
        r4 = r4.toString();	 Catch:{ all -> 0x004f }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x004f }
        monitor-exit(r3);
        return;
    L_0x0032:
        if (r8 == 0) goto L_0x0048;
    L_0x0034:
        r2 = r1.info;	 Catch:{ all -> 0x004f }
        r2 = r2.name;	 Catch:{ all -> 0x004f }
        r2 = r8.equals(r2);	 Catch:{ all -> 0x004f }
        r2 = r2 ^ 1;
        if (r2 == 0) goto L_0x0048;
    L_0x0040:
        r2 = r1.info;	 Catch:{ all -> 0x004f }
        r2.name = r8;	 Catch:{ all -> 0x004f }
        r6.writeUserLP(r1);	 Catch:{ all -> 0x004f }
        r0 = 1;
    L_0x0048:
        monitor-exit(r3);
        if (r0 == 0) goto L_0x004e;
    L_0x004b:
        r6.sendUserInfoChangedBroadcast(r7);
    L_0x004e:
        return;
    L_0x004f:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.setUserName(int, java.lang.String):void");
    }

    public void setUserIcon(int userId, Bitmap bitmap) {
        checkManageUsersPermission("update users");
        if (hasUserRestriction("no_set_user_icon", userId)) {
            Log.w(LOG_TAG, "Cannot set user icon. DISALLOW_SET_USER_ICON is enabled.");
        } else {
            this.mLocalService.setUserIcon(userId, bitmap);
        }
    }

    private void sendUserInfoChangedBroadcast(int userId) {
        Intent changedIntent = new Intent("android.intent.action.USER_INFO_CHANGED");
        changedIntent.putExtra("android.intent.extra.user_handle", userId);
        changedIntent.addFlags(1073741824);
        this.mContext.sendBroadcastAsUser(changedIntent, UserHandle.ALL);
    }

    public ParcelFileDescriptor getUserIcon(int targetUserId) {
        synchronized (this.mPackagesLock) {
            UserInfo targetUserInfo = getUserInfoNoChecks(targetUserId);
            if (targetUserInfo == null || targetUserInfo.partial) {
                Slog.w(LOG_TAG, "getUserIcon: unknown user #" + targetUserId);
                return null;
            }
            int callingUserId = UserHandle.getCallingUserId();
            int callingGroupId = getUserInfoNoChecks(callingUserId).profileGroupId;
            boolean sameGroup = callingGroupId != -10000 ? callingGroupId == targetUserInfo.profileGroupId : false;
            if (!(callingUserId == targetUserId || (sameGroup ^ 1) == 0)) {
                checkManageUsersPermission("get the icon of a user who is not related");
            }
            if (targetUserInfo.iconPath == null) {
                return null;
            }
            String iconPath = targetUserInfo.iconPath;
            try {
                return ParcelFileDescriptor.open(new File(iconPath), 268435456);
            } catch (FileNotFoundException e) {
                Log.e(LOG_TAG, "Couldn't find icon file", e);
                return null;
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void makeInitialized(int r7) {
        /*
        r6 = this;
        r2 = "makeInitialized";
        checkManageUsersPermission(r2);
        r0 = 0;
        r3 = r6.mUsersLock;
        monitor-enter(r3);
        r2 = r6.mUsers;	 Catch:{ all -> 0x004e }
        r1 = r2.get(r7);	 Catch:{ all -> 0x004e }
        r1 = (com.android.server.pm.UserManagerService.UserData) r1;	 Catch:{ all -> 0x004e }
        if (r1 == 0) goto L_0x001a;
    L_0x0014:
        r2 = r1.info;	 Catch:{ all -> 0x004e }
        r2 = r2.partial;	 Catch:{ all -> 0x004e }
        if (r2 == 0) goto L_0x0036;
    L_0x001a:
        r2 = "UserManagerService";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x004e }
        r4.<init>();	 Catch:{ all -> 0x004e }
        r5 = "makeInitialized: unknown user #";
        r4 = r4.append(r5);	 Catch:{ all -> 0x004e }
        r4 = r4.append(r7);	 Catch:{ all -> 0x004e }
        r4 = r4.toString();	 Catch:{ all -> 0x004e }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x004e }
        monitor-exit(r3);
        return;
    L_0x0036:
        r2 = r1.info;	 Catch:{ all -> 0x004e }
        r2 = r2.flags;	 Catch:{ all -> 0x004e }
        r2 = r2 & 16;
        if (r2 != 0) goto L_0x0047;
    L_0x003e:
        r2 = r1.info;	 Catch:{ all -> 0x004e }
        r4 = r2.flags;	 Catch:{ all -> 0x004e }
        r4 = r4 | 16;
        r2.flags = r4;	 Catch:{ all -> 0x004e }
        r0 = 1;
    L_0x0047:
        monitor-exit(r3);
        if (r0 == 0) goto L_0x004d;
    L_0x004a:
        r6.scheduleWriteUser(r1);
    L_0x004d:
        return;
    L_0x004e:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.makeInitialized(int):void");
    }

    private void initDefaultGuestRestrictions() {
        synchronized (this.mGuestRestrictions) {
            if (this.mGuestRestrictions.isEmpty()) {
                this.mGuestRestrictions.putBoolean("no_config_wifi", true);
                this.mGuestRestrictions.putBoolean("no_install_unknown_sources", true);
                this.mGuestRestrictions.putBoolean("no_outgoing_calls", true);
                this.mGuestRestrictions.putBoolean("no_sms", true);
            }
        }
    }

    public Bundle getDefaultGuestRestrictions() {
        Bundle bundle;
        checkManageUsersPermission("getDefaultGuestRestrictions");
        synchronized (this.mGuestRestrictions) {
            bundle = new Bundle(this.mGuestRestrictions);
        }
        return bundle;
    }

    public void setDefaultGuestRestrictions(Bundle restrictions) {
        checkManageUsersPermission("setDefaultGuestRestrictions");
        synchronized (this.mGuestRestrictions) {
            this.mGuestRestrictions.clear();
            this.mGuestRestrictions.putAll(restrictions);
        }
        synchronized (this.mPackagesLock) {
            writeUserListLP();
        }
    }

    private void setDevicePolicyUserRestrictionsInner(int userId, Bundle restrictions, boolean isDeviceOwner, int cameraRestrictionScope) {
        Bundle global = new Bundle();
        Bundle local = new Bundle();
        UserRestrictionsUtils.sortToGlobalAndLocal(restrictions, isDeviceOwner, cameraRestrictionScope, global, local);
        synchronized (this.mRestrictionsLock) {
            boolean globalChanged = updateRestrictionsIfNeededLR(userId, global, this.mDevicePolicyGlobalUserRestrictions);
            boolean localChanged = updateRestrictionsIfNeededLR(userId, local, this.mDevicePolicyLocalUserRestrictions);
            if (isDeviceOwner) {
                this.mDeviceOwnerUserId = userId;
            } else if (this.mDeviceOwnerUserId == userId) {
                this.mDeviceOwnerUserId = -10000;
            }
        }
        synchronized (this.mPackagesLock) {
            if (localChanged || globalChanged) {
                writeUserLP(getUserDataNoChecks(userId));
            }
        }
        synchronized (this.mRestrictionsLock) {
            if (globalChanged) {
                applyUserRestrictionsForAllUsersLR();
            } else if (localChanged) {
                applyUserRestrictionsLR(userId);
            }
        }
    }

    private boolean updateRestrictionsIfNeededLR(int userId, Bundle restrictions, SparseArray<Bundle> restrictionsArray) {
        boolean changed = UserRestrictionsUtils.areEqual((Bundle) restrictionsArray.get(userId), restrictions) ^ 1;
        if (changed) {
            if (UserRestrictionsUtils.isEmpty(restrictions)) {
                restrictionsArray.delete(userId);
            } else {
                restrictionsArray.put(userId, restrictions);
            }
        }
        return changed;
    }

    @GuardedBy("mRestrictionsLock")
    private Bundle computeEffectiveUserRestrictionsLR(int userId) {
        Bundle baseRestrictions = UserRestrictionsUtils.nonNull((Bundle) this.mBaseUserRestrictions.get(userId));
        Bundle global = UserRestrictionsUtils.mergeAll(this.mDevicePolicyGlobalUserRestrictions);
        Bundle local = (Bundle) this.mDevicePolicyLocalUserRestrictions.get(userId);
        if (UserRestrictionsUtils.isEmpty(global) && UserRestrictionsUtils.isEmpty(local)) {
            return baseRestrictions;
        }
        Bundle effective = UserRestrictionsUtils.clone(baseRestrictions);
        UserRestrictionsUtils.merge(effective, global);
        UserRestrictionsUtils.merge(effective, local);
        return effective;
    }

    @GuardedBy("mRestrictionsLock")
    private void invalidateEffectiveUserRestrictionsLR(int userId) {
        this.mCachedEffectiveUserRestrictions.remove(userId);
    }

    private Bundle getEffectiveUserRestrictions(int userId) {
        Bundle restrictions;
        synchronized (this.mRestrictionsLock) {
            restrictions = (Bundle) this.mCachedEffectiveUserRestrictions.get(userId);
            if (restrictions == null) {
                restrictions = computeEffectiveUserRestrictionsLR(userId);
                this.mCachedEffectiveUserRestrictions.put(userId, restrictions);
            }
        }
        return restrictions;
    }

    public boolean hasUserRestriction(String restrictionKey, int userId) {
        boolean z = false;
        if (!UserRestrictionsUtils.isValidRestriction(restrictionKey)) {
            return false;
        }
        Bundle restrictions = getEffectiveUserRestrictions(userId);
        if (restrictions != null) {
            z = restrictions.getBoolean(restrictionKey);
        }
        return z;
    }

    public int getUserRestrictionSource(String restrictionKey, int userId) {
        List<EnforcingUser> enforcingUsers = getUserRestrictionSources(restrictionKey, userId);
        int result = 0;
        for (int i = enforcingUsers.size() - 1; i >= 0; i--) {
            result |= ((EnforcingUser) enforcingUsers.get(i)).getUserRestrictionSource();
        }
        return result;
    }

    public List<EnforcingUser> getUserRestrictionSources(String restrictionKey, int userId) {
        checkManageUsersPermission("getUserRestrictionSource");
        if (!hasUserRestriction(restrictionKey, userId)) {
            return Collections.emptyList();
        }
        List<EnforcingUser> result = new ArrayList();
        if (hasBaseUserRestriction(restrictionKey, userId)) {
            result.add(new EnforcingUser(-10000, 1));
        }
        synchronized (this.mRestrictionsLock) {
            if (UserRestrictionsUtils.contains((Bundle) this.mDevicePolicyLocalUserRestrictions.get(userId), restrictionKey)) {
                result.add(getEnforcingUserLocked(userId));
            }
            for (int i = this.mDevicePolicyGlobalUserRestrictions.size() - 1; i >= 0; i--) {
                Bundle globalRestrictions = (Bundle) this.mDevicePolicyGlobalUserRestrictions.valueAt(i);
                int profileUserId = this.mDevicePolicyGlobalUserRestrictions.keyAt(i);
                if (UserRestrictionsUtils.contains(globalRestrictions, restrictionKey)) {
                    result.add(getEnforcingUserLocked(profileUserId));
                }
            }
        }
        return result;
    }

    private EnforcingUser getEnforcingUserLocked(int userId) {
        int source;
        if (this.mDeviceOwnerUserId == userId) {
            source = 2;
        } else {
            source = 4;
        }
        return new EnforcingUser(userId, source);
    }

    public Bundle getUserRestrictions(int userId) {
        return UserRestrictionsUtils.clone(getEffectiveUserRestrictions(userId));
    }

    public boolean hasBaseUserRestriction(String restrictionKey, int userId) {
        boolean z = false;
        checkManageUsersPermission("hasBaseUserRestriction");
        if (!UserRestrictionsUtils.isValidRestriction(restrictionKey)) {
            return false;
        }
        synchronized (this.mRestrictionsLock) {
            Bundle bundle = (Bundle) this.mBaseUserRestrictions.get(userId);
            if (bundle != null) {
                z = bundle.getBoolean(restrictionKey, false);
            }
        }
        return z;
    }

    public void setUserRestriction(String key, boolean value, int userId) {
        checkManageUsersPermission("setUserRestriction");
        if (UserRestrictionsUtils.isValidRestriction(key)) {
            synchronized (this.mRestrictionsLock) {
                Bundle newRestrictions = UserRestrictionsUtils.clone((Bundle) this.mBaseUserRestrictions.get(userId));
                newRestrictions.putBoolean(key, value);
                updateUserRestrictionsInternalLR(newRestrictions, userId);
            }
        }
    }

    @GuardedBy("mRestrictionsLock")
    private void updateUserRestrictionsInternalLR(Bundle newBaseRestrictions, final int userId) {
        boolean z = true;
        Bundle prevAppliedRestrictions = UserRestrictionsUtils.nonNull((Bundle) this.mAppliedUserRestrictions.get(userId));
        if (newBaseRestrictions != null) {
            boolean z2;
            if (((Bundle) this.mBaseUserRestrictions.get(userId)) != newBaseRestrictions) {
                z2 = true;
            } else {
                z2 = false;
            }
            Preconditions.checkState(z2);
            if (this.mCachedEffectiveUserRestrictions.get(userId) == newBaseRestrictions) {
                z = false;
            }
            Preconditions.checkState(z);
            if (updateRestrictionsIfNeededLR(userId, newBaseRestrictions, this.mBaseUserRestrictions)) {
                scheduleWriteUser(getUserDataNoChecks(userId));
            }
        }
        final Bundle effective = computeEffectiveUserRestrictionsLR(userId);
        this.mCachedEffectiveUserRestrictions.put(userId, effective);
        if (this.mAppOpsService != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    try {
                        UserManagerService.this.mAppOpsService.setUserRestrictions(effective, UserManagerService.mUserRestriconToken, userId);
                    } catch (RemoteException e) {
                        Log.w(UserManagerService.LOG_TAG, "Unable to notify AppOpsService of UserRestrictions");
                    }
                }
            });
        }
        propagateUserRestrictionsLR(userId, effective, prevAppliedRestrictions);
        this.mAppliedUserRestrictions.put(userId, new Bundle(effective));
    }

    private void propagateUserRestrictionsLR(final int userId, Bundle newRestrictions, Bundle prevRestrictions) {
        if (!UserRestrictionsUtils.areEqual(newRestrictions, prevRestrictions)) {
            final Bundle newRestrictionsFinal = new Bundle(newRestrictions);
            final Bundle prevRestrictionsFinal = new Bundle(prevRestrictions);
            this.mHandler.post(new Runnable() {
                public void run() {
                    UserRestrictionsUtils.applyUserRestrictions(UserManagerService.this.mContext, userId, newRestrictionsFinal, prevRestrictionsFinal);
                    synchronized (UserManagerService.this.mUserRestrictionsListeners) {
                        UserRestrictionsListener[] listeners = new UserRestrictionsListener[UserManagerService.this.mUserRestrictionsListeners.size()];
                        UserManagerService.this.mUserRestrictionsListeners.toArray(listeners);
                    }
                    for (UserRestrictionsListener onUserRestrictionsChanged : listeners) {
                        onUserRestrictionsChanged.onUserRestrictionsChanged(userId, newRestrictionsFinal, prevRestrictionsFinal);
                    }
                    UserManagerService.this.mContext.sendBroadcastAsUser(new Intent("android.os.action.USER_RESTRICTIONS_CHANGED").setFlags(1073741824), UserHandle.of(userId));
                }
            });
        }
    }

    void applyUserRestrictionsLR(int userId) {
        updateUserRestrictionsInternalLR(null, userId);
    }

    @GuardedBy("mRestrictionsLock")
    void applyUserRestrictionsForAllUsersLR() {
        this.mCachedEffectiveUserRestrictions.clear();
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    int[] runningUsers = ActivityManager.getService().getRunningUserIds();
                    synchronized (UserManagerService.this.mRestrictionsLock) {
                        for (int applyUserRestrictionsLR : runningUsers) {
                            UserManagerService.this.applyUserRestrictionsLR(applyUserRestrictionsLR);
                        }
                    }
                } catch (RemoteException e) {
                    Log.w(UserManagerService.LOG_TAG, "Unable to access ActivityManagerService");
                }
            }
        });
    }

    private boolean isUserLimitReached() {
        int count;
        synchronized (this.mUsersLock) {
            count = getAliveUsersExcludingGuestsCountLU();
        }
        return count >= UserManager.getMaxSupportedUsers();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean canAddMoreManagedProfiles(int r9, boolean r10) {
        /*
        r8 = this;
        r4 = 1;
        r5 = 0;
        r6 = "check if more managed profiles can be added.";
        checkManageUsersPermission(r6);
        r6 = android.app.ActivityManager.isLowRamDeviceStatic();
        if (r6 == 0) goto L_0x000f;
    L_0x000e:
        return r5;
    L_0x000f:
        r6 = r8.mContext;
        r6 = r6.getPackageManager();
        r7 = "android.software.managed_users";
        r6 = r6.hasSystemFeature(r7);
        if (r6 != 0) goto L_0x001f;
    L_0x001e:
        return r5;
    L_0x001f:
        r6 = r8.getProfiles(r9, r5);
        r6 = r6.size();
        r0 = r6 + -1;
        if (r0 <= 0) goto L_0x0037;
    L_0x002b:
        if (r10 == 0) goto L_0x0037;
    L_0x002d:
        r1 = 1;
    L_0x002e:
        r6 = r0 - r1;
        r7 = getMaxManagedProfiles();
        if (r6 < r7) goto L_0x0039;
    L_0x0036:
        return r5;
    L_0x0037:
        r1 = 0;
        goto L_0x002e;
    L_0x0039:
        r6 = r8.mUsersLock;
        monitor-enter(r6);
        r2 = r8.getUserInfoLU(r9);	 Catch:{ all -> 0x005e }
        if (r2 == 0) goto L_0x004a;
    L_0x0042:
        r7 = r2.canHaveProfile();	 Catch:{ all -> 0x005e }
        r7 = r7 ^ 1;
        if (r7 == 0) goto L_0x004c;
    L_0x004a:
        monitor-exit(r6);
        return r5;
    L_0x004c:
        r7 = r8.getAliveUsersExcludingGuestsCountLU();	 Catch:{ all -> 0x005e }
        r3 = r7 - r1;
        if (r3 == r4) goto L_0x005a;
    L_0x0054:
        r7 = android.os.UserManager.getMaxSupportedUsers();	 Catch:{ all -> 0x005e }
        if (r3 >= r7) goto L_0x005c;
    L_0x005a:
        monitor-exit(r6);
        return r4;
    L_0x005c:
        r4 = r5;
        goto L_0x005a;
    L_0x005e:
        r4 = move-exception;
        monitor-exit(r6);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.canAddMoreManagedProfiles(int, boolean):boolean");
    }

    private int getAliveUsersExcludingGuestsCountLU() {
        int aliveUserCount = 0;
        int totalUserCount = this.mUsers.size();
        for (int i = 0; i < totalUserCount; i++) {
            UserInfo user = ((UserData) this.mUsers.valueAt(i)).info;
            if (!(this.mRemovingUserIds.get(user.id) || (user.isGuest() ^ 1) == 0)) {
                aliveUserCount++;
            }
        }
        return aliveUserCount;
    }

    private static final void checkManageUserAndAcrossUsersFullPermission(String message) {
        int uid = Binder.getCallingUid();
        if (uid != 1000 && uid != 0 && ActivityManager.checkComponentPermission("android.permission.MANAGE_USERS", uid, -1, true) != 0 && ActivityManager.checkComponentPermission("android.permission.INTERACT_ACROSS_USERS_FULL", uid, -1, true) != 0) {
            throw new SecurityException("You need MANAGE_USERS and INTERACT_ACROSS_USERS_FULL permission to: " + message);
        }
    }

    private static final void checkManageUsersPermission(String message) {
        if (!hasManageUsersPermission()) {
            throw new SecurityException("You need MANAGE_USERS permission to: " + message);
        }
    }

    private static final void checkManageOrCreateUsersPermission(String message) {
        if (!hasManageOrCreateUsersPermission()) {
            throw new SecurityException("You either need MANAGE_USERS or CREATE_USERS permission to: " + message);
        }
    }

    private static final void checkManageOrCreateUsersPermission(int creationFlags) {
        if ((creationFlags & -813) == 0) {
            if (!hasManageOrCreateUsersPermission()) {
                throw new SecurityException("You either need MANAGE_USERS or CREATE_USERS permission to create an user with flags: " + creationFlags);
            }
        } else if (!hasManageUsersPermission()) {
            throw new SecurityException("You need MANAGE_USERS permission to create an user  with flags: " + creationFlags);
        }
    }

    private static final boolean hasManageUsersPermission() {
        int callingUid = Binder.getCallingUid();
        if (UserHandle.isSameApp(callingUid, 1000) || callingUid == 0 || ActivityManager.checkComponentPermission("android.permission.MANAGE_USERS", callingUid, -1, true) == 0) {
            return true;
        }
        return false;
    }

    private static final boolean hasManageOrCreateUsersPermission() {
        int callingUid = Binder.getCallingUid();
        if (UserHandle.isSameApp(callingUid, 1000) || callingUid == 0 || ActivityManager.checkComponentPermission("android.permission.MANAGE_USERS", callingUid, -1, true) == 0 || ActivityManager.checkComponentPermission("android.permission.CREATE_USERS", callingUid, -1, true) == 0) {
            return true;
        }
        return false;
    }

    private static void checkSystemOrRoot(String message) {
        int uid = Binder.getCallingUid();
        if (!UserHandle.isSameApp(uid, 1000) && uid != 0) {
            throw new SecurityException("Only system may: " + message);
        }
    }

    private void writeBitmapLP(UserInfo info, Bitmap bitmap) {
        try {
            File dir = new File(this.mUsersDir, Integer.toString(info.id));
            File file = new File(dir, USER_PHOTO_FILENAME);
            File tmp = new File(dir, USER_PHOTO_FILENAME_TMP);
            if (!dir.exists()) {
                dir.mkdir();
                FileUtils.setPermissions(dir.getPath(), 505, -1, -1);
            }
            CompressFormat compressFormat = CompressFormat.PNG;
            FileOutputStream os = new FileOutputStream(tmp);
            if (bitmap.compress(compressFormat, 100, os) && tmp.renameTo(file) && SELinux.restorecon(file)) {
                info.iconPath = file.getAbsolutePath();
            }
            try {
                os.close();
            } catch (IOException e) {
            }
            tmp.delete();
        } catch (FileNotFoundException e2) {
            Slog.w(LOG_TAG, "Error setting photo for user ", e2);
        }
    }

    public int[] getUserIds() {
        int[] iArr;
        synchronized (this.mUsersLock) {
            iArr = this.mUserIds;
        }
        return iArr;
    }

    private void readUserListLP() {
        if (this.mUserListFile.exists()) {
            int type;
            AutoCloseable autoCloseable = null;
            autoCloseable = new AtomicFile(this.mUserListFile).openRead();
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(autoCloseable, StandardCharsets.UTF_8.name());
            do {
                type = parser.next();
                if (type == 2) {
                    break;
                }
            } while (type != 1);
            if (type != 2) {
                Slog.e(LOG_TAG, "Unable to read user list");
                fallbackToSingleUserLP();
                IoUtils.closeQuietly(autoCloseable);
                return;
            }
            this.mNextSerialNumber = -1;
            if (parser.getName().equals("users")) {
                String lastSerialNumber = parser.getAttributeValue(null, ATTR_NEXT_SERIAL_NO);
                if (lastSerialNumber != null) {
                    this.mNextSerialNumber = Integer.parseInt(lastSerialNumber);
                }
                String versionNumber = parser.getAttributeValue(null, ATTR_USER_VERSION);
                if (versionNumber != null) {
                    this.mUserVersion = Integer.parseInt(versionNumber);
                }
            }
            Bundle bundle = null;
            while (true) {
                type = parser.next();
                if (type == 1) {
                    break;
                } else if (type == 2) {
                    String name = parser.getName();
                    if (name.equals(TAG_USER)) {
                        UserData userData = readUserLP(Integer.parseInt(parser.getAttributeValue(null, ATTR_ID)));
                        if (userData != null) {
                            synchronized (this.mUsersLock) {
                                this.mUsers.put(userData.info.id, userData);
                                if (this.mNextSerialNumber < 0 || this.mNextSerialNumber <= userData.info.id) {
                                    this.mNextSerialNumber = userData.info.id + 1;
                                }
                            }
                        } else {
                            continue;
                        }
                    } else if (name.equals(TAG_GUEST_RESTRICTIONS)) {
                        do {
                            type = parser.next();
                            if (type == 1 || type == 3) {
                                break;
                            }
                        } while (type != 2);
                        if (parser.getName().equals(TAG_RESTRICTIONS)) {
                            synchronized (this.mGuestRestrictions) {
                                UserRestrictionsUtils.readRestrictions(parser, this.mGuestRestrictions);
                                try {
                                } catch (IOException e) {
                                    fallbackToSingleUserLP();
                                    IoUtils.closeQuietly(autoCloseable);
                                } catch (Throwable th) {
                                    IoUtils.closeQuietly(autoCloseable);
                                }
                            }
                        } else {
                            continue;
                        }
                    } else if (name.equals(TAG_DEVICE_OWNER_USER_ID) || name.equals(TAG_GLOBAL_RESTRICTION_OWNER_ID)) {
                        String ownerUserId = parser.getAttributeValue(null, ATTR_ID);
                        if (ownerUserId != null) {
                            this.mDeviceOwnerUserId = Integer.parseInt(ownerUserId);
                        }
                    } else if (name.equals(TAG_DEVICE_POLICY_RESTRICTIONS)) {
                        bundle = UserRestrictionsUtils.readRestrictions(parser);
                    }
                }
            }
            updateUserIds();
            upgradeIfNecessaryLP(bundle);
            IoUtils.closeQuietly(autoCloseable);
            return;
        }
        fallbackToSingleUserLP();
    }

    private void upgradeIfNecessaryLP(Bundle oldGlobalUserRestrictions) {
        UserData userData;
        int originalVersion = this.mUserVersion;
        int userVersion = this.mUserVersion;
        if (userVersion < 1) {
            userData = getUserDataNoChecks(0);
            if ("Primary".equals(userData.info.name)) {
                userData.info.name = this.mContext.getResources().getString(17040342);
                scheduleWriteUser(userData);
            }
            userVersion = 1;
        }
        if (userVersion < 2) {
            userData = getUserDataNoChecks(0);
            if ((userData.info.flags & 16) == 0) {
                UserInfo userInfo = userData.info;
                userInfo.flags |= 16;
                scheduleWriteUser(userData);
            }
            userVersion = 2;
        }
        if (userVersion < 4) {
            userVersion = 4;
        }
        if (userVersion < 5) {
            initDefaultGuestRestrictions();
            userVersion = 5;
        }
        if (userVersion < 6) {
            boolean splitSystemUser = UserManager.isSplitSystemUser();
            synchronized (this.mUsersLock) {
                for (int i = 0; i < this.mUsers.size(); i++) {
                    userData = (UserData) this.mUsers.valueAt(i);
                    if (!splitSystemUser && userData.info.isRestricted() && userData.info.restrictedProfileParentId == -10000) {
                        userData.info.restrictedProfileParentId = 0;
                        scheduleWriteUser(userData);
                    }
                }
            }
            userVersion = 6;
        }
        if (userVersion < 7) {
            synchronized (this.mRestrictionsLock) {
                if (!(UserRestrictionsUtils.isEmpty(oldGlobalUserRestrictions) || this.mDeviceOwnerUserId == -10000)) {
                    this.mDevicePolicyGlobalUserRestrictions.put(this.mDeviceOwnerUserId, oldGlobalUserRestrictions);
                }
                UserRestrictionsUtils.moveRestriction("ensure_verify_apps", this.mDevicePolicyLocalUserRestrictions, this.mDevicePolicyGlobalUserRestrictions);
            }
            userVersion = 7;
        }
        if (userVersion < 7) {
            Slog.w(LOG_TAG, "User version " + this.mUserVersion + " didn't upgrade as expected to " + 7);
            return;
        }
        this.mUserVersion = userVersion;
        if (originalVersion < this.mUserVersion) {
            writeUserListLP();
        }
    }

    private void fallbackToSingleUserLP() {
        int i = 0;
        int flags = 16;
        if (!UserManager.isSplitSystemUser()) {
            flags = 19;
        }
        UserData userData = putUserInfo(new UserInfo(0, null, null, flags));
        this.mNextSerialNumber = 10;
        this.mUserVersion = 7;
        Bundle restrictions = new Bundle();
        try {
            String[] defaultFirstUserRestrictions = this.mContext.getResources().getStringArray(17235994);
            int length = defaultFirstUserRestrictions.length;
            while (i < length) {
                String userRestriction = defaultFirstUserRestrictions[i];
                if (UserRestrictionsUtils.isValidRestriction(userRestriction)) {
                    restrictions.putBoolean(userRestriction, true);
                }
                i++;
            }
        } catch (NotFoundException e) {
            Log.e(LOG_TAG, "Couldn't find resource: config_defaultFirstUserRestrictions", e);
        }
        if (!restrictions.isEmpty()) {
            synchronized (this.mRestrictionsLock) {
                this.mBaseUserRestrictions.append(0, restrictions);
            }
        }
        updateUserIds();
        initDefaultGuestRestrictions();
        writeUserLP(userData);
        writeUserListLP();
    }

    private String getOwnerName() {
        return this.mContext.getResources().getString(17040342);
    }

    private void scheduleWriteUser(UserData UserData) {
        if (!this.mHandler.hasMessages(1, UserData)) {
            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1, UserData), 2000);
        }
    }

    private void writeUserLP(UserData userData) {
        FileOutputStream fileOutputStream = null;
        AtomicFile userFile = new AtomicFile(new File(this.mUsersDir, userData.info.id + XML_SUFFIX));
        try {
            fileOutputStream = userFile.startWrite();
            writeUserLP(userData, new BufferedOutputStream(fileOutputStream));
            userFile.finishWrite(fileOutputStream);
        } catch (Exception ioe) {
            Slog.e(LOG_TAG, "Error writing user info " + userData.info.id, ioe);
            userFile.failWrite(fileOutputStream);
        }
    }

    void writeUserLP(UserData userData, OutputStream os) throws IOException, XmlPullParserException {
        XmlSerializer serializer = new FastXmlSerializer();
        serializer.setOutput(os, StandardCharsets.UTF_8.name());
        serializer.startDocument(null, Boolean.valueOf(true));
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        UserInfo userInfo = userData.info;
        serializer.startTag(null, TAG_USER);
        serializer.attribute(null, ATTR_ID, Integer.toString(userInfo.id));
        serializer.attribute(null, ATTR_SERIAL_NO, Integer.toString(userInfo.serialNumber));
        serializer.attribute(null, ATTR_FLAGS, Integer.toString(userInfo.flags));
        serializer.attribute(null, ATTR_CREATION_TIME, Long.toString(userInfo.creationTime));
        serializer.attribute(null, ATTR_LAST_LOGGED_IN_TIME, Long.toString(userInfo.lastLoggedInTime));
        if (userInfo.lastLoggedInFingerprint != null) {
            serializer.attribute(null, ATTR_LAST_LOGGED_IN_FINGERPRINT, userInfo.lastLoggedInFingerprint);
        }
        if (userInfo.iconPath != null) {
            serializer.attribute(null, ATTR_ICON_PATH, userInfo.iconPath);
        }
        if (userInfo.partial) {
            serializer.attribute(null, ATTR_PARTIAL, "true");
        }
        if (userInfo.guestToRemove) {
            serializer.attribute(null, ATTR_GUEST_TO_REMOVE, "true");
        }
        if (userInfo.profileGroupId != -10000) {
            serializer.attribute(null, ATTR_PROFILE_GROUP_ID, Integer.toString(userInfo.profileGroupId));
        }
        serializer.attribute(null, ATTR_PROFILE_BADGE, Integer.toString(userInfo.profileBadge));
        if (userInfo.restrictedProfileParentId != -10000) {
            serializer.attribute(null, ATTR_RESTRICTED_PROFILE_PARENT_ID, Integer.toString(userInfo.restrictedProfileParentId));
        }
        if (userData.persistSeedData) {
            if (userData.seedAccountName != null) {
                serializer.attribute(null, ATTR_SEED_ACCOUNT_NAME, userData.seedAccountName);
            }
            if (userData.seedAccountType != null) {
                serializer.attribute(null, ATTR_SEED_ACCOUNT_TYPE, userData.seedAccountType);
            }
        }
        if (userInfo.name != null) {
            serializer.startTag(null, TAG_NAME);
            serializer.text(userInfo.name);
            serializer.endTag(null, TAG_NAME);
        }
        synchronized (this.mRestrictionsLock) {
            UserRestrictionsUtils.writeRestrictions(serializer, (Bundle) this.mBaseUserRestrictions.get(userInfo.id), TAG_RESTRICTIONS);
            UserRestrictionsUtils.writeRestrictions(serializer, (Bundle) this.mDevicePolicyLocalUserRestrictions.get(userInfo.id), TAG_DEVICE_POLICY_RESTRICTIONS);
            UserRestrictionsUtils.writeRestrictions(serializer, (Bundle) this.mDevicePolicyGlobalUserRestrictions.get(userInfo.id), TAG_DEVICE_POLICY_GLOBAL_RESTRICTIONS);
        }
        if (userData.account != null) {
            serializer.startTag(null, TAG_ACCOUNT);
            serializer.text(userData.account);
            serializer.endTag(null, TAG_ACCOUNT);
        }
        if (userData.persistSeedData && userData.seedAccountOptions != null) {
            serializer.startTag(null, TAG_SEED_ACCOUNT_OPTIONS);
            userData.seedAccountOptions.saveToXml(serializer);
            serializer.endTag(null, TAG_SEED_ACCOUNT_OPTIONS);
        }
        serializer.endTag(null, TAG_USER);
        serializer.endDocument();
    }

    private void writeUserListLP() {
        FileOutputStream fileOutputStream = null;
        AtomicFile userListFile = new AtomicFile(this.mUserListFile);
        try {
            fileOutputStream = userListFile.startWrite();
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            XmlSerializer serializer = new FastXmlSerializer();
            serializer.setOutput(bos, StandardCharsets.UTF_8.name());
            serializer.startDocument(null, Boolean.valueOf(true));
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            serializer.startTag(null, "users");
            serializer.attribute(null, ATTR_NEXT_SERIAL_NO, Integer.toString(this.mNextSerialNumber));
            serializer.attribute(null, ATTR_USER_VERSION, Integer.toString(this.mUserVersion));
            serializer.startTag(null, TAG_GUEST_RESTRICTIONS);
            synchronized (this.mGuestRestrictions) {
                UserRestrictionsUtils.writeRestrictions(serializer, this.mGuestRestrictions, TAG_RESTRICTIONS);
            }
            serializer.endTag(null, TAG_GUEST_RESTRICTIONS);
            serializer.startTag(null, TAG_DEVICE_OWNER_USER_ID);
            serializer.attribute(null, ATTR_ID, Integer.toString(this.mDeviceOwnerUserId));
            serializer.endTag(null, TAG_DEVICE_OWNER_USER_ID);
            synchronized (this.mUsersLock) {
                int[] userIdsToWrite = new int[this.mUsers.size()];
                for (int i = 0; i < userIdsToWrite.length; i++) {
                    userIdsToWrite[i] = ((UserData) this.mUsers.valueAt(i)).info.id;
                }
            }
            for (int id : userIdsToWrite) {
                serializer.startTag(null, TAG_USER);
                serializer.attribute(null, ATTR_ID, Integer.toString(id));
                serializer.endTag(null, TAG_USER);
            }
            serializer.endTag(null, "users");
            serializer.endDocument();
            userListFile.finishWrite(fileOutputStream);
        } catch (Exception e) {
            userListFile.failWrite(fileOutputStream);
            Slog.e(LOG_TAG, "Error writing user list");
        }
    }

    private UserData readUserLP(int id) {
        AutoCloseable autoCloseable = null;
        UserData readUserLP;
        try {
            autoCloseable = new AtomicFile(new File(this.mUsersDir, Integer.toString(id) + XML_SUFFIX)).openRead();
            readUserLP = readUserLP(id, autoCloseable);
            return readUserLP;
        } catch (IOException e) {
            readUserLP = LOG_TAG;
            Slog.e(readUserLP, "Error reading user list");
            return null;
        } catch (XmlPullParserException e2) {
            readUserLP = LOG_TAG;
            Slog.e(readUserLP, "Error reading user list");
            return null;
        } finally {
            IoUtils.closeQuietly(autoCloseable);
        }
    }

    UserData readUserLP(int id, InputStream is) throws IOException, XmlPullParserException {
        int type;
        int flags = 0;
        int serialNumber = id;
        String str = null;
        String str2 = null;
        String str3 = null;
        long creationTime = 0;
        long lastLoggedInTime = 0;
        String str4 = null;
        int profileGroupId = -10000;
        int profileBadge = 0;
        int restrictedProfileParentId = -10000;
        boolean partial = false;
        boolean guestToRemove = false;
        boolean persistSeedData = false;
        String str5 = null;
        String str6 = null;
        PersistableBundle persistableBundle = null;
        Object obj = null;
        Object obj2 = null;
        Object obj3 = null;
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(is, StandardCharsets.UTF_8.name());
        do {
            type = parser.next();
            if (type == 2) {
                break;
            }
        } while (type != 1);
        if (type != 2) {
            Slog.e(LOG_TAG, "Unable to read user " + id);
            return null;
        }
        if (type == 2 && parser.getName().equals(TAG_USER)) {
            if (readIntAttribute(parser, ATTR_ID, -1) == id) {
                serialNumber = readIntAttribute(parser, ATTR_SERIAL_NO, id);
                flags = readIntAttribute(parser, ATTR_FLAGS, 0);
                str3 = parser.getAttributeValue(null, ATTR_ICON_PATH);
                creationTime = readLongAttribute(parser, ATTR_CREATION_TIME, 0);
                lastLoggedInTime = readLongAttribute(parser, ATTR_LAST_LOGGED_IN_TIME, 0);
                str4 = parser.getAttributeValue(null, ATTR_LAST_LOGGED_IN_FINGERPRINT);
                profileGroupId = readIntAttribute(parser, ATTR_PROFILE_GROUP_ID, -10000);
                profileBadge = readIntAttribute(parser, ATTR_PROFILE_BADGE, 0);
                restrictedProfileParentId = readIntAttribute(parser, ATTR_RESTRICTED_PROFILE_PARENT_ID, -10000);
                if ("true".equals(parser.getAttributeValue(null, ATTR_PARTIAL))) {
                    partial = true;
                }
                if ("true".equals(parser.getAttributeValue(null, ATTR_GUEST_TO_REMOVE))) {
                    guestToRemove = true;
                }
                str5 = parser.getAttributeValue(null, ATTR_SEED_ACCOUNT_NAME);
                str6 = parser.getAttributeValue(null, ATTR_SEED_ACCOUNT_TYPE);
                if (!(str5 == null && str6 == null)) {
                    persistSeedData = true;
                }
                int outerDepth = parser.getDepth();
                while (true) {
                    type = parser.next();
                    if (type == 1 || (type == 3 && parser.getDepth() <= outerDepth)) {
                        break;
                    } else if (!(type == 3 || type == 4)) {
                        String tag = parser.getName();
                        if (TAG_NAME.equals(tag)) {
                            if (parser.next() == 4) {
                                str = parser.getText();
                            }
                        } else if (TAG_RESTRICTIONS.equals(tag)) {
                            obj = UserRestrictionsUtils.readRestrictions(parser);
                        } else if (TAG_DEVICE_POLICY_RESTRICTIONS.equals(tag)) {
                            obj2 = UserRestrictionsUtils.readRestrictions(parser);
                        } else if (TAG_DEVICE_POLICY_GLOBAL_RESTRICTIONS.equals(tag)) {
                            obj3 = UserRestrictionsUtils.readRestrictions(parser);
                        } else if (TAG_ACCOUNT.equals(tag)) {
                            if (parser.next() == 4) {
                                str2 = parser.getText();
                            }
                        } else if (TAG_SEED_ACCOUNT_OPTIONS.equals(tag)) {
                            persistableBundle = PersistableBundle.restoreFromXml(parser);
                            persistSeedData = true;
                        }
                    }
                }
            } else {
                Slog.e(LOG_TAG, "User id does not match the file name");
                return null;
            }
        }
        UserInfo userInfo = new UserInfo(id, str, str3, flags);
        userInfo.serialNumber = serialNumber;
        userInfo.creationTime = creationTime;
        userInfo.lastLoggedInTime = lastLoggedInTime;
        userInfo.lastLoggedInFingerprint = str4;
        userInfo.partial = partial;
        userInfo.guestToRemove = guestToRemove;
        userInfo.profileGroupId = profileGroupId;
        userInfo.profileBadge = profileBadge;
        userInfo.restrictedProfileParentId = restrictedProfileParentId;
        UserData userData = new UserData();
        userData.info = userInfo;
        userData.account = str2;
        userData.seedAccountName = str5;
        userData.seedAccountType = str6;
        userData.persistSeedData = persistSeedData;
        userData.seedAccountOptions = persistableBundle;
        synchronized (this.mRestrictionsLock) {
            if (obj != null) {
                this.mBaseUserRestrictions.put(id, obj);
            }
            if (obj2 != null) {
                this.mDevicePolicyLocalUserRestrictions.put(id, obj2);
            }
            if (obj3 != null) {
                this.mDevicePolicyGlobalUserRestrictions.put(id, obj3);
            }
        }
        return userData;
    }

    private int readIntAttribute(XmlPullParser parser, String attr, int defaultValue) {
        String valueString = parser.getAttributeValue(null, attr);
        if (valueString == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(valueString);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private long readLongAttribute(XmlPullParser parser, String attr, long defaultValue) {
        String valueString = parser.getAttributeValue(null, attr);
        if (valueString == null) {
            return defaultValue;
        }
        try {
            return Long.parseLong(valueString);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private static void cleanAppRestrictionsForPackageLAr(String pkg, int userId) {
        File resFile = new File(Environment.getUserSystemDirectory(userId), packageToRestrictionsFileName(pkg));
        if (resFile.exists()) {
            resFile.delete();
        }
    }

    public UserInfo createProfileForUser(String name, int flags, int userId, String[] disallowedPackages) {
        checkManageOrCreateUsersPermission(flags);
        return createUserInternal(name, flags, userId, disallowedPackages);
    }

    public UserInfo createProfileForUserEvenWhenDisallowed(String name, int flags, int userId, String[] disallowedPackages) {
        checkManageOrCreateUsersPermission(flags);
        return createUserInternalUnchecked(name, flags, userId, disallowedPackages);
    }

    public boolean removeUserEvenWhenDisallowed(int userHandle) {
        checkManageOrCreateUsersPermission("Only the system can remove users");
        return removeUserUnchecked(userHandle);
    }

    public UserInfo createUser(String name, int flags) {
        checkManageOrCreateUsersPermission(flags);
        return createUserInternal(name, flags, -10000);
    }

    private UserInfo createUserInternal(String name, int flags, int parentId) {
        return createUserInternal(name, flags, parentId, null);
    }

    private UserInfo createUserInternal(String name, int flags, int parentId, String[] disallowedPackages) {
        String restriction;
        if ((flags & 32) != 0) {
            restriction = "no_add_managed_profile";
        } else {
            restriction = "no_add_user";
        }
        if (!hasUserRestriction(restriction, UserHandle.getCallingUserId())) {
            return createUserInternalUnchecked(name, flags, parentId, disallowedPackages);
        }
        Log.w(LOG_TAG, "Cannot add user. " + restriction + " is enabled.");
        return null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.content.pm.UserInfo createUserInternalUnchecked(java.lang.String r29, int r30, int r31, java.lang.String[] r32) {
        /*
        r28 = this;
        r23 = com.android.server.storage.DeviceStorageMonitorInternal.class;
        r7 = com.android.server.LocalServices.getService(r23);
        r7 = (com.android.server.storage.DeviceStorageMonitorInternal) r7;
        r23 = r7.isMemoryLow();
        if (r23 == 0) goto L_0x001a;
    L_0x000e:
        r23 = "UserManagerService";
        r24 = "Cannot add user. Not enough space on disk.";
        android.util.Log.w(r23, r24);
        r23 = 0;
        return r23;
    L_0x001a:
        r23 = android.app.ActivityManager.isLowRamDeviceStatic();
        if (r23 == 0) goto L_0x0023;
    L_0x0020:
        r23 = 0;
        return r23;
    L_0x0023:
        r23 = r30 & 4;
        if (r23 == 0) goto L_0x0068;
    L_0x0027:
        r12 = 1;
    L_0x0028:
        r23 = r30 & 32;
        if (r23 == 0) goto L_0x006a;
    L_0x002c:
        r13 = 1;
    L_0x002d:
        r23 = r30 & 8;
        if (r23 == 0) goto L_0x006c;
    L_0x0031:
        r14 = 1;
    L_0x0032:
        r0 = r30;
        r0 = r0 & 512;
        r23 = r0;
        if (r23 == 0) goto L_0x006e;
    L_0x003a:
        r9 = 1;
    L_0x003b:
        r10 = android.os.Binder.clearCallingIdentity();
        r0 = r28;
        r0 = r0.mPackagesLock;	 Catch:{ all -> 0x0076 }
        r24 = r0;
        monitor-enter(r24);	 Catch:{ all -> 0x0076 }
        r15 = 0;
        r23 = -10000; // 0xffffffffffffd8f0 float:NaN double:NaN;
        r0 = r31;
        r1 = r23;
        if (r0 == r1) goto L_0x007b;
    L_0x004f:
        r0 = r28;
        r0 = r0.mUsersLock;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        monitor-enter(r23);	 Catch:{ all -> 0x0073 }
        r0 = r28;
        r1 = r31;
        r15 = r0.getUserDataLU(r1);	 Catch:{ all -> 0x0070 }
        monitor-exit(r23);	 Catch:{ all -> 0x0073 }
        if (r15 != 0) goto L_0x007b;
    L_0x0061:
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        android.os.Binder.restoreCallingIdentity(r10);
        return r23;
    L_0x0068:
        r12 = 0;
        goto L_0x0028;
    L_0x006a:
        r13 = 0;
        goto L_0x002d;
    L_0x006c:
        r14 = 0;
        goto L_0x0032;
    L_0x006e:
        r9 = 0;
        goto L_0x003b;
    L_0x0070:
        r25 = move-exception;
        monitor-exit(r23);	 Catch:{ all -> 0x0073 }
        throw r25;	 Catch:{ all -> 0x0073 }
    L_0x0073:
        r23 = move-exception;
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        throw r23;	 Catch:{ all -> 0x0076 }
    L_0x0076:
        r23 = move-exception;
        android.os.Binder.restoreCallingIdentity(r10);
        throw r23;
    L_0x007b:
        if (r13 == 0) goto L_0x00b6;
    L_0x007d:
        r23 = 0;
        r0 = r28;
        r1 = r31;
        r2 = r23;
        r23 = r0.canAddMoreManagedProfiles(r1, r2);	 Catch:{ all -> 0x0073 }
        r23 = r23 ^ 1;
        if (r23 == 0) goto L_0x00b6;
    L_0x008d:
        r23 = "UserManagerService";
        r25 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0073 }
        r25.<init>();	 Catch:{ all -> 0x0073 }
        r26 = "Cannot add more managed profiles for user ";
        r25 = r25.append(r26);	 Catch:{ all -> 0x0073 }
        r0 = r25;
        r1 = r31;
        r25 = r0.append(r1);	 Catch:{ all -> 0x0073 }
        r25 = r25.toString();	 Catch:{ all -> 0x0073 }
        r0 = r23;
        r1 = r25;
        android.util.Log.e(r0, r1);	 Catch:{ all -> 0x0073 }
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        android.os.Binder.restoreCallingIdentity(r10);
        return r23;
    L_0x00b6:
        if (r12 != 0) goto L_0x00cd;
    L_0x00b8:
        r23 = r13 ^ 1;
        if (r23 == 0) goto L_0x00cd;
    L_0x00bc:
        r23 = r9 ^ 1;
        if (r23 == 0) goto L_0x00cd;
    L_0x00c0:
        r23 = r28.isUserLimitReached();	 Catch:{ all -> 0x0073 }
        if (r23 == 0) goto L_0x00cd;
    L_0x00c6:
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        android.os.Binder.restoreCallingIdentity(r10);
        return r23;
    L_0x00cd:
        if (r12 == 0) goto L_0x00dc;
    L_0x00cf:
        r23 = r28.findCurrentGuestUser();	 Catch:{ all -> 0x0073 }
        if (r23 == 0) goto L_0x00dc;
    L_0x00d5:
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        android.os.Binder.restoreCallingIdentity(r10);
        return r23;
    L_0x00dc:
        if (r14 == 0) goto L_0x00fc;
    L_0x00de:
        r23 = android.os.UserManager.isSplitSystemUser();	 Catch:{ all -> 0x0073 }
        r23 = r23 ^ 1;
        if (r23 == 0) goto L_0x00fc;
    L_0x00e6:
        if (r31 == 0) goto L_0x00fc;
    L_0x00e8:
        r23 = "UserManagerService";
        r25 = "Cannot add restricted profile - parent user must be owner";
        r0 = r23;
        r1 = r25;
        android.util.Log.w(r0, r1);	 Catch:{ all -> 0x0073 }
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        android.os.Binder.restoreCallingIdentity(r10);
        return r23;
    L_0x00fc:
        if (r14 == 0) goto L_0x014d;
    L_0x00fe:
        r23 = android.os.UserManager.isSplitSystemUser();	 Catch:{ all -> 0x0073 }
        if (r23 == 0) goto L_0x014d;
    L_0x0104:
        if (r15 != 0) goto L_0x011a;
    L_0x0106:
        r23 = "UserManagerService";
        r25 = "Cannot add restricted profile - parent user must be specified";
        r0 = r23;
        r1 = r25;
        android.util.Log.w(r0, r1);	 Catch:{ all -> 0x0073 }
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        android.os.Binder.restoreCallingIdentity(r10);
        return r23;
    L_0x011a:
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r23 = r23.canHaveProfile();	 Catch:{ all -> 0x0073 }
        if (r23 != 0) goto L_0x014d;
    L_0x0124:
        r23 = "UserManagerService";
        r25 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0073 }
        r25.<init>();	 Catch:{ all -> 0x0073 }
        r26 = "Cannot add restricted profile - profiles cannot be created for the specified parent user id ";
        r25 = r25.append(r26);	 Catch:{ all -> 0x0073 }
        r0 = r25;
        r1 = r31;
        r25 = r0.append(r1);	 Catch:{ all -> 0x0073 }
        r25 = r25.toString();	 Catch:{ all -> 0x0073 }
        r0 = r23;
        r1 = r25;
        android.util.Log.w(r0, r1);	 Catch:{ all -> 0x0073 }
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        android.os.Binder.restoreCallingIdentity(r10);
        return r23;
    L_0x014d:
        r23 = android.os.UserManager.isSplitSystemUser();	 Catch:{ all -> 0x0073 }
        if (r23 != 0) goto L_0x0177;
    L_0x0153:
        r0 = r30;
        r0 = r0 & 256;
        r23 = r0;
        if (r23 == 0) goto L_0x0177;
    L_0x015b:
        r0 = r30;
        r0 = r0 & 512;
        r23 = r0;
        if (r23 != 0) goto L_0x0177;
    L_0x0163:
        r23 = "UserManagerService";
        r25 = "Ephemeral users are supported on split-system-user systems only.";
        r0 = r23;
        r1 = r25;
        android.util.Log.e(r0, r1);	 Catch:{ all -> 0x0073 }
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        android.os.Binder.restoreCallingIdentity(r10);
        return r23;
    L_0x0177:
        r23 = android.os.UserManager.isSplitSystemUser();	 Catch:{ all -> 0x0073 }
        if (r23 == 0) goto L_0x019f;
    L_0x017d:
        r23 = r12 ^ 1;
        if (r23 == 0) goto L_0x019f;
    L_0x0181:
        r23 = r13 ^ 1;
        if (r23 == 0) goto L_0x019f;
    L_0x0185:
        r23 = r28.getPrimaryUser();	 Catch:{ all -> 0x0073 }
        if (r23 != 0) goto L_0x019f;
    L_0x018b:
        r30 = r30 | 1;
        r0 = r28;
        r0 = r0.mUsersLock;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        monitor-enter(r23);	 Catch:{ all -> 0x0073 }
        r0 = r28;
        r0 = r0.mIsDeviceManaged;	 Catch:{ all -> 0x038a }
        r25 = r0;
        if (r25 != 0) goto L_0x019e;
    L_0x019c:
        r30 = r30 | 2;
    L_0x019e:
        monitor-exit(r23);	 Catch:{ all -> 0x0073 }
    L_0x019f:
        r21 = r28.getNextAvailableId();	 Catch:{ all -> 0x0073 }
        r23 = android.os.Environment.getUserSystemDirectory(r21);	 Catch:{ all -> 0x0073 }
        r23.mkdirs();	 Catch:{ all -> 0x0073 }
        r23 = android.content.res.Resources.getSystem();	 Catch:{ all -> 0x0073 }
        r25 = 17956971; // 0x112006b float:2.6816265E-38 double:8.8719225E-317;
        r0 = r23;
        r1 = r25;
        r8 = r0.getBoolean(r1);	 Catch:{ all -> 0x0073 }
        r0 = r28;
        r0 = r0.mUsersLock;	 Catch:{ all -> 0x0073 }
        r25 = r0;
        monitor-enter(r25);	 Catch:{ all -> 0x0073 }
        if (r12 == 0) goto L_0x01c4;
    L_0x01c2:
        if (r8 != 0) goto L_0x01d8;
    L_0x01c4:
        r0 = r28;
        r0 = r0.mForceEphemeralUsers;	 Catch:{ all -> 0x0391 }
        r23 = r0;
        if (r23 != 0) goto L_0x01d8;
    L_0x01cc:
        if (r15 == 0) goto L_0x01de;
    L_0x01ce:
        r0 = r15.info;	 Catch:{ all -> 0x0391 }
        r23 = r0;
        r23 = r23.isEphemeral();	 Catch:{ all -> 0x0391 }
        if (r23 == 0) goto L_0x01de;
    L_0x01d8:
        r0 = r30;
        r0 = r0 | 256;
        r30 = r0;
    L_0x01de:
        r22 = new android.content.pm.UserInfo;	 Catch:{ all -> 0x0391 }
        r23 = 0;
        r0 = r22;
        r1 = r21;
        r2 = r29;
        r3 = r23;
        r4 = r30;
        r0.<init>(r1, r2, r3, r4);	 Catch:{ all -> 0x0391 }
        r0 = r28;
        r0 = r0.mNextSerialNumber;	 Catch:{ all -> 0x0391 }
        r23 = r0;
        r26 = r23 + 1;
        r0 = r26;
        r1 = r28;
        r1.mNextSerialNumber = r0;	 Catch:{ all -> 0x0391 }
        r0 = r23;
        r1 = r22;
        r1.serialNumber = r0;	 Catch:{ all -> 0x0391 }
        r16 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x0391 }
        r26 = 946080000000; // 0xdc46c32800 float:24980.0 double:4.674256262175E-312;
        r23 = (r16 > r26 ? 1 : (r16 == r26 ? 0 : -1));
        if (r23 <= 0) goto L_0x038d;
    L_0x0210:
        r0 = r16;
        r2 = r22;
        r2.creationTime = r0;	 Catch:{ all -> 0x0391 }
        r23 = 1;
        r0 = r23;
        r1 = r22;
        r1.partial = r0;	 Catch:{ all -> 0x0391 }
        r23 = android.os.Build.FINGERPRINT;	 Catch:{ all -> 0x0391 }
        r0 = r23;
        r1 = r22;
        r1.lastLoggedInFingerprint = r0;	 Catch:{ all -> 0x0391 }
        if (r13 == 0) goto L_0x023e;
    L_0x0228:
        r23 = -10000; // 0xffffffffffffd8f0 float:NaN double:NaN;
        r0 = r31;
        r1 = r23;
        if (r0 == r1) goto L_0x023e;
    L_0x0230:
        r0 = r28;
        r1 = r31;
        r23 = r0.getFreeProfileBadgeLU(r1);	 Catch:{ all -> 0x0391 }
        r0 = r23;
        r1 = r22;
        r1.profileBadge = r0;	 Catch:{ all -> 0x0391 }
    L_0x023e:
        r20 = new com.android.server.pm.UserManagerService$UserData;	 Catch:{ all -> 0x0391 }
        r20.<init>();	 Catch:{ all -> 0x0391 }
        r0 = r22;
        r1 = r20;
        r1.info = r0;	 Catch:{ all -> 0x0391 }
        r0 = r28;
        r0 = r0.mUsers;	 Catch:{ all -> 0x0391 }
        r23 = r0;
        r0 = r23;
        r1 = r21;
        r2 = r20;
        r0.put(r1, r2);	 Catch:{ all -> 0x0391 }
        monitor-exit(r25);	 Catch:{ all -> 0x0073 }
        r0 = r28;
        r1 = r20;
        r0.writeUserLP(r1);	 Catch:{ all -> 0x0073 }
        r28.writeUserListLP();	 Catch:{ all -> 0x0073 }
        if (r15 == 0) goto L_0x02a2;
    L_0x0265:
        if (r13 == 0) goto L_0x0394;
    L_0x0267:
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r0 = r23;
        r0 = r0.profileGroupId;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r25 = -10000; // 0xffffffffffffd8f0 float:NaN double:NaN;
        r0 = r23;
        r1 = r25;
        if (r0 != r1) goto L_0x0292;
    L_0x0279:
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r25 = r0;
        r0 = r25;
        r0 = r0.id;	 Catch:{ all -> 0x0073 }
        r25 = r0;
        r0 = r25;
        r1 = r23;
        r1.profileGroupId = r0;	 Catch:{ all -> 0x0073 }
        r0 = r28;
        r0.writeUserLP(r15);	 Catch:{ all -> 0x0073 }
    L_0x0292:
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r0 = r23;
        r0 = r0.profileGroupId;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r0 = r23;
        r1 = r22;
        r1.profileGroupId = r0;	 Catch:{ all -> 0x0073 }
    L_0x02a2:
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mContext;	 Catch:{ all -> 0x0076 }
        r23 = r0;
        r24 = android.os.storage.StorageManager.class;
        r19 = r23.getSystemService(r24);	 Catch:{ all -> 0x0076 }
        r19 = (android.os.storage.StorageManager) r19;	 Catch:{ all -> 0x0076 }
        r0 = r22;
        r0 = r0.serialNumber;	 Catch:{ all -> 0x0076 }
        r23 = r0;
        r24 = r22.isEphemeral();	 Catch:{ all -> 0x0076 }
        r0 = r19;
        r1 = r21;
        r2 = r23;
        r3 = r24;
        r0.createUserKey(r1, r2, r3);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mUserDataPreparer;	 Catch:{ all -> 0x0076 }
        r23 = r0;
        r0 = r22;
        r0 = r0.serialNumber;	 Catch:{ all -> 0x0076 }
        r24 = r0;
        r25 = 3;
        r0 = r23;
        r1 = r21;
        r2 = r24;
        r3 = r25;
        r0.prepareUserData(r1, r2, r3);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mPm;	 Catch:{ all -> 0x0076 }
        r23 = r0;
        r0 = r23;
        r1 = r21;
        r2 = r32;
        r0.createNewUser(r1, r2);	 Catch:{ all -> 0x0076 }
        r23 = 0;
        r0 = r23;
        r1 = r22;
        r1.partial = r0;	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mPackagesLock;	 Catch:{ all -> 0x0076 }
        r23 = r0;
        monitor-enter(r23);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r1 = r20;
        r0.writeUserLP(r1);	 Catch:{ all -> 0x03d3 }
        monitor-exit(r23);	 Catch:{ all -> 0x0076 }
        r28.updateUserIds();	 Catch:{ all -> 0x0076 }
        r18 = new android.os.Bundle;	 Catch:{ all -> 0x0076 }
        r18.<init>();	 Catch:{ all -> 0x0076 }
        if (r12 == 0) goto L_0x0324;
    L_0x030f:
        r0 = r28;
        r0 = r0.mGuestRestrictions;	 Catch:{ all -> 0x0076 }
        r24 = r0;
        monitor-enter(r24);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mGuestRestrictions;	 Catch:{ all -> 0x03d6 }
        r23 = r0;
        r0 = r18;
        r1 = r23;
        r0.putAll(r1);	 Catch:{ all -> 0x03d6 }
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
    L_0x0324:
        r0 = r28;
        r0 = r0.mRestrictionsLock;	 Catch:{ all -> 0x0076 }
        r24 = r0;
        monitor-enter(r24);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mBaseUserRestrictions;	 Catch:{ all -> 0x03d9 }
        r23 = r0;
        r0 = r23;
        r1 = r21;
        r2 = r18;
        r0.append(r1, r2);	 Catch:{ all -> 0x03d9 }
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mPm;	 Catch:{ all -> 0x0076 }
        r23 = r0;
        r0 = r23;
        r1 = r21;
        r0.onNewUserCreated(r1);	 Catch:{ all -> 0x0076 }
        r6 = new android.content.Intent;	 Catch:{ all -> 0x0076 }
        r23 = "android.intent.action.USER_ADDED";
        r0 = r23;
        r6.<init>(r0);	 Catch:{ all -> 0x0076 }
        r23 = "android.intent.extra.user_handle";
        r0 = r23;
        r1 = r21;
        r6.putExtra(r0, r1);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mContext;	 Catch:{ all -> 0x0076 }
        r23 = r0;
        r24 = android.os.UserHandle.ALL;	 Catch:{ all -> 0x0076 }
        r25 = "android.permission.MANAGE_USERS";
        r0 = r23;
        r1 = r24;
        r2 = r25;
        r0.sendBroadcastAsUser(r6, r1, r2);	 Catch:{ all -> 0x0076 }
        r0 = r28;
        r0 = r0.mContext;	 Catch:{ all -> 0x0076 }
        r24 = r0;
        if (r12 == 0) goto L_0x03dc;
    L_0x0378:
        r23 = "users_guest_created";
    L_0x037b:
        r25 = 1;
        r0 = r24;
        r1 = r23;
        r2 = r25;
        com.android.internal.logging.MetricsLogger.count(r0, r1, r2);	 Catch:{ all -> 0x0076 }
        android.os.Binder.restoreCallingIdentity(r10);
        return r22;
    L_0x038a:
        r25 = move-exception;
        monitor-exit(r23);	 Catch:{ all -> 0x0073 }
        throw r25;	 Catch:{ all -> 0x0073 }
    L_0x038d:
        r16 = 0;
        goto L_0x0210;
    L_0x0391:
        r23 = move-exception;
        monitor-exit(r25);	 Catch:{ all -> 0x0073 }
        throw r23;	 Catch:{ all -> 0x0073 }
    L_0x0394:
        if (r14 == 0) goto L_0x02a2;
    L_0x0396:
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r0 = r23;
        r0 = r0.restrictedProfileParentId;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r25 = -10000; // 0xffffffffffffd8f0 float:NaN double:NaN;
        r0 = r23;
        r1 = r25;
        if (r0 != r1) goto L_0x03c1;
    L_0x03a8:
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r25 = r0;
        r0 = r25;
        r0 = r0.id;	 Catch:{ all -> 0x0073 }
        r25 = r0;
        r0 = r25;
        r1 = r23;
        r1.restrictedProfileParentId = r0;	 Catch:{ all -> 0x0073 }
        r0 = r28;
        r0.writeUserLP(r15);	 Catch:{ all -> 0x0073 }
    L_0x03c1:
        r0 = r15.info;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r0 = r23;
        r0 = r0.restrictedProfileParentId;	 Catch:{ all -> 0x0073 }
        r23 = r0;
        r0 = r23;
        r1 = r22;
        r1.restrictedProfileParentId = r0;	 Catch:{ all -> 0x0073 }
        goto L_0x02a2;
    L_0x03d3:
        r24 = move-exception;
        monitor-exit(r23);	 Catch:{ all -> 0x0076 }
        throw r24;	 Catch:{ all -> 0x0076 }
    L_0x03d6:
        r23 = move-exception;
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        throw r23;	 Catch:{ all -> 0x0076 }
    L_0x03d9:
        r23 = move-exception;
        monitor-exit(r24);	 Catch:{ all -> 0x0076 }
        throw r23;	 Catch:{ all -> 0x0076 }
    L_0x03dc:
        if (r9 == 0) goto L_0x03e2;
    L_0x03de:
        r23 = "users_demo_created";
        goto L_0x037b;
    L_0x03e2:
        r23 = "users_user_created";
        goto L_0x037b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.createUserInternalUnchecked(java.lang.String, int, int, java.lang.String[]):android.content.pm.UserInfo");
    }

    UserData putUserInfo(UserInfo userInfo) {
        UserData userData = new UserData();
        userData.info = userInfo;
        synchronized (this.mUsers) {
            this.mUsers.put(userInfo.id, userData);
        }
        return userData;
    }

    void removeUserInfo(int userId) {
        synchronized (this.mUsers) {
            this.mUsers.remove(userId);
        }
    }

    public UserInfo createRestrictedProfile(String name, int parentUserId) {
        checkManageOrCreateUsersPermission("setupRestrictedProfile");
        UserInfo user = createProfileForUser(name, 8, parentUserId, null);
        if (user == null) {
            return null;
        }
        long identity = Binder.clearCallingIdentity();
        try {
            setUserRestriction("no_modify_accounts", true, user.id);
            Secure.putIntForUser(this.mContext.getContentResolver(), "location_mode", 0, user.id);
            setUserRestriction("no_share_location", true, user.id);
            return user;
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    private UserInfo findCurrentGuestUser() {
        synchronized (this.mUsersLock) {
            int size = this.mUsers.size();
            int i = 0;
            while (i < size) {
                UserInfo user = ((UserData) this.mUsers.valueAt(i)).info;
                if (!user.isGuest() || (user.guestToRemove ^ 1) == 0 || (this.mRemovingUserIds.get(user.id) ^ 1) == 0) {
                    i++;
                } else {
                    return user;
                }
            }
            return null;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean markGuestForDeletion(int r9) {
        /*
        r8 = this;
        r7 = 1;
        r6 = 0;
        r3 = "Only the system can remove users";
        checkManageUsersPermission(r3);
        r3 = android.os.UserHandle.getCallingUserId();
        r3 = r8.getUserRestrictions(r3);
        r4 = "no_remove_user";
        r3 = r3.getBoolean(r4, r6);
        if (r3 == 0) goto L_0x0023;
    L_0x0019:
        r3 = "UserManagerService";
        r4 = "Cannot remove user. DISALLOW_REMOVE_USER is enabled.";
        android.util.Log.w(r3, r4);
        return r6;
    L_0x0023:
        r0 = android.os.Binder.clearCallingIdentity();
        r4 = r8.mPackagesLock;	 Catch:{ all -> 0x005b }
        monitor-enter(r4);	 Catch:{ all -> 0x005b }
        r5 = r8.mUsersLock;	 Catch:{ all -> 0x0058 }
        monitor-enter(r5);	 Catch:{ all -> 0x0058 }
        r3 = r8.mUsers;	 Catch:{ all -> 0x0055 }
        r2 = r3.get(r9);	 Catch:{ all -> 0x0055 }
        r2 = (com.android.server.pm.UserManagerService.UserData) r2;	 Catch:{ all -> 0x0055 }
        if (r9 == 0) goto L_0x0039;
    L_0x0037:
        if (r2 != 0) goto L_0x003f;
    L_0x0039:
        monitor-exit(r5);	 Catch:{ all -> 0x0058 }
        monitor-exit(r4);	 Catch:{ all -> 0x005b }
        android.os.Binder.restoreCallingIdentity(r0);
        return r6;
    L_0x003f:
        r3 = r8.mRemovingUserIds;	 Catch:{ all -> 0x0055 }
        r3 = r3.get(r9);	 Catch:{ all -> 0x0055 }
        if (r3 != 0) goto L_0x0039;
    L_0x0047:
        monitor-exit(r5);	 Catch:{ all -> 0x0058 }
        r3 = r2.info;	 Catch:{ all -> 0x0058 }
        r3 = r3.isGuest();	 Catch:{ all -> 0x0058 }
        if (r3 != 0) goto L_0x0060;
    L_0x0050:
        monitor-exit(r4);	 Catch:{ all -> 0x005b }
        android.os.Binder.restoreCallingIdentity(r0);
        return r6;
    L_0x0055:
        r3 = move-exception;
        monitor-exit(r5);	 Catch:{ all -> 0x0058 }
        throw r3;	 Catch:{ all -> 0x0058 }
    L_0x0058:
        r3 = move-exception;
        monitor-exit(r4);	 Catch:{ all -> 0x005b }
        throw r3;	 Catch:{ all -> 0x005b }
    L_0x005b:
        r3 = move-exception;
        android.os.Binder.restoreCallingIdentity(r0);
        throw r3;
    L_0x0060:
        r3 = r2.info;	 Catch:{ all -> 0x0058 }
        r5 = 1;
        r3.guestToRemove = r5;	 Catch:{ all -> 0x0058 }
        r3 = r2.info;	 Catch:{ all -> 0x0058 }
        r5 = r3.flags;	 Catch:{ all -> 0x0058 }
        r5 = r5 | 64;
        r3.flags = r5;	 Catch:{ all -> 0x0058 }
        r8.writeUserLP(r2);	 Catch:{ all -> 0x0058 }
        monitor-exit(r4);	 Catch:{ all -> 0x005b }
        android.os.Binder.restoreCallingIdentity(r0);
        return r7;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.markGuestForDeletion(int):boolean");
    }

    public boolean removeUser(int userHandle) {
        boolean isManagedProfile;
        Slog.i(LOG_TAG, "removeUser u" + userHandle);
        checkManageOrCreateUsersPermission("Only the system can remove users");
        synchronized (this.mUsersLock) {
            UserInfo userInfo = getUserInfoLU(userHandle);
            isManagedProfile = userInfo != null ? userInfo.isManagedProfile() : false;
        }
        String restriction = isManagedProfile ? "no_remove_managed_profile" : "no_remove_user";
        if (!getUserRestrictions(UserHandle.getCallingUserId()).getBoolean(restriction, false)) {
            return removeUserUnchecked(userHandle);
        }
        Log.w(LOG_TAG, "Cannot remove user. " + restriction + " is enabled.");
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean removeUserUnchecked(int r12) {
        /*
        r11 = this;
        r6 = 1;
        r7 = 0;
        r2 = android.os.Binder.clearCallingIdentity();
        r0 = android.app.ActivityManager.getCurrentUser();	 Catch:{ all -> 0x0088 }
        if (r0 != r12) goto L_0x0019;
    L_0x000c:
        r6 = "UserManagerService";
        r8 = "Current user cannot be removed";
        android.util.Log.w(r6, r8);	 Catch:{ all -> 0x0088 }
        android.os.Binder.restoreCallingIdentity(r2);
        return r7;
    L_0x0019:
        r8 = r11.mPackagesLock;	 Catch:{ all -> 0x0088 }
        monitor-enter(r8);	 Catch:{ all -> 0x0088 }
        r9 = r11.mUsersLock;	 Catch:{ all -> 0x0085 }
        monitor-enter(r9);	 Catch:{ all -> 0x0085 }
        r10 = r11.mUsers;	 Catch:{ all -> 0x0082 }
        r5 = r10.get(r12);	 Catch:{ all -> 0x0082 }
        r5 = (com.android.server.pm.UserManagerService.UserData) r5;	 Catch:{ all -> 0x0082 }
        if (r12 == 0) goto L_0x002b;
    L_0x0029:
        if (r5 != 0) goto L_0x0031;
    L_0x002b:
        monitor-exit(r9);	 Catch:{ all -> 0x0085 }
        monitor-exit(r8);	 Catch:{ all -> 0x0088 }
        android.os.Binder.restoreCallingIdentity(r2);
        return r7;
    L_0x0031:
        r10 = r11.mRemovingUserIds;	 Catch:{ all -> 0x0082 }
        r10 = r10.get(r12);	 Catch:{ all -> 0x0082 }
        if (r10 != 0) goto L_0x002b;
    L_0x0039:
        r11.addRemovingUserIdLocked(r12);	 Catch:{ all -> 0x0082 }
        monitor-exit(r9);	 Catch:{ all -> 0x0085 }
        r9 = r5.info;	 Catch:{ all -> 0x0085 }
        r10 = 1;
        r9.partial = r10;	 Catch:{ all -> 0x0085 }
        r9 = r5.info;	 Catch:{ all -> 0x0085 }
        r10 = r9.flags;	 Catch:{ all -> 0x0085 }
        r10 = r10 | 64;
        r9.flags = r10;	 Catch:{ all -> 0x0085 }
        r11.writeUserLP(r5);	 Catch:{ all -> 0x0085 }
        monitor-exit(r8);	 Catch:{ all -> 0x0088 }
        r8 = r11.mAppOpsService;	 Catch:{ RemoteException -> 0x008d }
        r8.removeUser(r12);	 Catch:{ RemoteException -> 0x008d }
    L_0x0053:
        r8 = r5.info;	 Catch:{ all -> 0x0088 }
        r8 = r8.profileGroupId;	 Catch:{ all -> 0x0088 }
        r9 = -10000; // 0xffffffffffffd8f0 float:NaN double:NaN;
        if (r8 == r9) goto L_0x006e;
    L_0x005b:
        r8 = r5.info;	 Catch:{ all -> 0x0088 }
        r8 = r8.isManagedProfile();	 Catch:{ all -> 0x0088 }
        if (r8 == 0) goto L_0x006e;
    L_0x0063:
        r8 = r5.info;	 Catch:{ all -> 0x0088 }
        r8 = r8.profileGroupId;	 Catch:{ all -> 0x0088 }
        r9 = r5.info;	 Catch:{ all -> 0x0088 }
        r9 = r9.id;	 Catch:{ all -> 0x0088 }
        r11.sendProfileRemovedBroadcast(r8, r9);	 Catch:{ all -> 0x0088 }
    L_0x006e:
        r8 = android.app.ActivityManager.getService();	 Catch:{ RemoteException -> 0x0098 }
        r9 = new com.android.server.pm.UserManagerService$5;	 Catch:{ RemoteException -> 0x0098 }
        r9.<init>();	 Catch:{ RemoteException -> 0x0098 }
        r10 = 1;
        r4 = r8.stopUser(r12, r10, r9);	 Catch:{ RemoteException -> 0x0098 }
        if (r4 != 0) goto L_0x009d;
    L_0x007e:
        android.os.Binder.restoreCallingIdentity(r2);
        return r6;
    L_0x0082:
        r6 = move-exception;
        monitor-exit(r9);	 Catch:{ all -> 0x0085 }
        throw r6;	 Catch:{ all -> 0x0085 }
    L_0x0085:
        r6 = move-exception;
        monitor-exit(r8);	 Catch:{ all -> 0x0088 }
        throw r6;	 Catch:{ all -> 0x0088 }
    L_0x0088:
        r6 = move-exception;
        android.os.Binder.restoreCallingIdentity(r2);
        throw r6;
    L_0x008d:
        r1 = move-exception;
        r8 = "UserManagerService";
        r9 = "Unable to notify AppOpsService of removing user";
        android.util.Log.w(r8, r9, r1);	 Catch:{ all -> 0x0088 }
        goto L_0x0053;
    L_0x0098:
        r1 = move-exception;
        android.os.Binder.restoreCallingIdentity(r2);
        return r7;
    L_0x009d:
        r6 = r7;
        goto L_0x007e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.removeUserUnchecked(int):boolean");
    }

    void addRemovingUserIdLocked(int userId) {
        this.mRemovingUserIds.put(userId, true);
        this.mRecentlyRemovedIds.add(Integer.valueOf(userId));
        if (this.mRecentlyRemovedIds.size() > 100) {
            this.mRecentlyRemovedIds.removeFirst();
        }
    }

    void finishRemoveUser(final int userHandle) {
        long ident = Binder.clearCallingIdentity();
        try {
            Intent addedIntent = new Intent("android.intent.action.USER_REMOVED");
            addedIntent.putExtra("android.intent.extra.user_handle", userHandle);
            this.mContext.sendOrderedBroadcastAsUser(addedIntent, UserHandle.ALL, "android.permission.MANAGE_USERS", new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    final int i = userHandle;
                    new Thread() {
                        public void run() {
                            ((ActivityManagerInternal) LocalServices.getService(ActivityManagerInternal.class)).onUserRemoved(i);
                            UserManagerService.this.removeUserState(i);
                        }
                    }.start();
                }
            }, null, -1, null, null);
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    private void removeUserState(int userHandle) {
        try {
            ((StorageManager) this.mContext.getSystemService(StorageManager.class)).destroyUserKey(userHandle);
        } catch (IllegalStateException e) {
            Slog.i(LOG_TAG, "Destroying key for user " + userHandle + " failed, continuing anyway", e);
        }
        try {
            IGateKeeperService gk = GateKeeper.getService();
            if (gk != null) {
                gk.clearSecureUserId(userHandle);
            }
        } catch (Exception e2) {
            Slog.w(LOG_TAG, "unable to clear GK secure user id");
        }
        this.mPm.cleanUpUser(this, userHandle);
        this.mUserDataPreparer.destroyUserData(userHandle, 3);
        synchronized (this.mUsersLock) {
            this.mUsers.remove(userHandle);
            this.mIsUserManaged.delete(userHandle);
        }
        synchronized (this.mUserStates) {
            this.mUserStates.delete(userHandle);
        }
        synchronized (this.mRestrictionsLock) {
            this.mBaseUserRestrictions.remove(userHandle);
            this.mAppliedUserRestrictions.remove(userHandle);
            this.mCachedEffectiveUserRestrictions.remove(userHandle);
            this.mDevicePolicyLocalUserRestrictions.remove(userHandle);
            if (this.mDevicePolicyGlobalUserRestrictions.get(userHandle) != null) {
                this.mDevicePolicyGlobalUserRestrictions.remove(userHandle);
                applyUserRestrictionsForAllUsersLR();
            }
        }
        synchronized (this.mPackagesLock) {
            writeUserListLP();
        }
        new AtomicFile(new File(this.mUsersDir, userHandle + XML_SUFFIX)).delete();
        updateUserIds();
    }

    private void sendProfileRemovedBroadcast(int parentUserId, int removedUserId) {
        Intent managedProfileIntent = new Intent("android.intent.action.MANAGED_PROFILE_REMOVED");
        managedProfileIntent.addFlags(1342177280);
        managedProfileIntent.putExtra("android.intent.extra.USER", new UserHandle(removedUserId));
        managedProfileIntent.putExtra("android.intent.extra.user_handle", removedUserId);
        this.mContext.sendBroadcastAsUser(managedProfileIntent, new UserHandle(parentUserId), null);
    }

    public Bundle getApplicationRestrictions(String packageName) {
        return getApplicationRestrictionsForUser(packageName, UserHandle.getCallingUserId());
    }

    public Bundle getApplicationRestrictionsForUser(String packageName, int userId) {
        Bundle readApplicationRestrictionsLAr;
        if (!(UserHandle.getCallingUserId() == userId && (UserHandle.isSameApp(Binder.getCallingUid(), getUidForPackage(packageName)) ^ 1) == 0)) {
            checkSystemOrRoot("get application restrictions for other user/app " + packageName);
        }
        synchronized (this.mAppRestrictionsLock) {
            readApplicationRestrictionsLAr = readApplicationRestrictionsLAr(packageName, userId);
        }
        return readApplicationRestrictionsLAr;
    }

    public void setApplicationRestrictions(String packageName, Bundle restrictions, int userId) {
        checkSystemOrRoot("set application restrictions");
        if (restrictions != null) {
            restrictions.setDefusable(true);
        }
        synchronized (this.mAppRestrictionsLock) {
            if (restrictions != null) {
                if (!restrictions.isEmpty()) {
                    writeApplicationRestrictionsLAr(packageName, restrictions, userId);
                }
            }
            cleanAppRestrictionsForPackageLAr(packageName, userId);
        }
        Intent changeIntent = new Intent("android.intent.action.APPLICATION_RESTRICTIONS_CHANGED");
        changeIntent.setPackage(packageName);
        changeIntent.addFlags(1073741824);
        this.mContext.sendBroadcastAsUser(changeIntent, UserHandle.of(userId));
    }

    private int getUidForPackage(String packageName) {
        long ident = Binder.clearCallingIdentity();
        int i;
        try {
            i = this.mContext.getPackageManager().getApplicationInfo(packageName, DumpState.DUMP_CHANGES).uid;
            return i;
        } catch (NameNotFoundException e) {
            i = -1;
            return i;
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    @GuardedBy("mAppRestrictionsLock")
    private static Bundle readApplicationRestrictionsLAr(String packageName, int userId) {
        return readApplicationRestrictionsLAr(new AtomicFile(new File(Environment.getUserSystemDirectory(userId), packageToRestrictionsFileName(packageName))));
    }

    @GuardedBy("mAppRestrictionsLock")
    static Bundle readApplicationRestrictionsLAr(AtomicFile restrictionsFile) {
        Bundle restrictions = new Bundle();
        ArrayList<String> values = new ArrayList();
        if (!restrictionsFile.getBaseFile().exists()) {
            return restrictions;
        }
        AutoCloseable autoCloseable = null;
        try {
            autoCloseable = restrictionsFile.openRead();
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(autoCloseable, StandardCharsets.UTF_8.name());
            XmlUtils.nextElement(parser);
            if (parser.getEventType() != 2) {
                Slog.e(LOG_TAG, "Unable to read restrictions file " + restrictionsFile.getBaseFile());
                return restrictions;
            }
            while (parser.next() != 1) {
                readEntry(restrictions, values, parser);
            }
            IoUtils.closeQuietly(autoCloseable);
            return restrictions;
        } catch (Exception e) {
            Log.w(LOG_TAG, "Error parsing " + restrictionsFile.getBaseFile(), e);
        } finally {
            IoUtils.closeQuietly(autoCloseable);
        }
    }

    private static void readEntry(Bundle restrictions, ArrayList<String> values, XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() == 2 && parser.getName().equals(TAG_ENTRY)) {
            String key = parser.getAttributeValue(null, ATTR_KEY);
            String valType = parser.getAttributeValue(null, "type");
            String multiple = parser.getAttributeValue(null, ATTR_MULTIPLE);
            if (multiple != null) {
                values.clear();
                int count = Integer.parseInt(multiple);
                while (count > 0) {
                    int type = parser.next();
                    if (type == 1) {
                        break;
                    } else if (type == 2 && parser.getName().equals(TAG_VALUE)) {
                        values.add(parser.nextText().trim());
                        count--;
                    }
                }
                String[] valueStrings = new String[values.size()];
                values.toArray(valueStrings);
                restrictions.putStringArray(key, valueStrings);
            } else if (ATTR_TYPE_BUNDLE.equals(valType)) {
                restrictions.putBundle(key, readBundleEntry(parser, values));
            } else if (ATTR_TYPE_BUNDLE_ARRAY.equals(valType)) {
                int outerDepth = parser.getDepth();
                ArrayList<Bundle> bundleList = new ArrayList();
                while (XmlUtils.nextElementWithin(parser, outerDepth)) {
                    bundleList.add(readBundleEntry(parser, values));
                }
                restrictions.putParcelableArray(key, (Parcelable[]) bundleList.toArray(new Bundle[bundleList.size()]));
            } else {
                String value = parser.nextText().trim();
                if (ATTR_TYPE_BOOLEAN.equals(valType)) {
                    restrictions.putBoolean(key, Boolean.parseBoolean(value));
                } else if (ATTR_TYPE_INTEGER.equals(valType)) {
                    restrictions.putInt(key, Integer.parseInt(value));
                } else {
                    restrictions.putString(key, value);
                }
            }
        }
    }

    private static Bundle readBundleEntry(XmlPullParser parser, ArrayList<String> values) throws IOException, XmlPullParserException {
        Bundle childBundle = new Bundle();
        int outerDepth = parser.getDepth();
        while (XmlUtils.nextElementWithin(parser, outerDepth)) {
            readEntry(childBundle, values, parser);
        }
        return childBundle;
    }

    @GuardedBy("mAppRestrictionsLock")
    private static void writeApplicationRestrictionsLAr(String packageName, Bundle restrictions, int userId) {
        writeApplicationRestrictionsLAr(restrictions, new AtomicFile(new File(Environment.getUserSystemDirectory(userId), packageToRestrictionsFileName(packageName))));
    }

    @GuardedBy("mAppRestrictionsLock")
    static void writeApplicationRestrictionsLAr(Bundle restrictions, AtomicFile restrictionsFile) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = restrictionsFile.startWrite();
            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
            XmlSerializer serializer = new FastXmlSerializer();
            serializer.setOutput(bos, StandardCharsets.UTF_8.name());
            serializer.startDocument(null, Boolean.valueOf(true));
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            serializer.startTag(null, TAG_RESTRICTIONS);
            writeBundle(restrictions, serializer);
            serializer.endTag(null, TAG_RESTRICTIONS);
            serializer.endDocument();
            restrictionsFile.finishWrite(fileOutputStream);
        } catch (Exception e) {
            restrictionsFile.failWrite(fileOutputStream);
            Slog.e(LOG_TAG, "Error writing application restrictions list", e);
        }
    }

    private static void writeBundle(Bundle restrictions, XmlSerializer serializer) throws IOException {
        for (String key : restrictions.keySet()) {
            Object value = restrictions.get(key);
            serializer.startTag(null, TAG_ENTRY);
            serializer.attribute(null, ATTR_KEY, key);
            if (value instanceof Boolean) {
                serializer.attribute(null, "type", ATTR_TYPE_BOOLEAN);
                serializer.text(value.toString());
            } else if (value instanceof Integer) {
                serializer.attribute(null, "type", ATTR_TYPE_INTEGER);
                serializer.text(value.toString());
            } else if (value == null || (value instanceof String)) {
                serializer.attribute(null, "type", ATTR_TYPE_STRING);
                serializer.text(value != null ? (String) value : "");
            } else if (value instanceof Bundle) {
                serializer.attribute(null, "type", ATTR_TYPE_BUNDLE);
                writeBundle((Bundle) value, serializer);
            } else if (value instanceof Parcelable[]) {
                serializer.attribute(null, "type", ATTR_TYPE_BUNDLE_ARRAY);
                Parcelable[] array = (Parcelable[]) value;
                r9 = array.length;
                r7 = 0;
                while (r7 < r9) {
                    Parcelable parcelable = array[r7];
                    if (parcelable instanceof Bundle) {
                        serializer.startTag(null, TAG_ENTRY);
                        serializer.attribute(null, "type", ATTR_TYPE_BUNDLE);
                        writeBundle((Bundle) parcelable, serializer);
                        serializer.endTag(null, TAG_ENTRY);
                        r7++;
                    } else {
                        throw new IllegalArgumentException("bundle-array can only hold Bundles");
                    }
                }
                continue;
            } else {
                serializer.attribute(null, "type", ATTR_TYPE_STRING_ARRAY);
                String[] values = (String[]) value;
                serializer.attribute(null, ATTR_MULTIPLE, Integer.toString(values.length));
                for (String choice : values) {
                    String choice2;
                    serializer.startTag(null, TAG_VALUE);
                    if (choice2 == null) {
                        choice2 = "";
                    }
                    serializer.text(choice2);
                    serializer.endTag(null, TAG_VALUE);
                }
            }
            serializer.endTag(null, TAG_ENTRY);
        }
    }

    public int getUserSerialNumber(int userHandle) {
        synchronized (this.mUsersLock) {
            if (exists(userHandle)) {
                int i = getUserInfoLU(userHandle).serialNumber;
                return i;
            }
            return -1;
        }
    }

    public boolean isUserNameSet(int userHandle) {
        boolean z = false;
        synchronized (this.mUsersLock) {
            UserInfo userInfo = getUserInfoLU(userHandle);
            if (!(userInfo == null || userInfo.name == null)) {
                z = true;
            }
        }
        return z;
    }

    public int getUserHandle(int userSerialNumber) {
        synchronized (this.mUsersLock) {
            int[] iArr = this.mUserIds;
            int i = 0;
            int length = iArr.length;
            while (i < length) {
                int userId = iArr[i];
                UserInfo info = getUserInfoLU(userId);
                if (info == null || info.serialNumber != userSerialNumber) {
                    i++;
                } else {
                    return userId;
                }
            }
            return -1;
        }
    }

    public long getUserCreationTime(int userHandle) {
        int callingUserId = UserHandle.getCallingUserId();
        UserInfo userInfo = null;
        synchronized (this.mUsersLock) {
            if (callingUserId == userHandle) {
                userInfo = getUserInfoLU(userHandle);
            } else {
                UserInfo parent = getProfileParentLU(userHandle);
                if (parent != null && parent.id == callingUserId) {
                    userInfo = getUserInfoLU(userHandle);
                }
            }
        }
        if (userInfo != null) {
            return userInfo.creationTime;
        }
        throw new SecurityException("userHandle can only be the calling user or a managed profile associated with this user");
    }

    private void updateUserIds() {
        int num = 0;
        synchronized (this.mUsersLock) {
            int i;
            int userSize = this.mUsers.size();
            for (i = 0; i < userSize; i++) {
                if (!((UserData) this.mUsers.valueAt(i)).info.partial) {
                    num++;
                }
            }
            int[] newUsers = new int[num];
            i = 0;
            int n = 0;
            while (i < userSize) {
                int n2;
                if (((UserData) this.mUsers.valueAt(i)).info.partial) {
                    n2 = n;
                } else {
                    n2 = n + 1;
                    newUsers[n] = this.mUsers.keyAt(i);
                }
                i++;
                n = n2;
            }
            this.mUserIds = newUsers;
        }
    }

    public void onBeforeStartUser(int userId) {
        UserInfo userInfo = getUserInfo(userId);
        if (userInfo != null) {
            boolean migrateAppsData = Build.FINGERPRINT.equals(userInfo.lastLoggedInFingerprint) ^ 1;
            this.mUserDataPreparer.prepareUserData(userId, userInfo.serialNumber, 1);
            this.mPm.reconcileAppsData(userId, 1, migrateAppsData);
            if (userId != 0) {
                synchronized (this.mRestrictionsLock) {
                    applyUserRestrictionsLR(userId);
                }
            }
        }
    }

    public void onBeforeUnlockUser(int userId) {
        UserInfo userInfo = getUserInfo(userId);
        if (userInfo != null) {
            boolean migrateAppsData = Build.FINGERPRINT.equals(userInfo.lastLoggedInFingerprint) ^ 1;
            this.mUserDataPreparer.prepareUserData(userId, userInfo.serialNumber, 2);
            this.mPm.reconcileAppsData(userId, 2, migrateAppsData);
        }
    }

    void reconcileUsers(String volumeUuid) {
        this.mUserDataPreparer.reconcileUsers(volumeUuid, getUsers(true));
    }

    public void onUserLoggedIn(int userId) {
        UserData userData = getUserDataNoChecks(userId);
        if (userData == null || userData.info.partial) {
            Slog.w(LOG_TAG, "userForeground: unknown user #" + userId);
            return;
        }
        long now = System.currentTimeMillis();
        if (now > EPOCH_PLUS_30_YEARS) {
            userData.info.lastLoggedInTime = now;
        }
        userData.info.lastLoggedInFingerprint = Build.FINGERPRINT;
        scheduleWriteUser(userData);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    int getNextAvailableId() {
        /*
        r7 = this;
        r4 = r7.mUsersLock;
        monitor-enter(r4);
        r0 = r7.scanNextAvailableIdLocked();	 Catch:{ all -> 0x003e }
        if (r0 < 0) goto L_0x000b;
    L_0x0009:
        monitor-exit(r4);
        return r0;
    L_0x000b:
        r3 = r7.mRemovingUserIds;	 Catch:{ all -> 0x003e }
        r3 = r3.size();	 Catch:{ all -> 0x003e }
        if (r3 <= 0) goto L_0x0045;
    L_0x0013:
        r3 = "UserManagerService";
        r5 = "All available IDs are used. Recycling LRU ids.";
        android.util.Slog.i(r3, r5);	 Catch:{ all -> 0x003e }
        r3 = r7.mRemovingUserIds;	 Catch:{ all -> 0x003e }
        r3.clear();	 Catch:{ all -> 0x003e }
        r3 = r7.mRecentlyRemovedIds;	 Catch:{ all -> 0x003e }
        r2 = r3.iterator();	 Catch:{ all -> 0x003e }
    L_0x0027:
        r3 = r2.hasNext();	 Catch:{ all -> 0x003e }
        if (r3 == 0) goto L_0x0041;
    L_0x002d:
        r1 = r2.next();	 Catch:{ all -> 0x003e }
        r1 = (java.lang.Integer) r1;	 Catch:{ all -> 0x003e }
        r3 = r7.mRemovingUserIds;	 Catch:{ all -> 0x003e }
        r5 = r1.intValue();	 Catch:{ all -> 0x003e }
        r6 = 1;
        r3.put(r5, r6);	 Catch:{ all -> 0x003e }
        goto L_0x0027;
    L_0x003e:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
    L_0x0041:
        r0 = r7.scanNextAvailableIdLocked();	 Catch:{ all -> 0x003e }
    L_0x0045:
        monitor-exit(r4);
        if (r0 >= 0) goto L_0x0051;
    L_0x0048:
        r3 = new java.lang.IllegalStateException;
        r4 = "No user id available!";
        r3.<init>(r4);
        throw r3;
    L_0x0051:
        return r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.getNextAvailableId():int");
    }

    private int scanNextAvailableIdLocked() {
        int i = 10;
        while (i < MAX_USER_ID) {
            if (this.mUsers.indexOfKey(i) < 0 && (this.mRemovingUserIds.get(i) ^ 1) != 0) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private static String packageToRestrictionsFileName(String packageName) {
        return RESTRICTIONS_FILE_PREFIX + packageName + XML_SUFFIX;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setSeedAccountData(int r7, java.lang.String r8, java.lang.String r9, android.os.PersistableBundle r10, boolean r11) {
        /*
        r6 = this;
        r1 = "Require MANAGE_USERS permission to set user seed data";
        checkManageUsersPermission(r1);
        r2 = r6.mPackagesLock;
        monitor-enter(r2);
        r3 = r6.mUsersLock;	 Catch:{ all -> 0x0042 }
        monitor-enter(r3);	 Catch:{ all -> 0x0042 }
        r0 = r6.getUserDataLU(r7);	 Catch:{ all -> 0x003f }
        if (r0 != 0) goto L_0x002f;
    L_0x0012:
        r1 = "UserManagerService";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003f }
        r4.<init>();	 Catch:{ all -> 0x003f }
        r5 = "No such user for settings seed data u=";
        r4 = r4.append(r5);	 Catch:{ all -> 0x003f }
        r4 = r4.append(r7);	 Catch:{ all -> 0x003f }
        r4 = r4.toString();	 Catch:{ all -> 0x003f }
        android.util.Slog.e(r1, r4);	 Catch:{ all -> 0x003f }
        monitor-exit(r3);	 Catch:{ all -> 0x0042 }
        monitor-exit(r2);
        return;
    L_0x002f:
        r0.seedAccountName = r8;	 Catch:{ all -> 0x003f }
        r0.seedAccountType = r9;	 Catch:{ all -> 0x003f }
        r0.seedAccountOptions = r10;	 Catch:{ all -> 0x003f }
        r0.persistSeedData = r11;	 Catch:{ all -> 0x003f }
        monitor-exit(r3);	 Catch:{ all -> 0x0042 }
        if (r11 == 0) goto L_0x003d;
    L_0x003a:
        r6.writeUserLP(r0);	 Catch:{ all -> 0x0042 }
    L_0x003d:
        monitor-exit(r2);
        return;
    L_0x003f:
        r1 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0042 }
        throw r1;	 Catch:{ all -> 0x0042 }
    L_0x0042:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.UserManagerService.setSeedAccountData(int, java.lang.String, java.lang.String, android.os.PersistableBundle, boolean):void");
    }

    public String getSeedAccountName() throws RemoteException {
        String str;
        checkManageUsersPermission("Cannot get seed account information");
        synchronized (this.mUsersLock) {
            str = getUserDataLU(UserHandle.getCallingUserId()).seedAccountName;
        }
        return str;
    }

    public String getSeedAccountType() throws RemoteException {
        String str;
        checkManageUsersPermission("Cannot get seed account information");
        synchronized (this.mUsersLock) {
            str = getUserDataLU(UserHandle.getCallingUserId()).seedAccountType;
        }
        return str;
    }

    public PersistableBundle getSeedAccountOptions() throws RemoteException {
        PersistableBundle persistableBundle;
        checkManageUsersPermission("Cannot get seed account information");
        synchronized (this.mUsersLock) {
            persistableBundle = getUserDataLU(UserHandle.getCallingUserId()).seedAccountOptions;
        }
        return persistableBundle;
    }

    public void clearSeedAccountData() throws RemoteException {
        checkManageUsersPermission("Cannot clear seed account information");
        synchronized (this.mPackagesLock) {
            synchronized (this.mUsersLock) {
                UserData userData = getUserDataLU(UserHandle.getCallingUserId());
                if (userData == null) {
                    return;
                }
                userData.clearSeedAccountData();
                writeUserLP(userData);
            }
        }
    }

    public boolean someUserHasSeedAccount(String accountName, String accountType) throws RemoteException {
        checkManageUsersPermission("Cannot check seed account information");
        synchronized (this.mUsersLock) {
            int userSize = this.mUsers.size();
            int i = 0;
            while (i < userSize) {
                UserData data = (UserData) this.mUsers.valueAt(i);
                if (data.info.isInitialized() || data.seedAccountName == null || (data.seedAccountName.equals(accountName) ^ 1) != 0 || data.seedAccountType == null || (data.seedAccountType.equals(accountType) ^ 1) != 0) {
                    i++;
                } else {
                    return true;
                }
            }
            return false;
        }
    }

    public void onShellCommand(FileDescriptor in, FileDescriptor out, FileDescriptor err, String[] args, ShellCallback callback, ResultReceiver resultReceiver) {
        new Shell().exec(this, in, out, err, args, callback, resultReceiver);
    }

    int onShellCommand(Shell shell, String cmd) {
        if (cmd == null) {
            return shell.handleDefaultCommands(cmd);
        }
        PrintWriter pw = shell.getOutPrintWriter();
        try {
            if (cmd.equals("list")) {
                return runList(pw);
            }
        } catch (RemoteException e) {
            pw.println("Remote exception: " + e);
        }
        return -1;
    }

    private int runList(PrintWriter pw) throws RemoteException {
        IActivityManager am = ActivityManager.getService();
        List<UserInfo> users = getUsers(false);
        if (users == null) {
            pw.println("Error: couldn't get users");
            return 1;
        }
        pw.println("Users:");
        for (int i = 0; i < users.size(); i++) {
            pw.println("\t" + ((UserInfo) users.get(i)).toString() + (am.isUserRunning(((UserInfo) users.get(i)).id, 0) ? " running" : ""));
        }
        return 0;
    }

    protected void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        if (DumpUtils.checkDumpPermission(this.mContext, LOG_TAG, pw)) {
            long now = System.currentTimeMillis();
            StringBuilder sb = new StringBuilder();
            synchronized (this.mPackagesLock) {
                synchronized (this.mUsersLock) {
                    pw.println("Users:");
                    for (int i = 0; i < this.mUsers.size(); i++) {
                        UserData userData = (UserData) this.mUsers.valueAt(i);
                        if (userData != null) {
                            int state;
                            UserInfo userInfo = userData.info;
                            int userId = userInfo.id;
                            pw.print("  ");
                            pw.print(userInfo);
                            pw.print(" serialNo=");
                            pw.print(userInfo.serialNumber);
                            if (this.mRemovingUserIds.get(userId)) {
                                pw.print(" <removing> ");
                            }
                            if (userInfo.partial) {
                                pw.print(" <partial>");
                            }
                            pw.println();
                            pw.print("    State: ");
                            synchronized (this.mUserStates) {
                                state = this.mUserStates.get(userId, -1);
                            }
                            pw.println(UserState.stateToString(state));
                            pw.print("    Created: ");
                            if (userInfo.creationTime == 0) {
                                pw.println("<unknown>");
                            } else {
                                sb.setLength(0);
                                TimeUtils.formatDuration(now - userInfo.creationTime, sb);
                                sb.append(" ago");
                                pw.println(sb);
                            }
                            pw.print("    Last logged in: ");
                            if (userInfo.lastLoggedInTime == 0) {
                                pw.println("<unknown>");
                            } else {
                                sb.setLength(0);
                                TimeUtils.formatDuration(now - userInfo.lastLoggedInTime, sb);
                                sb.append(" ago");
                                pw.println(sb);
                            }
                            pw.print("    Last logged in fingerprint: ");
                            pw.println(userInfo.lastLoggedInFingerprint);
                            pw.print("    Has profile owner: ");
                            pw.println(this.mIsUserManaged.get(userId));
                            pw.println("    Restrictions:");
                            synchronized (this.mRestrictionsLock) {
                                UserRestrictionsUtils.dumpRestrictions(pw, "      ", (Bundle) this.mBaseUserRestrictions.get(userInfo.id));
                                pw.println("    Device policy global restrictions:");
                                UserRestrictionsUtils.dumpRestrictions(pw, "      ", (Bundle) this.mDevicePolicyGlobalUserRestrictions.get(userInfo.id));
                                pw.println("    Device policy local restrictions:");
                                UserRestrictionsUtils.dumpRestrictions(pw, "      ", (Bundle) this.mDevicePolicyLocalUserRestrictions.get(userInfo.id));
                                pw.println("    Effective restrictions:");
                                UserRestrictionsUtils.dumpRestrictions(pw, "      ", (Bundle) this.mCachedEffectiveUserRestrictions.get(userInfo.id));
                            }
                            if (userData.account != null) {
                                pw.print("    Account name: " + userData.account);
                                pw.println();
                            }
                            if (userData.seedAccountName != null) {
                                pw.print("    Seed account name: " + userData.seedAccountName);
                                pw.println();
                                if (userData.seedAccountType != null) {
                                    pw.print("         account type: " + userData.seedAccountType);
                                    pw.println();
                                }
                                if (userData.seedAccountOptions != null) {
                                    pw.print("         account options exist");
                                    pw.println();
                                }
                            }
                        }
                    }
                }
                pw.println();
                pw.println("  Device owner id:" + this.mDeviceOwnerUserId);
                pw.println();
                pw.println("  Guest restrictions:");
                synchronized (this.mGuestRestrictions) {
                    PrintWriter printWriter = pw;
                    UserRestrictionsUtils.dumpRestrictions(printWriter, "    ", this.mGuestRestrictions);
                }
                synchronized (this.mUsersLock) {
                    pw.println();
                    pw.println("  Device managed: " + this.mIsDeviceManaged);
                    if (this.mRemovingUserIds.size() > 0) {
                        pw.println();
                        pw.println("  Recently removed userIds: " + this.mRecentlyRemovedIds);
                    }
                }
                synchronized (this.mUserStates) {
                    pw.println("  Started users state: " + this.mUserStates);
                }
                pw.println();
                pw.println("  Max users: " + UserManager.getMaxSupportedUsers());
                pw.println("  Supports switchable users: " + UserManager.supportsMultipleUsers());
                pw.println("  All guests ephemeral: " + Resources.getSystem().getBoolean(17956971));
            }
        }
    }

    boolean isInitialized(int userId) {
        return (getUserInfo(userId).flags & 16) != 0;
    }

    private void removeNonSystemUsers() {
        ArrayList<UserInfo> usersToRemove = new ArrayList();
        synchronized (this.mUsersLock) {
            int userSize = this.mUsers.size();
            for (int i = 0; i < userSize; i++) {
                UserInfo ui = ((UserData) this.mUsers.valueAt(i)).info;
                if (ui.id != 0) {
                    usersToRemove.add(ui);
                }
            }
        }
        for (UserInfo ui2 : usersToRemove) {
            removeUser(ui2.id);
        }
    }

    private static void debug(String message) {
        Log.d(LOG_TAG, message + "");
    }

    static int getMaxManagedProfiles() {
        if (Build.IS_DEBUGGABLE) {
            return SystemProperties.getInt("persist.sys.max_profiles", 1);
        }
        return 1;
    }

    int getFreeProfileBadgeLU(int parentUserId) {
        int i;
        int maxManagedProfiles = getMaxManagedProfiles();
        boolean[] usedBadges = new boolean[maxManagedProfiles];
        int userSize = this.mUsers.size();
        for (i = 0; i < userSize; i++) {
            UserInfo ui = ((UserData) this.mUsers.valueAt(i)).info;
            if (ui.isManagedProfile() && ui.profileGroupId == parentUserId && (this.mRemovingUserIds.get(ui.id) ^ 1) != 0 && ui.profileBadge < maxManagedProfiles) {
                usedBadges[ui.profileBadge] = true;
            }
        }
        for (i = 0; i < maxManagedProfiles; i++) {
            if (!usedBadges[i]) {
                return i;
            }
        }
        return 0;
    }

    boolean hasManagedProfile(int userId) {
        synchronized (this.mUsersLock) {
            UserInfo userInfo = getUserInfoLU(userId);
            int userSize = this.mUsers.size();
            int i = 0;
            while (i < userSize) {
                UserInfo profile = ((UserData) this.mUsers.valueAt(i)).info;
                if (userId == profile.id || !isProfileOf(userInfo, profile)) {
                    i++;
                } else {
                    return true;
                }
            }
            return false;
        }
    }
}
