package com.android.server.pm;

import android.app.IInstantAppResolver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.InstantAppResolveInfo;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.IRemoteCallback;
import android.os.IRemoteCallback.Stub;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Slog;
import android.util.TimedRemoteCaller;
import com.android.internal.annotations.GuardedBy;
import com.android.server.SystemService;
import com.android.server.display.DisplayTransformManager;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeoutException;

final class EphemeralResolverConnection implements DeathRecipient {
    private static final long BIND_SERVICE_TIMEOUT_MS = ((long) (Build.IS_ENG ? SystemService.PHASE_SYSTEM_SERVICES_READY : DisplayTransformManager.LEVEL_COLOR_MATRIX_INVERT_COLOR));
    private static final long CALL_SERVICE_TIMEOUT_MS = ((long) (Build.IS_ENG ? DisplayTransformManager.LEVEL_COLOR_MATRIX_GRAYSCALE : 100));
    private static final boolean DEBUG_EPHEMERAL = Build.IS_DEBUGGABLE;
    private static final int STATE_BINDING = 1;
    private static final int STATE_IDLE = 0;
    private static final int STATE_PENDING = 2;
    private static final String TAG = "PackageManager";
    @GuardedBy("mLock")
    private int mBindState = 0;
    private final Context mContext;
    private final GetEphemeralResolveInfoCaller mGetEphemeralResolveInfoCaller = new GetEphemeralResolveInfoCaller();
    private final Intent mIntent;
    private final Object mLock = new Object();
    @GuardedBy("mLock")
    private IInstantAppResolver mRemoteInstance;
    private final ServiceConnection mServiceConnection = new MyServiceConnection();

    public static class ConnectionException extends Exception {
        public static final int FAILURE_BIND = 1;
        public static final int FAILURE_CALL = 2;
        public static final int FAILURE_INTERRUPTED = 3;
        public final int failure;

        public ConnectionException(int _failure) {
            this.failure = _failure;
        }
    }

    private static final class GetEphemeralResolveInfoCaller extends TimedRemoteCaller<List<InstantAppResolveInfo>> {
        private final IRemoteCallback mCallback = new Stub() {
            public void sendResult(Bundle data) throws RemoteException {
                GetEphemeralResolveInfoCaller.this.onRemoteMethodResult(data.getParcelableArrayList("android.app.extra.RESOLVE_INFO"), data.getInt("android.app.extra.SEQUENCE", -1));
            }
        };

        public GetEphemeralResolveInfoCaller() {
            super(EphemeralResolverConnection.CALL_SERVICE_TIMEOUT_MS);
        }

        public List<InstantAppResolveInfo> getEphemeralResolveInfoList(IInstantAppResolver target, int[] hashPrefix, String token) throws RemoteException, TimeoutException {
            int sequence = onBeforeRemoteCall();
            target.getInstantAppResolveInfoList(hashPrefix, token, sequence, this.mCallback);
            return (List) getResultTimed(sequence);
        }
    }

    private final class MyServiceConnection implements ServiceConnection {
        private MyServiceConnection() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            if (EphemeralResolverConnection.DEBUG_EPHEMERAL) {
                Slog.d(EphemeralResolverConnection.TAG, "Connected to instant app resolver");
            }
            synchronized (EphemeralResolverConnection.this.mLock) {
                EphemeralResolverConnection.this.mRemoteInstance = IInstantAppResolver.Stub.asInterface(service);
                if (EphemeralResolverConnection.this.mBindState == 2) {
                    EphemeralResolverConnection.this.mBindState = 0;
                }
                try {
                    service.linkToDeath(EphemeralResolverConnection.this, 0);
                } catch (RemoteException e) {
                    EphemeralResolverConnection.this.handleBinderDiedLocked();
                }
                EphemeralResolverConnection.this.mLock.notifyAll();
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            if (EphemeralResolverConnection.DEBUG_EPHEMERAL) {
                Slog.d(EphemeralResolverConnection.TAG, "Disconnected from instant app resolver");
            }
            synchronized (EphemeralResolverConnection.this.mLock) {
                EphemeralResolverConnection.this.handleBinderDiedLocked();
            }
        }
    }

    public static abstract class PhaseTwoCallback {
        abstract void onPhaseTwoResolved(List<InstantAppResolveInfo> list, long j);
    }

    public EphemeralResolverConnection(Context context, ComponentName componentName, String action) {
        this.mContext = context;
        this.mIntent = new Intent(action).setComponent(componentName);
    }

    public final List<InstantAppResolveInfo> getInstantAppResolveInfoList(int[] hashPrefix, String token) throws ConnectionException {
        throwIfCalledOnMainThread();
        try {
            List<InstantAppResolveInfo> ephemeralResolveInfoList = this.mGetEphemeralResolveInfoCaller.getEphemeralResolveInfoList(getRemoteInstanceLazy(token), hashPrefix, token);
            synchronized (this.mLock) {
                this.mLock.notifyAll();
            }
            return ephemeralResolveInfoList;
        } catch (TimeoutException e) {
            throw new ConnectionException(2);
        } catch (RemoteException e2) {
            synchronized (this.mLock) {
                this.mLock.notifyAll();
                return null;
            }
        } catch (TimeoutException e3) {
            throw new ConnectionException(1);
        } catch (InterruptedException e4) {
            throw new ConnectionException(3);
        } catch (Throwable th) {
            synchronized (this.mLock) {
                this.mLock.notifyAll();
            }
        }
    }

    public final void getInstantAppIntentFilterList(int[] hashPrefix, String token, String hostName, PhaseTwoCallback callback, Handler callbackHandler, long startTime) throws ConnectionException {
        final Handler handler = callbackHandler;
        final PhaseTwoCallback phaseTwoCallback = callback;
        final long j = startTime;
        try {
            getRemoteInstanceLazy(token).getInstantAppIntentFilterList(hashPrefix, token, hostName, new Stub() {
                public void sendResult(Bundle data) throws RemoteException {
                    final ArrayList<InstantAppResolveInfo> resolveList = data.getParcelableArrayList("android.app.extra.RESOLVE_INFO");
                    Handler handler = handler;
                    final PhaseTwoCallback phaseTwoCallback = phaseTwoCallback;
                    final long j = j;
                    handler.post(new Runnable() {
                        public void run() {
                            phaseTwoCallback.onPhaseTwoResolved(resolveList, j);
                        }
                    });
                }
            });
        } catch (TimeoutException e) {
            throw new ConnectionException(1);
        } catch (InterruptedException e2) {
            throw new ConnectionException(3);
        } catch (RemoteException e3) {
        }
    }

    private IInstantAppResolver getRemoteInstanceLazy(String token) throws ConnectionException, TimeoutException, InterruptedException {
        long binderToken = Binder.clearCallingIdentity();
        try {
            IInstantAppResolver bind = bind(token);
            return bind;
        } finally {
            Binder.restoreCallingIdentity(binderToken);
        }
    }

    private void waitForBindLocked(String token) throws TimeoutException, InterruptedException {
        long startMillis = SystemClock.uptimeMillis();
        while (this.mBindState != 0 && this.mRemoteInstance == null) {
            long remainingMillis = BIND_SERVICE_TIMEOUT_MS - (SystemClock.uptimeMillis() - startMillis);
            if (remainingMillis <= 0) {
                throw new TimeoutException("[" + token + "] Didn't bind to resolver in time!");
            }
            this.mLock.wait(remainingMillis);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.app.IInstantAppResolver bind(java.lang.String r11) throws com.android.server.pm.EphemeralResolverConnection.ConnectionException, java.util.concurrent.TimeoutException, java.lang.InterruptedException {
        /*
        r10 = this;
        r7 = 2;
        r9 = 1;
        r0 = 0;
        r6 = r10.mLock;
        monitor-enter(r6);
        r5 = r10.mRemoteInstance;	 Catch:{ all -> 0x007f }
        if (r5 == 0) goto L_0x000e;
    L_0x000a:
        r5 = r10.mRemoteInstance;	 Catch:{ all -> 0x007f }
        monitor-exit(r6);
        return r5;
    L_0x000e:
        r5 = r10.mBindState;	 Catch:{ all -> 0x007f }
        if (r5 != r7) goto L_0x0044;
    L_0x0012:
        r5 = DEBUG_EPHEMERAL;	 Catch:{ all -> 0x007f }
        if (r5 == 0) goto L_0x0037;
    L_0x0016:
        r5 = "PackageManager";
        r7 = new java.lang.StringBuilder;	 Catch:{ all -> 0x007f }
        r7.<init>();	 Catch:{ all -> 0x007f }
        r8 = "[";
        r7 = r7.append(r8);	 Catch:{ all -> 0x007f }
        r7 = r7.append(r11);	 Catch:{ all -> 0x007f }
        r8 = "] Previous bind timed out; waiting for connection";
        r7 = r7.append(r8);	 Catch:{ all -> 0x007f }
        r7 = r7.toString();	 Catch:{ all -> 0x007f }
        android.util.Slog.i(r5, r7);	 Catch:{ all -> 0x007f }
    L_0x0037:
        r10.waitForBindLocked(r11);	 Catch:{ TimeoutException -> 0x0042 }
        r5 = r10.mRemoteInstance;	 Catch:{ TimeoutException -> 0x0042 }
        if (r5 == 0) goto L_0x0044;
    L_0x003e:
        r5 = r10.mRemoteInstance;	 Catch:{ TimeoutException -> 0x0042 }
        monitor-exit(r6);
        return r5;
    L_0x0042:
        r1 = move-exception;
        r0 = 1;
    L_0x0044:
        r5 = r10.mBindState;	 Catch:{ all -> 0x007f }
        if (r5 != r9) goto L_0x0082;
    L_0x0048:
        r5 = DEBUG_EPHEMERAL;	 Catch:{ all -> 0x007f }
        if (r5 == 0) goto L_0x006d;
    L_0x004c:
        r5 = "PackageManager";
        r7 = new java.lang.StringBuilder;	 Catch:{ all -> 0x007f }
        r7.<init>();	 Catch:{ all -> 0x007f }
        r8 = "[";
        r7 = r7.append(r8);	 Catch:{ all -> 0x007f }
        r7 = r7.append(r11);	 Catch:{ all -> 0x007f }
        r8 = "] Another thread is binding; waiting for connection";
        r7 = r7.append(r8);	 Catch:{ all -> 0x007f }
        r7 = r7.toString();	 Catch:{ all -> 0x007f }
        android.util.Slog.i(r5, r7);	 Catch:{ all -> 0x007f }
    L_0x006d:
        r10.waitForBindLocked(r11);	 Catch:{ all -> 0x007f }
        r5 = r10.mRemoteInstance;	 Catch:{ all -> 0x007f }
        if (r5 == 0) goto L_0x0078;
    L_0x0074:
        r5 = r10.mRemoteInstance;	 Catch:{ all -> 0x007f }
        monitor-exit(r6);
        return r5;
    L_0x0078:
        r5 = new com.android.server.pm.EphemeralResolverConnection$ConnectionException;	 Catch:{ all -> 0x007f }
        r7 = 1;
        r5.<init>(r7);	 Catch:{ all -> 0x007f }
        throw r5;	 Catch:{ all -> 0x007f }
    L_0x007f:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x0082:
        r5 = 1;
        r10.mBindState = r5;	 Catch:{ all -> 0x007f }
        monitor-exit(r6);
        r4 = 0;
        r3 = 0;
        if (r0 == 0) goto L_0x00b6;
    L_0x008a:
        r5 = DEBUG_EPHEMERAL;	 Catch:{ all -> 0x0113 }
        if (r5 == 0) goto L_0x00af;
    L_0x008e:
        r5 = "PackageManager";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0113 }
        r6.<init>();	 Catch:{ all -> 0x0113 }
        r7 = "[";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0113 }
        r6 = r6.append(r11);	 Catch:{ all -> 0x0113 }
        r7 = "] Previous connection never established; rebinding";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0113 }
        r6 = r6.toString();	 Catch:{ all -> 0x0113 }
        android.util.Slog.i(r5, r6);	 Catch:{ all -> 0x0113 }
    L_0x00af:
        r5 = r10.mContext;	 Catch:{ all -> 0x0113 }
        r6 = r10.mServiceConnection;	 Catch:{ all -> 0x0113 }
        r5.unbindService(r6);	 Catch:{ all -> 0x0113 }
    L_0x00b6:
        r5 = DEBUG_EPHEMERAL;	 Catch:{ all -> 0x0113 }
        if (r5 == 0) goto L_0x00db;
    L_0x00ba:
        r5 = "PackageManager";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0113 }
        r6.<init>();	 Catch:{ all -> 0x0113 }
        r7 = "[";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0113 }
        r6 = r6.append(r11);	 Catch:{ all -> 0x0113 }
        r7 = "] Binding to instant app resolver";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0113 }
        r6 = r6.toString();	 Catch:{ all -> 0x0113 }
        android.util.Slog.v(r5, r6);	 Catch:{ all -> 0x0113 }
    L_0x00db:
        r2 = 67108865; // 0x4000001 float:1.504633E-36 double:3.31561847E-316;
        r5 = r10.mContext;	 Catch:{ all -> 0x0113 }
        r6 = r10.mIntent;	 Catch:{ all -> 0x0113 }
        r7 = r10.mServiceConnection;	 Catch:{ all -> 0x0113 }
        r8 = android.os.UserHandle.SYSTEM;	 Catch:{ all -> 0x0113 }
        r9 = 67108865; // 0x4000001 float:1.504633E-36 double:3.31561847E-316;
        r4 = r5.bindServiceAsUser(r6, r7, r9, r8);	 Catch:{ all -> 0x0113 }
        if (r4 == 0) goto L_0x0125;
    L_0x00ef:
        r6 = r10.mLock;	 Catch:{ all -> 0x0113 }
        monitor-enter(r6);	 Catch:{ all -> 0x0113 }
        r10.waitForBindLocked(r11);	 Catch:{ all -> 0x0110 }
        r3 = r10.mRemoteInstance;	 Catch:{ all -> 0x0110 }
        monitor-exit(r6);	 Catch:{ all -> 0x0113 }
        r6 = r10.mLock;
        monitor-enter(r6);
        if (r4 == 0) goto L_0x0109;
    L_0x00fd:
        if (r3 != 0) goto L_0x0109;
    L_0x00ff:
        r5 = 2;
        r10.mBindState = r5;	 Catch:{ all -> 0x010d }
    L_0x0102:
        r5 = r10.mLock;	 Catch:{ all -> 0x010d }
        r5.notifyAll();	 Catch:{ all -> 0x010d }
        monitor-exit(r6);
        return r3;
    L_0x0109:
        r5 = 0;
        r10.mBindState = r5;	 Catch:{ all -> 0x010d }
        goto L_0x0102;
    L_0x010d:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
    L_0x0110:
        r5 = move-exception;
        monitor-exit(r6);	 Catch:{ all -> 0x0113 }
        throw r5;	 Catch:{ all -> 0x0113 }
    L_0x0113:
        r5 = move-exception;
        r6 = r10.mLock;
        monitor-enter(r6);
        if (r4 == 0) goto L_0x0153;
    L_0x0119:
        if (r3 != 0) goto L_0x0153;
    L_0x011b:
        r7 = 2;
        r10.mBindState = r7;	 Catch:{ all -> 0x0157 }
    L_0x011e:
        r7 = r10.mLock;	 Catch:{ all -> 0x0157 }
        r7.notifyAll();	 Catch:{ all -> 0x0157 }
        monitor-exit(r6);
        throw r5;
    L_0x0125:
        r5 = "PackageManager";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0113 }
        r6.<init>();	 Catch:{ all -> 0x0113 }
        r7 = "[";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0113 }
        r6 = r6.append(r11);	 Catch:{ all -> 0x0113 }
        r7 = "] Failed to bind to: ";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0113 }
        r7 = r10.mIntent;	 Catch:{ all -> 0x0113 }
        r6 = r6.append(r7);	 Catch:{ all -> 0x0113 }
        r6 = r6.toString();	 Catch:{ all -> 0x0113 }
        android.util.Slog.w(r5, r6);	 Catch:{ all -> 0x0113 }
        r5 = new com.android.server.pm.EphemeralResolverConnection$ConnectionException;	 Catch:{ all -> 0x0113 }
        r6 = 1;
        r5.<init>(r6);	 Catch:{ all -> 0x0113 }
        throw r5;	 Catch:{ all -> 0x0113 }
    L_0x0153:
        r7 = 0;
        r10.mBindState = r7;	 Catch:{ all -> 0x0157 }
        goto L_0x011e;
    L_0x0157:
        r5 = move-exception;
        monitor-exit(r6);
        throw r5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.EphemeralResolverConnection.bind(java.lang.String):android.app.IInstantAppResolver");
    }

    private void throwIfCalledOnMainThread() {
        if (Thread.currentThread() == this.mContext.getMainLooper().getThread()) {
            throw new RuntimeException("Cannot invoke on the main thread");
        }
    }

    public void binderDied() {
        if (DEBUG_EPHEMERAL) {
            Slog.d(TAG, "Binder to instant app resolver died");
        }
        synchronized (this.mLock) {
            handleBinderDiedLocked();
        }
    }

    private void handleBinderDiedLocked() {
        if (this.mRemoteInstance != null) {
            try {
                this.mRemoteInstance.asBinder().unlinkToDeath(this, 0);
            } catch (NoSuchElementException e) {
            }
        }
        this.mRemoteInstance = null;
    }
}
