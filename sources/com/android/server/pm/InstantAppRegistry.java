package com.android.server.pm;

import android.content.pm.InstantAppInfo;
import android.content.pm.PackageParser.Package;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings.Global;
import android.util.ArrayMap;
import android.util.AtomicFile;
import android.util.ByteStringUtils;
import android.util.PackageUtils;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.Xml;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.os.BackgroundThread;
import com.android.internal.os.SomeArgs;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.XmlUtils;
import com.android.server.job.controllers.JobStatus;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Predicate;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class InstantAppRegistry {
    private static final String ATTR_GRANTED = "granted";
    private static final String ATTR_LABEL = "label";
    private static final String ATTR_NAME = "name";
    private static final boolean DEBUG = false;
    private static final long DEFAULT_INSTALLED_INSTANT_APP_MAX_CACHE_PERIOD = 15552000000L;
    static final long DEFAULT_INSTALLED_INSTANT_APP_MIN_CACHE_PERIOD = 604800000;
    private static final long DEFAULT_UNINSTALLED_INSTANT_APP_MAX_CACHE_PERIOD = 15552000000L;
    static final long DEFAULT_UNINSTALLED_INSTANT_APP_MIN_CACHE_PERIOD = 604800000;
    private static final String INSTANT_APPS_FOLDER = "instant";
    private static final String INSTANT_APP_ANDROID_ID_FILE = "android_id";
    private static final String INSTANT_APP_COOKIE_FILE_PREFIX = "cookie_";
    private static final String INSTANT_APP_COOKIE_FILE_SIFFIX = ".dat";
    private static final String INSTANT_APP_ICON_FILE = "icon.png";
    private static final String INSTANT_APP_METADATA_FILE = "metadata.xml";
    private static final String LOG_TAG = "InstantAppRegistry";
    private static final String TAG_PACKAGE = "package";
    private static final String TAG_PERMISSION = "permission";
    private static final String TAG_PERMISSIONS = "permissions";
    private final CookiePersistence mCookiePersistence = new CookiePersistence(BackgroundThread.getHandler().getLooper());
    @GuardedBy("mService.mPackages")
    private SparseArray<SparseBooleanArray> mInstalledInstantAppUids;
    @GuardedBy("mService.mPackages")
    private SparseArray<SparseArray<SparseBooleanArray>> mInstantGrants;
    private final PackageManagerService mService;
    @GuardedBy("mService.mPackages")
    private SparseArray<List<UninstalledInstantAppState>> mUninstalledInstantApps;

    private final class CookiePersistence extends Handler {
        private static final long PERSIST_COOKIE_DELAY_MILLIS = 1000;
        private final SparseArray<ArrayMap<Package, SomeArgs>> mPendingPersistCookies = new SparseArray();

        public CookiePersistence(Looper looper) {
            super(looper);
        }

        public void schedulePersistLPw(int userId, Package pkg, byte[] cookie) {
            File newCookieFile = InstantAppRegistry.computeInstantCookieFile(pkg.packageName, PackageUtils.computeSignaturesSha256Digest(pkg.mSignatures), userId);
            if (pkg.mSignatures.length > 0) {
                File oldCookieFile = InstantAppRegistry.peekInstantCookieFile(pkg.packageName, userId);
                if (!(oldCookieFile == null || (newCookieFile.equals(oldCookieFile) ^ 1) == 0)) {
                    oldCookieFile.delete();
                }
            }
            cancelPendingPersistLPw(pkg, userId);
            addPendingPersistCookieLPw(userId, pkg, cookie, newCookieFile);
            sendMessageDelayed(obtainMessage(userId, pkg), 1000);
        }

        public byte[] getPendingPersistCookieLPr(Package pkg, int userId) {
            ArrayMap<Package, SomeArgs> pendingWorkForUser = (ArrayMap) this.mPendingPersistCookies.get(userId);
            if (pendingWorkForUser != null) {
                SomeArgs state = (SomeArgs) pendingWorkForUser.get(pkg);
                if (state != null) {
                    return (byte[]) state.arg1;
                }
            }
            return null;
        }

        public void cancelPendingPersistLPw(Package pkg, int userId) {
            removeMessages(userId, pkg);
            SomeArgs state = removePendingPersistCookieLPr(pkg, userId);
            if (state != null) {
                state.recycle();
            }
        }

        private void addPendingPersistCookieLPw(int userId, Package pkg, byte[] cookie, File cookieFile) {
            ArrayMap<Package, SomeArgs> pendingWorkForUser = (ArrayMap) this.mPendingPersistCookies.get(userId);
            if (pendingWorkForUser == null) {
                pendingWorkForUser = new ArrayMap();
                this.mPendingPersistCookies.put(userId, pendingWorkForUser);
            }
            SomeArgs args = SomeArgs.obtain();
            args.arg1 = cookie;
            args.arg2 = cookieFile;
            pendingWorkForUser.put(pkg, args);
        }

        private SomeArgs removePendingPersistCookieLPr(Package pkg, int userId) {
            ArrayMap<Package, SomeArgs> pendingWorkForUser = (ArrayMap) this.mPendingPersistCookies.get(userId);
            SomeArgs state = null;
            if (pendingWorkForUser != null) {
                state = (SomeArgs) pendingWorkForUser.remove(pkg);
                if (pendingWorkForUser.isEmpty()) {
                    this.mPendingPersistCookies.remove(userId);
                }
            }
            return state;
        }

        public void handleMessage(Message message) {
            int userId = message.what;
            Package pkg = message.obj;
            SomeArgs state = removePendingPersistCookieLPr(pkg, userId);
            if (state != null) {
                byte[] cookie = state.arg1;
                File cookieFile = state.arg2;
                state.recycle();
                InstantAppRegistry.this.persistInstantApplicationCookie(cookie, pkg.packageName, cookieFile, userId);
            }
        }
    }

    private static final class UninstalledInstantAppState {
        final InstantAppInfo mInstantAppInfo;
        final long mTimestamp;

        public UninstalledInstantAppState(InstantAppInfo instantApp, long timestamp) {
            this.mInstantAppInfo = instantApp;
            this.mTimestamp = timestamp;
        }
    }

    public InstantAppRegistry(PackageManagerService service) {
        this.mService = service;
    }

    public byte[] getInstantAppCookieLPw(String packageName, int userId) {
        Package pkg = (Package) this.mService.mPackages.get(packageName);
        if (pkg == null) {
            return null;
        }
        byte[] pendingCookie = this.mCookiePersistence.getPendingPersistCookieLPr(pkg, userId);
        if (pendingCookie != null) {
            return pendingCookie;
        }
        File cookieFile = peekInstantCookieFile(packageName, userId);
        if (cookieFile != null && cookieFile.exists()) {
            try {
                return IoUtils.readFileAsByteArray(cookieFile.toString());
            } catch (IOException e) {
                Slog.w(LOG_TAG, "Error reading cookie file: " + cookieFile);
            }
        }
        return null;
    }

    public boolean setInstantAppCookieLPw(String packageName, byte[] cookie, int userId) {
        if (cookie != null && cookie.length > 0) {
            int maxCookieSize = this.mService.mContext.getPackageManager().getInstantAppCookieMaxBytes();
            if (cookie.length > maxCookieSize) {
                Slog.e(LOG_TAG, "Instant app cookie for package " + packageName + " size " + cookie.length + " bytes while max size is " + maxCookieSize);
                return false;
            }
        }
        Package pkg = (Package) this.mService.mPackages.get(packageName);
        if (pkg == null) {
            return false;
        }
        this.mCookiePersistence.schedulePersistLPw(userId, pkg, cookie);
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void persistInstantApplicationCookie(byte[] r10, java.lang.String r11, java.io.File r12, int r13) {
        /*
        r9 = this;
        r5 = 0;
        r4 = r9.mService;
        r6 = r4.mPackages;
        monitor-enter(r6);
        r0 = getInstantApplicationDir(r11, r13);	 Catch:{ all -> 0x0072 }
        r4 = r0.exists();	 Catch:{ all -> 0x0072 }
        if (r4 != 0) goto L_0x0023;
    L_0x0010:
        r4 = r0.mkdirs();	 Catch:{ all -> 0x0072 }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x0023;
    L_0x0018:
        r4 = "InstantAppRegistry";
        r5 = "Cannot create instant app cookie directory";
        android.util.Slog.e(r4, r5);	 Catch:{ all -> 0x0072 }
        monitor-exit(r6);
        return;
    L_0x0023:
        r4 = r12.exists();	 Catch:{ all -> 0x0072 }
        if (r4 == 0) goto L_0x003a;
    L_0x0029:
        r4 = r12.delete();	 Catch:{ all -> 0x0072 }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x003a;
    L_0x0031:
        r4 = "InstantAppRegistry";
        r7 = "Cannot delete instant app cookie file";
        android.util.Slog.e(r4, r7);	 Catch:{ all -> 0x0072 }
    L_0x003a:
        if (r10 == 0) goto L_0x003f;
    L_0x003c:
        r4 = r10.length;	 Catch:{ all -> 0x0072 }
        if (r4 > 0) goto L_0x0041;
    L_0x003f:
        monitor-exit(r6);
        return;
    L_0x0041:
        monitor-exit(r6);
        r2 = 0;
        r3 = new java.io.FileOutputStream;	 Catch:{ Throwable -> 0x0079, all -> 0x0095 }
        r3.<init>(r12);	 Catch:{ Throwable -> 0x0079, all -> 0x0095 }
        r4 = r10.length;	 Catch:{ Throwable -> 0x009a, all -> 0x0097 }
        r6 = 0;
        r3.write(r10, r6, r4);	 Catch:{ Throwable -> 0x009a, all -> 0x0097 }
        if (r3 == 0) goto L_0x0052;
    L_0x004f:
        r3.close();	 Catch:{ Throwable -> 0x0075 }
    L_0x0052:
        if (r5 == 0) goto L_0x0077;
    L_0x0054:
        throw r5;	 Catch:{ IOException -> 0x0055 }
    L_0x0055:
        r1 = move-exception;
        r2 = r3;
    L_0x0057:
        r4 = "InstantAppRegistry";
        r5 = new java.lang.StringBuilder;
        r5.<init>();
        r6 = "Error writing instant app cookie file: ";
        r5 = r5.append(r6);
        r5 = r5.append(r12);
        r5 = r5.toString();
        android.util.Slog.e(r4, r5, r1);
    L_0x0071:
        return;
    L_0x0072:
        r4 = move-exception;
        monitor-exit(r6);
        throw r4;
    L_0x0075:
        r5 = move-exception;
        goto L_0x0052;
    L_0x0077:
        r2 = r3;
        goto L_0x0071;
    L_0x0079:
        r4 = move-exception;
    L_0x007a:
        throw r4;	 Catch:{ all -> 0x007b }
    L_0x007b:
        r5 = move-exception;
        r8 = r5;
        r5 = r4;
        r4 = r8;
    L_0x007f:
        if (r2 == 0) goto L_0x0084;
    L_0x0081:
        r2.close();	 Catch:{ Throwable -> 0x0089 }
    L_0x0084:
        if (r5 == 0) goto L_0x0094;
    L_0x0086:
        throw r5;	 Catch:{ IOException -> 0x0087 }
    L_0x0087:
        r1 = move-exception;
        goto L_0x0057;
    L_0x0089:
        r6 = move-exception;
        if (r5 != 0) goto L_0x008e;
    L_0x008c:
        r5 = r6;
        goto L_0x0084;
    L_0x008e:
        if (r5 == r6) goto L_0x0084;
    L_0x0090:
        r5.addSuppressed(r6);	 Catch:{ IOException -> 0x0087 }
        goto L_0x0084;
    L_0x0094:
        throw r4;	 Catch:{ IOException -> 0x0087 }
    L_0x0095:
        r4 = move-exception;
        goto L_0x007f;
    L_0x0097:
        r4 = move-exception;
        r2 = r3;
        goto L_0x007f;
    L_0x009a:
        r4 = move-exception;
        r2 = r3;
        goto L_0x007a;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.InstantAppRegistry.persistInstantApplicationCookie(byte[], java.lang.String, java.io.File, int):void");
    }

    public Bitmap getInstantAppIconLPw(String packageName, int userId) {
        File iconFile = new File(getInstantApplicationDir(packageName, userId), INSTANT_APP_ICON_FILE);
        if (iconFile.exists()) {
            return BitmapFactory.decodeFile(iconFile.toString());
        }
        return null;
    }

    public String getInstantAppAndroidIdLPw(String packageName, int userId) {
        File idFile = new File(getInstantApplicationDir(packageName, userId), INSTANT_APP_ANDROID_ID_FILE);
        if (idFile.exists()) {
            try {
                return IoUtils.readFileAsString(idFile.getAbsolutePath());
            } catch (IOException e) {
                Slog.e(LOG_TAG, "Failed to read instant app android id file: " + idFile, e);
            }
        }
        return generateInstantAppAndroidIdLPw(packageName, userId);
    }

    private String generateInstantAppAndroidIdLPw(String packageName, int userId) {
        IOException e;
        Throwable th;
        Throwable th2 = null;
        byte[] randomBytes = new byte[8];
        new SecureRandom().nextBytes(randomBytes);
        String id = ByteStringUtils.toHexString(randomBytes).toLowerCase(Locale.US);
        File appDir = getInstantApplicationDir(packageName, userId);
        if (appDir.exists() || (appDir.mkdirs() ^ 1) == 0) {
            File idFile = new File(getInstantApplicationDir(packageName, userId), INSTANT_APP_ANDROID_ID_FILE);
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream fos = new FileOutputStream(idFile);
                try {
                    fos.write(id.getBytes());
                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (Throwable th3) {
                            th2 = th3;
                        }
                    }
                    if (th2 != null) {
                        try {
                            throw th2;
                        } catch (IOException e2) {
                            e = e2;
                            fileOutputStream = fos;
                        }
                    } else {
                        return id;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    fileOutputStream = fos;
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (Throwable th5) {
                            if (th2 == null) {
                                th2 = th5;
                            } else if (th2 != th5) {
                                th2.addSuppressed(th5);
                            }
                        }
                    }
                    if (th2 == null) {
                        try {
                            throw th2;
                        } catch (IOException e3) {
                            e = e3;
                            Slog.e(LOG_TAG, "Error writing instant app android id file: " + idFile, e);
                            return id;
                        }
                    }
                    throw th;
                }
            } catch (Throwable th6) {
                th = th6;
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                if (th2 == null) {
                    throw th;
                }
                throw th2;
            }
        }
        Slog.e(LOG_TAG, "Cannot create instant app cookie directory");
        return id;
    }

    public List<InstantAppInfo> getInstantAppsLPr(int userId) {
        List<InstantAppInfo> installedApps = getInstalledInstantApplicationsLPr(userId);
        List<InstantAppInfo> uninstalledApps = getUninstalledInstantApplicationsLPr(userId);
        if (installedApps == null) {
            return uninstalledApps;
        }
        if (uninstalledApps != null) {
            installedApps.addAll(uninstalledApps);
        }
        return installedApps;
    }

    public void onPackageInstalledLPw(Package pkg, int[] userIds) {
        PackageSetting ps = pkg.mExtras;
        if (ps != null) {
            for (int userId : userIds) {
                if (this.mService.mPackages.get(pkg.packageName) != null && (ps.getInstalled(userId) ^ 1) == 0) {
                    propagateInstantAppPermissionsIfNeeded(pkg.packageName, userId);
                    if (ps.getInstantApp(userId)) {
                        addInstantAppLPw(userId, ps.appId);
                    }
                    removeUninstalledInstantAppStateLPw(new -$Lambda$KFbchFEqJgs_hY1HweauKRNA_ds((byte) 1, pkg), userId);
                    File instantAppDir = getInstantApplicationDir(pkg.packageName, userId);
                    new File(instantAppDir, INSTANT_APP_METADATA_FILE).delete();
                    new File(instantAppDir, INSTANT_APP_ICON_FILE).delete();
                    File currentCookieFile = peekInstantCookieFile(pkg.packageName, userId);
                    if (currentCookieFile != null) {
                        String[] signaturesSha256Digests = PackageUtils.computeSignaturesSha256Digests(pkg.mSignatures);
                        if (!currentCookieFile.equals(computeInstantCookieFile(pkg.packageName, PackageUtils.computeSignaturesSha256Digest(signaturesSha256Digests), userId))) {
                            if (pkg.mSignatures.length <= 1 || !currentCookieFile.equals(computeInstantCookieFile(pkg.packageName, signaturesSha256Digests[0], userId))) {
                                Slog.i(LOG_TAG, "Signature for package " + pkg.packageName + " changed - dropping cookie");
                                this.mCookiePersistence.cancelPendingPersistLPw(pkg, userId);
                                currentCookieFile.delete();
                            } else {
                                return;
                            }
                        }
                        return;
                    }
                    continue;
                }
            }
        }
    }

    public void onPackageUninstalledLPw(Package pkg, int[] userIds) {
        PackageSetting ps = pkg.mExtras;
        if (ps != null) {
            for (int userId : userIds) {
                if (this.mService.mPackages.get(pkg.packageName) == null || !ps.getInstalled(userId)) {
                    if (ps.getInstantApp(userId)) {
                        addUninstalledInstantAppLPw(pkg, userId);
                        removeInstantAppLPw(userId, ps.appId);
                    } else {
                        deleteDir(getInstantApplicationDir(pkg.packageName, userId));
                        this.mCookiePersistence.cancelPendingPersistLPw(pkg, userId);
                        removeAppLPw(userId, ps.appId);
                    }
                }
            }
        }
    }

    public void onUserRemovedLPw(int userId) {
        if (this.mUninstalledInstantApps != null) {
            this.mUninstalledInstantApps.remove(userId);
            if (this.mUninstalledInstantApps.size() <= 0) {
                this.mUninstalledInstantApps = null;
            }
        }
        if (this.mInstalledInstantAppUids != null) {
            this.mInstalledInstantAppUids.remove(userId);
            if (this.mInstalledInstantAppUids.size() <= 0) {
                this.mInstalledInstantAppUids = null;
            }
        }
        if (this.mInstantGrants != null) {
            this.mInstantGrants.remove(userId);
            if (this.mInstantGrants.size() <= 0) {
                this.mInstantGrants = null;
            }
        }
        deleteDir(getInstantApplicationsDir(userId));
    }

    public boolean isInstantAccessGranted(int userId, int targetAppId, int instantAppId) {
        if (this.mInstantGrants == null) {
            return false;
        }
        SparseArray<SparseBooleanArray> targetAppList = (SparseArray) this.mInstantGrants.get(userId);
        if (targetAppList == null) {
            return false;
        }
        SparseBooleanArray instantGrantList = (SparseBooleanArray) targetAppList.get(targetAppId);
        if (instantGrantList == null) {
            return false;
        }
        return instantGrantList.get(instantAppId);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void grantInstantAccessLPw(int r7, android.content.Intent r8, int r9, int r10) {
        /*
        r6 = this;
        r4 = r6.mInstalledInstantAppUids;
        if (r4 != 0) goto L_0x0005;
    L_0x0004:
        return;
    L_0x0005:
        r4 = r6.mInstalledInstantAppUids;
        r1 = r4.get(r7);
        r1 = (android.util.SparseBooleanArray) r1;
        if (r1 == 0) goto L_0x0017;
    L_0x000f:
        r4 = r1.get(r10);
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x0018;
    L_0x0017:
        return;
    L_0x0018:
        r4 = r1.get(r9);
        if (r4 == 0) goto L_0x001f;
    L_0x001e:
        return;
    L_0x001f:
        if (r8 == 0) goto L_0x003e;
    L_0x0021:
        r4 = "android.intent.action.VIEW";
        r5 = r8.getAction();
        r4 = r4.equals(r5);
        if (r4 == 0) goto L_0x003e;
    L_0x002e:
        r0 = r8.getCategories();
        if (r0 == 0) goto L_0x003e;
    L_0x0034:
        r4 = "android.intent.category.BROWSABLE";
        r4 = r0.contains(r4);
        if (r4 == 0) goto L_0x003e;
    L_0x003d:
        return;
    L_0x003e:
        r4 = r6.mInstantGrants;
        if (r4 != 0) goto L_0x0049;
    L_0x0042:
        r4 = new android.util.SparseArray;
        r4.<init>();
        r6.mInstantGrants = r4;
    L_0x0049:
        r4 = r6.mInstantGrants;
        r3 = r4.get(r7);
        r3 = (android.util.SparseArray) r3;
        if (r3 != 0) goto L_0x005d;
    L_0x0053:
        r3 = new android.util.SparseArray;
        r3.<init>();
        r4 = r6.mInstantGrants;
        r4.put(r7, r3);
    L_0x005d:
        r2 = r3.get(r9);
        r2 = (android.util.SparseBooleanArray) r2;
        if (r2 != 0) goto L_0x006d;
    L_0x0065:
        r2 = new android.util.SparseBooleanArray;
        r2.<init>();
        r3.put(r9, r2);
    L_0x006d:
        r4 = 1;
        r2.put(r10, r4);
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.InstantAppRegistry.grantInstantAccessLPw(int, android.content.Intent, int, int):void");
    }

    public void addInstantAppLPw(int userId, int instantAppId) {
        if (this.mInstalledInstantAppUids == null) {
            this.mInstalledInstantAppUids = new SparseArray();
        }
        SparseBooleanArray instantAppList = (SparseBooleanArray) this.mInstalledInstantAppUids.get(userId);
        if (instantAppList == null) {
            instantAppList = new SparseBooleanArray();
            this.mInstalledInstantAppUids.put(userId, instantAppList);
        }
        instantAppList.put(instantAppId, true);
    }

    private void removeInstantAppLPw(int userId, int instantAppId) {
        if (this.mInstalledInstantAppUids != null) {
            SparseBooleanArray instantAppList = (SparseBooleanArray) this.mInstalledInstantAppUids.get(userId);
            if (instantAppList != null) {
                instantAppList.delete(instantAppId);
                if (this.mInstantGrants != null) {
                    SparseArray<SparseBooleanArray> targetAppList = (SparseArray) this.mInstantGrants.get(userId);
                    if (targetAppList != null) {
                        for (int i = targetAppList.size() - 1; i >= 0; i--) {
                            ((SparseBooleanArray) targetAppList.valueAt(i)).delete(instantAppId);
                        }
                    }
                }
            }
        }
    }

    private void removeAppLPw(int userId, int targetAppId) {
        if (this.mInstantGrants != null) {
            SparseArray<SparseBooleanArray> targetAppList = (SparseArray) this.mInstantGrants.get(userId);
            if (targetAppList != null) {
                targetAppList.delete(targetAppId);
            }
        }
    }

    private void addUninstalledInstantAppLPw(Package pkg, int userId) {
        InstantAppInfo uninstalledApp = createInstantAppInfoForPackage(pkg, userId, false);
        if (uninstalledApp != null) {
            if (this.mUninstalledInstantApps == null) {
                this.mUninstalledInstantApps = new SparseArray();
            }
            List<UninstalledInstantAppState> uninstalledAppStates = (List) this.mUninstalledInstantApps.get(userId);
            if (uninstalledAppStates == null) {
                uninstalledAppStates = new ArrayList();
                this.mUninstalledInstantApps.put(userId, uninstalledAppStates);
            }
            uninstalledAppStates.add(new UninstalledInstantAppState(uninstalledApp, System.currentTimeMillis()));
            writeUninstalledInstantAppMetadata(uninstalledApp, userId);
            writeInstantApplicationIconLPw(pkg, userId);
        }
    }

    private void writeInstantApplicationIconLPw(Package pkg, int userId) {
        Exception e;
        Throwable th;
        if (getInstantApplicationDir(pkg.packageName, userId).exists()) {
            Bitmap bitmap;
            Drawable icon = pkg.applicationInfo.loadIcon(this.mService.mContext.getPackageManager());
            if (icon instanceof BitmapDrawable) {
                bitmap = ((BitmapDrawable) icon).getBitmap();
            } else {
                bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight(), Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                icon.setBounds(0, 0, icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
                icon.draw(canvas);
            }
            Throwable th2 = null;
            FileOutputStream fileOutputStream = null;
            try {
                FileOutputStream out = new FileOutputStream(new File(getInstantApplicationDir(pkg.packageName, userId), INSTANT_APP_ICON_FILE));
                try {
                    bitmap.compress(CompressFormat.PNG, 100, out);
                    if (out != null) {
                        try {
                            out.close();
                        } catch (Throwable th3) {
                            th2 = th3;
                        }
                    }
                    if (th2 != null) {
                        try {
                            throw th2;
                        } catch (Exception e2) {
                            e = e2;
                            fileOutputStream = out;
                        }
                    }
                } catch (Throwable th4) {
                    th = th4;
                    fileOutputStream = out;
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (Throwable th5) {
                            if (th2 == null) {
                                th2 = th5;
                            } else if (th2 != th5) {
                                th2.addSuppressed(th5);
                            }
                        }
                    }
                    if (th2 == null) {
                        try {
                            throw th2;
                        } catch (Exception e3) {
                            e = e3;
                            Slog.e(LOG_TAG, "Error writing instant app icon", e);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th6) {
                th = th6;
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                if (th2 == null) {
                    throw th;
                }
                throw th2;
            }
        }
    }

    boolean hasInstantApplicationMetadataLPr(String packageName, int userId) {
        if (hasUninstalledInstantAppStateLPr(packageName, userId)) {
            return true;
        }
        return hasInstantAppMetadataLPr(packageName, userId);
    }

    public void deleteInstantApplicationMetadataLPw(String packageName, int userId) {
        removeUninstalledInstantAppStateLPw(new -$Lambda$KFbchFEqJgs_hY1HweauKRNA_ds((byte) 0, packageName), userId);
        File instantAppDir = getInstantApplicationDir(packageName, userId);
        new File(instantAppDir, INSTANT_APP_METADATA_FILE).delete();
        new File(instantAppDir, INSTANT_APP_ICON_FILE).delete();
        new File(instantAppDir, INSTANT_APP_ANDROID_ID_FILE).delete();
        File cookie = peekInstantCookieFile(packageName, userId);
        if (cookie != null) {
            cookie.delete();
        }
    }

    private void removeUninstalledInstantAppStateLPw(Predicate<UninstalledInstantAppState> criteria, int userId) {
        if (this.mUninstalledInstantApps != null) {
            List<UninstalledInstantAppState> uninstalledAppStates = (List) this.mUninstalledInstantApps.get(userId);
            if (uninstalledAppStates != null) {
                for (int i = uninstalledAppStates.size() - 1; i >= 0; i--) {
                    if (criteria.test((UninstalledInstantAppState) uninstalledAppStates.get(i))) {
                        uninstalledAppStates.remove(i);
                        if (uninstalledAppStates.isEmpty()) {
                            this.mUninstalledInstantApps.remove(userId);
                            if (this.mUninstalledInstantApps.size() <= 0) {
                                this.mUninstalledInstantApps = null;
                            }
                            return;
                        }
                    }
                }
            }
        }
    }

    private boolean hasUninstalledInstantAppStateLPr(String packageName, int userId) {
        if (this.mUninstalledInstantApps == null) {
            return false;
        }
        List<UninstalledInstantAppState> uninstalledAppStates = (List) this.mUninstalledInstantApps.get(userId);
        if (uninstalledAppStates == null) {
            return false;
        }
        int appCount = uninstalledAppStates.size();
        for (int i = 0; i < appCount; i++) {
            if (packageName.equals(((UninstalledInstantAppState) uninstalledAppStates.get(i)).mInstantAppInfo.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    private boolean hasInstantAppMetadataLPr(String packageName, int userId) {
        File instantAppDir = getInstantApplicationDir(packageName, userId);
        if (new File(instantAppDir, INSTANT_APP_METADATA_FILE).exists() || new File(instantAppDir, INSTANT_APP_ICON_FILE).exists() || new File(instantAppDir, INSTANT_APP_ANDROID_ID_FILE).exists() || peekInstantCookieFile(packageName, userId) != null) {
            return true;
        }
        return false;
    }

    void pruneInstantApps() {
        try {
            pruneInstantApps(JobStatus.NO_LATEST_RUNTIME, Global.getLong(this.mService.mContext.getContentResolver(), "installed_instant_app_max_cache_period", 15552000000L), Global.getLong(this.mService.mContext.getContentResolver(), "uninstalled_instant_app_max_cache_period", 15552000000L));
        } catch (IOException e) {
            Slog.e(LOG_TAG, "Error pruning installed and uninstalled instant apps", e);
        }
    }

    boolean pruneInstalledInstantApps(long neededSpace, long maxInstalledCacheDuration) {
        try {
            return pruneInstantApps(neededSpace, maxInstalledCacheDuration, JobStatus.NO_LATEST_RUNTIME);
        } catch (IOException e) {
            Slog.e(LOG_TAG, "Error pruning installed instant apps", e);
            return false;
        }
    }

    boolean pruneUninstalledInstantApps(long neededSpace, long maxUninstalledCacheDuration) {
        try {
            return pruneInstantApps(neededSpace, JobStatus.NO_LATEST_RUNTIME, maxUninstalledCacheDuration);
        } catch (IOException e) {
            Slog.e(LOG_TAG, "Error pruning uninstalled instant apps", e);
            return false;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean pruneInstantApps(long r38, long r40, long r42) throws java.io.IOException {
        /*
        r37 = this;
        r0 = r37;
        r0 = r0.mService;
        r26 = r0;
        r0 = r26;
        r0 = r0.mContext;
        r26 = r0;
        r27 = android.os.storage.StorageManager.class;
        r24 = r26.getSystemService(r27);
        r24 = (android.os.storage.StorageManager) r24;
        r26 = android.os.storage.StorageManager.UUID_PRIVATE_INTERNAL;
        r0 = r24;
        r1 = r26;
        r7 = r0.findPathForUuid(r1);
        r26 = r7.getUsableSpace();
        r26 = (r26 > r38 ? 1 : (r26 == r38 ? 0 : -1));
        if (r26 < 0) goto L_0x0029;
    L_0x0026:
        r26 = 1;
        return r26;
    L_0x0029:
        r20 = 0;
        r16 = java.lang.System.currentTimeMillis();
        r0 = r37;
        r0 = r0.mService;
        r26 = r0;
        r0 = r26;
        r0 = r0.mPackages;
        r27 = r0;
        monitor-enter(r27);
        r26 = com.android.server.pm.PackageManagerService.sUserManager;	 Catch:{ all -> 0x00d5 }
        r6 = r26.getUserIds();	 Catch:{ all -> 0x00d5 }
        r0 = r37;
        r0 = r0.mService;	 Catch:{ all -> 0x00d5 }
        r26 = r0;
        r0 = r26;
        r0 = r0.mPackages;	 Catch:{ all -> 0x00d5 }
        r26 = r0;
        r18 = r26.size();	 Catch:{ all -> 0x00d5 }
        r11 = 0;
        r21 = r20;
    L_0x0055:
        r0 = r18;
        if (r11 >= r0) goto L_0x00d8;
    L_0x0059:
        r0 = r37;
        r0 = r0.mService;	 Catch:{ all -> 0x01c9 }
        r26 = r0;
        r0 = r26;
        r0 = r0.mPackages;	 Catch:{ all -> 0x01c9 }
        r26 = r0;
        r0 = r26;
        r22 = r0.valueAt(r11);	 Catch:{ all -> 0x01c9 }
        r22 = (android.content.pm.PackageParser.Package) r22;	 Catch:{ all -> 0x01c9 }
        r28 = r22.getLatestPackageUseTimeInMills();	 Catch:{ all -> 0x01c9 }
        r28 = r16 - r28;
        r26 = (r28 > r40 ? 1 : (r28 == r40 ? 0 : -1));
        if (r26 >= 0) goto L_0x007e;
    L_0x0077:
        r20 = r21;
    L_0x0079:
        r11 = r11 + 1;
        r21 = r20;
        goto L_0x0055;
    L_0x007e:
        r0 = r22;
        r0 = r0.mExtras;	 Catch:{ all -> 0x01c9 }
        r26 = r0;
        r0 = r26;
        r0 = r0 instanceof com.android.server.pm.PackageSetting;	 Catch:{ all -> 0x01c9 }
        r26 = r0;
        if (r26 != 0) goto L_0x008f;
    L_0x008c:
        r20 = r21;
        goto L_0x0079;
    L_0x008f:
        r0 = r22;
        r0 = r0.mExtras;	 Catch:{ all -> 0x01c9 }
        r23 = r0;
        r23 = (com.android.server.pm.PackageSetting) r23;	 Catch:{ all -> 0x01c9 }
        r12 = 0;
        r26 = 0;
        r0 = r6.length;	 Catch:{ all -> 0x01c9 }
        r28 = r0;
    L_0x009d:
        r0 = r26;
        r1 = r28;
        if (r0 >= r1) goto L_0x00be;
    L_0x00a3:
        r25 = r6[r26];	 Catch:{ all -> 0x01c9 }
        r0 = r23;
        r1 = r25;
        r29 = r0.getInstalled(r1);	 Catch:{ all -> 0x01c9 }
        if (r29 == 0) goto L_0x00ba;
    L_0x00af:
        r0 = r23;
        r1 = r25;
        r29 = r0.getInstantApp(r1);	 Catch:{ all -> 0x01c9 }
        if (r29 == 0) goto L_0x00bd;
    L_0x00b9:
        r12 = 1;
    L_0x00ba:
        r26 = r26 + 1;
        goto L_0x009d;
    L_0x00bd:
        r12 = 0;
    L_0x00be:
        if (r12 == 0) goto L_0x01d2;
    L_0x00c0:
        if (r21 != 0) goto L_0x01ce;
    L_0x00c2:
        r20 = new java.util.ArrayList;	 Catch:{ all -> 0x01c9 }
        r20.<init>();	 Catch:{ all -> 0x01c9 }
    L_0x00c7:
        r0 = r22;
        r0 = r0.packageName;	 Catch:{ all -> 0x00d5 }
        r26 = r0;
        r0 = r20;
        r1 = r26;
        r0.add(r1);	 Catch:{ all -> 0x00d5 }
        goto L_0x0079;
    L_0x00d5:
        r26 = move-exception;
    L_0x00d6:
        monitor-exit(r27);
        throw r26;
    L_0x00d8:
        if (r21 == 0) goto L_0x00ea;
    L_0x00da:
        r26 = new com.android.server.pm.-$Lambda$JzP9CRiQ8kxViovHG-q6Wako1Xw;	 Catch:{ all -> 0x01c9 }
        r0 = r26;
        r1 = r37;
        r0.<init>(r1);	 Catch:{ all -> 0x01c9 }
        r0 = r21;
        r1 = r26;
        r0.sort(r1);	 Catch:{ all -> 0x01c9 }
    L_0x00ea:
        monitor-exit(r27);
        if (r21 == 0) goto L_0x012e;
    L_0x00ed:
        r18 = r21.size();
        r11 = 0;
    L_0x00f2:
        r0 = r18;
        if (r11 >= r0) goto L_0x012e;
    L_0x00f6:
        r0 = r21;
        r19 = r0.get(r11);
        r19 = (java.lang.String) r19;
        r0 = r37;
        r0 = r0.mService;
        r26 = r0;
        r27 = -1;
        r28 = 0;
        r29 = 2;
        r0 = r26;
        r1 = r19;
        r2 = r27;
        r3 = r28;
        r4 = r29;
        r26 = r0.deletePackageX(r1, r2, r3, r4);
        r27 = 1;
        r0 = r26;
        r1 = r27;
        if (r0 != r1) goto L_0x012b;
    L_0x0120:
        r26 = r7.getUsableSpace();
        r26 = (r26 > r38 ? 1 : (r26 == r38 ? 0 : -1));
        if (r26 < 0) goto L_0x012b;
    L_0x0128:
        r26 = 1;
        return r26;
    L_0x012b:
        r11 = r11 + 1;
        goto L_0x00f2;
    L_0x012e:
        r0 = r37;
        r0 = r0.mService;
        r26 = r0;
        r0 = r26;
        r0 = r0.mPackages;
        r28 = r0;
        monitor-enter(r28);
        r26 = com.android.server.pm.UserManagerService.getInstance();	 Catch:{ all -> 0x01c6 }
        r29 = r26.getUserIds();	 Catch:{ all -> 0x01c6 }
        r26 = 0;
        r0 = r29;
        r0 = r0.length;	 Catch:{ all -> 0x01c6 }
        r30 = r0;
        r27 = r26;
    L_0x014c:
        r0 = r27;
        r1 = r30;
        if (r0 >= r1) goto L_0x01c2;
    L_0x0152:
        r25 = r29[r27];	 Catch:{ all -> 0x01c6 }
        r26 = new com.android.server.pm.-$Lambda$5qSWip3Q3NYNf0S8FNRU2st8ZfA;	 Catch:{ all -> 0x01c6 }
        r31 = 0;
        r0 = r26;
        r1 = r31;
        r2 = r42;
        r0.<init>(r1, r2);	 Catch:{ all -> 0x01c6 }
        r0 = r37;
        r1 = r26;
        r2 = r25;
        r0.removeUninstalledInstantAppStateLPw(r1, r2);	 Catch:{ all -> 0x01c6 }
        r13 = getInstantApplicationsDir(r25);	 Catch:{ all -> 0x01c6 }
        r26 = r13.exists();	 Catch:{ all -> 0x01c6 }
        if (r26 != 0) goto L_0x0179;
    L_0x0174:
        r26 = r27 + 1;
        r27 = r26;
        goto L_0x014c;
    L_0x0179:
        r10 = r13.listFiles();	 Catch:{ all -> 0x01c6 }
        if (r10 == 0) goto L_0x0174;
    L_0x017f:
        r26 = 0;
        r0 = r10.length;	 Catch:{ all -> 0x01c6 }
        r31 = r0;
    L_0x0184:
        r0 = r26;
        r1 = r31;
        if (r0 >= r1) goto L_0x0174;
    L_0x018a:
        r14 = r10[r26];	 Catch:{ all -> 0x01c6 }
        r32 = r14.isDirectory();	 Catch:{ all -> 0x01c6 }
        if (r32 != 0) goto L_0x0195;
    L_0x0192:
        r26 = r26 + 1;
        goto L_0x0184;
    L_0x0195:
        r15 = new java.io.File;	 Catch:{ all -> 0x01c6 }
        r32 = "metadata.xml";
        r0 = r32;
        r15.<init>(r14, r0);	 Catch:{ all -> 0x01c6 }
        r32 = r15.exists();	 Catch:{ all -> 0x01c6 }
        if (r32 == 0) goto L_0x0192;
    L_0x01a5:
        r32 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x01c6 }
        r34 = r15.lastModified();	 Catch:{ all -> 0x01c6 }
        r8 = r32 - r34;
        r32 = (r8 > r42 ? 1 : (r8 == r42 ? 0 : -1));
        if (r32 <= 0) goto L_0x0192;
    L_0x01b3:
        deleteDir(r14);	 Catch:{ all -> 0x01c6 }
        r32 = r7.getUsableSpace();	 Catch:{ all -> 0x01c6 }
        r32 = (r32 > r38 ? 1 : (r32 == r38 ? 0 : -1));
        if (r32 < 0) goto L_0x0192;
    L_0x01be:
        r26 = 1;
        monitor-exit(r28);
        return r26;
    L_0x01c2:
        monitor-exit(r28);
        r26 = 0;
        return r26;
    L_0x01c6:
        r26 = move-exception;
        monitor-exit(r28);
        throw r26;
    L_0x01c9:
        r26 = move-exception;
        r20 = r21;
        goto L_0x00d6;
    L_0x01ce:
        r20 = r21;
        goto L_0x00c7;
    L_0x01d2:
        r20 = r21;
        goto L_0x0079;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.InstantAppRegistry.pruneInstantApps(long, long, long):boolean");
    }

    /* synthetic */ int lambda$-com_android_server_pm_InstantAppRegistry_29061(String lhs, String rhs) {
        Package lhsPkg = (Package) this.mService.mPackages.get(lhs);
        Package rhsPkg = (Package) this.mService.mPackages.get(rhs);
        if (lhsPkg == null && rhsPkg == null) {
            return 0;
        }
        if (lhsPkg == null) {
            return -1;
        }
        if (rhsPkg == null || lhsPkg.getLatestPackageUseTimeInMills() > rhsPkg.getLatestPackageUseTimeInMills()) {
            return 1;
        }
        if (lhsPkg.getLatestPackageUseTimeInMills() < rhsPkg.getLatestPackageUseTimeInMills()) {
            return -1;
        }
        if (!(lhsPkg.mExtras instanceof PackageSetting) || !(rhsPkg.mExtras instanceof PackageSetting)) {
            return 0;
        }
        return ((PackageSetting) lhsPkg.mExtras).firstInstallTime > ((PackageSetting) rhsPkg.mExtras).firstInstallTime ? 1 : -1;
    }

    static /* synthetic */ boolean lambda$-com_android_server_pm_InstantAppRegistry_31751(long maxUninstalledCacheDuration, UninstalledInstantAppState state) {
        return System.currentTimeMillis() - state.mTimestamp > maxUninstalledCacheDuration;
    }

    private List<InstantAppInfo> getInstalledInstantApplicationsLPr(int userId) {
        List<InstantAppInfo> result = null;
        int packageCount = this.mService.mPackages.size();
        for (int i = 0; i < packageCount; i++) {
            Package pkg = (Package) this.mService.mPackages.valueAt(i);
            PackageSetting ps = pkg.mExtras;
            if (ps != null && (ps.getInstantApp(userId) ^ 1) == 0) {
                InstantAppInfo info = createInstantAppInfoForPackage(pkg, userId, true);
                if (info != null) {
                    if (result == null) {
                        result = new ArrayList();
                    }
                    result.add(info);
                }
            }
        }
        return result;
    }

    private InstantAppInfo createInstantAppInfoForPackage(Package pkg, int userId, boolean addApplicationInfo) {
        PackageSetting ps = pkg.mExtras;
        if (ps == null || !ps.getInstalled(userId)) {
            return null;
        }
        String[] requestedPermissions = new String[pkg.requestedPermissions.size()];
        pkg.requestedPermissions.toArray(requestedPermissions);
        Set<String> permissions = ps.getPermissionsState().getPermissions(userId);
        String[] grantedPermissions = new String[permissions.size()];
        permissions.toArray(grantedPermissions);
        if (addApplicationInfo) {
            return new InstantAppInfo(pkg.applicationInfo, requestedPermissions, grantedPermissions);
        }
        return new InstantAppInfo(pkg.applicationInfo.packageName, pkg.applicationInfo.loadLabel(this.mService.mContext.getPackageManager()), requestedPermissions, grantedPermissions);
    }

    private List<InstantAppInfo> getUninstalledInstantApplicationsLPr(int userId) {
        List<UninstalledInstantAppState> uninstalledAppStates = getUninstalledInstantAppStatesLPr(userId);
        if (uninstalledAppStates == null || uninstalledAppStates.isEmpty()) {
            return null;
        }
        List<InstantAppInfo> uninstalledApps = null;
        int stateCount = uninstalledAppStates.size();
        for (int i = 0; i < stateCount; i++) {
            UninstalledInstantAppState uninstalledAppState = (UninstalledInstantAppState) uninstalledAppStates.get(i);
            if (uninstalledApps == null) {
                uninstalledApps = new ArrayList();
            }
            uninstalledApps.add(uninstalledAppState.mInstantAppInfo);
        }
        return uninstalledApps;
    }

    private void propagateInstantAppPermissionsIfNeeded(String packageName, int userId) {
        InstantAppInfo appInfo = peekOrParseUninstalledInstantAppInfo(packageName, userId);
        if (appInfo != null && !ArrayUtils.isEmpty(appInfo.getGrantedPermissions())) {
            long identity = Binder.clearCallingIdentity();
            try {
                for (String grantedPermission : appInfo.getGrantedPermissions()) {
                    BasePermission bp = (BasePermission) this.mService.mSettings.mPermissions.get(grantedPermission);
                    if (bp != null && ((bp.isRuntime() || bp.isDevelopment()) && bp.isInstant())) {
                        this.mService.grantRuntimePermission(packageName, grantedPermission, userId);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    private InstantAppInfo peekOrParseUninstalledInstantAppInfo(String packageName, int userId) {
        UninstalledInstantAppState uninstalledAppState;
        if (this.mUninstalledInstantApps != null) {
            List<UninstalledInstantAppState> uninstalledAppStates = (List) this.mUninstalledInstantApps.get(userId);
            if (uninstalledAppStates != null) {
                int appCount = uninstalledAppStates.size();
                for (int i = 0; i < appCount; i++) {
                    uninstalledAppState = (UninstalledInstantAppState) uninstalledAppStates.get(i);
                    if (uninstalledAppState.mInstantAppInfo.getPackageName().equals(packageName)) {
                        return uninstalledAppState.mInstantAppInfo;
                    }
                }
            }
        }
        uninstalledAppState = parseMetadataFile(new File(getInstantApplicationDir(packageName, userId), INSTANT_APP_METADATA_FILE));
        if (uninstalledAppState == null) {
            return null;
        }
        return uninstalledAppState.mInstantAppInfo;
    }

    private List<UninstalledInstantAppState> getUninstalledInstantAppStatesLPr(int userId) {
        List<UninstalledInstantAppState> list = null;
        if (this.mUninstalledInstantApps != null) {
            list = (List) this.mUninstalledInstantApps.get(userId);
            if (list != null) {
                return list;
            }
        }
        File instantAppsDir = getInstantApplicationsDir(userId);
        if (instantAppsDir.exists()) {
            File[] files = instantAppsDir.listFiles();
            if (files != null) {
                for (File instantDir : files) {
                    if (instantDir.isDirectory()) {
                        UninstalledInstantAppState uninstalledAppState = parseMetadataFile(new File(instantDir, INSTANT_APP_METADATA_FILE));
                        if (uninstalledAppState != null) {
                            if (list == null) {
                                list = new ArrayList();
                            }
                            list.add(uninstalledAppState);
                        }
                    }
                }
            }
        }
        if (list != null) {
            if (this.mUninstalledInstantApps == null) {
                this.mUninstalledInstantApps = new SparseArray();
            }
            this.mUninstalledInstantApps.put(userId, list);
        }
        return list;
    }

    private static UninstalledInstantAppState parseMetadataFile(File metadataFile) {
        if (!metadataFile.exists()) {
            return null;
        }
        try {
            FileInputStream in = new AtomicFile(metadataFile).openRead();
            File instantDir = metadataFile.getParentFile();
            long timestamp = metadataFile.lastModified();
            String packageName = instantDir.getName();
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(in, StandardCharsets.UTF_8.name());
                UninstalledInstantAppState uninstalledInstantAppState = new UninstalledInstantAppState(parseMetadata(parser, packageName), timestamp);
                IoUtils.closeQuietly(in);
                return uninstalledInstantAppState;
            } catch (Exception e) {
                throw new IllegalStateException("Failed parsing instant metadata file: " + metadataFile, e);
            } catch (Throwable th) {
                IoUtils.closeQuietly(in);
            }
        } catch (FileNotFoundException e2) {
            Slog.i(LOG_TAG, "No instant metadata file");
            return null;
        }
    }

    private static File computeInstantCookieFile(String packageName, String sha256Digest, int userId) {
        return new File(getInstantApplicationDir(packageName, userId), INSTANT_APP_COOKIE_FILE_PREFIX + sha256Digest + INSTANT_APP_COOKIE_FILE_SIFFIX);
    }

    private static File peekInstantCookieFile(String packageName, int userId) {
        File appDir = getInstantApplicationDir(packageName, userId);
        if (!appDir.exists()) {
            return null;
        }
        File[] files = appDir.listFiles();
        if (files == null) {
            return null;
        }
        for (File file : files) {
            if (!file.isDirectory() && file.getName().startsWith(INSTANT_APP_COOKIE_FILE_PREFIX) && file.getName().endsWith(INSTANT_APP_COOKIE_FILE_SIFFIX)) {
                return file;
            }
        }
        return null;
    }

    private static InstantAppInfo parseMetadata(XmlPullParser parser, String packageName) throws IOException, XmlPullParserException {
        int outerDepth = parser.getDepth();
        while (XmlUtils.nextElementWithin(parser, outerDepth)) {
            if (TAG_PACKAGE.equals(parser.getName())) {
                return parsePackage(parser, packageName);
            }
        }
        return null;
    }

    private static InstantAppInfo parsePackage(XmlPullParser parser, String packageName) throws IOException, XmlPullParserException {
        String label = parser.getAttributeValue(null, ATTR_LABEL);
        List<String> outRequestedPermissions = new ArrayList();
        List<String> outGrantedPermissions = new ArrayList();
        int outerDepth = parser.getDepth();
        while (XmlUtils.nextElementWithin(parser, outerDepth)) {
            if (TAG_PERMISSIONS.equals(parser.getName())) {
                parsePermissions(parser, outRequestedPermissions, outGrantedPermissions);
            }
        }
        String[] requestedPermissions = new String[outRequestedPermissions.size()];
        outRequestedPermissions.toArray(requestedPermissions);
        String[] grantedPermissions = new String[outGrantedPermissions.size()];
        outGrantedPermissions.toArray(grantedPermissions);
        return new InstantAppInfo(packageName, label, requestedPermissions, grantedPermissions);
    }

    private static void parsePermissions(XmlPullParser parser, List<String> outRequestedPermissions, List<String> outGrantedPermissions) throws IOException, XmlPullParserException {
        int outerDepth = parser.getDepth();
        while (XmlUtils.nextElementWithin(parser, outerDepth)) {
            if (TAG_PERMISSION.equals(parser.getName())) {
                String permission = XmlUtils.readStringAttribute(parser, ATTR_NAME);
                outRequestedPermissions.add(permission);
                if (XmlUtils.readBooleanAttribute(parser, ATTR_GRANTED)) {
                    outGrantedPermissions.add(permission);
                }
            }
        }
    }

    private void writeUninstalledInstantAppMetadata(InstantAppInfo instantApp, int userId) {
        File appDir = getInstantApplicationDir(instantApp.getPackageName(), userId);
        if (appDir.exists() || (appDir.mkdirs() ^ 1) == 0) {
            AtomicFile destination = new AtomicFile(new File(appDir, INSTANT_APP_METADATA_FILE));
            AutoCloseable autoCloseable = null;
            try {
                autoCloseable = destination.startWrite();
                XmlSerializer serializer = Xml.newSerializer();
                serializer.setOutput(autoCloseable, StandardCharsets.UTF_8.name());
                serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
                serializer.startDocument(null, Boolean.valueOf(true));
                serializer.startTag(null, TAG_PACKAGE);
                serializer.attribute(null, ATTR_LABEL, instantApp.loadLabel(this.mService.mContext.getPackageManager()).toString());
                serializer.startTag(null, TAG_PERMISSIONS);
                for (String permission : instantApp.getRequestedPermissions()) {
                    serializer.startTag(null, TAG_PERMISSION);
                    serializer.attribute(null, ATTR_NAME, permission);
                    if (ArrayUtils.contains(instantApp.getGrantedPermissions(), permission)) {
                        serializer.attribute(null, ATTR_GRANTED, String.valueOf(true));
                    }
                    serializer.endTag(null, TAG_PERMISSION);
                }
                serializer.endTag(null, TAG_PERMISSIONS);
                serializer.endTag(null, TAG_PACKAGE);
                serializer.endDocument();
                destination.finishWrite(autoCloseable);
            } catch (Throwable t) {
                Slog.wtf(LOG_TAG, "Failed to write instant state, restoring backup", t);
                destination.failWrite(null);
            } finally {
                IoUtils.closeQuietly(autoCloseable);
            }
        }
    }

    private static File getInstantApplicationsDir(int userId) {
        return new File(Environment.getUserSystemDirectory(userId), INSTANT_APPS_FOLDER);
    }

    private static File getInstantApplicationDir(String packageName, int userId) {
        return new File(getInstantApplicationsDir(userId), packageName);
    }

    private static void deleteDir(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                deleteDir(file);
            }
        }
        dir.delete();
    }
}
