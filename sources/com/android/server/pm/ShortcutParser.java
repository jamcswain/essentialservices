package com.android.server.pm;

import android.content.ComponentName;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ShortcutInfo;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import com.android.internal.R;
import java.io.IOException;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

public class ShortcutParser {
    private static final boolean DEBUG = false;
    static final String METADATA_KEY = "android.app.shortcuts";
    private static final String TAG = "ShortcutService";
    private static final String TAG_CATEGORIES = "categories";
    private static final String TAG_INTENT = "intent";
    private static final String TAG_SHORTCUT = "shortcut";
    private static final String TAG_SHORTCUTS = "shortcuts";

    public static List<ShortcutInfo> parseShortcuts(ShortcutService service, String packageName, int userId) throws IOException, XmlPullParserException {
        List<ResolveInfo> activities = service.injectGetMainActivities(packageName, userId);
        if (activities == null || activities.size() == 0) {
            return null;
        }
        List<ShortcutInfo> result = null;
        try {
            int size = activities.size();
            for (int i = 0; i < size; i++) {
                ActivityInfo activityInfoNoMetadata = ((ResolveInfo) activities.get(i)).activityInfo;
                if (activityInfoNoMetadata != null) {
                    ActivityInfo activityInfoWithMetadata = service.getActivityInfoWithMetadata(activityInfoNoMetadata.getComponentName(), userId);
                    if (activityInfoWithMetadata != null) {
                        result = parseShortcutsOneFile(service, activityInfoWithMetadata, packageName, userId, result);
                    }
                }
            }
            return result;
        } catch (RuntimeException e) {
            service.wtf("Exception caught while parsing shortcut XML for package=" + packageName, e);
            return null;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List<android.content.pm.ShortcutInfo> parseShortcutsOneFile(com.android.server.pm.ShortcutService r25, android.content.pm.ActivityInfo r26, java.lang.String r27, int r28, java.util.List<android.content.pm.ShortcutInfo> r29) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        /*
        r18 = 0;
        r2 = "android.app.shortcuts";
        r0 = r25;
        r1 = r26;
        r18 = r0.injectXmlMetaData(r1, r2);	 Catch:{ all -> 0x0263 }
        if (r18 != 0) goto L_0x0015;
    L_0x000f:
        if (r18 == 0) goto L_0x0014;
    L_0x0011:
        r18.close();
    L_0x0014:
        return r29;
    L_0x0015:
        r5 = new android.content.ComponentName;	 Catch:{ all -> 0x0263 }
        r0 = r26;
        r2 = r0.name;	 Catch:{ all -> 0x0263 }
        r0 = r27;
        r5.<init>(r0, r2);	 Catch:{ all -> 0x0263 }
        r3 = android.util.Xml.asAttributeSet(r18);	 Catch:{ all -> 0x0263 }
        r7 = 0;
        r15 = r25.getMaxActivityShortcuts();	 Catch:{ all -> 0x0263 }
        r17 = 0;
        r9 = 0;
        r8 = 0;
        r14 = new java.util.ArrayList;	 Catch:{ all -> 0x0263 }
        r14.<init>();	 Catch:{ all -> 0x0263 }
        r19 = r29;
    L_0x0034:
        r22 = r18.next();	 Catch:{ all -> 0x009a }
        r2 = 1;
        r0 = r22;
        if (r0 == r2) goto L_0x025d;
    L_0x003d:
        r2 = 3;
        r0 = r22;
        if (r0 != r2) goto L_0x0048;
    L_0x0042:
        r2 = r18.getDepth();	 Catch:{ all -> 0x009a }
        if (r2 <= 0) goto L_0x025d;
    L_0x0048:
        r10 = r18.getDepth();	 Catch:{ all -> 0x009a }
        r21 = r18.getName();	 Catch:{ all -> 0x009a }
        r2 = 3;
        r0 = r22;
        if (r0 != r2) goto L_0x0136;
    L_0x0055:
        r2 = 2;
        if (r10 != r2) goto L_0x0136;
    L_0x0058:
        r2 = "shortcut";
        r0 = r21;
        r2 = r2.equals(r0);	 Catch:{ all -> 0x009a }
        if (r2 == 0) goto L_0x0136;
    L_0x0063:
        if (r9 == 0) goto L_0x0034;
    L_0x0065:
        r20 = r9;
        r9 = 0;
        r2 = r20.isEnabled();	 Catch:{ all -> 0x009a }
        if (r2 == 0) goto L_0x00a3;
    L_0x006e:
        r2 = r14.size();	 Catch:{ all -> 0x009a }
        if (r2 != 0) goto L_0x00b1;
    L_0x0074:
        r2 = "ShortcutService";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009a }
        r4.<init>();	 Catch:{ all -> 0x009a }
        r6 = "Shortcut ";
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r6 = r20.getId();	 Catch:{ all -> 0x009a }
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r6 = " has no intent. Skipping it.";
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r4 = r4.toString();	 Catch:{ all -> 0x009a }
        android.util.Log.e(r2, r4);	 Catch:{ all -> 0x009a }
        goto L_0x0034;
    L_0x009a:
        r2 = move-exception;
        r29 = r19;
    L_0x009d:
        if (r18 == 0) goto L_0x00a2;
    L_0x009f:
        r18.close();
    L_0x00a2:
        throw r2;
    L_0x00a3:
        r14.clear();	 Catch:{ all -> 0x009a }
        r2 = new android.content.Intent;	 Catch:{ all -> 0x009a }
        r4 = "android.intent.action.VIEW";
        r2.<init>(r4);	 Catch:{ all -> 0x009a }
        r14.add(r2);	 Catch:{ all -> 0x009a }
    L_0x00b1:
        r0 = r17;
        if (r0 < r15) goto L_0x00eb;
    L_0x00b5:
        r2 = "ShortcutService";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009a }
        r4.<init>();	 Catch:{ all -> 0x009a }
        r6 = "More than ";
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r4 = r4.append(r15);	 Catch:{ all -> 0x009a }
        r6 = " shortcuts found for ";
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r6 = r26.getComponentName();	 Catch:{ all -> 0x009a }
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r6 = ". Skipping the rest.";
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r4 = r4.toString();	 Catch:{ all -> 0x009a }
        android.util.Log.e(r2, r4);	 Catch:{ all -> 0x009a }
        if (r18 == 0) goto L_0x00ea;
    L_0x00e7:
        r18.close();
    L_0x00ea:
        return r19;
    L_0x00eb:
        r2 = 0;
        r2 = r14.get(r2);	 Catch:{ all -> 0x009a }
        r2 = (android.content.Intent) r2;	 Catch:{ all -> 0x009a }
        r4 = 268484608; // 0x1000c000 float:2.539146E-29 double:1.326490212E-315;
        r2.addFlags(r4);	 Catch:{ all -> 0x009a }
        r2 = r14.size();	 Catch:{ RuntimeException -> 0x012a }
        r2 = new android.content.Intent[r2];	 Catch:{ RuntimeException -> 0x012a }
        r2 = r14.toArray(r2);	 Catch:{ RuntimeException -> 0x012a }
        r2 = (android.content.Intent[]) r2;	 Catch:{ RuntimeException -> 0x012a }
        r0 = r20;
        r0.setIntents(r2);	 Catch:{ RuntimeException -> 0x012a }
        r14.clear();	 Catch:{ all -> 0x009a }
        if (r8 == 0) goto L_0x0114;
    L_0x010e:
        r0 = r20;
        r0.setCategories(r8);	 Catch:{ all -> 0x009a }
        r8 = 0;
    L_0x0114:
        if (r19 != 0) goto L_0x0266;
    L_0x0116:
        r29 = new java.util.ArrayList;	 Catch:{ all -> 0x009a }
        r29.<init>();	 Catch:{ all -> 0x009a }
    L_0x011b:
        r0 = r29;
        r1 = r20;
        r0.add(r1);	 Catch:{ all -> 0x0263 }
        r17 = r17 + 1;
        r7 = r7 + 1;
        r19 = r29;
        goto L_0x0034;
    L_0x012a:
        r11 = move-exception;
        r2 = "ShortcutService";
        r4 = "Shortcut's extras contain un-persistable values. Skipping it.";
        android.util.Log.e(r2, r4);	 Catch:{ all -> 0x009a }
        goto L_0x0034;
    L_0x0136:
        r2 = 2;
        r0 = r22;
        if (r0 != r2) goto L_0x0034;
    L_0x013b:
        r2 = 1;
        if (r10 != r2) goto L_0x0149;
    L_0x013e:
        r2 = "shortcuts";
        r0 = r21;
        r2 = r2.equals(r0);	 Catch:{ all -> 0x009a }
        if (r2 != 0) goto L_0x0034;
    L_0x0149:
        r2 = 2;
        if (r10 != r2) goto L_0x0196;
    L_0x014c:
        r2 = "shortcut";
        r0 = r21;
        r2 = r2.equals(r0);	 Catch:{ all -> 0x009a }
        if (r2 == 0) goto L_0x0196;
    L_0x0157:
        r2 = r25;
        r4 = r27;
        r6 = r28;
        r20 = parseShortcutAttributes(r2, r3, r4, r5, r6, r7);	 Catch:{ all -> 0x009a }
        if (r20 == 0) goto L_0x0034;
    L_0x0163:
        if (r19 == 0) goto L_0x0191;
    L_0x0165:
        r2 = r19.size();	 Catch:{ all -> 0x009a }
        r12 = r2 + -1;
    L_0x016b:
        if (r12 < 0) goto L_0x0191;
    L_0x016d:
        r4 = r20.getId();	 Catch:{ all -> 0x009a }
        r0 = r19;
        r2 = r0.get(r12);	 Catch:{ all -> 0x009a }
        r2 = (android.content.pm.ShortcutInfo) r2;	 Catch:{ all -> 0x009a }
        r2 = r2.getId();	 Catch:{ all -> 0x009a }
        r2 = r4.equals(r2);	 Catch:{ all -> 0x009a }
        if (r2 == 0) goto L_0x018e;
    L_0x0183:
        r2 = "ShortcutService";
        r4 = "Duplicate shortcut ID detected. Skipping it.";
        android.util.Log.e(r2, r4);	 Catch:{ all -> 0x009a }
        goto L_0x0034;
    L_0x018e:
        r12 = r12 + -1;
        goto L_0x016b;
    L_0x0191:
        r9 = r20;
        r8 = 0;
        goto L_0x0034;
    L_0x0196:
        r2 = 3;
        if (r10 != r2) goto L_0x01f3;
    L_0x0199:
        r2 = "intent";
        r0 = r21;
        r2 = r2.equals(r0);	 Catch:{ all -> 0x009a }
        if (r2 == 0) goto L_0x01f3;
    L_0x01a4:
        if (r9 == 0) goto L_0x01ae;
    L_0x01a6:
        r2 = r9.isEnabled();	 Catch:{ all -> 0x009a }
        r2 = r2 ^ 1;
        if (r2 == 0) goto L_0x01b9;
    L_0x01ae:
        r2 = "ShortcutService";
        r4 = "Ignoring excessive intent tag.";
        android.util.Log.e(r2, r4);	 Catch:{ all -> 0x009a }
        goto L_0x0034;
    L_0x01b9:
        r0 = r25;
        r2 = r0.mContext;	 Catch:{ all -> 0x009a }
        r2 = r2.getResources();	 Catch:{ all -> 0x009a }
        r0 = r18;
        r13 = android.content.Intent.parseIntent(r2, r0, r3);	 Catch:{ all -> 0x009a }
        r2 = r13.getAction();	 Catch:{ all -> 0x009a }
        r2 = android.text.TextUtils.isEmpty(r2);	 Catch:{ all -> 0x009a }
        if (r2 == 0) goto L_0x01ee;
    L_0x01d1:
        r2 = "ShortcutService";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009a }
        r4.<init>();	 Catch:{ all -> 0x009a }
        r6 = "Shortcut intent action must be provided. activity=";
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r4 = r4.append(r5);	 Catch:{ all -> 0x009a }
        r4 = r4.toString();	 Catch:{ all -> 0x009a }
        android.util.Log.e(r2, r4);	 Catch:{ all -> 0x009a }
        r9 = 0;
        goto L_0x0034;
    L_0x01ee:
        r14.add(r13);	 Catch:{ all -> 0x009a }
        goto L_0x0034;
    L_0x01f3:
        r2 = 3;
        if (r10 != r2) goto L_0x023f;
    L_0x01f6:
        r2 = "categories";
        r0 = r21;
        r2 = r2.equals(r0);	 Catch:{ all -> 0x009a }
        if (r2 == 0) goto L_0x023f;
    L_0x0201:
        if (r9 == 0) goto L_0x0034;
    L_0x0203:
        r2 = r9.getCategories();	 Catch:{ all -> 0x009a }
        if (r2 != 0) goto L_0x0034;
    L_0x0209:
        r0 = r25;
        r16 = parseCategories(r0, r3);	 Catch:{ all -> 0x009a }
        r2 = android.text.TextUtils.isEmpty(r16);	 Catch:{ all -> 0x009a }
        if (r2 == 0) goto L_0x0231;
    L_0x0215:
        r2 = "ShortcutService";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009a }
        r4.<init>();	 Catch:{ all -> 0x009a }
        r6 = "Empty category found. activity=";
        r4 = r4.append(r6);	 Catch:{ all -> 0x009a }
        r4 = r4.append(r5);	 Catch:{ all -> 0x009a }
        r4 = r4.toString();	 Catch:{ all -> 0x009a }
        android.util.Log.e(r2, r4);	 Catch:{ all -> 0x009a }
        goto L_0x0034;
    L_0x0231:
        if (r8 != 0) goto L_0x0238;
    L_0x0233:
        r8 = new android.util.ArraySet;	 Catch:{ all -> 0x009a }
        r8.<init>();	 Catch:{ all -> 0x009a }
    L_0x0238:
        r0 = r16;
        r8.add(r0);	 Catch:{ all -> 0x009a }
        goto L_0x0034;
    L_0x023f:
        r2 = "ShortcutService";
        r4 = "Invalid tag '%s' found at depth %d";
        r6 = 2;
        r6 = new java.lang.Object[r6];	 Catch:{ all -> 0x009a }
        r23 = 0;
        r6[r23] = r21;	 Catch:{ all -> 0x009a }
        r23 = java.lang.Integer.valueOf(r10);	 Catch:{ all -> 0x009a }
        r24 = 1;
        r6[r24] = r23;	 Catch:{ all -> 0x009a }
        r4 = java.lang.String.format(r4, r6);	 Catch:{ all -> 0x009a }
        android.util.Log.w(r2, r4);	 Catch:{ all -> 0x009a }
        goto L_0x0034;
    L_0x025d:
        if (r18 == 0) goto L_0x0262;
    L_0x025f:
        r18.close();
    L_0x0262:
        return r19;
    L_0x0263:
        r2 = move-exception;
        goto L_0x009d;
    L_0x0266:
        r29 = r19;
        goto L_0x011b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.ShortcutParser.parseShortcutsOneFile(com.android.server.pm.ShortcutService, android.content.pm.ActivityInfo, java.lang.String, int, java.util.List):java.util.List<android.content.pm.ShortcutInfo>");
    }

    private static String parseCategories(ShortcutService service, AttributeSet attrs) {
        TypedArray sa = service.mContext.getResources().obtainAttributes(attrs, R.styleable.ShortcutCategories);
        try {
            if (sa.getType(0) == 3) {
                String nonResourceString = sa.getNonResourceString(0);
                return nonResourceString;
            }
            Log.w(TAG, "android:name for shortcut category must be string literal.");
            sa.recycle();
            return null;
        } finally {
            sa.recycle();
        }
    }

    private static ShortcutInfo parseShortcutAttributes(ShortcutService service, AttributeSet attrs, String packageName, ComponentName activity, int userId, int rank) {
        TypedArray sa = service.mContext.getResources().obtainAttributes(attrs, R.styleable.Shortcut);
        try {
            if (sa.getType(2) != 3) {
                Log.w(TAG, "android:shortcutId must be string literal. activity=" + activity);
                return null;
            }
            String id = sa.getNonResourceString(2);
            boolean enabled = sa.getBoolean(1, true);
            int iconResId = sa.getResourceId(0, 0);
            int titleResId = sa.getResourceId(3, 0);
            int textResId = sa.getResourceId(4, 0);
            int disabledMessageResId = sa.getResourceId(5, 0);
            if (TextUtils.isEmpty(id)) {
                Log.w(TAG, "android:shortcutId must be provided. activity=" + activity);
                sa.recycle();
                return null;
            } else if (titleResId == 0) {
                Log.w(TAG, "android:shortcutShortLabel must be provided. activity=" + activity);
                sa.recycle();
                return null;
            } else {
                ShortcutInfo createShortcutFromManifest = createShortcutFromManifest(service, userId, id, packageName, activity, titleResId, textResId, disabledMessageResId, rank, iconResId, enabled);
                sa.recycle();
                return createShortcutFromManifest;
            }
        } finally {
            sa.recycle();
        }
    }

    private static ShortcutInfo createShortcutFromManifest(ShortcutService service, int userId, String id, String packageName, ComponentName activityComponent, int titleResId, int textResId, int disabledMessageResId, int rank, int iconResId, boolean enabled) {
        return new ShortcutInfo(userId, id, packageName, activityComponent, null, null, titleResId, null, null, textResId, null, null, disabledMessageResId, null, null, null, rank, null, service.injectCurrentTimeMillis(), ((enabled ? 32 : 64) | 256) | (iconResId != 0 ? 4 : 0), iconResId, null, null);
    }
}
