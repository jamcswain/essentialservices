package com.android.server.pm;

import android.content.Context;
import android.util.SparseArray;
import com.android.internal.annotations.GuardedBy;

public class ProtectedPackages {
    private final Context mContext;
    @GuardedBy("this")
    private String mDeviceOwnerPackage;
    @GuardedBy("this")
    private int mDeviceOwnerUserId;
    @GuardedBy("this")
    private final String mDeviceProvisioningPackage = this.mContext.getResources().getString(17039647);
    @GuardedBy("this")
    private SparseArray<String> mProfileOwnerPackages;

    public ProtectedPackages(Context context) {
        this.mContext = context;
    }

    public synchronized void setDeviceAndProfileOwnerPackages(int deviceOwnerUserId, String deviceOwnerPackage, SparseArray<String> profileOwnerPackages) {
        SparseArray sparseArray = null;
        synchronized (this) {
            this.mDeviceOwnerUserId = deviceOwnerUserId;
            if (deviceOwnerUserId == -10000) {
                deviceOwnerPackage = null;
            }
            this.mDeviceOwnerPackage = deviceOwnerPackage;
            if (profileOwnerPackages != null) {
                sparseArray = profileOwnerPackages.clone();
            }
            this.mProfileOwnerPackages = sparseArray;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean hasDeviceOwnerOrProfileOwner(int r4, java.lang.String r5) {
        /*
        r3 = this;
        r2 = 1;
        r1 = 0;
        monitor-enter(r3);
        if (r5 != 0) goto L_0x0007;
    L_0x0005:
        monitor-exit(r3);
        return r1;
    L_0x0007:
        r0 = r3.mDeviceOwnerPackage;	 Catch:{ all -> 0x002d }
        if (r0 == 0) goto L_0x0019;
    L_0x000b:
        r0 = r3.mDeviceOwnerUserId;	 Catch:{ all -> 0x002d }
        if (r0 != r4) goto L_0x0019;
    L_0x000f:
        r0 = r3.mDeviceOwnerPackage;	 Catch:{ all -> 0x002d }
        r0 = r5.equals(r0);	 Catch:{ all -> 0x002d }
        if (r0 == 0) goto L_0x0019;
    L_0x0017:
        monitor-exit(r3);
        return r2;
    L_0x0019:
        r0 = r3.mProfileOwnerPackages;	 Catch:{ all -> 0x002d }
        if (r0 == 0) goto L_0x002b;
    L_0x001d:
        r0 = r3.mProfileOwnerPackages;	 Catch:{ all -> 0x002d }
        r0 = r0.get(r4);	 Catch:{ all -> 0x002d }
        r0 = r5.equals(r0);	 Catch:{ all -> 0x002d }
        if (r0 == 0) goto L_0x002b;
    L_0x0029:
        monitor-exit(r3);
        return r2;
    L_0x002b:
        monitor-exit(r3);
        return r1;
    L_0x002d:
        r0 = move-exception;
        monitor-exit(r3);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.pm.ProtectedPackages.hasDeviceOwnerOrProfileOwner(int, java.lang.String):boolean");
    }

    private synchronized boolean isProtectedPackage(String packageName) {
        return packageName != null ? packageName.equals(this.mDeviceProvisioningPackage) : false;
    }

    public boolean isPackageStateProtected(int userId, String packageName) {
        if (hasDeviceOwnerOrProfileOwner(userId, packageName)) {
            return true;
        }
        return isProtectedPackage(packageName);
    }

    public boolean isPackageDataProtected(int userId, String packageName) {
        if (hasDeviceOwnerOrProfileOwner(userId, packageName)) {
            return true;
        }
        return isProtectedPackage(packageName);
    }
}
