package com.android.server.content;

import android.accounts.Account;
import android.accounts.AccountAndUser;
import android.accounts.AccountManager;
import android.app.backup.BackupManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ISyncStatusObserver;
import android.content.PeriodicSync;
import android.content.SyncInfo;
import android.content.SyncRequest.Builder;
import android.content.SyncStatusInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteCallbackList;
import android.util.ArrayMap;
import android.util.AtomicFile;
import android.util.EventLog;
import android.util.Log;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import android.util.Xml;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.FastXmlSerializer;
import com.android.server.audio.AudioService;
import com.android.server.usage.UnixCalendar;
import com.android.server.voiceinteraction.DatabaseHelper.SoundModelContract;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class SyncStorageEngine extends Handler {
    private static final int ACCOUNTS_VERSION = 3;
    private static final double DEFAULT_FLEX_PERCENT_SYNC = 0.04d;
    private static final long DEFAULT_MIN_FLEX_ALLOWED_SECS = 5;
    private static final long DEFAULT_POLL_FREQUENCY_SECONDS = 86400;
    public static final int EVENT_START = 0;
    public static final int EVENT_STOP = 1;
    public static final int MAX_HISTORY = 100;
    public static final String MESG_CANCELED = "canceled";
    public static final String MESG_SUCCESS = "success";
    static final long MILLIS_IN_4WEEKS = 2419200000L;
    private static final int MSG_WRITE_STATISTICS = 2;
    private static final int MSG_WRITE_STATUS = 1;
    public static final long NOT_IN_BACKOFF_MODE = -1;
    public static final String[] SOURCES = new String[]{"SERVER", "LOCAL", "POLL", "USER", "PERIODIC", "SERVICE"};
    public static final int SOURCE_LOCAL = 1;
    public static final int SOURCE_PERIODIC = 4;
    public static final int SOURCE_POLL = 2;
    public static final int SOURCE_SERVER = 0;
    public static final int SOURCE_USER = 3;
    public static final int STATISTICS_FILE_END = 0;
    public static final int STATISTICS_FILE_ITEM = 101;
    public static final int STATISTICS_FILE_ITEM_OLD = 100;
    public static final int STATUS_FILE_END = 0;
    public static final int STATUS_FILE_ITEM = 100;
    private static final boolean SYNC_ENABLED_DEFAULT = false;
    private static final String TAG = "SyncManager";
    private static final String TAG_FILE = "SyncManagerFile";
    private static final long WRITE_STATISTICS_DELAY = 1800000;
    private static final long WRITE_STATUS_DELAY = 600000;
    private static final String XML_ATTR_ENABLED = "enabled";
    private static final String XML_ATTR_LISTEN_FOR_TICKLES = "listen-for-tickles";
    private static final String XML_ATTR_NEXT_AUTHORITY_ID = "nextAuthorityId";
    private static final String XML_ATTR_SYNC_RANDOM_OFFSET = "offsetInSeconds";
    private static final String XML_ATTR_USER = "user";
    private static final String XML_TAG_LISTEN_FOR_TICKLES = "listenForTickles";
    private static PeriodicSyncAddedListener mPeriodicSyncAddedListener;
    private static HashMap<String, String> sAuthorityRenames = new HashMap();
    private static volatile SyncStorageEngine sSyncStorageEngine = null;
    private final AtomicFile mAccountInfoFile;
    private final HashMap<AccountAndUser, AccountInfo> mAccounts = new HashMap();
    private final SparseArray<AuthorityInfo> mAuthorities = new SparseArray();
    private OnAuthorityRemovedListener mAuthorityRemovedListener;
    private final Calendar mCal;
    private final RemoteCallbackList<ISyncStatusObserver> mChangeListeners = new RemoteCallbackList();
    private final Context mContext;
    private final SparseArray<ArrayList<SyncInfo>> mCurrentSyncs = new SparseArray();
    private final DayStats[] mDayStats = new DayStats[28];
    private boolean mDefaultMasterSyncAutomatically;
    private boolean mGrantSyncAdaptersAccountAccess;
    private SparseArray<Boolean> mMasterSyncAutomatically = new SparseArray();
    private int mNextAuthorityId = 0;
    private int mNextHistoryId = 0;
    private final ArrayMap<ComponentName, SparseArray<AuthorityInfo>> mServices = new ArrayMap();
    private final AtomicFile mStatisticsFile;
    private final AtomicFile mStatusFile;
    private final ArrayList<SyncHistoryItem> mSyncHistory = new ArrayList();
    private int mSyncRandomOffset;
    private OnSyncRequestListener mSyncRequestListener;
    private final SparseArray<SyncStatusInfo> mSyncStatus = new SparseArray();
    private int mYear;
    private int mYearInDays;

    interface OnAuthorityRemovedListener {
        void onAuthorityRemoved(EndPoint endPoint);
    }

    interface OnSyncRequestListener {
        void onSyncRequest(EndPoint endPoint, int i, Bundle bundle);
    }

    interface PeriodicSyncAddedListener {
        void onPeriodicSyncAdded(EndPoint endPoint, Bundle bundle, long j, long j2);
    }

    private static class AccountAuthorityValidator {
        private final AccountManager mAccountManager;
        private final SparseArray<Account[]> mAccountsCache = new SparseArray();
        private final PackageManager mPackageManager;
        private final SparseArray<ArrayMap<String, Boolean>> mProvidersPerUserCache = new SparseArray();

        AccountAuthorityValidator(Context context) {
            this.mAccountManager = (AccountManager) context.getSystemService(AccountManager.class);
            this.mPackageManager = context.getPackageManager();
        }

        boolean isAccountValid(Account account, int userId) {
            Account[] accountsForUser = (Account[]) this.mAccountsCache.get(userId);
            if (accountsForUser == null) {
                accountsForUser = this.mAccountManager.getAccountsAsUser(userId);
                this.mAccountsCache.put(userId, accountsForUser);
            }
            return ArrayUtils.contains(accountsForUser, account);
        }

        boolean isAuthorityValid(String authority, int userId) {
            ArrayMap<String, Boolean> authorityMap = (ArrayMap) this.mProvidersPerUserCache.get(userId);
            if (authorityMap == null) {
                authorityMap = new ArrayMap();
                this.mProvidersPerUserCache.put(userId, authorityMap);
            }
            if (!authorityMap.containsKey(authority)) {
                authorityMap.put(authority, Boolean.valueOf(this.mPackageManager.resolveContentProviderAsUser(authority, 786432, userId) != null));
            }
            return ((Boolean) authorityMap.get(authority)).booleanValue();
        }
    }

    static class AccountInfo {
        final AccountAndUser accountAndUser;
        final HashMap<String, AuthorityInfo> authorities = new HashMap();

        AccountInfo(AccountAndUser accountAndUser) {
            this.accountAndUser = accountAndUser;
        }
    }

    public static class AuthorityInfo {
        public static final int NOT_INITIALIZED = -1;
        public static final int NOT_SYNCABLE = 0;
        public static final int SYNCABLE = 1;
        public static final int SYNCABLE_NOT_INITIALIZED = 2;
        public static final int SYNCABLE_NO_ACCOUNT_ACCESS = 3;
        public static final int UNDEFINED = -2;
        long backoffDelay;
        long backoffTime;
        long delayUntil;
        boolean enabled;
        final int ident;
        final ArrayList<PeriodicSync> periodicSyncs;
        int syncable;
        final EndPoint target;

        AuthorityInfo(AuthorityInfo toCopy) {
            this.target = toCopy.target;
            this.ident = toCopy.ident;
            this.enabled = toCopy.enabled;
            this.syncable = toCopy.syncable;
            this.backoffTime = toCopy.backoffTime;
            this.backoffDelay = toCopy.backoffDelay;
            this.delayUntil = toCopy.delayUntil;
            this.periodicSyncs = new ArrayList();
            for (PeriodicSync sync : toCopy.periodicSyncs) {
                this.periodicSyncs.add(new PeriodicSync(sync));
            }
        }

        AuthorityInfo(EndPoint info, int id) {
            this.target = info;
            this.ident = id;
            this.enabled = false;
            this.periodicSyncs = new ArrayList();
            defaultInitialisation();
        }

        private void defaultInitialisation() {
            this.syncable = -1;
            this.backoffTime = -1;
            this.backoffDelay = -1;
            if (SyncStorageEngine.mPeriodicSyncAddedListener != null) {
                SyncStorageEngine.mPeriodicSyncAddedListener.onPeriodicSyncAdded(this.target, new Bundle(), SyncStorageEngine.DEFAULT_POLL_FREQUENCY_SECONDS, SyncStorageEngine.calculateDefaultFlexTime(SyncStorageEngine.DEFAULT_POLL_FREQUENCY_SECONDS));
            }
        }

        public String toString() {
            return this.target + ", enabled=" + this.enabled + ", syncable=" + this.syncable + ", backoff=" + this.backoffTime + ", delay=" + this.delayUntil;
        }
    }

    public static class DayStats {
        public final int day;
        public int failureCount;
        public long failureTime;
        public int successCount;
        public long successTime;

        public DayStats(int day) {
            this.day = day;
        }
    }

    public static class EndPoint {
        public static final EndPoint USER_ALL_PROVIDER_ALL_ACCOUNTS_ALL = new EndPoint(null, null, -1);
        final Account account;
        final String provider;
        final int userId;

        public EndPoint(Account account, String provider, int userId) {
            this.account = account;
            this.provider = provider;
            this.userId = userId;
        }

        public boolean matchesSpec(EndPoint spec) {
            if (this.userId != spec.userId && this.userId != -1 && spec.userId != -1) {
                return false;
            }
            boolean z;
            boolean z2;
            if (spec.account == null) {
                z = true;
            } else {
                z = this.account.equals(spec.account);
            }
            if (spec.provider == null) {
                z2 = true;
            } else {
                z2 = this.provider.equals(spec.provider);
            }
            if (!z) {
                z2 = false;
            }
            return z2;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.account == null ? "ALL ACCS" : this.account.name).append("/").append(this.provider == null ? "ALL PDRS" : this.provider);
            sb.append(":u").append(this.userId);
            return sb.toString();
        }
    }

    public static class SyncHistoryItem {
        int authorityId;
        long downstreamActivity;
        long elapsedTime;
        int event;
        long eventTime;
        Bundle extras;
        int historyId;
        boolean initialization;
        String mesg;
        int reason;
        int source;
        long upstreamActivity;
    }

    static {
        sAuthorityRenames.put("contacts", "com.android.contacts");
        sAuthorityRenames.put("calendar", "com.android.calendar");
    }

    private SyncStorageEngine(Context context, File dataDir) {
        this.mContext = context;
        sSyncStorageEngine = this;
        this.mCal = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"));
        this.mDefaultMasterSyncAutomatically = this.mContext.getResources().getBoolean(17957037);
        File syncDir = new File(new File(dataDir, "system"), "sync");
        syncDir.mkdirs();
        maybeDeleteLegacyPendingInfoLocked(syncDir);
        this.mAccountInfoFile = new AtomicFile(new File(syncDir, "accounts.xml"));
        this.mStatusFile = new AtomicFile(new File(syncDir, "status.bin"));
        this.mStatisticsFile = new AtomicFile(new File(syncDir, "stats.bin"));
        readAccountInfoLocked();
        readStatusLocked();
        readStatisticsLocked();
        readAndDeleteLegacyAccountInfoLocked();
        writeAccountInfoLocked();
        writeStatusLocked();
        writeStatisticsLocked();
    }

    public static SyncStorageEngine newTestInstance(Context context) {
        return new SyncStorageEngine(context, context.getFilesDir());
    }

    public static void init(Context context) {
        if (sSyncStorageEngine == null) {
            sSyncStorageEngine = new SyncStorageEngine(context, Environment.getDataDirectory());
        }
    }

    public static SyncStorageEngine getSingleton() {
        if (sSyncStorageEngine != null) {
            return sSyncStorageEngine;
        }
        throw new IllegalStateException("not initialized");
    }

    protected void setOnSyncRequestListener(OnSyncRequestListener listener) {
        if (this.mSyncRequestListener == null) {
            this.mSyncRequestListener = listener;
        }
    }

    protected void setOnAuthorityRemovedListener(OnAuthorityRemovedListener listener) {
        if (this.mAuthorityRemovedListener == null) {
            this.mAuthorityRemovedListener = listener;
        }
    }

    protected void setPeriodicSyncAddedListener(PeriodicSyncAddedListener listener) {
        if (mPeriodicSyncAddedListener == null) {
            mPeriodicSyncAddedListener = listener;
        }
    }

    public void handleMessage(Message msg) {
        SparseArray sparseArray;
        if (msg.what == 1) {
            sparseArray = this.mAuthorities;
            synchronized (sparseArray) {
                writeStatusLocked();
            }
        } else if (msg.what == 2) {
            sparseArray = this.mAuthorities;
            synchronized (sparseArray) {
                writeStatisticsLocked();
            }
        } else {
            return;
        }
    }

    public int getSyncRandomOffset() {
        return this.mSyncRandomOffset;
    }

    public void addStatusChangeListener(int mask, ISyncStatusObserver callback) {
        synchronized (this.mAuthorities) {
            this.mChangeListeners.register(callback, Integer.valueOf(mask));
        }
    }

    public void removeStatusChangeListener(ISyncStatusObserver callback) {
        synchronized (this.mAuthorities) {
            this.mChangeListeners.unregister(callback);
        }
    }

    public static long calculateDefaultFlexTime(long syncTimeSeconds) {
        if (syncTimeSeconds < DEFAULT_MIN_FLEX_ALLOWED_SECS) {
            return 0;
        }
        if (syncTimeSeconds < DEFAULT_POLL_FREQUENCY_SECONDS) {
            return (long) (((double) syncTimeSeconds) * DEFAULT_FLEX_PERCENT_SYNC);
        }
        return 3456;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void reportChange(int r9) {
        /*
        r8 = this;
        r3 = 0;
        r6 = r8.mAuthorities;
        monitor-enter(r6);
        r5 = r8.mChangeListeners;	 Catch:{ all -> 0x007d }
        r1 = r5.beginBroadcast();	 Catch:{ all -> 0x007d }
        r4 = r3;
    L_0x000b:
        if (r1 <= 0) goto L_0x0032;
    L_0x000d:
        r1 = r1 + -1;
        r5 = r8.mChangeListeners;	 Catch:{ all -> 0x0081 }
        r2 = r5.getBroadcastCookie(r1);	 Catch:{ all -> 0x0081 }
        r2 = (java.lang.Integer) r2;	 Catch:{ all -> 0x0081 }
        r5 = r2.intValue();	 Catch:{ all -> 0x0081 }
        r5 = r5 & r9;
        if (r5 == 0) goto L_0x000b;
    L_0x001e:
        if (r4 != 0) goto L_0x0084;
    L_0x0020:
        r3 = new java.util.ArrayList;	 Catch:{ all -> 0x0081 }
        r3.<init>(r1);	 Catch:{ all -> 0x0081 }
    L_0x0025:
        r5 = r8.mChangeListeners;	 Catch:{ all -> 0x007d }
        r5 = r5.getBroadcastItem(r1);	 Catch:{ all -> 0x007d }
        r5 = (android.content.ISyncStatusObserver) r5;	 Catch:{ all -> 0x007d }
        r3.add(r5);	 Catch:{ all -> 0x007d }
        r4 = r3;
        goto L_0x000b;
    L_0x0032:
        r5 = r8.mChangeListeners;	 Catch:{ all -> 0x0081 }
        r5.finishBroadcast();	 Catch:{ all -> 0x0081 }
        monitor-exit(r6);
        r5 = "SyncManager";
        r6 = 2;
        r5 = android.util.Log.isLoggable(r5, r6);
        if (r5 == 0) goto L_0x0067;
    L_0x0042:
        r5 = "SyncManager";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "reportChange ";
        r6 = r6.append(r7);
        r6 = r6.append(r9);
        r7 = " to: ";
        r6 = r6.append(r7);
        r6 = r6.append(r4);
        r6 = r6.toString();
        android.util.Slog.v(r5, r6);
    L_0x0067:
        if (r4 == 0) goto L_0x0080;
    L_0x0069:
        r1 = r4.size();
    L_0x006d:
        if (r1 <= 0) goto L_0x0080;
    L_0x006f:
        r1 = r1 + -1;
        r5 = r4.get(r1);	 Catch:{ RemoteException -> 0x007b }
        r5 = (android.content.ISyncStatusObserver) r5;	 Catch:{ RemoteException -> 0x007b }
        r5.onStatusChanged(r9);	 Catch:{ RemoteException -> 0x007b }
        goto L_0x006d;
    L_0x007b:
        r0 = move-exception;
        goto L_0x006d;
    L_0x007d:
        r5 = move-exception;
    L_0x007e:
        monitor-exit(r6);
        throw r5;
    L_0x0080:
        return;
    L_0x0081:
        r5 = move-exception;
        r3 = r4;
        goto L_0x007e;
    L_0x0084:
        r3 = r4;
        goto L_0x0025;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.reportChange(int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getSyncAutomatically(android.accounts.Account r8, int r9, java.lang.String r10) {
        /*
        r7 = this;
        r3 = 0;
        r4 = r7.mAuthorities;
        monitor-enter(r4);
        if (r8 == 0) goto L_0x0018;
    L_0x0006:
        r5 = new com.android.server.content.SyncStorageEngine$EndPoint;	 Catch:{ all -> 0x0040 }
        r5.<init>(r8, r10, r9);	 Catch:{ all -> 0x0040 }
        r6 = "getSyncAutomatically";
        r0 = r7.getAuthorityLocked(r5, r6);	 Catch:{ all -> 0x0040 }
        if (r0 == 0) goto L_0x0016;
    L_0x0014:
        r3 = r0.enabled;	 Catch:{ all -> 0x0040 }
    L_0x0016:
        monitor-exit(r4);
        return r3;
    L_0x0018:
        r5 = r7.mAuthorities;	 Catch:{ all -> 0x0040 }
        r2 = r5.size();	 Catch:{ all -> 0x0040 }
    L_0x001e:
        if (r2 <= 0) goto L_0x003e;
    L_0x0020:
        r2 = r2 + -1;
        r5 = r7.mAuthorities;	 Catch:{ all -> 0x0040 }
        r1 = r5.valueAt(r2);	 Catch:{ all -> 0x0040 }
        r1 = (com.android.server.content.SyncStorageEngine.AuthorityInfo) r1;	 Catch:{ all -> 0x0040 }
        r5 = r1.target;	 Catch:{ all -> 0x0040 }
        r6 = new com.android.server.content.SyncStorageEngine$EndPoint;	 Catch:{ all -> 0x0040 }
        r6.<init>(r8, r10, r9);	 Catch:{ all -> 0x0040 }
        r5 = r5.matchesSpec(r6);	 Catch:{ all -> 0x0040 }
        if (r5 == 0) goto L_0x001e;
    L_0x0037:
        r5 = r1.enabled;	 Catch:{ all -> 0x0040 }
        if (r5 == 0) goto L_0x001e;
    L_0x003b:
        r3 = 1;
        monitor-exit(r4);
        return r3;
    L_0x003e:
        monitor-exit(r4);
        return r3;
    L_0x0040:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.getSyncAutomatically(android.accounts.Account, int, java.lang.String):boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setSyncAutomatically(android.accounts.Account r8, int r9, java.lang.String r10, boolean r11) {
        /*
        r7 = this;
        r4 = 2;
        r0 = "SyncManager";
        r0 = android.util.Log.isLoggable(r0, r4);
        if (r0 == 0) goto L_0x003a;
    L_0x000a:
        r0 = "SyncManager";
        r1 = new java.lang.StringBuilder;
        r1.<init>();
        r2 = "setSyncAutomatically:  provider ";
        r1 = r1.append(r2);
        r1 = r1.append(r10);
        r2 = ", user ";
        r1 = r1.append(r2);
        r1 = r1.append(r9);
        r2 = " -> ";
        r1 = r1.append(r2);
        r1 = r1.append(r11);
        r1 = r1.toString();
        android.util.Slog.d(r0, r1);
    L_0x003a:
        r1 = r7.mAuthorities;
        monitor-enter(r1);
        r0 = new com.android.server.content.SyncStorageEngine$EndPoint;	 Catch:{ all -> 0x009f }
        r0.<init>(r8, r10, r9);	 Catch:{ all -> 0x009f }
        r2 = -1;
        r3 = 0;
        r6 = r7.getOrCreateAuthorityLocked(r0, r2, r3);	 Catch:{ all -> 0x009f }
        r0 = r6.enabled;	 Catch:{ all -> 0x009f }
        if (r0 != r11) goto L_0x0079;
    L_0x004c:
        r0 = "SyncManager";
        r2 = 2;
        r0 = android.util.Log.isLoggable(r0, r2);	 Catch:{ all -> 0x009f }
        if (r0 == 0) goto L_0x0077;
    L_0x0056:
        r0 = "SyncManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009f }
        r2.<init>();	 Catch:{ all -> 0x009f }
        r3 = "setSyncAutomatically: already set to ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x009f }
        r2 = r2.append(r11);	 Catch:{ all -> 0x009f }
        r3 = ", doing nothing";
        r2 = r2.append(r3);	 Catch:{ all -> 0x009f }
        r2 = r2.toString();	 Catch:{ all -> 0x009f }
        android.util.Slog.d(r0, r2);	 Catch:{ all -> 0x009f }
    L_0x0077:
        monitor-exit(r1);
        return;
    L_0x0079:
        if (r11 == 0) goto L_0x0082;
    L_0x007b:
        r0 = r6.syncable;	 Catch:{ all -> 0x009f }
        if (r0 != r4) goto L_0x0082;
    L_0x007f:
        r0 = -1;
        r6.syncable = r0;	 Catch:{ all -> 0x009f }
    L_0x0082:
        r6.enabled = r11;	 Catch:{ all -> 0x009f }
        r7.writeAccountInfoLocked();	 Catch:{ all -> 0x009f }
        monitor-exit(r1);
        if (r11 == 0) goto L_0x0097;
    L_0x008a:
        r5 = new android.os.Bundle;
        r5.<init>();
        r3 = -6;
        r0 = r7;
        r1 = r8;
        r2 = r9;
        r4 = r10;
        r0.requestSync(r1, r2, r3, r4, r5);
    L_0x0097:
        r0 = 1;
        r7.reportChange(r0);
        r7.queueBackup();
        return;
    L_0x009f:
        r0 = move-exception;
        monitor-exit(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.setSyncAutomatically(android.accounts.Account, int, java.lang.String, boolean):void");
    }

    public int getIsSyncable(Account account, int userId, String providerName) {
        synchronized (this.mAuthorities) {
            if (account != null) {
                AuthorityInfo authority = getAuthorityLocked(new EndPoint(account, providerName, userId), "get authority syncable");
                if (authority == null) {
                    return -1;
                }
                int i = authority.syncable;
                return i;
            }
            int i2 = this.mAuthorities.size();
            while (i2 > 0) {
                i2--;
                AuthorityInfo authorityInfo = (AuthorityInfo) this.mAuthorities.valueAt(i2);
                if (authorityInfo.target != null && authorityInfo.target.provider.equals(providerName)) {
                    i = authorityInfo.syncable;
                    return i;
                }
            }
            return -1;
        }
    }

    public void setIsSyncable(Account account, int userId, String providerName, int syncable) {
        setSyncableStateForEndPoint(new EndPoint(account, providerName, userId), syncable);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setSyncableStateForEndPoint(com.android.server.content.SyncStorageEngine.EndPoint r7, int r8) {
        /*
        r6 = this;
        r5 = 1;
        r4 = -1;
        r2 = r6.mAuthorities;
        monitor-enter(r2);
        r1 = -1;
        r3 = 0;
        r0 = r6.getOrCreateAuthorityLocked(r7, r1, r3);	 Catch:{ all -> 0x0087 }
        if (r8 >= r4) goto L_0x000e;
    L_0x000d:
        r8 = -1;
    L_0x000e:
        r1 = "SyncManager";
        r3 = 2;
        r1 = android.util.Log.isLoggable(r1, r3);	 Catch:{ all -> 0x0087 }
        if (r1 == 0) goto L_0x0041;
    L_0x0018:
        r1 = "SyncManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0087 }
        r3.<init>();	 Catch:{ all -> 0x0087 }
        r4 = "setIsSyncable: ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x0087 }
        r4 = r0.toString();	 Catch:{ all -> 0x0087 }
        r3 = r3.append(r4);	 Catch:{ all -> 0x0087 }
        r4 = " -> ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x0087 }
        r3 = r3.append(r8);	 Catch:{ all -> 0x0087 }
        r3 = r3.toString();	 Catch:{ all -> 0x0087 }
        android.util.Slog.d(r1, r3);	 Catch:{ all -> 0x0087 }
    L_0x0041:
        r1 = r0.syncable;	 Catch:{ all -> 0x0087 }
        if (r1 != r8) goto L_0x0072;
    L_0x0045:
        r1 = "SyncManager";
        r3 = 2;
        r1 = android.util.Log.isLoggable(r1, r3);	 Catch:{ all -> 0x0087 }
        if (r1 == 0) goto L_0x0070;
    L_0x004f:
        r1 = "SyncManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0087 }
        r3.<init>();	 Catch:{ all -> 0x0087 }
        r4 = "setIsSyncable: already set to ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x0087 }
        r3 = r3.append(r8);	 Catch:{ all -> 0x0087 }
        r4 = ", doing nothing";
        r3 = r3.append(r4);	 Catch:{ all -> 0x0087 }
        r3 = r3.toString();	 Catch:{ all -> 0x0087 }
        android.util.Slog.d(r1, r3);	 Catch:{ all -> 0x0087 }
    L_0x0070:
        monitor-exit(r2);
        return;
    L_0x0072:
        r0.syncable = r8;	 Catch:{ all -> 0x0087 }
        r6.writeAccountInfoLocked();	 Catch:{ all -> 0x0087 }
        monitor-exit(r2);
        if (r8 != r5) goto L_0x0083;
    L_0x007a:
        r1 = new android.os.Bundle;
        r1.<init>();
        r2 = -5;
        r6.requestSync(r0, r2, r1);
    L_0x0083:
        r6.reportChange(r5);
        return;
    L_0x0087:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.setSyncableStateForEndPoint(com.android.server.content.SyncStorageEngine$EndPoint, int):void");
    }

    public Pair<Long, Long> getBackoff(EndPoint info) {
        synchronized (this.mAuthorities) {
            AuthorityInfo authority = getAuthorityLocked(info, "getBackoff");
            if (authority != null) {
                Pair<Long, Long> create = Pair.create(Long.valueOf(authority.backoffTime), Long.valueOf(authority.backoffDelay));
                return create;
            }
            return null;
        }
    }

    public void setBackoff(EndPoint info, long nextSyncTime, long nextDelay) {
        boolean backoffLocked;
        if (Log.isLoggable("SyncManager", 2)) {
            Slog.v("SyncManager", "setBackoff: " + info + " -> nextSyncTime " + nextSyncTime + ", nextDelay " + nextDelay);
        }
        synchronized (this.mAuthorities) {
            if (info.account == null || info.provider == null) {
                backoffLocked = setBackoffLocked(info.account, info.userId, info.provider, nextSyncTime, nextDelay);
            } else {
                AuthorityInfo authorityInfo = getOrCreateAuthorityLocked(info, -1, true);
                if (authorityInfo.backoffTime == nextSyncTime && authorityInfo.backoffDelay == nextDelay) {
                    backoffLocked = false;
                } else {
                    authorityInfo.backoffTime = nextSyncTime;
                    authorityInfo.backoffDelay = nextDelay;
                    backoffLocked = true;
                }
            }
        }
        if (backoffLocked) {
            reportChange(1);
        }
    }

    private boolean setBackoffLocked(Account account, int userId, String providerName, long nextSyncTime, long nextDelay) {
        boolean changed = false;
        for (AccountInfo accountInfo : this.mAccounts.values()) {
            if (account == null || (account.equals(accountInfo.accountAndUser.account) ^ 1) == 0 || userId == accountInfo.accountAndUser.userId) {
                for (AuthorityInfo authorityInfo : accountInfo.authorities.values()) {
                    if ((providerName == null || (providerName.equals(authorityInfo.target.provider) ^ 1) == 0) && !(authorityInfo.backoffTime == nextSyncTime && authorityInfo.backoffDelay == nextDelay)) {
                        authorityInfo.backoffTime = nextSyncTime;
                        authorityInfo.backoffDelay = nextDelay;
                        changed = true;
                    }
                }
            }
        }
        return changed;
    }

    public void clearAllBackoffsLocked() {
        boolean changed = false;
        synchronized (this.mAuthorities) {
            for (AccountInfo accountInfo : this.mAccounts.values()) {
                for (AuthorityInfo authorityInfo : accountInfo.authorities.values()) {
                    if (authorityInfo.backoffTime != -1 || authorityInfo.backoffDelay != -1) {
                        if (Log.isLoggable("SyncManager", 2)) {
                            Slog.v("SyncManager", "clearAllBackoffsLocked: authority:" + authorityInfo.target + " account:" + accountInfo.accountAndUser.account.name + " user:" + accountInfo.accountAndUser.userId + " backoffTime was: " + authorityInfo.backoffTime + " backoffDelay was: " + authorityInfo.backoffDelay);
                        }
                        authorityInfo.backoffTime = -1;
                        authorityInfo.backoffDelay = -1;
                        changed = true;
                    }
                }
            }
        }
        if (changed) {
            reportChange(1);
        }
    }

    public long getDelayUntilTime(EndPoint info) {
        synchronized (this.mAuthorities) {
            AuthorityInfo authority = getAuthorityLocked(info, "getDelayUntil");
            if (authority == null) {
                return 0;
            }
            long j = authority.delayUntil;
            return j;
        }
    }

    public void setDelayUntilTime(EndPoint info, long delayUntil) {
        if (Log.isLoggable("SyncManager", 2)) {
            Slog.v("SyncManager", "setDelayUntil: " + info + " -> delayUntil " + delayUntil);
        }
        synchronized (this.mAuthorities) {
            AuthorityInfo authority = getOrCreateAuthorityLocked(info, -1, true);
            if (authority.delayUntil == delayUntil) {
                return;
            }
            authority.delayUntil = delayUntil;
            reportChange(1);
        }
    }

    boolean restoreAllPeriodicSyncs() {
        if (mPeriodicSyncAddedListener == null) {
            return false;
        }
        synchronized (this.mAuthorities) {
            for (int i = 0; i < this.mAuthorities.size(); i++) {
                AuthorityInfo authority = (AuthorityInfo) this.mAuthorities.valueAt(i);
                for (PeriodicSync periodicSync : authority.periodicSyncs) {
                    mPeriodicSyncAddedListener.onPeriodicSyncAdded(authority.target, periodicSync.extras, periodicSync.period, periodicSync.flexTime);
                }
                authority.periodicSyncs.clear();
            }
            writeAccountInfoLocked();
        }
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setMasterSyncAutomatically(boolean r8, int r9) {
        /*
        r7 = this;
        r1 = 0;
        r2 = r7.mAuthorities;
        monitor-enter(r2);
        r0 = r7.mMasterSyncAutomatically;	 Catch:{ all -> 0x0044 }
        r6 = r0.get(r9);	 Catch:{ all -> 0x0044 }
        r6 = (java.lang.Boolean) r6;	 Catch:{ all -> 0x0044 }
        if (r6 == 0) goto L_0x001a;
    L_0x000e:
        r0 = java.lang.Boolean.valueOf(r8);	 Catch:{ all -> 0x0044 }
        r0 = r6.equals(r0);	 Catch:{ all -> 0x0044 }
        if (r0 == 0) goto L_0x001a;
    L_0x0018:
        monitor-exit(r2);
        return;
    L_0x001a:
        r0 = r7.mMasterSyncAutomatically;	 Catch:{ all -> 0x0044 }
        r3 = java.lang.Boolean.valueOf(r8);	 Catch:{ all -> 0x0044 }
        r0.put(r9, r3);	 Catch:{ all -> 0x0044 }
        r7.writeAccountInfoLocked();	 Catch:{ all -> 0x0044 }
        monitor-exit(r2);
        if (r8 == 0) goto L_0x0035;
    L_0x0029:
        r5 = new android.os.Bundle;
        r5.<init>();
        r3 = -7;
        r0 = r7;
        r2 = r9;
        r4 = r1;
        r0.requestSync(r1, r2, r3, r4, r5);
    L_0x0035:
        r0 = 1;
        r7.reportChange(r0);
        r0 = r7.mContext;
        r1 = android.content.ContentResolver.ACTION_SYNC_CONN_STATUS_CHANGED;
        r0.sendBroadcast(r1);
        r7.queueBackup();
        return;
    L_0x0044:
        r0 = move-exception;
        monitor-exit(r2);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.setMasterSyncAutomatically(boolean, int):void");
    }

    public boolean getMasterSyncAutomatically(int userId) {
        boolean booleanValue;
        synchronized (this.mAuthorities) {
            Boolean auto = (Boolean) this.mMasterSyncAutomatically.get(userId);
            booleanValue = auto == null ? this.mDefaultMasterSyncAutomatically : auto.booleanValue();
        }
        return booleanValue;
    }

    public int getAuthorityCount() {
        int size;
        synchronized (this.mAuthorities) {
            size = this.mAuthorities.size();
        }
        return size;
    }

    public AuthorityInfo getAuthority(int authorityId) {
        AuthorityInfo authorityInfo;
        synchronized (this.mAuthorities) {
            authorityInfo = (AuthorityInfo) this.mAuthorities.get(authorityId);
        }
        return authorityInfo;
    }

    public boolean isSyncActive(EndPoint info) {
        synchronized (this.mAuthorities) {
            for (SyncInfo syncInfo : getCurrentSyncs(info.userId)) {
                AuthorityInfo ainfo = getAuthority(syncInfo.authorityId);
                if (ainfo != null && ainfo.target.matchesSpec(info)) {
                    return true;
                }
            }
            return false;
        }
    }

    public void markPending(EndPoint info, boolean pendingValue) {
        synchronized (this.mAuthorities) {
            AuthorityInfo authority = getOrCreateAuthorityLocked(info, -1, true);
            if (authority == null) {
                return;
            }
            getOrCreateSyncStatusLocked(authority.ident).pending = pendingValue;
            reportChange(2);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doDatabaseCleanup(android.accounts.Account[] r13, int r14) {
        /*
        r12 = this;
        r9 = r12.mAuthorities;
        monitor-enter(r9);
        r8 = "SyncManager";
        r10 = 2;
        r8 = android.util.Log.isLoggable(r8, r10);	 Catch:{ all -> 0x0083 }
        if (r8 == 0) goto L_0x0016;
    L_0x000d:
        r8 = "SyncManager";
        r10 = "Updating for new accounts...";
        android.util.Slog.v(r8, r10);	 Catch:{ all -> 0x0083 }
    L_0x0016:
        r7 = new android.util.SparseArray;	 Catch:{ all -> 0x0083 }
        r7.<init>();	 Catch:{ all -> 0x0083 }
        r8 = r12.mAccounts;	 Catch:{ all -> 0x0083 }
        r8 = r8.values();	 Catch:{ all -> 0x0083 }
        r1 = r8.iterator();	 Catch:{ all -> 0x0083 }
    L_0x0025:
        r8 = r1.hasNext();	 Catch:{ all -> 0x0083 }
        if (r8 == 0) goto L_0x008a;
    L_0x002b:
        r0 = r1.next();	 Catch:{ all -> 0x0083 }
        r0 = (com.android.server.content.SyncStorageEngine.AccountInfo) r0;	 Catch:{ all -> 0x0083 }
        r8 = r0.accountAndUser;	 Catch:{ all -> 0x0083 }
        r8 = r8.account;	 Catch:{ all -> 0x0083 }
        r8 = com.android.internal.util.ArrayUtils.contains(r13, r8);	 Catch:{ all -> 0x0083 }
        if (r8 != 0) goto L_0x0025;
    L_0x003b:
        r8 = r0.accountAndUser;	 Catch:{ all -> 0x0083 }
        r8 = r8.userId;	 Catch:{ all -> 0x0083 }
        if (r8 != r14) goto L_0x0025;
    L_0x0041:
        r8 = "SyncManager";
        r10 = 2;
        r8 = android.util.Log.isLoggable(r8, r10);	 Catch:{ all -> 0x0083 }
        if (r8 == 0) goto L_0x0067;
    L_0x004b:
        r8 = "SyncManager";
        r10 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0083 }
        r10.<init>();	 Catch:{ all -> 0x0083 }
        r11 = "Account removed: ";
        r10 = r10.append(r11);	 Catch:{ all -> 0x0083 }
        r11 = r0.accountAndUser;	 Catch:{ all -> 0x0083 }
        r10 = r10.append(r11);	 Catch:{ all -> 0x0083 }
        r10 = r10.toString();	 Catch:{ all -> 0x0083 }
        android.util.Slog.v(r8, r10);	 Catch:{ all -> 0x0083 }
    L_0x0067:
        r8 = r0.authorities;	 Catch:{ all -> 0x0083 }
        r8 = r8.values();	 Catch:{ all -> 0x0083 }
        r3 = r8.iterator();	 Catch:{ all -> 0x0083 }
    L_0x0071:
        r8 = r3.hasNext();	 Catch:{ all -> 0x0083 }
        if (r8 == 0) goto L_0x0086;
    L_0x0077:
        r2 = r3.next();	 Catch:{ all -> 0x0083 }
        r2 = (com.android.server.content.SyncStorageEngine.AuthorityInfo) r2;	 Catch:{ all -> 0x0083 }
        r8 = r2.ident;	 Catch:{ all -> 0x0083 }
        r7.put(r8, r2);	 Catch:{ all -> 0x0083 }
        goto L_0x0071;
    L_0x0083:
        r8 = move-exception;
        monitor-exit(r9);
        throw r8;
    L_0x0086:
        r1.remove();	 Catch:{ all -> 0x0083 }
        goto L_0x0025;
    L_0x008a:
        r4 = r7.size();	 Catch:{ all -> 0x0083 }
        if (r4 <= 0) goto L_0x00f1;
    L_0x0090:
        if (r4 <= 0) goto L_0x00e8;
    L_0x0092:
        r4 = r4 + -1;
        r5 = r7.keyAt(r4);	 Catch:{ all -> 0x0083 }
        r2 = r7.valueAt(r4);	 Catch:{ all -> 0x0083 }
        r2 = (com.android.server.content.SyncStorageEngine.AuthorityInfo) r2;	 Catch:{ all -> 0x0083 }
        r8 = r12.mAuthorityRemovedListener;	 Catch:{ all -> 0x0083 }
        if (r8 == 0) goto L_0x00a9;
    L_0x00a2:
        r8 = r12.mAuthorityRemovedListener;	 Catch:{ all -> 0x0083 }
        r10 = r2.target;	 Catch:{ all -> 0x0083 }
        r8.onAuthorityRemoved(r10);	 Catch:{ all -> 0x0083 }
    L_0x00a9:
        r8 = r12.mAuthorities;	 Catch:{ all -> 0x0083 }
        r8.remove(r5);	 Catch:{ all -> 0x0083 }
        r8 = r12.mSyncStatus;	 Catch:{ all -> 0x0083 }
        r6 = r8.size();	 Catch:{ all -> 0x0083 }
    L_0x00b4:
        if (r6 <= 0) goto L_0x00cc;
    L_0x00b6:
        r6 = r6 + -1;
        r8 = r12.mSyncStatus;	 Catch:{ all -> 0x0083 }
        r8 = r8.keyAt(r6);	 Catch:{ all -> 0x0083 }
        if (r8 != r5) goto L_0x00b4;
    L_0x00c0:
        r8 = r12.mSyncStatus;	 Catch:{ all -> 0x0083 }
        r10 = r12.mSyncStatus;	 Catch:{ all -> 0x0083 }
        r10 = r10.keyAt(r6);	 Catch:{ all -> 0x0083 }
        r8.remove(r10);	 Catch:{ all -> 0x0083 }
        goto L_0x00b4;
    L_0x00cc:
        r8 = r12.mSyncHistory;	 Catch:{ all -> 0x0083 }
        r6 = r8.size();	 Catch:{ all -> 0x0083 }
    L_0x00d2:
        if (r6 <= 0) goto L_0x0090;
    L_0x00d4:
        r6 = r6 + -1;
        r8 = r12.mSyncHistory;	 Catch:{ all -> 0x0083 }
        r8 = r8.get(r6);	 Catch:{ all -> 0x0083 }
        r8 = (com.android.server.content.SyncStorageEngine.SyncHistoryItem) r8;	 Catch:{ all -> 0x0083 }
        r8 = r8.authorityId;	 Catch:{ all -> 0x0083 }
        if (r8 != r5) goto L_0x00d2;
    L_0x00e2:
        r8 = r12.mSyncHistory;	 Catch:{ all -> 0x0083 }
        r8.remove(r6);	 Catch:{ all -> 0x0083 }
        goto L_0x00d2;
    L_0x00e8:
        r12.writeAccountInfoLocked();	 Catch:{ all -> 0x0083 }
        r12.writeStatusLocked();	 Catch:{ all -> 0x0083 }
        r12.writeStatisticsLocked();	 Catch:{ all -> 0x0083 }
    L_0x00f1:
        monitor-exit(r9);
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.doDatabaseCleanup(android.accounts.Account[], int):void");
    }

    public SyncInfo addActiveSync(ActiveSyncContext activeSyncContext) {
        SyncInfo syncInfo;
        synchronized (this.mAuthorities) {
            if (Log.isLoggable("SyncManager", 2)) {
                Slog.v("SyncManager", "setActiveSync: account= auth=" + activeSyncContext.mSyncOperation.target + " src=" + activeSyncContext.mSyncOperation.syncSource + " extras=" + activeSyncContext.mSyncOperation.extras);
            }
            AuthorityInfo authorityInfo = getOrCreateAuthorityLocked(activeSyncContext.mSyncOperation.target, -1, true);
            syncInfo = new SyncInfo(authorityInfo.ident, authorityInfo.target.account, authorityInfo.target.provider, activeSyncContext.mStartTime);
            getCurrentSyncs(authorityInfo.target.userId).add(syncInfo);
        }
        reportActiveChange();
        return syncInfo;
    }

    public void removeActiveSync(SyncInfo syncInfo, int userId) {
        synchronized (this.mAuthorities) {
            if (Log.isLoggable("SyncManager", 2)) {
                Slog.v("SyncManager", "removeActiveSync: account=" + syncInfo.account + " user=" + userId + " auth=" + syncInfo.authority);
            }
            getCurrentSyncs(userId).remove(syncInfo);
        }
        reportActiveChange();
    }

    public void reportActiveChange() {
        reportChange(4);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long insertStartSyncEvent(com.android.server.content.SyncOperation r9, long r10) {
        /*
        r8 = this;
        r5 = r8.mAuthorities;
        monitor-enter(r5);
        r4 = "SyncManager";
        r6 = 2;
        r4 = android.util.Log.isLoggable(r4, r6);	 Catch:{ all -> 0x0083 }
        if (r4 == 0) goto L_0x0027;
    L_0x000d:
        r4 = "SyncManager";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0083 }
        r6.<init>();	 Catch:{ all -> 0x0083 }
        r7 = "insertStartSyncEvent: ";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0083 }
        r6 = r6.append(r9);	 Catch:{ all -> 0x0083 }
        r6 = r6.toString();	 Catch:{ all -> 0x0083 }
        android.util.Slog.v(r4, r6);	 Catch:{ all -> 0x0083 }
    L_0x0027:
        r4 = r9.target;	 Catch:{ all -> 0x0083 }
        r6 = "insertStartSyncEvent";
        r0 = r8.getAuthorityLocked(r4, r6);	 Catch:{ all -> 0x0083 }
        if (r0 != 0) goto L_0x0036;
    L_0x0032:
        r6 = -1;
        monitor-exit(r5);
        return r6;
    L_0x0036:
        r1 = new com.android.server.content.SyncStorageEngine$SyncHistoryItem;	 Catch:{ all -> 0x0083 }
        r1.<init>();	 Catch:{ all -> 0x0083 }
        r4 = r9.isInitialization();	 Catch:{ all -> 0x0083 }
        r1.initialization = r4;	 Catch:{ all -> 0x0083 }
        r4 = r0.ident;	 Catch:{ all -> 0x0083 }
        r1.authorityId = r4;	 Catch:{ all -> 0x0083 }
        r4 = r8.mNextHistoryId;	 Catch:{ all -> 0x0083 }
        r6 = r4 + 1;
        r8.mNextHistoryId = r6;	 Catch:{ all -> 0x0083 }
        r1.historyId = r4;	 Catch:{ all -> 0x0083 }
        r4 = r8.mNextHistoryId;	 Catch:{ all -> 0x0083 }
        if (r4 >= 0) goto L_0x0054;
    L_0x0051:
        r4 = 0;
        r8.mNextHistoryId = r4;	 Catch:{ all -> 0x0083 }
    L_0x0054:
        r1.eventTime = r10;	 Catch:{ all -> 0x0083 }
        r4 = r9.syncSource;	 Catch:{ all -> 0x0083 }
        r1.source = r4;	 Catch:{ all -> 0x0083 }
        r4 = r9.reason;	 Catch:{ all -> 0x0083 }
        r1.reason = r4;	 Catch:{ all -> 0x0083 }
        r4 = r9.extras;	 Catch:{ all -> 0x0083 }
        r1.extras = r4;	 Catch:{ all -> 0x0083 }
        r4 = 0;
        r1.event = r4;	 Catch:{ all -> 0x0083 }
        r4 = r8.mSyncHistory;	 Catch:{ all -> 0x0083 }
        r6 = 0;
        r4.add(r6, r1);	 Catch:{ all -> 0x0083 }
    L_0x006b:
        r4 = r8.mSyncHistory;	 Catch:{ all -> 0x0083 }
        r4 = r4.size();	 Catch:{ all -> 0x0083 }
        r6 = 100;
        if (r4 <= r6) goto L_0x0086;
    L_0x0075:
        r4 = r8.mSyncHistory;	 Catch:{ all -> 0x0083 }
        r6 = r8.mSyncHistory;	 Catch:{ all -> 0x0083 }
        r6 = r6.size();	 Catch:{ all -> 0x0083 }
        r6 = r6 + -1;
        r4.remove(r6);	 Catch:{ all -> 0x0083 }
        goto L_0x006b;
    L_0x0083:
        r4 = move-exception;
        monitor-exit(r5);
        throw r4;
    L_0x0086:
        r4 = r1.historyId;	 Catch:{ all -> 0x0083 }
        r2 = (long) r4;	 Catch:{ all -> 0x0083 }
        r4 = "SyncManager";
        r6 = 2;
        r4 = android.util.Log.isLoggable(r4, r6);	 Catch:{ all -> 0x0083 }
        if (r4 == 0) goto L_0x00ad;
    L_0x0093:
        r4 = "SyncManager";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0083 }
        r6.<init>();	 Catch:{ all -> 0x0083 }
        r7 = "returning historyId ";
        r6 = r6.append(r7);	 Catch:{ all -> 0x0083 }
        r6 = r6.append(r2);	 Catch:{ all -> 0x0083 }
        r6 = r6.toString();	 Catch:{ all -> 0x0083 }
        android.util.Slog.v(r4, r6);	 Catch:{ all -> 0x0083 }
    L_0x00ad:
        monitor-exit(r5);
        r4 = 8;
        r8.reportChange(r4);
        return r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.insertStartSyncEvent(com.android.server.content.SyncOperation, long):long");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void stopSyncEvent(long r20, long r22, java.lang.String r24, long r25, long r27) {
        /*
        r19 = this;
        r0 = r19;
        r13 = r0.mAuthorities;
        monitor-enter(r13);
        r12 = "SyncManager";
        r14 = 2;
        r12 = android.util.Log.isLoggable(r12, r14);	 Catch:{ all -> 0x0164 }
        if (r12 == 0) goto L_0x002b;
    L_0x000f:
        r12 = "SyncManager";
        r14 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0164 }
        r14.<init>();	 Catch:{ all -> 0x0164 }
        r15 = "stopSyncEvent: historyId=";
        r14 = r14.append(r15);	 Catch:{ all -> 0x0164 }
        r0 = r20;
        r14 = r14.append(r0);	 Catch:{ all -> 0x0164 }
        r14 = r14.toString();	 Catch:{ all -> 0x0164 }
        android.util.Slog.v(r12, r14);	 Catch:{ all -> 0x0164 }
    L_0x002b:
        r6 = 0;
        r0 = r19;
        r12 = r0.mSyncHistory;	 Catch:{ all -> 0x0164 }
        r5 = r12.size();	 Catch:{ all -> 0x0164 }
    L_0x0034:
        if (r5 <= 0) goto L_0x0049;
    L_0x0036:
        r5 = r5 + -1;
        r0 = r19;
        r12 = r0.mSyncHistory;	 Catch:{ all -> 0x0164 }
        r6 = r12.get(r5);	 Catch:{ all -> 0x0164 }
        r6 = (com.android.server.content.SyncStorageEngine.SyncHistoryItem) r6;	 Catch:{ all -> 0x0164 }
        r12 = r6.historyId;	 Catch:{ all -> 0x0164 }
        r14 = (long) r12;	 Catch:{ all -> 0x0164 }
        r12 = (r14 > r20 ? 1 : (r14 == r20 ? 0 : -1));
        if (r12 != 0) goto L_0x0069;
    L_0x0049:
        if (r6 != 0) goto L_0x006b;
    L_0x004b:
        r12 = "SyncManager";
        r14 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0164 }
        r14.<init>();	 Catch:{ all -> 0x0164 }
        r15 = "stopSyncEvent: no history for id ";
        r14 = r14.append(r15);	 Catch:{ all -> 0x0164 }
        r0 = r20;
        r14 = r14.append(r0);	 Catch:{ all -> 0x0164 }
        r14 = r14.toString();	 Catch:{ all -> 0x0164 }
        android.util.Slog.w(r12, r14);	 Catch:{ all -> 0x0164 }
        monitor-exit(r13);
        return;
    L_0x0069:
        r6 = 0;
        goto L_0x0034;
    L_0x006b:
        r0 = r22;
        r6.elapsedTime = r0;	 Catch:{ all -> 0x0164 }
        r12 = 1;
        r6.event = r12;	 Catch:{ all -> 0x0164 }
        r0 = r24;
        r6.mesg = r0;	 Catch:{ all -> 0x0164 }
        r0 = r25;
        r6.downstreamActivity = r0;	 Catch:{ all -> 0x0164 }
        r0 = r27;
        r6.upstreamActivity = r0;	 Catch:{ all -> 0x0164 }
        r12 = r6.authorityId;	 Catch:{ all -> 0x0164 }
        r0 = r19;
        r7 = r0.getOrCreateSyncStatusLocked(r12);	 Catch:{ all -> 0x0164 }
        r12 = r7.numSyncs;	 Catch:{ all -> 0x0164 }
        r12 = r12 + 1;
        r7.numSyncs = r12;	 Catch:{ all -> 0x0164 }
        r14 = r7.totalElapsedTime;	 Catch:{ all -> 0x0164 }
        r14 = r14 + r22;
        r7.totalElapsedTime = r14;	 Catch:{ all -> 0x0164 }
        r12 = r6.source;	 Catch:{ all -> 0x0164 }
        switch(r12) {
            case 0: goto L_0x0177;
            case 1: goto L_0x015c;
            case 2: goto L_0x0167;
            case 3: goto L_0x016f;
            case 4: goto L_0x017f;
            default: goto L_0x0097;
        };	 Catch:{ all -> 0x0164 }
    L_0x0097:
        r10 = 0;
        r2 = r19.getCurrentDayLocked();	 Catch:{ all -> 0x0164 }
        r0 = r19;
        r12 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r14 = 0;
        r12 = r12[r14];	 Catch:{ all -> 0x0164 }
        if (r12 != 0) goto L_0x0187;
    L_0x00a5:
        r0 = r19;
        r12 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r14 = new com.android.server.content.SyncStorageEngine$DayStats;	 Catch:{ all -> 0x0164 }
        r14.<init>(r2);	 Catch:{ all -> 0x0164 }
        r15 = 0;
        r12[r15] = r14;	 Catch:{ all -> 0x0164 }
    L_0x00b1:
        r0 = r19;
        r12 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r14 = 0;
        r3 = r12[r14];	 Catch:{ all -> 0x0164 }
        r14 = r6.eventTime;	 Catch:{ all -> 0x0164 }
        r8 = r14 + r22;
        r11 = 0;
        r12 = "success";
        r0 = r24;
        r12 = r12.equals(r0);	 Catch:{ all -> 0x0164 }
        if (r12 == 0) goto L_0x01c4;
    L_0x00c8:
        r14 = r7.lastSuccessTime;	 Catch:{ all -> 0x0164 }
        r16 = 0;
        r12 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1));
        if (r12 == 0) goto L_0x00d8;
    L_0x00d0:
        r14 = r7.lastFailureTime;	 Catch:{ all -> 0x0164 }
        r16 = 0;
        r12 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1));
        if (r12 == 0) goto L_0x00d9;
    L_0x00d8:
        r11 = 1;
    L_0x00d9:
        r7.lastSuccessTime = r8;	 Catch:{ all -> 0x0164 }
        r12 = r6.source;	 Catch:{ all -> 0x0164 }
        r7.lastSuccessSource = r12;	 Catch:{ all -> 0x0164 }
        r14 = 0;
        r7.lastFailureTime = r14;	 Catch:{ all -> 0x0164 }
        r12 = -1;
        r7.lastFailureSource = r12;	 Catch:{ all -> 0x0164 }
        r12 = 0;
        r7.lastFailureMesg = r12;	 Catch:{ all -> 0x0164 }
        r14 = 0;
        r7.initialFailureTime = r14;	 Catch:{ all -> 0x0164 }
        r12 = r3.successCount;	 Catch:{ all -> 0x0164 }
        r12 = r12 + 1;
        r3.successCount = r12;	 Catch:{ all -> 0x0164 }
        r14 = r3.successTime;	 Catch:{ all -> 0x0164 }
        r14 = r14 + r22;
        r3.successTime = r14;	 Catch:{ all -> 0x0164 }
    L_0x00f9:
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0164 }
        r4.<init>();	 Catch:{ all -> 0x0164 }
        r12 = "";
        r12 = r4.append(r12);	 Catch:{ all -> 0x0164 }
        r0 = r24;
        r12 = r12.append(r0);	 Catch:{ all -> 0x0164 }
        r14 = " Source=";
        r12 = r12.append(r14);	 Catch:{ all -> 0x0164 }
        r14 = SOURCES;	 Catch:{ all -> 0x0164 }
        r15 = r6.source;	 Catch:{ all -> 0x0164 }
        r14 = r14[r15];	 Catch:{ all -> 0x0164 }
        r12 = r12.append(r14);	 Catch:{ all -> 0x0164 }
        r14 = " Elapsed=";
        r12.append(r14);	 Catch:{ all -> 0x0164 }
        r0 = r22;
        com.android.server.content.SyncManager.formatDurationHMS(r4, r0);	 Catch:{ all -> 0x0164 }
        r12 = " Reason=";
        r4.append(r12);	 Catch:{ all -> 0x0164 }
        r12 = r6.reason;	 Catch:{ all -> 0x0164 }
        r14 = 0;
        r12 = com.android.server.content.SyncOperation.reasonToString(r14, r12);	 Catch:{ all -> 0x0164 }
        r4.append(r12);	 Catch:{ all -> 0x0164 }
        r12 = " Extras=";
        r4.append(r12);	 Catch:{ all -> 0x0164 }
        r12 = r6.extras;	 Catch:{ all -> 0x0164 }
        com.android.server.content.SyncOperation.extrasToStringBuilder(r12, r4);	 Catch:{ all -> 0x0164 }
        r12 = r4.toString();	 Catch:{ all -> 0x0164 }
        r7.addEvent(r12);	 Catch:{ all -> 0x0164 }
        if (r11 == 0) goto L_0x01fa;
    L_0x014b:
        r19.writeStatusLocked();	 Catch:{ all -> 0x0164 }
    L_0x014e:
        if (r10 == 0) goto L_0x0214;
    L_0x0150:
        r19.writeStatisticsLocked();	 Catch:{ all -> 0x0164 }
    L_0x0153:
        monitor-exit(r13);
        r12 = 8;
        r0 = r19;
        r0.reportChange(r12);
        return;
    L_0x015c:
        r12 = r7.numSourceLocal;	 Catch:{ all -> 0x0164 }
        r12 = r12 + 1;
        r7.numSourceLocal = r12;	 Catch:{ all -> 0x0164 }
        goto L_0x0097;
    L_0x0164:
        r12 = move-exception;
        monitor-exit(r13);
        throw r12;
    L_0x0167:
        r12 = r7.numSourcePoll;	 Catch:{ all -> 0x0164 }
        r12 = r12 + 1;
        r7.numSourcePoll = r12;	 Catch:{ all -> 0x0164 }
        goto L_0x0097;
    L_0x016f:
        r12 = r7.numSourceUser;	 Catch:{ all -> 0x0164 }
        r12 = r12 + 1;
        r7.numSourceUser = r12;	 Catch:{ all -> 0x0164 }
        goto L_0x0097;
    L_0x0177:
        r12 = r7.numSourceServer;	 Catch:{ all -> 0x0164 }
        r12 = r12 + 1;
        r7.numSourceServer = r12;	 Catch:{ all -> 0x0164 }
        goto L_0x0097;
    L_0x017f:
        r12 = r7.numSourcePeriodic;	 Catch:{ all -> 0x0164 }
        r12 = r12 + 1;
        r7.numSourcePeriodic = r12;	 Catch:{ all -> 0x0164 }
        goto L_0x0097;
    L_0x0187:
        r0 = r19;
        r12 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r14 = 0;
        r12 = r12[r14];	 Catch:{ all -> 0x0164 }
        r12 = r12.day;	 Catch:{ all -> 0x0164 }
        if (r2 == r12) goto L_0x01bb;
    L_0x0192:
        r0 = r19;
        r12 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r0 = r19;
        r14 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r0 = r19;
        r15 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r15 = r15.length;	 Catch:{ all -> 0x0164 }
        r15 = r15 + -1;
        r16 = 0;
        r17 = 1;
        r0 = r16;
        r1 = r17;
        java.lang.System.arraycopy(r12, r0, r14, r1, r15);	 Catch:{ all -> 0x0164 }
        r0 = r19;
        r12 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r14 = new com.android.server.content.SyncStorageEngine$DayStats;	 Catch:{ all -> 0x0164 }
        r14.<init>(r2);	 Catch:{ all -> 0x0164 }
        r15 = 0;
        r12[r15] = r14;	 Catch:{ all -> 0x0164 }
        r10 = 1;
        goto L_0x00b1;
    L_0x01bb:
        r0 = r19;
        r12 = r0.mDayStats;	 Catch:{ all -> 0x0164 }
        r14 = 0;
        r12 = r12[r14];	 Catch:{ all -> 0x0164 }
        goto L_0x00b1;
    L_0x01c4:
        r12 = "canceled";
        r0 = r24;
        r12 = r12.equals(r0);	 Catch:{ all -> 0x0164 }
        if (r12 != 0) goto L_0x00f9;
    L_0x01cf:
        r14 = r7.lastFailureTime;	 Catch:{ all -> 0x0164 }
        r16 = 0;
        r12 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1));
        if (r12 != 0) goto L_0x01d8;
    L_0x01d7:
        r11 = 1;
    L_0x01d8:
        r7.lastFailureTime = r8;	 Catch:{ all -> 0x0164 }
        r12 = r6.source;	 Catch:{ all -> 0x0164 }
        r7.lastFailureSource = r12;	 Catch:{ all -> 0x0164 }
        r0 = r24;
        r7.lastFailureMesg = r0;	 Catch:{ all -> 0x0164 }
        r14 = r7.initialFailureTime;	 Catch:{ all -> 0x0164 }
        r16 = 0;
        r12 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1));
        if (r12 != 0) goto L_0x01ec;
    L_0x01ea:
        r7.initialFailureTime = r8;	 Catch:{ all -> 0x0164 }
    L_0x01ec:
        r12 = r3.failureCount;	 Catch:{ all -> 0x0164 }
        r12 = r12 + 1;
        r3.failureCount = r12;	 Catch:{ all -> 0x0164 }
        r14 = r3.failureTime;	 Catch:{ all -> 0x0164 }
        r14 = r14 + r22;
        r3.failureTime = r14;	 Catch:{ all -> 0x0164 }
        goto L_0x00f9;
    L_0x01fa:
        r12 = 1;
        r0 = r19;
        r12 = r0.hasMessages(r12);	 Catch:{ all -> 0x0164 }
        if (r12 != 0) goto L_0x014e;
    L_0x0203:
        r12 = 1;
        r0 = r19;
        r12 = r0.obtainMessage(r12);	 Catch:{ all -> 0x0164 }
        r14 = 600000; // 0x927c0 float:8.40779E-40 double:2.964394E-318;
        r0 = r19;
        r0.sendMessageDelayed(r12, r14);	 Catch:{ all -> 0x0164 }
        goto L_0x014e;
    L_0x0214:
        r12 = 2;
        r0 = r19;
        r12 = r0.hasMessages(r12);	 Catch:{ all -> 0x0164 }
        if (r12 != 0) goto L_0x0153;
    L_0x021d:
        r12 = 2;
        r0 = r19;
        r12 = r0.obtainMessage(r12);	 Catch:{ all -> 0x0164 }
        r14 = 1800000; // 0x1b7740 float:2.522337E-39 double:8.89318E-318;
        r0 = r19;
        r0.sendMessageDelayed(r12, r14);	 Catch:{ all -> 0x0164 }
        goto L_0x0153;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.stopSyncEvent(long, long, java.lang.String, long, long):void");
    }

    private List<SyncInfo> getCurrentSyncs(int userId) {
        List<SyncInfo> currentSyncsLocked;
        synchronized (this.mAuthorities) {
            currentSyncsLocked = getCurrentSyncsLocked(userId);
        }
        return currentSyncsLocked;
    }

    public List<SyncInfo> getCurrentSyncsCopy(int userId, boolean canAccessAccounts) {
        List<SyncInfo> syncsCopy;
        synchronized (this.mAuthorities) {
            List<SyncInfo> syncs = getCurrentSyncsLocked(userId);
            syncsCopy = new ArrayList();
            for (SyncInfo sync : syncs) {
                SyncInfo copy;
                if (canAccessAccounts) {
                    copy = new SyncInfo(sync);
                } else {
                    copy = SyncInfo.createAccountRedacted(sync.authorityId, sync.authority, sync.startTime);
                }
                syncsCopy.add(copy);
            }
        }
        return syncsCopy;
    }

    private List<SyncInfo> getCurrentSyncsLocked(int userId) {
        ArrayList<SyncInfo> syncs = (ArrayList) this.mCurrentSyncs.get(userId);
        if (syncs != null) {
            return syncs;
        }
        syncs = new ArrayList();
        this.mCurrentSyncs.put(userId, syncs);
        return syncs;
    }

    public Pair<AuthorityInfo, SyncStatusInfo> getCopyOfAuthorityWithSyncStatus(EndPoint info) {
        Pair<AuthorityInfo, SyncStatusInfo> createCopyPairOfAuthorityWithSyncStatusLocked;
        synchronized (this.mAuthorities) {
            createCopyPairOfAuthorityWithSyncStatusLocked = createCopyPairOfAuthorityWithSyncStatusLocked(getOrCreateAuthorityLocked(info, -1, true));
        }
        return createCopyPairOfAuthorityWithSyncStatusLocked;
    }

    public SyncStatusInfo getStatusByAuthority(EndPoint info) {
        if (info.account == null || info.provider == null) {
            return null;
        }
        synchronized (this.mAuthorities) {
            int N = this.mSyncStatus.size();
            int i = 0;
            while (i < N) {
                SyncStatusInfo cur = (SyncStatusInfo) this.mSyncStatus.valueAt(i);
                AuthorityInfo ainfo = (AuthorityInfo) this.mAuthorities.get(cur.authorityId);
                if (ainfo == null || !ainfo.target.matchesSpec(info)) {
                    i++;
                } else {
                    return cur;
                }
            }
            return null;
        }
    }

    public boolean isSyncPending(EndPoint info) {
        synchronized (this.mAuthorities) {
            int N = this.mSyncStatus.size();
            for (int i = 0; i < N; i++) {
                SyncStatusInfo cur = (SyncStatusInfo) this.mSyncStatus.valueAt(i);
                AuthorityInfo ainfo = (AuthorityInfo) this.mAuthorities.get(cur.authorityId);
                if (ainfo != null && ainfo.target.matchesSpec(info) && cur.pending) {
                    return true;
                }
            }
            return false;
        }
    }

    public ArrayList<SyncHistoryItem> getSyncHistory() {
        ArrayList<SyncHistoryItem> items;
        synchronized (this.mAuthorities) {
            int N = this.mSyncHistory.size();
            items = new ArrayList(N);
            for (int i = 0; i < N; i++) {
                items.add((SyncHistoryItem) this.mSyncHistory.get(i));
            }
        }
        return items;
    }

    public DayStats[] getDayStatistics() {
        DayStats[] ds;
        synchronized (this.mAuthorities) {
            ds = new DayStats[this.mDayStats.length];
            System.arraycopy(this.mDayStats, 0, ds, 0, ds.length);
        }
        return ds;
    }

    private Pair<AuthorityInfo, SyncStatusInfo> createCopyPairOfAuthorityWithSyncStatusLocked(AuthorityInfo authorityInfo) {
        return Pair.create(new AuthorityInfo(authorityInfo), new SyncStatusInfo(getOrCreateSyncStatusLocked(authorityInfo.ident)));
    }

    private int getCurrentDayLocked() {
        this.mCal.setTimeInMillis(System.currentTimeMillis());
        int dayOfYear = this.mCal.get(6);
        if (this.mYear != this.mCal.get(1)) {
            this.mYear = this.mCal.get(1);
            this.mCal.clear();
            this.mCal.set(1, this.mYear);
            this.mYearInDays = (int) (this.mCal.getTimeInMillis() / UnixCalendar.DAY_IN_MILLIS);
        }
        return this.mYearInDays + dayOfYear;
    }

    private AuthorityInfo getAuthorityLocked(EndPoint info, String tag) {
        AccountAndUser au = new AccountAndUser(info.account, info.userId);
        AccountInfo accountInfo = (AccountInfo) this.mAccounts.get(au);
        if (accountInfo == null) {
            if (tag != null && Log.isLoggable("SyncManager", 2)) {
                Slog.v("SyncManager", tag + ": unknown account " + au);
            }
            return null;
        }
        AuthorityInfo authority = (AuthorityInfo) accountInfo.authorities.get(info.provider);
        if (authority != null) {
            return authority;
        }
        if (tag != null && Log.isLoggable("SyncManager", 2)) {
            Slog.v("SyncManager", tag + ": unknown provider " + info.provider);
        }
        return null;
    }

    private AuthorityInfo getOrCreateAuthorityLocked(EndPoint info, int ident, boolean doWrite) {
        AccountAndUser au = new AccountAndUser(info.account, info.userId);
        AccountInfo account = (AccountInfo) this.mAccounts.get(au);
        if (account == null) {
            account = new AccountInfo(au);
            this.mAccounts.put(au, account);
        }
        AuthorityInfo authority = (AuthorityInfo) account.authorities.get(info.provider);
        if (authority != null) {
            return authority;
        }
        authority = createAuthorityLocked(info, ident, doWrite);
        account.authorities.put(info.provider, authority);
        return authority;
    }

    private AuthorityInfo createAuthorityLocked(EndPoint info, int ident, boolean doWrite) {
        if (ident < 0) {
            ident = this.mNextAuthorityId;
            this.mNextAuthorityId++;
            doWrite = true;
        }
        if (Log.isLoggable("SyncManager", 2)) {
            Slog.v("SyncManager", "created a new AuthorityInfo for " + info);
        }
        AuthorityInfo authority = new AuthorityInfo(info, ident);
        this.mAuthorities.put(ident, authority);
        if (doWrite) {
            writeAccountInfoLocked();
        }
        return authority;
    }

    public void removeAuthority(EndPoint info) {
        synchronized (this.mAuthorities) {
            removeAuthorityLocked(info.account, info.userId, info.provider, true);
        }
    }

    private void removeAuthorityLocked(Account account, int userId, String authorityName, boolean doWrite) {
        AccountInfo accountInfo = (AccountInfo) this.mAccounts.get(new AccountAndUser(account, userId));
        if (accountInfo != null) {
            AuthorityInfo authorityInfo = (AuthorityInfo) accountInfo.authorities.remove(authorityName);
            if (authorityInfo != null) {
                if (this.mAuthorityRemovedListener != null) {
                    this.mAuthorityRemovedListener.onAuthorityRemoved(authorityInfo.target);
                }
                this.mAuthorities.remove(authorityInfo.ident);
                if (doWrite) {
                    writeAccountInfoLocked();
                }
            }
        }
    }

    private SyncStatusInfo getOrCreateSyncStatusLocked(int authorityId) {
        SyncStatusInfo status = (SyncStatusInfo) this.mSyncStatus.get(authorityId);
        if (status != null) {
            return status;
        }
        status = new SyncStatusInfo(authorityId);
        this.mSyncStatus.put(authorityId, status);
        return status;
    }

    public void writeAllState() {
        synchronized (this.mAuthorities) {
            writeStatusLocked();
            writeStatisticsLocked();
        }
    }

    public boolean shouldGrantSyncAdaptersAccountAccess() {
        return this.mGrantSyncAdaptersAccountAccess;
    }

    public void clearAndReadState() {
        synchronized (this.mAuthorities) {
            this.mAuthorities.clear();
            this.mAccounts.clear();
            this.mServices.clear();
            this.mSyncStatus.clear();
            this.mSyncHistory.clear();
            readAccountInfoLocked();
            readStatusLocked();
            readStatisticsLocked();
            readAndDeleteLegacyAccountInfoLocked();
            writeAccountInfoLocked();
            writeStatusLocked();
            writeStatisticsLocked();
        }
    }

    private void readAccountInfoLocked() {
        int highestAuthorityId = -1;
        FileInputStream fis = null;
        try {
            fis = this.mAccountInfoFile.openRead();
            if (Log.isLoggable(TAG_FILE, 2)) {
                Slog.v(TAG_FILE, "Reading " + this.mAccountInfoFile.getBaseFile());
            }
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(fis, StandardCharsets.UTF_8.name());
            int eventType = parser.getEventType();
            while (eventType != 2 && eventType != 1) {
                eventType = parser.next();
            }
            if (eventType == 1) {
                Slog.i("SyncManager", "No initial accounts");
                this.mNextAuthorityId = Math.max(0, this.mNextAuthorityId);
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                    }
                }
                return;
            }
            if ("accounts".equals(parser.getName())) {
                int version;
                String listen = parser.getAttributeValue(null, XML_ATTR_LISTEN_FOR_TICKLES);
                String versionString = parser.getAttributeValue(null, "version");
                if (versionString == null) {
                    version = 0;
                } else {
                    try {
                        version = Integer.parseInt(versionString);
                    } catch (NumberFormatException e2) {
                        version = 0;
                    }
                }
                if (version < 3) {
                    this.mGrantSyncAdaptersAccountAccess = true;
                }
                String nextIdString = parser.getAttributeValue(null, XML_ATTR_NEXT_AUTHORITY_ID);
                try {
                    this.mNextAuthorityId = Math.max(this.mNextAuthorityId, nextIdString == null ? 0 : Integer.parseInt(nextIdString));
                } catch (NumberFormatException e3) {
                }
                String offsetString = parser.getAttributeValue(null, XML_ATTR_SYNC_RANDOM_OFFSET);
                try {
                    this.mSyncRandomOffset = offsetString == null ? 0 : Integer.parseInt(offsetString);
                } catch (NumberFormatException e4) {
                    this.mSyncRandomOffset = 0;
                }
                if (this.mSyncRandomOffset == 0) {
                    this.mSyncRandomOffset = new Random(System.currentTimeMillis()).nextInt(86400);
                }
                this.mMasterSyncAutomatically.put(0, Boolean.valueOf(listen != null ? Boolean.parseBoolean(listen) : true));
                eventType = parser.next();
                AuthorityInfo authority = null;
                PeriodicSync periodicSync = null;
                AccountAuthorityValidator accountAuthorityValidator = new AccountAuthorityValidator(this.mContext);
                do {
                    if (eventType == 2) {
                        String tagName = parser.getName();
                        if (parser.getDepth() == 2) {
                            if ("authority".equals(tagName)) {
                                authority = parseAuthority(parser, version, accountAuthorityValidator);
                                periodicSync = null;
                                if (authority == null) {
                                    EventLog.writeEvent(1397638484, new Object[]{"26513719", Integer.valueOf(-1), "Malformed authority"});
                                } else if (authority.ident > highestAuthorityId) {
                                    highestAuthorityId = authority.ident;
                                }
                            } else if (XML_TAG_LISTEN_FOR_TICKLES.equals(tagName)) {
                                parseListenForTickles(parser);
                            }
                        } else if (parser.getDepth() == 3) {
                            if ("periodicSync".equals(tagName) && authority != null) {
                                periodicSync = parsePeriodicSync(parser, authority);
                            }
                        } else if (parser.getDepth() == 4 && periodicSync != null && "extra".equals(tagName)) {
                            parseExtra(parser, periodicSync.extras);
                        }
                    }
                    eventType = parser.next();
                } while (eventType != 1);
            }
            this.mNextAuthorityId = Math.max(highestAuthorityId + 1, this.mNextAuthorityId);
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e5) {
                }
            }
            maybeMigrateSettingsForRenamedAuthorities();
        } catch (XmlPullParserException e6) {
            Slog.w("SyncManager", "Error reading accounts", e6);
            this.mNextAuthorityId = Math.max(highestAuthorityId + 1, this.mNextAuthorityId);
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e7) {
                }
            }
        } catch (IOException e8) {
            if (fis == null) {
                Slog.i("SyncManager", "No initial accounts");
            } else {
                Slog.w("SyncManager", "Error reading accounts", e8);
            }
            this.mNextAuthorityId = Math.max(highestAuthorityId + 1, this.mNextAuthorityId);
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e9) {
                }
            }
        } catch (Throwable th) {
            this.mNextAuthorityId = Math.max(highestAuthorityId + 1, this.mNextAuthorityId);
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e10) {
                }
            }
        }
    }

    private void maybeDeleteLegacyPendingInfoLocked(File syncDir) {
        File file = new File(syncDir, "pending.bin");
        if (file.exists()) {
            file.delete();
        }
    }

    private boolean maybeMigrateSettingsForRenamedAuthorities() {
        boolean writeNeeded = false;
        ArrayList<AuthorityInfo> authoritiesToRemove = new ArrayList();
        int N = this.mAuthorities.size();
        for (int i = 0; i < N; i++) {
            AuthorityInfo authority = (AuthorityInfo) this.mAuthorities.valueAt(i);
            String newAuthorityName = (String) sAuthorityRenames.get(authority.target.provider);
            if (newAuthorityName != null) {
                authoritiesToRemove.add(authority);
                if (authority.enabled) {
                    EndPoint newInfo = new EndPoint(authority.target.account, newAuthorityName, authority.target.userId);
                    if (getAuthorityLocked(newInfo, "cleanup") == null) {
                        getOrCreateAuthorityLocked(newInfo, -1, false).enabled = true;
                        writeNeeded = true;
                    }
                }
            }
        }
        for (AuthorityInfo authorityInfo : authoritiesToRemove) {
            removeAuthorityLocked(authorityInfo.target.account, authorityInfo.target.userId, authorityInfo.target.provider, false);
            writeNeeded = true;
        }
        return writeNeeded;
    }

    private void parseListenForTickles(XmlPullParser parser) {
        int userId = 0;
        try {
            userId = Integer.parseInt(parser.getAttributeValue(null, XML_ATTR_USER));
        } catch (NumberFormatException e) {
            Slog.e("SyncManager", "error parsing the user for listen-for-tickles", e);
        } catch (NullPointerException e2) {
            Slog.e("SyncManager", "the user in listen-for-tickles is null", e2);
        }
        String enabled = parser.getAttributeValue(null, XML_ATTR_ENABLED);
        this.mMasterSyncAutomatically.put(userId, Boolean.valueOf(enabled != null ? Boolean.parseBoolean(enabled) : true));
    }

    private AuthorityInfo parseAuthority(XmlPullParser parser, int version, AccountAuthorityValidator validator) {
        AuthorityInfo authority = null;
        int id = -1;
        try {
            id = Integer.parseInt(parser.getAttributeValue(null, "id"));
        } catch (NumberFormatException e) {
            Slog.e("SyncManager", "error parsing the id of the authority", e);
        } catch (NullPointerException e2) {
            Slog.e("SyncManager", "the id of the authority is null", e2);
        }
        if (id >= 0) {
            String authorityName = parser.getAttributeValue(null, "authority");
            String enabled = parser.getAttributeValue(null, XML_ATTR_ENABLED);
            String syncable = parser.getAttributeValue(null, "syncable");
            String accountName = parser.getAttributeValue(null, "account");
            String accountType = parser.getAttributeValue(null, SoundModelContract.KEY_TYPE);
            String user = parser.getAttributeValue(null, XML_ATTR_USER);
            String packageName = parser.getAttributeValue(null, "package");
            String className = parser.getAttributeValue(null, AudioService.CONNECT_INTENT_KEY_DEVICE_CLASS);
            int userId = user == null ? 0 : Integer.parseInt(user);
            if (accountType == null && packageName == null) {
                accountType = "com.google";
                syncable = String.valueOf(-1);
            }
            authority = (AuthorityInfo) this.mAuthorities.get(id);
            if (Log.isLoggable(TAG_FILE, 2)) {
                Slog.v(TAG_FILE, "Adding authority: account=" + accountName + " accountType=" + accountType + " auth=" + authorityName + " package=" + packageName + " class=" + className + " user=" + userId + " enabled=" + enabled + " syncable=" + syncable);
            }
            if (authority == null) {
                if (Log.isLoggable(TAG_FILE, 2)) {
                    Slog.v(TAG_FILE, "Creating authority entry");
                }
                if (!(accountName == null || authorityName == null)) {
                    EndPoint info = new EndPoint(new Account(accountName, accountType), authorityName, userId);
                    if (validator.isAccountValid(info.account, userId) && validator.isAuthorityValid(authorityName, userId)) {
                        authority = getOrCreateAuthorityLocked(info, id, false);
                        if (version > 0) {
                            authority.periodicSyncs.clear();
                        }
                    } else {
                        EventLog.writeEvent(1397638484, new Object[]{"35028827", Integer.valueOf(-1), "account:" + info.account + " provider:" + authorityName + " user:" + userId});
                    }
                }
            }
            if (authority != null) {
                authority.enabled = enabled != null ? Boolean.parseBoolean(enabled) : true;
                try {
                    authority.syncable = syncable == null ? -1 : Integer.parseInt(syncable);
                } catch (NumberFormatException e3) {
                    if (Shell.NIGHT_MODE_STR_UNKNOWN.equals(syncable)) {
                        authority.syncable = -1;
                    } else {
                        authority.syncable = Boolean.parseBoolean(syncable) ? 1 : 0;
                    }
                }
            } else {
                Slog.w("SyncManager", "Failure adding authority: account=" + accountName + " auth=" + authorityName + " enabled=" + enabled + " syncable=" + syncable);
            }
        }
        return authority;
    }

    private PeriodicSync parsePeriodicSync(XmlPullParser parser, AuthorityInfo authorityInfo) {
        long flextime;
        Bundle extras = new Bundle();
        String periodValue = parser.getAttributeValue(null, "period");
        String flexValue = parser.getAttributeValue(null, "flex");
        try {
            long period = Long.parseLong(periodValue);
            try {
                flextime = Long.parseLong(flexValue);
            } catch (NumberFormatException e) {
                flextime = calculateDefaultFlexTime(period);
                Slog.e("SyncManager", "Error formatting value parsed for periodic sync flex: " + flexValue + ", using default: " + flextime);
            } catch (NullPointerException e2) {
                flextime = calculateDefaultFlexTime(period);
                Slog.d("SyncManager", "No flex time specified for this sync, using a default. period: " + period + " flex: " + flextime);
            }
            PeriodicSync periodicSync = new PeriodicSync(authorityInfo.target.account, authorityInfo.target.provider, extras, period, flextime);
            authorityInfo.periodicSyncs.add(periodicSync);
            return periodicSync;
        } catch (NumberFormatException e3) {
            Slog.e("SyncManager", "error parsing the period of a periodic sync", e3);
            return null;
        } catch (NullPointerException e4) {
            Slog.e("SyncManager", "the period of a periodic sync is null", e4);
            return null;
        }
    }

    private void parseExtra(XmlPullParser parser, Bundle extras) {
        String name = parser.getAttributeValue(null, "name");
        String type = parser.getAttributeValue(null, SoundModelContract.KEY_TYPE);
        String value1 = parser.getAttributeValue(null, "value1");
        String value2 = parser.getAttributeValue(null, "value2");
        try {
            if ("long".equals(type)) {
                extras.putLong(name, Long.parseLong(value1));
            } else if ("integer".equals(type)) {
                extras.putInt(name, Integer.parseInt(value1));
            } else if ("double".equals(type)) {
                extras.putDouble(name, Double.parseDouble(value1));
            } else if ("float".equals(type)) {
                extras.putFloat(name, Float.parseFloat(value1));
            } else if ("boolean".equals(type)) {
                extras.putBoolean(name, Boolean.parseBoolean(value1));
            } else if ("string".equals(type)) {
                extras.putString(name, value1);
            } else if ("account".equals(type)) {
                extras.putParcelable(name, new Account(value1, value2));
            }
        } catch (NumberFormatException e) {
            Slog.e("SyncManager", "error parsing bundle value", e);
        } catch (NullPointerException e2) {
            Slog.e("SyncManager", "error parsing bundle value", e2);
        }
    }

    private void writeAccountInfoLocked() {
        if (Log.isLoggable(TAG_FILE, 2)) {
            Slog.v(TAG_FILE, "Writing new " + this.mAccountInfoFile.getBaseFile());
        }
        try {
            FileOutputStream fos = this.mAccountInfoFile.startWrite();
            XmlSerializer out = new FastXmlSerializer();
            out.setOutput(fos, StandardCharsets.UTF_8.name());
            out.startDocument(null, Boolean.valueOf(true));
            out.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            out.startTag(null, "accounts");
            out.attribute(null, "version", Integer.toString(3));
            out.attribute(null, XML_ATTR_NEXT_AUTHORITY_ID, Integer.toString(this.mNextAuthorityId));
            out.attribute(null, XML_ATTR_SYNC_RANDOM_OFFSET, Integer.toString(this.mSyncRandomOffset));
            int M = this.mMasterSyncAutomatically.size();
            for (int m = 0; m < M; m++) {
                int userId = this.mMasterSyncAutomatically.keyAt(m);
                Boolean listen = (Boolean) this.mMasterSyncAutomatically.valueAt(m);
                out.startTag(null, XML_TAG_LISTEN_FOR_TICKLES);
                out.attribute(null, XML_ATTR_USER, Integer.toString(userId));
                out.attribute(null, XML_ATTR_ENABLED, Boolean.toString(listen.booleanValue()));
                out.endTag(null, XML_TAG_LISTEN_FOR_TICKLES);
            }
            int N = this.mAuthorities.size();
            for (int i = 0; i < N; i++) {
                AuthorityInfo authority = (AuthorityInfo) this.mAuthorities.valueAt(i);
                EndPoint info = authority.target;
                out.startTag(null, "authority");
                out.attribute(null, "id", Integer.toString(authority.ident));
                out.attribute(null, XML_ATTR_USER, Integer.toString(info.userId));
                out.attribute(null, XML_ATTR_ENABLED, Boolean.toString(authority.enabled));
                out.attribute(null, "account", info.account.name);
                out.attribute(null, SoundModelContract.KEY_TYPE, info.account.type);
                out.attribute(null, "authority", info.provider);
                out.attribute(null, "syncable", Integer.toString(authority.syncable));
                out.endTag(null, "authority");
            }
            out.endTag(null, "accounts");
            out.endDocument();
            this.mAccountInfoFile.finishWrite(fos);
        } catch (IOException e1) {
            Slog.w("SyncManager", "Error writing accounts", e1);
            if (null != null) {
                this.mAccountInfoFile.failWrite(null);
            }
        }
    }

    static int getIntColumn(Cursor c, String name) {
        return c.getInt(c.getColumnIndex(name));
    }

    static long getLongColumn(Cursor c, String name) {
        return c.getLong(c.getColumnIndex(name));
    }

    private void readAndDeleteLegacyAccountInfoLocked() {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r24_5 'st' android.content.SyncStatusInfo) in PHI: PHI: (r24_6 android.content.SyncStatusInfo) = (r24_4 android.content.SyncStatusInfo), (r24_5 'st' android.content.SyncStatusInfo) binds: {(r24_4 android.content.SyncStatusInfo)=B:32:0x017e, (r24_5 'st' android.content.SyncStatusInfo)=B:33:0x0180}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:79)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:60)
	at jadx.core.ProcessClass.process(ProcessClass.java:39)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r26 = this;
        r0 = r26;
        r4 = r0.mContext;
        r5 = "syncmanager.db";
        r16 = r4.getDatabasePath(r5);
        r4 = r16.exists();
        if (r4 != 0) goto L_0x0012;
    L_0x0011:
        return;
    L_0x0012:
        r22 = r16.getPath();
        r3 = 0;
        r4 = 0;
        r5 = 1;
        r0 = r22;	 Catch:{ SQLiteException -> 0x0226 }
        r3 = android.database.sqlite.SQLiteDatabase.openDatabase(r0, r4, r5);	 Catch:{ SQLiteException -> 0x0226 }
    L_0x001f:
        if (r3 == 0) goto L_0x02e0;
    L_0x0021:
        r4 = r3.getVersion();
        r5 = 11;
        if (r4 < r5) goto L_0x0229;
    L_0x0029:
        r18 = 1;
    L_0x002b:
        r4 = "SyncManagerFile";
        r5 = 2;
        r4 = android.util.Log.isLoggable(r4, r5);
        if (r4 == 0) goto L_0x003e;
    L_0x0035:
        r4 = "SyncManagerFile";
        r5 = "Reading legacy sync accounts db";
        android.util.Slog.v(r4, r5);
    L_0x003e:
        r2 = new android.database.sqlite.SQLiteQueryBuilder;
        r2.<init>();
        r4 = "stats, status";
        r2.setTables(r4);
        r20 = new java.util.HashMap;
        r20.<init>();
        r4 = "_id";
        r5 = "status._id as _id";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "account";
        r5 = "stats.account as account";
        r0 = r20;
        r0.put(r4, r5);
        if (r18 == 0) goto L_0x0071;
    L_0x0066:
        r4 = "account_type";
        r5 = "stats.account_type as account_type";
        r0 = r20;
        r0.put(r4, r5);
    L_0x0071:
        r4 = "authority";
        r5 = "stats.authority as authority";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "totalElapsedTime";
        r5 = "totalElapsedTime";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "numSyncs";
        r5 = "numSyncs";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "numSourceLocal";
        r5 = "numSourceLocal";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "numSourcePoll";
        r5 = "numSourcePoll";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "numSourceServer";
        r5 = "numSourceServer";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "numSourceUser";
        r5 = "numSourceUser";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "lastSuccessSource";
        r5 = "lastSuccessSource";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "lastSuccessTime";
        r5 = "lastSuccessTime";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "lastFailureSource";
        r5 = "lastFailureSource";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "lastFailureTime";
        r5 = "lastFailureTime";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "lastFailureMesg";
        r5 = "lastFailureMesg";
        r0 = r20;
        r0.put(r4, r5);
        r4 = "pending";
        r5 = "pending";
        r0 = r20;
        r0.put(r4, r5);
        r0 = r20;
        r2.setProjectionMap(r0);
        r4 = "stats._id = status.stats_id";
        r2.appendWhere(r4);
        r4 = 0;
        r5 = 0;
        r6 = 0;
        r7 = 0;
        r8 = 0;
        r9 = 0;
        r14 = r2.query(r3, r4, r5, r6, r7, r8, r9);
    L_0x0115:
        r4 = r14.moveToNext();
        if (r4 == 0) goto L_0x0232;
    L_0x011b:
        r4 = "account";
        r4 = r14.getColumnIndex(r4);
        r10 = r14.getString(r4);
        if (r18 == 0) goto L_0x022d;
    L_0x0128:
        r4 = "account_type";
        r4 = r14.getColumnIndex(r4);
        r11 = r14.getString(r4);
    L_0x0133:
        if (r11 != 0) goto L_0x0138;
    L_0x0135:
        r11 = "com.google";
    L_0x0138:
        r4 = "authority";
        r4 = r14.getColumnIndex(r4);
        r13 = r14.getString(r4);
        r4 = new com.android.server.content.SyncStorageEngine$EndPoint;
        r5 = new android.accounts.Account;
        r5.<init>(r10, r11);
        r6 = 0;
        r4.<init>(r5, r13, r6);
        r5 = -1;
        r6 = 0;
        r0 = r26;
        r12 = r0.getOrCreateAuthorityLocked(r4, r5, r6);
        if (r12 == 0) goto L_0x0115;
    L_0x0158:
        r0 = r26;
        r4 = r0.mSyncStatus;
        r19 = r4.size();
        r17 = 0;
        r24 = 0;
    L_0x0164:
        if (r19 <= 0) goto L_0x017e;
    L_0x0166:
        r19 = r19 + -1;
        r0 = r26;
        r4 = r0.mSyncStatus;
        r0 = r19;
        r24 = r4.valueAt(r0);
        r24 = (android.content.SyncStatusInfo) r24;
        r0 = r24;
        r4 = r0.authorityId;
        r5 = r12.ident;
        if (r4 != r5) goto L_0x0164;
    L_0x017c:
        r17 = 1;
    L_0x017e:
        if (r17 != 0) goto L_0x0194;
    L_0x0180:
        r24 = new android.content.SyncStatusInfo;
        r4 = r12.ident;
        r0 = r24;
        r0.<init>(r4);
        r0 = r26;
        r4 = r0.mSyncStatus;
        r5 = r12.ident;
        r0 = r24;
        r4.put(r5, r0);
    L_0x0194:
        r4 = "totalElapsedTime";
        r4 = getLongColumn(r14, r4);
        r0 = r24;
        r0.totalElapsedTime = r4;
        r4 = "numSyncs";
        r4 = getIntColumn(r14, r4);
        r0 = r24;
        r0.numSyncs = r4;
        r4 = "numSourceLocal";
        r4 = getIntColumn(r14, r4);
        r0 = r24;
        r0.numSourceLocal = r4;
        r4 = "numSourcePoll";
        r4 = getIntColumn(r14, r4);
        r0 = r24;
        r0.numSourcePoll = r4;
        r4 = "numSourceServer";
        r4 = getIntColumn(r14, r4);
        r0 = r24;
        r0.numSourceServer = r4;
        r4 = "numSourceUser";
        r4 = getIntColumn(r14, r4);
        r0 = r24;
        r0.numSourceUser = r4;
        r4 = 0;
        r0 = r24;
        r0.numSourcePeriodic = r4;
        r4 = "lastSuccessSource";
        r4 = getIntColumn(r14, r4);
        r0 = r24;
        r0.lastSuccessSource = r4;
        r4 = "lastSuccessTime";
        r4 = getLongColumn(r14, r4);
        r0 = r24;
        r0.lastSuccessTime = r4;
        r4 = "lastFailureSource";
        r4 = getIntColumn(r14, r4);
        r0 = r24;
        r0.lastFailureSource = r4;
        r4 = "lastFailureTime";
        r4 = getLongColumn(r14, r4);
        r0 = r24;
        r0.lastFailureTime = r4;
        r4 = "lastFailureMesg";
        r4 = r14.getColumnIndex(r4);
        r4 = r14.getString(r4);
        r0 = r24;
        r0.lastFailureMesg = r4;
        r4 = "pending";
        r4 = getIntColumn(r14, r4);
        if (r4 == 0) goto L_0x0230;
    L_0x021f:
        r4 = 1;
    L_0x0220:
        r0 = r24;
        r0.pending = r4;
        goto L_0x0115;
    L_0x0226:
        r15 = move-exception;
        goto L_0x001f;
    L_0x0229:
        r18 = 0;
        goto L_0x002b;
    L_0x022d:
        r11 = 0;
        goto L_0x0133;
    L_0x0230:
        r4 = 0;
        goto L_0x0220;
    L_0x0232:
        r14.close();
        r2 = new android.database.sqlite.SQLiteQueryBuilder;
        r2.<init>();
        r4 = "settings";
        r2.setTables(r4);
        r4 = 0;
        r5 = 0;
        r6 = 0;
        r7 = 0;
        r8 = 0;
        r9 = 0;
        r14 = r2.query(r3, r4, r5, r6, r7, r8, r9);
    L_0x024a:
        r4 = r14.moveToNext();
        if (r4 == 0) goto L_0x02d0;
    L_0x0250:
        r4 = "name";
        r4 = r14.getColumnIndex(r4);
        r21 = r14.getString(r4);
        r4 = "value";
        r4 = r14.getColumnIndex(r4);
        r25 = r14.getString(r4);
        if (r21 == 0) goto L_0x024a;
    L_0x0268:
        r4 = "listen_for_tickles";
        r0 = r21;
        r4 = r0.equals(r4);
        if (r4 == 0) goto L_0x0282;
    L_0x0273:
        if (r25 == 0) goto L_0x0280;
    L_0x0275:
        r4 = java.lang.Boolean.parseBoolean(r25);
    L_0x0279:
        r5 = 0;
        r0 = r26;
        r0.setMasterSyncAutomatically(r4, r5);
        goto L_0x024a;
    L_0x0280:
        r4 = 1;
        goto L_0x0279;
    L_0x0282:
        r4 = "sync_provider_";
        r0 = r21;
        r4 = r0.startsWith(r4);
        if (r4 == 0) goto L_0x024a;
    L_0x028d:
        r4 = "sync_provider_";
        r4 = r4.length();
        r5 = r21.length();
        r0 = r21;
        r23 = r0.substring(r4, r5);
        r0 = r26;
        r4 = r0.mAuthorities;
        r19 = r4.size();
    L_0x02a6:
        if (r19 <= 0) goto L_0x024a;
    L_0x02a8:
        r19 = r19 + -1;
        r0 = r26;
        r4 = r0.mAuthorities;
        r0 = r19;
        r12 = r4.valueAt(r0);
        r12 = (com.android.server.content.SyncStorageEngine.AuthorityInfo) r12;
        r4 = r12.target;
        r4 = r4.provider;
        r0 = r23;
        r4 = r4.equals(r0);
        if (r4 == 0) goto L_0x02a6;
    L_0x02c2:
        if (r25 == 0) goto L_0x02ce;
    L_0x02c4:
        r4 = java.lang.Boolean.parseBoolean(r25);
    L_0x02c8:
        r12.enabled = r4;
        r4 = 1;
        r12.syncable = r4;
        goto L_0x02a6;
    L_0x02ce:
        r4 = 1;
        goto L_0x02c8;
    L_0x02d0:
        r14.close();
        r3.close();
        r4 = new java.io.File;
        r0 = r22;
        r4.<init>(r0);
        r4.delete();
    L_0x02e0:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.content.SyncStorageEngine.readAndDeleteLegacyAccountInfoLocked():void");
    }

    private void readStatusLocked() {
        if (Log.isLoggable(TAG_FILE, 2)) {
            Slog.v(TAG_FILE, "Reading " + this.mStatusFile.getBaseFile());
        }
        try {
            byte[] data = this.mStatusFile.readFully();
            Parcel in = Parcel.obtain();
            in.unmarshall(data, 0, data.length);
            in.setDataPosition(0);
            while (true) {
                int token = in.readInt();
                if (token == 0) {
                    return;
                }
                if (token == 100) {
                    SyncStatusInfo status = new SyncStatusInfo(in);
                    if (this.mAuthorities.indexOfKey(status.authorityId) >= 0) {
                        status.pending = false;
                        if (Log.isLoggable(TAG_FILE, 2)) {
                            Slog.v(TAG_FILE, "Adding status for id " + status.authorityId);
                        }
                        this.mSyncStatus.put(status.authorityId, status);
                    }
                } else {
                    Slog.w("SyncManager", "Unknown status token: " + token);
                    return;
                }
            }
        } catch (IOException e) {
            Slog.i("SyncManager", "No initial status");
        }
    }

    private void writeStatusLocked() {
        if (Log.isLoggable(TAG_FILE, 2)) {
            Slog.v(TAG_FILE, "Writing new " + this.mStatusFile.getBaseFile());
        }
        removeMessages(1);
        try {
            FileOutputStream fos = this.mStatusFile.startWrite();
            Parcel out = Parcel.obtain();
            int N = this.mSyncStatus.size();
            for (int i = 0; i < N; i++) {
                SyncStatusInfo status = (SyncStatusInfo) this.mSyncStatus.valueAt(i);
                out.writeInt(100);
                status.writeToParcel(out, 0);
            }
            out.writeInt(0);
            fos.write(out.marshall());
            out.recycle();
            this.mStatusFile.finishWrite(fos);
        } catch (IOException e1) {
            Slog.w("SyncManager", "Error writing status", e1);
            if (null != null) {
                this.mStatusFile.failWrite(null);
            }
        }
    }

    private void requestSync(AuthorityInfo authorityInfo, int reason, Bundle extras) {
        if (Process.myUid() != 1000 || this.mSyncRequestListener == null) {
            Builder req = new Builder().syncOnce().setExtras(extras);
            req.setSyncAdapter(authorityInfo.target.account, authorityInfo.target.provider);
            ContentResolver.requestSync(req.build());
            return;
        }
        this.mSyncRequestListener.onSyncRequest(authorityInfo.target, reason, extras);
    }

    private void requestSync(Account account, int userId, int reason, String authority, Bundle extras) {
        if (Process.myUid() != 1000 || this.mSyncRequestListener == null) {
            ContentResolver.requestSync(account, authority, extras);
        } else {
            this.mSyncRequestListener.onSyncRequest(new EndPoint(account, authority, userId), reason, extras);
        }
    }

    private void readStatisticsLocked() {
        try {
            byte[] data = this.mStatisticsFile.readFully();
            Parcel in = Parcel.obtain();
            in.unmarshall(data, 0, data.length);
            in.setDataPosition(0);
            int index = 0;
            while (true) {
                int token = in.readInt();
                if (token == 0) {
                    return;
                }
                if (token == 101 || token == 100) {
                    int day = in.readInt();
                    if (token == 100) {
                        day = (day - 2009) + 14245;
                    }
                    DayStats ds = new DayStats(day);
                    ds.successCount = in.readInt();
                    ds.successTime = in.readLong();
                    ds.failureCount = in.readInt();
                    ds.failureTime = in.readLong();
                    if (index < this.mDayStats.length) {
                        this.mDayStats[index] = ds;
                        index++;
                    }
                } else {
                    Slog.w("SyncManager", "Unknown stats token: " + token);
                    return;
                }
            }
        } catch (IOException e) {
            Slog.i("SyncManager", "No initial statistics");
        }
    }

    private void writeStatisticsLocked() {
        if (Log.isLoggable(TAG_FILE, 2)) {
            Slog.v("SyncManager", "Writing new " + this.mStatisticsFile.getBaseFile());
        }
        removeMessages(2);
        try {
            FileOutputStream fos = this.mStatisticsFile.startWrite();
            Parcel out = Parcel.obtain();
            for (DayStats ds : this.mDayStats) {
                if (ds == null) {
                    break;
                }
                out.writeInt(101);
                out.writeInt(ds.day);
                out.writeInt(ds.successCount);
                out.writeLong(ds.successTime);
                out.writeInt(ds.failureCount);
                out.writeLong(ds.failureTime);
            }
            out.writeInt(0);
            fos.write(out.marshall());
            out.recycle();
            this.mStatisticsFile.finishWrite(fos);
        } catch (IOException e1) {
            Slog.w("SyncManager", "Error writing stats", e1);
            if (null != null) {
                this.mStatisticsFile.failWrite(null);
            }
        }
    }

    public void queueBackup() {
        BackupManager.dataChanged("android");
    }
}
