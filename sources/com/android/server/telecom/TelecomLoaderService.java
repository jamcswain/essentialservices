package com.android.server.telecom;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManagerInternal;
import android.content.pm.PackageManagerInternal.PackagesProvider;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.provider.Settings.Secure;
import android.telecom.DefaultDialerManager;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.util.IntArray;
import android.util.Slog;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.telephony.SmsApplication;
import com.android.server.LocalServices;
import com.android.server.SystemService;
import com.android.server.pm.UserManagerService;

public class TelecomLoaderService extends SystemService {
    private static final String SERVICE_ACTION = "com.android.ITelecomService";
    private static final ComponentName SERVICE_COMPONENT = new ComponentName("com.android.server.telecom", "com.android.server.telecom.components.TelecomService");
    private static final String TAG = "TelecomLoaderService";
    private final Context mContext;
    @GuardedBy("mLock")
    private IntArray mDefaultDialerAppRequests;
    @GuardedBy("mLock")
    private IntArray mDefaultSimCallManagerRequests;
    @GuardedBy("mLock")
    private IntArray mDefaultSmsAppRequests;
    private final Object mLock = new Object();
    @GuardedBy("mLock")
    private TelecomServiceConnection mServiceConnection;

    private class TelecomServiceConnection implements ServiceConnection {
        private TelecomServiceConnection() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            try {
                service.linkToDeath(new DeathRecipient() {
                    public void binderDied() {
                        TelecomLoaderService.this.connectToTelecom();
                    }
                }, 0);
                SmsApplication.getDefaultMmsApplication(TelecomLoaderService.this.mContext, false);
                ServiceManager.addService("telecom", service);
                synchronized (TelecomLoaderService.this.mLock) {
                    if (!(TelecomLoaderService.this.mDefaultSmsAppRequests == null && TelecomLoaderService.this.mDefaultDialerAppRequests == null && TelecomLoaderService.this.mDefaultSimCallManagerRequests == null)) {
                        int i;
                        String packageName;
                        int userId;
                        PackageManagerInternal packageManagerInternal = (PackageManagerInternal) LocalServices.getService(PackageManagerInternal.class);
                        if (TelecomLoaderService.this.mDefaultSmsAppRequests != null) {
                            ComponentName smsComponent = SmsApplication.getDefaultSmsApplication(TelecomLoaderService.this.mContext, true);
                            if (smsComponent != null) {
                                for (i = TelecomLoaderService.this.mDefaultSmsAppRequests.size() - 1; i >= 0; i--) {
                                    int userid = TelecomLoaderService.this.mDefaultSmsAppRequests.get(i);
                                    TelecomLoaderService.this.mDefaultSmsAppRequests.remove(i);
                                    packageManagerInternal.grantDefaultPermissionsToDefaultSmsApp(smsComponent.getPackageName(), userid);
                                }
                            }
                        }
                        if (TelecomLoaderService.this.mDefaultDialerAppRequests != null) {
                            packageName = DefaultDialerManager.getDefaultDialerApplication(TelecomLoaderService.this.mContext);
                            if (packageName != null) {
                                for (i = TelecomLoaderService.this.mDefaultDialerAppRequests.size() - 1; i >= 0; i--) {
                                    userId = TelecomLoaderService.this.mDefaultDialerAppRequests.get(i);
                                    TelecomLoaderService.this.mDefaultDialerAppRequests.remove(i);
                                    packageManagerInternal.grantDefaultPermissionsToDefaultDialerApp(packageName, userId);
                                }
                            }
                        }
                        if (TelecomLoaderService.this.mDefaultSimCallManagerRequests != null) {
                            PhoneAccountHandle phoneAccount = ((TelecomManager) TelecomLoaderService.this.mContext.getSystemService("telecom")).getSimCallManager();
                            if (phoneAccount != null) {
                                int requestCount = TelecomLoaderService.this.mDefaultSimCallManagerRequests.size();
                                packageName = phoneAccount.getComponentName().getPackageName();
                                for (i = requestCount - 1; i >= 0; i--) {
                                    userId = TelecomLoaderService.this.mDefaultSimCallManagerRequests.get(i);
                                    TelecomLoaderService.this.mDefaultSimCallManagerRequests.remove(i);
                                    packageManagerInternal.grantDefaultPermissionsToDefaultSimCallManager(packageName, userId);
                                }
                            }
                        }
                    }
                }
            } catch (RemoteException e) {
                Slog.w(TelecomLoaderService.TAG, "Failed linking to death.");
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            TelecomLoaderService.this.connectToTelecom();
        }
    }

    public TelecomLoaderService(Context context) {
        super(context);
        this.mContext = context;
        registerDefaultAppProviders();
    }

    public void onStart() {
    }

    public void onBootPhase(int phase) {
        if (phase == SystemService.PHASE_ACTIVITY_MANAGER_READY) {
            registerDefaultAppNotifier();
            registerCarrierConfigChangedReceiver();
            connectToTelecom();
        }
    }

    private void connectToTelecom() {
        synchronized (this.mLock) {
            if (this.mServiceConnection != null) {
                this.mContext.unbindService(this.mServiceConnection);
                this.mServiceConnection = null;
            }
            TelecomServiceConnection serviceConnection = new TelecomServiceConnection();
            Intent intent = new Intent(SERVICE_ACTION);
            intent.setComponent(SERVICE_COMPONENT);
            if (this.mContext.bindServiceAsUser(intent, serviceConnection, 67108929, UserHandle.SYSTEM)) {
                this.mServiceConnection = serviceConnection;
            }
        }
    }

    private void registerDefaultAppProviders() {
        PackageManagerInternal packageManagerInternal = (PackageManagerInternal) LocalServices.getService(PackageManagerInternal.class);
        packageManagerInternal.setSmsAppPackagesProvider(new PackagesProvider() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.String[] getPackages(int r6) {
                /*
                r5 = this;
                r3 = 1;
                r4 = 0;
                r1 = com.android.server.telecom.TelecomLoaderService.this;
                r2 = r1.mLock;
                monitor-enter(r2);
                r1 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0045 }
                r1 = r1.mServiceConnection;	 Catch:{ all -> 0x0045 }
                if (r1 != 0) goto L_0x002e;
            L_0x0011:
                r1 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0045 }
                r1 = r1.mDefaultSmsAppRequests;	 Catch:{ all -> 0x0045 }
                if (r1 != 0) goto L_0x0023;
            L_0x0019:
                r1 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0045 }
                r3 = new android.util.IntArray;	 Catch:{ all -> 0x0045 }
                r3.<init>();	 Catch:{ all -> 0x0045 }
                r1.mDefaultSmsAppRequests = r3;	 Catch:{ all -> 0x0045 }
            L_0x0023:
                r1 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0045 }
                r1 = r1.mDefaultSmsAppRequests;	 Catch:{ all -> 0x0045 }
                r1.add(r6);	 Catch:{ all -> 0x0045 }
                monitor-exit(r2);
                return r4;
            L_0x002e:
                monitor-exit(r2);
                r1 = com.android.server.telecom.TelecomLoaderService.this;
                r1 = r1.mContext;
                r0 = com.android.internal.telephony.SmsApplication.getDefaultSmsApplication(r1, r3);
                if (r0 == 0) goto L_0x0048;
            L_0x003b:
                r1 = new java.lang.String[r3];
                r2 = r0.getPackageName();
                r3 = 0;
                r1[r3] = r2;
                return r1;
            L_0x0045:
                r1 = move-exception;
                monitor-exit(r2);
                throw r1;
            L_0x0048:
                return r4;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.android.server.telecom.TelecomLoaderService.1.getPackages(int):java.lang.String[]");
            }
        });
        packageManagerInternal.setDialerAppPackagesProvider(new PackagesProvider() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.String[] getPackages(int r6) {
                /*
                r5 = this;
                r4 = 0;
                r1 = com.android.server.telecom.TelecomLoaderService.this;
                r2 = r1.mLock;
                monitor-enter(r2);
                r1 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0041 }
                r1 = r1.mServiceConnection;	 Catch:{ all -> 0x0041 }
                if (r1 != 0) goto L_0x002d;
            L_0x0010:
                r1 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0041 }
                r1 = r1.mDefaultDialerAppRequests;	 Catch:{ all -> 0x0041 }
                if (r1 != 0) goto L_0x0022;
            L_0x0018:
                r1 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0041 }
                r3 = new android.util.IntArray;	 Catch:{ all -> 0x0041 }
                r3.<init>();	 Catch:{ all -> 0x0041 }
                r1.mDefaultDialerAppRequests = r3;	 Catch:{ all -> 0x0041 }
            L_0x0022:
                r1 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0041 }
                r1 = r1.mDefaultDialerAppRequests;	 Catch:{ all -> 0x0041 }
                r1.add(r6);	 Catch:{ all -> 0x0041 }
                monitor-exit(r2);
                return r4;
            L_0x002d:
                monitor-exit(r2);
                r1 = com.android.server.telecom.TelecomLoaderService.this;
                r1 = r1.mContext;
                r0 = android.telecom.DefaultDialerManager.getDefaultDialerApplication(r1);
                if (r0 == 0) goto L_0x0044;
            L_0x003a:
                r1 = 1;
                r1 = new java.lang.String[r1];
                r2 = 0;
                r1[r2] = r0;
                return r1;
            L_0x0041:
                r1 = move-exception;
                monitor-exit(r2);
                throw r1;
            L_0x0044:
                return r4;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.android.server.telecom.TelecomLoaderService.2.getPackages(int):java.lang.String[]");
            }
        });
        packageManagerInternal.setSimCallManagerPackagesProvider(new PackagesProvider() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.String[] getPackages(int r7) {
                /*
                r6 = this;
                r5 = 0;
                r2 = com.android.server.telecom.TelecomLoaderService.this;
                r3 = r2.mLock;
                monitor-enter(r3);
                r2 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0052 }
                r2 = r2.mServiceConnection;	 Catch:{ all -> 0x0052 }
                if (r2 != 0) goto L_0x002d;
            L_0x0010:
                r2 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0052 }
                r2 = r2.mDefaultSimCallManagerRequests;	 Catch:{ all -> 0x0052 }
                if (r2 != 0) goto L_0x0022;
            L_0x0018:
                r2 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0052 }
                r4 = new android.util.IntArray;	 Catch:{ all -> 0x0052 }
                r4.<init>();	 Catch:{ all -> 0x0052 }
                r2.mDefaultSimCallManagerRequests = r4;	 Catch:{ all -> 0x0052 }
            L_0x0022:
                r2 = com.android.server.telecom.TelecomLoaderService.this;	 Catch:{ all -> 0x0052 }
                r2 = r2.mDefaultSimCallManagerRequests;	 Catch:{ all -> 0x0052 }
                r2.add(r7);	 Catch:{ all -> 0x0052 }
                monitor-exit(r3);
                return r5;
            L_0x002d:
                monitor-exit(r3);
                r2 = com.android.server.telecom.TelecomLoaderService.this;
                r2 = r2.mContext;
                r3 = "telecom";
                r1 = r2.getSystemService(r3);
                r1 = (android.telecom.TelecomManager) r1;
                r0 = r1.getSimCallManager(r7);
                if (r0 == 0) goto L_0x0055;
            L_0x0043:
                r2 = 1;
                r2 = new java.lang.String[r2];
                r3 = r0.getComponentName();
                r3 = r3.getPackageName();
                r4 = 0;
                r2[r4] = r3;
                return r2;
            L_0x0052:
                r2 = move-exception;
                monitor-exit(r3);
                throw r2;
            L_0x0055:
                return r5;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.android.server.telecom.TelecomLoaderService.3.getPackages(int):java.lang.String[]");
            }
        });
    }

    private void registerDefaultAppNotifier() {
        final PackageManagerInternal packageManagerInternal = (PackageManagerInternal) LocalServices.getService(PackageManagerInternal.class);
        final Uri defaultSmsAppUri = Secure.getUriFor("sms_default_application");
        final Uri defaultDialerAppUri = Secure.getUriFor("dialer_default_application");
        ContentObserver contentObserver = new ContentObserver(new Handler(Looper.getMainLooper())) {
            public void onChange(boolean selfChange, Uri uri, int userId) {
                if (defaultSmsAppUri.equals(uri)) {
                    ComponentName smsComponent = SmsApplication.getDefaultSmsApplication(TelecomLoaderService.this.mContext, true);
                    if (smsComponent != null) {
                        packageManagerInternal.grantDefaultPermissionsToDefaultSmsApp(smsComponent.getPackageName(), userId);
                    }
                } else if (defaultDialerAppUri.equals(uri)) {
                    String packageName = DefaultDialerManager.getDefaultDialerApplication(TelecomLoaderService.this.mContext);
                    if (packageName != null) {
                        packageManagerInternal.grantDefaultPermissionsToDefaultDialerApp(packageName, userId);
                    }
                    TelecomLoaderService.this.updateSimCallManagerPermissions(packageManagerInternal, userId);
                }
            }
        };
        this.mContext.getContentResolver().registerContentObserver(defaultSmsAppUri, false, contentObserver, -1);
        this.mContext.getContentResolver().registerContentObserver(defaultDialerAppUri, false, contentObserver, -1);
    }

    private void registerCarrierConfigChangedReceiver() {
        final PackageManagerInternal packageManagerInternal = (PackageManagerInternal) LocalServices.getService(PackageManagerInternal.class);
        this.mContext.registerReceiverAsUser(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("android.telephony.action.CARRIER_CONFIG_CHANGED")) {
                    for (int userId : UserManagerService.getInstance().getUserIds()) {
                        TelecomLoaderService.this.updateSimCallManagerPermissions(packageManagerInternal, userId);
                    }
                }
            }
        }, UserHandle.ALL, new IntentFilter("android.telephony.action.CARRIER_CONFIG_CHANGED"), null, null);
    }

    private void updateSimCallManagerPermissions(PackageManagerInternal packageManagerInternal, int userId) {
        PhoneAccountHandle phoneAccount = ((TelecomManager) this.mContext.getSystemService("telecom")).getSimCallManager(userId);
        if (phoneAccount != null) {
            Slog.i(TAG, "updating sim call manager permissions for userId:" + userId);
            packageManagerInternal.grantDefaultPermissionsToDefaultSimCallManager(phoneAccount.getComponentName().getPackageName(), userId);
        }
    }
}
