package com.android.server.wm;

import android.content.ClipData;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.Trace;
import android.util.MergedConfiguration;
import android.util.Slog;
import android.view.Display;
import android.view.IWindow;
import android.view.IWindowId;
import android.view.IWindowSession.Stub;
import android.view.IWindowSessionCallback;
import android.view.InputChannel;
import android.view.Surface;
import android.view.SurfaceControl;
import android.view.SurfaceSession;
import android.view.WindowManager.LayoutParams;
import com.android.internal.view.IInputMethodClient;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

public class Session extends Stub implements DeathRecipient {
    private AlertWindowNotification mAlertWindowNotification;
    private final Set<WindowSurfaceController> mAlertWindowSurfaces = new HashSet();
    private final Set<WindowSurfaceController> mAppOverlaySurfaces = new HashSet();
    final IWindowSessionCallback mCallback;
    final boolean mCanAcquireSleepToken;
    final boolean mCanAddInternalSystemWindow;
    final boolean mCanHideNonSystemOverlayWindows;
    final IInputMethodClient mClient;
    private boolean mClientDead = false;
    private float mLastReportedAnimatorScale;
    private int mNumWindow = 0;
    private String mPackageName;
    final int mPid;
    private String mRelayoutTag;
    final WindowManagerService mService;
    private boolean mShowingAlertWindowNotificationAllowed;
    private final String mStringName;
    SurfaceSession mSurfaceSession;
    final int mUid;

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Session(com.android.server.wm.WindowManagerService r11, android.view.IWindowSessionCallback r12, com.android.internal.view.IInputMethodClient r13, com.android.internal.view.IInputContext r14) {
        /*
        r10 = this;
        r7 = 1;
        r8 = 0;
        r10.<init>();
        r10.mNumWindow = r8;
        r6 = new java.util.HashSet;
        r6.<init>();
        r10.mAppOverlaySurfaces = r6;
        r6 = new java.util.HashSet;
        r6.<init>();
        r10.mAlertWindowSurfaces = r6;
        r10.mClientDead = r8;
        r10.mService = r11;
        r10.mCallback = r12;
        r10.mClient = r13;
        r6 = android.os.Binder.getCallingUid();
        r10.mUid = r6;
        r6 = android.os.Binder.getCallingPid();
        r10.mPid = r6;
        r6 = r11.getCurrentAnimatorScale();
        r10.mLastReportedAnimatorScale = r6;
        r6 = r11.mContext;
        r9 = "android.permission.INTERNAL_SYSTEM_WINDOW";
        r6 = r6.checkCallingOrSelfPermission(r9);
        if (r6 != 0) goto L_0x00e4;
    L_0x003a:
        r6 = r7;
    L_0x003b:
        r10.mCanAddInternalSystemWindow = r6;
        r6 = r11.mContext;
        r9 = "android.permission.HIDE_NON_SYSTEM_OVERLAY_WINDOWS";
        r6 = r6.checkCallingOrSelfPermission(r9);
        if (r6 != 0) goto L_0x00e7;
    L_0x0048:
        r6 = r7;
    L_0x0049:
        r10.mCanHideNonSystemOverlayWindows = r6;
        r6 = r11.mContext;
        r9 = "android.permission.DEVICE_POWER";
        r6 = r6.checkCallingOrSelfPermission(r9);
        if (r6 != 0) goto L_0x00ea;
    L_0x0056:
        r10.mCanAcquireSleepToken = r7;
        r6 = r10.mService;
        r6 = r6.mShowAlertWindowNotifications;
        r10.mShowingAlertWindowNotificationAllowed = r6;
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r6 = "Session{";
        r3.append(r6);
        r6 = java.lang.System.identityHashCode(r10);
        r6 = java.lang.Integer.toHexString(r6);
        r3.append(r6);
        r6 = " ";
        r3.append(r6);
        r6 = r10.mPid;
        r3.append(r6);
        r6 = r10.mUid;
        r7 = 10000; // 0x2710 float:1.4013E-41 double:4.9407E-320;
        if (r6 >= r7) goto L_0x00ed;
    L_0x0085:
        r6 = ":";
        r3.append(r6);
        r6 = r10.mUid;
        r3.append(r6);
    L_0x0090:
        r6 = "}";
        r3.append(r6);
        r6 = r3.toString();
        r10.mStringName = r6;
        r6 = r10.mService;
        r7 = r6.mWindowMap;
        monitor-enter(r7);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x010b }
        r6 = r10.mService;	 Catch:{ all -> 0x010b }
        r6 = r6.mInputMethodManager;	 Catch:{ all -> 0x010b }
        if (r6 != 0) goto L_0x00bf;
    L_0x00aa:
        r6 = r10.mService;	 Catch:{ all -> 0x010b }
        r6 = r6.mHaveInputMethods;	 Catch:{ all -> 0x010b }
        if (r6 == 0) goto L_0x00bf;
    L_0x00b0:
        r6 = "input_method";
        r0 = android.os.ServiceManager.getService(r6);	 Catch:{ all -> 0x010b }
        r6 = r10.mService;	 Catch:{ all -> 0x010b }
        r8 = com.android.internal.view.IInputMethodManager.Stub.asInterface(r0);	 Catch:{ all -> 0x010b }
        r6.mInputMethodManager = r8;	 Catch:{ all -> 0x010b }
    L_0x00bf:
        monitor-exit(r7);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        r4 = android.os.Binder.clearCallingIdentity();
        r6 = r10.mService;	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        r6 = r6.mInputMethodManager;	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        if (r6 == 0) goto L_0x0111;
    L_0x00cd:
        r6 = r10.mService;	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        r6 = r6.mInputMethodManager;	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        r7 = r10.mUid;	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        r8 = r10.mPid;	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        r6.addClient(r13, r14, r7, r8);	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
    L_0x00d8:
        r6 = r13.asBinder();	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        r7 = 0;
        r6.linkToDeath(r10, r7);	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        android.os.Binder.restoreCallingIdentity(r4);
    L_0x00e3:
        return;
    L_0x00e4:
        r6 = r8;
        goto L_0x003b;
    L_0x00e7:
        r6 = r8;
        goto L_0x0049;
    L_0x00ea:
        r7 = r8;
        goto L_0x0056;
    L_0x00ed:
        r6 = ":u";
        r3.append(r6);
        r6 = r10.mUid;
        r6 = android.os.UserHandle.getUserId(r6);
        r3.append(r6);
        r6 = 97;
        r3.append(r6);
        r6 = r10.mUid;
        r6 = android.os.UserHandle.getAppId(r6);
        r3.append(r6);
        goto L_0x0090;
    L_0x010b:
        r6 = move-exception;
        monitor-exit(r7);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r6;
    L_0x0111:
        r6 = 0;
        r13.setUsingInputMethod(r6);	 Catch:{ RemoteException -> 0x0116, all -> 0x0128 }
        goto L_0x00d8;
    L_0x0116:
        r1 = move-exception;
        r6 = r10.mService;	 Catch:{ RemoteException -> 0x012d, all -> 0x0128 }
        r6 = r6.mInputMethodManager;	 Catch:{ RemoteException -> 0x012d, all -> 0x0128 }
        if (r6 == 0) goto L_0x0124;
    L_0x011d:
        r6 = r10.mService;	 Catch:{ RemoteException -> 0x012d, all -> 0x0128 }
        r6 = r6.mInputMethodManager;	 Catch:{ RemoteException -> 0x012d, all -> 0x0128 }
        r6.removeClient(r13);	 Catch:{ RemoteException -> 0x012d, all -> 0x0128 }
    L_0x0124:
        android.os.Binder.restoreCallingIdentity(r4);
        goto L_0x00e3;
    L_0x0128:
        r6 = move-exception;
        android.os.Binder.restoreCallingIdentity(r4);
        throw r6;
    L_0x012d:
        r2 = move-exception;
        goto L_0x0124;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.Session.<init>(com.android.server.wm.WindowManagerService, android.view.IWindowSessionCallback, com.android.internal.view.IInputMethodClient, com.android.internal.view.IInputContext):void");
    }

    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        try {
            return super.onTransact(code, data, reply, flags);
        } catch (RuntimeException e) {
            if (!(e instanceof SecurityException)) {
                Slog.wtf("WindowManager", "Window Session Crash", e);
            }
            throw e;
        }
    }

    public void binderDied() {
        try {
            if (this.mService.mInputMethodManager != null) {
                this.mService.mInputMethodManager.removeClient(this.mClient);
            }
        } catch (RemoteException e) {
        }
        synchronized (this.mService.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                this.mClient.asBinder().unlinkToDeath(this, 0);
                this.mClientDead = true;
                killSessionLocked();
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public int add(IWindow window, int seq, LayoutParams attrs, int viewVisibility, Rect outContentInsets, Rect outStableInsets, InputChannel outInputChannel) {
        return addToDisplay(window, seq, attrs, viewVisibility, 0, outContentInsets, outStableInsets, null, outInputChannel);
    }

    public int addToDisplay(IWindow window, int seq, LayoutParams attrs, int viewVisibility, int displayId, Rect outContentInsets, Rect outStableInsets, Rect outOutsets, InputChannel outInputChannel) {
        return this.mService.addWindow(this, window, seq, attrs, viewVisibility, displayId, outContentInsets, outStableInsets, outOutsets, outInputChannel);
    }

    public int addWithoutInputChannel(IWindow window, int seq, LayoutParams attrs, int viewVisibility, Rect outContentInsets, Rect outStableInsets) {
        return addToDisplayWithoutInputChannel(window, seq, attrs, viewVisibility, 0, outContentInsets, outStableInsets);
    }

    public int addToDisplayWithoutInputChannel(IWindow window, int seq, LayoutParams attrs, int viewVisibility, int displayId, Rect outContentInsets, Rect outStableInsets) {
        return this.mService.addWindow(this, window, seq, attrs, viewVisibility, displayId, outContentInsets, outStableInsets, null, null);
    }

    public void remove(IWindow window) {
        this.mService.removeWindow(this, window);
    }

    public void prepareToReplaceWindows(IBinder appToken, boolean childrenOnly) {
        this.mService.setWillReplaceWindows(appToken, childrenOnly);
    }

    public int relayout(IWindow window, int seq, LayoutParams attrs, int requestedWidth, int requestedHeight, int viewFlags, int flags, Rect outFrame, Rect outOverscanInsets, Rect outContentInsets, Rect outVisibleInsets, Rect outStableInsets, Rect outsets, Rect outBackdropFrame, MergedConfiguration mergedConfiguration, Surface outSurface) {
        Trace.traceBegin(32, this.mRelayoutTag);
        int res = this.mService.relayoutWindow(this, window, seq, attrs, requestedWidth, requestedHeight, viewFlags, flags, outFrame, outOverscanInsets, outContentInsets, outVisibleInsets, outStableInsets, outsets, outBackdropFrame, mergedConfiguration, outSurface);
        Trace.traceEnd(32);
        return res;
    }

    public boolean outOfMemory(IWindow window) {
        return this.mService.outOfMemoryWindow(this, window);
    }

    public void setTransparentRegion(IWindow window, Region region) {
        this.mService.setTransparentRegionWindow(this, window, region);
    }

    public void setInsets(IWindow window, int touchableInsets, Rect contentInsets, Rect visibleInsets, Region touchableArea) {
        this.mService.setInsetsWindow(this, window, touchableInsets, contentInsets, visibleInsets, touchableArea);
    }

    public void getDisplayFrame(IWindow window, Rect outDisplayFrame) {
        this.mService.getWindowDisplayFrame(this, window, outDisplayFrame);
    }

    public void finishDrawing(IWindow window) {
        this.mService.finishDrawingWindow(this, window);
    }

    public void setInTouchMode(boolean mode) {
        synchronized (this.mService.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                this.mService.mInTouchMode = mode;
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public boolean getInTouchMode() {
        boolean z;
        synchronized (this.mService.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                z = this.mService.mInTouchMode;
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        return z;
    }

    public boolean performHapticFeedback(IWindow window, int effectId, boolean always) {
        boolean performHapticFeedbackLw;
        synchronized (this.mService.mWindowMap) {
            long ident;
            try {
                WindowManagerService.boostPriorityForLockedSection();
                ident = Binder.clearCallingIdentity();
                performHapticFeedbackLw = this.mService.mPolicy.performHapticFeedbackLw(this.mService.windowForClientLocked(this, window, true), effectId, always);
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        WindowManagerService.resetPriorityAfterLockedSection();
        return performHapticFeedbackLw;
    }

    public IBinder prepareDrag(IWindow window, int flags, int width, int height, Surface outSurface) {
        return this.mService.prepareDragSurface(window, this.mSurfaceSession, flags, width, height, outSurface);
    }

    public boolean performDrag(IWindow window, IBinder dragToken, int touchSource, float touchX, float touchY, float thumbCenterX, float thumbCenterY, ClipData data) {
        synchronized (this.mService.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mService.mDragState == null) {
                    Slog.w("WindowManager", "No drag prepared");
                    throw new IllegalStateException("performDrag() without prepareDrag()");
                } else if (dragToken != this.mService.mDragState.mToken) {
                    Slog.w("WindowManager", "Performing mismatched drag");
                    throw new IllegalStateException("performDrag() does not match prepareDrag()");
                } else {
                    WindowState callingWin = this.mService.windowForClientLocked(null, window, false);
                    if (callingWin == null) {
                        Slog.w("WindowManager", "Bad requesting window " + window);
                        WindowManagerService.resetPriorityAfterLockedSection();
                        return false;
                    }
                    this.mService.mH.removeMessages(20, window.asBinder());
                    DisplayContent displayContent = callingWin.getDisplayContent();
                    if (displayContent == null) {
                        WindowManagerService.resetPriorityAfterLockedSection();
                        return false;
                    }
                    Display display = displayContent.getDisplay();
                    this.mService.mDragState.register(display);
                    if (this.mService.mInputManager.transferTouchFocus(callingWin.mInputChannel, this.mService.mDragState.getInputChannel())) {
                        this.mService.mDragState.mDisplayContent = displayContent;
                        this.mService.mDragState.mData = data;
                        this.mService.mDragState.broadcastDragStartedLw(touchX, touchY);
                        this.mService.mDragState.overridePointerIconLw(touchSource);
                        this.mService.mDragState.mThumbOffsetX = thumbCenterX;
                        this.mService.mDragState.mThumbOffsetY = thumbCenterY;
                        SurfaceControl surfaceControl = this.mService.mDragState.mSurfaceControl;
                        this.mService.openSurfaceTransaction();
                        surfaceControl.setPosition(touchX - thumbCenterX, touchY - thumbCenterY);
                        surfaceControl.setLayer(this.mService.mDragState.getDragLayerLw());
                        surfaceControl.setLayerStack(display.getLayerStack());
                        surfaceControl.show();
                        this.mService.closeSurfaceTransaction();
                        this.mService.mDragState.notifyLocationLw(touchX, touchY);
                        WindowManagerService.resetPriorityAfterLockedSection();
                        return true;
                    }
                    Slog.e("WindowManager", "Unable to transfer touch focus");
                    this.mService.mDragState.unregister();
                    this.mService.mDragState.reset();
                    this.mService.mDragState = null;
                    WindowManagerService.resetPriorityAfterLockedSection();
                    return false;
                }
            } catch (Throwable th) {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public boolean startMovingTask(IWindow window, float startX, float startY) {
        long ident = Binder.clearCallingIdentity();
        try {
            boolean startMovingTask = this.mService.startMovingTask(window, startX, startY);
            return startMovingTask;
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    public void reportDropResult(IWindow window, boolean consumed) {
        IBinder token = window.asBinder();
        synchronized (this.mService.mWindowMap) {
            long ident;
            try {
                WindowManagerService.boostPriorityForLockedSection();
                ident = Binder.clearCallingIdentity();
                if (this.mService.mDragState == null) {
                    Slog.w("WindowManager", "Drop result given but no drag in progress");
                    Binder.restoreCallingIdentity(ident);
                    WindowManagerService.resetPriorityAfterLockedSection();
                } else if (this.mService.mDragState.mToken != token) {
                    Slog.w("WindowManager", "Invalid drop-result claim by " + window);
                    throw new IllegalStateException("reportDropResult() by non-recipient");
                } else {
                    this.mService.mH.removeMessages(21, window.asBinder());
                    if (this.mService.windowForClientLocked(null, window, false) == null) {
                        Slog.w("WindowManager", "Bad result-reporting window " + window);
                        Binder.restoreCallingIdentity(ident);
                        WindowManagerService.resetPriorityAfterLockedSection();
                        return;
                    }
                    this.mService.mDragState.mDragResult = consumed;
                    this.mService.mDragState.endDragLw();
                    Binder.restoreCallingIdentity(ident);
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            } catch (Throwable th) {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public void cancelDragAndDrop(IBinder dragToken) {
        synchronized (this.mService.mWindowMap) {
            long ident;
            try {
                WindowManagerService.boostPriorityForLockedSection();
                ident = Binder.clearCallingIdentity();
                if (this.mService.mDragState == null) {
                    Slog.w("WindowManager", "cancelDragAndDrop() without prepareDrag()");
                    throw new IllegalStateException("cancelDragAndDrop() without prepareDrag()");
                } else if (this.mService.mDragState.mToken != dragToken) {
                    Slog.w("WindowManager", "cancelDragAndDrop() does not match prepareDrag()");
                    throw new IllegalStateException("cancelDragAndDrop() does not match prepareDrag()");
                } else {
                    this.mService.mDragState.mDragResult = false;
                    this.mService.mDragState.cancelDragLw();
                    Binder.restoreCallingIdentity(ident);
                }
            } catch (Throwable th) {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        WindowManagerService.resetPriorityAfterLockedSection();
    }

    public void dragRecipientEntered(IWindow window) {
    }

    public void dragRecipientExited(IWindow window) {
    }

    public void setWallpaperPosition(IBinder window, float x, float y, float xStep, float yStep) {
        synchronized (this.mService.mWindowMap) {
            long ident;
            try {
                WindowManagerService.boostPriorityForLockedSection();
                ident = Binder.clearCallingIdentity();
                this.mService.mRoot.mWallpaperController.setWindowWallpaperPosition(this.mService.windowForClientLocked(this, window, true), x, y, xStep, yStep);
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        WindowManagerService.resetPriorityAfterLockedSection();
    }

    public void wallpaperOffsetsComplete(IBinder window) {
        synchronized (this.mService.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                this.mService.mRoot.mWallpaperController.wallpaperOffsetsComplete(window);
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public void setWallpaperDisplayOffset(IBinder window, int x, int y) {
        synchronized (this.mService.mWindowMap) {
            long ident;
            try {
                WindowManagerService.boostPriorityForLockedSection();
                ident = Binder.clearCallingIdentity();
                this.mService.mRoot.mWallpaperController.setWindowWallpaperDisplayOffset(this.mService.windowForClientLocked(this, window, true), x, y);
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        WindowManagerService.resetPriorityAfterLockedSection();
    }

    public Bundle sendWallpaperCommand(IBinder window, String action, int x, int y, int z, Bundle extras, boolean sync) {
        Bundle sendWindowWallpaperCommand;
        synchronized (this.mService.mWindowMap) {
            long ident;
            try {
                WindowManagerService.boostPriorityForLockedSection();
                ident = Binder.clearCallingIdentity();
                sendWindowWallpaperCommand = this.mService.mRoot.mWallpaperController.sendWindowWallpaperCommand(this.mService.windowForClientLocked(this, window, true), action, x, y, z, extras, sync);
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        WindowManagerService.resetPriorityAfterLockedSection();
        return sendWindowWallpaperCommand;
    }

    public void wallpaperCommandComplete(IBinder window, Bundle result) {
        synchronized (this.mService.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                this.mService.mRoot.mWallpaperController.wallpaperCommandComplete(window);
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public void onRectangleOnScreenRequested(IBinder token, Rect rectangle) {
        synchronized (this.mService.mWindowMap) {
            long identity;
            try {
                WindowManagerService.boostPriorityForLockedSection();
                identity = Binder.clearCallingIdentity();
                this.mService.onRectangleOnScreenRequested(token, rectangle);
                Binder.restoreCallingIdentity(identity);
            } catch (Throwable th) {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        WindowManagerService.resetPriorityAfterLockedSection();
    }

    public IWindowId getWindowId(IBinder window) {
        return this.mService.getWindowId(window);
    }

    public void pokeDrawLock(IBinder window) {
        long identity = Binder.clearCallingIdentity();
        try {
            this.mService.pokeDrawLock(this, window);
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    public void updatePointerIcon(IWindow window) {
        long identity = Binder.clearCallingIdentity();
        try {
            this.mService.updatePointerIcon(window);
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    void windowAddedLocked(String packageName) {
        this.mPackageName = packageName;
        this.mRelayoutTag = "relayoutWindow: " + this.mPackageName;
        if (this.mSurfaceSession == null) {
            this.mSurfaceSession = new SurfaceSession();
            this.mService.mSessions.add(this);
            if (this.mLastReportedAnimatorScale != this.mService.getCurrentAnimatorScale()) {
                this.mService.dispatchNewAnimatorScaleLocked(this);
            }
        }
        this.mNumWindow++;
    }

    void windowRemovedLocked() {
        this.mNumWindow--;
        killSessionLocked();
    }

    void onWindowSurfaceVisibilityChanged(WindowSurfaceController surfaceController, boolean visible, int type) {
        if (LayoutParams.isSystemAlertWindowType(type)) {
            boolean changed;
            if (!this.mCanAddInternalSystemWindow) {
                if (visible) {
                    changed = this.mAlertWindowSurfaces.add(surfaceController);
                } else {
                    changed = this.mAlertWindowSurfaces.remove(surfaceController);
                }
                if (changed) {
                    if (this.mAlertWindowSurfaces.isEmpty()) {
                        cancelAlertWindowNotification();
                    } else if (this.mAlertWindowNotification == null) {
                        this.mAlertWindowNotification = new AlertWindowNotification(this.mService, this.mPackageName);
                        if (this.mShowingAlertWindowNotificationAllowed) {
                            this.mAlertWindowNotification.post();
                        }
                    }
                }
            }
            if (type == 2038) {
                if (visible) {
                    changed = this.mAppOverlaySurfaces.add(surfaceController);
                } else {
                    changed = this.mAppOverlaySurfaces.remove(surfaceController);
                }
                if (changed) {
                    setHasOverlayUi(this.mAppOverlaySurfaces.isEmpty() ^ 1);
                }
            }
        }
    }

    void setShowingAlertWindowNotificationAllowed(boolean allowed) {
        this.mShowingAlertWindowNotificationAllowed = allowed;
        if (this.mAlertWindowNotification == null) {
            return;
        }
        if (allowed) {
            this.mAlertWindowNotification.post();
        } else {
            this.mAlertWindowNotification.cancel();
        }
    }

    private void killSessionLocked() {
        if (this.mNumWindow <= 0 && (this.mClientDead ^ 1) == 0) {
            this.mService.mSessions.remove(this);
            if (this.mSurfaceSession != null) {
                try {
                    this.mSurfaceSession.kill();
                } catch (Exception e) {
                    Slog.w("WindowManager", "Exception thrown when killing surface session " + this.mSurfaceSession + " in session " + this + ": " + e.toString());
                }
                this.mSurfaceSession = null;
                this.mAlertWindowSurfaces.clear();
                this.mAppOverlaySurfaces.clear();
                setHasOverlayUi(false);
                cancelAlertWindowNotification();
            }
        }
    }

    private void setHasOverlayUi(boolean hasOverlayUi) {
        this.mService.mH.obtainMessage(58, this.mPid, hasOverlayUi ? 1 : 0).sendToTarget();
    }

    private void cancelAlertWindowNotification() {
        if (this.mAlertWindowNotification != null) {
            this.mAlertWindowNotification.cancel();
            this.mAlertWindowNotification = null;
        }
    }

    void dump(PrintWriter pw, String prefix) {
        pw.print(prefix);
        pw.print("mNumWindow=");
        pw.print(this.mNumWindow);
        pw.print(" mCanAddInternalSystemWindow=");
        pw.print(this.mCanAddInternalSystemWindow);
        pw.print(" mAppOverlaySurfaces=");
        pw.print(this.mAppOverlaySurfaces);
        pw.print(" mAlertWindowSurfaces=");
        pw.print(this.mAlertWindowSurfaces);
        pw.print(" mClientDead=");
        pw.print(this.mClientDead);
        pw.print(" mSurfaceSession=");
        pw.println(this.mSurfaceSession);
        pw.print(prefix);
        pw.print("mPackageName=");
        pw.println(this.mPackageName);
    }

    public String toString() {
        return this.mStringName;
    }

    boolean hasAlertWindowSurfaces() {
        return this.mAlertWindowSurfaces.isEmpty() ^ 1;
    }
}
