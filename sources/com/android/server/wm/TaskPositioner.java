package com.android.server.wm;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Looper;
import android.os.Process;
import android.util.DisplayMetrics;
import android.util.Slog;
import android.view.BatchedInputEventReceiver;
import android.view.Choreographer;
import android.view.Display;
import android.view.DisplayInfo;
import android.view.InputChannel;
import com.android.server.input.InputApplicationHandle;
import com.android.server.input.InputWindowHandle;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

class TaskPositioner implements DimLayerUser {
    private static final int CTRL_BOTTOM = 8;
    private static final int CTRL_LEFT = 1;
    private static final int CTRL_NONE = 0;
    private static final int CTRL_RIGHT = 2;
    private static final int CTRL_TOP = 4;
    private static final boolean DEBUG_ORIENTATION_VIOLATIONS = false;
    static final float MIN_ASPECT = 1.2f;
    public static final float RESIZING_HINT_ALPHA = 0.5f;
    public static final int RESIZING_HINT_DURATION_MS = 0;
    static final int SIDE_MARGIN_DIP = 100;
    private static final String TAG = "WindowManager";
    private static final String TAG_LOCAL = "TaskPositioner";
    InputChannel mClientChannel;
    private int mCtrlType = 0;
    private int mCurrentDimSide;
    private DimLayer mDimLayer;
    private Display mDisplay;
    private final DisplayMetrics mDisplayMetrics = new DisplayMetrics();
    InputApplicationHandle mDragApplicationHandle;
    private boolean mDragEnded = false;
    InputWindowHandle mDragWindowHandle;
    private WindowPositionerEventReceiver mInputEventReceiver;
    private final Point mMaxVisibleSize = new Point();
    private int mMinVisibleHeight;
    private int mMinVisibleWidth;
    private boolean mPreserveOrientation;
    private boolean mResizing;
    InputChannel mServerChannel;
    private final WindowManagerService mService;
    private int mSideMargin;
    private float mStartDragX;
    private float mStartDragY;
    private boolean mStartOrientationWasLandscape;
    private Task mTask;
    private Rect mTmpRect = new Rect();
    private final Rect mWindowDragBounds = new Rect();
    private final Rect mWindowOriginalBounds = new Rect();

    @Retention(RetentionPolicy.SOURCE)
    @interface CtrlType {
    }

    private final class WindowPositionerEventReceiver extends BatchedInputEventReceiver {
        public WindowPositionerEventReceiver(InputChannel inputChannel, Looper looper, Choreographer choreographer) {
            super(inputChannel, looper, choreographer);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onInputEvent(android.view.InputEvent r14, int r15) {
            /*
            r13 = this;
            r0 = r14 instanceof android.view.MotionEvent;
            if (r0 == 0) goto L_0x000c;
        L_0x0004:
            r0 = r14.getSource();
            r0 = r0 & 2;
            if (r0 != 0) goto L_0x000d;
        L_0x000c:
            return;
        L_0x000d:
            r9 = r14;
            r9 = (android.view.MotionEvent) r9;
            r8 = 0;
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r0 = r0.mDragEnded;	 Catch:{ Exception -> 0x012e }
            if (r0 == 0) goto L_0x001e;
        L_0x0019:
            r8 = 1;
            r13.finishInputEvent(r14, r8);
            return;
        L_0x001e:
            r10 = r9.getRawX();	 Catch:{ Exception -> 0x012e }
            r11 = r9.getRawY();	 Catch:{ Exception -> 0x012e }
            r0 = r9.getAction();	 Catch:{ Exception -> 0x012e }
            switch(r0) {
                case 0: goto L_0x002d;
                case 1: goto L_0x0148;
                case 2: goto L_0x00c9;
                case 3: goto L_0x0150;
                default: goto L_0x002d;
            };	 Catch:{ Exception -> 0x012e }
        L_0x002d:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r0 = r0.mDragEnded;	 Catch:{ Exception -> 0x012e }
            if (r0 == 0) goto L_0x00c4;
        L_0x0035:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r12 = r0.mResizing;	 Catch:{ Exception -> 0x012e }
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r0 = r0.mService;	 Catch:{ Exception -> 0x012e }
            r1 = r0.mWindowMap;	 Catch:{ Exception -> 0x012e }
            monitor-enter(r1);	 Catch:{ Exception -> 0x012e }
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0158 }
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ all -> 0x0158 }
            r0.endDragLocked();	 Catch:{ all -> 0x0158 }
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ all -> 0x0158 }
            r0 = r0.mTask;	 Catch:{ all -> 0x0158 }
            r3 = com.android.server.wm.TaskPositioner.this;	 Catch:{ all -> 0x0158 }
            r3 = r3.mTmpRect;	 Catch:{ all -> 0x0158 }
            r0.getDimBounds(r3);	 Catch:{ all -> 0x0158 }
            monitor-exit(r1);	 Catch:{ Exception -> 0x012e }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x012e }
            if (r12 == 0) goto L_0x008f;
        L_0x0061:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0.mTmpRect;	 Catch:{ RemoteException -> 0x0161 }
            r1 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r1 = r1.mWindowDragBounds;	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0.equals(r1);	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0 ^ 1;
            if (r0 == 0) goto L_0x008f;
        L_0x0075:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0.mService;	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0.mActivityManager;	 Catch:{ RemoteException -> 0x0161 }
            r1 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r1 = r1.mTask;	 Catch:{ RemoteException -> 0x0161 }
            r1 = r1.mTaskId;	 Catch:{ RemoteException -> 0x0161 }
            r3 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r3 = r3.mWindowDragBounds;	 Catch:{ RemoteException -> 0x0161 }
            r4 = 3;
            r0.resizeTask(r1, r3, r4);	 Catch:{ RemoteException -> 0x0161 }
        L_0x008f:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0.mCurrentDimSide;	 Catch:{ RemoteException -> 0x0161 }
            if (r0 == 0) goto L_0x00b7;
        L_0x0097:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0.mCurrentDimSide;	 Catch:{ RemoteException -> 0x0161 }
            r1 = 1;
            if (r0 != r1) goto L_0x015e;
        L_0x00a0:
            r2 = 0;
        L_0x00a1:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0.mService;	 Catch:{ RemoteException -> 0x0161 }
            r0 = r0.mActivityManager;	 Catch:{ RemoteException -> 0x0161 }
            r1 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0161 }
            r1 = r1.mTask;	 Catch:{ RemoteException -> 0x0161 }
            r1 = r1.mTaskId;	 Catch:{ RemoteException -> 0x0161 }
            r3 = 1;
            r4 = 1;
            r5 = 0;
            r0.moveTaskToDockedStack(r1, r2, r3, r4, r5);	 Catch:{ RemoteException -> 0x0161 }
        L_0x00b7:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r0 = r0.mService;	 Catch:{ Exception -> 0x012e }
            r0 = r0.mH;	 Catch:{ Exception -> 0x012e }
            r1 = 40;
            r0.sendEmptyMessage(r1);	 Catch:{ Exception -> 0x012e }
        L_0x00c4:
            r8 = 1;
            r13.finishInputEvent(r14, r8);
        L_0x00c8:
            return;
        L_0x00c9:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r0 = r0.mService;	 Catch:{ Exception -> 0x012e }
            r1 = r0.mWindowMap;	 Catch:{ Exception -> 0x012e }
            monitor-enter(r1);	 Catch:{ Exception -> 0x012e }
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x013d }
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ all -> 0x013d }
            r3 = com.android.server.wm.TaskPositioner.this;	 Catch:{ all -> 0x013d }
            r3 = r3.notifyMoveLocked(r10, r11);	 Catch:{ all -> 0x013d }
            r0.mDragEnded = r3;	 Catch:{ all -> 0x013d }
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ all -> 0x013d }
            r0 = r0.mTask;	 Catch:{ all -> 0x013d }
            r3 = com.android.server.wm.TaskPositioner.this;	 Catch:{ all -> 0x013d }
            r3 = r3.mTmpRect;	 Catch:{ all -> 0x013d }
            r0.getDimBounds(r3);	 Catch:{ all -> 0x013d }
            monitor-exit(r1);	 Catch:{ Exception -> 0x012e }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x012e }
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r0 = r0.mTmpRect;	 Catch:{ Exception -> 0x012e }
            r1 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r1 = r1.mWindowDragBounds;	 Catch:{ Exception -> 0x012e }
            r0 = r0.equals(r1);	 Catch:{ Exception -> 0x012e }
            if (r0 != 0) goto L_0x002d;
        L_0x0105:
            r0 = "wm.TaskPositioner.resizeTask";
            r4 = 32;
            android.os.Trace.traceBegin(r4, r0);	 Catch:{ Exception -> 0x012e }
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0164 }
            r0 = r0.mService;	 Catch:{ RemoteException -> 0x0164 }
            r0 = r0.mActivityManager;	 Catch:{ RemoteException -> 0x0164 }
            r1 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0164 }
            r1 = r1.mTask;	 Catch:{ RemoteException -> 0x0164 }
            r1 = r1.mTaskId;	 Catch:{ RemoteException -> 0x0164 }
            r3 = com.android.server.wm.TaskPositioner.this;	 Catch:{ RemoteException -> 0x0164 }
            r3 = r3.mWindowDragBounds;	 Catch:{ RemoteException -> 0x0164 }
            r4 = 1;
            r0.resizeTask(r1, r3, r4);	 Catch:{ RemoteException -> 0x0164 }
        L_0x0127:
            r0 = 32;
            android.os.Trace.traceEnd(r0);	 Catch:{ Exception -> 0x012e }
            goto L_0x002d;
        L_0x012e:
            r7 = move-exception;
            r0 = com.android.server.wm.TaskPositioner.TAG;	 Catch:{ all -> 0x0143 }
            r1 = "Exception caught by drag handleMotion";
            android.util.Slog.e(r0, r1, r7);	 Catch:{ all -> 0x0143 }
            r13.finishInputEvent(r14, r8);
            goto L_0x00c8;
        L_0x013d:
            r0 = move-exception;
            monitor-exit(r1);	 Catch:{ Exception -> 0x012e }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x012e }
            throw r0;	 Catch:{ Exception -> 0x012e }
        L_0x0143:
            r0 = move-exception;
            r13.finishInputEvent(r14, r8);
            throw r0;
        L_0x0148:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r1 = 1;
            r0.mDragEnded = r1;	 Catch:{ Exception -> 0x012e }
            goto L_0x002d;
        L_0x0150:
            r0 = com.android.server.wm.TaskPositioner.this;	 Catch:{ Exception -> 0x012e }
            r1 = 1;
            r0.mDragEnded = r1;	 Catch:{ Exception -> 0x012e }
            goto L_0x002d;
        L_0x0158:
            r0 = move-exception;
            monitor-exit(r1);	 Catch:{ Exception -> 0x012e }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x012e }
            throw r0;	 Catch:{ Exception -> 0x012e }
        L_0x015e:
            r2 = 1;
            goto L_0x00a1;
        L_0x0161:
            r6 = move-exception;
            goto L_0x00b7;
        L_0x0164:
            r6 = move-exception;
            goto L_0x0127;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskPositioner.WindowPositionerEventReceiver.onInputEvent(android.view.InputEvent, int):void");
        }
    }

    TaskPositioner(WindowManagerService service) {
        this.mService = service;
    }

    Rect getWindowDragBounds() {
        return this.mWindowDragBounds;
    }

    void register(Display display) {
        if (this.mClientChannel != null) {
            Slog.e(TAG, "Task positioner already registered");
            return;
        }
        this.mDisplay = display;
        this.mDisplay.getMetrics(this.mDisplayMetrics);
        InputChannel[] channels = InputChannel.openInputChannelPair(TAG);
        this.mServerChannel = channels[0];
        this.mClientChannel = channels[1];
        this.mService.mInputManager.registerInputChannel(this.mServerChannel, null);
        this.mInputEventReceiver = new WindowPositionerEventReceiver(this.mClientChannel, this.mService.mAnimationHandler.getLooper(), this.mService.mAnimator.getChoreographer());
        this.mDragApplicationHandle = new InputApplicationHandle(null);
        this.mDragApplicationHandle.name = TAG;
        this.mDragApplicationHandle.dispatchingTimeoutNanos = 5000000000L;
        this.mDragWindowHandle = new InputWindowHandle(this.mDragApplicationHandle, null, null, this.mDisplay.getDisplayId());
        this.mDragWindowHandle.name = TAG;
        this.mDragWindowHandle.inputChannel = this.mServerChannel;
        this.mDragWindowHandle.layer = this.mService.getDragLayerLocked();
        this.mDragWindowHandle.layoutParamsFlags = 0;
        this.mDragWindowHandle.layoutParamsType = 2016;
        this.mDragWindowHandle.dispatchingTimeoutNanos = 5000000000L;
        this.mDragWindowHandle.visible = true;
        this.mDragWindowHandle.canReceiveKeys = false;
        this.mDragWindowHandle.hasFocus = true;
        this.mDragWindowHandle.hasWallpaper = false;
        this.mDragWindowHandle.paused = false;
        this.mDragWindowHandle.ownerPid = Process.myPid();
        this.mDragWindowHandle.ownerUid = Process.myUid();
        this.mDragWindowHandle.inputFeatures = 0;
        this.mDragWindowHandle.scaleFactor = 1.0f;
        this.mDragWindowHandle.touchableRegion.setEmpty();
        this.mDragWindowHandle.frameLeft = 0;
        this.mDragWindowHandle.frameTop = 0;
        Point p = new Point();
        this.mDisplay.getRealSize(p);
        this.mDragWindowHandle.frameRight = p.x;
        this.mDragWindowHandle.frameBottom = p.y;
        this.mService.pauseRotationLocked();
        this.mDimLayer = new DimLayer(this.mService, this, this.mDisplay.getDisplayId(), TAG_LOCAL);
        this.mSideMargin = WindowManagerService.dipToPixel(100, this.mDisplayMetrics);
        this.mMinVisibleWidth = WindowManagerService.dipToPixel(48, this.mDisplayMetrics);
        this.mMinVisibleHeight = WindowManagerService.dipToPixel(32, this.mDisplayMetrics);
        this.mDisplay.getRealSize(this.mMaxVisibleSize);
        this.mDragEnded = false;
    }

    void unregister() {
        if (this.mClientChannel == null) {
            Slog.e(TAG, "Task positioner not registered");
            return;
        }
        this.mService.mInputManager.unregisterInputChannel(this.mServerChannel);
        this.mInputEventReceiver.dispose();
        this.mInputEventReceiver = null;
        this.mClientChannel.dispose();
        this.mServerChannel.dispose();
        this.mClientChannel = null;
        this.mServerChannel = null;
        this.mDragWindowHandle = null;
        this.mDragApplicationHandle = null;
        this.mDisplay = null;
        if (this.mDimLayer != null) {
            this.mDimLayer.destroySurface();
            this.mDimLayer = null;
        }
        this.mCurrentDimSide = 0;
        this.mDragEnded = true;
        this.mService.resumeRotationLocked();
    }

    void startDrag(WindowState win, boolean resize, boolean preserveOrientation, float startX, float startY) {
        this.mTask = win.getTask();
        this.mTask.getDimBounds(this.mTmpRect);
        startDrag(resize, preserveOrientation, startX, startY, this.mTmpRect);
    }

    void startDrag(boolean resize, boolean preserveOrientation, float startX, float startY, Rect startBounds) {
        boolean z = true;
        this.mCtrlType = 0;
        this.mStartDragX = startX;
        this.mStartDragY = startY;
        this.mPreserveOrientation = preserveOrientation;
        if (resize) {
            if (startX < ((float) startBounds.left)) {
                this.mCtrlType |= 1;
            }
            if (startX > ((float) startBounds.right)) {
                this.mCtrlType |= 2;
            }
            if (startY < ((float) startBounds.top)) {
                this.mCtrlType |= 4;
            }
            if (startY > ((float) startBounds.bottom)) {
                this.mCtrlType |= 8;
            }
            this.mResizing = this.mCtrlType != 0;
        }
        if (startBounds.width() < startBounds.height()) {
            z = false;
        }
        this.mStartOrientationWasLandscape = z;
        this.mWindowOriginalBounds.set(startBounds);
        this.mWindowDragBounds.set(startBounds);
    }

    private void endDragLocked() {
        this.mResizing = false;
        this.mTask.setDragResizing(false, 0);
    }

    private boolean notifyMoveLocked(float x, float y) {
        if (this.mCtrlType != 0) {
            resizeDrag(x, y);
            this.mTask.setDragResizing(true, 0);
            return false;
        }
        this.mTask.mStack.getDimBounds(this.mTmpRect);
        int nX = (int) x;
        int nY = (int) y;
        if (!this.mTmpRect.contains(nX, nY)) {
            nX = Math.min(Math.max(nX, this.mTmpRect.left), this.mTmpRect.right);
            nY = Math.min(Math.max(nY, this.mTmpRect.top), this.mTmpRect.bottom);
        }
        updateWindowDragBounds(nX, nY, this.mTmpRect);
        updateDimLayerVisibility(nX);
        return false;
    }

    void resizeDrag(float x, float y) {
        int deltaX = Math.round(x - this.mStartDragX);
        int deltaY = Math.round(y - this.mStartDragY);
        int left = this.mWindowOriginalBounds.left;
        int top = this.mWindowOriginalBounds.top;
        int right = this.mWindowOriginalBounds.right;
        int bottom = this.mWindowOriginalBounds.bottom;
        if (this.mPreserveOrientation) {
            if (this.mStartOrientationWasLandscape) {
            }
        }
        int width = right - left;
        int height = bottom - top;
        if ((this.mCtrlType & 1) != 0) {
            width = Math.max(this.mMinVisibleWidth, width - deltaX);
        } else if ((this.mCtrlType & 2) != 0) {
            width = Math.max(this.mMinVisibleWidth, width + deltaX);
        }
        if ((this.mCtrlType & 4) != 0) {
            height = Math.max(this.mMinVisibleHeight, height - deltaY);
        } else if ((this.mCtrlType & 8) != 0) {
            height = Math.max(this.mMinVisibleHeight, height + deltaY);
        }
        float aspect = ((float) width) / ((float) height);
        if (this.mPreserveOrientation) {
            int width1;
            int height1;
            int height2;
            int width2;
            if (!this.mStartOrientationWasLandscape || aspect >= MIN_ASPECT) {
                if (!this.mStartOrientationWasLandscape && ((double) aspect) > 0.8333333002196431d) {
                }
            }
            if (this.mStartOrientationWasLandscape) {
                width1 = Math.max(this.mMinVisibleWidth, Math.min(this.mMaxVisibleSize.x, width));
                height1 = Math.min(height, Math.round(((float) width1) / MIN_ASPECT));
                if (height1 < this.mMinVisibleHeight) {
                    height1 = this.mMinVisibleHeight;
                    width1 = Math.max(this.mMinVisibleWidth, Math.min(this.mMaxVisibleSize.x, Math.round(((float) height1) * MIN_ASPECT)));
                }
                height2 = Math.max(this.mMinVisibleHeight, Math.min(this.mMaxVisibleSize.y, height));
                width2 = Math.max(width, Math.round(((float) height2) * MIN_ASPECT));
                if (width2 < this.mMinVisibleWidth) {
                    width2 = this.mMinVisibleWidth;
                    height2 = Math.max(this.mMinVisibleHeight, Math.min(this.mMaxVisibleSize.y, Math.round(((float) width2) / MIN_ASPECT)));
                }
            } else {
                width1 = Math.max(this.mMinVisibleWidth, Math.min(this.mMaxVisibleSize.x, width));
                height1 = Math.max(height, Math.round(((float) width1) * MIN_ASPECT));
                if (height1 < this.mMinVisibleHeight) {
                    height1 = this.mMinVisibleHeight;
                    width1 = Math.max(this.mMinVisibleWidth, Math.min(this.mMaxVisibleSize.x, Math.round(((float) height1) / MIN_ASPECT)));
                }
                height2 = Math.max(this.mMinVisibleHeight, Math.min(this.mMaxVisibleSize.y, height));
                width2 = Math.min(width, Math.round(((float) height2) / MIN_ASPECT));
                if (width2 < this.mMinVisibleWidth) {
                    width2 = this.mMinVisibleWidth;
                    height2 = Math.max(this.mMinVisibleHeight, Math.min(this.mMaxVisibleSize.y, Math.round(((float) width2) * MIN_ASPECT)));
                }
            }
            boolean grows = width > right - left || height > bottom - top;
            if (grows == (width1 * height1 > width2 * height2)) {
                width = width1;
                height = height1;
            } else {
                width = width2;
                height = height2;
            }
        }
        updateDraggedBounds(left, top, right, bottom, width, height);
    }

    void updateDraggedBounds(int left, int top, int right, int bottom, int newWidth, int newHeight) {
        if ((this.mCtrlType & 1) != 0) {
            left = right - newWidth;
        } else {
            right = left + newWidth;
        }
        if ((this.mCtrlType & 4) != 0) {
            top = bottom - newHeight;
        } else {
            bottom = top + newHeight;
        }
        this.mWindowDragBounds.set(left, top, right, bottom);
        checkBoundsForOrientationViolations(this.mWindowDragBounds);
    }

    private void checkBoundsForOrientationViolations(Rect bounds) {
    }

    private void updateWindowDragBounds(int x, int y, Rect stackBounds) {
        int offsetX = Math.round(((float) x) - this.mStartDragX);
        int offsetY = Math.round(((float) y) - this.mStartDragY);
        this.mWindowDragBounds.set(this.mWindowOriginalBounds);
        int maxTop = stackBounds.bottom - this.mMinVisibleHeight;
        this.mWindowDragBounds.offsetTo(Math.min(Math.max(this.mWindowOriginalBounds.left + offsetX, (stackBounds.left + this.mMinVisibleWidth) - this.mWindowOriginalBounds.width()), stackBounds.right - this.mMinVisibleWidth), Math.min(Math.max(this.mWindowOriginalBounds.top + offsetY, stackBounds.top), maxTop));
    }

    private void updateDimLayerVisibility(int x) {
        int dimSide = getDimSide(x);
        if (dimSide != this.mCurrentDimSide) {
            this.mCurrentDimSide = dimSide;
            this.mService.openSurfaceTransaction();
            if (this.mCurrentDimSide == 0) {
                this.mDimLayer.hide();
            } else {
                showDimLayer();
            }
            this.mService.closeSurfaceTransaction();
        }
    }

    private int getDimSide(int x) {
        if (this.mTask.mStack.mStackId != 2 || (this.mTask.mStack.fillsParent() ^ 1) != 0 || this.mTask.mStack.getConfiguration().orientation != 2) {
            return 0;
        }
        this.mTask.mStack.getDimBounds(this.mTmpRect);
        if (x - this.mSideMargin <= this.mTmpRect.left) {
            return 1;
        }
        return this.mSideMargin + x >= this.mTmpRect.right ? 2 : 0;
    }

    private void showDimLayer() {
        this.mTask.mStack.getDimBounds(this.mTmpRect);
        if (this.mCurrentDimSide == 1) {
            this.mTmpRect.right = this.mTmpRect.centerX();
        } else if (this.mCurrentDimSide == 2) {
            this.mTmpRect.left = this.mTmpRect.centerX();
        }
        this.mDimLayer.setBounds(this.mTmpRect);
        this.mDimLayer.show(this.mService.getDragLayerLocked(), 0.5f, 0);
    }

    public boolean dimFullscreen() {
        return isFullscreen();
    }

    boolean isFullscreen() {
        return false;
    }

    public DisplayInfo getDisplayInfo() {
        return this.mTask.mStack.getDisplayInfo();
    }

    public boolean isAttachedToDisplay() {
        return (this.mTask == null || this.mTask.getDisplayContent() == null) ? false : true;
    }

    public void getDimBounds(Rect out) {
    }

    public String toShortString() {
        return TAG;
    }
}
