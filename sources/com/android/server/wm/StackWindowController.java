package com.android.server.wm;

import android.app.ActivityManager.StackId;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;
import android.view.DisplayInfo;
import java.lang.ref.WeakReference;

public class StackWindowController extends WindowContainerController<TaskStack, StackWindowListener> {
    private final H mHandler;
    final int mStackId;
    private final Rect mTmpDisplayBounds;
    private final Rect mTmpNonDecorInsets;
    private final Rect mTmpRect;
    private final Rect mTmpStableInsets;

    private static final class H extends Handler {
        static final int REQUEST_RESIZE = 0;
        private final WeakReference<StackWindowController> mController;

        H(WeakReference<StackWindowController> controller, Looper looper) {
            super(looper);
            this.mController = controller;
        }

        public void handleMessage(Message msg) {
            StackWindowController controller = (StackWindowController) this.mController.get();
            StackWindowListener stackWindowListener = controller != null ? (StackWindowListener) controller.mListener : null;
            if (stackWindowListener != null) {
                switch (msg.what) {
                    case 0:
                        stackWindowListener.requestResize((Rect) msg.obj);
                        break;
                }
            }
        }
    }

    public StackWindowController(int stackId, StackWindowListener listener, int displayId, boolean onTop, Rect outBounds) {
        this(stackId, listener, displayId, onTop, outBounds, WindowManagerService.getInstance());
    }

    public StackWindowController(int stackId, StackWindowListener listener, int displayId, boolean onTop, Rect outBounds, WindowManagerService service) {
        super(listener, service);
        this.mTmpRect = new Rect();
        this.mTmpStableInsets = new Rect();
        this.mTmpNonDecorInsets = new Rect();
        this.mTmpDisplayBounds = new Rect();
        this.mStackId = stackId;
        this.mHandler = new H(new WeakReference(this), service.mH.getLooper());
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                DisplayContent dc = this.mRoot.getDisplayContent(displayId);
                if (dc == null) {
                    throw new IllegalArgumentException("Trying to add stackId=" + stackId + " to unknown displayId=" + displayId);
                }
                dc.addStackToDisplay(stackId, onTop).setController(this);
                getRawBounds(outBounds);
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public void removeContainer() {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer != null) {
                    ((TaskStack) this.mContainer).removeIfPossible();
                    super.removeContainer();
                }
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public boolean isVisible() {
        boolean isVisible;
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                isVisible = this.mContainer != null ? ((TaskStack) this.mContainer).isVisible() : false;
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        return isVisible;
    }

    public void reparent(int displayId, Rect outStackBounds, boolean onTop) {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer == null) {
                    throw new IllegalArgumentException("Trying to move unknown stackId=" + this.mStackId + " to displayId=" + displayId);
                }
                DisplayContent targetDc = this.mRoot.getDisplayContent(displayId);
                if (targetDc == null) {
                    throw new IllegalArgumentException("Trying to move stackId=" + this.mStackId + " to unknown displayId=" + displayId);
                }
                targetDc.moveStackToDisplay((TaskStack) this.mContainer, onTop);
                getRawBounds(outStackBounds);
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void positionChildAt(com.android.server.wm.TaskWindowContainerController r3, int r4, android.graphics.Rect r5, android.content.res.Configuration r6) {
        /*
        r2 = this;
        r1 = r2.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x002f }
        r0 = r3.mContainer;	 Catch:{ all -> 0x002f }
        if (r0 != 0) goto L_0x000f;
    L_0x000a:
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x000f:
        r0 = r2.mContainer;	 Catch:{ all -> 0x002f }
        if (r0 != 0) goto L_0x0018;
    L_0x0013:
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0018:
        r0 = r3.mContainer;	 Catch:{ all -> 0x002f }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x002f }
        r0.positionAt(r4, r5, r6);	 Catch:{ all -> 0x002f }
        r0 = r2.mContainer;	 Catch:{ all -> 0x002f }
        r0 = (com.android.server.wm.TaskStack) r0;	 Catch:{ all -> 0x002f }
        r0 = r0.getDisplayContent();	 Catch:{ all -> 0x002f }
        r0.layoutAndAssignWindowLayersIfNeeded();	 Catch:{ all -> 0x002f }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x002f:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.StackWindowController.positionChildAt(com.android.server.wm.TaskWindowContainerController, int, android.graphics.Rect, android.content.res.Configuration):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void positionChildAtTop(com.android.server.wm.TaskWindowContainerController r6, boolean r7) {
        /*
        r5 = this;
        if (r6 != 0) goto L_0x0003;
    L_0x0002:
        return;
    L_0x0003:
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x005d }
        r0 = r6.mContainer;	 Catch:{ all -> 0x005d }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x005d }
        if (r0 != 0) goto L_0x0035;
    L_0x000f:
        r1 = "WindowManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x005d }
        r3.<init>();	 Catch:{ all -> 0x005d }
        r4 = "positionChildAtTop: task=";
        r3 = r3.append(r4);	 Catch:{ all -> 0x005d }
        r3 = r3.append(r6);	 Catch:{ all -> 0x005d }
        r4 = " not found";
        r3 = r3.append(r4);	 Catch:{ all -> 0x005d }
        r3 = r3.toString();	 Catch:{ all -> 0x005d }
        android.util.Slog.e(r1, r3);	 Catch:{ all -> 0x005d }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0035:
        r1 = r5.mContainer;	 Catch:{ all -> 0x005d }
        r1 = (com.android.server.wm.TaskStack) r1;	 Catch:{ all -> 0x005d }
        r3 = 2147483647; // 0x7fffffff float:NaN double:1.060997895E-314;
        r1.positionChildAt(r3, r0, r7);	 Catch:{ all -> 0x005d }
        r1 = r5.mService;	 Catch:{ all -> 0x005d }
        r1 = r1.mAppTransition;	 Catch:{ all -> 0x005d }
        r1 = r1.isTransitionSet();	 Catch:{ all -> 0x005d }
        if (r1 == 0) goto L_0x004d;
    L_0x0049:
        r1 = 0;
        r0.setSendingToBottom(r1);	 Catch:{ all -> 0x005d }
    L_0x004d:
        r1 = r5.mContainer;	 Catch:{ all -> 0x005d }
        r1 = (com.android.server.wm.TaskStack) r1;	 Catch:{ all -> 0x005d }
        r1 = r1.getDisplayContent();	 Catch:{ all -> 0x005d }
        r1.layoutAndAssignWindowLayersIfNeeded();	 Catch:{ all -> 0x005d }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x005d:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.StackWindowController.positionChildAtTop(com.android.server.wm.TaskWindowContainerController, boolean):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void positionChildAtBottom(com.android.server.wm.TaskWindowContainerController r6) {
        /*
        r5 = this;
        if (r6 != 0) goto L_0x0003;
    L_0x0002:
        return;
    L_0x0003:
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x005d }
        r0 = r6.mContainer;	 Catch:{ all -> 0x005d }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x005d }
        if (r0 != 0) goto L_0x0035;
    L_0x000f:
        r1 = "WindowManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x005d }
        r3.<init>();	 Catch:{ all -> 0x005d }
        r4 = "positionChildAtBottom: task=";
        r3 = r3.append(r4);	 Catch:{ all -> 0x005d }
        r3 = r3.append(r6);	 Catch:{ all -> 0x005d }
        r4 = " not found";
        r3 = r3.append(r4);	 Catch:{ all -> 0x005d }
        r3 = r3.toString();	 Catch:{ all -> 0x005d }
        android.util.Slog.e(r1, r3);	 Catch:{ all -> 0x005d }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0035:
        r1 = r5.mContainer;	 Catch:{ all -> 0x005d }
        r1 = (com.android.server.wm.TaskStack) r1;	 Catch:{ all -> 0x005d }
        r3 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r4 = 0;
        r1.positionChildAt(r3, r0, r4);	 Catch:{ all -> 0x005d }
        r1 = r5.mService;	 Catch:{ all -> 0x005d }
        r1 = r1.mAppTransition;	 Catch:{ all -> 0x005d }
        r1 = r1.isTransitionSet();	 Catch:{ all -> 0x005d }
        if (r1 == 0) goto L_0x004d;
    L_0x0049:
        r1 = 1;
        r0.setSendingToBottom(r1);	 Catch:{ all -> 0x005d }
    L_0x004d:
        r1 = r5.mContainer;	 Catch:{ all -> 0x005d }
        r1 = (com.android.server.wm.TaskStack) r1;	 Catch:{ all -> 0x005d }
        r1 = r1.getDisplayContent();	 Catch:{ all -> 0x005d }
        r1.layoutAndAssignWindowLayersIfNeeded();	 Catch:{ all -> 0x005d }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x005d:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.StackWindowController.positionChildAtBottom(com.android.server.wm.TaskWindowContainerController):void");
    }

    public boolean resize(Rect bounds, SparseArray<Configuration> configs, SparseArray<Rect> taskBounds, SparseArray<Rect> taskTempInsetBounds) {
        boolean rawFullscreen;
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer == null) {
                    throw new IllegalArgumentException("resizeStack: stack " + this + " not found.");
                }
                ((TaskStack) this.mContainer).prepareFreezingTaskBounds();
                if (((TaskStack) this.mContainer).setBounds(bounds, configs, taskBounds, taskTempInsetBounds) && ((TaskStack) this.mContainer).isVisible()) {
                    ((TaskStack) this.mContainer).getDisplayContent().setLayoutNeeded();
                    this.mService.mWindowPlacerLocked.performSurfacePlacement();
                }
                rawFullscreen = ((TaskStack) this.mContainer).getRawFullscreen();
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        return rawFullscreen;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void getStackDockedModeBounds(android.graphics.Rect r3, android.graphics.Rect r4, android.graphics.Rect r5, boolean r6) {
        /*
        r2 = this;
        r1 = r2.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0021 }
        r0 = r2.mContainer;	 Catch:{ all -> 0x0021 }
        if (r0 == 0) goto L_0x0016;
    L_0x000a:
        r0 = r2.mContainer;	 Catch:{ all -> 0x0021 }
        r0 = (com.android.server.wm.TaskStack) r0;	 Catch:{ all -> 0x0021 }
        r0.getStackDockedModeBoundsLocked(r3, r4, r5, r6);	 Catch:{ all -> 0x0021 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0016:
        r4.setEmpty();	 Catch:{ all -> 0x0021 }
        r5.setEmpty();	 Catch:{ all -> 0x0021 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0021:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.StackWindowController.getStackDockedModeBounds(android.graphics.Rect, android.graphics.Rect, android.graphics.Rect, boolean):void");
    }

    public void prepareFreezingTaskBounds() {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer == null) {
                    throw new IllegalArgumentException("prepareFreezingTaskBounds: stack " + this + " not found.");
                }
                ((TaskStack) this.mContainer).prepareFreezingTaskBounds();
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    private void getRawBounds(Rect outBounds) {
        if (((TaskStack) this.mContainer).getRawFullscreen()) {
            outBounds.setEmpty();
        } else {
            ((TaskStack) this.mContainer).getRawBounds(outBounds);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void getBounds(android.graphics.Rect r3) {
        /*
        r2 = this;
        r1 = r2.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x001e }
        r0 = r2.mContainer;	 Catch:{ all -> 0x001e }
        if (r0 == 0) goto L_0x0016;
    L_0x000a:
        r0 = r2.mContainer;	 Catch:{ all -> 0x001e }
        r0 = (com.android.server.wm.TaskStack) r0;	 Catch:{ all -> 0x001e }
        r0.getBounds(r3);	 Catch:{ all -> 0x001e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0016:
        r3.setEmpty();	 Catch:{ all -> 0x001e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x001e:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.StackWindowController.getBounds(android.graphics.Rect):void");
    }

    public void getBoundsForNewConfiguration(Rect outBounds) {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                ((TaskStack) this.mContainer).getBoundsForNewConfiguration(outBounds);
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public void adjustConfigurationForBounds(Rect bounds, Rect insetBounds, Rect nonDecorBounds, Rect stableBounds, boolean overrideWidth, boolean overrideHeight, float density, Configuration config, Configuration parentConfig) {
        synchronized (this.mWindowMap) {
            try {
                int width;
                int height;
                WindowManagerService.boostPriorityForLockedSection();
                DisplayInfo di = this.mContainer.getDisplayContent().getDisplayInfo();
                this.mService.mPolicy.getStableInsetsLw(di.rotation, di.logicalWidth, di.logicalHeight, this.mTmpStableInsets);
                this.mService.mPolicy.getNonDecorInsetsLw(di.rotation, di.logicalWidth, di.logicalHeight, this.mTmpNonDecorInsets);
                this.mTmpDisplayBounds.set(0, 0, di.logicalWidth, di.logicalHeight);
                Rect parentAppBounds = parentConfig.appBounds;
                config.setAppBounds(!bounds.isEmpty() ? bounds : null);
                boolean intersectParentBounds = false;
                if (StackId.tasksAreFloating(this.mStackId)) {
                    if (this.mStackId == 4 && bounds.width() == this.mTmpDisplayBounds.width() && bounds.height() == this.mTmpDisplayBounds.height()) {
                        stableBounds.inset(this.mTmpStableInsets);
                        nonDecorBounds.inset(this.mTmpNonDecorInsets);
                        config.appBounds.offsetTo(0, 0);
                        intersectParentBounds = true;
                    }
                    width = (int) (((float) stableBounds.width()) / density);
                    height = (int) (((float) stableBounds.height()) / density);
                } else {
                    Rect rect;
                    if (insetBounds != null) {
                        rect = insetBounds;
                    } else {
                        rect = bounds;
                    }
                    intersectDisplayBoundsExcludeInsets(nonDecorBounds, rect, this.mTmpNonDecorInsets, this.mTmpDisplayBounds, overrideWidth, overrideHeight);
                    if (insetBounds != null) {
                        rect = insetBounds;
                    } else {
                        rect = bounds;
                    }
                    intersectDisplayBoundsExcludeInsets(stableBounds, rect, this.mTmpStableInsets, this.mTmpDisplayBounds, overrideWidth, overrideHeight);
                    width = Math.min((int) (((float) stableBounds.width()) / density), parentConfig.screenWidthDp);
                    height = Math.min((int) (((float) stableBounds.height()) / density), parentConfig.screenHeightDp);
                    intersectParentBounds = true;
                }
                if (intersectParentBounds && config.appBounds != null) {
                    config.appBounds.intersect(parentAppBounds);
                }
                config.screenWidthDp = width;
                config.screenHeightDp = height;
                if (insetBounds == null) {
                    insetBounds = bounds;
                }
                config.smallestScreenWidthDp = getSmallestWidthForTaskBounds(insetBounds, density);
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    private void intersectDisplayBoundsExcludeInsets(Rect inOutBounds, Rect inInsetBounds, Rect stableInsets, Rect displayBounds, boolean overrideWidth, boolean overrideHeight) {
        this.mTmpRect.set(inInsetBounds);
        this.mService.intersectDisplayInsetBounds(displayBounds, stableInsets, this.mTmpRect);
        inOutBounds.inset(this.mTmpRect.left - inInsetBounds.left, this.mTmpRect.top - inInsetBounds.top, overrideWidth ? 0 : inInsetBounds.right - this.mTmpRect.right, overrideHeight ? 0 : inInsetBounds.bottom - this.mTmpRect.bottom);
    }

    private int getSmallestWidthForTaskBounds(Rect bounds, float density) {
        DisplayContent displayContent = ((TaskStack) this.mContainer).getDisplayContent();
        DisplayInfo displayInfo = displayContent.getDisplayInfo();
        if (bounds == null || (bounds.width() == displayInfo.logicalWidth && bounds.height() == displayInfo.logicalHeight)) {
            return displayContent.getConfiguration().smallestScreenWidthDp;
        }
        if (StackId.tasksAreFloating(this.mStackId)) {
            return (int) (((float) Math.min(bounds.width(), bounds.height())) / density);
        }
        return displayContent.getDockedDividerController().getSmallestWidthDpForBounds(bounds);
    }

    void requestResize(Rect bounds) {
        this.mHandler.obtainMessage(0, bounds).sendToTarget();
    }

    public String toString() {
        return "{StackWindowController stackId=" + this.mStackId + "}";
    }
}
