package com.android.server.wm;

import android.animation.AnimationHandler;
import android.animation.ValueAnimator;
import android.app.ActivityManager;
import android.app.ActivityManager.TaskSnapshot;
import android.app.ActivityManagerInternal;
import android.app.ActivityThread;
import android.app.AppOpsManager;
import android.app.AppOpsManager.OnOpChangedInternalListener;
import android.app.IActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.GraphicBuffer;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.hardware.configstore.V1_0.ISurfaceFlingerConfigs;
import android.hardware.configstore.V1_0.OptionalBool;
import android.hardware.display.DisplayManager;
import android.hardware.display.DisplayManagerInternal;
import android.hardware.input.InputManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.IRemoteCallback;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.PowerManagerInternal;
import android.os.PowerManagerInternal.LowPowerModeListener;
import android.os.PowerSaveState;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.SystemService;
import android.os.Trace;
import android.os.UserHandle;
import android.os.WorkSource;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.util.ArraySet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TimeUtils;
import android.util.TypedValue;
import android.view.AppTransitionAnimationSpec;
import android.view.Display;
import android.view.DisplayInfo;
import android.view.IAppTransitionAnimationSpecsFuture;
import android.view.IDockedStackListener;
import android.view.IInputFilter;
import android.view.IOnKeyguardExitResult;
import android.view.IPinnedStackListener;
import android.view.IRotationWatcher;
import android.view.IWallpaperVisibilityListener;
import android.view.IWindow;
import android.view.IWindowId;
import android.view.IWindowManager.Stub;
import android.view.IWindowSession;
import android.view.IWindowSessionCallback;
import android.view.InputChannel;
import android.view.InputEventReceiver;
import android.view.InputEventReceiver.Factory;
import android.view.MagnificationSpec;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.Surface.OutOfResourcesException;
import android.view.SurfaceControl;
import android.view.SurfaceSession;
import android.view.WindowContentFrameStats;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerInternal;
import android.view.WindowManagerInternal.AppTransitionListener;
import android.view.WindowManagerInternal.MagnificationCallbacks;
import android.view.WindowManagerInternal.OnHardKeyboardStatusChangeListener;
import android.view.WindowManagerInternal.WindowsForAccessibilityCallback;
import android.view.WindowManagerPolicy;
import android.view.WindowManagerPolicy.InputConsumer;
import android.view.WindowManagerPolicy.OnKeyguardExitResult;
import android.view.WindowManagerPolicy.PointerEventListener;
import android.view.WindowManagerPolicy.ScreenOffListener;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.view.WindowManagerPolicy.WindowState;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManagerInternal;
import com.android.internal.app.IAssistScreenshotReceiver;
import com.android.internal.graphics.SfVsyncFrameCallbackProvider;
import com.android.internal.os.IResultReceiver;
import com.android.internal.policy.IKeyguardDismissCallback;
import com.android.internal.policy.IShortcutService;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.FastPrintWriter;
import com.android.internal.view.IInputContext;
import com.android.internal.view.IInputMethodClient;
import com.android.internal.view.IInputMethodManager;
import com.android.internal.view.WindowManagerPolicyThread;
import com.android.server.AnimationThread;
import com.android.server.DisplayThread;
import com.android.server.FgThread;
import com.android.server.LocalServices;
import com.android.server.LockGuard;
import com.android.server.UiThread;
import com.android.server.Watchdog;
import com.android.server.Watchdog.Monitor;
import com.android.server.input.InputManagerService;
import com.android.server.job.controllers.JobStatus;
import com.android.server.power.ShutdownThread;
import com.android.server.usb.descriptors.UsbACInterface;
import com.android.server.usb.descriptors.UsbDescriptor;
import com.android.server.usb.descriptors.UsbTerminalTypes;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.Socket;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class WindowManagerService extends Stub implements Monitor, WindowManagerFuncs {
    private static final boolean ALWAYS_KEEP_CURRENT = true;
    private static final int ANIMATION_DURATION_SCALE = 2;
    private static final int BOOT_ANIMATION_POLL_INTERVAL = 200;
    private static final String BOOT_ANIMATION_SERVICE = "bootanim";
    static final boolean CUSTOM_SCREEN_ROTATION = true;
    static final long DEFAULT_INPUT_DISPATCHING_TIMEOUT_NANOS = 5000000000L;
    private static final String DENSITY_OVERRIDE = "ro.config.density_override";
    private static final float DRAG_SHADOW_ALPHA_TRANSPARENT = 0.7071f;
    private static final int INPUT_DEVICES_READY_FOR_SAFE_MODE_DETECTION_TIMEOUT_MILLIS = 1000;
    static final int LAST_ANR_LIFETIME_DURATION_MSECS = 7200000;
    static final int LAYER_OFFSET_DIM = 1;
    static final int LAYER_OFFSET_THUMBNAIL = 4;
    static final int LAYOUT_REPEAT_THRESHOLD = 4;
    static final int MAX_ANIMATION_DURATION = 10000;
    private static final int MAX_SCREENSHOT_RETRIES = 3;
    static final boolean PROFILE_ORIENTATION = false;
    private static final String PROPERTY_EMULATOR_CIRCULAR = "ro.emulator.circular";
    static final int SEAMLESS_ROTATION_TIMEOUT_DURATION = 2000;
    private static final String SIZE_OVERRIDE = "ro.config.size_override";
    private static final String SYSTEM_DEBUGGABLE = "ro.debuggable";
    private static final String SYSTEM_SECURE = "ro.secure";
    private static final String TAG = "WindowManager";
    private static final int TRANSITION_ANIMATION_SCALE = 1;
    static final int TYPE_LAYER_MULTIPLIER = 10000;
    static final int TYPE_LAYER_OFFSET = 1000;
    static final int UPDATE_FOCUS_NORMAL = 0;
    static final int UPDATE_FOCUS_PLACING_SURFACES = 2;
    static final int UPDATE_FOCUS_WILL_ASSIGN_LAYERS = 1;
    static final int UPDATE_FOCUS_WILL_PLACE_SURFACES = 3;
    static final int WINDOWS_FREEZING_SCREENS_ACTIVE = 1;
    static final int WINDOWS_FREEZING_SCREENS_NONE = 0;
    static final int WINDOWS_FREEZING_SCREENS_TIMEOUT = 2;
    private static final int WINDOW_ANIMATION_SCALE = 0;
    static final int WINDOW_FREEZE_TIMEOUT_DURATION = 2000;
    static final int WINDOW_LAYER_MULTIPLIER = 5;
    static final int WINDOW_REPLACEMENT_TIMEOUT_DURATION = 2000;
    static final boolean localLOGV = false;
    private static WindowManagerService sInstance;
    static WindowManagerThreadPriorityBooster sThreadPriorityBooster = new WindowManagerThreadPriorityBooster();
    AccessibilityController mAccessibilityController;
    final IActivityManager mActivityManager;
    final AppTransitionListener mActivityManagerAppTransitionNotifier = new AppTransitionListener() {
        public void onAppTransitionCancelledLocked(int transit) {
            WindowManagerService.this.mH.sendEmptyMessage(48);
        }

        public void onAppTransitionFinishedLocked(IBinder token) {
            WindowManagerService.this.mH.sendEmptyMessage(49);
            AppWindowToken atoken = WindowManagerService.this.mRoot.getAppWindowToken(token);
            if (atoken != null) {
                if (atoken.mLaunchTaskBehind) {
                    try {
                        WindowManagerService.this.mActivityManager.notifyLaunchTaskBehindComplete(atoken.token);
                    } catch (RemoteException e) {
                    }
                    atoken.mLaunchTaskBehind = false;
                } else {
                    atoken.updateReportedVisibilityLocked();
                    if (atoken.mEnteringAnimation) {
                        atoken.mEnteringAnimation = false;
                        try {
                            WindowManagerService.this.mActivityManager.notifyEnterAnimationComplete(atoken.token);
                        } catch (RemoteException e2) {
                        }
                    }
                }
            }
        }
    };
    final boolean mAllowAnimationsInLowPowerMode;
    final boolean mAllowBootMessages;
    boolean mAllowTheaterModeWakeFromLayout;
    final ActivityManagerInternal mAmInternal;
    boolean mAnimateWallpaperWithTarget;
    final Handler mAnimationHandler = new Handler(AnimationThread.getHandler().getLooper());
    private boolean mAnimationsDisabled = false;
    final WindowAnimator mAnimator;
    private float mAnimatorDurationScaleSetting = 1.0f;
    final ArrayList<AppFreezeListener> mAppFreezeListeners = new ArrayList();
    final AppOpsManager mAppOps;
    final AppTransition mAppTransition;
    int mAppsFreezingScreen = 0;
    boolean mBootAnimationStopped = false;
    final BoundsAnimationController mBoundsAnimationController;
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED")) {
                WindowManagerService.this.mKeyguardDisableHandler.sendEmptyMessage(3);
            } else if (action.equals("android.intent.action.USER_REMOVED")) {
                int userId = intent.getIntExtra("android.intent.extra.user_handle", -10000);
                if (userId != -10000) {
                    synchronized (WindowManagerService.this.mWindowMap) {
                        try {
                            WindowManagerService.boostPriorityForLockedSection();
                            WindowManagerService.this.mScreenCaptureDisabled.remove(userId);
                        } finally {
                            WindowManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                }
            }
        }
    };
    CircularDisplayMask mCircularDisplayMask;
    boolean mClientFreezingScreen = false;
    final ArraySet<AppWindowToken> mClosingApps = new ArraySet();
    final Context mContext;
    WindowState mCurrentFocus = null;
    int[] mCurrentProfileIds = new int[0];
    int mCurrentUserId;
    int mDeferredRotationPauseCount;
    final ArrayList<WindowState> mDestroyPreservedSurface = new ArrayList();
    final ArrayList<WindowState> mDestroySurface = new ArrayList();
    boolean mDisplayEnabled = false;
    long mDisplayFreezeTime = 0;
    boolean mDisplayFrozen = false;
    final DisplayManager mDisplayManager;
    final DisplayManagerInternal mDisplayManagerInternal;
    boolean mDisplayReady;
    final DisplaySettings mDisplaySettings;
    private final Display[] mDisplays;
    Rect mDockedStackCreateBounds;
    int mDockedStackCreateMode = 0;
    DragState mDragState = null;
    final long mDrawLockTimeoutMillis;
    EmulatorDisplayOverlay mEmulatorDisplayOverlay;
    private int mEnterAnimId;
    private boolean mEventDispatchingEnabled;
    private int mExitAnimId;
    final ArrayList<AppWindowToken> mFinishedEarlyAnim = new ArrayList();
    final ArrayList<AppWindowToken> mFinishedStarting = new ArrayList();
    boolean mFocusMayChange;
    AppWindowToken mFocusedApp = null;
    boolean mForceDisplayEnabled = false;
    final ArrayList<WindowState> mForceRemoves = new ArrayList();
    boolean mForceResizableTasks = false;
    private int mFrozenDisplayId;
    final SurfaceSession mFxSession;
    final H mH = new H();
    boolean mHardKeyboardAvailable;
    OnHardKeyboardStatusChangeListener mHardKeyboardStatusChangeListener;
    final boolean mHasPermanentDpad;
    private boolean mHasWideColorGamutSupport;
    final boolean mHaveInputMethods;
    private ArrayList<WindowState> mHidingNonSystemOverlayWindows = new ArrayList();
    private Session mHoldingScreenOn;
    private WakeLock mHoldingScreenWakeLock;
    boolean mInTouchMode;
    final InputManagerService mInputManager;
    IInputMethodManager mInputMethodManager;
    WindowState mInputMethodTarget = null;
    boolean mInputMethodTargetWaitingAnim;
    WindowState mInputMethodWindow = null;
    final InputMonitor mInputMonitor = new InputMonitor(this);
    boolean mIsTouchDevice;
    private final KeyguardDisableHandler mKeyguardDisableHandler;
    boolean mKeyguardGoingAway;
    String mLastANRState;
    int mLastDispatchedSystemUiVisibility = 0;
    int mLastDisplayFreezeDuration = 0;
    Object mLastFinishedFreezeSource = null;
    WindowState mLastFocus = null;
    int mLastStatusBarVisibility = 0;
    WindowState mLastWakeLockHoldingWindow = null;
    WindowState mLastWakeLockObscuringWindow = null;
    int mLayoutSeq = 0;
    final boolean mLimitedAlphaCompositing;
    ArrayList<WindowState> mLosingFocus = new ArrayList();
    final int mMaxUiWidth;
    MousePositionTracker mMousePositionTracker = new MousePositionTracker();
    final List<IBinder> mNoAnimationNotifyOnTransitionFinished = new ArrayList();
    final boolean mOnlyCore;
    final ArraySet<AppWindowToken> mOpeningApps = new ArraySet();
    final ArrayList<WindowState> mPendingRemove = new ArrayList();
    WindowState[] mPendingRemoveTmp = new WindowState[20];
    private final PointerEventDispatcher mPointerEventDispatcher;
    final WindowManagerPolicy mPolicy;
    PowerManager mPowerManager;
    PowerManagerInternal mPowerManagerInternal;
    final ArrayList<WindowState> mResizingWindows = new ArrayList();
    RootWindowContainer mRoot;
    ArrayList<RotationWatcher> mRotationWatchers = new ArrayList();
    boolean mSafeMode;
    private SparseArray<Boolean> mScreenCaptureDisabled = new SparseArray();
    private final WakeLock mScreenFrozenLock;
    final Rect mScreenRect = new Rect();
    int mSeamlessRotationCount = 0;
    final ArraySet<Session> mSessions = new ArraySet();
    SettingsObserver mSettingsObserver;
    boolean mShowAlertWindowNotifications = true;
    boolean mShowingBootMessages = false;
    boolean mSkipAppTransitionAnimation = false;
    StrictModeFlash mStrictModeFlash;
    boolean mSupportsPictureInPicture = false;
    boolean mSwitchingUser = false;
    boolean mSystemBooted = false;
    int mSystemDecorLayer = 0;
    TaskPositioner mTaskPositioner;
    final TaskSnapshotController mTaskSnapshotController;
    final Configuration mTempConfiguration = new Configuration();
    private WindowContentFrameStats mTempWindowRenderStats;
    final float[] mTmpFloats = new float[9];
    final Rect mTmpRect = new Rect();
    final Rect mTmpRect2 = new Rect();
    final Rect mTmpRect3 = new Rect();
    final RectF mTmpRectF = new RectF();
    private final SparseIntArray mTmpTaskIds = new SparseIntArray();
    final Matrix mTmpTransform = new Matrix();
    int mTransactionSequence;
    private float mTransitionAnimationScaleSetting = 1.0f;
    boolean mTurnOnScreen;
    final UnknownAppVisibilityController mUnknownAppVisibilityController = new UnknownAppVisibilityController(this);
    private ViewServer mViewServer;
    int mVr2dDisplayId = -1;
    boolean mWaitingForConfig = false;
    ArrayList<WindowState> mWaitingForDrawn = new ArrayList();
    Runnable mWaitingForDrawnCallback;
    final WallpaperVisibilityListeners mWallpaperVisibilityListeners = new WallpaperVisibilityListeners();
    Watermark mWatermark;
    private final ArrayList<WindowState> mWinAddedSinceNullFocus = new ArrayList();
    private final ArrayList<WindowState> mWinRemovedSinceNullFocus = new ArrayList();
    private float mWindowAnimationScaleSetting = 1.0f;
    final ArrayList<WindowChangeListener> mWindowChangeListeners = new ArrayList();
    final WindowHashMap mWindowMap = new WindowHashMap();
    final WindowSurfacePlacer mWindowPlacerLocked;
    final ArrayList<AppWindowToken> mWindowReplacementTimeouts = new ArrayList();
    boolean mWindowsChanged = false;
    int mWindowsFreezingScreen = 0;

    interface AppFreezeListener {
        void onAppFreezeTimeout();
    }

    public interface WindowChangeListener {
        void focusChanged();

        void windowsChanged();
    }

    final class DragInputEventReceiver extends InputEventReceiver {
        private boolean mIsStartEvent = true;
        private boolean mMuteInput = false;
        private boolean mStylusButtonDownAtStart;

        public DragInputEventReceiver(InputChannel inputChannel, Looper looper) {
            super(inputChannel, looper);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onInputEvent(android.view.InputEvent r11, int r12) {
            /*
            r10 = this;
            r3 = 0;
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ Exception -> 0x0089 }
            r8 = r8.mDragState;	 Catch:{ Exception -> 0x0089 }
            if (r8 != 0) goto L_0x000c;
        L_0x0007:
            r3 = 1;
            r10.finishInputEvent(r11, r3);
            return;
        L_0x000c:
            r8 = r11 instanceof android.view.MotionEvent;	 Catch:{ Exception -> 0x0089 }
            if (r8 == 0) goto L_0x0063;
        L_0x0010:
            r8 = r11.getSource();	 Catch:{ Exception -> 0x0089 }
            r8 = r8 & 2;
            if (r8 == 0) goto L_0x0063;
        L_0x0018:
            r8 = r10.mMuteInput;	 Catch:{ Exception -> 0x0089 }
            r8 = r8 ^ 1;
            if (r8 == 0) goto L_0x0063;
        L_0x001e:
            r0 = r11;
            r0 = (android.view.MotionEvent) r0;	 Catch:{ Exception -> 0x0089 }
            r5 = r0;
            r2 = 0;
            r6 = r5.getRawX();	 Catch:{ Exception -> 0x0089 }
            r7 = r5.getRawY();	 Catch:{ Exception -> 0x0089 }
            r8 = r5.getButtonState();	 Catch:{ Exception -> 0x0089 }
            r8 = r8 & 32;
            if (r8 == 0) goto L_0x0067;
        L_0x0033:
            r4 = 1;
        L_0x0034:
            r8 = r10.mIsStartEvent;	 Catch:{ Exception -> 0x0089 }
            if (r8 == 0) goto L_0x0040;
        L_0x0038:
            if (r4 == 0) goto L_0x003d;
        L_0x003a:
            r8 = 1;
            r10.mStylusButtonDownAtStart = r8;	 Catch:{ Exception -> 0x0089 }
        L_0x003d:
            r8 = 0;
            r10.mIsStartEvent = r8;	 Catch:{ Exception -> 0x0089 }
        L_0x0040:
            r8 = r5.getAction();	 Catch:{ Exception -> 0x0089 }
            switch(r8) {
                case 0: goto L_0x0047;
                case 1: goto L_0x00bc;
                case 2: goto L_0x0069;
                case 3: goto L_0x00db;
                default: goto L_0x0047;
            };	 Catch:{ Exception -> 0x0089 }
        L_0x0047:
            if (r2 == 0) goto L_0x0062;
        L_0x0049:
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ Exception -> 0x0089 }
            r9 = r8.mWindowMap;	 Catch:{ Exception -> 0x0089 }
            monitor-enter(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x00e1 }
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00e1 }
            r8 = r8.mDragState;	 Catch:{ all -> 0x00e1 }
            r8.endDragLw();	 Catch:{ all -> 0x00e1 }
            monitor-exit(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x0089 }
            r8 = 0;
            r10.mStylusButtonDownAtStart = r8;	 Catch:{ Exception -> 0x0089 }
            r8 = 1;
            r10.mIsStartEvent = r8;	 Catch:{ Exception -> 0x0089 }
        L_0x0062:
            r3 = 1;
        L_0x0063:
            r10.finishInputEvent(r11, r3);
        L_0x0066:
            return;
        L_0x0067:
            r4 = 0;
            goto L_0x0034;
        L_0x0069:
            r8 = r10.mStylusButtonDownAtStart;	 Catch:{ Exception -> 0x0089 }
            if (r8 == 0) goto L_0x00a2;
        L_0x006d:
            r8 = r4 ^ 1;
            if (r8 == 0) goto L_0x00a2;
        L_0x0071:
            r8 = 1;
            r10.mMuteInput = r8;	 Catch:{ Exception -> 0x0089 }
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ Exception -> 0x0089 }
            r9 = r8.mWindowMap;	 Catch:{ Exception -> 0x0089 }
            monitor-enter(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0097 }
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0097 }
            r8 = r8.mDragState;	 Catch:{ all -> 0x0097 }
            r2 = r8.notifyDropLw(r6, r7);	 Catch:{ all -> 0x0097 }
            monitor-exit(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x0089 }
            goto L_0x0047;
        L_0x0089:
            r1 = move-exception;
            r8 = "WindowManager";
            r9 = "Exception caught by drag handleMotion";
            android.util.Slog.e(r8, r9, r1);	 Catch:{ all -> 0x009d }
            r10.finishInputEvent(r11, r3);
            goto L_0x0066;
        L_0x0097:
            r8 = move-exception;
            monitor-exit(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x0089 }
            throw r8;	 Catch:{ Exception -> 0x0089 }
        L_0x009d:
            r8 = move-exception;
            r10.finishInputEvent(r11, r3);
            throw r8;
        L_0x00a2:
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ Exception -> 0x0089 }
            r9 = r8.mWindowMap;	 Catch:{ Exception -> 0x0089 }
            monitor-enter(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x00b6 }
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00b6 }
            r8 = r8.mDragState;	 Catch:{ all -> 0x00b6 }
            r8.notifyMoveLw(r6, r7);	 Catch:{ all -> 0x00b6 }
            monitor-exit(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x0089 }
            goto L_0x0047;
        L_0x00b6:
            r8 = move-exception;
            monitor-exit(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x0089 }
            throw r8;	 Catch:{ Exception -> 0x0089 }
        L_0x00bc:
            r8 = 1;
            r10.mMuteInput = r8;	 Catch:{ Exception -> 0x0089 }
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ Exception -> 0x0089 }
            r9 = r8.mWindowMap;	 Catch:{ Exception -> 0x0089 }
            monitor-enter(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x00d5 }
            r8 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00d5 }
            r8 = r8.mDragState;	 Catch:{ all -> 0x00d5 }
            r2 = r8.notifyDropLw(r6, r7);	 Catch:{ all -> 0x00d5 }
            monitor-exit(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x0089 }
            goto L_0x0047;
        L_0x00d5:
            r8 = move-exception;
            monitor-exit(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x0089 }
            throw r8;	 Catch:{ Exception -> 0x0089 }
        L_0x00db:
            r8 = 1;
            r10.mMuteInput = r8;	 Catch:{ Exception -> 0x0089 }
            r2 = 1;
            goto L_0x0047;
        L_0x00e1:
            r8 = move-exception;
            monitor-exit(r9);	 Catch:{ Exception -> 0x0089 }
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();	 Catch:{ Exception -> 0x0089 }
            throw r8;	 Catch:{ Exception -> 0x0089 }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.DragInputEventReceiver.onInputEvent(android.view.InputEvent, int):void");
        }
    }

    final class H extends Handler {
        public static final int ALL_WINDOWS_DRAWN = 33;
        public static final int APP_FREEZE_TIMEOUT = 17;
        public static final int APP_TRANSITION_TIMEOUT = 13;
        public static final int BOOT_TIMEOUT = 23;
        public static final int CHECK_IF_BOOT_ANIMATION_FINISHED = 37;
        public static final int CLIENT_FREEZE_TIMEOUT = 30;
        public static final int DO_ANIMATION_CALLBACK = 26;
        public static final int DRAG_END_TIMEOUT = 21;
        public static final int DRAG_START_TIMEOUT = 20;
        public static final int ENABLE_SCREEN = 16;
        public static final int FINISH_TASK_POSITIONING = 40;
        public static final int FORCE_GC = 15;
        public static final int NEW_ANIMATOR_SCALE = 34;
        public static final int NOTIFY_ACTIVITY_DRAWN = 32;
        public static final int NOTIFY_APP_TRANSITION_CANCELLED = 48;
        public static final int NOTIFY_APP_TRANSITION_FINISHED = 49;
        public static final int NOTIFY_APP_TRANSITION_STARTING = 47;
        public static final int NOTIFY_DOCKED_STACK_MINIMIZED_CHANGED = 53;
        public static final int NOTIFY_KEYGUARD_FLAGS_CHANGED = 56;
        public static final int NOTIFY_KEYGUARD_TRUSTED_CHANGED = 57;
        public static final int PERSIST_ANIMATION_SCALE = 14;
        public static final int REPORT_FOCUS_CHANGE = 2;
        public static final int REPORT_HARD_KEYBOARD_STATUS_CHANGE = 22;
        public static final int REPORT_LOSING_FOCUS = 3;
        public static final int REPORT_WINDOWS_CHANGE = 19;
        public static final int RESET_ANR_MESSAGE = 38;
        public static final int RESTORE_POINTER_ICON = 55;
        public static final int SEAMLESS_ROTATION_TIMEOUT = 54;
        public static final int SEND_NEW_CONFIGURATION = 18;
        public static final int SET_HAS_OVERLAY_UI = 58;
        public static final int SHOW_CIRCULAR_DISPLAY_MASK = 35;
        public static final int SHOW_EMULATOR_DISPLAY_OVERLAY = 36;
        public static final int SHOW_STRICT_MODE_VIOLATION = 25;
        public static final int TAP_OUTSIDE_TASK = 31;
        public static final int TEAR_DOWN_DRAG_AND_DROP_INPUT = 44;
        public static final int UNUSED = 0;
        public static final int UPDATE_ANIMATION_SCALE = 51;
        public static final int UPDATE_DOCKED_STACK_DIVIDER = 41;
        public static final int WAITING_FOR_DRAWN_TIMEOUT = 24;
        public static final int WALLPAPER_DRAW_PENDING_TIMEOUT = 39;
        public static final int WINDOW_FREEZE_TIMEOUT = 11;
        public static final int WINDOW_HIDE_TIMEOUT = 52;
        public static final int WINDOW_REPLACEMENT_TIMEOUT = 46;

        H() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void handleMessage(android.os.Message r30) {
            /*
            r29 = this;
            r0 = r30;
            r0 = r0.what;
            r24 = r0;
            switch(r24) {
                case 2: goto L_0x000a;
                case 3: goto L_0x00d6;
                case 4: goto L_0x0009;
                case 5: goto L_0x0009;
                case 6: goto L_0x0009;
                case 7: goto L_0x0009;
                case 8: goto L_0x0009;
                case 9: goto L_0x0009;
                case 10: goto L_0x0009;
                case 11: goto L_0x0136;
                case 12: goto L_0x0009;
                case 13: goto L_0x015f;
                case 14: goto L_0x01d3;
                case 15: goto L_0x02cf;
                case 16: goto L_0x033b;
                case 17: goto L_0x0346;
                case 18: goto L_0x03fb;
                case 19: goto L_0x0439;
                case 20: goto L_0x047a;
                case 21: goto L_0x04d8;
                case 22: goto L_0x0554;
                case 23: goto L_0x055f;
                case 24: goto L_0x056a;
                case 25: goto L_0x05dd;
                case 26: goto L_0x0621;
                case 27: goto L_0x0009;
                case 28: goto L_0x0009;
                case 29: goto L_0x0009;
                case 30: goto L_0x03ab;
                case 31: goto L_0x0633;
                case 32: goto L_0x0665;
                case 33: goto L_0x0685;
                case 34: goto L_0x06be;
                case 35: goto L_0x05f4;
                case 36: goto L_0x0616;
                case 37: goto L_0x0756;
                case 38: goto L_0x0787;
                case 39: goto L_0x07c0;
                case 40: goto L_0x065a;
                case 41: goto L_0x0803;
                case 42: goto L_0x0009;
                case 43: goto L_0x0009;
                case 44: goto L_0x052d;
                case 45: goto L_0x0009;
                case 46: goto L_0x0839;
                case 47: goto L_0x0892;
                case 48: goto L_0x08b5;
                case 49: goto L_0x08c6;
                case 50: goto L_0x0009;
                case 51: goto L_0x0235;
                case 52: goto L_0x08d7;
                case 53: goto L_0x0928;
                case 54: goto L_0x099b;
                case 55: goto L_0x0950;
                case 56: goto L_0x09c4;
                case 57: goto L_0x09e1;
                case 58: goto L_0x09f2;
                default: goto L_0x0009;
            };
        L_0x0009:
            return;
        L_0x000a:
            r5 = 0;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x00d0 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mAccessibilityController;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            if (r24 == 0) goto L_0x0043;
        L_0x0029:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            r24 = r24.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x00d0 }
            r24 = r24.getDisplayId();	 Catch:{ all -> 0x00d0 }
            if (r24 != 0) goto L_0x0043;
        L_0x0039:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            r0 = r24;
            r5 = r0.mAccessibilityController;	 Catch:{ all -> 0x00d0 }
        L_0x0043:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            r0 = r24;
            r15 = r0.mLastFocus;	 Catch:{ all -> 0x00d0 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mCurrentFocus;	 Catch:{ all -> 0x00d0 }
            r18 = r0;
            r0 = r18;
            if (r15 != r0) goto L_0x0062;
        L_0x005d:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return;
        L_0x0062:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            r0 = r18;
            r1 = r24;
            r1.mLastFocus = r0;	 Catch:{ all -> 0x00d0 }
            if (r18 == 0) goto L_0x008c;
        L_0x0070:
            if (r15 == 0) goto L_0x008c;
        L_0x0072:
            r24 = r18.isDisplayedLw();	 Catch:{ all -> 0x00d0 }
            r24 = r24 ^ 1;
            if (r24 == 0) goto L_0x008c;
        L_0x007a:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mLosingFocus;	 Catch:{ all -> 0x00d0 }
            r24 = r0;
            r0 = r24;
            r0.add(r15);	 Catch:{ all -> 0x00d0 }
            r15 = 0;
        L_0x008c:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            if (r5 == 0) goto L_0x0095;
        L_0x0092:
            r5.onWindowFocusChangedNotLocked();
        L_0x0095:
            if (r18 == 0) goto L_0x00b7;
        L_0x0097:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mInTouchMode;
            r24 = r0;
            r25 = 1;
            r0 = r18;
            r1 = r25;
            r2 = r24;
            r0.reportFocusChangedSerialized(r1, r2);
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r24.notifyFocusChanged();
        L_0x00b7:
            if (r15 == 0) goto L_0x0009;
        L_0x00b9:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mInTouchMode;
            r24 = r0;
            r25 = 0;
            r0 = r25;
            r1 = r24;
            r15.reportFocusChangedSerialized(r0, r1);
            goto L_0x0009;
        L_0x00d0:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x00d6:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0130 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0130 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mLosingFocus;	 Catch:{ all -> 0x0130 }
            r16 = r0;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0130 }
            r24 = r0;
            r26 = new java.util.ArrayList;	 Catch:{ all -> 0x0130 }
            r26.<init>();	 Catch:{ all -> 0x0130 }
            r0 = r26;
            r1 = r24;
            r1.mLosingFocus = r0;	 Catch:{ all -> 0x0130 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            r4 = r16.size();
            r13 = 0;
        L_0x010c:
            if (r13 >= r4) goto L_0x0009;
        L_0x010e:
            r0 = r16;
            r24 = r0.get(r13);
            r24 = (com.android.server.wm.WindowState) r24;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r25 = r0;
            r0 = r25;
            r0 = r0.mInTouchMode;
            r25 = r0;
            r26 = 0;
            r0 = r24;
            r1 = r26;
            r2 = r25;
            r0.reportFocusChangedSerialized(r1, r2);
            r13 = r13 + 1;
            goto L_0x010c;
        L_0x0130:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x0136:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0159 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0159 }
            r24 = r0;
            r24 = r24.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x0159 }
            r24.onWindowFreezeTimeout();	 Catch:{ all -> 0x0159 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x0159:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x015f:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x01cd }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r0 = r24;
            r0 = r0.mAppTransition;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r24 = r24.isTransitionSet();	 Catch:{ all -> 0x01cd }
            if (r24 != 0) goto L_0x01a9;
        L_0x0181:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r0 = r24;
            r0 = r0.mOpeningApps;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r24 = r24.isEmpty();	 Catch:{ all -> 0x01cd }
            r24 = r24 ^ 1;
            if (r24 != 0) goto L_0x01a9;
        L_0x0195:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r0 = r24;
            r0 = r0.mClosingApps;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r24 = r24.isEmpty();	 Catch:{ all -> 0x01cd }
            r24 = r24 ^ 1;
            if (r24 == 0) goto L_0x01c7;
        L_0x01a9:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r0 = r24;
            r0 = r0.mAppTransition;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r24.setTimeout();	 Catch:{ all -> 0x01cd }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowPlacerLocked;	 Catch:{ all -> 0x01cd }
            r24 = r0;
            r24.performSurfacePlacement();	 Catch:{ all -> 0x01cd }
        L_0x01c7:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x01cd:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x01d3:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mContext;
            r24 = r0;
            r24 = r24.getContentResolver();
            r25 = "window_animation_scale";
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r26 = r0;
            r26 = r26.mWindowAnimationScaleSetting;
            android.provider.Settings.Global.putFloat(r24, r25, r26);
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mContext;
            r24 = r0;
            r24 = r24.getContentResolver();
            r25 = "transition_animation_scale";
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r26 = r0;
            r26 = r26.mTransitionAnimationScaleSetting;
            android.provider.Settings.Global.putFloat(r24, r25, r26);
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mContext;
            r24 = r0;
            r24 = r24.getContentResolver();
            r25 = "animator_duration_scale";
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r26 = r0;
            r26 = r26.mAnimatorDurationScaleSetting;
            android.provider.Settings.Global.putFloat(r24, r25, r26);
            goto L_0x0009;
        L_0x0235:
            r0 = r30;
            r0 = r0.arg1;
            r17 = r0;
            switch(r17) {
                case 0: goto L_0x0240;
                case 1: goto L_0x026c;
                case 2: goto L_0x0298;
                default: goto L_0x023e;
            };
        L_0x023e:
            goto L_0x0009;
        L_0x0240:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r25 = r0;
            r0 = r25;
            r0 = r0.mContext;
            r25 = r0;
            r25 = r25.getContentResolver();
            r26 = "window_animation_scale";
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r27 = r0;
            r27 = r27.mWindowAnimationScaleSetting;
            r25 = android.provider.Settings.Global.getFloat(r25, r26, r27);
            r24.mWindowAnimationScaleSetting = r25;
            goto L_0x0009;
        L_0x026c:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r25 = r0;
            r0 = r25;
            r0 = r0.mContext;
            r25 = r0;
            r25 = r25.getContentResolver();
            r26 = "transition_animation_scale";
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r27 = r0;
            r27 = r27.mTransitionAnimationScaleSetting;
            r25 = android.provider.Settings.Global.getFloat(r25, r26, r27);
            r24.mTransitionAnimationScaleSetting = r25;
            goto L_0x0009;
        L_0x0298:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r25 = r0;
            r0 = r25;
            r0 = r0.mContext;
            r25 = r0;
            r25 = r25.getContentResolver();
            r26 = "animator_duration_scale";
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r27 = r0;
            r27 = r27.mAnimatorDurationScaleSetting;
            r25 = android.provider.Settings.Global.getFloat(r25, r26, r27);
            r24.mAnimatorDurationScaleSetting = r25;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r25 = 0;
            r24.dispatchNewAnimatorScaleLocked(r25);
            goto L_0x0009;
        L_0x02cf:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0335 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0335 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mAnimator;	 Catch:{ all -> 0x0335 }
            r24 = r0;
            r24 = r24.isAnimating();	 Catch:{ all -> 0x0335 }
            if (r24 != 0) goto L_0x0303;
        L_0x02f1:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0335 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mAnimator;	 Catch:{ all -> 0x0335 }
            r24 = r0;
            r24 = r24.isAnimationScheduled();	 Catch:{ all -> 0x0335 }
            if (r24 == 0) goto L_0x0315;
        L_0x0303:
            r26 = 2000; // 0x7d0 float:2.803E-42 double:9.88E-321;
            r24 = 15;
            r0 = r29;
            r1 = r24;
            r2 = r26;
            r0.sendEmptyMessageDelayed(r1, r2);	 Catch:{ all -> 0x0335 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return;
        L_0x0315:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0335 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mDisplayFrozen;	 Catch:{ all -> 0x0335 }
            r24 = r0;
            if (r24 == 0) goto L_0x0328;
        L_0x0323:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return;
        L_0x0328:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            r24 = java.lang.Runtime.getRuntime();
            r24.gc();
            goto L_0x0009;
        L_0x0335:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x033b:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r24.performEnableScreen();
            goto L_0x0009;
        L_0x0346:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x03a5 }
            r24 = "WindowManager";
            r26 = "App freeze timeout expired.";
            r0 = r24;
            r1 = r26;
            android.util.Slog.w(r0, r1);	 Catch:{ all -> 0x03a5 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x03a5 }
            r24 = r0;
            r26 = 2;
            r0 = r26;
            r1 = r24;
            r1.mWindowsFreezingScreen = r0;	 Catch:{ all -> 0x03a5 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x03a5 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mAppFreezeListeners;	 Catch:{ all -> 0x03a5 }
            r24 = r0;
            r24 = r24.size();	 Catch:{ all -> 0x03a5 }
            r13 = r24 + -1;
        L_0x0383:
            if (r13 < 0) goto L_0x039f;
        L_0x0385:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x03a5 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mAppFreezeListeners;	 Catch:{ all -> 0x03a5 }
            r24 = r0;
            r0 = r24;
            r24 = r0.get(r13);	 Catch:{ all -> 0x03a5 }
            r24 = (com.android.server.wm.WindowManagerService.AppFreezeListener) r24;	 Catch:{ all -> 0x03a5 }
            r24.onAppFreezeTimeout();	 Catch:{ all -> 0x03a5 }
            r13 = r13 + -1;
            goto L_0x0383;
        L_0x039f:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x03a5:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x03ab:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x03f5 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mClientFreezingScreen;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            if (r24 == 0) goto L_0x03ef;
        L_0x03c9:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            r26 = 0;
            r0 = r26;
            r1 = r24;
            r1.mClientFreezingScreen = r0;	 Catch:{ all -> 0x03f5 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            r26 = "client-timeout";
            r0 = r26;
            r1 = r24;
            r1.mLastFinishedFreezeSource = r0;	 Catch:{ all -> 0x03f5 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            r24.stopFreezingDisplayLocked();	 Catch:{ all -> 0x03f5 }
        L_0x03ef:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x03f5:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x03fb:
            r0 = r30;
            r0 = r0.obj;
            r24 = r0;
            r25 = 18;
            r0 = r29;
            r1 = r25;
            r2 = r24;
            r0.removeMessages(r1, r2);
            r0 = r30;
            r0 = r0.obj;
            r24 = r0;
            r24 = (java.lang.Integer) r24;
            r11 = r24.intValue();
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mRoot;
            r24 = r0;
            r0 = r24;
            r24 = r0.getDisplayContent(r11);
            if (r24 == 0) goto L_0x0009;
        L_0x042c:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0.sendNewConfiguration(r11);
            goto L_0x0009;
        L_0x0439:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowsChanged;
            r24 = r0;
            if (r24 == 0) goto L_0x0009;
        L_0x0447:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0474 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0474 }
            r24 = r0;
            r26 = 0;
            r0 = r26;
            r1 = r24;
            r1.mWindowsChanged = r0;	 Catch:{ all -> 0x0474 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r24.notifyWindowsChanged();
            goto L_0x0009;
        L_0x0474:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x047a:
            r0 = r30;
            r0 = r0.obj;
            r22 = r0;
            r22 = (android.os.IBinder) r22;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x04d2 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x04d2 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mDragState;	 Catch:{ all -> 0x04d2 }
            r24 = r0;
            if (r24 == 0) goto L_0x04cc;
        L_0x04a0:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x04d2 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mDragState;	 Catch:{ all -> 0x04d2 }
            r24 = r0;
            r24.unregister();	 Catch:{ all -> 0x04d2 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x04d2 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mDragState;	 Catch:{ all -> 0x04d2 }
            r24 = r0;
            r24.reset();	 Catch:{ all -> 0x04d2 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x04d2 }
            r24 = r0;
            r26 = 0;
            r0 = r26;
            r1 = r24;
            r1.mDragState = r0;	 Catch:{ all -> 0x04d2 }
        L_0x04cc:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x04d2:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x04d8:
            r0 = r30;
            r0 = r0.obj;
            r22 = r0;
            r22 = (android.os.IBinder) r22;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0527 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0527 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mDragState;	 Catch:{ all -> 0x0527 }
            r24 = r0;
            if (r24 == 0) goto L_0x0521;
        L_0x04fe:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0527 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mDragState;	 Catch:{ all -> 0x0527 }
            r24 = r0;
            r26 = 0;
            r0 = r26;
            r1 = r24;
            r1.mDragResult = r0;	 Catch:{ all -> 0x0527 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0527 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mDragState;	 Catch:{ all -> 0x0527 }
            r24 = r0;
            r24.endDragLw();	 Catch:{ all -> 0x0527 }
        L_0x0521:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x0527:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x052d:
            r0 = r30;
            r14 = r0.obj;
            r14 = (com.android.server.wm.DragState.InputInterceptor) r14;
            if (r14 == 0) goto L_0x0009;
        L_0x0535:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x054e }
            r14.tearDown();	 Catch:{ all -> 0x054e }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x054e:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x0554:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r24.notifyHardKeyboardStatusChange();
            goto L_0x0009;
        L_0x055f:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r24.performBootTimeout();
            goto L_0x0009;
        L_0x056a:
            r7 = 0;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x05d7 }
            r24 = "WindowManager";
            r26 = new java.lang.StringBuilder;	 Catch:{ all -> 0x05d7 }
            r26.<init>();	 Catch:{ all -> 0x05d7 }
            r27 = "Timeout waiting for drawn: undrawn=";
            r26 = r26.append(r27);	 Catch:{ all -> 0x05d7 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x05d7 }
            r27 = r0;
            r0 = r27;
            r0 = r0.mWaitingForDrawn;	 Catch:{ all -> 0x05d7 }
            r27 = r0;
            r26 = r26.append(r27);	 Catch:{ all -> 0x05d7 }
            r26 = r26.toString();	 Catch:{ all -> 0x05d7 }
            r0 = r24;
            r1 = r26;
            android.util.Slog.w(r0, r1);	 Catch:{ all -> 0x05d7 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x05d7 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mWaitingForDrawn;	 Catch:{ all -> 0x05d7 }
            r24 = r0;
            r24.clear();	 Catch:{ all -> 0x05d7 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x05d7 }
            r24 = r0;
            r0 = r24;
            r7 = r0.mWaitingForDrawnCallback;	 Catch:{ all -> 0x05d7 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x05d7 }
            r24 = r0;
            r26 = 0;
            r0 = r26;
            r1 = r24;
            r1.mWaitingForDrawnCallback = r0;	 Catch:{ all -> 0x05d7 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            if (r7 == 0) goto L_0x0009;
        L_0x05d2:
            r7.run();
            goto L_0x0009;
        L_0x05d7:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x05dd:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r30;
            r0 = r0.arg1;
            r25 = r0;
            r0 = r30;
            r0 = r0.arg2;
            r26 = r0;
            r24.showStrictModeViolation(r25, r26);
            goto L_0x0009;
        L_0x05f4:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r25 = r0;
            r0 = r30;
            r0 = r0.arg1;
            r24 = r0;
            r26 = 1;
            r0 = r24;
            r1 = r26;
            if (r0 != r1) goto L_0x0613;
        L_0x0608:
            r24 = 1;
        L_0x060a:
            r0 = r25;
            r1 = r24;
            r0.showCircularMask(r1);
            goto L_0x0009;
        L_0x0613:
            r24 = 0;
            goto L_0x060a;
        L_0x0616:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r24.showEmulatorDisplayOverlay();
            goto L_0x0009;
        L_0x0621:
            r0 = r30;
            r0 = r0.obj;	 Catch:{ RemoteException -> 0x0630 }
            r24 = r0;
            r24 = (android.os.IRemoteCallback) r24;	 Catch:{ RemoteException -> 0x0630 }
            r25 = 0;
            r24.sendResult(r25);	 Catch:{ RemoteException -> 0x0630 }
            goto L_0x0009;
        L_0x0630:
            r12 = move-exception;
            goto L_0x0009;
        L_0x0633:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r25 = r0;
            r0 = r30;
            r0 = r0.obj;
            r24 = r0;
            r24 = (com.android.server.wm.DisplayContent) r24;
            r0 = r30;
            r0 = r0.arg1;
            r26 = r0;
            r0 = r30;
            r0 = r0.arg2;
            r27 = r0;
            r0 = r25;
            r1 = r24;
            r2 = r26;
            r3 = r27;
            r0.handleTapOutsideTask(r1, r2, r3);
            goto L_0x0009;
        L_0x065a:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r24.finishPositioning();
            goto L_0x0009;
        L_0x0665:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ RemoteException -> 0x0682 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mActivityManager;	 Catch:{ RemoteException -> 0x0682 }
            r25 = r0;
            r0 = r30;
            r0 = r0.obj;	 Catch:{ RemoteException -> 0x0682 }
            r24 = r0;
            r24 = (android.os.IBinder) r24;	 Catch:{ RemoteException -> 0x0682 }
            r0 = r25;
            r1 = r24;
            r0.notifyActivityDrawn(r1);	 Catch:{ RemoteException -> 0x0682 }
            goto L_0x0009;
        L_0x0682:
            r12 = move-exception;
            goto L_0x0009;
        L_0x0685:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x06b8 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x06b8 }
            r24 = r0;
            r0 = r24;
            r7 = r0.mWaitingForDrawnCallback;	 Catch:{ all -> 0x06b8 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x06b8 }
            r24 = r0;
            r26 = 0;
            r0 = r26;
            r1 = r24;
            r1.mWaitingForDrawnCallback = r0;	 Catch:{ all -> 0x06b8 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            if (r7 == 0) goto L_0x0009;
        L_0x06b3:
            r7.run();
            goto L_0x0009;
        L_0x06b8:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x06be:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r19 = r24.getCurrentAnimatorScale();
            android.animation.ValueAnimator.setDurationScale(r19);
            r0 = r30;
            r0 = r0.obj;
            r20 = r0;
            r20 = (com.android.server.wm.Session) r20;
            if (r20 == 0) goto L_0x06e7;
        L_0x06d5:
            r0 = r20;
            r0 = r0.mCallback;	 Catch:{ RemoteException -> 0x06e4 }
            r24 = r0;
            r0 = r24;
            r1 = r19;
            r0.onAnimatorScaleChanged(r1);	 Catch:{ RemoteException -> 0x06e4 }
            goto L_0x0009;
        L_0x06e4:
            r12 = move-exception;
            goto L_0x0009;
        L_0x06e7:
            r8 = new java.util.ArrayList;
            r8.<init>();
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0750 }
            r13 = 0;
        L_0x06fd:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0750 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mSessions;	 Catch:{ all -> 0x0750 }
            r24 = r0;
            r24 = r24.size();	 Catch:{ all -> 0x0750 }
            r0 = r24;
            if (r13 >= r0) goto L_0x0733;
        L_0x0711:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0750 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mSessions;	 Catch:{ all -> 0x0750 }
            r24 = r0;
            r0 = r24;
            r24 = r0.valueAt(r13);	 Catch:{ all -> 0x0750 }
            r24 = (com.android.server.wm.Session) r24;	 Catch:{ all -> 0x0750 }
            r0 = r24;
            r0 = r0.mCallback;	 Catch:{ all -> 0x0750 }
            r24 = r0;
            r0 = r24;
            r8.add(r0);	 Catch:{ all -> 0x0750 }
            r13 = r13 + 1;
            goto L_0x06fd;
        L_0x0733:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            r13 = 0;
        L_0x0738:
            r24 = r8.size();
            r0 = r24;
            if (r13 >= r0) goto L_0x0009;
        L_0x0740:
            r24 = r8.get(r13);	 Catch:{ RemoteException -> 0x0a22 }
            r24 = (android.view.IWindowSessionCallback) r24;	 Catch:{ RemoteException -> 0x0a22 }
            r0 = r24;
            r1 = r19;
            r0.onAnimatorScaleChanged(r1);	 Catch:{ RemoteException -> 0x0a22 }
        L_0x074d:
            r13 = r13 + 1;
            goto L_0x0738;
        L_0x0750:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x0756:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0781 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0781 }
            r24 = r0;
            r6 = r24.checkBootAnimationCompleteLocked();	 Catch:{ all -> 0x0781 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            if (r6 == 0) goto L_0x0009;
        L_0x0776:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r24.performEnableScreen();
            goto L_0x0009;
        L_0x0781:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x0787:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x07ba }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x07ba }
            r24 = r0;
            r26 = 0;
            r0 = r26;
            r1 = r24;
            r1.mLastANRState = r0;	 Catch:{ all -> 0x07ba }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mAmInternal;
            r24 = r0;
            r24.clearSavedANRState();
            goto L_0x0009;
        L_0x07ba:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x07c0:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x07fd }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x07fd }
            r24 = r0;
            r0 = r24;
            r0 = r0.mRoot;	 Catch:{ all -> 0x07fd }
            r24 = r0;
            r0 = r24;
            r0 = r0.mWallpaperController;	 Catch:{ all -> 0x07fd }
            r24 = r0;
            r24 = r24.processWallpaperDrawPendingTimeout();	 Catch:{ all -> 0x07fd }
            if (r24 == 0) goto L_0x07f7;
        L_0x07e8:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x07fd }
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowPlacerLocked;	 Catch:{ all -> 0x07fd }
            r24 = r0;
            r24.performSurfacePlacement();	 Catch:{ all -> 0x07fd }
        L_0x07f7:
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x07fd:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x0803:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0833 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0833 }
            r24 = r0;
            r10 = r24.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x0833 }
            r24 = r10.getDockedDividerController();	 Catch:{ all -> 0x0833 }
            r26 = 0;
            r0 = r24;
            r1 = r26;
            r0.reevaluateVisibility(r1);	 Catch:{ all -> 0x0833 }
            r10.adjustForImeIfNeeded();	 Catch:{ all -> 0x0833 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x0833:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x0839:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x088c }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x088c }
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowReplacementTimeouts;	 Catch:{ all -> 0x088c }
            r24 = r0;
            r24 = r24.size();	 Catch:{ all -> 0x088c }
            r13 = r24 + -1;
        L_0x085b:
            if (r13 < 0) goto L_0x0877;
        L_0x085d:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x088c }
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowReplacementTimeouts;	 Catch:{ all -> 0x088c }
            r24 = r0;
            r0 = r24;
            r21 = r0.get(r13);	 Catch:{ all -> 0x088c }
            r21 = (com.android.server.wm.AppWindowToken) r21;	 Catch:{ all -> 0x088c }
            r21.onWindowReplacementTimeout();	 Catch:{ all -> 0x088c }
            r13 = r13 + -1;
            goto L_0x085b;
        L_0x0877:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x088c }
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowReplacementTimeouts;	 Catch:{ all -> 0x088c }
            r24 = r0;
            r24.clear();	 Catch:{ all -> 0x088c }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x088c:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x0892:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mAmInternal;
            r25 = r0;
            r0 = r30;
            r0 = r0.obj;
            r24 = r0;
            r24 = (android.util.SparseIntArray) r24;
            r26 = r30.getWhen();
            r0 = r25;
            r1 = r24;
            r2 = r26;
            r0.notifyAppTransitionStarting(r1, r2);
            goto L_0x0009;
        L_0x08b5:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mAmInternal;
            r24 = r0;
            r24.notifyAppTransitionCancelled();
            goto L_0x0009;
        L_0x08c6:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mAmInternal;
            r24 = r0;
            r24.notifyAppTransitionFinished();
            goto L_0x0009;
        L_0x08d7:
            r0 = r30;
            r0 = r0.obj;
            r23 = r0;
            r23 = (com.android.server.wm.WindowState) r23;
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0922 }
            r0 = r23;
            r0 = r0.mAttrs;	 Catch:{ all -> 0x0922 }
            r24 = r0;
            r0 = r24;
            r0 = r0.flags;	 Catch:{ all -> 0x0922 }
            r26 = r0;
            r0 = r26;
            r0 = r0 & -129;
            r26 = r0;
            r0 = r26;
            r1 = r24;
            r1.flags = r0;	 Catch:{ all -> 0x0922 }
            r23.hidePermanentlyLw();	 Catch:{ all -> 0x0922 }
            r23.setDisplayLayoutNeeded();	 Catch:{ all -> 0x0922 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0922 }
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowPlacerLocked;	 Catch:{ all -> 0x0922 }
            r24 = r0;
            r24.performSurfacePlacement();	 Catch:{ all -> 0x0922 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x0922:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x0928:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mAmInternal;
            r25 = r0;
            r0 = r30;
            r0 = r0.arg1;
            r24 = r0;
            r26 = 1;
            r0 = r24;
            r1 = r26;
            if (r0 != r1) goto L_0x094d;
        L_0x0942:
            r24 = 1;
        L_0x0944:
            r0 = r25;
            r1 = r24;
            r0.notifyDockedStackMinimizedChanged(r1);
            goto L_0x0009;
        L_0x094d:
            r24 = 0;
            goto L_0x0944;
        L_0x0950:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0995 }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0995 }
            r26 = r0;
            r0 = r30;
            r0 = r0.obj;	 Catch:{ all -> 0x0995 }
            r24 = r0;
            r24 = (com.android.server.wm.DisplayContent) r24;	 Catch:{ all -> 0x0995 }
            r0 = r30;
            r0 = r0.arg1;	 Catch:{ all -> 0x0995 }
            r27 = r0;
            r0 = r27;
            r0 = (float) r0;	 Catch:{ all -> 0x0995 }
            r27 = r0;
            r0 = r30;
            r0 = r0.arg2;	 Catch:{ all -> 0x0995 }
            r28 = r0;
            r0 = r28;
            r0 = (float) r0;	 Catch:{ all -> 0x0995 }
            r28 = r0;
            r0 = r26;
            r1 = r24;
            r2 = r27;
            r3 = r28;
            r0.restorePointerIconLocked(r1, r2, r3);	 Catch:{ all -> 0x0995 }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x0995:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x099b:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mWindowMap;
            r25 = r0;
            monitor-enter(r25);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x09be }
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x09be }
            r24 = r0;
            r9 = r24.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x09be }
            r9.onSeamlessRotationTimeout();	 Catch:{ all -> 0x09be }
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            goto L_0x0009;
        L_0x09be:
            r24 = move-exception;
            monitor-exit(r25);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r24;
        L_0x09c4:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mAmInternal;
            r25 = r0;
            r0 = r30;
            r0 = r0.obj;
            r24 = r0;
            r24 = (java.lang.Runnable) r24;
            r0 = r25;
            r1 = r24;
            r0.notifyKeyguardFlagsChanged(r1);
            goto L_0x0009;
        L_0x09e1:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mAmInternal;
            r24 = r0;
            r24.notifyKeyguardTrustedChanged();
            goto L_0x0009;
        L_0x09f2:
            r0 = r29;
            r0 = com.android.server.wm.WindowManagerService.this;
            r24 = r0;
            r0 = r24;
            r0 = r0.mAmInternal;
            r25 = r0;
            r0 = r30;
            r0 = r0.arg1;
            r26 = r0;
            r0 = r30;
            r0 = r0.arg2;
            r24 = r0;
            r27 = 1;
            r0 = r24;
            r1 = r27;
            if (r0 != r1) goto L_0x0a1f;
        L_0x0a12:
            r24 = 1;
        L_0x0a14:
            r0 = r25;
            r1 = r26;
            r2 = r24;
            r0.setHasOverlayUi(r1, r2);
            goto L_0x0009;
        L_0x0a1f:
            r24 = 0;
            goto L_0x0a14;
        L_0x0a22:
            r12 = move-exception;
            goto L_0x074d;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.H.handleMessage(android.os.Message):void");
        }
    }

    private final class LocalService extends WindowManagerInternal {
        private LocalService() {
        }

        public void requestTraversalFromDisplayManager() {
            WindowManagerService.this.requestTraversal();
        }

        public void setMagnificationSpec(MagnificationSpec spec) {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    if (WindowManagerService.this.mAccessibilityController != null) {
                        WindowManagerService.this.mAccessibilityController.setMagnificationSpecLocked(spec);
                    } else {
                        throw new IllegalStateException("Magnification callbacks not set!");
                    }
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
            if (Binder.getCallingPid() != Process.myPid()) {
                spec.recycle();
            }
        }

        public void setForceShowMagnifiableBounds(boolean show) {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    if (WindowManagerService.this.mAccessibilityController != null) {
                        WindowManagerService.this.mAccessibilityController.setForceShowMagnifiableBoundsLocked(show);
                    } else {
                        throw new IllegalStateException("Magnification callbacks not set!");
                    }
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        public void getMagnificationRegion(Region magnificationRegion) {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    if (WindowManagerService.this.mAccessibilityController != null) {
                        WindowManagerService.this.mAccessibilityController.getMagnificationRegionLocked(magnificationRegion);
                    } else {
                        throw new IllegalStateException("Magnification callbacks not set!");
                    }
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.view.MagnificationSpec getCompatibleMagnificationSpecForWindow(android.os.IBinder r7) {
            /*
            r6 = this;
            r5 = 0;
            r2 = com.android.server.wm.WindowManagerService.this;
            r3 = r2.mWindowMap;
            monitor-enter(r3);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0055 }
            r2 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0055 }
            r2 = r2.mWindowMap;	 Catch:{ all -> 0x0055 }
            r1 = r2.get(r7);	 Catch:{ all -> 0x0055 }
            r1 = (com.android.server.wm.WindowState) r1;	 Catch:{ all -> 0x0055 }
            if (r1 != 0) goto L_0x001a;
        L_0x0015:
            monitor-exit(r3);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return r5;
        L_0x001a:
            r0 = 0;
            r2 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0055 }
            r2 = r2.mAccessibilityController;	 Catch:{ all -> 0x0055 }
            if (r2 == 0) goto L_0x0029;
        L_0x0021:
            r2 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0055 }
            r2 = r2.mAccessibilityController;	 Catch:{ all -> 0x0055 }
            r0 = r2.getMagnificationSpecForWindowLocked(r1);	 Catch:{ all -> 0x0055 }
        L_0x0029:
            if (r0 == 0) goto L_0x0031;
        L_0x002b:
            r2 = r0.isNop();	 Catch:{ all -> 0x0055 }
            if (r2 == 0) goto L_0x003e;
        L_0x0031:
            r2 = r1.mGlobalScale;	 Catch:{ all -> 0x0055 }
            r4 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
            r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1));
            if (r2 != 0) goto L_0x003e;
        L_0x0039:
            monitor-exit(r3);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return r5;
        L_0x003e:
            if (r0 != 0) goto L_0x0050;
        L_0x0040:
            r0 = android.view.MagnificationSpec.obtain();	 Catch:{ all -> 0x0055 }
        L_0x0044:
            r2 = r0.scale;	 Catch:{ all -> 0x0055 }
            r4 = r1.mGlobalScale;	 Catch:{ all -> 0x0055 }
            r2 = r2 * r4;
            r0.scale = r2;	 Catch:{ all -> 0x0055 }
            monitor-exit(r3);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return r0;
        L_0x0050:
            r0 = android.view.MagnificationSpec.obtain(r0);	 Catch:{ all -> 0x0055 }
            goto L_0x0044;
        L_0x0055:
            r2 = move-exception;
            monitor-exit(r3);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r2;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.LocalService.getCompatibleMagnificationSpecForWindow(android.os.IBinder):android.view.MagnificationSpec");
        }

        public void setMagnificationCallbacks(MagnificationCallbacks callbacks) {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    if (WindowManagerService.this.mAccessibilityController == null) {
                        WindowManagerService.this.mAccessibilityController = new AccessibilityController(WindowManagerService.this);
                    }
                    WindowManagerService.this.mAccessibilityController.setMagnificationCallbacksLocked(callbacks);
                    if (!WindowManagerService.this.mAccessibilityController.hasCallbacksLocked()) {
                        WindowManagerService.this.mAccessibilityController = null;
                    }
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        public void setWindowsForAccessibilityCallback(WindowsForAccessibilityCallback callback) {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    if (WindowManagerService.this.mAccessibilityController == null) {
                        WindowManagerService.this.mAccessibilityController = new AccessibilityController(WindowManagerService.this);
                    }
                    WindowManagerService.this.mAccessibilityController.setWindowsForAccessibilityCallback(callback);
                    if (!WindowManagerService.this.mAccessibilityController.hasCallbacksLocked()) {
                        WindowManagerService.this.mAccessibilityController = null;
                    }
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        public void setInputFilter(IInputFilter filter) {
            WindowManagerService.this.mInputManager.setInputFilter(filter);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.os.IBinder getFocusedWindowToken() {
            /*
            r4 = this;
            r3 = 0;
            r1 = com.android.server.wm.WindowManagerService.this;
            r2 = r1.mWindowMap;
            monitor-enter(r2);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0021 }
            r1 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0021 }
            r0 = r1.getFocusedWindowLocked();	 Catch:{ all -> 0x0021 }
            if (r0 == 0) goto L_0x001c;
        L_0x0011:
            r1 = r0.mClient;	 Catch:{ all -> 0x0021 }
            r1 = r1.asBinder();	 Catch:{ all -> 0x0021 }
            monitor-exit(r2);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return r1;
        L_0x001c:
            monitor-exit(r2);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return r3;
        L_0x0021:
            r1 = move-exception;
            monitor-exit(r2);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.LocalService.getFocusedWindowToken():android.os.IBinder");
        }

        public boolean isKeyguardLocked() {
            return WindowManagerService.this.isKeyguardLocked();
        }

        public boolean isKeyguardShowingAndNotOccluded() {
            return WindowManagerService.this.isKeyguardShowingAndNotOccluded();
        }

        public void showGlobalActions() {
            WindowManagerService.this.showGlobalActions();
        }

        public void getWindowFrame(IBinder token, Rect outBounds) {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    WindowState windowState = (WindowState) WindowManagerService.this.mWindowMap.get(token);
                    if (windowState != null) {
                        outBounds.set(windowState.mFrame);
                    } else {
                        outBounds.setEmpty();
                    }
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        public void waitForAllWindowsDrawn(Runnable callback, long timeout) {
            boolean allWindowsDrawn = false;
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    WindowManagerService.this.mWaitingForDrawnCallback = callback;
                    WindowManagerService.this.getDefaultDisplayContentLocked().waitForAllWindowsDrawn();
                    WindowManagerService.this.mWindowPlacerLocked.requestTraversal();
                    WindowManagerService.this.mH.removeMessages(24);
                    if (WindowManagerService.this.mWaitingForDrawn.isEmpty()) {
                        allWindowsDrawn = true;
                    } else {
                        WindowManagerService.this.mH.sendEmptyMessageDelayed(24, timeout);
                        WindowManagerService.this.checkDrawnWindowsLocked();
                    }
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
            if (allWindowsDrawn) {
                callback.run();
            }
        }

        public void addWindowToken(IBinder token, int type, int displayId) {
            WindowManagerService.this.addWindowToken(token, type, displayId);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void removeWindowToken(android.os.IBinder r7, boolean r8, int r9) {
            /*
            r6 = this;
            r2 = com.android.server.wm.WindowManagerService.this;
            r3 = r2.mWindowMap;
            monitor-enter(r3);
            com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0070 }
            if (r8 == 0) goto L_0x0066;
        L_0x000a:
            r2 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0070 }
            r2 = r2.mRoot;	 Catch:{ all -> 0x0070 }
            r0 = r2.getDisplayContent(r9);	 Catch:{ all -> 0x0070 }
            if (r0 != 0) goto L_0x003e;
        L_0x0014:
            r2 = "WindowManager";
            r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0070 }
            r4.<init>();	 Catch:{ all -> 0x0070 }
            r5 = "removeWindowToken: Attempted to remove token: ";
            r4 = r4.append(r5);	 Catch:{ all -> 0x0070 }
            r4 = r4.append(r7);	 Catch:{ all -> 0x0070 }
            r5 = " for non-exiting displayId=";
            r4 = r4.append(r5);	 Catch:{ all -> 0x0070 }
            r4 = r4.append(r9);	 Catch:{ all -> 0x0070 }
            r4 = r4.toString();	 Catch:{ all -> 0x0070 }
            android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x0070 }
            monitor-exit(r3);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return;
        L_0x003e:
            r1 = r0.removeWindowToken(r7);	 Catch:{ all -> 0x0070 }
            if (r1 != 0) goto L_0x0063;
        L_0x0044:
            r2 = "WindowManager";
            r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0070 }
            r4.<init>();	 Catch:{ all -> 0x0070 }
            r5 = "removeWindowToken: Attempted to remove non-existing token: ";
            r4 = r4.append(r5);	 Catch:{ all -> 0x0070 }
            r4 = r4.append(r7);	 Catch:{ all -> 0x0070 }
            r4 = r4.toString();	 Catch:{ all -> 0x0070 }
            android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x0070 }
            monitor-exit(r3);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return;
        L_0x0063:
            r1.removeAllWindowsIfPossible();	 Catch:{ all -> 0x0070 }
        L_0x0066:
            r2 = com.android.server.wm.WindowManagerService.this;	 Catch:{ all -> 0x0070 }
            r2.removeWindowToken(r7, r9);	 Catch:{ all -> 0x0070 }
            monitor-exit(r3);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            return;
        L_0x0070:
            r2 = move-exception;
            monitor-exit(r3);
            com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
            throw r2;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.LocalService.removeWindowToken(android.os.IBinder, boolean, int):void");
        }

        public void registerAppTransitionListener(AppTransitionListener listener) {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    WindowManagerService.this.mAppTransition.registerListenerLocked(listener);
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        public int getInputMethodWindowVisibleHeight() {
            int inputMethodWindowVisibleHeightLw;
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    inputMethodWindowVisibleHeightLw = WindowManagerService.this.mPolicy.getInputMethodWindowVisibleHeightLw();
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
            return inputMethodWindowVisibleHeightLw;
        }

        public void saveLastInputMethodWindowForTransition() {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    if (WindowManagerService.this.mInputMethodWindow != null) {
                        WindowManagerService.this.mPolicy.setLastInputMethodWindowLw(WindowManagerService.this.mInputMethodWindow, WindowManagerService.this.mInputMethodTarget);
                    }
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        public void clearLastInputMethodWindowForTransition() {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    WindowManagerService.this.mPolicy.setLastInputMethodWindowLw(null, null);
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        public void updateInputMethodWindowStatus(IBinder imeToken, boolean imeWindowVisible, boolean dismissImeOnBackKeyPressed, IBinder targetWindowToken) {
            WindowManagerService.this.mPolicy.setDismissImeOnBackKeyPressed(dismissImeOnBackKeyPressed);
        }

        public boolean isHardKeyboardAvailable() {
            boolean z;
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    z = WindowManagerService.this.mHardKeyboardAvailable;
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
            return z;
        }

        public void setOnHardKeyboardStatusChangeListener(OnHardKeyboardStatusChangeListener listener) {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    WindowManagerService.this.mHardKeyboardStatusChangeListener = listener;
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        public boolean isStackVisible(int stackId) {
            boolean isStackVisible;
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    isStackVisible = WindowManagerService.this.getDefaultDisplayContentLocked().isStackVisible(stackId);
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
            return isStackVisible;
        }

        public boolean isDockedDividerResizing() {
            boolean isResizing;
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    isResizing = WindowManagerService.this.getDefaultDisplayContentLocked().getDockedDividerController().isResizing();
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
            return isResizing;
        }

        public void computeWindowsForAccessibility() {
            synchronized (WindowManagerService.this.mWindowMap) {
                try {
                    WindowManagerService.boostPriorityForLockedSection();
                    AccessibilityController accessibilityController = WindowManagerService.this.mAccessibilityController;
                } finally {
                    WindowManagerService.resetPriorityAfterLockedSection();
                }
            }
            if (accessibilityController != null) {
                accessibilityController.performComputeChangedWindowsNotLocked();
            }
        }

        public void setVr2dDisplayId(int vr2dDisplayId) {
            synchronized (WindowManagerService.this) {
                WindowManagerService.this.mVr2dDisplayId = vr2dDisplayId;
            }
        }
    }

    private static class MousePositionTracker implements PointerEventListener {
        private boolean mLatestEventWasMouse;
        private float mLatestMouseX;
        private float mLatestMouseY;

        private MousePositionTracker() {
        }

        void updatePosition(float x, float y) {
            synchronized (this) {
                this.mLatestEventWasMouse = true;
                this.mLatestMouseX = x;
                this.mLatestMouseY = y;
            }
        }

        public void onPointerEvent(MotionEvent motionEvent) {
            if (motionEvent.isFromSource(UsbACInterface.FORMAT_III_IEC1937_MPEG1_Layer1)) {
                updatePosition(motionEvent.getRawX(), motionEvent.getRawY());
                return;
            }
            synchronized (this) {
                this.mLatestEventWasMouse = false;
            }
        }
    }

    class RotationWatcher {
        final DeathRecipient mDeathRecipient;
        final int mDisplayId;
        final IRotationWatcher mWatcher;

        RotationWatcher(IRotationWatcher watcher, DeathRecipient deathRecipient, int displayId) {
            this.mWatcher = watcher;
            this.mDeathRecipient = deathRecipient;
            this.mDisplayId = displayId;
        }
    }

    private final class SettingsObserver extends ContentObserver {
        private final Uri mAnimationDurationScaleUri = Global.getUriFor("animator_duration_scale");
        private final Uri mDisplayInversionEnabledUri = Secure.getUriFor("accessibility_display_inversion_enabled");
        private final Uri mTransitionAnimationScaleUri = Global.getUriFor("transition_animation_scale");
        private final Uri mWindowAnimationScaleUri = Global.getUriFor("window_animation_scale");

        public SettingsObserver() {
            super(new Handler());
            ContentResolver resolver = WindowManagerService.this.mContext.getContentResolver();
            resolver.registerContentObserver(this.mDisplayInversionEnabledUri, false, this, -1);
            resolver.registerContentObserver(this.mWindowAnimationScaleUri, false, this, -1);
            resolver.registerContentObserver(this.mTransitionAnimationScaleUri, false, this, -1);
            resolver.registerContentObserver(this.mAnimationDurationScaleUri, false, this, -1);
        }

        public void onChange(boolean selfChange, Uri uri) {
            if (uri != null) {
                if (this.mDisplayInversionEnabledUri.equals(uri)) {
                    WindowManagerService.this.updateCircularDisplayMaskIfNeeded();
                } else {
                    int mode;
                    if (this.mWindowAnimationScaleUri.equals(uri)) {
                        mode = 0;
                    } else if (this.mTransitionAnimationScaleUri.equals(uri)) {
                        mode = 1;
                    } else if (this.mAnimationDurationScaleUri.equals(uri)) {
                        mode = 2;
                    } else {
                        return;
                    }
                    WindowManagerService.this.mH.sendMessage(WindowManagerService.this.mH.obtainMessage(51, mode, 0));
                }
            }
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    private @interface UpdateAnimationScaleMode {
    }

    int getDragLayerLocked() {
        return (this.mPolicy.getWindowLayerFromTypeLw(2016) * 10000) + 1000;
    }

    static void boostPriorityForLockedSection() {
        sThreadPriorityBooster.boost();
    }

    static void resetPriorityAfterLockedSection() {
        sThreadPriorityBooster.reset();
    }

    void openSurfaceTransaction() {
        try {
            Trace.traceBegin(32, "openSurfaceTransaction");
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                if (this.mRoot.mSurfaceTraceEnabled) {
                    this.mRoot.mRemoteEventTrace.openSurfaceTransaction();
                }
                SurfaceControl.openTransaction();
            }
            resetPriorityAfterLockedSection();
            Trace.traceEnd(32);
        } catch (Throwable th) {
            Trace.traceEnd(32);
        }
    }

    void closeSurfaceTransaction() {
        try {
            Trace.traceBegin(32, "closeSurfaceTransaction");
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                if (this.mRoot.mSurfaceTraceEnabled) {
                    this.mRoot.mRemoteEventTrace.closeSurfaceTransaction();
                }
                SurfaceControl.closeTransaction();
            }
            resetPriorityAfterLockedSection();
            Trace.traceEnd(32);
        } catch (Throwable th) {
            Trace.traceEnd(32);
        }
    }

    void executeEmptyAnimationTransaction() {
        try {
            Trace.traceBegin(32, "openSurfaceTransaction");
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                if (this.mRoot.mSurfaceTraceEnabled) {
                    this.mRoot.mRemoteEventTrace.openSurfaceTransaction();
                }
                SurfaceControl.openTransaction();
                SurfaceControl.setAnimationTransaction();
                if (this.mRoot.mSurfaceTraceEnabled) {
                    this.mRoot.mRemoteEventTrace.closeSurfaceTransaction();
                }
            }
            resetPriorityAfterLockedSection();
            Trace.traceEnd(32);
            try {
                Trace.traceBegin(32, "closeSurfaceTransaction");
                SurfaceControl.closeTransaction();
            } finally {
                Trace.traceEnd(32);
            }
        } catch (Throwable th) {
            Trace.traceEnd(32);
        }
    }

    static WindowManagerService getInstance() {
        return sInstance;
    }

    public static WindowManagerService main(Context context, InputManagerService im, boolean haveInputMethods, boolean showBootMsgs, boolean onlyCore, WindowManagerPolicy policy) {
        DisplayThread.getHandler().runWithScissors(new com.android.server.wm.-$Lambda$eBBEuGZ8VbEXJy0r5EYYbvnl-8w.AnonymousClass1(haveInputMethods, showBootMsgs, onlyCore, context, im, policy), 0);
        return sInstance;
    }

    private void initPolicy() {
        UiThread.getHandler().runWithScissors(new Runnable() {
            public void run() {
                WindowManagerPolicyThread.set(Thread.currentThread(), Looper.myLooper());
                WindowManagerService.this.mPolicy.init(WindowManagerService.this.mContext, WindowManagerService.this, WindowManagerService.this);
            }
        }, 0);
    }

    private WindowManagerService(Context context, InputManagerService inputManager, boolean haveInputMethods, boolean showBootMsgs, boolean onlyCore, WindowManagerPolicy policy) {
        LockGuard.installLock((Object) this, 5);
        this.mRoot = new RootWindowContainer(this);
        this.mContext = context;
        this.mHaveInputMethods = haveInputMethods;
        this.mAllowBootMessages = showBootMsgs;
        this.mOnlyCore = onlyCore;
        this.mLimitedAlphaCompositing = context.getResources().getBoolean(17957006);
        this.mHasPermanentDpad = context.getResources().getBoolean(17956973);
        this.mInTouchMode = context.getResources().getBoolean(17956916);
        this.mDrawLockTimeoutMillis = (long) context.getResources().getInteger(17694779);
        this.mAllowAnimationsInLowPowerMode = context.getResources().getBoolean(17956871);
        this.mMaxUiWidth = context.getResources().getInteger(17694805);
        this.mInputManager = inputManager;
        this.mDisplayManagerInternal = (DisplayManagerInternal) LocalServices.getService(DisplayManagerInternal.class);
        this.mDisplaySettings = new DisplaySettings();
        this.mDisplaySettings.readSettingsLocked();
        this.mWindowPlacerLocked = new WindowSurfacePlacer(this);
        this.mPolicy = policy;
        this.mTaskSnapshotController = new TaskSnapshotController(this);
        LocalServices.addService(WindowManagerPolicy.class, this.mPolicy);
        if (this.mInputManager != null) {
            InputChannel inputChannel = this.mInputManager.monitorInput(TAG);
            this.mPointerEventDispatcher = inputChannel != null ? new PointerEventDispatcher(inputChannel) : null;
        } else {
            this.mPointerEventDispatcher = null;
        }
        this.mFxSession = new SurfaceSession();
        this.mDisplayManager = (DisplayManager) context.getSystemService("display");
        this.mDisplays = this.mDisplayManager.getDisplays();
        for (Display display : this.mDisplays) {
            createDisplayContentLocked(display);
        }
        this.mKeyguardDisableHandler = new KeyguardDisableHandler(this.mContext, this.mPolicy);
        this.mPowerManager = (PowerManager) context.getSystemService("power");
        this.mPowerManagerInternal = (PowerManagerInternal) LocalServices.getService(PowerManagerInternal.class);
        if (this.mPowerManagerInternal != null) {
            this.mPowerManagerInternal.registerLowPowerModeObserver(new LowPowerModeListener() {
                public int getServiceType() {
                    return 3;
                }

                public void onLowPowerModeChanged(PowerSaveState result) {
                    synchronized (WindowManagerService.this.mWindowMap) {
                        try {
                            WindowManagerService.boostPriorityForLockedSection();
                            boolean enabled = result.batterySaverEnabled;
                            if (!(WindowManagerService.this.mAnimationsDisabled == enabled || (WindowManagerService.this.mAllowAnimationsInLowPowerMode ^ 1) == 0)) {
                                WindowManagerService.this.mAnimationsDisabled = enabled;
                                WindowManagerService.this.dispatchNewAnimatorScaleLocked(null);
                            }
                        } finally {
                            WindowManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                }
            });
            this.mAnimationsDisabled = this.mPowerManagerInternal.getLowPowerState(3).batterySaverEnabled;
        }
        this.mScreenFrozenLock = this.mPowerManager.newWakeLock(1, "SCREEN_FROZEN");
        this.mScreenFrozenLock.setReferenceCounted(false);
        this.mAppTransition = new AppTransition(context, this);
        this.mAppTransition.registerListenerLocked(this.mActivityManagerAppTransitionNotifier);
        AnimationHandler animationHandler = new AnimationHandler();
        animationHandler.setProvider(new SfVsyncFrameCallbackProvider());
        this.mBoundsAnimationController = new BoundsAnimationController(context, this.mAppTransition, AnimationThread.getHandler(), animationHandler);
        this.mActivityManager = ActivityManager.getService();
        this.mAmInternal = (ActivityManagerInternal) LocalServices.getService(ActivityManagerInternal.class);
        this.mAppOps = (AppOpsManager) context.getSystemService("appops");
        OnOpChangedInternalListener opListener = new OnOpChangedInternalListener() {
            public void onOpChanged(int op, String packageName) {
                WindowManagerService.this.updateAppOpsState();
            }
        };
        this.mAppOps.startWatchingMode(24, null, opListener);
        this.mAppOps.startWatchingMode(45, null, opListener);
        this.mWindowAnimationScaleSetting = Global.getFloat(context.getContentResolver(), "window_animation_scale", this.mWindowAnimationScaleSetting);
        this.mTransitionAnimationScaleSetting = Global.getFloat(context.getContentResolver(), "transition_animation_scale", context.getResources().getFloat(17104949));
        setAnimatorDurationScale(Global.getFloat(context.getContentResolver(), "animator_duration_scale", this.mAnimatorDurationScaleSetting));
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.app.action.DEVICE_POLICY_MANAGER_STATE_CHANGED");
        filter.addAction("android.intent.action.USER_REMOVED");
        this.mContext.registerReceiver(this.mBroadcastReceiver, filter);
        this.mSettingsObserver = new SettingsObserver();
        this.mHoldingScreenWakeLock = this.mPowerManager.newWakeLock(536870922, TAG);
        this.mHoldingScreenWakeLock.setReferenceCounted(false);
        this.mAnimator = new WindowAnimator(this);
        this.mAllowTheaterModeWakeFromLayout = context.getResources().getBoolean(17956885);
        LocalServices.addService(WindowManagerInternal.class, new LocalService());
        initPolicy();
        Watchdog.getInstance().addMonitor(this);
        openSurfaceTransaction();
        try {
            createWatermarkInTransaction();
            showEmulatorDisplayOverlayIfNeeded();
        } finally {
            closeSurfaceTransaction();
        }
    }

    public InputMonitor getInputMonitor() {
        return this.mInputMonitor;
    }

    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        try {
            return super.onTransact(code, data, reply, flags);
        } catch (RuntimeException e) {
            if (!(e instanceof SecurityException)) {
                Slog.wtf(TAG, "Window Manager Crash", e);
            }
            throw e;
        }
    }

    static boolean excludeWindowTypeFromTapOutTask(int windowType) {
        switch (windowType) {
            case 2000:
            case 2012:
            case 2019:
                return true;
            default:
                return false;
        }
    }

    public int addWindow(com.android.server.wm.Session r43, android.view.IWindow r44, int r45, android.view.WindowManager.LayoutParams r46, int r47, int r48, android.graphics.Rect r49, android.graphics.Rect r50, android.graphics.Rect r51, android.view.InputChannel r52) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r4_3 'token' com.android.server.wm.WindowToken) in PHI: PHI: (r4_2 'token' com.android.server.wm.WindowToken) = (r4_1 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_0 'token' com.android.server.wm.WindowToken), (r4_3 'token' com.android.server.wm.WindowToken) binds: {(r4_1 'token' com.android.server.wm.WindowToken)=B:156:0x035d, (r4_0 'token' com.android.server.wm.WindowToken)=B:179:0x03fc, (r4_0 'token' com.android.server.wm.WindowToken)=B:189:0x042f, (r4_0 'token' com.android.server.wm.WindowToken)=B:199:0x0466, (r4_0 'token' com.android.server.wm.WindowToken)=B:209:0x049d, (r4_0 'token' com.android.server.wm.WindowToken)=B:219:0x04d4, (r4_0 'token' com.android.server.wm.WindowToken)=B:229:0x050b, (r4_0 'token' com.android.server.wm.WindowToken)=B:239:0x054a, (r4_0 'token' com.android.server.wm.WindowToken)=B:241:0x0550, (r4_0 'token' com.android.server.wm.WindowToken)=B:251:0x0585, (r4_0 'token' com.android.server.wm.WindowToken)=B:259:0x05b6, (r4_3 'token' com.android.server.wm.WindowToken)=B:260:0x05b8}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:79)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:60)
	at jadx.core.ProcessClass.process(ProcessClass.java:39)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r42 = this;
        r5 = 1;
        r0 = new int[r5];
        r24 = r0;
        r0 = r42;
        r5 = r0.mPolicy;
        r0 = r46;
        r1 = r24;
        r37 = r5.checkAddPermission(r0, r1);
        if (r37 == 0) goto L_0x0014;
    L_0x0013:
        return r37;
    L_0x0014:
        r36 = 0;
        r33 = 0;
        r26 = android.os.Binder.getCallingUid();
        r0 = r46;
        r7 = r0.type;
        r0 = r42;
        r0 = r0.mWindowMap;
        r41 = r0;
        monitor-enter(r41);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mDisplayReady;	 Catch:{ all -> 0x0039 }
        if (r5 != 0) goto L_0x003f;	 Catch:{ all -> 0x0039 }
    L_0x0030:
        r5 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x0039 }
        r8 = "Display has not been initialialized";	 Catch:{ all -> 0x0039 }
        r5.<init>(r8);	 Catch:{ all -> 0x0039 }
        throw r5;	 Catch:{ all -> 0x0039 }
    L_0x0039:
        r5 = move-exception;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        throw r5;
    L_0x003f:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mRoot;	 Catch:{ all -> 0x0039 }
        r0 = r48;	 Catch:{ all -> 0x0039 }
        r9 = r5.getDisplayContentOrCreate(r0);	 Catch:{ all -> 0x0039 }
        if (r9 != 0) goto L_0x0075;	 Catch:{ all -> 0x0039 }
    L_0x004b:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add window to a display that does not exist: ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r48;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r0);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -9;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0075:
        r0 = r43;	 Catch:{ all -> 0x0039 }
        r5 = r0.mUid;	 Catch:{ all -> 0x0039 }
        r5 = r9.hasAccess(r5);	 Catch:{ all -> 0x0039 }
        if (r5 != 0) goto L_0x00bb;	 Catch:{ all -> 0x0039 }
    L_0x007f:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mDisplayManagerInternal;	 Catch:{ all -> 0x0039 }
        r0 = r43;	 Catch:{ all -> 0x0039 }
        r8 = r0.mUid;	 Catch:{ all -> 0x0039 }
        r0 = r48;	 Catch:{ all -> 0x0039 }
        r5 = r5.isUidPresentOnDisplay(r8, r0);	 Catch:{ all -> 0x0039 }
        r5 = r5 ^ 1;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x00bb;	 Catch:{ all -> 0x0039 }
    L_0x0091:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add window to a display for which the application does not have access: ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r48;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r0);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -9;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x00bb:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mWindowMap;	 Catch:{ all -> 0x0039 }
        r8 = r44.asBinder();	 Catch:{ all -> 0x0039 }
        r5 = r5.containsKey(r8);	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x00f2;	 Catch:{ all -> 0x0039 }
    L_0x00c9:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Window ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r44;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r0);	 Catch:{ all -> 0x0039 }
        r11 = " is already added";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -5;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x00f2:
        r5 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        if (r7 < r5) goto L_0x0172;
    L_0x00f6:
        r5 = 1999; // 0x7cf float:2.801E-42 double:9.876E-321;
        if (r7 > r5) goto L_0x0172;
    L_0x00fa:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = 0;	 Catch:{ all -> 0x0039 }
        r11 = 0;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r33 = r0.windowForClientLocked(r8, r5, r11);	 Catch:{ all -> 0x0039 }
        if (r33 != 0) goto L_0x0133;	 Catch:{ all -> 0x0039 }
    L_0x0108:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add window with token that is not a window: ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -2;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0133:
        r0 = r33;	 Catch:{ all -> 0x0039 }
        r5 = r0.mAttrs;	 Catch:{ all -> 0x0039 }
        r5 = r5.type;	 Catch:{ all -> 0x0039 }
        r8 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;	 Catch:{ all -> 0x0039 }
        if (r5 < r8) goto L_0x0172;	 Catch:{ all -> 0x0039 }
    L_0x013d:
        r0 = r33;	 Catch:{ all -> 0x0039 }
        r5 = r0.mAttrs;	 Catch:{ all -> 0x0039 }
        r5 = r5.type;	 Catch:{ all -> 0x0039 }
        r8 = 1999; // 0x7cf float:2.801E-42 double:9.876E-321;	 Catch:{ all -> 0x0039 }
        if (r5 > r8) goto L_0x0172;	 Catch:{ all -> 0x0039 }
    L_0x0147:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add window with token that is a sub-window: ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -2;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0172:
        r5 = 2030; // 0x7ee float:2.845E-42 double:1.003E-320;
        if (r7 != r5) goto L_0x018d;
    L_0x0176:
        r5 = r9.isPrivate();	 Catch:{ all -> 0x0039 }
        r5 = r5 ^ 1;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x018d;	 Catch:{ all -> 0x0039 }
    L_0x017e:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = "Attempted to add private presentation window to a non-private display.  Aborting.";	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -8;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x018d:
        r25 = 0;
        if (r33 == 0) goto L_0x01e3;
    L_0x0191:
        r29 = 1;
    L_0x0193:
        if (r29 == 0) goto L_0x01e6;
    L_0x0195:
        r0 = r33;	 Catch:{ all -> 0x0039 }
        r5 = r0.mAttrs;	 Catch:{ all -> 0x0039 }
        r5 = r5.token;	 Catch:{ all -> 0x0039 }
    L_0x019b:
        r4 = r9.getWindowToken(r5);	 Catch:{ all -> 0x0039 }
        if (r29 == 0) goto L_0x01eb;	 Catch:{ all -> 0x0039 }
    L_0x01a1:
        r0 = r33;	 Catch:{ all -> 0x0039 }
        r5 = r0.mAttrs;	 Catch:{ all -> 0x0039 }
        r0 = r5.type;	 Catch:{ all -> 0x0039 }
        r38 = r0;	 Catch:{ all -> 0x0039 }
    L_0x01a9:
        r23 = 0;	 Catch:{ all -> 0x0039 }
        if (r4 != 0) goto L_0x03c0;	 Catch:{ all -> 0x0039 }
    L_0x01ad:
        r5 = 1;	 Catch:{ all -> 0x0039 }
        r0 = r38;	 Catch:{ all -> 0x0039 }
        if (r0 < r5) goto L_0x01ee;	 Catch:{ all -> 0x0039 }
    L_0x01b2:
        r5 = 99;	 Catch:{ all -> 0x0039 }
        r0 = r38;	 Catch:{ all -> 0x0039 }
        if (r0 > r5) goto L_0x01ee;	 Catch:{ all -> 0x0039 }
    L_0x01b8:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add application window with unknown token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x01e3:
        r29 = 0;
        goto L_0x0193;
    L_0x01e6:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r0.token;	 Catch:{ all -> 0x0039 }
        goto L_0x019b;	 Catch:{ all -> 0x0039 }
    L_0x01eb:
        r38 = r7;	 Catch:{ all -> 0x0039 }
        goto L_0x01a9;	 Catch:{ all -> 0x0039 }
    L_0x01ee:
        r5 = 2011; // 0x7db float:2.818E-42 double:9.936E-321;	 Catch:{ all -> 0x0039 }
        r0 = r38;	 Catch:{ all -> 0x0039 }
        if (r0 != r5) goto L_0x021f;	 Catch:{ all -> 0x0039 }
    L_0x01f4:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add input method window with unknown token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x021f:
        r5 = 2031; // 0x7ef float:2.846E-42 double:1.0034E-320;
        r0 = r38;
        if (r0 != r5) goto L_0x0250;
    L_0x0225:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add voice interaction window with unknown token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0250:
        r5 = 2013; // 0x7dd float:2.821E-42 double:9.946E-321;
        r0 = r38;
        if (r0 != r5) goto L_0x0281;
    L_0x0256:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add wallpaper window with unknown token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0281:
        r5 = 2023; // 0x7e7 float:2.835E-42 double:9.995E-321;
        r0 = r38;
        if (r0 != r5) goto L_0x02b2;
    L_0x0287:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add Dream window with unknown token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x02b2:
        r5 = 2035; // 0x7f3 float:2.852E-42 double:1.0054E-320;
        r0 = r38;
        if (r0 != r5) goto L_0x02e3;
    L_0x02b8:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add QS dialog window with unknown token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x02e3:
        r5 = 2032; // 0x7f0 float:2.847E-42 double:1.004E-320;
        r0 = r38;
        if (r0 != r5) goto L_0x0314;
    L_0x02e9:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add Accessibility overlay window with unknown token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0314:
        r5 = 2005; // 0x7d5 float:2.81E-42 double:9.906E-321;
        if (r7 != r5) goto L_0x0353;
    L_0x0318:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r0.packageName;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r1 = r26;	 Catch:{ all -> 0x0039 }
        r2 = r33;	 Catch:{ all -> 0x0039 }
        r5 = r0.doesAddToastWindowRequireToken(r5, r1, r2);	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x0353;	 Catch:{ all -> 0x0039 }
    L_0x0328:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add a toast window with unknown token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0353:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r0.token;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x03bb;	 Catch:{ all -> 0x0039 }
    L_0x0359:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r6 = r0.token;	 Catch:{ all -> 0x0039 }
    L_0x035d:
        r4 = new com.android.server.wm.WindowToken;	 Catch:{ all -> 0x0039 }
        r0 = r43;	 Catch:{ all -> 0x0039 }
        r10 = r0.mCanAddInternalSystemWindow;	 Catch:{ all -> 0x0039 }
        r8 = 0;	 Catch:{ all -> 0x0039 }
        r5 = r42;	 Catch:{ all -> 0x0039 }
        r4.<init>(r5, r6, r7, r8, r9, r10);	 Catch:{ all -> 0x0039 }
    L_0x0369:
        r10 = new com.android.server.wm.WindowState;	 Catch:{ all -> 0x0039 }
        r5 = 0;	 Catch:{ all -> 0x0039 }
        r16 = r24[r5];	 Catch:{ all -> 0x0039 }
        r0 = r43;	 Catch:{ all -> 0x0039 }
        r0 = r0.mUid;	 Catch:{ all -> 0x0039 }
        r20 = r0;	 Catch:{ all -> 0x0039 }
        r0 = r43;	 Catch:{ all -> 0x0039 }
        r0 = r0.mCanAddInternalSystemWindow;	 Catch:{ all -> 0x0039 }
        r21 = r0;	 Catch:{ all -> 0x0039 }
        r11 = r42;	 Catch:{ all -> 0x0039 }
        r12 = r43;	 Catch:{ all -> 0x0039 }
        r13 = r44;	 Catch:{ all -> 0x0039 }
        r14 = r4;	 Catch:{ all -> 0x0039 }
        r15 = r33;	 Catch:{ all -> 0x0039 }
        r17 = r45;	 Catch:{ all -> 0x0039 }
        r18 = r46;	 Catch:{ all -> 0x0039 }
        r19 = r47;	 Catch:{ all -> 0x0039 }
        r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21);	 Catch:{ all -> 0x0039 }
        r5 = r10.mDeathRecipient;	 Catch:{ all -> 0x0039 }
        if (r5 != 0) goto L_0x05f0;	 Catch:{ all -> 0x0039 }
    L_0x0390:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Adding window client ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = r44.asBinder();	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = " that is dead, aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -4;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x03bb:
        r6 = r44.asBinder();	 Catch:{ all -> 0x0039 }
        goto L_0x035d;	 Catch:{ all -> 0x0039 }
    L_0x03c0:
        r5 = 1;	 Catch:{ all -> 0x0039 }
        r0 = r38;	 Catch:{ all -> 0x0039 }
        if (r0 < r5) goto L_0x0425;	 Catch:{ all -> 0x0039 }
    L_0x03c5:
        r5 = 99;	 Catch:{ all -> 0x0039 }
        r0 = r38;	 Catch:{ all -> 0x0039 }
        if (r0 > r5) goto L_0x0425;	 Catch:{ all -> 0x0039 }
    L_0x03cb:
        r25 = r4.asAppWindowToken();	 Catch:{ all -> 0x0039 }
        if (r25 != 0) goto L_0x03f8;	 Catch:{ all -> 0x0039 }
    L_0x03d1:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add window with non-application token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r4);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -3;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x03f8:
        r0 = r25;	 Catch:{ all -> 0x0039 }
        r5 = r0.removed;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x03fe:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add window with exiting application token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r4);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -4;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0425:
        r5 = 2011; // 0x7db float:2.818E-42 double:9.936E-321;
        r0 = r38;
        if (r0 != r5) goto L_0x045c;
    L_0x042b:
        r5 = r4.windowType;	 Catch:{ all -> 0x0039 }
        r8 = 2011; // 0x7db float:2.818E-42 double:9.936E-321;	 Catch:{ all -> 0x0039 }
        if (r5 == r8) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x0431:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add input method window with bad token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x045c:
        r5 = 2031; // 0x7ef float:2.846E-42 double:1.0034E-320;
        r0 = r38;
        if (r0 != r5) goto L_0x0493;
    L_0x0462:
        r5 = r4.windowType;	 Catch:{ all -> 0x0039 }
        r8 = 2031; // 0x7ef float:2.846E-42 double:1.0034E-320;	 Catch:{ all -> 0x0039 }
        if (r5 == r8) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x0468:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add voice interaction window with bad token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0493:
        r5 = 2013; // 0x7dd float:2.821E-42 double:9.946E-321;
        r0 = r38;
        if (r0 != r5) goto L_0x04ca;
    L_0x0499:
        r5 = r4.windowType;	 Catch:{ all -> 0x0039 }
        r8 = 2013; // 0x7dd float:2.821E-42 double:9.946E-321;	 Catch:{ all -> 0x0039 }
        if (r5 == r8) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x049f:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add wallpaper window with bad token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x04ca:
        r5 = 2023; // 0x7e7 float:2.835E-42 double:9.995E-321;
        r0 = r38;
        if (r0 != r5) goto L_0x0501;
    L_0x04d0:
        r5 = r4.windowType;	 Catch:{ all -> 0x0039 }
        r8 = 2023; // 0x7e7 float:2.835E-42 double:9.995E-321;	 Catch:{ all -> 0x0039 }
        if (r5 == r8) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x04d6:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add Dream window with bad token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0501:
        r5 = 2032; // 0x7f0 float:2.847E-42 double:1.004E-320;
        r0 = r38;
        if (r0 != r5) goto L_0x0538;
    L_0x0507:
        r5 = r4.windowType;	 Catch:{ all -> 0x0039 }
        r8 = 2032; // 0x7f0 float:2.847E-42 double:1.004E-320;	 Catch:{ all -> 0x0039 }
        if (r5 == r8) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x050d:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add Accessibility overlay window with bad token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0538:
        r5 = 2005; // 0x7d5 float:2.81E-42 double:9.906E-321;
        if (r7 != r5) goto L_0x057d;
    L_0x053c:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r0.packageName;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r1 = r26;	 Catch:{ all -> 0x0039 }
        r2 = r33;	 Catch:{ all -> 0x0039 }
        r23 = r0.doesAddToastWindowRequireToken(r5, r1, r2);	 Catch:{ all -> 0x0039 }
        if (r23 == 0) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x054c:
        r5 = r4.windowType;	 Catch:{ all -> 0x0039 }
        r8 = 2005; // 0x7d5 float:2.81E-42 double:9.906E-321;	 Catch:{ all -> 0x0039 }
        if (r5 == r8) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x0552:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add a toast window with bad token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x057d:
        r5 = 2035; // 0x7f3 float:2.852E-42 double:1.0054E-320;
        if (r7 != r5) goto L_0x05b2;
    L_0x0581:
        r5 = r4.windowType;	 Catch:{ all -> 0x0039 }
        r8 = 2035; // 0x7f3 float:2.852E-42 double:1.0054E-320;	 Catch:{ all -> 0x0039 }
        if (r5 == r8) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x0587:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Attempted to add QS dialog window with bad token ";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r11 = r0.token;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r11 = ".  Aborting.";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -1;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x05b2:
        r5 = r4.asAppWindowToken();	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x05b8:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0039 }
        r8.<init>();	 Catch:{ all -> 0x0039 }
        r11 = "Non-null appWindowToken for system window of rootType=";	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0039 }
        r0 = r38;	 Catch:{ all -> 0x0039 }
        r8 = r8.append(r0);	 Catch:{ all -> 0x0039 }
        r8 = r8.toString();	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = 0;	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r0.token = r5;	 Catch:{ all -> 0x0039 }
        r4 = new com.android.server.wm.WindowToken;	 Catch:{ all -> 0x0039 }
        r12 = r44.asBinder();	 Catch:{ all -> 0x0039 }
        r0 = r43;	 Catch:{ all -> 0x0039 }
        r0 = r0.mCanAddInternalSystemWindow;	 Catch:{ all -> 0x0039 }
        r16 = r0;	 Catch:{ all -> 0x0039 }
        r14 = 0;	 Catch:{ all -> 0x0039 }
        r10 = r4;	 Catch:{ all -> 0x0039 }
        r11 = r42;	 Catch:{ all -> 0x0039 }
        r13 = r7;	 Catch:{ all -> 0x0039 }
        r15 = r9;	 Catch:{ all -> 0x0039 }
        r10.<init>(r11, r12, r13, r14, r15, r16);	 Catch:{ all -> 0x0039 }
        goto L_0x0369;	 Catch:{ all -> 0x0039 }
    L_0x05f0:
        r5 = r10.getDisplayContent();	 Catch:{ all -> 0x0039 }
        if (r5 != 0) goto L_0x0606;	 Catch:{ all -> 0x0039 }
    L_0x05f6:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = "Adding window to Display that has been removed.";	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -9;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0606:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mPolicy;	 Catch:{ all -> 0x0039 }
        r8 = r10.mAttrs;	 Catch:{ all -> 0x0039 }
        r5.adjustWindowParamsLw(r8);	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mPolicy;	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r5.checkShowToOwnerOnly(r0);	 Catch:{ all -> 0x0039 }
        r10.setShowToOwnerOnlyLocked(r5);	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mPolicy;	 Catch:{ all -> 0x0039 }
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r37 = r5.prepareAddWindowLw(r10, r0);	 Catch:{ all -> 0x0039 }
        if (r37 == 0) goto L_0x062d;
    L_0x0628:
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r37;
    L_0x062d:
        if (r52 == 0) goto L_0x0662;
    L_0x062f:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r0.inputFeatures;	 Catch:{ all -> 0x0039 }
        r5 = r5 & 2;	 Catch:{ all -> 0x0039 }
        if (r5 != 0) goto L_0x065f;	 Catch:{ all -> 0x0039 }
    L_0x0637:
        r32 = 1;	 Catch:{ all -> 0x0039 }
    L_0x0639:
        if (r32 == 0) goto L_0x0640;	 Catch:{ all -> 0x0039 }
    L_0x063b:
        r0 = r52;	 Catch:{ all -> 0x0039 }
        r10.openInputChannel(r0);	 Catch:{ all -> 0x0039 }
    L_0x0640:
        r5 = 2005; // 0x7d5 float:2.81E-42 double:9.906E-321;	 Catch:{ all -> 0x0039 }
        if (r7 != r5) goto L_0x0684;	 Catch:{ all -> 0x0039 }
    L_0x0644:
        r5 = r42.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x0039 }
        r0 = r26;	 Catch:{ all -> 0x0039 }
        r5 = r5.canAddToastWindowForUid(r0);	 Catch:{ all -> 0x0039 }
        if (r5 != 0) goto L_0x0665;	 Catch:{ all -> 0x0039 }
    L_0x0650:
        r5 = "WindowManager";	 Catch:{ all -> 0x0039 }
        r8 = "Adding more than one toast window for UID at a time.";	 Catch:{ all -> 0x0039 }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x0039 }
        r5 = -5;
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x065f:
        r32 = 0;
        goto L_0x0639;
    L_0x0662:
        r32 = 0;
        goto L_0x0639;
    L_0x0665:
        if (r23 != 0) goto L_0x066f;
    L_0x0667:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r0.flags;	 Catch:{ all -> 0x0039 }
        r5 = r5 & 8;	 Catch:{ all -> 0x0039 }
        if (r5 != 0) goto L_0x07fc;	 Catch:{ all -> 0x0039 }
    L_0x066f:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mH;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r8 = r0.mH;	 Catch:{ all -> 0x0039 }
        r11 = 52;	 Catch:{ all -> 0x0039 }
        r8 = r8.obtainMessage(r11, r10);	 Catch:{ all -> 0x0039 }
        r11 = r10.mAttrs;	 Catch:{ all -> 0x0039 }
        r14 = r11.hideTimeoutMilliseconds;	 Catch:{ all -> 0x0039 }
        r5.sendMessageDelayed(r8, r14);	 Catch:{ all -> 0x0039 }
    L_0x0684:
        r37 = 0;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mCurrentFocus;	 Catch:{ all -> 0x0039 }
        if (r5 != 0) goto L_0x0693;	 Catch:{ all -> 0x0039 }
    L_0x068c:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mWinAddedSinceNullFocus;	 Catch:{ all -> 0x0039 }
        r5.add(r10);	 Catch:{ all -> 0x0039 }
    L_0x0693:
        r5 = excludeWindowTypeFromTapOutTask(r7);	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x069e;	 Catch:{ all -> 0x0039 }
    L_0x0699:
        r5 = r9.mTapExcludedWindows;	 Catch:{ all -> 0x0039 }
        r5.add(r10);	 Catch:{ all -> 0x0039 }
    L_0x069e:
        r34 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x0039 }
        r10.attach();	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mWindowMap;	 Catch:{ all -> 0x0039 }
        r8 = r44.asBinder();	 Catch:{ all -> 0x0039 }
        r5.put(r8, r10);	 Catch:{ all -> 0x0039 }
        r5 = r10.mAppOp;	 Catch:{ all -> 0x0039 }
        r8 = -1;	 Catch:{ all -> 0x0039 }
        if (r5 == r8) goto L_0x06d2;	 Catch:{ all -> 0x0039 }
    L_0x06b5:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mAppOps;	 Catch:{ all -> 0x0039 }
        r8 = r10.mAppOp;	 Catch:{ all -> 0x0039 }
        r11 = r10.getOwningUid();	 Catch:{ all -> 0x0039 }
        r12 = r10.getOwningPackage();	 Catch:{ all -> 0x0039 }
        r39 = r5.startOpNoThrow(r8, r11, r12);	 Catch:{ all -> 0x0039 }
        if (r39 == 0) goto L_0x06d2;	 Catch:{ all -> 0x0039 }
    L_0x06c9:
        r5 = 3;	 Catch:{ all -> 0x0039 }
        r0 = r39;	 Catch:{ all -> 0x0039 }
        if (r0 == r5) goto L_0x06d2;	 Catch:{ all -> 0x0039 }
    L_0x06ce:
        r5 = 0;	 Catch:{ all -> 0x0039 }
        r10.setAppOpVisibilityLw(r5);	 Catch:{ all -> 0x0039 }
    L_0x06d2:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mHidingNonSystemOverlayWindows;	 Catch:{ all -> 0x0039 }
        r5 = r5.isEmpty();	 Catch:{ all -> 0x0039 }
        r30 = r5 ^ 1;	 Catch:{ all -> 0x0039 }
        r0 = r30;	 Catch:{ all -> 0x0039 }
        r10.setForceHideNonSystemOverlayWindowIfNeeded(r0);	 Catch:{ all -> 0x0039 }
        r22 = r4.asAppWindowToken();	 Catch:{ all -> 0x0039 }
        r5 = 3;	 Catch:{ all -> 0x0039 }
        if (r7 != r5) goto L_0x06ee;	 Catch:{ all -> 0x0039 }
    L_0x06e8:
        if (r22 == 0) goto L_0x06ee;	 Catch:{ all -> 0x0039 }
    L_0x06ea:
        r0 = r22;	 Catch:{ all -> 0x0039 }
        r0.startingWindow = r10;	 Catch:{ all -> 0x0039 }
    L_0x06ee:
        r31 = 1;	 Catch:{ all -> 0x0039 }
        r5 = r10.mToken;	 Catch:{ all -> 0x0039 }
        r5.addWindow(r10);	 Catch:{ all -> 0x0039 }
        r5 = 2011; // 0x7db float:2.818E-42 double:9.936E-321;	 Catch:{ all -> 0x0039 }
        if (r7 != r5) goto L_0x080e;	 Catch:{ all -> 0x0039 }
    L_0x06f9:
        r5 = 1;	 Catch:{ all -> 0x0039 }
        r10.mGivenInsetsPending = r5;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r0.setInputMethodWindowLocked(r10);	 Catch:{ all -> 0x0039 }
        r31 = 0;	 Catch:{ all -> 0x0039 }
    L_0x0703:
        r10.applyAdjustForImeIfNeeded();	 Catch:{ all -> 0x0039 }
        r5 = 2034; // 0x7f2 float:2.85E-42 double:1.005E-320;	 Catch:{ all -> 0x0039 }
        if (r7 != r5) goto L_0x071b;	 Catch:{ all -> 0x0039 }
    L_0x070a:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mRoot;	 Catch:{ all -> 0x0039 }
        r0 = r48;	 Catch:{ all -> 0x0039 }
        r5 = r5.getDisplayContent(r0);	 Catch:{ all -> 0x0039 }
        r5 = r5.getDockedDividerController();	 Catch:{ all -> 0x0039 }
        r5.setWindow(r10);	 Catch:{ all -> 0x0039 }
    L_0x071b:
        r0 = r10.mWinAnimator;	 Catch:{ all -> 0x0039 }
        r40 = r0;	 Catch:{ all -> 0x0039 }
        r5 = 1;	 Catch:{ all -> 0x0039 }
        r0 = r40;	 Catch:{ all -> 0x0039 }
        r0.mEnterAnimationPending = r5;	 Catch:{ all -> 0x0039 }
        r5 = 1;	 Catch:{ all -> 0x0039 }
        r0 = r40;	 Catch:{ all -> 0x0039 }
        r0.mEnteringAnimation = r5;	 Catch:{ all -> 0x0039 }
        if (r25 == 0) goto L_0x0744;	 Catch:{ all -> 0x0039 }
    L_0x072b:
        r5 = r25.isVisible();	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x0744;	 Catch:{ all -> 0x0039 }
    L_0x0731:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r1 = r25;	 Catch:{ all -> 0x0039 }
        r5 = r0.prepareWindowReplacementTransition(r1);	 Catch:{ all -> 0x0039 }
        r5 = r5 ^ 1;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x0744;	 Catch:{ all -> 0x0039 }
    L_0x073d:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r1 = r25;	 Catch:{ all -> 0x0039 }
        r0.prepareNoneTransitionForRelaunching(r1);	 Catch:{ all -> 0x0039 }
    L_0x0744:
        r5 = r9.isDefaultDisplay;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x084f;	 Catch:{ all -> 0x0039 }
    L_0x0748:
        r27 = r9.getDisplayInfo();	 Catch:{ all -> 0x0039 }
        if (r25 == 0) goto L_0x084c;	 Catch:{ all -> 0x0039 }
    L_0x074e:
        r5 = r25.getTask();	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x084c;	 Catch:{ all -> 0x0039 }
    L_0x0754:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r13 = r0.mTmpRect;	 Catch:{ all -> 0x0039 }
        r5 = r25.getTask();	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r8 = r0.mTmpRect;	 Catch:{ all -> 0x0039 }
        r5.getBounds(r8);	 Catch:{ all -> 0x0039 }
    L_0x0763:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r11 = r0.mPolicy;	 Catch:{ all -> 0x0039 }
        r12 = r10.mAttrs;	 Catch:{ all -> 0x0039 }
        r0 = r27;	 Catch:{ all -> 0x0039 }
        r14 = r0.rotation;	 Catch:{ all -> 0x0039 }
        r0 = r27;	 Catch:{ all -> 0x0039 }
        r15 = r0.logicalWidth;	 Catch:{ all -> 0x0039 }
        r0 = r27;	 Catch:{ all -> 0x0039 }
        r0 = r0.logicalHeight;	 Catch:{ all -> 0x0039 }
        r16 = r0;	 Catch:{ all -> 0x0039 }
        r17 = r49;	 Catch:{ all -> 0x0039 }
        r18 = r50;	 Catch:{ all -> 0x0039 }
        r19 = r51;	 Catch:{ all -> 0x0039 }
        r5 = r11.getInsetHintLw(r12, r13, r14, r15, r16, r17, r18, r19);	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x0785;	 Catch:{ all -> 0x0039 }
    L_0x0783:
        r37 = 4;	 Catch:{ all -> 0x0039 }
    L_0x0785:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mInTouchMode;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x078d;	 Catch:{ all -> 0x0039 }
    L_0x078b:
        r37 = r37 | 1;	 Catch:{ all -> 0x0039 }
    L_0x078d:
        r5 = r10.mAppToken;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x079b;	 Catch:{ all -> 0x0039 }
    L_0x0791:
        r5 = r10.mAppToken;	 Catch:{ all -> 0x0039 }
        r5 = r5.isClientHidden();	 Catch:{ all -> 0x0039 }
        r5 = r5 ^ 1;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x079d;	 Catch:{ all -> 0x0039 }
    L_0x079b:
        r37 = r37 | 2;	 Catch:{ all -> 0x0039 }
    L_0x079d:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mInputMonitor;	 Catch:{ all -> 0x0039 }
        r5.setUpdateInputWindowsNeededLw();	 Catch:{ all -> 0x0039 }
        r28 = 0;	 Catch:{ all -> 0x0039 }
        r5 = r10.canReceiveKeys();	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x07b8;	 Catch:{ all -> 0x0039 }
    L_0x07ac:
        r5 = 1;	 Catch:{ all -> 0x0039 }
        r8 = 0;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r28 = r0.updateFocusedWindowLocked(r5, r8);	 Catch:{ all -> 0x0039 }
        if (r28 == 0) goto L_0x07b8;	 Catch:{ all -> 0x0039 }
    L_0x07b6:
        r31 = 0;	 Catch:{ all -> 0x0039 }
    L_0x07b8:
        if (r31 == 0) goto L_0x07be;	 Catch:{ all -> 0x0039 }
    L_0x07ba:
        r5 = 1;	 Catch:{ all -> 0x0039 }
        r9.computeImeTarget(r5);	 Catch:{ all -> 0x0039 }
    L_0x07be:
        r5 = 0;	 Catch:{ all -> 0x0039 }
        r9.assignWindowLayers(r5);	 Catch:{ all -> 0x0039 }
        if (r28 == 0) goto L_0x07d0;	 Catch:{ all -> 0x0039 }
    L_0x07c4:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mInputMonitor;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r8 = r0.mCurrentFocus;	 Catch:{ all -> 0x0039 }
        r11 = 0;	 Catch:{ all -> 0x0039 }
        r5.setInputFocusLw(r8, r11);	 Catch:{ all -> 0x0039 }
    L_0x07d0:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mInputMonitor;	 Catch:{ all -> 0x0039 }
        r8 = 0;	 Catch:{ all -> 0x0039 }
        r5.updateInputWindowsLw(r8);	 Catch:{ all -> 0x0039 }
        r5 = r10.isVisibleOrAdding();	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x07eb;	 Catch:{ all -> 0x0039 }
    L_0x07de:
        r5 = 0;	 Catch:{ all -> 0x0039 }
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r1 = r48;	 Catch:{ all -> 0x0039 }
        r5 = r0.updateOrientationFromAppTokensLocked(r5, r1);	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x07eb;
    L_0x07e9:
        r36 = 1;
    L_0x07eb:
        monitor-exit(r41);
        resetPriorityAfterLockedSection();
        if (r36 == 0) goto L_0x07f8;
    L_0x07f1:
        r0 = r42;
        r1 = r48;
        r0.sendNewConfiguration(r1);
    L_0x07f8:
        android.os.Binder.restoreCallingIdentity(r34);
        return r37;
    L_0x07fc:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mCurrentFocus;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x066f;	 Catch:{ all -> 0x0039 }
    L_0x0802:
        r0 = r42;	 Catch:{ all -> 0x0039 }
        r5 = r0.mCurrentFocus;	 Catch:{ all -> 0x0039 }
        r5 = r5.mOwnerUid;	 Catch:{ all -> 0x0039 }
        r0 = r26;	 Catch:{ all -> 0x0039 }
        if (r5 == r0) goto L_0x0684;	 Catch:{ all -> 0x0039 }
    L_0x080c:
        goto L_0x066f;	 Catch:{ all -> 0x0039 }
    L_0x080e:
        r5 = 2012; // 0x7dc float:2.82E-42 double:9.94E-321;	 Catch:{ all -> 0x0039 }
        if (r7 != r5) goto L_0x081a;	 Catch:{ all -> 0x0039 }
    L_0x0812:
        r5 = 1;	 Catch:{ all -> 0x0039 }
        r9.computeImeTarget(r5);	 Catch:{ all -> 0x0039 }
        r31 = 0;	 Catch:{ all -> 0x0039 }
        goto L_0x0703;	 Catch:{ all -> 0x0039 }
    L_0x081a:
        r5 = 2013; // 0x7dd float:2.821E-42 double:9.946E-321;	 Catch:{ all -> 0x0039 }
        if (r7 != r5) goto L_0x082b;	 Catch:{ all -> 0x0039 }
    L_0x081e:
        r5 = r9.mWallpaperController;	 Catch:{ all -> 0x0039 }
        r5.clearLastWallpaperTimeoutTime();	 Catch:{ all -> 0x0039 }
        r5 = r9.pendingLayoutChanges;	 Catch:{ all -> 0x0039 }
        r5 = r5 | 4;	 Catch:{ all -> 0x0039 }
        r9.pendingLayoutChanges = r5;	 Catch:{ all -> 0x0039 }
        goto L_0x0703;	 Catch:{ all -> 0x0039 }
    L_0x082b:
        r0 = r46;	 Catch:{ all -> 0x0039 }
        r5 = r0.flags;	 Catch:{ all -> 0x0039 }
        r8 = 1048576; // 0x100000 float:1.469368E-39 double:5.180654E-318;	 Catch:{ all -> 0x0039 }
        r5 = r5 & r8;	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x083c;	 Catch:{ all -> 0x0039 }
    L_0x0834:
        r5 = r9.pendingLayoutChanges;	 Catch:{ all -> 0x0039 }
        r5 = r5 | 4;	 Catch:{ all -> 0x0039 }
        r9.pendingLayoutChanges = r5;	 Catch:{ all -> 0x0039 }
        goto L_0x0703;	 Catch:{ all -> 0x0039 }
    L_0x083c:
        r5 = r9.mWallpaperController;	 Catch:{ all -> 0x0039 }
        r5 = r5.isBelowWallpaperTarget(r10);	 Catch:{ all -> 0x0039 }
        if (r5 == 0) goto L_0x0703;	 Catch:{ all -> 0x0039 }
    L_0x0844:
        r5 = r9.pendingLayoutChanges;	 Catch:{ all -> 0x0039 }
        r5 = r5 | 4;	 Catch:{ all -> 0x0039 }
        r9.pendingLayoutChanges = r5;	 Catch:{ all -> 0x0039 }
        goto L_0x0703;	 Catch:{ all -> 0x0039 }
    L_0x084c:
        r13 = 0;	 Catch:{ all -> 0x0039 }
        goto L_0x0763;	 Catch:{ all -> 0x0039 }
    L_0x084f:
        r49.setEmpty();	 Catch:{ all -> 0x0039 }
        r50.setEmpty();	 Catch:{ all -> 0x0039 }
        goto L_0x0785;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.addWindow(com.android.server.wm.Session, android.view.IWindow, int, android.view.WindowManager$LayoutParams, int, int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect, android.view.InputChannel):int");
    }

    private boolean doesAddToastWindowRequireToken(String packageName, int callingUid, WindowState attachedWindow) {
        boolean z = true;
        if (attachedWindow != null) {
            if (attachedWindow.mAppToken == null) {
                z = false;
            } else if (attachedWindow.mAppToken.mTargetSdk < 26) {
                z = false;
            }
            return z;
        }
        try {
            ApplicationInfo appInfo = this.mContext.getPackageManager().getApplicationInfoAsUser(packageName, 0, UserHandle.getUserId(callingUid));
            if (appInfo.uid == callingUid) {
                return appInfo.targetSdkVersion >= 26;
            } else {
                throw new SecurityException("Package " + packageName + " not in UID " + callingUid);
            }
        } catch (NameNotFoundException e) {
        }
    }

    private boolean prepareWindowReplacementTransition(AppWindowToken atoken) {
        atoken.clearAllDrawn();
        WindowState replacedWindow = atoken.getReplacingWindow();
        if (replacedWindow == null) {
            return false;
        }
        Rect frame = replacedWindow.mVisibleFrame;
        this.mOpeningApps.add(atoken);
        prepareAppTransition(18, true);
        this.mAppTransition.overridePendingAppTransitionClipReveal(frame.left, frame.top, frame.width(), frame.height());
        executeAppTransition();
        return true;
    }

    private void prepareNoneTransitionForRelaunching(AppWindowToken atoken) {
        if (this.mDisplayFrozen && (this.mOpeningApps.contains(atoken) ^ 1) != 0 && atoken.isRelaunching()) {
            this.mOpeningApps.add(atoken);
            prepareAppTransition(0, false);
            executeAppTransition();
        }
    }

    boolean isScreenCaptureDisabledLocked(int userId) {
        Boolean disabled = (Boolean) this.mScreenCaptureDisabled.get(userId);
        if (disabled == null) {
            return false;
        }
        return disabled.booleanValue();
    }

    boolean isSecureLocked(WindowState w) {
        return (w.mAttrs.flags & 8192) != 0 || isScreenCaptureDisabledLocked(UserHandle.getUserId(w.mOwnerUid));
    }

    public void enableSurfaceTrace(ParcelFileDescriptor pfd) {
        int callingUid = Binder.getCallingUid();
        if (callingUid == 2000 || callingUid == 0) {
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    this.mRoot.enableSurfaceTrace(pfd);
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
            return;
        }
        throw new SecurityException("Only shell can call enableSurfaceTrace");
    }

    public void disableSurfaceTrace() {
        int callingUid = Binder.getCallingUid();
        if (callingUid == 2000 || callingUid == 0 || callingUid == 1000) {
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    this.mRoot.disableSurfaceTrace();
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
            return;
        }
        throw new SecurityException("Only shell can call disableSurfaceTrace");
    }

    public void setScreenCaptureDisabled(int userId, boolean disabled) {
        if (Binder.getCallingUid() != 1000) {
            throw new SecurityException("Only system can call setScreenCaptureDisabled.");
        }
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mScreenCaptureDisabled.put(userId, Boolean.valueOf(disabled));
                this.mRoot.setSecureSurfaceState(userId, disabled);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void removeWindow(com.android.server.wm.Session r4, android.view.IWindow r5) {
        /*
        r3 = this;
        r2 = r3.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x001a }
        r1 = 0;
        r0 = r3.windowForClientLocked(r4, r5, r1);	 Catch:{ all -> 0x001a }
        if (r0 != 0) goto L_0x0012;
    L_0x000d:
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0012:
        r0.removeIfPossible();	 Catch:{ all -> 0x001a }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x001a:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.removeWindow(com.android.server.wm.Session, android.view.IWindow):void");
    }

    void postWindowRemoveCleanupLocked(WindowState win) {
        this.mWindowMap.remove(win.mClient.asBinder());
        if (win.mAppOp != -1) {
            this.mAppOps.finishOp(win.mAppOp, win.getOwningUid(), win.getOwningPackage());
        }
        if (this.mCurrentFocus == null) {
            this.mWinRemovedSinceNullFocus.add(win);
        }
        this.mPendingRemove.remove(win);
        this.mResizingWindows.remove(win);
        updateNonSystemOverlayWindowsVisibilityIfNeeded(win, false);
        this.mWindowsChanged = true;
        if (this.mInputMethodWindow == win) {
            setInputMethodWindowLocked(null);
        }
        WindowToken token = win.mToken;
        AppWindowToken atoken = win.mAppToken;
        if (token.isEmpty()) {
            if (!token.mPersistOnEmpty) {
                token.removeImmediately();
            } else if (atoken != null) {
                atoken.firstWindowDrawn = false;
                atoken.clearAllDrawn();
                TaskStack stack = atoken.getStack();
                if (stack != null) {
                    stack.mExitingAppTokens.remove(atoken);
                }
            }
        }
        if (atoken != null) {
            atoken.postWindowRemoveStartingWindowCleanup(win);
        }
        DisplayContent dc = win.getDisplayContent();
        if (win.mAttrs.type == 2013) {
            dc.mWallpaperController.clearLastWallpaperTimeoutTime();
            dc.pendingLayoutChanges |= 4;
        } else if ((win.mAttrs.flags & DumpState.DUMP_DEXOPT) != 0) {
            dc.pendingLayoutChanges |= 4;
        }
        if (!(dc == null || (this.mWindowPlacerLocked.isInLayout() ^ 1) == 0)) {
            dc.assignWindowLayers(true);
            this.mWindowPlacerLocked.performSurfacePlacement();
            if (win.mAppToken != null) {
                win.mAppToken.updateReportedVisibilityLocked();
            }
        }
        this.mInputMonitor.updateInputWindowsLw(true);
    }

    void setInputMethodWindowLocked(WindowState win) {
        this.mInputMethodWindow = win;
        (win != null ? win.getDisplayContent() : getDefaultDisplayContentLocked()).computeImeTarget(true);
    }

    private void updateAppOpsState() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mRoot.updateAppOpsState();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    static void logSurface(WindowState w, String msg, boolean withStackTrace) {
        String str = "  SURFACE " + msg + ": " + w;
        if (withStackTrace) {
            logWithStack(TAG, str);
        } else {
            Slog.i(TAG, str);
        }
    }

    static void logSurface(SurfaceControl s, String title, String msg) {
        Slog.i(TAG, "  SURFACE " + s + ": " + msg + " / " + title);
    }

    static void logWithStack(String tag, String s) {
        Slog.i(tag, s, null);
    }

    void setTransparentRegionWindow(Session session, IWindow client, Region region) {
        long origId = Binder.clearCallingIdentity();
        try {
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                WindowState w = windowForClientLocked(session, client, false);
                if (w != null && w.mHasSurface) {
                    w.mWinAnimator.setTransparentRegionHintLocked(region);
                }
            }
            resetPriorityAfterLockedSection();
            Binder.restoreCallingIdentity(origId);
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(origId);
        }
    }

    void setInsetsWindow(Session session, IWindow client, int touchableInsets, Rect contentInsets, Rect visibleInsets, Region touchableRegion) {
        long origId = Binder.clearCallingIdentity();
        try {
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                WindowState w = windowForClientLocked(session, client, false);
                if (w != null) {
                    w.mGivenInsetsPending = false;
                    w.mGivenContentInsets.set(contentInsets);
                    w.mGivenVisibleInsets.set(visibleInsets);
                    w.mGivenTouchableRegion.set(touchableRegion);
                    w.mTouchableInsets = touchableInsets;
                    if (w.mGlobalScale != 1.0f) {
                        w.mGivenContentInsets.scale(w.mGlobalScale);
                        w.mGivenVisibleInsets.scale(w.mGlobalScale);
                        w.mGivenTouchableRegion.scale(w.mGlobalScale);
                    }
                    w.setDisplayLayoutNeeded();
                    this.mWindowPlacerLocked.performSurfacePlacement();
                }
            }
            resetPriorityAfterLockedSection();
            Binder.restoreCallingIdentity(origId);
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(origId);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void getWindowDisplayFrame(com.android.server.wm.Session r4, android.view.IWindow r5, android.graphics.Rect r6) {
        /*
        r3 = this;
        r2 = r3.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x001f }
        r1 = 0;
        r0 = r3.windowForClientLocked(r4, r5, r1);	 Catch:{ all -> 0x001f }
        if (r0 != 0) goto L_0x0015;
    L_0x000d:
        r6.setEmpty();	 Catch:{ all -> 0x001f }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0015:
        r1 = r0.mDisplayFrame;	 Catch:{ all -> 0x001f }
        r6.set(r1);	 Catch:{ all -> 0x001f }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x001f:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.getWindowDisplayFrame(com.android.server.wm.Session, android.view.IWindow, android.graphics.Rect):void");
    }

    public void onRectangleOnScreenRequested(IBinder token, Rect rectangle) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                if (this.mAccessibilityController != null) {
                    WindowState window = (WindowState) this.mWindowMap.get(token);
                    if (window != null && window.getDisplayId() == 0) {
                        this.mAccessibilityController.onRectangleOnScreenRequestedLocked(rectangle);
                    }
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public IWindowId getWindowId(IBinder token) {
        IWindowId iWindowId = null;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                WindowState window = (WindowState) this.mWindowMap.get(token);
                if (window != null) {
                    iWindowId = window.mWindowId;
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return iWindowId;
    }

    public void pokeDrawLock(Session session, IBinder token) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                WindowState window = windowForClientLocked(session, token, false);
                if (window != null) {
                    window.pokeDrawLockLw(this.mDrawLockTimeoutMillis);
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int relayoutWindow(com.android.server.wm.Session r34, android.view.IWindow r35, int r36, android.view.WindowManager.LayoutParams r37, int r38, int r39, int r40, int r41, android.graphics.Rect r42, android.graphics.Rect r43, android.graphics.Rect r44, android.graphics.Rect r45, android.graphics.Rect r46, android.graphics.Rect r47, android.graphics.Rect r48, android.util.MergedConfiguration r49, android.view.Surface r50) {
        /*
        r33 = this;
        r20 = 0;
        r0 = r33;
        r0 = r0.mContext;
        r28 = r0;
        r29 = "android.permission.STATUS_BAR";
        r28 = r28.checkCallingOrSelfPermission(r29);
        if (r28 != 0) goto L_0x0037;
    L_0x0011:
        r14 = 1;
    L_0x0012:
        r18 = android.os.Binder.clearCallingIdentity();
        r0 = r33;
        r0 = r0.mWindowMap;
        r29 = r0;
        monitor-enter(r29);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x00b7 }
        r28 = 0;
        r0 = r33;
        r1 = r34;
        r2 = r35;
        r3 = r28;
        r26 = r0.windowForClientLocked(r1, r2, r3);	 Catch:{ all -> 0x00b7 }
        if (r26 != 0) goto L_0x0039;
    L_0x0030:
        r28 = 0;
        monitor-exit(r29);
        resetPriorityAfterLockedSection();
        return r28;
    L_0x0037:
        r14 = 0;
        goto L_0x0012;
    L_0x0039:
        r9 = r26.getDisplayId();	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mWinAnimator;	 Catch:{ all -> 0x00b7 }
        r27 = r0;
        r28 = 8;
        r0 = r40;
        r1 = r28;
        if (r0 == r1) goto L_0x0054;
    L_0x004b:
        r0 = r26;
        r1 = r38;
        r2 = r39;
        r0.setRequestedSize(r1, r2);	 Catch:{ all -> 0x00b7 }
    L_0x0054:
        r6 = 0;
        r12 = 0;
        if (r37 == 0) goto L_0x0181;
    L_0x0058:
        r0 = r33;
        r0 = r0.mPolicy;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r37;
        r0.adjustWindowParamsLw(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mSeq;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r36;
        r1 = r28;
        if (r0 != r1) goto L_0x0092;
    L_0x0071:
        r0 = r37;
        r0 = r0.systemUiVisibility;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r37;
        r0 = r0.subtreeSystemUiVisibility;	 Catch:{ all -> 0x00b7 }
        r30 = r0;
        r22 = r28 | r30;
        r28 = 67043328; // 0x3ff0000 float:1.4987553E-36 double:3.3123805E-316;
        r28 = r28 & r22;
        if (r28 == 0) goto L_0x008c;
    L_0x0085:
        if (r14 != 0) goto L_0x008c;
    L_0x0087:
        r28 = -67043329; // 0xfffffffffc00ffff float:-2.679225E36 double:NaN;
        r22 = r22 & r28;
    L_0x008c:
        r0 = r22;
        r1 = r26;
        r1.mSystemUiVisibility = r0;	 Catch:{ all -> 0x00b7 }
    L_0x0092:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.type;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r37;
        r0 = r0.type;	 Catch:{ all -> 0x00b7 }
        r30 = r0;
        r0 = r28;
        r1 = r30;
        if (r0 == r1) goto L_0x00bd;
    L_0x00aa:
        r28 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x00b7 }
        r30 = "Window type can not be changed after the window is added.";
        r0 = r28;
        r1 = r30;
        r0.<init>(r1);	 Catch:{ all -> 0x00b7 }
        throw r28;	 Catch:{ all -> 0x00b7 }
    L_0x00b7:
        r28 = move-exception;
        monitor-exit(r29);
        resetPriorityAfterLockedSection();
        throw r28;
    L_0x00bd:
        r0 = r37;
        r0 = r0.privateFlags;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0 & 8192;
        r28 = r0;
        if (r28 == 0) goto L_0x0113;
    L_0x00cb:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.x;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r37;
        r1.x = r0;	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.y;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r37;
        r1.y = r0;	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.width;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r37;
        r1.width = r0;	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.height;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r37;
        r1.height = r0;	 Catch:{ all -> 0x00b7 }
    L_0x0113:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.flags;	 Catch:{ all -> 0x00b7 }
        r30 = r0;
        r0 = r37;
        r0 = r0.flags;	 Catch:{ all -> 0x00b7 }
        r31 = r0;
        r12 = r30 ^ r31;
        r0 = r28;
        r0.flags = r12;	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r37;
        r6 = r0.copyFrom(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r6 & 16385;
        r28 = r0;
        if (r28 == 0) goto L_0x0147;
    L_0x013f:
        r28 = 1;
        r0 = r28;
        r1 = r26;
        r1.mLayoutNeeded = r0;	 Catch:{ all -> 0x00b7 }
    L_0x0147:
        r0 = r26;
        r0 = r0.mAppToken;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x0164;
    L_0x014f:
        r28 = 524288; // 0x80000 float:7.34684E-40 double:2.590327E-318;
        r28 = r28 & r12;
        if (r28 != 0) goto L_0x015b;
    L_0x0155:
        r28 = 4194304; // 0x400000 float:5.877472E-39 double:2.0722615E-317;
        r28 = r28 & r12;
        if (r28 == 0) goto L_0x0164;
    L_0x015b:
        r0 = r26;
        r0 = r0.mAppToken;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r28.checkKeyguardFlagsChanged();	 Catch:{ all -> 0x00b7 }
    L_0x0164:
        r28 = 33554432; // 0x2000000 float:9.403955E-38 double:1.6578092E-316;
        r28 = r28 & r6;
        if (r28 == 0) goto L_0x0181;
    L_0x016a:
        r0 = r33;
        r0 = r0.mAccessibilityController;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x0181;
    L_0x0172:
        r28 = r26.getDisplayId();	 Catch:{ all -> 0x00b7 }
        if (r28 != 0) goto L_0x0181;
    L_0x0178:
        r0 = r33;
        r0 = r0.mAccessibilityController;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r28.onSomeWindowResizedOrMovedLocked();	 Catch:{ all -> 0x00b7 }
    L_0x0181:
        r28 = r41 & 2;
        if (r28 == 0) goto L_0x04ed;
    L_0x0185:
        r28 = 1;
    L_0x0187:
        r0 = r28;
        r1 = r27;
        r1.mSurfaceDestroyDeferred = r0;	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.privateFlags;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0 & 128;
        r28 = r0;
        if (r28 == 0) goto L_0x04f1;
    L_0x01a1:
        r28 = 1;
    L_0x01a3:
        r0 = r28;
        r1 = r26;
        r1.mEnforceSizeCompat = r0;	 Catch:{ all -> 0x00b7 }
        r0 = r6 & 128;
        r28 = r0;
        if (r28 == 0) goto L_0x01bb;
    L_0x01af:
        r0 = r37;
        r0 = r0.alpha;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r27;
        r1.mAlpha = r0;	 Catch:{ all -> 0x00b7 }
    L_0x01bb:
        r0 = r26;
        r0 = r0.mRequestedWidth;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r26;
        r0 = r0.mRequestedHeight;	 Catch:{ all -> 0x00b7 }
        r30 = r0;
        r0 = r26;
        r1 = r28;
        r2 = r30;
        r0.setWindowScale(r1, r2);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.surfaceInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.left;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 != 0) goto L_0x01f8;
    L_0x01e4:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.surfaceInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.top;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x04f5;
    L_0x01f8:
        r28 = 0;
        r27.setOpaqueLocked(r28);	 Catch:{ all -> 0x00b7 }
    L_0x01fd:
        r28 = 131080; // 0x20008 float:1.83682E-40 double:6.4762E-319;
        r28 = r28 & r12;
        if (r28 == 0) goto L_0x051f;
    L_0x0204:
        r15 = 1;
    L_0x0205:
        r16 = r26.isDefaultDisplay();	 Catch:{ all -> 0x00b7 }
        if (r16 == 0) goto L_0x052c;
    L_0x020b:
        r0 = r26;
        r0 = r0.mViewVisibility;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r40;
        if (r0 != r1) goto L_0x021b;
    L_0x0217:
        r28 = r12 & 8;
        if (r28 == 0) goto L_0x0522;
    L_0x021b:
        r13 = 1;
    L_0x021c:
        r0 = r26;
        r0 = r0.mViewVisibility;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r40;
        if (r0 == r1) goto L_0x0533;
    L_0x0228:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.flags;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r30 = 1048576; // 0x100000 float:1.469368E-39 double:5.180654E-318;
        r28 = r28 & r30;
        if (r28 == 0) goto L_0x052f;
    L_0x023a:
        r25 = 1;
    L_0x023c:
        r28 = 1048576; // 0x100000 float:1.469368E-39 double:5.180654E-318;
        r28 = r28 & r12;
        if (r28 == 0) goto L_0x0537;
    L_0x0242:
        r28 = 1;
    L_0x0244:
        r25 = r25 | r28;
        r0 = r12 & 8192;
        r28 = r0;
        if (r28 == 0) goto L_0x0269;
    L_0x024c:
        r0 = r27;
        r0 = r0.mSurfaceController;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x0269;
    L_0x0254:
        r0 = r27;
        r0 = r0.mSurfaceController;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r33;
        r1 = r26;
        r30 = r0.isSecureLocked(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r28;
        r1 = r30;
        r0.setSecure(r1);	 Catch:{ all -> 0x00b7 }
    L_0x0269:
        r28 = 1;
        r0 = r28;
        r1 = r26;
        r1.mRelayoutCalled = r0;	 Catch:{ all -> 0x00b7 }
        r28 = 1;
        r0 = r28;
        r1 = r26;
        r1.mInRelayout = r0;	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mViewVisibility;	 Catch:{ all -> 0x00b7 }
        r17 = r0;
        r0 = r40;
        r1 = r26;
        r1.mViewVisibility = r0;	 Catch:{ all -> 0x00b7 }
        if (r40 != 0) goto L_0x0549;
    L_0x0287:
        r0 = r26;
        r0 = r0.mAppToken;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x02a3;
    L_0x028f:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.type;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r30 = 3;
        r0 = r28;
        r1 = r30;
        if (r0 != r1) goto L_0x053b;
    L_0x02a3:
        r21 = 1;
    L_0x02a5:
        if (r21 == 0) goto L_0x05a5;
    L_0x02a7:
        r28 = "relayoutWindow: viewVisibility_1";
        r30 = 32;
        r0 = r30;
        r2 = r28;
        android.os.Trace.traceBegin(r0, r2);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mLayoutSeq;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r30 = -1;
        r0 = r28;
        r1 = r30;
        if (r0 != r1) goto L_0x02d3;
    L_0x02c1:
        r26.setDisplayLayoutNeeded();	 Catch:{ all -> 0x00b7 }
        r0 = r33;
        r0 = r0.mWindowPlacerLocked;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r30 = 1;
        r0 = r28;
        r1 = r30;
        r0.performSurfacePlacement(r1);	 Catch:{ all -> 0x00b7 }
    L_0x02d3:
        r0 = r26;
        r1 = r20;
        r2 = r17;
        r20 = r0.relayoutVisibleWindow(r1, r6, r2);	 Catch:{ all -> 0x00b7 }
        r0 = r33;
        r1 = r50;
        r2 = r20;
        r3 = r26;
        r4 = r27;
        r20 = r0.createSurfaceControl(r1, r2, r3, r4);	 Catch:{ Exception -> 0x054d }
        r28 = r20 & 2;
        if (r28 == 0) goto L_0x02f1;
    L_0x02ef:
        r13 = r16;
    L_0x02f1:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.type;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r30 = 2011; // 0x7db float:2.818E-42 double:9.936E-321;
        r0 = r28;
        r1 = r30;
        if (r0 != r1) goto L_0x0315;
    L_0x0305:
        r0 = r33;
        r0 = r0.mInputMethodWindow;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 != 0) goto L_0x0315;
    L_0x030d:
        r0 = r33;
        r1 = r26;
        r0.setInputMethodWindowLocked(r1);	 Catch:{ all -> 0x00b7 }
        r15 = 1;
    L_0x0315:
        r26.adjustStartingWindowFlags();	 Catch:{ all -> 0x00b7 }
        r30 = 32;
        android.os.Trace.traceEnd(r30);	 Catch:{ all -> 0x00b7 }
    L_0x031d:
        if (r13 == 0) goto L_0x0330;
    L_0x031f:
        r28 = 3;
        r30 = 0;
        r0 = r33;
        r1 = r28;
        r2 = r30;
        r28 = r0.updateFocusedWindowLocked(r1, r2);	 Catch:{ all -> 0x00b7 }
        if (r28 == 0) goto L_0x0330;
    L_0x032f:
        r15 = 0;
    L_0x0330:
        r28 = r20 & 2;
        if (r28 == 0) goto L_0x0660;
    L_0x0334:
        r23 = 1;
    L_0x0336:
        r8 = r26.getDisplayContent();	 Catch:{ all -> 0x00b7 }
        if (r15 == 0) goto L_0x034c;
    L_0x033c:
        r28 = 1;
        r0 = r28;
        r8.computeImeTarget(r0);	 Catch:{ all -> 0x00b7 }
        if (r23 == 0) goto L_0x034c;
    L_0x0345:
        r28 = 0;
        r0 = r28;
        r8.assignWindowLayers(r0);	 Catch:{ all -> 0x00b7 }
    L_0x034c:
        if (r25 == 0) goto L_0x0360;
    L_0x034e:
        r28 = r26.getDisplayContent();	 Catch:{ all -> 0x00b7 }
        r0 = r28;
        r0 = r0.pendingLayoutChanges;	 Catch:{ all -> 0x00b7 }
        r30 = r0;
        r30 = r30 | 4;
        r0 = r30;
        r1 = r28;
        r1.pendingLayoutChanges = r0;	 Catch:{ all -> 0x00b7 }
    L_0x0360:
        r0 = r26;
        r0 = r0.mAppToken;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x037b;
    L_0x0368:
        r0 = r33;
        r0 = r0.mUnknownAppVisibilityController;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r26;
        r0 = r0.mAppToken;	 Catch:{ all -> 0x00b7 }
        r30 = r0;
        r0 = r28;
        r1 = r30;
        r0.notifyRelayouted(r1);	 Catch:{ all -> 0x00b7 }
    L_0x037b:
        r26.setDisplayLayoutNeeded();	 Catch:{ all -> 0x00b7 }
        r28 = r41 & 1;
        if (r28 == 0) goto L_0x0664;
    L_0x0382:
        r28 = 1;
    L_0x0384:
        r0 = r28;
        r1 = r26;
        r1.mGivenInsetsPending = r0;	 Catch:{ all -> 0x00b7 }
        r28 = "relayoutWindow: updateOrientationFromAppTokens";
        r30 = 32;
        r0 = r30;
        r2 = r28;
        android.os.Trace.traceBegin(r0, r2);	 Catch:{ all -> 0x00b7 }
        r28 = 0;
        r0 = r33;
        r1 = r28;
        r7 = r0.updateOrientationFromAppTokensLocked(r1, r9);	 Catch:{ all -> 0x00b7 }
        r30 = 32;
        android.os.Trace.traceEnd(r30);	 Catch:{ all -> 0x00b7 }
        r0 = r33;
        r0 = r0.mWindowPlacerLocked;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r30 = 1;
        r0 = r28;
        r1 = r30;
        r0.performSurfacePlacement(r1);	 Catch:{ all -> 0x00b7 }
        if (r23 == 0) goto L_0x03e1;
    L_0x03b6:
        r0 = r26;
        r0 = r0.mIsWallpaper;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x03e1;
    L_0x03be:
        r28 = r26.getDisplayContent();	 Catch:{ all -> 0x00b7 }
        r10 = r28.getDisplayInfo();	 Catch:{ all -> 0x00b7 }
        r0 = r8.mWallpaperController;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r10.logicalWidth;	 Catch:{ all -> 0x00b7 }
        r30 = r0;
        r0 = r10.logicalHeight;	 Catch:{ all -> 0x00b7 }
        r31 = r0;
        r32 = 0;
        r0 = r28;
        r1 = r26;
        r2 = r30;
        r3 = r31;
        r4 = r32;
        r0.updateWallpaperOffset(r1, r2, r3, r4);	 Catch:{ all -> 0x00b7 }
    L_0x03e1:
        r0 = r26;
        r0 = r0.mAppToken;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x03f2;
    L_0x03e9:
        r0 = r26;
        r0 = r0.mAppToken;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r28.updateReportedVisibilityLocked();	 Catch:{ all -> 0x00b7 }
    L_0x03f2:
        r0 = r27;
        r0 = r0.mReportSurfaceResized;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x0404;
    L_0x03fa:
        r28 = 0;
        r0 = r28;
        r1 = r27;
        r1.mReportSurfaceResized = r0;	 Catch:{ all -> 0x00b7 }
        r20 = r20 | 32;
    L_0x0404:
        r0 = r33;
        r0 = r0.mPolicy;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r26;
        r28 = r0.isNavBarForcedShownLw(r1);	 Catch:{ all -> 0x00b7 }
        if (r28 == 0) goto L_0x0416;
    L_0x0414:
        r20 = r20 | 64;
    L_0x0416:
        r28 = r26.isGoneForLayoutLw();	 Catch:{ all -> 0x00b7 }
        if (r28 != 0) goto L_0x0424;
    L_0x041c:
        r28 = 0;
        r0 = r28;
        r1 = r26;
        r1.mResizedWhileGone = r0;	 Catch:{ all -> 0x00b7 }
    L_0x0424:
        if (r21 == 0) goto L_0x0668;
    L_0x0426:
        r0 = r26;
        r1 = r49;
        r0.getMergedConfiguration(r1);	 Catch:{ all -> 0x00b7 }
    L_0x042d:
        r0 = r26;
        r1 = r49;
        r0.setLastReportedMergedConfiguration(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mCompatFrame;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r42;
        r1 = r28;
        r0.set(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mOverscanInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r43;
        r1 = r28;
        r0.set(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mContentInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r44;
        r1 = r28;
        r0.set(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mLastRelayoutContentInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r26;
        r0 = r0.mContentInsets;	 Catch:{ all -> 0x00b7 }
        r30 = r0;
        r0 = r28;
        r1 = r30;
        r0.set(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mVisibleInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r45;
        r1 = r28;
        r0.set(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mStableInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r46;
        r1 = r28;
        r0.set(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mOutsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r47;
        r1 = r28;
        r0.set(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mFrame;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r26;
        r1 = r28;
        r28 = r0.getBackdropFrame(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r48;
        r1 = r28;
        r0.set(r1);	 Catch:{ all -> 0x00b7 }
        r0 = r33;
        r0 = r0.mInTouchMode;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x0671;
    L_0x04b2:
        r28 = 1;
    L_0x04b4:
        r20 = r20 | r28;
        r0 = r33;
        r0 = r0.mInputMonitor;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r30 = 1;
        r0 = r28;
        r1 = r30;
        r0.updateInputWindowsLw(r1);	 Catch:{ all -> 0x00b7 }
        r28 = 0;
        r0 = r28;
        r1 = r26;
        r1.mInRelayout = r0;	 Catch:{ all -> 0x00b7 }
        monitor-exit(r29);
        resetPriorityAfterLockedSection();
        if (r7 == 0) goto L_0x04e9;
    L_0x04d3:
        r28 = "relayoutWindow: sendNewConfiguration";
        r30 = 32;
        r0 = r30;
        r2 = r28;
        android.os.Trace.traceBegin(r0, r2);
        r0 = r33;
        r0.sendNewConfiguration(r9);
        r28 = 32;
        android.os.Trace.traceEnd(r28);
    L_0x04e9:
        android.os.Binder.restoreCallingIdentity(r18);
        return r20;
    L_0x04ed:
        r28 = 0;
        goto L_0x0187;
    L_0x04f1:
        r28 = 0;
        goto L_0x01a3;
    L_0x04f5:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.surfaceInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.right;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 != 0) goto L_0x01f8;
    L_0x0509:
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.surfaceInsets;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r0 = r0.bottom;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 == 0) goto L_0x01fd;
    L_0x051d:
        goto L_0x01f8;
    L_0x051f:
        r15 = 0;
        goto L_0x0205;
    L_0x0522:
        r0 = r26;
        r0 = r0.mRelayoutCalled;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r13 = r28 ^ 1;
        goto L_0x021c;
    L_0x052c:
        r13 = 0;
        goto L_0x021c;
    L_0x052f:
        r25 = 0;
        goto L_0x023c;
    L_0x0533:
        r25 = 0;
        goto L_0x023c;
    L_0x0537:
        r28 = 0;
        goto L_0x0244;
    L_0x053b:
        r0 = r26;
        r0 = r0.mAppToken;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r28 = r28.isClientHidden();	 Catch:{ all -> 0x00b7 }
        r21 = r28 ^ 1;
        goto L_0x02a5;
    L_0x0549:
        r21 = 0;
        goto L_0x02a5;
    L_0x054d:
        r11 = move-exception;
        r0 = r33;
        r0 = r0.mInputMonitor;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r30 = 1;
        r0 = r28;
        r1 = r30;
        r0.updateInputWindowsLw(r1);	 Catch:{ all -> 0x00b7 }
        r28 = "WindowManager";
        r30 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00b7 }
        r30.<init>();	 Catch:{ all -> 0x00b7 }
        r31 = "Exception thrown when creating surface for client ";
        r30 = r30.append(r31);	 Catch:{ all -> 0x00b7 }
        r0 = r30;
        r1 = r35;
        r30 = r0.append(r1);	 Catch:{ all -> 0x00b7 }
        r31 = " (";
        r30 = r30.append(r31);	 Catch:{ all -> 0x00b7 }
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x00b7 }
        r31 = r0;
        r31 = r31.getTitle();	 Catch:{ all -> 0x00b7 }
        r30 = r30.append(r31);	 Catch:{ all -> 0x00b7 }
        r31 = ")";
        r30 = r30.append(r31);	 Catch:{ all -> 0x00b7 }
        r30 = r30.toString();	 Catch:{ all -> 0x00b7 }
        r0 = r28;
        r1 = r30;
        android.util.Slog.w(r0, r1, r11);	 Catch:{ all -> 0x00b7 }
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x00b7 }
        r28 = 0;
        monitor-exit(r29);
        resetPriorityAfterLockedSection();
        return r28;
    L_0x05a5:
        r28 = "relayoutWindow: viewVisibility_2";
        r30 = 32;
        r0 = r30;
        r2 = r28;
        android.os.Trace.traceBegin(r0, r2);	 Catch:{ all -> 0x00b7 }
        r28 = 0;
        r0 = r28;
        r1 = r27;
        r1.mEnterAnimationPending = r0;	 Catch:{ all -> 0x00b7 }
        r28 = 0;
        r0 = r28;
        r1 = r27;
        r1.mEnteringAnimation = r0;	 Catch:{ all -> 0x00b7 }
        if (r17 == 0) goto L_0x061e;
    L_0x05c3:
        r24 = r26.isAnimatingWithSavedSurface();	 Catch:{ all -> 0x00b7 }
    L_0x05c7:
        r28 = r27.hasSurface();	 Catch:{ all -> 0x00b7 }
        if (r28 == 0) goto L_0x05f1;
    L_0x05cd:
        r0 = r26;
        r0 = r0.mAnimatingExit;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r28 = r28 ^ 1;
        if (r28 == 0) goto L_0x05f1;
    L_0x05d7:
        r28 = r24 ^ 1;
        if (r28 == 0) goto L_0x05f1;
    L_0x05db:
        r0 = r26;
        r0 = r0.mWillReplaceWindow;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        if (r28 != 0) goto L_0x05ef;
    L_0x05e3:
        r0 = r33;
        r1 = r26;
        r2 = r27;
        r3 = r16;
        r13 = r0.tryStartExitingAnimation(r1, r2, r3, r13);	 Catch:{ all -> 0x00b7 }
    L_0x05ef:
        r20 = 4;
    L_0x05f1:
        if (r40 != 0) goto L_0x0621;
    L_0x05f3:
        r28 = r27.hasSurface();	 Catch:{ all -> 0x00b7 }
        if (r28 == 0) goto L_0x0621;
    L_0x05f9:
        r28 = "relayoutWindow: getSurface";
        r30 = 32;
        r0 = r30;
        r2 = r28;
        android.os.Trace.traceBegin(r0, r2);	 Catch:{ all -> 0x00b7 }
        r0 = r27;
        r0 = r0.mSurfaceController;	 Catch:{ all -> 0x00b7 }
        r28 = r0;
        r0 = r28;
        r1 = r50;
        r0.getSurface(r1);	 Catch:{ all -> 0x00b7 }
        r30 = 32;
        android.os.Trace.traceEnd(r30);	 Catch:{ all -> 0x00b7 }
    L_0x0617:
        r30 = 32;
        android.os.Trace.traceEnd(r30);	 Catch:{ all -> 0x00b7 }
        goto L_0x031d;
    L_0x061e:
        r24 = 0;
        goto L_0x05c7;
    L_0x0621:
        r28 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0659 }
        r28.<init>();	 Catch:{ all -> 0x0659 }
        r30 = "wmReleaseOutSurface_";
        r0 = r28;
        r1 = r30;
        r28 = r0.append(r1);	 Catch:{ all -> 0x0659 }
        r0 = r26;
        r0 = r0.mAttrs;	 Catch:{ all -> 0x0659 }
        r30 = r0;
        r30 = r30.getTitle();	 Catch:{ all -> 0x0659 }
        r0 = r28;
        r1 = r30;
        r28 = r0.append(r1);	 Catch:{ all -> 0x0659 }
        r28 = r28.toString();	 Catch:{ all -> 0x0659 }
        r30 = 32;
        r0 = r30;
        r2 = r28;
        android.os.Trace.traceBegin(r0, r2);	 Catch:{ all -> 0x0659 }
        r50.release();	 Catch:{ all -> 0x0659 }
        r30 = 32;
        android.os.Trace.traceEnd(r30);	 Catch:{ all -> 0x00b7 }
        goto L_0x0617;
    L_0x0659:
        r28 = move-exception;
        r30 = 32;
        android.os.Trace.traceEnd(r30);	 Catch:{ all -> 0x00b7 }
        throw r28;	 Catch:{ all -> 0x00b7 }
    L_0x0660:
        r23 = 0;
        goto L_0x0336;
    L_0x0664:
        r28 = 0;
        goto L_0x0384;
    L_0x0668:
        r0 = r26;
        r1 = r49;
        r0.getLastReportedMergedConfiguration(r1);	 Catch:{ all -> 0x00b7 }
        goto L_0x042d;
    L_0x0671:
        r28 = 0;
        goto L_0x04b4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.relayoutWindow(com.android.server.wm.Session, android.view.IWindow, int, android.view.WindowManager$LayoutParams, int, int, int, int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect, android.util.MergedConfiguration, android.view.Surface):int");
    }

    private boolean tryStartExitingAnimation(WindowState win, WindowStateAnimator winAnimator, boolean isDefaultDisplay, boolean focusMayChange) {
        int transit = 2;
        if (win.mAttrs.type == 3) {
            transit = 5;
        }
        if (win.isWinVisibleLw() && winAnimator.applyAnimationLocked(transit, false)) {
            focusMayChange = isDefaultDisplay;
            win.mAnimatingExit = true;
            win.mWinAnimator.mAnimating = true;
        } else if (win.mWinAnimator.isAnimationSet()) {
            win.mAnimatingExit = true;
            win.mWinAnimator.mAnimating = true;
        } else if (win.getDisplayContent().mWallpaperController.isWallpaperTarget(win)) {
            win.mAnimatingExit = true;
            win.mWinAnimator.mAnimating = true;
        } else {
            if (this.mInputMethodWindow == win) {
                setInputMethodWindowLocked(null);
            }
            boolean z = win.mAppToken != null ? win.mAppToken.mAppStopped : false;
            win.mDestroying = true;
            win.destroySurface(false, z);
        }
        if (this.mAccessibilityController != null && win.getDisplayId() == 0) {
            this.mAccessibilityController.onWindowTransitionLocked(win, transit);
        }
        SurfaceControl.openTransaction();
        winAnimator.detachChildren();
        SurfaceControl.closeTransaction();
        return focusMayChange;
    }

    private int createSurfaceControl(Surface outSurface, int result, WindowState win, WindowStateAnimator winAnimator) {
        if (!win.mHasSurface) {
            result |= 4;
        }
        try {
            Trace.traceBegin(32, "createSurfaceControl");
            WindowSurfaceController surfaceController = winAnimator.createSurfaceLocked(win.mAttrs.type, win.mOwnerUid);
            if (surfaceController != null) {
                surfaceController.getSurface(outSurface);
            } else {
                Slog.w(TAG, "Failed to create surface control for " + win);
                outSurface.release();
            }
            return result;
        } finally {
            Trace.traceEnd(32);
        }
    }

    public boolean outOfMemoryWindow(Session session, IWindow client) {
        long origId = Binder.clearCallingIdentity();
        try {
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                WindowState win = windowForClientLocked(session, client, false);
                if (win == null) {
                    resetPriorityAfterLockedSection();
                    Binder.restoreCallingIdentity(origId);
                    return false;
                }
                boolean reclaimSomeSurfaceMemory = this.mRoot.reclaimSomeSurfaceMemory(win.mWinAnimator, "from-client", false);
                resetPriorityAfterLockedSection();
                Binder.restoreCallingIdentity(origId);
                return reclaimSomeSurfaceMemory;
            }
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(origId);
        }
    }

    void finishDrawingWindow(Session session, IWindow client) {
        long origId = Binder.clearCallingIdentity();
        try {
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                WindowState win = windowForClientLocked(session, client, false);
                if (win != null && win.mWinAnimator.finishDrawingLocked()) {
                    if ((win.mAttrs.flags & DumpState.DUMP_DEXOPT) != 0) {
                        DisplayContent displayContent = win.getDisplayContent();
                        displayContent.pendingLayoutChanges |= 4;
                    }
                    win.setDisplayLayoutNeeded();
                    this.mWindowPlacerLocked.requestTraversal();
                }
            }
            resetPriorityAfterLockedSection();
            Binder.restoreCallingIdentity(origId);
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(origId);
        }
    }

    boolean applyAnimationLocked(AppWindowToken atoken, LayoutParams lp, int transit, boolean enter, boolean isVoiceInteraction) {
        Trace.traceBegin(32, "WM#applyAnimationLocked");
        if (atoken.okToAnimate()) {
            DisplayContent displayContent = atoken.getTask().getDisplayContent();
            DisplayInfo displayInfo = displayContent.getDisplayInfo();
            int width = displayInfo.appWidth;
            int height = displayInfo.appHeight;
            WindowState win = atoken.findMainWindow();
            Rect frame = new Rect(0, 0, width, height);
            Rect displayFrame = new Rect(0, 0, displayInfo.logicalWidth, displayInfo.logicalHeight);
            Rect insets = new Rect();
            Rect stableInsets = new Rect();
            Rect surfaceInsets = null;
            boolean inFreeformWorkspace = win != null ? win.inFreeformWorkspace() : false;
            if (win != null) {
                if (inFreeformWorkspace) {
                    frame.set(win.mFrame);
                } else {
                    frame.set(win.mContainingFrame);
                }
                surfaceInsets = win.getAttrs().surfaceInsets;
                insets.set(win.mContentInsets);
                stableInsets.set(win.mStableInsets);
            }
            if (atoken.mLaunchTaskBehind) {
                enter = false;
            }
            Configuration displayConfig = displayContent.getConfiguration();
            Animation a = this.mAppTransition.loadAnimation(lp, transit, enter, displayConfig.uiMode, displayConfig.orientation, frame, displayFrame, insets, surfaceInsets, stableInsets, isVoiceInteraction, inFreeformWorkspace, atoken.getTask().mTaskId);
            if (a != null) {
                atoken.mAppAnimator.setAnimation(a, frame.width(), frame.height(), width, height, this.mAppTransition.canSkipFirstFrame(), this.mAppTransition.getAppStackClipMode(), transit, this.mAppTransition.getTransitFlags());
            }
        } else {
            atoken.mAppAnimator.clearAnimation();
        }
        Trace.traceEnd(32);
        if (atoken.mAppAnimator.animation != null) {
            return true;
        }
        return false;
    }

    boolean checkCallingPermission(String permission, String func) {
        if (Binder.getCallingPid() == Process.myPid() || this.mContext.checkCallingPermission(permission) == 0) {
            return true;
        }
        Slog.w(TAG, "Permission Denial: " + func + " from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " requires " + permission);
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addWindowToken(android.os.IBinder r16, int r17, int r18) {
        /*
        r15 = this;
        r1 = "android.permission.MANAGE_APP_TOKENS";
        r2 = "addWindowToken()";
        r1 = r15.checkCallingPermission(r1, r2);
        if (r1 != 0) goto L_0x0015;
    L_0x000c:
        r1 = new java.lang.SecurityException;
        r2 = "Requires MANAGE_APP_TOKENS permission";
        r1.<init>(r2);
        throw r1;
    L_0x0015:
        r14 = r15.mWindowMap;
        monitor-enter(r14);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0087 }
        r1 = r15.mRoot;	 Catch:{ all -> 0x0087 }
        r0 = r18;
        r5 = r1.getDisplayContentOrCreate(r0);	 Catch:{ all -> 0x0087 }
        r0 = r16;
        r13 = r5.getWindowToken(r0);	 Catch:{ all -> 0x0087 }
        if (r13 == 0) goto L_0x0064;
    L_0x002b:
        r1 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0087 }
        r2.<init>();	 Catch:{ all -> 0x0087 }
        r3 = "addWindowToken: Attempted to add binder token: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0087 }
        r0 = r16;
        r2 = r2.append(r0);	 Catch:{ all -> 0x0087 }
        r3 = " for already created window token: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0087 }
        r2 = r2.append(r13);	 Catch:{ all -> 0x0087 }
        r3 = " displayId=";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0087 }
        r0 = r18;
        r2 = r2.append(r0);	 Catch:{ all -> 0x0087 }
        r2 = r2.toString();	 Catch:{ all -> 0x0087 }
        android.util.Slog.w(r1, r2);	 Catch:{ all -> 0x0087 }
        monitor-exit(r14);
        resetPriorityAfterLockedSection();
        return;
    L_0x0064:
        r1 = 2013; // 0x7dd float:2.821E-42 double:9.946E-321;
        r0 = r17;
        if (r0 != r1) goto L_0x0079;
    L_0x006a:
        r1 = new com.android.server.wm.WallpaperWindowToken;	 Catch:{ all -> 0x0087 }
        r4 = 1;
        r6 = 1;
        r2 = r15;
        r3 = r16;
        r1.<init>(r2, r3, r4, r5, r6);	 Catch:{ all -> 0x0087 }
    L_0x0074:
        monitor-exit(r14);
        resetPriorityAfterLockedSection();
        return;
    L_0x0079:
        r6 = new com.android.server.wm.WindowToken;	 Catch:{ all -> 0x0087 }
        r10 = 1;
        r12 = 1;
        r7 = r15;
        r8 = r16;
        r9 = r17;
        r11 = r5;
        r6.<init>(r7, r8, r9, r10, r11, r12);	 Catch:{ all -> 0x0087 }
        goto L_0x0074;
    L_0x0087:
        r1 = move-exception;
        monitor-exit(r14);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.addWindowToken(android.os.IBinder, int, int):void");
    }

    public void removeWindowToken(IBinder binder, int displayId) {
        if (checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "removeWindowToken()")) {
            long origId = Binder.clearCallingIdentity();
            try {
                synchronized (this.mWindowMap) {
                    boostPriorityForLockedSection();
                    DisplayContent dc = this.mRoot.getDisplayContent(displayId);
                    if (dc == null) {
                        Slog.w(TAG, "removeWindowToken: Attempted to remove token: " + binder + " for non-exiting displayId=" + displayId);
                        resetPriorityAfterLockedSection();
                        Binder.restoreCallingIdentity(origId);
                    } else if (dc.removeWindowToken(binder) == null) {
                        Slog.w(TAG, "removeWindowToken: Attempted to remove non-existing token: " + binder);
                        resetPriorityAfterLockedSection();
                        Binder.restoreCallingIdentity(origId);
                    } else {
                        this.mInputMonitor.updateInputWindowsLw(true);
                        resetPriorityAfterLockedSection();
                        Binder.restoreCallingIdentity(origId);
                    }
                }
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(origId);
            }
        } else {
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        }
    }

    public Configuration updateOrientationFromAppTokens(Configuration currentConfig, IBinder freezeThisOneIfNeeded, int displayId) {
        if (checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "updateOrientationFromAppTokens()")) {
            long ident = Binder.clearCallingIdentity();
            try {
                Configuration config;
                synchronized (this.mWindowMap) {
                    boostPriorityForLockedSection();
                    config = updateOrientationFromAppTokensLocked(currentConfig, freezeThisOneIfNeeded, displayId);
                }
                resetPriorityAfterLockedSection();
                Binder.restoreCallingIdentity(ident);
                return config;
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        } else {
            throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
        }
    }

    private Configuration updateOrientationFromAppTokensLocked(Configuration currentConfig, IBinder freezeThisOneIfNeeded, int displayId) {
        if (!this.mDisplayReady) {
            return null;
        }
        Configuration config = null;
        if (updateOrientationFromAppTokensLocked(false, displayId)) {
            if (!(freezeThisOneIfNeeded == null || (this.mRoot.mOrientationChangeComplete ^ 1) == 0)) {
                AppWindowToken atoken = this.mRoot.getAppWindowToken(freezeThisOneIfNeeded);
                if (atoken != null) {
                    atoken.startFreezingScreen();
                }
            }
            config = computeNewConfigurationLocked(displayId);
        } else if (currentConfig != null) {
            this.mTempConfiguration.unset();
            this.mTempConfiguration.updateFrom(currentConfig);
            DisplayContent displayContent = this.mRoot.getDisplayContent(displayId);
            displayContent.computeScreenConfiguration(this.mTempConfiguration);
            if (currentConfig.diff(this.mTempConfiguration) != 0) {
                this.mWaitingForConfig = true;
                displayContent.setLayoutNeeded();
                int[] anim = new int[2];
                if (displayContent.isDimming()) {
                    anim[1] = 0;
                    anim[0] = 0;
                } else {
                    this.mPolicy.selectRotationAnimationLw(anim);
                }
                startFreezingDisplayLocked(false, anim[0], anim[1], displayContent);
                config = new Configuration(this.mTempConfiguration);
            }
        }
        return config;
    }

    boolean updateOrientationFromAppTokensLocked(boolean inTransaction, int displayId) {
        long ident = Binder.clearCallingIdentity();
        try {
            DisplayContent dc = this.mRoot.getDisplayContent(displayId);
            int req = dc.getOrientation();
            if (req != dc.getLastOrientation()) {
                dc.setLastOrientation(req);
                if (dc.isDefaultDisplay) {
                    this.mPolicy.setCurrentOrientationLw(req);
                }
                if (dc.updateRotationUnchecked(inTransaction)) {
                    return true;
                }
            }
            Binder.restoreCallingIdentity(ident);
            return false;
        } finally {
            Binder.restoreCallingIdentity(ident);
        }
    }

    boolean rotationNeedsUpdateLocked() {
        DisplayContent defaultDisplayContent = getDefaultDisplayContentLocked();
        int lastOrientation = defaultDisplayContent.getLastOrientation();
        int oldRotation = defaultDisplayContent.getRotation();
        boolean oldAltOrientation = defaultDisplayContent.getAltOrientation();
        int rotation = this.mPolicy.rotationForOrientationLw(lastOrientation, oldRotation);
        boolean altOrientation = this.mPolicy.rotationHasCompatibleMetricsLw(lastOrientation, rotation) ^ 1;
        if (oldRotation == rotation && oldAltOrientation == altOrientation) {
            return false;
        }
        return true;
    }

    public int[] setNewDisplayOverrideConfiguration(Configuration overrideConfig, int displayId) {
        if (checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setNewDisplayOverrideConfiguration()")) {
            int[] displayOverrideConfigurationIfNeeded;
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    if (this.mWaitingForConfig) {
                        this.mWaitingForConfig = false;
                        this.mLastFinishedFreezeSource = "new-config";
                    }
                    displayOverrideConfigurationIfNeeded = this.mRoot.setDisplayOverrideConfigurationIfNeeded(overrideConfig, displayId);
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
            return displayOverrideConfigurationIfNeeded;
        }
        throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
    }

    void setFocusTaskRegionLocked(AppWindowToken previousFocus) {
        Task task = this.mFocusedApp != null ? this.mFocusedApp.getTask() : null;
        Task task2 = previousFocus != null ? previousFocus.getTask() : null;
        DisplayContent displayContent = task != null ? task.getDisplayContent() : null;
        DisplayContent displayContent2 = task2 != null ? task2.getDisplayContent() : null;
        if (!(displayContent2 == null || displayContent2 == displayContent)) {
            displayContent2.setTouchExcludeRegion(null);
        }
        if (displayContent != null) {
            displayContent.setTouchExcludeRegion(task);
        }
    }

    public void setFocusedApp(IBinder token, boolean moveFocusNow) {
        if (checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setFocusedApp()")) {
            synchronized (this.mWindowMap) {
                try {
                    AppWindowToken appWindowToken;
                    boostPriorityForLockedSection();
                    if (token == null) {
                        appWindowToken = null;
                    } else {
                        appWindowToken = this.mRoot.getAppWindowToken(token);
                        if (appWindowToken == null) {
                            Slog.w(TAG, "Attempted to set focus to non-existing app token: " + token);
                        }
                    }
                    boolean changed = this.mFocusedApp != appWindowToken;
                    if (changed) {
                        AppWindowToken prev = this.mFocusedApp;
                        this.mFocusedApp = appWindowToken;
                        this.mInputMonitor.setFocusedAppLw(appWindowToken);
                        setFocusTaskRegionLocked(prev);
                    }
                    if (moveFocusNow && changed) {
                        long origId = Binder.clearCallingIdentity();
                        updateFocusedWindowLocked(0, true);
                        Binder.restoreCallingIdentity(origId);
                    }
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
            return;
        }
        throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
    }

    public void prepareAppTransition(int transit, boolean alwaysKeepCurrent) {
        prepareAppTransition(transit, alwaysKeepCurrent, 0, false);
    }

    public void prepareAppTransition(int transit, boolean alwaysKeepCurrent, int flags, boolean forceOverride) {
        if (checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "prepareAppTransition()")) {
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    boolean prepared = this.mAppTransition.prepareAppTransitionLocked(transit, alwaysKeepCurrent, flags, forceOverride);
                    DisplayContent dc = this.mRoot.getDisplayContent(0);
                    if (prepared && dc != null && dc.okToAnimate()) {
                        this.mSkipAppTransitionAnimation = false;
                    }
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
            return;
        }
        throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
    }

    public int getPendingAppTransition() {
        return this.mAppTransition.getAppTransition();
    }

    public void overridePendingAppTransition(String packageName, int enterAnim, int exitAnim, IRemoteCallback startedCallback) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mAppTransition.overridePendingAppTransition(packageName, enterAnim, exitAnim, startedCallback);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void overridePendingAppTransitionScaleUp(int startX, int startY, int startWidth, int startHeight) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mAppTransition.overridePendingAppTransitionScaleUp(startX, startY, startWidth, startHeight);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void overridePendingAppTransitionClipReveal(int startX, int startY, int startWidth, int startHeight) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mAppTransition.overridePendingAppTransitionClipReveal(startX, startY, startWidth, startHeight);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void overridePendingAppTransitionThumb(GraphicBuffer srcThumb, int startX, int startY, IRemoteCallback startedCallback, boolean scaleUp) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mAppTransition.overridePendingAppTransitionThumb(srcThumb, startX, startY, startedCallback, scaleUp);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void overridePendingAppTransitionAspectScaledThumb(GraphicBuffer srcThumb, int startX, int startY, int targetWidth, int targetHeight, IRemoteCallback startedCallback, boolean scaleUp) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mAppTransition.overridePendingAppTransitionAspectScaledThumb(srcThumb, startX, startY, targetWidth, targetHeight, startedCallback, scaleUp);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void overridePendingAppTransitionMultiThumb(AppTransitionAnimationSpec[] specs, IRemoteCallback onAnimationStartedCallback, IRemoteCallback onAnimationFinishedCallback, boolean scaleUp) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mAppTransition.overridePendingAppTransitionMultiThumb(specs, onAnimationStartedCallback, onAnimationFinishedCallback, scaleUp);
                prolongAnimationsFromSpecs(specs, scaleUp);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    void prolongAnimationsFromSpecs(AppTransitionAnimationSpec[] specs, boolean scaleUp) {
        this.mTmpTaskIds.clear();
        for (int i = specs.length - 1; i >= 0; i--) {
            this.mTmpTaskIds.put(specs[i].taskId, 0);
        }
        for (WindowState win : this.mWindowMap.values()) {
            Task task = win.getTask();
            if (!(task == null || this.mTmpTaskIds.get(task.mTaskId, -1) == -1 || !task.inFreeformWorkspace())) {
                AppWindowToken appToken = win.mAppToken;
                if (!(appToken == null || appToken.mAppAnimator == null)) {
                    appToken.mAppAnimator.startProlongAnimation(scaleUp ? 2 : 1);
                }
            }
        }
    }

    public void overridePendingAppTransitionInPlace(String packageName, int anim) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mAppTransition.overrideInPlaceAppTransition(packageName, anim);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void overridePendingAppTransitionMultiThumbFuture(IAppTransitionAnimationSpecsFuture specsFuture, IRemoteCallback callback, boolean scaleUp) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mAppTransition.overridePendingAppTransitionMultiThumbFuture(specsFuture, callback, scaleUp);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void endProlongedAnimations() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                for (WindowState win : this.mWindowMap.values()) {
                    AppWindowToken appToken = win.mAppToken;
                    if (!(appToken == null || appToken.mAppAnimator == null)) {
                        appToken.mAppAnimator.endProlongedAnimation();
                    }
                }
                this.mAppTransition.notifyProlongedAnimationsEnded();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void executeAppTransition() {
        if (checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "executeAppTransition()")) {
            synchronized (this.mWindowMap) {
                long origId;
                try {
                    boostPriorityForLockedSection();
                    if (this.mAppTransition.isTransitionSet()) {
                        this.mAppTransition.setReady();
                        origId = Binder.clearCallingIdentity();
                        this.mWindowPlacerLocked.performSurfacePlacement();
                        Binder.restoreCallingIdentity(origId);
                    }
                } catch (Throwable th) {
                    resetPriorityAfterLockedSection();
                }
            }
            resetPriorityAfterLockedSection();
            return;
        }
        throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
    }

    public void setAppFullscreen(IBinder token, boolean toOpaque) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                AppWindowToken atoken = this.mRoot.getAppWindowToken(token);
                if (atoken != null) {
                    atoken.setFillsParent(toOpaque);
                    setWindowOpaqueLocked(token, toOpaque);
                    this.mWindowPlacerLocked.requestTraversal();
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setWindowOpaque(IBinder token, boolean isOpaque) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                setWindowOpaqueLocked(token, isOpaque);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    private void setWindowOpaqueLocked(IBinder token, boolean isOpaque) {
        AppWindowToken wtoken = this.mRoot.getAppWindowToken(token);
        if (wtoken != null) {
            WindowState win = wtoken.findMainWindow();
            if (win != null) {
                win.mWinAnimator.setOpaqueLocked(isOpaque);
            }
        }
    }

    void updateTokenInPlaceLocked(AppWindowToken wtoken, int transit) {
        if (transit != -1) {
            if (wtoken.mAppAnimator.animation == AppWindowAnimator.sDummyAnimation) {
                wtoken.mAppAnimator.setNullAnimation();
            }
            applyAnimationLocked(wtoken, null, transit, false, false);
        }
    }

    public void setDockedStackCreateState(int mode, Rect bounds) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                setDockedStackCreateStateLocked(mode, bounds);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    void setDockedStackCreateStateLocked(int mode, Rect bounds) {
        this.mDockedStackCreateMode = mode;
        this.mDockedStackCreateBounds = bounds;
    }

    public boolean isValidPictureInPictureAspectRatio(int displayId, float aspectRatio) {
        return this.mRoot.getDisplayContent(displayId).getPinnedStackController().isValidPictureInPictureAspectRatio(aspectRatio);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void getStackBounds(int r4, android.graphics.Rect r5) {
        /*
        r3 = this;
        r2 = r3.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x001e }
        r1 = r3.mRoot;	 Catch:{ all -> 0x001e }
        r0 = r1.getStackById(r4);	 Catch:{ all -> 0x001e }
        if (r0 == 0) goto L_0x0016;
    L_0x000e:
        r0.getBounds(r5);	 Catch:{ all -> 0x001e }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0016:
        r5.setEmpty();	 Catch:{ all -> 0x001e }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x001e:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.getStackBounds(int, android.graphics.Rect):void");
    }

    public void notifyShowingDreamChanged() {
        notifyKeyguardFlagsChanged(null);
    }

    public WindowState getInputMethodWindowLw() {
        return this.mInputMethodWindow;
    }

    public void notifyKeyguardTrustedChanged() {
        this.mH.sendEmptyMessage(57);
    }

    public void screenTurningOff(ScreenOffListener listener) {
        this.mTaskSnapshotController.screenTurningOff(listener);
    }

    public void deferSurfaceLayout() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mWindowPlacerLocked.deferLayout();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void continueSurfaceLayout() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mWindowPlacerLocked.continueLayout();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public boolean containsShowWhenLockedWindow(IBinder token) {
        boolean containsShowWhenLockedWindow;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                AppWindowToken wtoken = this.mRoot.getAppWindowToken(token);
                containsShowWhenLockedWindow = wtoken != null ? wtoken.containsShowWhenLockedWindow() : false;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return containsShowWhenLockedWindow;
    }

    public boolean containsDismissKeyguardWindow(IBinder token) {
        boolean containsDismissKeyguardWindow;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                AppWindowToken wtoken = this.mRoot.getAppWindowToken(token);
                containsDismissKeyguardWindow = wtoken != null ? wtoken.containsDismissKeyguardWindow() : false;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return containsDismissKeyguardWindow;
    }

    void notifyKeyguardFlagsChanged(Runnable callback) {
        Object -_lambda_jlkbn4gpn9-0nfms_2kb8vtwgfi;
        if (callback != null) {
            -_lambda_jlkbn4gpn9-0nfms_2kb8vtwgfi = new -$Lambda$jlKbn4GPn9-0nFmS_2KB8vTwgFI((byte) 2, this, callback);
        } else {
            -_lambda_jlkbn4gpn9-0nfms_2kb8vtwgfi = null;
        }
        this.mH.obtainMessage(56, -_lambda_jlkbn4gpn9-0nfms_2kb8vtwgfi).sendToTarget();
    }

    /* synthetic */ void lambda$-com_android_server_wm_WindowManagerService_133187(Runnable callback) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                callback.run();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public boolean isKeyguardTrusted() {
        boolean isKeyguardTrustedLw;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                isKeyguardTrustedLw = this.mPolicy.isKeyguardTrustedLw();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return isKeyguardTrustedLw;
    }

    public void setKeyguardGoingAway(boolean keyguardGoingAway) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mKeyguardGoingAway = keyguardGoingAway;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void startFreezingScreen(int exitAnim, int enterAnim) {
        if (checkCallingPermission("android.permission.FREEZE_SCREEN", "startFreezingScreen()")) {
            synchronized (this.mWindowMap) {
                long origId;
                try {
                    boostPriorityForLockedSection();
                    if (!this.mClientFreezingScreen) {
                        this.mClientFreezingScreen = true;
                        origId = Binder.clearCallingIdentity();
                        startFreezingDisplayLocked(false, exitAnim, enterAnim);
                        this.mH.removeMessages(30);
                        this.mH.sendEmptyMessageDelayed(30, 5000);
                        Binder.restoreCallingIdentity(origId);
                    }
                } catch (Throwable th) {
                    resetPriorityAfterLockedSection();
                }
            }
            resetPriorityAfterLockedSection();
            return;
        }
        throw new SecurityException("Requires FREEZE_SCREEN permission");
    }

    public void stopFreezingScreen() {
        if (checkCallingPermission("android.permission.FREEZE_SCREEN", "stopFreezingScreen()")) {
            synchronized (this.mWindowMap) {
                long origId;
                try {
                    boostPriorityForLockedSection();
                    if (this.mClientFreezingScreen) {
                        this.mClientFreezingScreen = false;
                        this.mLastFinishedFreezeSource = "client";
                        origId = Binder.clearCallingIdentity();
                        stopFreezingDisplayLocked();
                        Binder.restoreCallingIdentity(origId);
                    }
                } catch (Throwable th) {
                    resetPriorityAfterLockedSection();
                }
            }
            resetPriorityAfterLockedSection();
            return;
        }
        throw new SecurityException("Requires FREEZE_SCREEN permission");
    }

    public void disableKeyguard(IBinder token, String tag) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DISABLE_KEYGUARD") != 0) {
            throw new SecurityException("Requires DISABLE_KEYGUARD permission");
        } else if (Binder.getCallingUid() != 1000 && isKeyguardSecure()) {
            Log.d(TAG, "current mode is SecurityMode, ignore disableKeyguard");
        } else if (!isCurrentProfileLocked(UserHandle.getCallingUserId())) {
            Log.d(TAG, "non-current profiles, ignore disableKeyguard");
        } else if (token == null) {
            throw new IllegalArgumentException("token == null");
        } else {
            this.mKeyguardDisableHandler.sendMessage(this.mKeyguardDisableHandler.obtainMessage(1, new Pair(token, tag)));
        }
    }

    public void reenableKeyguard(IBinder token) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DISABLE_KEYGUARD") != 0) {
            throw new SecurityException("Requires DISABLE_KEYGUARD permission");
        } else if (token == null) {
            throw new IllegalArgumentException("token == null");
        } else {
            this.mKeyguardDisableHandler.sendMessage(this.mKeyguardDisableHandler.obtainMessage(2, token));
        }
    }

    public void exitKeyguardSecurely(final IOnKeyguardExitResult callback) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.DISABLE_KEYGUARD") != 0) {
            throw new SecurityException("Requires DISABLE_KEYGUARD permission");
        } else if (callback == null) {
            throw new IllegalArgumentException("callback == null");
        } else {
            this.mPolicy.exitKeyguardSecurely(new OnKeyguardExitResult() {
                public void onKeyguardExitResult(boolean success) {
                    try {
                        callback.onKeyguardExitResult(success);
                    } catch (RemoteException e) {
                    }
                }
            });
        }
    }

    public boolean inKeyguardRestrictedInputMode() {
        return this.mPolicy.inKeyguardRestrictedKeyInputMode();
    }

    public boolean isKeyguardLocked() {
        return this.mPolicy.isKeyguardLocked();
    }

    public boolean isKeyguardShowingAndNotOccluded() {
        return this.mPolicy.isKeyguardShowingAndNotOccluded();
    }

    public boolean isKeyguardSecure() {
        int userId = UserHandle.getCallingUserId();
        long origId = Binder.clearCallingIdentity();
        try {
            boolean isKeyguardSecure = this.mPolicy.isKeyguardSecure(userId);
            return isKeyguardSecure;
        } finally {
            Binder.restoreCallingIdentity(origId);
        }
    }

    public boolean isShowingDream() {
        boolean isShowingDreamLw;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                isShowingDreamLw = this.mPolicy.isShowingDreamLw();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return isShowingDreamLw;
    }

    public void dismissKeyguard(IKeyguardDismissCallback callback) {
        checkCallingPermission("android.permission.CONTROL_KEYGUARD", "dismissKeyguard");
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mPolicy.dismissKeyguardLw(callback);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void onKeyguardOccludedChanged(boolean occluded) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mPolicy.onKeyguardOccludedChangedLw(occluded);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setSwitchingUser(boolean switching) {
        if (checkCallingPermission("android.permission.INTERACT_ACROSS_USERS_FULL", "setSwitchingUser()")) {
            this.mPolicy.setSwitchingUser(switching);
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    this.mSwitchingUser = switching;
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
            return;
        }
        throw new SecurityException("Requires INTERACT_ACROSS_USERS_FULL permission");
    }

    void showGlobalActions() {
        this.mPolicy.showGlobalActions();
    }

    public void closeSystemDialogs(String reason) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mRoot.closeSystemDialogs(reason);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    static float fixScale(float scale) {
        if (scale < 0.0f) {
            scale = 0.0f;
        } else if (scale > 20.0f) {
            scale = 20.0f;
        }
        return Math.abs(scale);
    }

    public void setAnimationScale(int which, float scale) {
        if (checkCallingPermission("android.permission.SET_ANIMATION_SCALE", "setAnimationScale()")) {
            scale = fixScale(scale);
            switch (which) {
                case 0:
                    this.mWindowAnimationScaleSetting = scale;
                    break;
                case 1:
                    this.mTransitionAnimationScaleSetting = scale;
                    break;
                case 2:
                    this.mAnimatorDurationScaleSetting = scale;
                    break;
            }
            this.mH.sendEmptyMessage(14);
            return;
        }
        throw new SecurityException("Requires SET_ANIMATION_SCALE permission");
    }

    public void setAnimationScales(float[] scales) {
        if (checkCallingPermission("android.permission.SET_ANIMATION_SCALE", "setAnimationScale()")) {
            if (scales != null) {
                if (scales.length >= 1) {
                    this.mWindowAnimationScaleSetting = fixScale(scales[0]);
                }
                if (scales.length >= 2) {
                    this.mTransitionAnimationScaleSetting = fixScale(scales[1]);
                }
                if (scales.length >= 3) {
                    this.mAnimatorDurationScaleSetting = fixScale(scales[2]);
                    dispatchNewAnimatorScaleLocked(null);
                }
            }
            this.mH.sendEmptyMessage(14);
            return;
        }
        throw new SecurityException("Requires SET_ANIMATION_SCALE permission");
    }

    private void setAnimatorDurationScale(float scale) {
        this.mAnimatorDurationScaleSetting = scale;
        ValueAnimator.setDurationScale(scale);
    }

    public float getWindowAnimationScaleLocked() {
        return this.mAnimationsDisabled ? 0.0f : this.mWindowAnimationScaleSetting;
    }

    public float getTransitionAnimationScaleLocked() {
        return this.mAnimationsDisabled ? 0.0f : this.mTransitionAnimationScaleSetting;
    }

    public void setLetterBoxInsets(Rect insets, int rotation) {
        DisplayContent defaultDisplayContent = getDefaultDisplayContentLocked();
        if (defaultDisplayContent != null) {
            defaultDisplayContent.setLetterBoxInsets(insets, rotation);
        }
    }

    public float getAnimationScale(int which) {
        switch (which) {
            case 0:
                return this.mWindowAnimationScaleSetting;
            case 1:
                return this.mTransitionAnimationScaleSetting;
            case 2:
                return this.mAnimatorDurationScaleSetting;
            default:
                return 0.0f;
        }
    }

    public float[] getAnimationScales() {
        return new float[]{this.mWindowAnimationScaleSetting, this.mTransitionAnimationScaleSetting, this.mAnimatorDurationScaleSetting};
    }

    public float getCurrentAnimatorScale() {
        float f;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                f = this.mAnimationsDisabled ? 0.0f : this.mAnimatorDurationScaleSetting;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return f;
    }

    void dispatchNewAnimatorScaleLocked(Session session) {
        this.mH.obtainMessage(34, session).sendToTarget();
    }

    public void registerPointerEventListener(PointerEventListener listener) {
        this.mPointerEventDispatcher.registerInputEventListener(listener);
    }

    public void unregisterPointerEventListener(PointerEventListener listener) {
        this.mPointerEventDispatcher.unregisterInputEventListener(listener);
    }

    boolean canDispatchPointerEvents() {
        return this.mPointerEventDispatcher != null;
    }

    public int getLidState() {
        int sw = this.mInputManager.getSwitchState(-1, -256, 0);
        if (sw > 0) {
            return 0;
        }
        if (sw == 0) {
            return 1;
        }
        return -1;
    }

    public void lockDeviceNow() {
        lockNow(null);
    }

    public int getCameraLensCoverState() {
        int sw = this.mInputManager.getSwitchState(-1, -256, 9);
        if (sw > 0) {
            return 1;
        }
        return sw == 0 ? 0 : -1;
    }

    public void switchInputMethod(boolean forwardDirection) {
        InputMethodManagerInternal inputMethodManagerInternal = (InputMethodManagerInternal) LocalServices.getService(InputMethodManagerInternal.class);
        if (inputMethodManagerInternal != null) {
            inputMethodManagerInternal.switchInputMethod(forwardDirection);
        }
    }

    public void shutdown(boolean confirm) {
        ShutdownThread.shutdown(ActivityThread.currentActivityThread().getSystemUiContext(), "userrequested", confirm);
    }

    public void reboot(boolean confirm) {
        ShutdownThread.reboot(ActivityThread.currentActivityThread().getSystemUiContext(), "userrequested", confirm);
    }

    public void rebootSafeMode(boolean confirm) {
        ShutdownThread.rebootSafeMode(ActivityThread.currentActivityThread().getSystemUiContext(), confirm);
    }

    public void setCurrentProfileIds(int[] currentProfileIds) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mCurrentProfileIds = currentProfileIds;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setCurrentUser(int newUserId, int[] currentProfileIds) {
        boolean z = false;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mCurrentUserId = newUserId;
                this.mCurrentProfileIds = currentProfileIds;
                this.mAppTransition.setCurrentUser(newUserId);
                this.mPolicy.setCurrentUserLw(newUserId);
                this.mPolicy.enableKeyguard(true);
                this.mRoot.switchUser();
                this.mWindowPlacerLocked.performSurfacePlacement();
                DisplayContent displayContent = getDefaultDisplayContentLocked();
                TaskStack stack = displayContent.getDockedStackIgnoringVisibility();
                DockedStackDividerController dockedStackDividerController = displayContent.mDividerControllerLocked;
                if (stack != null) {
                    z = stack.hasTaskForUser(newUserId);
                }
                dockedStackDividerController.notifyDockedStackExistsChanged(z);
                if (this.mDisplayReady) {
                    int targetDensity;
                    int forcedDensity = getForcedDisplayDensityForUserLocked(newUserId);
                    if (forcedDensity != 0) {
                        targetDensity = forcedDensity;
                    } else {
                        targetDensity = displayContent.mInitialDisplayDensity;
                    }
                    setForcedDisplayDensityLocked(displayContent, targetDensity);
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    boolean isCurrentProfileLocked(int userId) {
        if (userId == this.mCurrentUserId) {
            return true;
        }
        for (int i : this.mCurrentProfileIds) {
            if (i == userId) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void enableScreenAfterBoot() {
        /*
        r5 = this;
        r1 = r5.mWindowMap;
        monitor-enter(r1);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x002b }
        r0 = r5.mSystemBooted;	 Catch:{ all -> 0x002b }
        if (r0 == 0) goto L_0x000f;
    L_0x000a:
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        return;
    L_0x000f:
        r0 = 1;
        r5.mSystemBooted = r0;	 Catch:{ all -> 0x002b }
        r5.hideBootMessagesLocked();	 Catch:{ all -> 0x002b }
        r0 = r5.mH;	 Catch:{ all -> 0x002b }
        r2 = 30000; // 0x7530 float:4.2039E-41 double:1.4822E-319;
        r4 = 23;
        r0.sendEmptyMessageDelayed(r4, r2);	 Catch:{ all -> 0x002b }
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        r0 = r5.mPolicy;
        r0.systemBooted();
        r5.performEnableScreen();
        return;
    L_0x002b:
        r0 = move-exception;
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.enableScreenAfterBoot():void");
    }

    public void enableScreenIfNeeded() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                enableScreenIfNeededLocked();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    void enableScreenIfNeededLocked() {
        if (!this.mDisplayEnabled) {
            if (this.mSystemBooted || (this.mShowingBootMessages ^ 1) == 0) {
                this.mH.sendEmptyMessage(16);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void performBootTimeout() {
        /*
        r3 = this;
        r1 = r3.mWindowMap;
        monitor-enter(r1);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0023 }
        r0 = r3.mDisplayEnabled;	 Catch:{ all -> 0x0023 }
        if (r0 == 0) goto L_0x000f;
    L_0x000a:
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        return;
    L_0x000f:
        r0 = "WindowManager";
        r2 = "***** BOOT TIMEOUT: forcing display enabled";
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x0023 }
        r0 = 1;
        r3.mForceDisplayEnabled = r0;	 Catch:{ all -> 0x0023 }
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        r3.performEnableScreen();
        return;
    L_0x0023:
        r0 = move-exception;
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.performBootTimeout():void");
    }

    public void onSystemUiStarted() {
        this.mPolicy.onSystemUiStarted();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void performEnableScreen() {
        /*
        r10 = this;
        r9 = 0;
        r5 = r10.mWindowMap;
        monitor-enter(r5);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x00cd }
        r4 = r10.mDisplayEnabled;	 Catch:{ all -> 0x00cd }
        if (r4 == 0) goto L_0x0010;
    L_0x000b:
        monitor-exit(r5);
        resetPriorityAfterLockedSection();
        return;
    L_0x0010:
        r4 = r10.mSystemBooted;	 Catch:{ all -> 0x00cd }
        if (r4 != 0) goto L_0x001f;
    L_0x0014:
        r4 = r10.mShowingBootMessages;	 Catch:{ all -> 0x00cd }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x001f;
    L_0x001a:
        monitor-exit(r5);
        resetPriorityAfterLockedSection();
        return;
    L_0x001f:
        r4 = r10.mShowingBootMessages;	 Catch:{ all -> 0x00cd }
        if (r4 != 0) goto L_0x0032;
    L_0x0023:
        r4 = r10.mPolicy;	 Catch:{ all -> 0x00cd }
        r4 = r4.canDismissBootAnimation();	 Catch:{ all -> 0x00cd }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x0032;
    L_0x002d:
        monitor-exit(r5);
        resetPriorityAfterLockedSection();
        return;
    L_0x0032:
        r4 = r10.mForceDisplayEnabled;	 Catch:{ all -> 0x00cd }
        if (r4 != 0) goto L_0x0045;
    L_0x0036:
        r4 = r10.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x00cd }
        r4 = r4.checkWaitingForWindows();	 Catch:{ all -> 0x00cd }
        if (r4 == 0) goto L_0x0045;
    L_0x0040:
        monitor-exit(r5);
        resetPriorityAfterLockedSection();
        return;
    L_0x0045:
        r4 = r10.mBootAnimationStopped;	 Catch:{ all -> 0x00cd }
        if (r4 != 0) goto L_0x005e;
    L_0x0049:
        r4 = "Stop bootanim";
        r6 = 32;
        r8 = 0;
        android.os.Trace.asyncTraceBegin(r6, r4, r8);	 Catch:{ all -> 0x00cd }
        r4 = "service.bootanim.exit";
        r6 = "1";
        android.os.SystemProperties.set(r4, r6);	 Catch:{ all -> 0x00cd }
        r4 = 1;
        r10.mBootAnimationStopped = r4;	 Catch:{ all -> 0x00cd }
    L_0x005e:
        r4 = r10.mForceDisplayEnabled;	 Catch:{ all -> 0x00cd }
        if (r4 != 0) goto L_0x006f;
    L_0x0062:
        r4 = r10.checkBootAnimationCompleteLocked();	 Catch:{ all -> 0x00cd }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x006f;
    L_0x006a:
        monitor-exit(r5);
        resetPriorityAfterLockedSection();
        return;
    L_0x006f:
        r4 = "SurfaceFlinger";
        r3 = android.os.ServiceManager.getService(r4);	 Catch:{ RemoteException -> 0x00c2 }
        if (r3 == 0) goto L_0x0094;
    L_0x0078:
        r4 = "WindowManager";
        r6 = "******* TELLING SURFACE FLINGER WE ARE BOOTED!";
        android.util.Slog.i(r4, r6);	 Catch:{ RemoteException -> 0x00c2 }
        r0 = android.os.Parcel.obtain();	 Catch:{ RemoteException -> 0x00c2 }
        r4 = "android.ui.ISurfaceComposer";
        r0.writeInterfaceToken(r4);	 Catch:{ RemoteException -> 0x00c2 }
        r4 = 1;
        r6 = 0;
        r7 = 0;
        r3.transact(r4, r0, r6, r7);	 Catch:{ RemoteException -> 0x00c2 }
        r0.recycle();	 Catch:{ RemoteException -> 0x00c2 }
    L_0x0094:
        r6 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x00cd }
        r4 = 31007; // 0x791f float:4.345E-41 double:1.53195E-319;
        android.util.EventLog.writeEvent(r4, r6);	 Catch:{ all -> 0x00cd }
        r4 = "Stop bootanim";
        r6 = 32;
        r8 = 0;
        android.os.Trace.asyncTraceEnd(r6, r4, r8);	 Catch:{ all -> 0x00cd }
        r4 = 1;
        r10.mDisplayEnabled = r4;	 Catch:{ all -> 0x00cd }
        r4 = r10.mInputMonitor;	 Catch:{ all -> 0x00cd }
        r6 = r10.mEventDispatchingEnabled;	 Catch:{ all -> 0x00cd }
        r4.setEventDispatchingLw(r6);	 Catch:{ all -> 0x00cd }
        monitor-exit(r5);
        resetPriorityAfterLockedSection();
        r4 = r10.mActivityManager;	 Catch:{ RemoteException -> 0x00d3 }
        r4.bootAnimationComplete();	 Catch:{ RemoteException -> 0x00d3 }
    L_0x00b9:
        r4 = r10.mPolicy;
        r4.enableScreenAfterBoot();
        r10.updateRotationUnchecked(r9, r9);
        return;
    L_0x00c2:
        r2 = move-exception;
        r4 = "WindowManager";
        r6 = "Boot completed: SurfaceFlinger is dead!";
        android.util.Slog.e(r4, r6);	 Catch:{ all -> 0x00cd }
        goto L_0x0094;
    L_0x00cd:
        r4 = move-exception;
        monitor-exit(r5);
        resetPriorityAfterLockedSection();
        throw r4;
    L_0x00d3:
        r1 = move-exception;
        goto L_0x00b9;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.performEnableScreen():void");
    }

    private boolean checkBootAnimationCompleteLocked() {
        if (!SystemService.isRunning(BOOT_ANIMATION_SERVICE)) {
            return true;
        }
        this.mH.removeMessages(37);
        this.mH.sendEmptyMessageDelayed(37, 200);
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void showBootMessage(java.lang.CharSequence r4, boolean r5) {
        /*
        r3 = this;
        r0 = 0;
        r2 = r3.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0037 }
        r1 = r3.mAllowBootMessages;	 Catch:{ all -> 0x0037 }
        if (r1 != 0) goto L_0x0010;
    L_0x000b:
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0010:
        r1 = r3.mShowingBootMessages;	 Catch:{ all -> 0x0037 }
        if (r1 != 0) goto L_0x001c;
    L_0x0014:
        if (r5 != 0) goto L_0x001b;
    L_0x0016:
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x001b:
        r0 = 1;
    L_0x001c:
        r1 = r3.mSystemBooted;	 Catch:{ all -> 0x0037 }
        if (r1 == 0) goto L_0x0025;
    L_0x0020:
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0025:
        r1 = 1;
        r3.mShowingBootMessages = r1;	 Catch:{ all -> 0x0037 }
        r1 = r3.mPolicy;	 Catch:{ all -> 0x0037 }
        r1.showBootMessage(r4, r5);	 Catch:{ all -> 0x0037 }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        if (r0 == 0) goto L_0x0036;
    L_0x0033:
        r3.performEnableScreen();
    L_0x0036:
        return;
    L_0x0037:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.showBootMessage(java.lang.CharSequence, boolean):void");
    }

    public void hideBootMessagesLocked() {
        if (this.mShowingBootMessages) {
            this.mShowingBootMessages = false;
            this.mPolicy.hideBootMessages();
        }
    }

    public void setInTouchMode(boolean mode) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mInTouchMode = mode;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    private void updateCircularDisplayMaskIfNeeded() {
        if (this.mContext.getResources().getConfiguration().isScreenRound() && this.mContext.getResources().getBoolean(17957075)) {
            int currentUserId;
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    currentUserId = this.mCurrentUserId;
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
            int showMask = Secure.getIntForUser(this.mContext.getContentResolver(), "accessibility_display_inversion_enabled", 0, currentUserId) == 1 ? 0 : 1;
            Message m = this.mH.obtainMessage(35);
            m.arg1 = showMask;
            this.mH.sendMessage(m);
        }
    }

    public void showEmulatorDisplayOverlayIfNeeded() {
        if (this.mContext.getResources().getBoolean(17957071) && SystemProperties.getBoolean(PROPERTY_EMULATOR_CIRCULAR, false) && Build.IS_EMULATOR) {
            this.mH.sendMessage(this.mH.obtainMessage(36));
        }
    }

    public void showCircularMask(boolean visible) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                openSurfaceTransaction();
                if (visible) {
                    if (this.mCircularDisplayMask == null) {
                        this.mCircularDisplayMask = new CircularDisplayMask(getDefaultDisplayContentLocked().getDisplay(), this.mFxSession, (this.mPolicy.getWindowLayerFromTypeLw(2018) * 10000) + 10, this.mContext.getResources().getInteger(17694918), this.mContext.getResources().getDimensionPixelSize(17104947));
                    }
                    this.mCircularDisplayMask.setVisibility(true);
                } else if (this.mCircularDisplayMask != null) {
                    this.mCircularDisplayMask.setVisibility(false);
                    this.mCircularDisplayMask = null;
                }
                closeSurfaceTransaction();
            } catch (Throwable th) {
                resetPriorityAfterLockedSection();
            }
        }
        resetPriorityAfterLockedSection();
    }

    public void showEmulatorDisplayOverlay() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                openSurfaceTransaction();
                if (this.mEmulatorDisplayOverlay == null) {
                    this.mEmulatorDisplayOverlay = new EmulatorDisplayOverlay(this.mContext, getDefaultDisplayContentLocked().getDisplay(), this.mFxSession, (this.mPolicy.getWindowLayerFromTypeLw(2018) * 10000) + 10);
                }
                this.mEmulatorDisplayOverlay.setVisibility(true);
                closeSurfaceTransaction();
            } catch (Throwable th) {
                resetPriorityAfterLockedSection();
            }
        }
        resetPriorityAfterLockedSection();
    }

    public void showStrictModeViolation(boolean on) {
        int pid = Binder.getCallingPid();
        if (on) {
            this.mH.sendMessage(this.mH.obtainMessage(25, 1, pid));
            this.mH.sendMessageDelayed(this.mH.obtainMessage(25, 0, pid), 1000);
            return;
        }
        this.mH.sendMessage(this.mH.obtainMessage(25, 0, pid));
    }

    private void showStrictModeViolation(int arg, int pid) {
        boolean on = arg != 0;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                if (!on || (this.mRoot.canShowStrictModeViolation(pid) ^ 1) == 0) {
                    SurfaceControl.openTransaction();
                    if (this.mStrictModeFlash == null) {
                        this.mStrictModeFlash = new StrictModeFlash(getDefaultDisplayContentLocked().getDisplay(), this.mFxSession);
                    }
                    this.mStrictModeFlash.setVisibility(on);
                    SurfaceControl.closeTransaction();
                    resetPriorityAfterLockedSection();
                    return;
                }
                resetPriorityAfterLockedSection();
            } catch (Throwable th) {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setStrictModeVisualIndicatorPreference(String value) {
        SystemProperties.set("persist.sys.strictmode.visual", value);
    }

    public Bitmap screenshotWallpaper() {
        if (checkCallingPermission("android.permission.READ_FRAME_BUFFER", "screenshotWallpaper()")) {
            try {
                Trace.traceBegin(32, "screenshotWallpaper");
                Bitmap screenshotApplications = screenshotApplications(null, 0, -1, -1, true, 1.0f, Config.ARGB_8888, true, false);
                return screenshotApplications;
            } finally {
                Trace.traceEnd(32);
            }
        } else {
            throw new SecurityException("Requires READ_FRAME_BUFFER permission");
        }
    }

    public boolean requestAssistScreenshot(IAssistScreenshotReceiver receiver) {
        if (checkCallingPermission("android.permission.READ_FRAME_BUFFER", "requestAssistScreenshot()")) {
            FgThread.getHandler().post(new -$Lambda$jlKbn4GPn9-0nFmS_2KB8vTwgFI((byte) 3, this, receiver));
            return true;
        }
        throw new SecurityException("Requires READ_FRAME_BUFFER permission");
    }

    /* synthetic */ void lambda$-com_android_server_wm_WindowManagerService_164182(IAssistScreenshotReceiver receiver) {
        try {
            receiver.send(screenshotApplications(null, 0, -1, -1, true, 1.0f, Config.ARGB_8888, false, false));
        } catch (RemoteException e) {
        }
    }

    public TaskSnapshot getTaskSnapshot(int taskId, int userId, boolean reducedResolution) {
        return this.mTaskSnapshotController.getSnapshot(taskId, userId, true, reducedResolution);
    }

    public void removeObsoleteTaskFiles(ArraySet<Integer> persistentTaskIds, int[] runningUserIds) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mTaskSnapshotController.removeObsoleteTaskFiles(persistentTaskIds, runningUserIds);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Bitmap screenshotApplications(android.os.IBinder r10, int r11, int r12, int r13, boolean r14, float r15, android.graphics.Bitmap.Config r16, boolean r17, boolean r18) {
        /*
        r9 = this;
        r2 = r9.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0028 }
        r1 = r9.mRoot;	 Catch:{ all -> 0x0028 }
        r0 = r1.getDisplayContentOrCreate(r11);	 Catch:{ all -> 0x0028 }
        if (r0 != 0) goto L_0x0014;
    L_0x000e:
        r1 = 0;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return r1;
    L_0x0014:
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        r1 = r10;
        r2 = r12;
        r3 = r13;
        r4 = r14;
        r5 = r15;
        r6 = r16;
        r7 = r17;
        r8 = r18;
        r1 = r0.screenshotApplications(r1, r2, r3, r4, r5, r6, r7, r8);
        return r1;
    L_0x0028:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.screenshotApplications(android.os.IBinder, int, int, int, boolean, float, android.graphics.Bitmap$Config, boolean, boolean):android.graphics.Bitmap");
    }

    public void freezeRotation(int rotation) {
        if (!checkCallingPermission("android.permission.SET_ORIENTATION", "freezeRotation()")) {
            throw new SecurityException("Requires SET_ORIENTATION permission");
        } else if (rotation < -1 || rotation > 3) {
            throw new IllegalArgumentException("Rotation argument must be -1 or a valid rotation constant.");
        } else {
            int defaultDisplayRotation = getDefaultDisplayRotation();
            long origId = Binder.clearCallingIdentity();
            try {
                WindowManagerPolicy windowManagerPolicy = this.mPolicy;
                if (rotation != -1) {
                    defaultDisplayRotation = rotation;
                }
                windowManagerPolicy.setUserRotationMode(1, defaultDisplayRotation);
                updateRotationUnchecked(false, false);
            } finally {
                Binder.restoreCallingIdentity(origId);
            }
        }
    }

    public void thawRotation() {
        if (checkCallingPermission("android.permission.SET_ORIENTATION", "thawRotation()")) {
            long origId = Binder.clearCallingIdentity();
            try {
                this.mPolicy.setUserRotationMode(0, 777);
                updateRotationUnchecked(false, false);
            } finally {
                Binder.restoreCallingIdentity(origId);
            }
        } else {
            throw new SecurityException("Requires SET_ORIENTATION permission");
        }
    }

    public void updateRotation(boolean alwaysSendConfiguration, boolean forceRelayout) {
        updateRotationUnchecked(alwaysSendConfiguration, forceRelayout);
    }

    void pauseRotationLocked() {
        this.mDeferredRotationPauseCount++;
    }

    void resumeRotationLocked() {
        if (this.mDeferredRotationPauseCount > 0) {
            this.mDeferredRotationPauseCount--;
            if (this.mDeferredRotationPauseCount == 0) {
                DisplayContent displayContent = getDefaultDisplayContentLocked();
                if (displayContent.updateRotationUnchecked(false)) {
                    this.mH.obtainMessage(18, Integer.valueOf(displayContent.getDisplayId())).sendToTarget();
                }
            }
        }
    }

    private void updateRotationUnchecked(boolean alwaysSendConfiguration, boolean forceRelayout) {
        Trace.traceBegin(32, "updateRotation");
        long origId = Binder.clearCallingIdentity();
        try {
            boolean rotationChanged;
            int displayId;
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                DisplayContent displayContent = getDefaultDisplayContentLocked();
                Trace.traceBegin(32, "updateRotation: display");
                rotationChanged = displayContent.updateRotationUnchecked(false);
                Trace.traceEnd(32);
                if (!rotationChanged || forceRelayout) {
                    displayContent.setLayoutNeeded();
                    Trace.traceBegin(32, "updateRotation: performSurfacePlacement");
                    this.mWindowPlacerLocked.performSurfacePlacement();
                    Trace.traceEnd(32);
                }
                displayId = displayContent.getDisplayId();
            }
            resetPriorityAfterLockedSection();
            if (rotationChanged || alwaysSendConfiguration) {
                Trace.traceBegin(32, "updateRotation: sendNewConfiguration");
                sendNewConfiguration(displayId);
                Trace.traceEnd(32);
            }
            Binder.restoreCallingIdentity(origId);
            Trace.traceEnd(32);
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(origId);
            Trace.traceEnd(32);
        }
    }

    public int getDefaultDisplayRotation() {
        int rotation;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                rotation = getDefaultDisplayContentLocked().getRotation();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return rotation;
    }

    public boolean isRotationFrozen() {
        return this.mPolicy.getUserRotationMode() == 1;
    }

    public int watchRotation(IRotationWatcher watcher, int displayId) {
        int defaultDisplayRotation;
        final IBinder watcherBinder = watcher.asBinder();
        DeathRecipient dr = new DeathRecipient() {
            public void binderDied() {
                synchronized (WindowManagerService.this.mWindowMap) {
                    try {
                        WindowManagerService.boostPriorityForLockedSection();
                        int i = 0;
                        while (i < WindowManagerService.this.mRotationWatchers.size()) {
                            if (watcherBinder == ((RotationWatcher) WindowManagerService.this.mRotationWatchers.get(i)).mWatcher.asBinder()) {
                                IBinder binder = ((RotationWatcher) WindowManagerService.this.mRotationWatchers.remove(i)).mWatcher.asBinder();
                                if (binder != null) {
                                    binder.unlinkToDeath(this, 0);
                                }
                                i--;
                            }
                            i++;
                        }
                    } finally {
                        WindowManagerService.resetPriorityAfterLockedSection();
                    }
                }
            }
        };
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                try {
                    watcher.asBinder().linkToDeath(dr, 0);
                    this.mRotationWatchers.add(new RotationWatcher(watcher, dr, displayId));
                } catch (RemoteException e) {
                }
                defaultDisplayRotation = getDefaultDisplayRotation();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return defaultDisplayRotation;
    }

    public void removeRotationWatcher(IRotationWatcher watcher) {
        IBinder watcherBinder = watcher.asBinder();
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                int i = 0;
                while (i < this.mRotationWatchers.size()) {
                    if (watcherBinder == ((RotationWatcher) this.mRotationWatchers.get(i)).mWatcher.asBinder()) {
                        RotationWatcher removed = (RotationWatcher) this.mRotationWatchers.remove(i);
                        IBinder binder = removed.mWatcher.asBinder();
                        if (binder != null) {
                            binder.unlinkToDeath(removed.mDeathRecipient, 0);
                        }
                        i--;
                    }
                    i++;
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public boolean registerWallpaperVisibilityListener(IWallpaperVisibilityListener listener, int displayId) {
        boolean isWallpaperVisible;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                if (displayContent == null) {
                    throw new IllegalArgumentException("Trying to register visibility event for invalid display: " + displayId);
                }
                this.mWallpaperVisibilityListeners.registerWallpaperVisibilityListener(listener, displayId);
                isWallpaperVisible = displayContent.mWallpaperController.isWallpaperVisible();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return isWallpaperVisible;
    }

    public void unregisterWallpaperVisibilityListener(IWallpaperVisibilityListener listener, int displayId) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mWallpaperVisibilityListeners.unregisterWallpaperVisibilityListener(listener, displayId);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getPreferredOptionsPanelGravity() {
        /*
        r8 = this;
        r7 = 8388691; // 0x800053 float:1.175506E-38 double:4.144564E-317;
        r6 = 85;
        r5 = 81;
        r3 = r8.mWindowMap;
        monitor-enter(r3);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0049 }
        r0 = r8.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x0049 }
        r1 = r0.getRotation();	 Catch:{ all -> 0x0049 }
        r2 = r0.mInitialDisplayWidth;	 Catch:{ all -> 0x0049 }
        r4 = r0.mInitialDisplayHeight;	 Catch:{ all -> 0x0049 }
        if (r2 >= r4) goto L_0x0032;
    L_0x001b:
        switch(r1) {
            case 0: goto L_0x001e;
            case 1: goto L_0x0023;
            case 2: goto L_0x0028;
            case 3: goto L_0x002d;
            default: goto L_0x001e;
        };
    L_0x001e:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0023:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r6;
    L_0x0028:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x002d:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r7;
    L_0x0032:
        switch(r1) {
            case 0: goto L_0x0035;
            case 1: goto L_0x003a;
            case 2: goto L_0x003f;
            case 3: goto L_0x0044;
            default: goto L_0x0035;
        };
    L_0x0035:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r6;
    L_0x003a:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x003f:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r7;
    L_0x0044:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0049:
        r2 = move-exception;
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.getPreferredOptionsPanelGravity():int");
    }

    public boolean startViewServer(int port) {
        if (isSystemSecure() || !checkCallingPermission("android.permission.DUMP", "startViewServer") || port < 1024) {
            return false;
        }
        if (this.mViewServer != null) {
            if (!this.mViewServer.isRunning()) {
                try {
                    return this.mViewServer.start();
                } catch (IOException e) {
                    Slog.w(TAG, "View server did not start");
                }
            }
            return false;
        }
        try {
            this.mViewServer = new ViewServer(this, port);
            return this.mViewServer.start();
        } catch (IOException e2) {
            Slog.w(TAG, "View server did not start");
            return false;
        }
    }

    private boolean isSystemSecure() {
        if ("1".equals(SystemProperties.get(SYSTEM_SECURE, "1"))) {
            return "0".equals(SystemProperties.get(SYSTEM_DEBUGGABLE, "0"));
        }
        return false;
    }

    public boolean stopViewServer() {
        if (isSystemSecure() || !checkCallingPermission("android.permission.DUMP", "stopViewServer") || this.mViewServer == null) {
            return false;
        }
        return this.mViewServer.stop();
    }

    public boolean isViewServerRunning() {
        boolean z = false;
        if (isSystemSecure() || !checkCallingPermission("android.permission.DUMP", "isViewServerRunning")) {
            return false;
        }
        if (this.mViewServer != null) {
            z = this.mViewServer.isRunning();
        }
        return z;
    }

    boolean viewServerListWindows(Socket client) {
        Throwable th;
        if (isSystemSecure()) {
            return false;
        }
        boolean result = true;
        ArrayList<WindowState> windows = new ArrayList();
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mRoot.forAllWindows((Consumer) new -$Lambda$YIZfR4m-B8z_tYbP2x4OJ3o7OYE((byte) 19, windows), false);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        BufferedWriter bufferedWriter = null;
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()), 8192);
            try {
                int count = windows.size();
                for (int i = 0; i < count; i++) {
                    WindowState w = (WindowState) windows.get(i);
                    out.write(Integer.toHexString(System.identityHashCode(w)));
                    out.write(32);
                    out.append(w.mAttrs.getTitle());
                    out.write(10);
                }
                out.write("DONE.\n");
                out.flush();
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        result = false;
                    }
                }
                bufferedWriter = out;
            } catch (Exception e2) {
                bufferedWriter = out;
                result = false;
                if (bufferedWriter != null) {
                    try {
                        bufferedWriter.close();
                    } catch (IOException e3) {
                        result = false;
                    }
                }
                return result;
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = out;
                if (bufferedWriter != null) {
                    try {
                        bufferedWriter.close();
                    } catch (IOException e4) {
                    }
                }
                throw th;
            }
        } catch (Exception e5) {
            result = false;
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            return result;
        } catch (Throwable th3) {
            th = th3;
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            throw th;
        }
        return result;
    }

    boolean viewServerGetFocusedWindow(Socket client) {
        Throwable th;
        if (isSystemSecure()) {
            return false;
        }
        boolean result = true;
        WindowState focusedWindow = getFocusedWindow();
        BufferedWriter bufferedWriter = null;
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()), 8192);
            if (focusedWindow != null) {
                try {
                    out.write(Integer.toHexString(System.identityHashCode(focusedWindow)));
                    out.write(32);
                    out.append(focusedWindow.mAttrs.getTitle());
                } catch (Exception e) {
                    bufferedWriter = out;
                    result = false;
                    if (bufferedWriter != null) {
                        try {
                            bufferedWriter.close();
                        } catch (IOException e2) {
                            result = false;
                        }
                    }
                    return result;
                } catch (Throwable th2) {
                    th = th2;
                    bufferedWriter = out;
                    if (bufferedWriter != null) {
                        try {
                            bufferedWriter.close();
                        } catch (IOException e3) {
                        }
                    }
                    throw th;
                }
            }
            out.write(10);
            out.flush();
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e4) {
                    result = false;
                }
            }
            bufferedWriter = out;
        } catch (Exception e5) {
            result = false;
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            return result;
        } catch (Throwable th3) {
            th = th3;
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            throw th;
        }
        return result;
    }

    boolean viewServerWindowCommand(Socket client, String command, String parameters) {
        Exception e;
        Throwable th;
        if (isSystemSecure()) {
            return false;
        }
        boolean success = true;
        Parcel parcel = null;
        Parcel parcel2 = null;
        BufferedWriter bufferedWriter = null;
        try {
            int index = parameters.indexOf(32);
            if (index == -1) {
                index = parameters.length();
            }
            int hashCode = (int) Long.parseLong(parameters.substring(0, index), 16);
            if (index < parameters.length()) {
                parameters = parameters.substring(index + 1);
            } else {
                parameters = "";
            }
            WindowState window = findWindow(hashCode);
            if (window == null) {
                return false;
            }
            parcel = Parcel.obtain();
            parcel.writeInterfaceToken("android.view.IWindow");
            parcel.writeString(command);
            parcel.writeString(parameters);
            parcel.writeInt(1);
            ParcelFileDescriptor.fromSocket(client).writeToParcel(parcel, 0);
            parcel2 = Parcel.obtain();
            window.mClient.asBinder().transact(1, parcel, parcel2, 0);
            parcel2.readException();
            if (!client.isOutputShutdown()) {
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
                try {
                    out.write("DONE\n");
                    out.flush();
                    bufferedWriter = out;
                } catch (Exception e2) {
                    e = e2;
                    bufferedWriter = out;
                    try {
                        Slog.w(TAG, "Could not send command " + command + " with parameters " + parameters, e);
                        success = false;
                        if (parcel != null) {
                            parcel.recycle();
                        }
                        if (parcel2 != null) {
                            parcel2.recycle();
                        }
                        if (bufferedWriter != null) {
                            try {
                                bufferedWriter.close();
                            } catch (IOException e3) {
                            }
                        }
                        return success;
                    } catch (Throwable th2) {
                        th = th2;
                        if (parcel != null) {
                            parcel.recycle();
                        }
                        if (parcel2 != null) {
                            parcel2.recycle();
                        }
                        if (bufferedWriter != null) {
                            try {
                                bufferedWriter.close();
                            } catch (IOException e4) {
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    bufferedWriter = out;
                    if (parcel != null) {
                        parcel.recycle();
                    }
                    if (parcel2 != null) {
                        parcel2.recycle();
                    }
                    if (bufferedWriter != null) {
                        bufferedWriter.close();
                    }
                    throw th;
                }
            }
            if (parcel != null) {
                parcel.recycle();
            }
            if (parcel2 != null) {
                parcel2.recycle();
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException e5) {
                }
            }
            return success;
        } catch (Exception e6) {
            e = e6;
            Slog.w(TAG, "Could not send command " + command + " with parameters " + parameters, e);
            success = false;
            if (parcel != null) {
                parcel.recycle();
            }
            if (parcel2 != null) {
                parcel2.recycle();
            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            return success;
        }
    }

    public void addWindowChangeListener(WindowChangeListener listener) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mWindowChangeListeners.add(listener);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void removeWindowChangeListener(WindowChangeListener listener) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mWindowChangeListeners.remove(listener);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void notifyWindowsChanged() {
        /*
        r5 = this;
        r4 = r5.mWindowMap;
        monitor-enter(r4);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0033 }
        r3 = r5.mWindowChangeListeners;	 Catch:{ all -> 0x0033 }
        r3 = r3.isEmpty();	 Catch:{ all -> 0x0033 }
        if (r3 == 0) goto L_0x0013;
    L_0x000e:
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        return;
    L_0x0013:
        r3 = r5.mWindowChangeListeners;	 Catch:{ all -> 0x0033 }
        r3 = r3.size();	 Catch:{ all -> 0x0033 }
        r2 = new com.android.server.wm.WindowManagerService.WindowChangeListener[r3];	 Catch:{ all -> 0x0033 }
        r3 = r5.mWindowChangeListeners;	 Catch:{ all -> 0x0033 }
        r2 = r3.toArray(r2);	 Catch:{ all -> 0x0033 }
        r2 = (com.android.server.wm.WindowManagerService.WindowChangeListener[]) r2;	 Catch:{ all -> 0x0033 }
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        r0 = r2.length;
        r1 = 0;
    L_0x0029:
        if (r1 >= r0) goto L_0x0039;
    L_0x002b:
        r3 = r2[r1];
        r3.windowsChanged();
        r1 = r1 + 1;
        goto L_0x0029;
    L_0x0033:
        r3 = move-exception;
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        throw r3;
    L_0x0039:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.notifyWindowsChanged():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void notifyFocusChanged() {
        /*
        r5 = this;
        r4 = r5.mWindowMap;
        monitor-enter(r4);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0033 }
        r3 = r5.mWindowChangeListeners;	 Catch:{ all -> 0x0033 }
        r3 = r3.isEmpty();	 Catch:{ all -> 0x0033 }
        if (r3 == 0) goto L_0x0013;
    L_0x000e:
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        return;
    L_0x0013:
        r3 = r5.mWindowChangeListeners;	 Catch:{ all -> 0x0033 }
        r3 = r3.size();	 Catch:{ all -> 0x0033 }
        r2 = new com.android.server.wm.WindowManagerService.WindowChangeListener[r3];	 Catch:{ all -> 0x0033 }
        r3 = r5.mWindowChangeListeners;	 Catch:{ all -> 0x0033 }
        r2 = r3.toArray(r2);	 Catch:{ all -> 0x0033 }
        r2 = (com.android.server.wm.WindowManagerService.WindowChangeListener[]) r2;	 Catch:{ all -> 0x0033 }
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        r0 = r2.length;
        r1 = 0;
    L_0x0029:
        if (r1 >= r0) goto L_0x0039;
    L_0x002b:
        r3 = r2[r1];
        r3.focusChanged();
        r1 = r1 + 1;
        goto L_0x0029;
    L_0x0033:
        r3 = move-exception;
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        throw r3;
    L_0x0039:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.notifyFocusChanged():void");
    }

    private WindowState findWindow(int hashCode) {
        if (hashCode == -1) {
            return getFocusedWindow();
        }
        WindowState window;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                window = this.mRoot.getWindow(new -$Lambda$tS7nL17Ous75692M4rHLEZu640I((byte) 3, hashCode));
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return window;
    }

    static /* synthetic */ boolean lambda$-com_android_server_wm_WindowManagerService_189567(int hashCode, WindowState w) {
        return System.identityHashCode(w) == hashCode;
    }

    void sendNewConfiguration(int displayId) {
        try {
            if (!this.mActivityManager.updateDisplayOverrideConfiguration(null, displayId)) {
                synchronized (this.mWindowMap) {
                    boostPriorityForLockedSection();
                    if (this.mWaitingForConfig) {
                        this.mWaitingForConfig = false;
                        this.mLastFinishedFreezeSource = "config-unchanged";
                        DisplayContent dc = this.mRoot.getDisplayContent(displayId);
                        if (dc != null) {
                            dc.setLayoutNeeded();
                        }
                        this.mWindowPlacerLocked.performSurfacePlacement();
                    }
                }
                resetPriorityAfterLockedSection();
            }
        } catch (RemoteException e) {
        } catch (Throwable th) {
            resetPriorityAfterLockedSection();
        }
    }

    public Configuration computeNewConfiguration(int displayId) {
        Configuration computeNewConfigurationLocked;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                computeNewConfigurationLocked = computeNewConfigurationLocked(displayId);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return computeNewConfigurationLocked;
    }

    private Configuration computeNewConfigurationLocked(int displayId) {
        if (!this.mDisplayReady) {
            return null;
        }
        Configuration config = new Configuration();
        this.mRoot.getDisplayContent(displayId).computeScreenConfiguration(config);
        return config;
    }

    void notifyHardKeyboardStatusChange() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                OnHardKeyboardStatusChangeListener listener = this.mHardKeyboardStatusChangeListener;
                boolean available = this.mHardKeyboardAvailable;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        if (listener != null) {
            listener.onHardKeyboardStatusChange(available);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    boolean startMovingTask(android.view.IWindow r10, float r11, float r12) {
        /*
        r9 = this;
        r8 = 0;
        r1 = 0;
        r7 = r9.mWindowMap;
        monitor-enter(r7);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x002f }
        r0 = 0;
        r2 = 0;
        r1 = r9.windowForClientLocked(r0, r10, r2);	 Catch:{ all -> 0x002f }
        r2 = 0;
        r3 = 0;
        r0 = r9;
        r4 = r11;
        r5 = r12;
        r0 = r0.startPositioningLocked(r1, r2, r3, r4, r5);	 Catch:{ all -> 0x002f }
        if (r0 != 0) goto L_0x001e;
    L_0x0019:
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        return r8;
    L_0x001e:
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        r0 = r9.mActivityManager;	 Catch:{ RemoteException -> 0x0035 }
        r2 = r1.getTask();	 Catch:{ RemoteException -> 0x0035 }
        r2 = r2.mTaskId;	 Catch:{ RemoteException -> 0x0035 }
        r0.setFocusedTask(r2);	 Catch:{ RemoteException -> 0x0035 }
    L_0x002d:
        r0 = 1;
        return r0;
    L_0x002f:
        r0 = move-exception;
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        throw r0;
    L_0x0035:
        r6 = move-exception;
        goto L_0x002d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.startMovingTask(android.view.IWindow, float, float):boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handleTapOutsideTask(com.android.server.wm.DisplayContent r11, int r12, int r13) {
        /*
        r10 = this;
        r8 = -1;
        r9 = r10.mWindowMap;
        monitor-enter(r9);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0037 }
        r7 = r11.findTaskForResizePoint(r12, r13);	 Catch:{ all -> 0x0037 }
        if (r7 == 0) goto L_0x0032;
    L_0x000d:
        r1 = r7.getTopVisibleAppMainWindow();	 Catch:{ all -> 0x0037 }
        r3 = r7.preserveOrientationOnResize();	 Catch:{ all -> 0x0037 }
        r4 = (float) r12;	 Catch:{ all -> 0x0037 }
        r5 = (float) r13;	 Catch:{ all -> 0x0037 }
        r2 = 1;
        r0 = r10;
        r0 = r0.startPositioningLocked(r1, r2, r3, r4, r5);	 Catch:{ all -> 0x0037 }
        if (r0 != 0) goto L_0x0024;
    L_0x001f:
        monitor-exit(r9);
        resetPriorityAfterLockedSection();
        return;
    L_0x0024:
        r8 = r7.mTaskId;	 Catch:{ all -> 0x0037 }
    L_0x0026:
        monitor-exit(r9);
        resetPriorityAfterLockedSection();
        if (r8 < 0) goto L_0x0031;
    L_0x002c:
        r0 = r10.mActivityManager;	 Catch:{ RemoteException -> 0x003d }
        r0.setFocusedTask(r8);	 Catch:{ RemoteException -> 0x003d }
    L_0x0031:
        return;
    L_0x0032:
        r8 = r11.taskIdFromPoint(r12, r13);	 Catch:{ all -> 0x0037 }
        goto L_0x0026;
    L_0x0037:
        r0 = move-exception;
        monitor-exit(r9);
        resetPriorityAfterLockedSection();
        throw r0;
    L_0x003d:
        r6 = move-exception;
        goto L_0x0031;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.handleTapOutsideTask(com.android.server.wm.DisplayContent, int, int):void");
    }

    private boolean startPositioningLocked(WindowState win, boolean resize, boolean preserveOrientation, float startX, float startY) {
        if (win == null || win.getAppToken() == null) {
            Slog.w(TAG, "startPositioningLocked: Bad window " + win);
            return false;
        } else if (win.mInputChannel == null) {
            Slog.wtf(TAG, "startPositioningLocked: " + win + " has no input channel, " + " probably being removed");
            return false;
        } else {
            DisplayContent displayContent = win.getDisplayContent();
            if (displayContent == null) {
                Slog.w(TAG, "startPositioningLocked: Invalid display content " + win);
                return false;
            }
            Display display = displayContent.getDisplay();
            this.mTaskPositioner = new TaskPositioner(this);
            this.mTaskPositioner.register(display);
            this.mInputMonitor.updateInputWindowsLw(true);
            WindowState transferFocusFromWin = win;
            if (!(this.mCurrentFocus == null || this.mCurrentFocus == win || this.mCurrentFocus.mAppToken != win.mAppToken)) {
                transferFocusFromWin = this.mCurrentFocus;
            }
            if (this.mInputManager.transferTouchFocus(transferFocusFromWin.mInputChannel, this.mTaskPositioner.mServerChannel)) {
                this.mTaskPositioner.startDrag(win, resize, preserveOrientation, startX, startY);
                return true;
            }
            Slog.e(TAG, "startPositioningLocked: Unable to transfer touch focus");
            this.mTaskPositioner.unregister();
            this.mTaskPositioner = null;
            this.mInputMonitor.updateInputWindowsLw(true);
            return false;
        }
    }

    private void finishPositioning() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                if (this.mTaskPositioner != null) {
                    this.mTaskPositioner.unregister();
                    this.mTaskPositioner = null;
                    this.mInputMonitor.updateInputWindowsLw(true);
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    IBinder prepareDragSurface(IWindow window, SurfaceSession session, int flags, int width, int height, Surface outSurface) {
        OutOfResourcesException e;
        Throwable th;
        int callerPid = Binder.getCallingPid();
        int callerUid = Binder.getCallingUid();
        long origId = Binder.clearCallingIdentity();
        IBinder iBinder = null;
        IBinder token;
        try {
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    try {
                        if (this.mDragState == null) {
                            Display display = getDefaultDisplayContentLocked().getDisplay();
                            SurfaceControl surface = new SurfaceControl(session, "drag surface", width, height, -3, 4);
                            surface.setLayerStack(display.getLayerStack());
                            float alpha = 1.0f;
                            if ((flags & 512) == 0) {
                                alpha = DRAG_SHADOW_ALPHA_TRANSPARENT;
                            }
                            surface.setAlpha(alpha);
                            outSurface.copyFrom(surface);
                            IBinder winBinder = window.asBinder();
                            token = new Binder();
                            try {
                                this.mDragState = new DragState(this, token, surface, flags, winBinder);
                                this.mDragState.mPid = callerPid;
                                this.mDragState.mUid = callerUid;
                                this.mDragState.mOriginalAlpha = alpha;
                                iBinder = new Binder();
                                this.mDragState.mToken = iBinder;
                                this.mH.removeMessages(20, winBinder);
                                this.mH.sendMessageDelayed(this.mH.obtainMessage(20, winBinder), 5000);
                                token = iBinder;
                            } catch (OutOfResourcesException e2) {
                                e = e2;
                                try {
                                    Slog.e(TAG, "Can't allocate drag surface w=" + width + " h=" + height, e);
                                    if (this.mDragState != null) {
                                        this.mDragState.reset();
                                        this.mDragState = null;
                                    }
                                    resetPriorityAfterLockedSection();
                                    Binder.restoreCallingIdentity(origId);
                                    return token;
                                } catch (Throwable th2) {
                                    th = th2;
                                    resetPriorityAfterLockedSection();
                                    throw th;
                                }
                            }
                            try {
                                resetPriorityAfterLockedSection();
                                Binder.restoreCallingIdentity(origId);
                                return token;
                            } catch (Throwable th3) {
                                th = th3;
                                Binder.restoreCallingIdentity(origId);
                                throw th;
                            }
                        }
                        Slog.w(TAG, "Drag already in progress");
                        token = null;
                        resetPriorityAfterLockedSection();
                        Binder.restoreCallingIdentity(origId);
                        return token;
                    } catch (OutOfResourcesException e3) {
                        e = e3;
                        token = iBinder;
                        Slog.e(TAG, "Can't allocate drag surface w=" + width + " h=" + height, e);
                        if (this.mDragState != null) {
                            this.mDragState.reset();
                            this.mDragState = null;
                        }
                        resetPriorityAfterLockedSection();
                        Binder.restoreCallingIdentity(origId);
                        return token;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    token = null;
                    resetPriorityAfterLockedSection();
                    throw th;
                }
            }
        } catch (Throwable th5) {
            th = th5;
            token = null;
            Binder.restoreCallingIdentity(origId);
            throw th;
        }
    }

    public void setEventDispatching(boolean enabled) {
        if (checkCallingPermission("android.permission.MANAGE_APP_TOKENS", "setEventDispatching()")) {
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    this.mEventDispatchingEnabled = enabled;
                    if (this.mDisplayEnabled) {
                        this.mInputMonitor.setEventDispatchingLw(enabled);
                    }
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
            return;
        }
        throw new SecurityException("Requires MANAGE_APP_TOKENS permission");
    }

    private WindowState getFocusedWindow() {
        WindowState focusedWindowLocked;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                focusedWindowLocked = getFocusedWindowLocked();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return focusedWindowLocked;
    }

    private WindowState getFocusedWindowLocked() {
        return this.mCurrentFocus;
    }

    TaskStack getImeFocusStackLocked() {
        if (this.mFocusedApp == null || this.mFocusedApp.getTask() == null) {
            return null;
        }
        return this.mFocusedApp.getTask().mStack;
    }

    public boolean detectSafeMode() {
        boolean z = true;
        if (!this.mInputMonitor.waitForInputDevicesReady(1000)) {
            Slog.w(TAG, "Devices still not ready after waiting 1000 milliseconds before attempting to detect safe mode.");
        }
        if (Global.getInt(this.mContext.getContentResolver(), "safe_boot_disallowed", 0) != 0) {
            return false;
        }
        int menuState = this.mInputManager.getKeyCodeState(-1, -256, 82);
        int sState = this.mInputManager.getKeyCodeState(-1, -256, 47);
        int dpadState = this.mInputManager.getKeyCodeState(-1, UsbTerminalTypes.TERMINAL_IN_MIC, 23);
        int trackballState = this.mInputManager.getScanCodeState(-1, 65540, 272);
        int volumeDownState = this.mInputManager.getKeyCodeState(-1, -256, 25);
        if (menuState <= 0 && sState <= 0 && dpadState <= 0 && trackballState <= 0 && volumeDownState <= 0) {
            z = false;
        }
        this.mSafeMode = z;
        try {
            if (!(SystemProperties.getInt(ShutdownThread.REBOOT_SAFEMODE_PROPERTY, 0) == 0 && SystemProperties.getInt(ShutdownThread.RO_SAFEMODE_PROPERTY, 0) == 0)) {
                this.mSafeMode = true;
                SystemProperties.set(ShutdownThread.REBOOT_SAFEMODE_PROPERTY, "");
            }
        } catch (IllegalArgumentException e) {
        }
        if (this.mSafeMode) {
            Log.i(TAG, "SAFE MODE ENABLED (menu=" + menuState + " s=" + sState + " dpad=" + dpadState + " trackball=" + trackballState + ")");
            SystemProperties.set(ShutdownThread.RO_SAFEMODE_PROPERTY, "1");
        } else {
            Log.i(TAG, "SAFE MODE not enabled");
        }
        this.mPolicy.setSafeMode(this.mSafeMode);
        return this.mSafeMode;
    }

    public void displayReady() {
        for (Display display : this.mDisplays) {
            displayReady(display.getDisplayId());
        }
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                DisplayContent displayContent = getDefaultDisplayContentLocked();
                if (this.mMaxUiWidth > 0) {
                    displayContent.setMaxUiWidth(this.mMaxUiWidth);
                }
                readForcedDisplayPropertiesLocked(displayContent);
                this.mDisplayReady = true;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        try {
            this.mActivityManager.updateConfiguration(null);
        } catch (RemoteException e) {
        }
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mIsTouchDevice = this.mContext.getPackageManager().hasSystemFeature("android.hardware.touchscreen");
                configureDisplayPolicyLocked(getDefaultDisplayContentLocked());
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        try {
            this.mActivityManager.updateConfiguration(null);
        } catch (RemoteException e2) {
        }
        updateCircularDisplayMaskIfNeeded();
    }

    private void displayReady(int displayId) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                if (displayContent != null) {
                    this.mAnimator.addDisplayLocked(displayId);
                    displayContent.initializeDisplayBaseInfo();
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void systemReady() {
        this.mPolicy.systemReady();
        this.mTaskSnapshotController.systemReady();
        this.mHasWideColorGamutSupport = queryWideColorGamutSupport();
    }

    private static boolean queryWideColorGamutSupport() {
        try {
            OptionalBool hasWideColor = ISurfaceFlingerConfigs.getService().hasWideColorDisplay();
            if (hasWideColor != null) {
                return hasWideColor.value;
            }
        } catch (RemoteException e) {
        }
        return false;
    }

    void destroyPreservedSurfaceLocked() {
        for (int i = this.mDestroyPreservedSurface.size() - 1; i >= 0; i--) {
            ((WindowState) this.mDestroyPreservedSurface.get(i)).mWinAnimator.destroyPreservedSurfaceLocked();
        }
        this.mDestroyPreservedSurface.clear();
    }

    void stopUsingSavedSurfaceLocked() {
        for (int i = this.mFinishedEarlyAnim.size() - 1; i >= 0; i--) {
            ((AppWindowToken) this.mFinishedEarlyAnim.get(i)).stopUsingSavedSurfaceLocked();
        }
        this.mFinishedEarlyAnim.clear();
    }

    public IWindowSession openSession(IWindowSessionCallback callback, IInputMethodClient client, IInputContext inputContext) {
        if (client == null) {
            throw new IllegalArgumentException("null client");
        } else if (inputContext != null) {
            return new Session(this, callback, client, inputContext);
        } else {
            throw new IllegalArgumentException("null inputContext");
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean inputMethodClientHasFocus(com.android.internal.view.IInputMethodClient r5) {
        /*
        r4 = this;
        r3 = 1;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x003d }
        r0 = r4.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x003d }
        r0 = r0.inputMethodClientHasFocus(r5);	 Catch:{ all -> 0x003d }
        if (r0 == 0) goto L_0x0016;
    L_0x0011:
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        return r3;
    L_0x0016:
        r0 = r4.mCurrentFocus;	 Catch:{ all -> 0x003d }
        if (r0 == 0) goto L_0x0037;
    L_0x001a:
        r0 = r4.mCurrentFocus;	 Catch:{ all -> 0x003d }
        r0 = r0.mSession;	 Catch:{ all -> 0x003d }
        r0 = r0.mClient;	 Catch:{ all -> 0x003d }
        if (r0 == 0) goto L_0x0037;
    L_0x0022:
        r0 = r4.mCurrentFocus;	 Catch:{ all -> 0x003d }
        r0 = r0.mSession;	 Catch:{ all -> 0x003d }
        r0 = r0.mClient;	 Catch:{ all -> 0x003d }
        r0 = r0.asBinder();	 Catch:{ all -> 0x003d }
        r2 = r5.asBinder();	 Catch:{ all -> 0x003d }
        if (r0 != r2) goto L_0x0037;
    L_0x0032:
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        return r3;
    L_0x0037:
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        r0 = 0;
        return r0;
    L_0x003d:
        r0 = move-exception;
        monitor-exit(r1);
        resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.inputMethodClientHasFocus(com.android.internal.view.IInputMethodClient):boolean");
    }

    public void getInitialDisplaySize(int displayId, Point size) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                if (displayContent != null && displayContent.hasAccess(Binder.getCallingUid())) {
                    size.x = displayContent.mInitialDisplayWidth;
                    size.y = displayContent.mInitialDisplayHeight;
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void getBaseDisplaySize(int displayId, Point size) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                if (displayContent != null && displayContent.hasAccess(Binder.getCallingUid())) {
                    size.x = displayContent.mBaseDisplayWidth;
                    size.y = displayContent.mBaseDisplayHeight;
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setForcedDisplaySize(int displayId, int width, int height) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0) {
            throw new SecurityException("Must hold permission android.permission.WRITE_SECURE_SETTINGS");
        } else if (displayId != 0) {
            throw new IllegalArgumentException("Can only set the default display");
        } else {
            long ident = Binder.clearCallingIdentity();
            try {
                synchronized (this.mWindowMap) {
                    boostPriorityForLockedSection();
                    DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                    if (displayContent != null) {
                        width = Math.min(Math.max(width, 200), displayContent.mInitialDisplayWidth * 2);
                        height = Math.min(Math.max(height, 200), displayContent.mInitialDisplayHeight * 2);
                        setForcedDisplaySizeLocked(displayContent, width, height);
                        Global.putString(this.mContext.getContentResolver(), "display_size_forced", width + "," + height);
                    }
                }
                resetPriorityAfterLockedSection();
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    public void setForcedDisplayScalingMode(int displayId, int mode) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0) {
            throw new SecurityException("Must hold permission android.permission.WRITE_SECURE_SETTINGS");
        } else if (displayId != 0) {
            throw new IllegalArgumentException("Can only set the default display");
        } else {
            long ident = Binder.clearCallingIdentity();
            try {
                synchronized (this.mWindowMap) {
                    boostPriorityForLockedSection();
                    DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                    if (displayContent != null) {
                        if (mode < 0 || mode > 1) {
                            mode = 0;
                        }
                        setForcedDisplayScalingModeLocked(displayContent, mode);
                        Global.putInt(this.mContext.getContentResolver(), "display_scaling_force", mode);
                    }
                }
                resetPriorityAfterLockedSection();
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    private void setForcedDisplayScalingModeLocked(DisplayContent displayContent, int mode) {
        boolean z;
        Slog.i(TAG, "Using display scaling mode: " + (mode == 0 ? Shell.NIGHT_MODE_STR_AUTO : "off"));
        if (mode != 0) {
            z = true;
        } else {
            z = false;
        }
        displayContent.mDisplayScalingDisabled = z;
        reconfigureDisplayLocked(displayContent);
    }

    private void readForcedDisplayPropertiesLocked(DisplayContent displayContent) {
        String sizeStr = Global.getString(this.mContext.getContentResolver(), "display_size_forced");
        if (sizeStr == null || sizeStr.length() == 0) {
            sizeStr = SystemProperties.get(SIZE_OVERRIDE, null);
        }
        if (sizeStr != null && sizeStr.length() > 0) {
            int pos = sizeStr.indexOf(44);
            if (pos > 0 && sizeStr.lastIndexOf(44) == pos) {
                try {
                    int width = Integer.parseInt(sizeStr.substring(0, pos));
                    int height = Integer.parseInt(sizeStr.substring(pos + 1));
                    if (!(displayContent.mBaseDisplayWidth == width && displayContent.mBaseDisplayHeight == height)) {
                        Slog.i(TAG, "FORCED DISPLAY SIZE: " + width + "x" + height);
                        displayContent.updateBaseDisplayMetrics(width, height, displayContent.mBaseDisplayDensity);
                    }
                } catch (NumberFormatException e) {
                }
            }
        }
        int density = getForcedDisplayDensityForUserLocked(this.mCurrentUserId);
        if (density != 0) {
            displayContent.mBaseDisplayDensity = density;
        }
        if (Global.getInt(this.mContext.getContentResolver(), "display_scaling_force", 0) != 0) {
            Slog.i(TAG, "FORCED DISPLAY SCALING DISABLED");
            displayContent.mDisplayScalingDisabled = true;
        }
    }

    private void setForcedDisplaySizeLocked(DisplayContent displayContent, int width, int height) {
        Slog.i(TAG, "Using new display size: " + width + "x" + height);
        displayContent.updateBaseDisplayMetrics(width, height, displayContent.mBaseDisplayDensity);
        reconfigureDisplayLocked(displayContent);
    }

    public void clearForcedDisplaySize(int displayId) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0) {
            throw new SecurityException("Must hold permission android.permission.WRITE_SECURE_SETTINGS");
        } else if (displayId != 0) {
            throw new IllegalArgumentException("Can only set the default display");
        } else {
            long ident = Binder.clearCallingIdentity();
            try {
                synchronized (this.mWindowMap) {
                    boostPriorityForLockedSection();
                    DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                    if (displayContent != null) {
                        setForcedDisplaySizeLocked(displayContent, displayContent.mInitialDisplayWidth, displayContent.mInitialDisplayHeight);
                        Global.putString(this.mContext.getContentResolver(), "display_size_forced", "");
                    }
                }
                resetPriorityAfterLockedSection();
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getInitialDisplayDensity(int r4) {
        /*
        r3 = this;
        r2 = r3.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0025 }
        r1 = r3.mRoot;	 Catch:{ all -> 0x0025 }
        r0 = r1.getDisplayContentOrCreate(r4);	 Catch:{ all -> 0x0025 }
        if (r0 == 0) goto L_0x001f;
    L_0x000e:
        r1 = android.os.Binder.getCallingUid();	 Catch:{ all -> 0x0025 }
        r1 = r0.hasAccess(r1);	 Catch:{ all -> 0x0025 }
        if (r1 == 0) goto L_0x001f;
    L_0x0018:
        r1 = r0.mInitialDisplayDensity;	 Catch:{ all -> 0x0025 }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return r1;
    L_0x001f:
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        r1 = -1;
        return r1;
    L_0x0025:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.getInitialDisplayDensity(int):int");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getBaseDisplayDensity(int r4) {
        /*
        r3 = this;
        r2 = r3.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0025 }
        r1 = r3.mRoot;	 Catch:{ all -> 0x0025 }
        r0 = r1.getDisplayContentOrCreate(r4);	 Catch:{ all -> 0x0025 }
        if (r0 == 0) goto L_0x001f;
    L_0x000e:
        r1 = android.os.Binder.getCallingUid();	 Catch:{ all -> 0x0025 }
        r1 = r0.hasAccess(r1);	 Catch:{ all -> 0x0025 }
        if (r1 == 0) goto L_0x001f;
    L_0x0018:
        r1 = r0.mBaseDisplayDensity;	 Catch:{ all -> 0x0025 }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return r1;
    L_0x001f:
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        r1 = -1;
        return r1;
    L_0x0025:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.getBaseDisplayDensity(int):int");
    }

    public void setForcedDisplayDensityForUser(int displayId, int density, int userId) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0) {
            throw new SecurityException("Must hold permission android.permission.WRITE_SECURE_SETTINGS");
        } else if (displayId != 0) {
            throw new IllegalArgumentException("Can only set the default display");
        } else {
            int targetUserId = ActivityManager.handleIncomingUser(Binder.getCallingPid(), Binder.getCallingUid(), userId, false, true, "setForcedDisplayDensityForUser", null);
            long ident = Binder.clearCallingIdentity();
            try {
                synchronized (this.mWindowMap) {
                    boostPriorityForLockedSection();
                    DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                    if (displayContent != null && this.mCurrentUserId == targetUserId) {
                        setForcedDisplayDensityLocked(displayContent, density);
                    }
                    Secure.putStringForUser(this.mContext.getContentResolver(), "display_density_forced", Integer.toString(density), targetUserId);
                }
                resetPriorityAfterLockedSection();
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    public void clearForcedDisplayDensityForUser(int displayId, int userId) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0) {
            throw new SecurityException("Must hold permission android.permission.WRITE_SECURE_SETTINGS");
        } else if (displayId != 0) {
            throw new IllegalArgumentException("Can only set the default display");
        } else {
            int callingUserId = ActivityManager.handleIncomingUser(Binder.getCallingPid(), Binder.getCallingUid(), userId, false, true, "clearForcedDisplayDensityForUser", null);
            long ident = Binder.clearCallingIdentity();
            try {
                synchronized (this.mWindowMap) {
                    boostPriorityForLockedSection();
                    DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                    if (displayContent != null && this.mCurrentUserId == callingUserId) {
                        setForcedDisplayDensityLocked(displayContent, displayContent.mInitialDisplayDensity);
                    }
                    Secure.putStringForUser(this.mContext.getContentResolver(), "display_density_forced", "", callingUserId);
                }
                resetPriorityAfterLockedSection();
                Binder.restoreCallingIdentity(ident);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    private int getForcedDisplayDensityForUserLocked(int userId) {
        String densityStr = Secure.getStringForUser(this.mContext.getContentResolver(), "display_density_forced", userId);
        if (densityStr == null || densityStr.length() == 0) {
            densityStr = SystemProperties.get(DENSITY_OVERRIDE, null);
        }
        if (densityStr != null && densityStr.length() > 0) {
            try {
                return Integer.parseInt(densityStr);
            } catch (NumberFormatException e) {
            }
        }
        return 0;
    }

    private void setForcedDisplayDensityLocked(DisplayContent displayContent, int density) {
        displayContent.mBaseDisplayDensity = density;
        reconfigureDisplayLocked(displayContent);
    }

    void reconfigureDisplayLocked(DisplayContent displayContent) {
        if (this.mDisplayReady) {
            configureDisplayPolicyLocked(displayContent);
            displayContent.setLayoutNeeded();
            int displayId = displayContent.getDisplayId();
            boolean configChanged = updateOrientationFromAppTokensLocked(false, displayId);
            Configuration currentDisplayConfig = displayContent.getConfiguration();
            this.mTempConfiguration.setTo(currentDisplayConfig);
            displayContent.computeScreenConfiguration(this.mTempConfiguration);
            if (configChanged | (currentDisplayConfig.diff(this.mTempConfiguration) != 0 ? 1 : 0)) {
                this.mWaitingForConfig = true;
                startFreezingDisplayLocked(false, 0, 0, displayContent);
                this.mH.obtainMessage(18, Integer.valueOf(displayId)).sendToTarget();
            }
            this.mWindowPlacerLocked.performSurfacePlacement();
        }
    }

    void configureDisplayPolicyLocked(DisplayContent displayContent) {
        this.mPolicy.setInitialDisplaySize(displayContent.getDisplay(), displayContent.mBaseDisplayWidth, displayContent.mBaseDisplayHeight, displayContent.mBaseDisplayDensity);
        DisplayInfo displayInfo = displayContent.getDisplayInfo();
        this.mPolicy.setDisplayOverscan(displayContent.getDisplay(), displayInfo.overscanLeft, displayInfo.overscanTop, displayInfo.overscanRight, displayInfo.overscanBottom);
    }

    public void getDisplaysInFocusOrder(SparseIntArray displaysInFocusOrder) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mRoot.getDisplaysInFocusOrder(displaysInFocusOrder);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setOverscan(int displayId, int left, int top, int right, int bottom) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.WRITE_SECURE_SETTINGS") != 0) {
            throw new SecurityException("Must hold permission android.permission.WRITE_SECURE_SETTINGS");
        }
        long ident = Binder.clearCallingIdentity();
        try {
            synchronized (this.mWindowMap) {
                boostPriorityForLockedSection();
                DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                if (displayContent != null) {
                    setOverscanLocked(displayContent, left, top, right, bottom);
                }
            }
            resetPriorityAfterLockedSection();
            Binder.restoreCallingIdentity(ident);
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(ident);
        }
    }

    private void setOverscanLocked(DisplayContent displayContent, int left, int top, int right, int bottom) {
        DisplayInfo displayInfo = displayContent.getDisplayInfo();
        displayInfo.overscanLeft = left;
        displayInfo.overscanTop = top;
        displayInfo.overscanRight = right;
        displayInfo.overscanBottom = bottom;
        this.mDisplaySettings.setOverscanLocked(displayInfo.uniqueId, displayInfo.name, left, top, right, bottom);
        this.mDisplaySettings.writeSettingsLocked();
        reconfigureDisplayLocked(displayContent);
    }

    final WindowState windowForClientLocked(Session session, IWindow client, boolean throwOnError) {
        return windowForClientLocked(session, client.asBinder(), throwOnError);
    }

    final WindowState windowForClientLocked(Session session, IBinder client, boolean throwOnError) {
        WindowState win = (WindowState) this.mWindowMap.get(client);
        if (win == null) {
            if (throwOnError) {
                throw new IllegalArgumentException("Requested window " + client + " does not exist");
            }
            Slog.w(TAG, "Failed looking up window callers=" + Debug.getCallers(3));
            return null;
        } else if (session == null || win.mSession == session) {
            return win;
        } else {
            if (throwOnError) {
                throw new IllegalArgumentException("Requested window " + client + " is in session " + win.mSession + ", not " + session);
            }
            Slog.w(TAG, "Failed looking up window callers=" + Debug.getCallers(3));
            return null;
        }
    }

    void makeWindowFreezingScreenIfNeededLocked(WindowState w) {
        if (!w.mToken.okToDisplay() && this.mWindowsFreezingScreen != 2) {
            w.setOrientationChanging(true);
            w.mLastFreezeDuration = 0;
            this.mRoot.mOrientationChangeComplete = false;
            if (this.mWindowsFreezingScreen == 0) {
                this.mWindowsFreezingScreen = 1;
                this.mH.removeMessages(11);
                this.mH.sendEmptyMessageDelayed(11, 2000);
            }
        }
    }

    int handleAnimatingStoppedAndTransitionLocked() {
        this.mAppTransition.setIdle();
        for (int i = this.mNoAnimationNotifyOnTransitionFinished.size() - 1; i >= 0; i--) {
            this.mAppTransition.notifyAppTransitionFinishedLocked((IBinder) this.mNoAnimationNotifyOnTransitionFinished.get(i));
        }
        this.mNoAnimationNotifyOnTransitionFinished.clear();
        DisplayContent dc = getDefaultDisplayContentLocked();
        dc.mWallpaperController.hideDeferredWallpapersIfNeeded();
        dc.onAppTransitionDone();
        dc.computeImeTarget(true);
        this.mRoot.mWallpaperMayChange = true;
        this.mFocusMayChange = true;
        return 1;
    }

    void checkDrawnWindowsLocked() {
        if (!this.mWaitingForDrawn.isEmpty() && this.mWaitingForDrawnCallback != null) {
            for (int j = this.mWaitingForDrawn.size() - 1; j >= 0; j--) {
                WindowState win = (WindowState) this.mWaitingForDrawn.get(j);
                if (win.mRemoved || (win.mHasSurface ^ 1) != 0 || (win.mPolicyVisibility ^ 1) != 0) {
                    this.mWaitingForDrawn.remove(win);
                } else if (win.hasDrawnLw()) {
                    this.mWaitingForDrawn.remove(win);
                }
            }
            if (this.mWaitingForDrawn.isEmpty()) {
                this.mH.removeMessages(24);
                this.mH.sendEmptyMessage(33);
            }
        }
    }

    void setHoldScreenLocked(Session newHoldScreen) {
        boolean hold = newHoldScreen != null;
        if (hold && this.mHoldingScreenOn != newHoldScreen) {
            this.mHoldingScreenWakeLock.setWorkSource(new WorkSource(newHoldScreen.mUid));
        }
        this.mHoldingScreenOn = newHoldScreen;
        if (hold == this.mHoldingScreenWakeLock.isHeld()) {
            return;
        }
        if (hold) {
            this.mLastWakeLockHoldingWindow = this.mRoot.mHoldScreenWindow;
            this.mLastWakeLockObscuringWindow = null;
            this.mHoldingScreenWakeLock.acquire();
            this.mPolicy.keepScreenOnStartedLw();
            return;
        }
        this.mLastWakeLockHoldingWindow = null;
        this.mLastWakeLockObscuringWindow = this.mRoot.mObscuringWindow;
        this.mPolicy.keepScreenOnStoppedLw();
        this.mHoldingScreenWakeLock.release();
    }

    void requestTraversal() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mWindowPlacerLocked.requestTraversal();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    void scheduleAnimationLocked() {
        this.mAnimator.scheduleAnimation();
    }

    boolean updateFocusedWindowLocked(int mode, boolean updateInputWindows) {
        WindowState newFocus = this.mRoot.computeFocusedWindow();
        if (this.mCurrentFocus == newFocus) {
            return false;
        }
        Trace.traceBegin(32, "wmUpdateFocus");
        this.mH.removeMessages(2);
        this.mH.sendEmptyMessage(2);
        DisplayContent displayContent = getDefaultDisplayContentLocked();
        boolean imWindowChanged = false;
        if (this.mInputMethodWindow != null) {
            imWindowChanged = this.mInputMethodTarget != displayContent.computeImeTarget(true);
            if (!(mode == 1 || mode == 3)) {
                int prevImeAnimLayer = this.mInputMethodWindow.mWinAnimator.mAnimLayer;
                displayContent.assignWindowLayers(false);
                imWindowChanged |= prevImeAnimLayer != this.mInputMethodWindow.mWinAnimator.mAnimLayer ? 1 : 0;
            }
        }
        if (imWindowChanged) {
            this.mWindowsChanged = true;
            displayContent.setLayoutNeeded();
            newFocus = this.mRoot.computeFocusedWindow();
        }
        WindowState oldFocus = this.mCurrentFocus;
        this.mCurrentFocus = newFocus;
        this.mLosingFocus.remove(newFocus);
        if (this.mCurrentFocus != null) {
            this.mWinAddedSinceNullFocus.clear();
            this.mWinRemovedSinceNullFocus.clear();
        }
        int focusChanged = this.mPolicy.focusChangedLw(oldFocus, newFocus);
        if (imWindowChanged && oldFocus != this.mInputMethodWindow) {
            if (mode == 2) {
                displayContent.performLayout(true, updateInputWindows);
                focusChanged &= -2;
            } else if (mode == 3) {
                displayContent.assignWindowLayers(false);
            }
        }
        if ((focusChanged & 1) != 0) {
            displayContent.setLayoutNeeded();
            if (mode == 2) {
                displayContent.performLayout(true, updateInputWindows);
            }
        }
        if (mode != 1) {
            this.mInputMonitor.setInputFocusLw(this.mCurrentFocus, updateInputWindows);
        }
        displayContent.adjustForImeIfNeeded();
        displayContent.scheduleToastWindowsTimeoutIfNeededLocked(oldFocus, newFocus);
        Trace.traceEnd(32);
        return true;
    }

    void startFreezingDisplayLocked(boolean inTransaction, int exitAnim, int enterAnim) {
        startFreezingDisplayLocked(inTransaction, exitAnim, enterAnim, getDefaultDisplayContentLocked());
    }

    void startFreezingDisplayLocked(boolean inTransaction, int exitAnim, int enterAnim, DisplayContent displayContent) {
        if (!this.mDisplayFrozen && displayContent.isReady() && (this.mPolicy.isScreenOn() ^ 1) == 0 && (displayContent.okToAnimate() ^ 1) == 0) {
            this.mScreenFrozenLock.acquire();
            this.mDisplayFrozen = true;
            this.mDisplayFreezeTime = SystemClock.elapsedRealtime();
            this.mLastFinishedFreezeSource = null;
            this.mFrozenDisplayId = displayContent.getDisplayId();
            this.mInputMonitor.freezeInputDispatchingLw();
            this.mPolicy.setLastInputMethodWindowLw(null, null);
            if (this.mAppTransition.isTransitionSet()) {
                this.mAppTransition.freeze();
            }
            if (displayContent.isDefaultDisplay) {
                this.mExitAnimId = exitAnim;
                this.mEnterAnimId = enterAnim;
                ScreenRotationAnimation screenRotationAnimation = this.mAnimator.getScreenRotationAnimationLocked(this.mFrozenDisplayId);
                if (screenRotationAnimation != null) {
                    screenRotationAnimation.kill();
                }
                boolean isSecure = displayContent.hasSecureWindowOnScreen();
                displayContent.updateDisplayInfo();
                this.mAnimator.setScreenRotationAnimationLocked(this.mFrozenDisplayId, new ScreenRotationAnimation(this.mContext, displayContent, this.mFxSession, inTransaction, this.mPolicy.isDefaultOrientationForced(), isSecure, this));
            }
        }
    }

    void stopFreezingDisplayLocked() {
        if (this.mDisplayFrozen && !this.mWaitingForConfig && this.mAppsFreezingScreen <= 0 && this.mWindowsFreezingScreen != 1 && !this.mClientFreezingScreen && (this.mOpeningApps.isEmpty() ^ 1) == 0) {
            DisplayContent displayContent = this.mRoot.getDisplayContent(this.mFrozenDisplayId);
            int displayId = this.mFrozenDisplayId;
            this.mFrozenDisplayId = -1;
            this.mDisplayFrozen = false;
            this.mLastDisplayFreezeDuration = (int) (SystemClock.elapsedRealtime() - this.mDisplayFreezeTime);
            StringBuilder stringBuilder = new StringBuilder(128);
            stringBuilder.append("Screen frozen for ");
            TimeUtils.formatDuration((long) this.mLastDisplayFreezeDuration, stringBuilder);
            if (this.mLastFinishedFreezeSource != null) {
                stringBuilder.append(" due to ");
                stringBuilder.append(this.mLastFinishedFreezeSource);
            }
            Slog.i(TAG, stringBuilder.toString());
            this.mH.removeMessages(17);
            this.mH.removeMessages(30);
            boolean updateRotation = false;
            ScreenRotationAnimation screenRotationAnimation = this.mAnimator.getScreenRotationAnimationLocked(displayId);
            if (screenRotationAnimation == null || !screenRotationAnimation.hasScreenshot()) {
                if (screenRotationAnimation != null) {
                    screenRotationAnimation.kill();
                    this.mAnimator.setScreenRotationAnimationLocked(displayId, null);
                }
                updateRotation = true;
            } else {
                DisplayInfo displayInfo = displayContent.getDisplayInfo();
                if (!this.mPolicy.validateRotationAnimationLw(this.mExitAnimId, this.mEnterAnimId, displayContent.isDimming())) {
                    this.mEnterAnimId = 0;
                    this.mExitAnimId = 0;
                }
                if (screenRotationAnimation.dismiss(this.mFxSession, JobStatus.DEFAULT_TRIGGER_UPDATE_DELAY, getTransitionAnimationScaleLocked(), displayInfo.logicalWidth, displayInfo.logicalHeight, this.mExitAnimId, this.mEnterAnimId)) {
                    scheduleAnimationLocked();
                } else {
                    screenRotationAnimation.kill();
                    this.mAnimator.setScreenRotationAnimationLocked(displayId, null);
                    updateRotation = true;
                }
            }
            this.mInputMonitor.thawInputDispatchingLw();
            boolean configChanged = updateOrientationFromAppTokensLocked(false, displayId);
            this.mH.removeMessages(15);
            this.mH.sendEmptyMessageDelayed(15, 2000);
            this.mScreenFrozenLock.release();
            if (updateRotation) {
                configChanged |= displayContent.updateRotationUnchecked(false);
            }
            if (configChanged) {
                this.mH.obtainMessage(18, Integer.valueOf(displayId)).sendToTarget();
            }
        }
    }

    static int getPropertyInt(String[] tokens, int index, int defUnits, int defDps, DisplayMetrics dm) {
        if (index < tokens.length) {
            String str = tokens[index];
            if (str != null && str.length() > 0) {
                try {
                    return Integer.parseInt(str);
                } catch (Exception e) {
                }
            }
        }
        if (defUnits == 0) {
            return defDps;
        }
        return (int) TypedValue.applyDimension(defUnits, (float) defDps, dm);
    }

    void createWatermarkInTransaction() {
        Throwable th;
        if (this.mWatermark == null) {
            FileInputStream fileInputStream = null;
            DataInputStream dataInputStream = null;
            try {
                FileInputStream in = new FileInputStream(new File("/system/etc/setup.conf"));
                try {
                    DataInputStream ind = new DataInputStream(in);
                    try {
                        String line = ind.readLine();
                        if (line != null) {
                            String[] toks = line.split("%");
                            if (toks != null && toks.length > 0) {
                                DisplayContent displayContent = getDefaultDisplayContentLocked();
                                this.mWatermark = new Watermark(displayContent.getDisplay(), displayContent.mRealDisplayMetrics, this.mFxSession, toks);
                            }
                        }
                        if (ind != null) {
                            try {
                                ind.close();
                            } catch (IOException e) {
                            }
                        } else if (in != null) {
                            try {
                                in.close();
                            } catch (IOException e2) {
                            }
                        }
                        fileInputStream = in;
                    } catch (FileNotFoundException e3) {
                        dataInputStream = ind;
                        fileInputStream = in;
                        if (dataInputStream == null) {
                            try {
                                dataInputStream.close();
                            } catch (IOException e4) {
                            }
                        } else if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (IOException e5) {
                            }
                        }
                    } catch (IOException e6) {
                        dataInputStream = ind;
                        fileInputStream = in;
                        if (dataInputStream == null) {
                            try {
                                dataInputStream.close();
                            } catch (IOException e7) {
                            }
                        } else if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (IOException e8) {
                            }
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        dataInputStream = ind;
                        fileInputStream = in;
                        if (dataInputStream == null) {
                            try {
                                dataInputStream.close();
                            } catch (IOException e9) {
                            }
                        } else if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (IOException e10) {
                            }
                        }
                        throw th;
                    }
                } catch (FileNotFoundException e11) {
                    fileInputStream = in;
                    if (dataInputStream == null) {
                        dataInputStream.close();
                    } else if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                } catch (IOException e12) {
                    fileInputStream = in;
                    if (dataInputStream == null) {
                        dataInputStream.close();
                    } else if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                } catch (Throwable th3) {
                    th = th3;
                    fileInputStream = in;
                    if (dataInputStream == null) {
                        dataInputStream.close();
                    } else if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                    throw th;
                }
            } catch (FileNotFoundException e13) {
                if (dataInputStream == null) {
                    dataInputStream.close();
                } else if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e14) {
                if (dataInputStream == null) {
                    dataInputStream.close();
                } else if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (Throwable th4) {
                th = th4;
                if (dataInputStream == null) {
                    dataInputStream.close();
                } else if (fileInputStream != null) {
                    fileInputStream.close();
                }
                throw th;
            }
        }
    }

    public void setRecentsVisibility(boolean visible) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.STATUS_BAR") != 0) {
            throw new SecurityException("Caller does not hold permission android.permission.STATUS_BAR");
        }
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mPolicy.setRecentsVisibilityLw(visible);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setPipVisibility(boolean visible) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.STATUS_BAR") != 0) {
            throw new SecurityException("Caller does not hold permission android.permission.STATUS_BAR");
        }
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mPolicy.setPipVisibilityLw(visible);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void statusBarVisibilityChanged(int visibility) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.STATUS_BAR") != 0) {
            throw new SecurityException("Caller does not hold permission android.permission.STATUS_BAR");
        }
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mLastStatusBarVisibility = visibility;
                updateStatusBarVisibilityLocked(this.mPolicy.adjustSystemUiVisibilityLw(visibility));
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    private boolean updateStatusBarVisibilityLocked(int visibility) {
        if (this.mLastDispatchedSystemUiVisibility == visibility) {
            return false;
        }
        int globalDiff = ((this.mLastDispatchedSystemUiVisibility ^ visibility) & 7) & (~visibility);
        this.mLastDispatchedSystemUiVisibility = visibility;
        this.mInputManager.setSystemUiVisibility(visibility);
        getDefaultDisplayContentLocked().updateSystemUiVisibility(visibility, globalDiff);
        return true;
    }

    public void reevaluateStatusBarVisibility() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                if (updateStatusBarVisibilityLocked(this.mPolicy.adjustSystemUiVisibilityLw(this.mLastStatusBarVisibility))) {
                    this.mWindowPlacerLocked.requestTraversal();
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public int getNavBarPosition() {
        int navBarPosition;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                getDefaultDisplayContentLocked().performLayout(false, false);
                navBarPosition = this.mPolicy.getNavBarPosition();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return navBarPosition;
    }

    public InputConsumer createInputConsumer(Looper looper, String name, Factory inputEventReceiverFactory) {
        InputConsumer createInputConsumer;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                createInputConsumer = this.mInputMonitor.createInputConsumer(looper, name, inputEventReceiverFactory);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return createInputConsumer;
    }

    public void createInputConsumer(String name, InputChannel inputChannel) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mInputMonitor.createInputConsumer(name, inputChannel);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public boolean destroyInputConsumer(String name) {
        boolean destroyInputConsumer;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                destroyInputConsumer = this.mInputMonitor.destroyInputConsumer(name);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return destroyInputConsumer;
    }

    public Region getCurrentImeTouchRegion() {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.RESTRICTED_VR_ACCESS") != 0) {
            throw new SecurityException("getCurrentImeTouchRegion is restricted to VR services");
        }
        Region r;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                r = new Region();
                if (this.mInputMethodWindow != null) {
                    this.mInputMethodWindow.getTouchableRegion(r);
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return r;
    }

    public boolean hasNavigationBar() {
        return this.mPolicy.hasNavigationBar();
    }

    public void lockNow(Bundle options) {
        this.mPolicy.lockNow(options);
    }

    public void showRecentApps(boolean fromHome) {
        this.mPolicy.showRecentApps(fromHome);
    }

    public boolean isSafeModeEnabled() {
        return this.mSafeMode;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean clearWindowContentFrameStats(android.os.IBinder r6) {
        /*
        r5 = this;
        r4 = 0;
        r2 = "android.permission.FRAME_STATS";
        r3 = "clearWindowContentFrameStats()";
        r2 = r5.checkCallingPermission(r2, r3);
        if (r2 != 0) goto L_0x0016;
    L_0x000d:
        r2 = new java.lang.SecurityException;
        r3 = "Requires FRAME_STATS permission";
        r2.<init>(r3);
        throw r2;
    L_0x0016:
        r3 = r5.mWindowMap;
        monitor-enter(r3);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x003f }
        r2 = r5.mWindowMap;	 Catch:{ all -> 0x003f }
        r1 = r2.get(r6);	 Catch:{ all -> 0x003f }
        r1 = (com.android.server.wm.WindowState) r1;	 Catch:{ all -> 0x003f }
        if (r1 != 0) goto L_0x002b;
    L_0x0026:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r4;
    L_0x002b:
        r2 = r1.mWinAnimator;	 Catch:{ all -> 0x003f }
        r0 = r2.mSurfaceController;	 Catch:{ all -> 0x003f }
        if (r0 != 0) goto L_0x0036;
    L_0x0031:
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r4;
    L_0x0036:
        r2 = r0.clearWindowContentFrameStats();	 Catch:{ all -> 0x003f }
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        return r2;
    L_0x003f:
        r2 = move-exception;
        monitor-exit(r3);
        resetPriorityAfterLockedSection();
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.clearWindowContentFrameStats(android.os.IBinder):boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.WindowContentFrameStats getWindowContentFrameStats(android.os.IBinder r7) {
        /*
        r6 = this;
        r5 = 0;
        r3 = "android.permission.FRAME_STATS";
        r4 = "getWindowContentFrameStats()";
        r3 = r6.checkCallingPermission(r3, r4);
        if (r3 != 0) goto L_0x0016;
    L_0x000d:
        r3 = new java.lang.SecurityException;
        r4 = "Requires FRAME_STATS permission";
        r3.<init>(r4);
        throw r3;
    L_0x0016:
        r4 = r6.mWindowMap;
        monitor-enter(r4);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0053 }
        r3 = r6.mWindowMap;	 Catch:{ all -> 0x0053 }
        r2 = r3.get(r7);	 Catch:{ all -> 0x0053 }
        r2 = (com.android.server.wm.WindowState) r2;	 Catch:{ all -> 0x0053 }
        if (r2 != 0) goto L_0x002b;
    L_0x0026:
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x002b:
        r3 = r2.mWinAnimator;	 Catch:{ all -> 0x0053 }
        r1 = r3.mSurfaceController;	 Catch:{ all -> 0x0053 }
        if (r1 != 0) goto L_0x0036;
    L_0x0031:
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x0036:
        r3 = r6.mTempWindowRenderStats;	 Catch:{ all -> 0x0053 }
        if (r3 != 0) goto L_0x0041;
    L_0x003a:
        r3 = new android.view.WindowContentFrameStats;	 Catch:{ all -> 0x0053 }
        r3.<init>();	 Catch:{ all -> 0x0053 }
        r6.mTempWindowRenderStats = r3;	 Catch:{ all -> 0x0053 }
    L_0x0041:
        r0 = r6.mTempWindowRenderStats;	 Catch:{ all -> 0x0053 }
        r3 = r1.getWindowContentFrameStats(r0);	 Catch:{ all -> 0x0053 }
        if (r3 != 0) goto L_0x004e;
    L_0x0049:
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        return r5;
    L_0x004e:
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        return r0;
    L_0x0053:
        r3 = move-exception;
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.getWindowContentFrameStats(android.os.IBinder):android.view.WindowContentFrameStats");
    }

    public void notifyAppRelaunching(IBinder token) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                AppWindowToken appWindow = this.mRoot.getAppWindowToken(token);
                if (appWindow != null) {
                    appWindow.startRelaunching();
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void notifyAppRelaunchingFinished(IBinder token) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                AppWindowToken appWindow = this.mRoot.getAppWindowToken(token);
                if (appWindow != null) {
                    appWindow.finishRelaunching();
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void notifyAppRelaunchesCleared(IBinder token) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                AppWindowToken appWindow = this.mRoot.getAppWindowToken(token);
                if (appWindow != null) {
                    appWindow.clearRelaunching();
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void notifyAppResumedFinished(IBinder token) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                AppWindowToken appWindow = this.mRoot.getAppWindowToken(token);
                if (appWindow != null) {
                    this.mUnknownAppVisibilityController.notifyAppResumedFinished(appWindow);
                }
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void notifyTaskRemovedFromRecents(int taskId, int userId) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mTaskSnapshotController.notifyTaskRemovedFromRecents(taskId, userId);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public int getDockedDividerInsetsLw() {
        return getDefaultDisplayContentLocked().getDockedDividerController().getContentInsets();
    }

    private void dumpPolicyLocked(PrintWriter pw, String[] args, boolean dumpAll) {
        pw.println("WINDOW MANAGER POLICY STATE (dumpsys window policy)");
        this.mPolicy.dump("    ", pw, args);
    }

    private void dumpAnimatorLocked(PrintWriter pw, String[] args, boolean dumpAll) {
        pw.println("WINDOW MANAGER ANIMATOR STATE (dumpsys window animator)");
        this.mAnimator.dumpLocked(pw, "    ", dumpAll);
    }

    private void dumpTokensLocked(PrintWriter pw, boolean dumpAll) {
        pw.println("WINDOW MANAGER TOKENS (dumpsys window tokens)");
        this.mRoot.dumpTokens(pw, dumpAll);
        if (!this.mOpeningApps.isEmpty() || (this.mClosingApps.isEmpty() ^ 1) != 0) {
            pw.println();
            if (this.mOpeningApps.size() > 0) {
                pw.print("  mOpeningApps=");
                pw.println(this.mOpeningApps);
            }
            if (this.mClosingApps.size() > 0) {
                pw.print("  mClosingApps=");
                pw.println(this.mClosingApps);
            }
        }
    }

    private void dumpSessionsLocked(PrintWriter pw, boolean dumpAll) {
        pw.println("WINDOW MANAGER SESSIONS (dumpsys window sessions)");
        for (int i = 0; i < this.mSessions.size(); i++) {
            Session s = (Session) this.mSessions.valueAt(i);
            pw.print("  Session ");
            pw.print(s);
            pw.println(':');
            s.dump(pw, "    ");
        }
    }

    private void dumpWindowsLocked(PrintWriter pw, boolean dumpAll, ArrayList<WindowState> windows) {
        pw.println("WINDOW MANAGER WINDOWS (dumpsys window windows)");
        dumpWindowsNoHeaderLocked(pw, dumpAll, windows);
    }

    private void dumpWindowsNoHeaderLocked(PrintWriter pw, boolean dumpAll, ArrayList<WindowState> windows) {
        int i;
        WindowState w;
        this.mRoot.dumpWindowsNoHeader(pw, dumpAll, windows);
        if (!this.mHidingNonSystemOverlayWindows.isEmpty()) {
            pw.println();
            pw.println("  Hiding System Alert Windows:");
            for (i = this.mHidingNonSystemOverlayWindows.size() - 1; i >= 0; i--) {
                w = (WindowState) this.mHidingNonSystemOverlayWindows.get(i);
                pw.print("  #");
                pw.print(i);
                pw.print(' ');
                pw.print(w);
                if (dumpAll) {
                    pw.println(":");
                    w.dump(pw, "    ", true);
                } else {
                    pw.println();
                }
            }
        }
        if (this.mPendingRemove.size() > 0) {
            pw.println();
            pw.println("  Remove pending for:");
            for (i = this.mPendingRemove.size() - 1; i >= 0; i--) {
                w = (WindowState) this.mPendingRemove.get(i);
                if (windows == null || windows.contains(w)) {
                    pw.print("  Remove #");
                    pw.print(i);
                    pw.print(' ');
                    pw.print(w);
                    if (dumpAll) {
                        pw.println(":");
                        w.dump(pw, "    ", true);
                    } else {
                        pw.println();
                    }
                }
            }
        }
        if (this.mForceRemoves != null && this.mForceRemoves.size() > 0) {
            pw.println();
            pw.println("  Windows force removing:");
            for (i = this.mForceRemoves.size() - 1; i >= 0; i--) {
                w = (WindowState) this.mForceRemoves.get(i);
                pw.print("  Removing #");
                pw.print(i);
                pw.print(' ');
                pw.print(w);
                if (dumpAll) {
                    pw.println(":");
                    w.dump(pw, "    ", true);
                } else {
                    pw.println();
                }
            }
        }
        if (this.mDestroySurface.size() > 0) {
            pw.println();
            pw.println("  Windows waiting to destroy their surface:");
            for (i = this.mDestroySurface.size() - 1; i >= 0; i--) {
                w = (WindowState) this.mDestroySurface.get(i);
                if (windows == null || windows.contains(w)) {
                    pw.print("  Destroy #");
                    pw.print(i);
                    pw.print(' ');
                    pw.print(w);
                    if (dumpAll) {
                        pw.println(":");
                        w.dump(pw, "    ", true);
                    } else {
                        pw.println();
                    }
                }
            }
        }
        if (this.mLosingFocus.size() > 0) {
            pw.println();
            pw.println("  Windows losing focus:");
            for (i = this.mLosingFocus.size() - 1; i >= 0; i--) {
                w = (WindowState) this.mLosingFocus.get(i);
                if (windows == null || windows.contains(w)) {
                    pw.print("  Losing #");
                    pw.print(i);
                    pw.print(' ');
                    pw.print(w);
                    if (dumpAll) {
                        pw.println(":");
                        w.dump(pw, "    ", true);
                    } else {
                        pw.println();
                    }
                }
            }
        }
        if (this.mResizingWindows.size() > 0) {
            pw.println();
            pw.println("  Windows waiting to resize:");
            for (i = this.mResizingWindows.size() - 1; i >= 0; i--) {
                w = (WindowState) this.mResizingWindows.get(i);
                if (windows == null || windows.contains(w)) {
                    pw.print("  Resizing #");
                    pw.print(i);
                    pw.print(' ');
                    pw.print(w);
                    if (dumpAll) {
                        pw.println(":");
                        w.dump(pw, "    ", true);
                    } else {
                        pw.println();
                    }
                }
            }
        }
        if (this.mWaitingForDrawn.size() > 0) {
            pw.println();
            pw.println("  Clients waiting for these windows to be drawn:");
            for (i = this.mWaitingForDrawn.size() - 1; i >= 0; i--) {
                WindowState win = (WindowState) this.mWaitingForDrawn.get(i);
                pw.print("  Waiting #");
                pw.print(i);
                pw.print(' ');
                pw.print(win);
            }
        }
        pw.println();
        pw.print("  mGlobalConfiguration=");
        pw.println(this.mRoot.getConfiguration());
        pw.print("  mHasPermanentDpad=");
        pw.println(this.mHasPermanentDpad);
        pw.print("  mCurrentFocus=");
        pw.println(this.mCurrentFocus);
        if (this.mLastFocus != this.mCurrentFocus) {
            pw.print("  mLastFocus=");
            pw.println(this.mLastFocus);
        }
        pw.print("  mFocusedApp=");
        pw.println(this.mFocusedApp);
        if (this.mInputMethodTarget != null) {
            pw.print("  mInputMethodTarget=");
            pw.println(this.mInputMethodTarget);
        }
        pw.print("  mInTouchMode=");
        pw.print(this.mInTouchMode);
        pw.print(" mLayoutSeq=");
        pw.println(this.mLayoutSeq);
        pw.print("  mLastDisplayFreezeDuration=");
        TimeUtils.formatDuration((long) this.mLastDisplayFreezeDuration, pw);
        if (this.mLastFinishedFreezeSource != null) {
            pw.print(" due to ");
            pw.print(this.mLastFinishedFreezeSource);
        }
        pw.println();
        pw.print("  mLastWakeLockHoldingWindow=");
        pw.print(this.mLastWakeLockHoldingWindow);
        pw.print(" mLastWakeLockObscuringWindow=");
        pw.print(this.mLastWakeLockObscuringWindow);
        pw.println();
        this.mInputMonitor.dump(pw, "  ");
        this.mUnknownAppVisibilityController.dump(pw, "  ");
        this.mTaskSnapshotController.dump(pw, "  ");
        if (dumpAll) {
            pw.print("  mSystemDecorLayer=");
            pw.print(this.mSystemDecorLayer);
            pw.print(" mScreenRect=");
            pw.println(this.mScreenRect.toShortString());
            if (this.mLastStatusBarVisibility != 0) {
                pw.print("  mLastStatusBarVisibility=0x");
                pw.println(Integer.toHexString(this.mLastStatusBarVisibility));
            }
            if (this.mInputMethodWindow != null) {
                pw.print("  mInputMethodWindow=");
                pw.println(this.mInputMethodWindow);
            }
            this.mWindowPlacerLocked.dump(pw, "  ");
            this.mRoot.mWallpaperController.dump(pw, "  ");
            pw.print("  mSystemBooted=");
            pw.print(this.mSystemBooted);
            pw.print(" mDisplayEnabled=");
            pw.println(this.mDisplayEnabled);
            this.mRoot.dumpLayoutNeededDisplayIds(pw);
            pw.print("  mTransactionSequence=");
            pw.println(this.mTransactionSequence);
            pw.print("  mDisplayFrozen=");
            pw.print(this.mDisplayFrozen);
            pw.print(" windows=");
            pw.print(this.mWindowsFreezingScreen);
            pw.print(" client=");
            pw.print(this.mClientFreezingScreen);
            pw.print(" apps=");
            pw.print(this.mAppsFreezingScreen);
            pw.print(" waitingForConfig=");
            pw.println(this.mWaitingForConfig);
            DisplayContent defaultDisplayContent = getDefaultDisplayContentLocked();
            pw.print("  mRotation=");
            pw.print(defaultDisplayContent.getRotation());
            pw.print(" mAltOrientation=");
            pw.println(defaultDisplayContent.getAltOrientation());
            pw.print("  mLastWindowForcedOrientation=");
            pw.print(defaultDisplayContent.getLastWindowForcedOrientation());
            pw.print(" mLastOrientation=");
            pw.println(defaultDisplayContent.getLastOrientation());
            pw.print("  mDeferredRotationPauseCount=");
            pw.println(this.mDeferredRotationPauseCount);
            pw.print("  Animation settings: disabled=");
            pw.print(this.mAnimationsDisabled);
            pw.print(" window=");
            pw.print(this.mWindowAnimationScaleSetting);
            pw.print(" transition=");
            pw.print(this.mTransitionAnimationScaleSetting);
            pw.print(" animator=");
            pw.println(this.mAnimatorDurationScaleSetting);
            pw.print("  mSkipAppTransitionAnimation=");
            pw.println(this.mSkipAppTransitionAnimation);
            pw.println("  mLayoutToAnim:");
            this.mAppTransition.dump(pw, "    ");
        }
    }

    private boolean dumpWindows(PrintWriter pw, String name, String[] args, int opti, boolean dumpAll) {
        ArrayList<WindowState> windows = new ArrayList();
        if ("apps".equals(name) || "visible".equals(name) || "visible-apps".equals(name)) {
            boolean appsOnly = name.contains("apps");
            boolean visibleOnly = name.contains("visible");
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    if (appsOnly) {
                        this.mRoot.dumpDisplayContents(pw);
                    }
                    this.mRoot.forAllWindows((Consumer) new -$Lambda$AUkchKtIxrbCkLkg2ILGagAqXvc((byte) 1, visibleOnly, appsOnly, windows), true);
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
        } else {
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    this.mRoot.getWindowsByName(windows, name);
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
        }
        if (windows.size() <= 0) {
            return false;
        }
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                dumpWindowsLocked(pw, dumpAll, windows);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return true;
    }

    static /* synthetic */ void lambda$-com_android_server_wm_WindowManagerService_286361(boolean visibleOnly, boolean appsOnly, ArrayList windows, WindowState w) {
        if (visibleOnly && !w.mWinAnimator.getShown()) {
            return;
        }
        if (!appsOnly || w.mAppToken != null) {
            windows.add(w);
        }
    }

    private void dumpLastANRLocked(PrintWriter pw) {
        pw.println("WINDOW MANAGER LAST ANR (dumpsys window lastanr)");
        if (this.mLastANRState == null) {
            pw.println("  <no ANR has occurred since boot>");
        } else {
            pw.println(this.mLastANRState);
        }
    }

    void saveANRStateLocked(AppWindowToken appWindowToken, WindowState windowState, String reason) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new FastPrintWriter(sw, false, 1024);
        pw.println("  ANR time: " + DateFormat.getDateTimeInstance().format(new Date()));
        if (appWindowToken != null) {
            pw.println("  Application at fault: " + appWindowToken.stringName);
        }
        if (windowState != null) {
            pw.println("  Window at fault: " + windowState.mAttrs.getTitle());
        }
        if (reason != null) {
            pw.println("  Reason: " + reason);
        }
        if (!this.mWinAddedSinceNullFocus.isEmpty()) {
            pw.println("  Windows added since null focus: " + this.mWinAddedSinceNullFocus);
        }
        if (!this.mWinRemovedSinceNullFocus.isEmpty()) {
            pw.println("  Windows removed since null focus: " + this.mWinRemovedSinceNullFocus);
        }
        pw.println();
        dumpWindowsNoHeaderLocked(pw, true, null);
        pw.println();
        pw.println("Last ANR continued");
        this.mRoot.dumpDisplayContents(pw);
        pw.close();
        this.mLastANRState = sw.toString();
        this.mH.removeMessages(38);
        this.mH.sendEmptyMessageDelayed(38, 7200000);
    }

    public void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        String str = null;
        if (DumpUtils.checkDumpPermission(this.mContext, TAG, pw)) {
            boolean dumpAll = false;
            int opti = 0;
            while (opti < args.length) {
                String opt = args[opti];
                if (opt == null || opt.length() <= 0 || opt.charAt(0) != '-') {
                    break;
                }
                opti++;
                if ("-a".equals(opt)) {
                    dumpAll = true;
                } else if ("-h".equals(opt)) {
                    pw.println("Window manager dump options:");
                    pw.println("  [-a] [-h] [cmd] ...");
                    pw.println("  cmd may be one of:");
                    pw.println("    l[astanr]: last ANR information");
                    pw.println("    p[policy]: policy state");
                    pw.println("    a[animator]: animator state");
                    pw.println("    s[essions]: active sessions");
                    pw.println("    surfaces: active surfaces (debugging enabled only)");
                    pw.println("    d[isplays]: active display contents");
                    pw.println("    t[okens]: token list");
                    pw.println("    w[indows]: window list");
                    pw.println("  cmd may also be a NAME to dump windows.  NAME may");
                    pw.println("    be a partial substring in a window name, a");
                    pw.println("    Window hex object identifier, or");
                    pw.println("    \"all\" for all windows, or");
                    pw.println("    \"visible\" for the visible windows.");
                    pw.println("    \"visible-apps\" for the visible app windows.");
                    pw.println("  -a: include all available server state.");
                    return;
                } else {
                    pw.println("Unknown argument: " + opt + "; use -h for help");
                }
            }
            if (opti < args.length) {
                String cmd = args[opti];
                opti++;
                if ("lastanr".equals(cmd) || "l".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            dumpLastANRLocked(pw);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("policy".equals(cmd) || "p".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            dumpPolicyLocked(pw, args, true);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("animator".equals(cmd) || "a".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            dumpAnimatorLocked(pw, args, true);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("sessions".equals(cmd) || "s".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            dumpSessionsLocked(pw, true);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("surfaces".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            SurfaceTrace.dumpAllSurfaces(pw, null);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("displays".equals(cmd) || "d".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            this.mRoot.dumpDisplayContents(pw);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("tokens".equals(cmd) || "t".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            dumpTokensLocked(pw, true);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("windows".equals(cmd) || "w".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            dumpWindowsLocked(pw, true, null);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("all".equals(cmd) || "a".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            dumpWindowsLocked(pw, true, null);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else if ("containers".equals(cmd)) {
                    synchronized (this.mWindowMap) {
                        try {
                            boostPriorityForLockedSection();
                            StringBuilder output = new StringBuilder();
                            this.mRoot.dumpChildrenNames(output, " ");
                            pw.println(output.toString());
                            pw.println(" ");
                            this.mRoot.forAllWindows((Consumer) new -$Lambda$YIZfR4m-B8z_tYbP2x4OJ3o7OYE(UsbDescriptor.CLASSID_TYPECBRIDGE, pw), true);
                        } finally {
                            resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                } else {
                    if (!dumpWindows(pw, cmd, args, opti, dumpAll)) {
                        pw.println("Bad window command, or no windows match: " + cmd);
                        pw.println("Use -h for help.");
                    }
                    return;
                }
            }
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    pw.println();
                    if (dumpAll) {
                        pw.println("-------------------------------------------------------------------------------");
                    }
                    dumpLastANRLocked(pw);
                    pw.println();
                    if (dumpAll) {
                        pw.println("-------------------------------------------------------------------------------");
                    }
                    dumpPolicyLocked(pw, args, dumpAll);
                    pw.println();
                    if (dumpAll) {
                        pw.println("-------------------------------------------------------------------------------");
                    }
                    dumpAnimatorLocked(pw, args, dumpAll);
                    pw.println();
                    if (dumpAll) {
                        pw.println("-------------------------------------------------------------------------------");
                    }
                    dumpSessionsLocked(pw, dumpAll);
                    pw.println();
                    if (dumpAll) {
                        pw.println("-------------------------------------------------------------------------------");
                    }
                    if (dumpAll) {
                        str = "-------------------------------------------------------------------------------";
                    }
                    SurfaceTrace.dumpAllSurfaces(pw, str);
                    pw.println();
                    if (dumpAll) {
                        pw.println("-------------------------------------------------------------------------------");
                    }
                    this.mRoot.dumpDisplayContents(pw);
                    pw.println();
                    if (dumpAll) {
                        pw.println("-------------------------------------------------------------------------------");
                    }
                    dumpTokensLocked(pw, dumpAll);
                    pw.println();
                    if (dumpAll) {
                        pw.println("-------------------------------------------------------------------------------");
                    }
                    dumpWindowsLocked(pw, dumpAll, null);
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
        }
    }

    public void monitor() {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    private void createDisplayContentLocked(Display display) {
        if (display == null) {
            throw new IllegalArgumentException("getDisplayContent: display must not be null");
        }
        this.mRoot.getDisplayContentOrCreate(display.getDisplayId());
    }

    DisplayContent getDefaultDisplayContentLocked() {
        return this.mRoot.getDisplayContentOrCreate(0);
    }

    public void onDisplayAdded(int displayId) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                Display display = this.mDisplayManager.getDisplay(displayId);
                if (display != null) {
                    createDisplayContentLocked(display);
                    displayReady(displayId);
                }
                this.mWindowPlacerLocked.requestTraversal();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void onDisplayRemoved(int displayId) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                if (displayContent != null) {
                    displayContent.removeIfPossible();
                }
                this.mAnimator.removeDisplayLocked(displayId);
                this.mWindowPlacerLocked.requestTraversal();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void onDisplayChanged(int displayId) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                DisplayContent displayContent = this.mRoot.getDisplayContentOrCreate(displayId);
                if (displayContent != null) {
                    displayContent.updateDisplayInfo();
                }
                this.mWindowPlacerLocked.requestTraversal();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public Object getWindowManagerLock() {
        return this.mWindowMap;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setWillReplaceWindow(android.os.IBinder r6, boolean r7) {
        /*
        r5 = this;
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x003d }
        r1 = r5.mRoot;	 Catch:{ all -> 0x003d }
        r0 = r1.getAppWindowToken(r6);	 Catch:{ all -> 0x003d }
        if (r0 == 0) goto L_0x0016;
    L_0x000e:
        r1 = r0.hasContentToDisplay();	 Catch:{ all -> 0x003d }
        r1 = r1 ^ 1;
        if (r1 == 0) goto L_0x0035;
    L_0x0016:
        r1 = "WindowManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003d }
        r3.<init>();	 Catch:{ all -> 0x003d }
        r4 = "Attempted to set replacing window on non-existing app token ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x003d }
        r3 = r3.append(r6);	 Catch:{ all -> 0x003d }
        r3 = r3.toString();	 Catch:{ all -> 0x003d }
        android.util.Slog.w(r1, r3);	 Catch:{ all -> 0x003d }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0035:
        r0.setWillReplaceWindows(r7);	 Catch:{ all -> 0x003d }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x003d:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.setWillReplaceWindow(android.os.IBinder, boolean):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void setWillReplaceWindows(android.os.IBinder r6, boolean r7) {
        /*
        r5 = this;
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0048 }
        r1 = r5.mRoot;	 Catch:{ all -> 0x0048 }
        r0 = r1.getAppWindowToken(r6);	 Catch:{ all -> 0x0048 }
        if (r0 == 0) goto L_0x0016;
    L_0x000e:
        r1 = r0.hasContentToDisplay();	 Catch:{ all -> 0x0048 }
        r1 = r1 ^ 1;
        if (r1 == 0) goto L_0x0035;
    L_0x0016:
        r1 = "WindowManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0048 }
        r3.<init>();	 Catch:{ all -> 0x0048 }
        r4 = "Attempted to set replacing window on non-existing app token ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x0048 }
        r3 = r3.append(r6);	 Catch:{ all -> 0x0048 }
        r3 = r3.toString();	 Catch:{ all -> 0x0048 }
        android.util.Slog.w(r1, r3);	 Catch:{ all -> 0x0048 }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0035:
        if (r7 == 0) goto L_0x0043;
    L_0x0037:
        r0.setWillReplaceChildWindows();	 Catch:{ all -> 0x0048 }
    L_0x003a:
        r1 = 1;
        r5.scheduleClearWillReplaceWindows(r6, r1);	 Catch:{ all -> 0x0048 }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0043:
        r1 = 0;
        r0.setWillReplaceWindows(r1);	 Catch:{ all -> 0x0048 }
        goto L_0x003a;
    L_0x0048:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.setWillReplaceWindows(android.os.IBinder, boolean):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void scheduleClearWillReplaceWindows(android.os.IBinder r6, boolean r7) {
        /*
        r5 = this;
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x003b }
        r1 = r5.mRoot;	 Catch:{ all -> 0x003b }
        r0 = r1.getAppWindowToken(r6);	 Catch:{ all -> 0x003b }
        if (r0 != 0) goto L_0x002d;
    L_0x000e:
        r1 = "WindowManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003b }
        r3.<init>();	 Catch:{ all -> 0x003b }
        r4 = "Attempted to reset replacing window on non-existing app token ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x003b }
        r3 = r3.append(r6);	 Catch:{ all -> 0x003b }
        r3 = r3.toString();	 Catch:{ all -> 0x003b }
        android.util.Slog.w(r1, r3);	 Catch:{ all -> 0x003b }
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x002d:
        if (r7 == 0) goto L_0x0037;
    L_0x002f:
        r5.scheduleWindowReplacementTimeouts(r0);	 Catch:{ all -> 0x003b }
    L_0x0032:
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        return;
    L_0x0037:
        r0.clearWillReplaceWindows();	 Catch:{ all -> 0x003b }
        goto L_0x0032;
    L_0x003b:
        r1 = move-exception;
        monitor-exit(r2);
        resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.scheduleClearWillReplaceWindows(android.os.IBinder, boolean):void");
    }

    void scheduleWindowReplacementTimeouts(AppWindowToken appWindowToken) {
        if (!this.mWindowReplacementTimeouts.contains(appWindowToken)) {
            this.mWindowReplacementTimeouts.add(appWindowToken);
        }
        this.mH.removeMessages(46);
        this.mH.sendEmptyMessageDelayed(46, 2000);
    }

    public int getDockedStackSide() {
        int dockSide;
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                TaskStack dockedStack = getDefaultDisplayContentLocked().getDockedStackIgnoringVisibility();
                dockSide = dockedStack == null ? -1 : dockedStack.getDockSide();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
        return dockSide;
    }

    public void setDockedStackResizing(boolean resizing) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                getDefaultDisplayContentLocked().getDockedDividerController().setResizing(resizing);
                requestTraversal();
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setDockedStackDividerTouchRegion(Rect touchRegion) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                getDefaultDisplayContentLocked().getDockedDividerController().setTouchRegion(touchRegion);
                setFocusTaskRegionLocked(null);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setResizeDimLayer(boolean visible, int targetStackId, float alpha) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                getDefaultDisplayContentLocked().getDockedDividerController().setResizeDimLayer(visible, targetStackId, alpha);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setForceResizableTasks(boolean forceResizableTasks) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mForceResizableTasks = forceResizableTasks;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    public void setSupportsPictureInPicture(boolean supportsPictureInPicture) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                this.mSupportsPictureInPicture = supportsPictureInPicture;
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    static int dipToPixel(int dip, DisplayMetrics displayMetrics) {
        return (int) TypedValue.applyDimension(1, (float) dip, displayMetrics);
    }

    public void registerDockedStackListener(IDockedStackListener listener) {
        if (checkCallingPermission("android.permission.REGISTER_WINDOW_MANAGER_LISTENERS", "registerDockedStackListener()")) {
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    getDefaultDisplayContentLocked().mDividerControllerLocked.registerDockedStackListener(listener);
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
        }
    }

    public void registerPinnedStackListener(int displayId, IPinnedStackListener listener) {
        if (checkCallingPermission("android.permission.REGISTER_WINDOW_MANAGER_LISTENERS", "registerPinnedStackListener()") && this.mSupportsPictureInPicture) {
            synchronized (this.mWindowMap) {
                try {
                    boostPriorityForLockedSection();
                    this.mRoot.getDisplayContent(displayId).getPinnedStackController().registerPinnedStackListener(listener);
                } finally {
                    resetPriorityAfterLockedSection();
                }
            }
        }
    }

    public void requestAppKeyboardShortcuts(IResultReceiver receiver, int deviceId) {
        try {
            WindowState focusedWindow = getFocusedWindow();
            if (focusedWindow != null && focusedWindow.mClient != null) {
                getFocusedWindow().mClient.requestAppKeyboardShortcuts(receiver, deviceId);
            }
        } catch (RemoteException e) {
        }
    }

    public void getStableInsets(int displayId, Rect outInsets) throws RemoteException {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                getStableInsetsLocked(displayId, outInsets);
            } finally {
                resetPriorityAfterLockedSection();
            }
        }
    }

    void getStableInsetsLocked(int displayId, Rect outInsets) {
        outInsets.setEmpty();
        DisplayContent dc = this.mRoot.getDisplayContent(displayId);
        if (dc != null) {
            DisplayInfo di = dc.getDisplayInfo();
            this.mPolicy.getStableInsetsLw(di.rotation, di.logicalWidth, di.logicalHeight, outInsets);
        }
    }

    void intersectDisplayInsetBounds(Rect display, Rect insets, Rect inOutBounds) {
        this.mTmpRect3.set(display);
        this.mTmpRect3.inset(insets);
        inOutBounds.intersect(this.mTmpRect3);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void updatePointerIcon(android.view.IWindow r11) {
        /*
        r10 = this;
        r7 = r10.mMousePositionTracker;
        monitor-enter(r7);
        r6 = r10.mMousePositionTracker;	 Catch:{ all -> 0x0029 }
        r6 = r6.mLatestEventWasMouse;	 Catch:{ all -> 0x0029 }
        if (r6 != 0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r7);
        return;
    L_0x000d:
        r6 = r10.mMousePositionTracker;	 Catch:{ all -> 0x0029 }
        r3 = r6.mLatestMouseX;	 Catch:{ all -> 0x0029 }
        r6 = r10.mMousePositionTracker;	 Catch:{ all -> 0x0029 }
        r4 = r6.mLatestMouseY;	 Catch:{ all -> 0x0029 }
        monitor-exit(r7);
        r7 = r10.mWindowMap;
        monitor-enter(r7);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0086 }
        r6 = r10.mDragState;	 Catch:{ all -> 0x0086 }
        if (r6 == 0) goto L_0x002c;
    L_0x0024:
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        return;
    L_0x0029:
        r6 = move-exception;
        monitor-exit(r7);
        throw r6;
    L_0x002c:
        r6 = 0;
        r8 = 0;
        r0 = r10.windowForClientLocked(r6, r11, r8);	 Catch:{ all -> 0x0086 }
        if (r0 != 0) goto L_0x0053;
    L_0x0034:
        r6 = "WindowManager";
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0086 }
        r8.<init>();	 Catch:{ all -> 0x0086 }
        r9 = "Bad requesting window ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0086 }
        r8 = r8.append(r11);	 Catch:{ all -> 0x0086 }
        r8 = r8.toString();	 Catch:{ all -> 0x0086 }
        android.util.Slog.w(r6, r8);	 Catch:{ all -> 0x0086 }
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        return;
    L_0x0053:
        r1 = r0.getDisplayContent();	 Catch:{ all -> 0x0086 }
        if (r1 != 0) goto L_0x005e;
    L_0x0059:
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        return;
    L_0x005e:
        r5 = r1.getTouchableWinAtPointLocked(r3, r4);	 Catch:{ all -> 0x0086 }
        if (r5 == r0) goto L_0x0069;
    L_0x0064:
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        return;
    L_0x0069:
        r6 = r5.mClient;	 Catch:{ RemoteException -> 0x007b }
        r8 = r5.translateToWindowX(r3);	 Catch:{ RemoteException -> 0x007b }
        r9 = r5.translateToWindowY(r4);	 Catch:{ RemoteException -> 0x007b }
        r6.updatePointerIcon(r8, r9);	 Catch:{ RemoteException -> 0x007b }
    L_0x0076:
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        return;
    L_0x007b:
        r2 = move-exception;
        r6 = "WindowManager";
        r8 = "unable to update pointer icon";
        android.util.Slog.w(r6, r8);	 Catch:{ all -> 0x0086 }
        goto L_0x0076;
    L_0x0086:
        r6 = move-exception;
        monitor-exit(r7);
        resetPriorityAfterLockedSection();
        throw r6;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.updatePointerIcon(android.view.IWindow):void");
    }

    void restorePointerIconLocked(DisplayContent displayContent, float latestX, float latestY) {
        this.mMousePositionTracker.updatePosition(latestX, latestY);
        WindowState windowUnderPointer = displayContent.getTouchableWinAtPointLocked(latestX, latestY);
        if (windowUnderPointer != null) {
            try {
                windowUnderPointer.mClient.updatePointerIcon(windowUnderPointer.translateToWindowX(latestX), windowUnderPointer.translateToWindowY(latestY));
                return;
            } catch (RemoteException e) {
                Slog.w(TAG, "unable to restore pointer icon");
                return;
            }
        }
        InputManager.getInstance().setPointerIconType(1000);
    }

    public void registerShortcutKey(long shortcutCode, IShortcutService shortcutKeyReceiver) throws RemoteException {
        if (checkCallingPermission("android.permission.REGISTER_WINDOW_MANAGER_LISTENERS", "registerShortcutKey")) {
            this.mPolicy.registerShortcutKey(shortcutCode, shortcutKeyReceiver);
            return;
        }
        throw new SecurityException("Requires REGISTER_WINDOW_MANAGER_LISTENERS permission");
    }

    void markForSeamlessRotation(WindowState w, boolean seamlesslyRotated) {
        if (seamlesslyRotated != w.mSeamlesslyRotated) {
            w.mSeamlesslyRotated = seamlesslyRotated;
            if (seamlesslyRotated) {
                this.mSeamlessRotationCount++;
            } else {
                this.mSeamlessRotationCount--;
            }
            if (this.mSeamlessRotationCount == 0) {
                DisplayContent displayContent = w.getDisplayContent();
                if (displayContent.updateRotationUnchecked(false)) {
                    this.mH.obtainMessage(18, Integer.valueOf(displayContent.getDisplayId())).sendToTarget();
                }
            }
        }
    }

    void registerAppFreezeListener(AppFreezeListener listener) {
        if (!this.mAppFreezeListeners.contains(listener)) {
            this.mAppFreezeListeners.add(listener);
        }
    }

    void unregisterAppFreezeListener(AppFreezeListener listener) {
        this.mAppFreezeListeners.remove(listener);
    }

    public void inSurfaceTransaction(Runnable exec) {
        synchronized (this.mWindowMap) {
            try {
                boostPriorityForLockedSection();
                SurfaceControl.openTransaction();
                exec.run();
                SurfaceControl.closeTransaction();
            } catch (Throwable th) {
                resetPriorityAfterLockedSection();
            }
        }
        resetPriorityAfterLockedSection();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void disableNonVrUi(boolean r6) {
        /*
        r5 = this;
        r4 = r5.mWindowMap;
        monitor-enter(r4);
        boostPriorityForLockedSection();	 Catch:{ all -> 0x0032 }
        r2 = r6 ^ 1;
        r3 = r5.mShowAlertWindowNotifications;	 Catch:{ all -> 0x0032 }
        if (r2 != r3) goto L_0x0011;
    L_0x000c:
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        return;
    L_0x0011:
        r5.mShowAlertWindowNotifications = r2;	 Catch:{ all -> 0x0032 }
        r3 = r5.mSessions;	 Catch:{ all -> 0x0032 }
        r3 = r3.size();	 Catch:{ all -> 0x0032 }
        r0 = r3 + -1;
    L_0x001b:
        if (r0 < 0) goto L_0x002d;
    L_0x001d:
        r3 = r5.mSessions;	 Catch:{ all -> 0x0032 }
        r1 = r3.valueAt(r0);	 Catch:{ all -> 0x0032 }
        r1 = (com.android.server.wm.Session) r1;	 Catch:{ all -> 0x0032 }
        r3 = r5.mShowAlertWindowNotifications;	 Catch:{ all -> 0x0032 }
        r1.setShowingAlertWindowNotificationAllowed(r3);	 Catch:{ all -> 0x0032 }
        r0 = r0 + -1;
        goto L_0x001b;
    L_0x002d:
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        return;
    L_0x0032:
        r3 = move-exception;
        monitor-exit(r4);
        resetPriorityAfterLockedSection();
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowManagerService.disableNonVrUi(boolean):void");
    }

    boolean hasWideColorGamutSupport() {
        if (this.mHasWideColorGamutSupport) {
            return SystemProperties.getBoolean("persist.sys.sf.native_mode", false) ^ 1;
        }
        return false;
    }

    void updateNonSystemOverlayWindowsVisibilityIfNeeded(WindowState win, boolean surfaceShown) {
        if (win.hideNonSystemOverlayWindowsWhenVisible()) {
            boolean systemAlertWindowsHidden = this.mHidingNonSystemOverlayWindows.isEmpty() ^ 1;
            if (!surfaceShown) {
                this.mHidingNonSystemOverlayWindows.remove(win);
            } else if (!this.mHidingNonSystemOverlayWindows.contains(win)) {
                this.mHidingNonSystemOverlayWindows.add(win);
            }
            boolean hideSystemAlertWindows = this.mHidingNonSystemOverlayWindows.isEmpty() ^ 1;
            if (systemAlertWindowsHidden != hideSystemAlertWindows) {
                this.mRoot.forAllWindows((Consumer) new -$Lambda$eBBEuGZ8VbEXJy0r5EYYbvnl-8w(hideSystemAlertWindows), false);
            }
        }
    }
}
