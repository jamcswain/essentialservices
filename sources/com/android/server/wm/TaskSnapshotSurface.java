package com.android.server.wm;

import android.app.ActivityManager.TaskSnapshot;
import android.app.ActivityThread;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.GraphicBuffer;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.MergedConfiguration;
import android.view.IWindowSession;
import android.view.Surface;
import android.view.SurfaceControl;
import android.view.SurfaceSession;
import android.view.WindowManagerGlobal;
import android.view.WindowManagerPolicy.StartingSurface;
import com.android.internal.policy.DecorView;
import com.android.internal.view.BaseIWindow;

class TaskSnapshotSurface implements StartingSurface {
    private static final int FLAG_INHERIT_EXCLUDES = 830922808;
    private static final int MSG_REPORT_DRAW = 0;
    private static final int PRIVATE_FLAG_INHERITS = 131072;
    private static final long SIZE_MISMATCH_MINIMUM_TIME_MS = 450;
    private static final String TAG = "WindowManager";
    private static final String TITLE_FORMAT = "SnapshotStartingWindow for taskId=%s";
    private static Handler sHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    boolean hasDrawn;
                    TaskSnapshotSurface surface = msg.obj;
                    synchronized (surface.mService.mWindowMap) {
                        try {
                            WindowManagerService.boostPriorityForLockedSection();
                            hasDrawn = surface.mHasDrawn;
                        } finally {
                            WindowManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    if (hasDrawn) {
                        surface.reportDrawn();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    private final Paint mBackgroundPaint = new Paint();
    private SurfaceControl mChildSurfaceControl;
    private final Rect mContentInsets = new Rect();
    private final Rect mFrame = new Rect();
    private final Handler mHandler;
    private boolean mHasDrawn;
    private final int mOrientationOnCreation;
    private final WindowManagerService mService;
    private final IWindowSession mSession;
    private long mShownTime;
    private boolean mSizeMismatch;
    private TaskSnapshot mSnapshot;
    private final Rect mStableInsets = new Rect();
    private final int mStatusBarColor;
    private final Surface mSurface;
    final SystemBarBackgroundPainter mSystemBarBackgroundPainter;
    private final Rect mTaskBounds;
    private final CharSequence mTitle;
    private final Window mWindow;

    static class SystemBarBackgroundPainter {
        private final Rect mContentInsets = new Rect();
        private final int mNavigationBarColor;
        private final Paint mNavigationBarPaint = new Paint();
        private final Rect mStableInsets = new Rect();
        private final int mStatusBarColor;
        private final Paint mStatusBarPaint = new Paint();
        private final int mSysUiVis;
        private final int mWindowFlags;
        private final int mWindowPrivateFlags;

        SystemBarBackgroundPainter(int windowFlags, int windowPrivateFlags, int sysUiVis, int statusBarColor, int navigationBarColor) {
            this.mWindowFlags = windowFlags;
            this.mWindowPrivateFlags = windowPrivateFlags;
            this.mSysUiVis = sysUiVis;
            this.mStatusBarColor = DecorView.calculateStatusBarColor(windowFlags, ActivityThread.currentActivityThread().getSystemUiContext().getColor(17170762), statusBarColor);
            this.mNavigationBarColor = navigationBarColor;
            this.mStatusBarPaint.setColor(this.mStatusBarColor);
            this.mNavigationBarPaint.setColor(navigationBarColor);
        }

        void setInsets(Rect contentInsets, Rect stableInsets) {
            this.mContentInsets.set(contentInsets);
            this.mStableInsets.set(stableInsets);
        }

        int getStatusBarColorViewHeight() {
            if (DecorView.STATUS_BAR_COLOR_VIEW_ATTRIBUTES.isVisible(this.mSysUiVis, this.mStatusBarColor, this.mWindowFlags, (this.mWindowPrivateFlags & 131072) != 0)) {
                return DecorView.getColorViewTopInset(this.mStableInsets.top, this.mContentInsets.top);
            }
            return 0;
        }

        private boolean isNavigationBarColorViewVisible() {
            return DecorView.NAVIGATION_BAR_COLOR_VIEW_ATTRIBUTES.isVisible(this.mSysUiVis, this.mNavigationBarColor, this.mWindowFlags, false);
        }

        void drawDecors(Canvas c, Rect alreadyDrawnFrame) {
            drawStatusBarBackground(c, alreadyDrawnFrame, getStatusBarColorViewHeight());
            drawNavigationBarBackground(c);
        }

        void drawStatusBarBackground(Canvas c, Rect alreadyDrawnFrame, int statusBarHeight) {
            if (statusBarHeight <= 0) {
                return;
            }
            if (alreadyDrawnFrame == null || c.getWidth() > alreadyDrawnFrame.right) {
                c.drawRect((float) (alreadyDrawnFrame != null ? alreadyDrawnFrame.right : 0), 0.0f, (float) (c.getWidth() - DecorView.getColorViewRightInset(this.mStableInsets.right, this.mContentInsets.right)), (float) statusBarHeight, this.mStatusBarPaint);
            }
        }

        void drawNavigationBarBackground(Canvas c) {
            Rect navigationBarRect = new Rect();
            DecorView.getNavigationBarRect(c.getWidth(), c.getHeight(), this.mStableInsets, this.mContentInsets, navigationBarRect);
            if (isNavigationBarColorViewVisible() && (navigationBarRect.isEmpty() ^ 1) != 0) {
                c.drawRect(navigationBarRect, this.mNavigationBarPaint);
            }
        }
    }

    static class Window extends BaseIWindow {
        private TaskSnapshotSurface mOuter;

        Window() {
        }

        public void setOuter(TaskSnapshotSurface outer) {
            this.mOuter = outer;
        }

        public void resized(Rect frame, Rect overscanInsets, Rect contentInsets, Rect visibleInsets, Rect stableInsets, Rect outsets, boolean reportDraw, MergedConfiguration mergedConfiguration, Rect backDropFrame, boolean forceLayout, boolean alwaysConsumeNavBar, int displayId) {
            if (!(mergedConfiguration == null || this.mOuter == null || this.mOuter.mOrientationOnCreation == mergedConfiguration.getMergedConfiguration().orientation)) {
                Handler -get3 = TaskSnapshotSurface.sHandler;
                TaskSnapshotSurface taskSnapshotSurface = this.mOuter;
                taskSnapshotSurface.getClass();
                -get3.post(new -$Lambda$aEpJ2RCAIjecjyIIYTv6ricEwh4((byte) 8, taskSnapshotSurface));
            }
            if (reportDraw) {
                TaskSnapshotSurface.sHandler.obtainMessage(0, this.mOuter).sendToTarget();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.android.server.wm.TaskSnapshotSurface create(com.android.server.wm.WindowManagerService r48, com.android.server.wm.AppWindowToken r49, android.app.ActivityManager.TaskSnapshot r50) {
        /*
        r6 = new android.view.WindowManager$LayoutParams;
        r6.<init>();
        r4 = new com.android.server.wm.TaskSnapshotSurface$Window;
        r4.<init>();
        r3 = android.view.WindowManagerGlobal.getWindowSession();
        r4.setSession(r3);
        r13 = new android.view.Surface;
        r13.<init>();
        r9 = new android.graphics.Rect;
        r9.<init>();
        r32 = new android.graphics.Rect;
        r32.<init>();
        r34 = new android.graphics.Rect;
        r34.<init>();
        r36 = new android.graphics.Rect;
        r36.<init>();
        r39 = new android.util.MergedConfiguration;
        r39.<init>();
        r16 = -1;
        r17 = 0;
        r18 = 0;
        r0 = r48;
        r7 = r0.mWindowMap;
        monitor-enter(r7);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x017f }
        r42 = r49.findMainWindow();	 Catch:{ all -> 0x017f }
        r44 = r49.getTask();	 Catch:{ all -> 0x017f }
        if (r44 != 0) goto L_0x0068;
    L_0x0047:
        r5 = TAG;	 Catch:{ all -> 0x017f }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x017f }
        r8.<init>();	 Catch:{ all -> 0x017f }
        r11 = "TaskSnapshotSurface.create: Failed to find task for token=";
        r8 = r8.append(r11);	 Catch:{ all -> 0x017f }
        r0 = r49;
        r8 = r8.append(r0);	 Catch:{ all -> 0x017f }
        r8 = r8.toString();	 Catch:{ all -> 0x017f }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x017f }
        r5 = 0;
        monitor-exit(r7);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r5;
    L_0x0068:
        r5 = r49.getTask();	 Catch:{ all -> 0x017f }
        r46 = r5.getTopFullscreenAppToken();	 Catch:{ all -> 0x017f }
        if (r46 != 0) goto L_0x0093;
    L_0x0072:
        r5 = TAG;	 Catch:{ all -> 0x017f }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x017f }
        r8.<init>();	 Catch:{ all -> 0x017f }
        r11 = "TaskSnapshotSurface.create: Failed to find top fullscreen for task=";
        r8 = r8.append(r11);	 Catch:{ all -> 0x017f }
        r0 = r44;
        r8 = r8.append(r0);	 Catch:{ all -> 0x017f }
        r8 = r8.toString();	 Catch:{ all -> 0x017f }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x017f }
        r5 = 0;
        monitor-exit(r7);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r5;
    L_0x0093:
        r47 = r46.getTopFullscreenWindow();	 Catch:{ all -> 0x017f }
        if (r42 == 0) goto L_0x009b;
    L_0x0099:
        if (r47 != 0) goto L_0x00bc;
    L_0x009b:
        r5 = TAG;	 Catch:{ all -> 0x017f }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x017f }
        r8.<init>();	 Catch:{ all -> 0x017f }
        r11 = "TaskSnapshotSurface.create: Failed to find main window for token=";
        r8 = r8.append(r11);	 Catch:{ all -> 0x017f }
        r0 = r49;
        r8 = r8.append(r0);	 Catch:{ all -> 0x017f }
        r8 = r8.toString();	 Catch:{ all -> 0x017f }
        android.util.Slog.w(r5, r8);	 Catch:{ all -> 0x017f }
        r5 = 0;
        monitor-exit(r7);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r5;
    L_0x00bc:
        r19 = r47.getSystemUiVisibility();	 Catch:{ all -> 0x017f }
        r5 = r47.getAttrs();	 Catch:{ all -> 0x017f }
        r0 = r5.flags;	 Catch:{ all -> 0x017f }
        r20 = r0;
        r5 = r47.getAttrs();	 Catch:{ all -> 0x017f }
        r0 = r5.privateFlags;	 Catch:{ all -> 0x017f }
        r21 = r0;
        r5 = r42.getAttrs();	 Catch:{ all -> 0x017f }
        r5 = r5.dimAmount;	 Catch:{ all -> 0x017f }
        r6.dimAmount = r5;	 Catch:{ all -> 0x017f }
        r5 = 3;
        r6.type = r5;	 Catch:{ all -> 0x017f }
        r5 = r50.getSnapshot();	 Catch:{ all -> 0x017f }
        r5 = r5.getFormat();	 Catch:{ all -> 0x017f }
        r6.format = r5;	 Catch:{ all -> 0x017f }
        r5 = -830922809; // 0xffffffffce791fc7 float:-1.04490234E9 double:NaN;
        r5 = r5 & r20;
        r5 = r5 | 8;
        r5 = r5 | 16;
        r6.flags = r5;	 Catch:{ all -> 0x017f }
        r5 = 131072; // 0x20000 float:1.83671E-40 double:6.47582E-319;
        r5 = r5 & r21;
        r6.privateFlags = r5;	 Catch:{ all -> 0x017f }
        r0 = r49;
        r5 = r0.token;	 Catch:{ all -> 0x017f }
        r6.token = r5;	 Catch:{ all -> 0x017f }
        r5 = -1;
        r6.width = r5;	 Catch:{ all -> 0x017f }
        r5 = -1;
        r6.height = r5;	 Catch:{ all -> 0x017f }
        r0 = r19;
        r6.systemUiVisibility = r0;	 Catch:{ all -> 0x017f }
        r5 = r42.getOwningPackage();	 Catch:{ all -> 0x017f }
        r6.packageName = r5;	 Catch:{ all -> 0x017f }
        r5 = "SnapshotStartingWindow for taskId=%s";
        r8 = 1;
        r8 = new java.lang.Object[r8];	 Catch:{ all -> 0x017f }
        r0 = r44;
        r11 = r0.mTaskId;	 Catch:{ all -> 0x017f }
        r11 = java.lang.Integer.valueOf(r11);	 Catch:{ all -> 0x017f }
        r12 = 0;
        r8[r12] = r11;	 Catch:{ all -> 0x017f }
        r5 = java.lang.String.format(r5, r8);	 Catch:{ all -> 0x017f }
        r6.setTitle(r5);	 Catch:{ all -> 0x017f }
        r45 = r44.getTaskDescription();	 Catch:{ all -> 0x017f }
        if (r45 == 0) goto L_0x0136;
    L_0x012a:
        r16 = r45.getBackgroundColor();	 Catch:{ all -> 0x017f }
        r17 = r45.getStatusBarColor();	 Catch:{ all -> 0x017f }
        r18 = r45.getNavigationBarColor();	 Catch:{ all -> 0x017f }
    L_0x0136:
        r22 = new android.graphics.Rect;	 Catch:{ all -> 0x017f }
        r22.<init>();	 Catch:{ all -> 0x017f }
        r0 = r44;
        r1 = r22;
        r0.getBounds(r1);	 Catch:{ all -> 0x017f }
        r5 = r47.getConfiguration();	 Catch:{ all -> 0x017f }
        r0 = r5.orientation;	 Catch:{ all -> 0x017f }
        r23 = r0;
        monitor-exit(r7);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        r5 = r4.mSeq;	 Catch:{ RemoteException -> 0x0185 }
        r7 = r49.getDisplayContent();	 Catch:{ RemoteException -> 0x0185 }
        r8 = r7.getDisplayId();	 Catch:{ RemoteException -> 0x0185 }
        r7 = 0;
        r12 = 0;
        r10 = r9;
        r11 = r9;
        r43 = r3.addToDisplay(r4, r5, r6, r7, r8, r9, r10, r11, r12);	 Catch:{ RemoteException -> 0x0185 }
        if (r43 >= 0) goto L_0x0186;
    L_0x0162:
        r5 = TAG;	 Catch:{ RemoteException -> 0x0185 }
        r7 = new java.lang.StringBuilder;	 Catch:{ RemoteException -> 0x0185 }
        r7.<init>();	 Catch:{ RemoteException -> 0x0185 }
        r8 = "Failed to add snapshot starting window res=";
        r7 = r7.append(r8);	 Catch:{ RemoteException -> 0x0185 }
        r0 = r43;
        r7 = r7.append(r0);	 Catch:{ RemoteException -> 0x0185 }
        r7 = r7.toString();	 Catch:{ RemoteException -> 0x0185 }
        android.util.Slog.w(r5, r7);	 Catch:{ RemoteException -> 0x0185 }
        r5 = 0;
        return r5;
    L_0x017f:
        r5 = move-exception;
        monitor-exit(r7);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r5;
    L_0x0185:
        r41 = move-exception;
    L_0x0186:
        r10 = new com.android.server.wm.TaskSnapshotSurface;
        r15 = r6.getTitle();
        r11 = r48;
        r12 = r4;
        r14 = r50;
        r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23);
        r4.setOuter(r10);
        r0 = r4.mSeq;	 Catch:{ RemoteException -> 0x01c3 }
        r26 = r0;
        r28 = -1;
        r29 = -1;
        r30 = 0;
        r31 = 0;
        r24 = r3;
        r25 = r4;
        r27 = r6;
        r33 = r9;
        r35 = r9;
        r37 = r9;
        r38 = r9;
        r40 = r13;
        r24.relayout(r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35, r36, r37, r38, r39, r40);	 Catch:{ RemoteException -> 0x01c3 }
    L_0x01b6:
        r0 = r32;
        r1 = r34;
        r2 = r36;
        r10.setFrames(r0, r1, r2);
        r10.drawSnapshot();
        return r10;
    L_0x01c3:
        r41 = move-exception;
        goto L_0x01b6;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskSnapshotSurface.create(com.android.server.wm.WindowManagerService, com.android.server.wm.AppWindowToken, android.app.ActivityManager$TaskSnapshot):com.android.server.wm.TaskSnapshotSurface");
    }

    TaskSnapshotSurface(WindowManagerService service, Window window, Surface surface, TaskSnapshot snapshot, CharSequence title, int backgroundColor, int statusBarColor, int navigationBarColor, int sysUiVis, int windowFlags, int windowPrivateFlags, Rect taskBounds, int currentOrientation) {
        this.mService = service;
        this.mHandler = new Handler(this.mService.mH.getLooper());
        this.mSession = WindowManagerGlobal.getWindowSession();
        this.mWindow = window;
        this.mSurface = surface;
        this.mSnapshot = snapshot;
        this.mTitle = title;
        Paint paint = this.mBackgroundPaint;
        if (backgroundColor == 0) {
            backgroundColor = -1;
        }
        paint.setColor(backgroundColor);
        this.mTaskBounds = taskBounds;
        this.mSystemBarBackgroundPainter = new SystemBarBackgroundPainter(windowFlags, windowPrivateFlags, sysUiVis, statusBarColor, navigationBarColor);
        this.mStatusBarColor = statusBarColor;
        this.mOrientationOnCreation = currentOrientation;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void remove() {
        /*
        r10 = this;
        r8 = 450; // 0x1c2 float:6.3E-43 double:2.223E-321;
        r1 = r10.mService;
        r4 = r1.mWindowMap;
        monitor-enter(r4);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x003a }
        r2 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x003a }
        r1 = r10.mSizeMismatch;	 Catch:{ all -> 0x003a }
        if (r1 == 0) goto L_0x002e;
    L_0x0012:
        r6 = r10.mShownTime;	 Catch:{ all -> 0x003a }
        r6 = r2 - r6;
        r1 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1));
        if (r1 >= 0) goto L_0x002e;
    L_0x001a:
        r1 = r10.mHandler;	 Catch:{ all -> 0x003a }
        r5 = new com.android.server.wm.-$Lambda$aEpJ2RCAIjecjyIIYTv6ricEwh4;	 Catch:{ all -> 0x003a }
        r6 = 9;
        r5.<init>(r6, r10);	 Catch:{ all -> 0x003a }
        r6 = r10.mShownTime;	 Catch:{ all -> 0x003a }
        r6 = r6 + r8;
        r1.postAtTime(r5, r6);	 Catch:{ all -> 0x003a }
        monitor-exit(r4);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x002e:
        monitor-exit(r4);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        r1 = r10.mSession;	 Catch:{ RemoteException -> 0x0040 }
        r4 = r10.mWindow;	 Catch:{ RemoteException -> 0x0040 }
        r1.remove(r4);	 Catch:{ RemoteException -> 0x0040 }
    L_0x0039:
        return;
    L_0x003a:
        r1 = move-exception;
        monitor-exit(r4);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
    L_0x0040:
        r0 = move-exception;
        goto L_0x0039;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskSnapshotSurface.remove():void");
    }

    /* synthetic */ void -com_android_server_wm_TaskSnapshotSurface-mthref-0() {
        remove();
    }

    void setFrames(Rect frame, Rect contentInsets, Rect stableInsets) {
        boolean z = true;
        this.mFrame.set(frame);
        this.mContentInsets.set(contentInsets);
        this.mStableInsets.set(stableInsets);
        if (this.mFrame.width() == this.mSnapshot.getSnapshot().getWidth() && this.mFrame.height() == this.mSnapshot.getSnapshot().getHeight()) {
            z = false;
        }
        this.mSizeMismatch = z;
        this.mSystemBarBackgroundPainter.setInsets(contentInsets, stableInsets);
    }

    private void drawSnapshot() {
        GraphicBuffer buffer = this.mSnapshot.getSnapshot();
        if (this.mSizeMismatch) {
            drawSizeMismatchSnapshot(buffer);
        } else {
            drawSizeMatchSnapshot(buffer);
        }
        synchronized (this.mService.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                this.mShownTime = SystemClock.uptimeMillis();
                this.mHasDrawn = true;
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        reportDrawn();
        this.mSnapshot = null;
    }

    private void drawSizeMatchSnapshot(GraphicBuffer buffer) {
        this.mSurface.attachAndQueueBuffer(buffer);
        this.mSurface.release();
    }

    private void drawSizeMismatchSnapshot(GraphicBuffer buffer) {
        this.mChildSurfaceControl = new SurfaceControl(new SurfaceSession(this.mSurface), this.mTitle + " - task-snapshot-surface", buffer.getWidth(), buffer.getHeight(), buffer.getFormat(), 4);
        Surface surface = new Surface();
        surface.copyFrom(this.mChildSurfaceControl);
        Rect crop = calculateSnapshotCrop();
        Rect frame = calculateSnapshotFrame(crop);
        SurfaceControl.openTransaction();
        try {
            this.mChildSurfaceControl.show();
            this.mChildSurfaceControl.setWindowCrop(crop);
            this.mChildSurfaceControl.setPosition((float) frame.left, (float) frame.top);
            float scale = 1.0f / this.mSnapshot.getScale();
            this.mChildSurfaceControl.setMatrix(scale, 0.0f, 0.0f, scale);
            surface.attachAndQueueBuffer(buffer);
            surface.release();
            Canvas c = this.mSurface.lockCanvas(null);
            drawBackgroundAndBars(c, frame);
            this.mSurface.unlockCanvasAndPost(c);
            this.mSurface.release();
        } finally {
            SurfaceControl.closeTransaction();
        }
    }

    Rect calculateSnapshotCrop() {
        int i = 0;
        Rect rect = new Rect();
        rect.set(0, 0, this.mSnapshot.getSnapshot().getWidth(), this.mSnapshot.getSnapshot().getHeight());
        Rect insets = this.mSnapshot.getContentInsets();
        int scale = (int) (((float) insets.left) * this.mSnapshot.getScale());
        if (this.mTaskBounds.top != 0) {
            i = (int) (((float) insets.top) * this.mSnapshot.getScale());
        }
        rect.inset(scale, i, (int) (((float) insets.right) * this.mSnapshot.getScale()), (int) (((float) insets.bottom) * this.mSnapshot.getScale()));
        return rect;
    }

    Rect calculateSnapshotFrame(Rect crop) {
        Rect frame = new Rect(crop);
        float scale = this.mSnapshot.getScale();
        frame.scale(1.0f / scale);
        frame.offsetTo((int) (((float) (-crop.left)) / scale), (int) (((float) (-crop.top)) / scale));
        frame.offset(DecorView.getColorViewLeftInset(this.mStableInsets.left, this.mContentInsets.left), 0);
        return frame;
    }

    void drawBackgroundAndBars(Canvas c, Rect frame) {
        int statusBarHeight = this.mSystemBarBackgroundPainter.getStatusBarColorViewHeight();
        boolean fillHorizontally = c.getWidth() > frame.right;
        boolean fillVertically = c.getHeight() > frame.bottom;
        if (fillHorizontally) {
            int i;
            float f = (float) frame.right;
            if (Color.alpha(this.mStatusBarColor) != 255) {
                statusBarHeight = 0;
            }
            float f2 = (float) statusBarHeight;
            float width = (float) c.getWidth();
            if (fillVertically) {
                i = frame.bottom;
            } else {
                i = c.getHeight();
            }
            c.drawRect(f, f2, width, (float) i, this.mBackgroundPaint);
        }
        if (fillVertically) {
            c.drawRect(0.0f, (float) frame.bottom, (float) c.getWidth(), (float) c.getHeight(), this.mBackgroundPaint);
        }
        this.mSystemBarBackgroundPainter.drawDecors(c, frame);
    }

    private void reportDrawn() {
        try {
            this.mSession.finishDrawing(this.mWindow);
        } catch (RemoteException e) {
        }
    }
}
