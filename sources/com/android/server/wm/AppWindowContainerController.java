package com.android.server.wm;

import android.app.ActivityManager.TaskSnapshot;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Trace;
import android.util.Slog;
import android.view.IApplicationToken;
import android.view.WindowManagerPolicy.StartingSurface;

public class AppWindowContainerController extends WindowContainerController<AppWindowToken, AppWindowContainerListener> {
    private static final int STARTING_WINDOW_TYPE_NONE = 0;
    private static final int STARTING_WINDOW_TYPE_SNAPSHOT = 1;
    private static final int STARTING_WINDOW_TYPE_SPLASH_SCREEN = 2;
    private final Runnable mAddStartingWindow;
    private final Handler mHandler;
    private final Runnable mOnWindowsGone;
    private final Runnable mOnWindowsVisible;
    private final IApplicationToken mToken;

    private final class H extends Handler {
        public static final int NOTIFY_STARTING_WINDOW_DRAWN = 2;
        public static final int NOTIFY_WINDOWS_DRAWN = 1;

        public H(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (AppWindowContainerController.this.mListener != null) {
                        ((AppWindowContainerListener) AppWindowContainerController.this.mListener).onWindowsDrawn(msg.getWhen());
                        break;
                    }
                    return;
                case 2:
                    if (AppWindowContainerController.this.mListener != null) {
                        ((AppWindowContainerListener) AppWindowContainerController.this.mListener).onStartingWindowDrawn(msg.getWhen());
                        break;
                    }
                    return;
            }
        }
    }

    /* synthetic */ void lambda$-com_android_server_wm_AppWindowContainerController_4155() {
        if (this.mListener != null) {
            ((AppWindowContainerListener) this.mListener).onWindowsVisible();
        }
    }

    /* synthetic */ void lambda$-com_android_server_wm_AppWindowContainerController_4446() {
        if (this.mListener != null) {
            ((AppWindowContainerListener) this.mListener).onWindowsGone();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    /* synthetic */ void lambda$-com_android_server_wm_AppWindowContainerController_4735() {
        /*
        r7 = this;
        r6 = r7.mWindowMap;
        monitor-enter(r6);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0020 }
        r5 = r7.mContainer;	 Catch:{ all -> 0x0020 }
        if (r5 != 0) goto L_0x000f;
    L_0x000a:
        monitor-exit(r6);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x000f:
        r5 = r7.mContainer;	 Catch:{ all -> 0x0020 }
        r5 = (com.android.server.wm.AppWindowToken) r5;	 Catch:{ all -> 0x0020 }
        r3 = r5.startingData;	 Catch:{ all -> 0x0020 }
        r1 = r7.mContainer;	 Catch:{ all -> 0x0020 }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x0020 }
        monitor-exit(r6);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        if (r3 != 0) goto L_0x0026;
    L_0x001f:
        return;
    L_0x0020:
        r5 = move-exception;
        monitor-exit(r6);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r5;
    L_0x0026:
        r4 = 0;
        r4 = r3.createStartingSurface(r1);	 Catch:{ Exception -> 0x004d }
    L_0x002b:
        if (r4 == 0) goto L_0x004c;
    L_0x002d:
        r0 = 0;
        r6 = r7.mWindowMap;
        monitor-enter(r6);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x005b }
        r5 = r1.removed;	 Catch:{ all -> 0x005b }
        if (r5 != 0) goto L_0x003c;
    L_0x0038:
        r5 = r1.startingData;	 Catch:{ all -> 0x005b }
        if (r5 != 0) goto L_0x0058;
    L_0x003c:
        r5 = 0;
        r1.startingWindow = r5;	 Catch:{ all -> 0x005b }
        r5 = 0;
        r1.startingData = r5;	 Catch:{ all -> 0x005b }
        r0 = 1;
    L_0x0043:
        monitor-exit(r6);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        if (r0 == 0) goto L_0x004c;
    L_0x0049:
        r4.remove();
    L_0x004c:
        return;
    L_0x004d:
        r2 = move-exception;
        r5 = "WindowManager";
        r6 = "Exception when adding starting window";
        android.util.Slog.w(r5, r6, r2);
        goto L_0x002b;
    L_0x0058:
        r1.startingSurface = r4;	 Catch:{ all -> 0x005b }
        goto L_0x0043;
    L_0x005b:
        r5 = move-exception;
        monitor-exit(r6);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.lambda$-com_android_server_wm_AppWindowContainerController_4735():void");
    }

    public AppWindowContainerController(TaskWindowContainerController taskController, IApplicationToken token, AppWindowContainerListener listener, int index, int requestedOrientation, boolean fullscreen, boolean showForAllUsers, int configChanges, boolean voiceInteraction, boolean launchTaskBehind, boolean alwaysFocusable, int targetSdkVersion, int rotationAnimationHint, long inputDispatchingTimeoutNanos, Configuration overrideConfig, Rect bounds) {
        this(taskController, token, listener, index, requestedOrientation, fullscreen, showForAllUsers, configChanges, voiceInteraction, launchTaskBehind, alwaysFocusable, targetSdkVersion, rotationAnimationHint, inputDispatchingTimeoutNanos, WindowManagerService.getInstance(), overrideConfig, bounds);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppWindowContainerController(com.android.server.wm.TaskWindowContainerController r27, android.view.IApplicationToken r28, com.android.server.wm.AppWindowContainerListener r29, int r30, int r31, boolean r32, boolean r33, int r34, boolean r35, boolean r36, boolean r37, int r38, int r39, long r40, com.android.server.wm.WindowManagerService r42, android.content.res.Configuration r43, android.graphics.Rect r44) {
        /*
        r26 = this;
        r0 = r26;
        r1 = r29;
        r2 = r42;
        r0.<init>(r1, r2);
        r5 = new com.android.server.wm.-$Lambda$aEpJ2RCAIjecjyIIYTv6ricEwh4;
        r6 = 2;
        r0 = r26;
        r5.<init>(r6, r0);
        r0 = r26;
        r0.mOnWindowsVisible = r5;
        r5 = new com.android.server.wm.-$Lambda$aEpJ2RCAIjecjyIIYTv6ricEwh4;
        r6 = 3;
        r0 = r26;
        r5.<init>(r6, r0);
        r0 = r26;
        r0.mOnWindowsGone = r5;
        r5 = new com.android.server.wm.-$Lambda$aEpJ2RCAIjecjyIIYTv6ricEwh4;
        r6 = 4;
        r0 = r26;
        r5.<init>(r6, r0);
        r0 = r26;
        r0.mAddStartingWindow = r5;
        r5 = new com.android.server.wm.AppWindowContainerController$H;
        r0 = r42;
        r6 = r0.mH;
        r6 = r6.getLooper();
        r0 = r26;
        r5.<init>(r6);
        r0 = r26;
        r0.mHandler = r5;
        r0 = r28;
        r1 = r26;
        r1.mToken = r0;
        r0 = r26;
        r0 = r0.mWindowMap;
        r24 = r0;
        monitor-enter(r24);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x00ab }
        r0 = r26;
        r5 = r0.mRoot;	 Catch:{ all -> 0x00ab }
        r0 = r26;
        r6 = r0.mToken;	 Catch:{ all -> 0x00ab }
        r6 = r6.asBinder();	 Catch:{ all -> 0x00ab }
        r4 = r5.getAppWindowToken(r6);	 Catch:{ all -> 0x00ab }
        if (r4 == 0) goto L_0x0085;
    L_0x0062:
        r5 = "WindowManager";
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00ab }
        r6.<init>();	 Catch:{ all -> 0x00ab }
        r7 = "Attempted to add existing app token: ";
        r6 = r6.append(r7);	 Catch:{ all -> 0x00ab }
        r0 = r26;
        r7 = r0.mToken;	 Catch:{ all -> 0x00ab }
        r6 = r6.append(r7);	 Catch:{ all -> 0x00ab }
        r6 = r6.toString();	 Catch:{ all -> 0x00ab }
        android.util.Slog.w(r5, r6);	 Catch:{ all -> 0x00ab }
        monitor-exit(r24);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0085:
        r0 = r27;
        r0 = r0.mContainer;	 Catch:{ all -> 0x00ab }
        r23 = r0;
        r23 = (com.android.server.wm.Task) r23;	 Catch:{ all -> 0x00ab }
        if (r23 != 0) goto L_0x00b1;
    L_0x008f:
        r5 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x00ab }
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00ab }
        r6.<init>();	 Catch:{ all -> 0x00ab }
        r7 = "AppWindowContainerController: invalid  controller=";
        r6 = r6.append(r7);	 Catch:{ all -> 0x00ab }
        r0 = r27;
        r6 = r6.append(r0);	 Catch:{ all -> 0x00ab }
        r6 = r6.toString();	 Catch:{ all -> 0x00ab }
        r5.<init>(r6);	 Catch:{ all -> 0x00ab }
        throw r5;	 Catch:{ all -> 0x00ab }
    L_0x00ab:
        r5 = move-exception;
        monitor-exit(r24);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r5;
    L_0x00b1:
        r0 = r26;
        r6 = r0.mService;	 Catch:{ all -> 0x00ab }
        r9 = r23.getDisplayContent();	 Catch:{ all -> 0x00ab }
        r5 = r26;
        r7 = r28;
        r8 = r35;
        r10 = r40;
        r12 = r32;
        r13 = r33;
        r14 = r38;
        r15 = r31;
        r16 = r39;
        r17 = r34;
        r18 = r36;
        r19 = r37;
        r20 = r26;
        r21 = r43;
        r22 = r44;
        r4 = r5.createAppWindow(r6, r7, r8, r9, r10, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22);	 Catch:{ all -> 0x00ab }
        r0 = r23;
        r1 = r30;
        r0.addChild(r4, r1);	 Catch:{ all -> 0x00ab }
        monitor-exit(r24);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.<init>(com.android.server.wm.TaskWindowContainerController, android.view.IApplicationToken, com.android.server.wm.AppWindowContainerListener, int, int, boolean, boolean, int, boolean, boolean, boolean, int, int, long, com.android.server.wm.WindowManagerService, android.content.res.Configuration, android.graphics.Rect):void");
    }

    AppWindowToken createAppWindow(WindowManagerService service, IApplicationToken token, boolean voiceInteraction, DisplayContent dc, long inputDispatchingTimeoutNanos, boolean fullscreen, boolean showForAllUsers, int targetSdk, int orientation, int rotationAnimationHint, int configChanges, boolean launchTaskBehind, boolean alwaysFocusable, AppWindowContainerController controller, Configuration overrideConfig, Rect bounds) {
        return new AppWindowToken(service, token, voiceInteraction, dc, inputDispatchingTimeoutNanos, fullscreen, showForAllUsers, targetSdk, orientation, rotationAnimationHint, configChanges, launchTaskBehind, alwaysFocusable, controller, overrideConfig, bounds);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeContainer(int r6) {
        /*
        r5 = this;
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x004b }
        r1 = r5.mRoot;	 Catch:{ all -> 0x004b }
        r0 = r1.getDisplayContent(r6);	 Catch:{ all -> 0x004b }
        if (r0 != 0) goto L_0x003a;
    L_0x000e:
        r1 = "WindowManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x004b }
        r3.<init>();	 Catch:{ all -> 0x004b }
        r4 = "removeAppToken: Attempted to remove binder token: ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x004b }
        r4 = r5.mToken;	 Catch:{ all -> 0x004b }
        r3 = r3.append(r4);	 Catch:{ all -> 0x004b }
        r4 = " from non-existing displayId=";
        r3 = r3.append(r4);	 Catch:{ all -> 0x004b }
        r3 = r3.append(r6);	 Catch:{ all -> 0x004b }
        r3 = r3.toString();	 Catch:{ all -> 0x004b }
        android.util.Slog.w(r1, r3);	 Catch:{ all -> 0x004b }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x003a:
        r1 = r5.mToken;	 Catch:{ all -> 0x004b }
        r1 = r1.asBinder();	 Catch:{ all -> 0x004b }
        r0.removeAppToken(r1);	 Catch:{ all -> 0x004b }
        super.removeContainer();	 Catch:{ all -> 0x004b }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x004b:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.removeContainer(int):void");
    }

    public void removeContainer() {
        throw new UnsupportedOperationException("Use removeContainer(displayId) instead.");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void reparent(com.android.server.wm.TaskWindowContainerController r6, int r7) {
        /*
        r5 = this;
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x002f }
        r1 = r5.mContainer;	 Catch:{ all -> 0x002f }
        if (r1 != 0) goto L_0x000f;
    L_0x000a:
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x000f:
        r0 = r6.mContainer;	 Catch:{ all -> 0x002f }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x002f }
        if (r0 != 0) goto L_0x0035;
    L_0x0015:
        r1 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x002f }
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x002f }
        r3.<init>();	 Catch:{ all -> 0x002f }
        r4 = "reparent: could not find task=";
        r3 = r3.append(r4);	 Catch:{ all -> 0x002f }
        r3 = r3.append(r6);	 Catch:{ all -> 0x002f }
        r3 = r3.toString();	 Catch:{ all -> 0x002f }
        r1.<init>(r3);	 Catch:{ all -> 0x002f }
        throw r1;	 Catch:{ all -> 0x002f }
    L_0x002f:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
    L_0x0035:
        r1 = r5.mContainer;	 Catch:{ all -> 0x002f }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x002f }
        r1.reparent(r0, r7);	 Catch:{ all -> 0x002f }
        r1 = r5.mContainer;	 Catch:{ all -> 0x002f }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x002f }
        r1 = r1.getDisplayContent();	 Catch:{ all -> 0x002f }
        r1.layoutAndAssignWindowLayersIfNeeded();	 Catch:{ all -> 0x002f }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.reparent(com.android.server.wm.TaskWindowContainerController, int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.content.res.Configuration setOrientation(int r7, int r8, android.content.res.Configuration r9, boolean r10) {
        /*
        r6 = this;
        r5 = 0;
        r2 = r6.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0048 }
        r1 = r6.mContainer;	 Catch:{ all -> 0x0048 }
        if (r1 != 0) goto L_0x002c;
    L_0x000b:
        r1 = "WindowManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0048 }
        r3.<init>();	 Catch:{ all -> 0x0048 }
        r4 = "Attempted to set orientation of non-existing app token: ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x0048 }
        r4 = r6.mToken;	 Catch:{ all -> 0x0048 }
        r3 = r3.append(r4);	 Catch:{ all -> 0x0048 }
        r3 = r3.toString();	 Catch:{ all -> 0x0048 }
        android.util.Slog.w(r1, r3);	 Catch:{ all -> 0x0048 }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r5;
    L_0x002c:
        r1 = r6.mContainer;	 Catch:{ all -> 0x0048 }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x0048 }
        r1.setOrientation(r7);	 Catch:{ all -> 0x0048 }
        if (r10 == 0) goto L_0x0046;
    L_0x0035:
        r1 = r6.mToken;	 Catch:{ all -> 0x0048 }
        r0 = r1.asBinder();	 Catch:{ all -> 0x0048 }
    L_0x003b:
        r1 = r6.mService;	 Catch:{ all -> 0x0048 }
        r1 = r1.updateOrientationFromAppTokens(r9, r0, r8);	 Catch:{ all -> 0x0048 }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r1;
    L_0x0046:
        r0 = 0;
        goto L_0x003b;
    L_0x0048:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.setOrientation(int, int, android.content.res.Configuration, boolean):android.content.res.Configuration");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getOrientation() {
        /*
        r2 = this;
        r1 = r2.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x001d }
        r0 = r2.mContainer;	 Catch:{ all -> 0x001d }
        if (r0 != 0) goto L_0x0010;
    L_0x000a:
        r0 = -1;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r0;
    L_0x0010:
        r0 = r2.mContainer;	 Catch:{ all -> 0x001d }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x001d }
        r0 = r0.getOrientationIgnoreVisibility();	 Catch:{ all -> 0x001d }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r0;
    L_0x001d:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.getOrientation():int");
    }

    public void onOverrideConfigurationChanged(Configuration overrideConfiguration, Rect bounds) {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer != null) {
                    ((AppWindowToken) this.mContainer).onOverrideConfigurationChanged(overrideConfiguration, bounds);
                }
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setDisablePreviewScreenshots(boolean r5) {
        /*
        r4 = this;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0037 }
        r0 = r4.mContainer;	 Catch:{ all -> 0x0037 }
        if (r0 != 0) goto L_0x002b;
    L_0x000a:
        r0 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0037 }
        r2.<init>();	 Catch:{ all -> 0x0037 }
        r3 = "Attempted to set disable screenshots of non-existing app token: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0037 }
        r3 = r4.mToken;	 Catch:{ all -> 0x0037 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0037 }
        r2 = r2.toString();	 Catch:{ all -> 0x0037 }
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x0037 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x002b:
        r0 = r4.mContainer;	 Catch:{ all -> 0x0037 }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x0037 }
        r0.setDisablePreviewScreenshots(r5);	 Catch:{ all -> 0x0037 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0037:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.setDisablePreviewScreenshots(boolean):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setVisibility(boolean r10, boolean r11) {
        /*
        r9 = this;
        r8 = r9.mWindowMap;
        monitor-enter(r8);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0133 }
        r1 = r9.mContainer;	 Catch:{ all -> 0x0133 }
        if (r1 != 0) goto L_0x002b;
    L_0x000a:
        r1 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0133 }
        r2.<init>();	 Catch:{ all -> 0x0133 }
        r3 = "Attempted to set visibility of non-existing app token: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0133 }
        r3 = r9.mToken;	 Catch:{ all -> 0x0133 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0133 }
        r2 = r2.toString();	 Catch:{ all -> 0x0133 }
        android.util.Slog.w(r1, r2);	 Catch:{ all -> 0x0133 }
        monitor-exit(r8);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x002b:
        r0 = r9.mContainer;	 Catch:{ all -> 0x0133 }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x0133 }
        if (r10 != 0) goto L_0x0046;
    L_0x0031:
        r1 = r0.hiddenRequested;	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x0046;
    L_0x0035:
        if (r11 != 0) goto L_0x0041;
    L_0x0037:
        r1 = r0.mDeferHidingClient;	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x0041;
    L_0x003b:
        r0.mDeferHidingClient = r11;	 Catch:{ all -> 0x0133 }
        r1 = 1;
        r0.setClientHidden(r1);	 Catch:{ all -> 0x0133 }
    L_0x0041:
        monitor-exit(r8);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0046:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mOpeningApps;	 Catch:{ all -> 0x0133 }
        r1.remove(r0);	 Catch:{ all -> 0x0133 }
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mClosingApps;	 Catch:{ all -> 0x0133 }
        r1.remove(r0);	 Catch:{ all -> 0x0133 }
        r1 = 0;
        r0.waitingToShow = r1;	 Catch:{ all -> 0x0133 }
        r1 = r10 ^ 1;
        r0.hiddenRequested = r1;	 Catch:{ all -> 0x0133 }
        r0.mDeferHidingClient = r11;	 Catch:{ all -> 0x0133 }
        if (r10 != 0) goto L_0x00f1;
    L_0x005f:
        r0.removeDeadWindows();	 Catch:{ all -> 0x0133 }
        r0.setVisibleBeforeClientHidden();	 Catch:{ all -> 0x0133 }
    L_0x0065:
        r1 = r0.okToAnimate();	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x0144;
    L_0x006b:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mAppTransition;	 Catch:{ all -> 0x0133 }
        r1 = r1.isTransitionSet();	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x0144;
    L_0x0075:
        r1 = r0.mAppAnimator;	 Catch:{ all -> 0x0133 }
        r1 = r1.usingTransferredAnimation;	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x00a2;
    L_0x007b:
        r1 = r0.mAppAnimator;	 Catch:{ all -> 0x0133 }
        r1 = r1.animation;	 Catch:{ all -> 0x0133 }
        if (r1 != 0) goto L_0x00a2;
    L_0x0081:
        r1 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0133 }
        r2.<init>();	 Catch:{ all -> 0x0133 }
        r3 = "Will NOT set dummy animation on: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0133 }
        r2 = r2.append(r0);	 Catch:{ all -> 0x0133 }
        r3 = ", using null transferred animation!";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0133 }
        r2 = r2.toString();	 Catch:{ all -> 0x0133 }
        android.util.Slog.wtf(r1, r2);	 Catch:{ all -> 0x0133 }
    L_0x00a2:
        r1 = r0.mAppAnimator;	 Catch:{ all -> 0x0133 }
        r1 = r1.usingTransferredAnimation;	 Catch:{ all -> 0x0133 }
        if (r1 != 0) goto L_0x00b7;
    L_0x00a8:
        r1 = r0.startingDisplayed;	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x00b2;
    L_0x00ac:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mSkipAppTransitionAnimation;	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x00b7;
    L_0x00b2:
        r1 = r0.mAppAnimator;	 Catch:{ all -> 0x0133 }
        r1.setDummyAnimation();	 Catch:{ all -> 0x0133 }
    L_0x00b7:
        r1 = 1;
        r0.inPendingTransaction = r1;	 Catch:{ all -> 0x0133 }
        if (r10 == 0) goto L_0x0139;
    L_0x00bc:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mOpeningApps;	 Catch:{ all -> 0x0133 }
        r1.add(r0);	 Catch:{ all -> 0x0133 }
        r1 = 1;
        r0.mEnteringAnimation = r1;	 Catch:{ all -> 0x0133 }
    L_0x00c6:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mAppTransition;	 Catch:{ all -> 0x0133 }
        r1 = r1.getAppTransition();	 Catch:{ all -> 0x0133 }
        r2 = 16;
        if (r1 != r2) goto L_0x00ec;
    L_0x00d2:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.getDefaultDisplayContentLocked();	 Catch:{ all -> 0x0133 }
        r7 = r1.findFocusedWindow();	 Catch:{ all -> 0x0133 }
        if (r7 == 0) goto L_0x00ec;
    L_0x00de:
        r6 = r7.mAppToken;	 Catch:{ all -> 0x0133 }
        if (r6 == 0) goto L_0x00ec;
    L_0x00e2:
        r1 = 1;
        r6.hidden = r1;	 Catch:{ all -> 0x0133 }
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mOpeningApps;	 Catch:{ all -> 0x0133 }
        r1.add(r6);	 Catch:{ all -> 0x0133 }
    L_0x00ec:
        monitor-exit(r8);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x00f1:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mAppTransition;	 Catch:{ all -> 0x0133 }
        r1 = r1.isTransitionSet();	 Catch:{ all -> 0x0133 }
        if (r1 != 0) goto L_0x010c;
    L_0x00fb:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mAppTransition;	 Catch:{ all -> 0x0133 }
        r1 = r1.isReady();	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x010c;
    L_0x0105:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mOpeningApps;	 Catch:{ all -> 0x0133 }
        r1.add(r0);	 Catch:{ all -> 0x0133 }
    L_0x010c:
        r1 = 0;
        r0.startingMoved = r1;	 Catch:{ all -> 0x0133 }
        r1 = r0.hidden;	 Catch:{ all -> 0x0133 }
        if (r1 != 0) goto L_0x0117;
    L_0x0113:
        r1 = r0.mAppStopped;	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x012b;
    L_0x0117:
        r0.clearAllDrawn();	 Catch:{ all -> 0x0133 }
        r1 = r0.hidden;	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x0121;
    L_0x011e:
        r1 = 1;
        r0.waitingToShow = r1;	 Catch:{ all -> 0x0133 }
    L_0x0121:
        r1 = r0.isClientHidden();	 Catch:{ all -> 0x0133 }
        if (r1 == 0) goto L_0x012b;
    L_0x0127:
        r1 = 0;
        r0.setClientHidden(r1);	 Catch:{ all -> 0x0133 }
    L_0x012b:
        r0.requestUpdateWallpaperIfNeeded();	 Catch:{ all -> 0x0133 }
        r1 = 0;
        r0.mAppStopped = r1;	 Catch:{ all -> 0x0133 }
        goto L_0x0065;
    L_0x0133:
        r1 = move-exception;
        monitor-exit(r8);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
    L_0x0139:
        r1 = r9.mService;	 Catch:{ all -> 0x0133 }
        r1 = r1.mClosingApps;	 Catch:{ all -> 0x0133 }
        r1.add(r0);	 Catch:{ all -> 0x0133 }
        r1 = 0;
        r0.mEnteringAnimation = r1;	 Catch:{ all -> 0x0133 }
        goto L_0x00c6;
    L_0x0144:
        r5 = r0.mVoiceInteraction;	 Catch:{ all -> 0x0133 }
        r1 = 0;
        r3 = -1;
        r4 = 1;
        r2 = r10;
        r0.setVisibility(r1, r2, r3, r4, r5);	 Catch:{ all -> 0x0133 }
        r0.updateReportedVisibilityLocked();	 Catch:{ all -> 0x0133 }
        monitor-exit(r8);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.setVisibility(boolean, boolean):void");
    }

    public void notifyUnknownVisibilityLaunched() {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer != null) {
                    this.mService.mUnknownAppVisibilityController.notifyLaunched((AppWindowToken) this.mContainer);
                }
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean addStartingWindow(java.lang.String r30, int r31, android.content.res.CompatibilityInfo r32, java.lang.CharSequence r33, int r34, int r35, int r36, int r37, android.os.IBinder r38, boolean r39, boolean r40, boolean r41, boolean r42, boolean r43, boolean r44) {
        /*
        r29 = this;
        r0 = r29;
        r0 = r0.mWindowMap;
        r28 = r0;
        monitor-enter(r28);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0183 }
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        if (r2 != 0) goto L_0x0034;
    L_0x0010:
        r2 = "WindowManager";
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r3.<init>();	 Catch:{ all -> 0x0183 }
        r4 = "Attempted to set icon of non-existing app token: ";
        r3 = r3.append(r4);	 Catch:{ all -> 0x0183 }
        r0 = r29;
        r4 = r0.mToken;	 Catch:{ all -> 0x0183 }
        r3 = r3.append(r4);	 Catch:{ all -> 0x0183 }
        r3 = r3.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.w(r2, r3);	 Catch:{ all -> 0x0183 }
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x0034:
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r2 = (com.android.server.wm.AppWindowToken) r2;	 Catch:{ all -> 0x0183 }
        r2 = r2.okToDisplay();	 Catch:{ all -> 0x0183 }
        if (r2 != 0) goto L_0x0046;
    L_0x0040:
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x0046:
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r2 = (com.android.server.wm.AppWindowToken) r2;	 Catch:{ all -> 0x0183 }
        r2 = r2.startingData;	 Catch:{ all -> 0x0183 }
        if (r2 == 0) goto L_0x0056;
    L_0x0050:
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x0056:
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r2 = (com.android.server.wm.AppWindowToken) r2;	 Catch:{ all -> 0x0183 }
        r22 = r2.findMainWindow();	 Catch:{ all -> 0x0183 }
        if (r22 == 0) goto L_0x0072;
    L_0x0062:
        r0 = r22;
        r2 = r0.mWinAnimator;	 Catch:{ all -> 0x0183 }
        r2 = r2.getShown();	 Catch:{ all -> 0x0183 }
        if (r2 == 0) goto L_0x0072;
    L_0x006c:
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x0072:
        r0 = r29;
        r2 = r0.mService;	 Catch:{ all -> 0x0183 }
        r3 = r2.mTaskSnapshotController;	 Catch:{ all -> 0x0183 }
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r2 = (com.android.server.wm.AppWindowToken) r2;	 Catch:{ all -> 0x0183 }
        r2 = r2.getTask();	 Catch:{ all -> 0x0183 }
        r4 = r2.mTaskId;	 Catch:{ all -> 0x0183 }
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r2 = (com.android.server.wm.AppWindowToken) r2;	 Catch:{ all -> 0x0183 }
        r2 = r2.getTask();	 Catch:{ all -> 0x0183 }
        r2 = r2.mUserId;	 Catch:{ all -> 0x0183 }
        r5 = 0;
        r6 = 0;
        r9 = r3.getSnapshot(r4, r2, r5, r6);	 Catch:{ all -> 0x0183 }
        r2 = r29;
        r3 = r39;
        r4 = r40;
        r5 = r41;
        r6 = r42;
        r7 = r43;
        r8 = r44;
        r23 = r2.getStartingWindowType(r3, r4, r5, r6, r7, r8, r9);	 Catch:{ all -> 0x0183 }
        r2 = 1;
        r0 = r23;
        if (r0 != r2) goto L_0x00b8;
    L_0x00ad:
        r0 = r29;
        r2 = r0.createSnapshot(r9);	 Catch:{ all -> 0x0183 }
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x00b8:
        if (r31 == 0) goto L_0x012a;
    L_0x00ba:
        r2 = com.android.server.AttributeCache.instance();	 Catch:{ all -> 0x0183 }
        r3 = com.android.internal.R.styleable.Window;	 Catch:{ all -> 0x0183 }
        r0 = r29;
        r4 = r0.mService;	 Catch:{ all -> 0x0183 }
        r4 = r4.mCurrentUserId;	 Catch:{ all -> 0x0183 }
        r0 = r30;
        r1 = r31;
        r21 = r2.get(r0, r1, r3, r4);	 Catch:{ all -> 0x0183 }
        if (r21 != 0) goto L_0x00d6;
    L_0x00d0:
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x00d6:
        r0 = r21;
        r2 = r0.array;	 Catch:{ all -> 0x0183 }
        r3 = 5;
        r4 = 0;
        r26 = r2.getBoolean(r3, r4);	 Catch:{ all -> 0x0183 }
        r0 = r21;
        r2 = r0.array;	 Catch:{ all -> 0x0183 }
        r3 = 4;
        r4 = 0;
        r25 = r2.getBoolean(r3, r4);	 Catch:{ all -> 0x0183 }
        r0 = r21;
        r2 = r0.array;	 Catch:{ all -> 0x0183 }
        r3 = 14;
        r4 = 0;
        r27 = r2.getBoolean(r3, r4);	 Catch:{ all -> 0x0183 }
        r0 = r21;
        r2 = r0.array;	 Catch:{ all -> 0x0183 }
        r3 = 12;
        r4 = 0;
        r24 = r2.getBoolean(r3, r4);	 Catch:{ all -> 0x0183 }
        if (r26 == 0) goto L_0x0108;
    L_0x0102:
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x0108:
        if (r25 != 0) goto L_0x010c;
    L_0x010a:
        if (r24 == 0) goto L_0x0112;
    L_0x010c:
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x0112:
        if (r27 == 0) goto L_0x012a;
    L_0x0114:
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r2 = (com.android.server.wm.AppWindowToken) r2;	 Catch:{ all -> 0x0183 }
        r2 = r2.getDisplayContent();	 Catch:{ all -> 0x0183 }
        r2 = r2.mWallpaperController;	 Catch:{ all -> 0x0183 }
        r2 = r2.getWallpaperTarget();	 Catch:{ all -> 0x0183 }
        if (r2 != 0) goto L_0x013e;
    L_0x0126:
        r2 = 1048576; // 0x100000 float:1.469368E-39 double:5.180654E-318;
        r37 = r37 | r2;
    L_0x012a:
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r2 = (com.android.server.wm.AppWindowToken) r2;	 Catch:{ all -> 0x0183 }
        r0 = r38;
        r2 = r2.transferStartingWindow(r0);	 Catch:{ all -> 0x0183 }
        if (r2 == 0) goto L_0x0144;
    L_0x0138:
        r2 = 1;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x013e:
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x0144:
        r2 = 2;
        r0 = r23;
        if (r0 == r2) goto L_0x014f;
    L_0x0149:
        r2 = 0;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x014f:
        r0 = r29;
        r2 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r2 = (com.android.server.wm.AppWindowToken) r2;	 Catch:{ all -> 0x0183 }
        r10 = new com.android.server.wm.SplashScreenStartingData;	 Catch:{ all -> 0x0183 }
        r0 = r29;
        r11 = r0.mService;	 Catch:{ all -> 0x0183 }
        r0 = r29;
        r3 = r0.mContainer;	 Catch:{ all -> 0x0183 }
        r3 = (com.android.server.wm.AppWindowToken) r3;	 Catch:{ all -> 0x0183 }
        r20 = r3.getMergedOverrideConfiguration();	 Catch:{ all -> 0x0183 }
        r12 = r30;
        r13 = r31;
        r14 = r32;
        r15 = r33;
        r16 = r34;
        r17 = r35;
        r18 = r36;
        r19 = r37;
        r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20);	 Catch:{ all -> 0x0183 }
        r2.startingData = r10;	 Catch:{ all -> 0x0183 }
        r29.scheduleAddStartingWindow();	 Catch:{ all -> 0x0183 }
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        r2 = 1;
        return r2;
    L_0x0183:
        r2 = move-exception;
        monitor-exit(r28);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.addStartingWindow(java.lang.String, int, android.content.res.CompatibilityInfo, java.lang.CharSequence, int, int, int, int, android.os.IBinder, boolean, boolean, boolean, boolean, boolean, boolean):boolean");
    }

    private int getStartingWindowType(boolean newTask, boolean taskSwitch, boolean processRunning, boolean allowTaskSnapshot, boolean activityCreated, boolean fromRecents, TaskSnapshot snapshot) {
        int i = 0;
        if (this.mService.mAppTransition.getAppTransition() == 19) {
            return 0;
        }
        if (newTask || (processRunning ^ 1) != 0 || (taskSwitch && (activityCreated ^ 1) != 0)) {
            return 2;
        }
        if (!taskSwitch || !allowTaskSnapshot) {
            return 0;
        }
        if (snapshot != null) {
            i = (snapshotOrientationSameAsTask(snapshot) || fromRecents) ? 1 : 2;
        }
        return i;
    }

    void scheduleAddStartingWindow() {
        this.mService.mAnimationHandler.postAtFrontOfQueue(this.mAddStartingWindow);
    }

    private boolean createSnapshot(TaskSnapshot snapshot) {
        if (snapshot == null) {
            return false;
        }
        ((AppWindowToken) this.mContainer).startingData = new SnapshotStartingData(this.mService, snapshot);
        scheduleAddStartingWindow();
        return true;
    }

    private boolean snapshotOrientationSameAsTask(TaskSnapshot snapshot) {
        if (snapshot == null) {
            return false;
        }
        return ((AppWindowToken) this.mContainer).getTask().getConfiguration().orientation == snapshot.getOrientation();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeStartingWindow() {
        /*
        r5 = this;
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x006a }
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r1 = r1.startingWindow;	 Catch:{ all -> 0x006a }
        if (r1 != 0) goto L_0x0022;
    L_0x000e:
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r1 = r1.startingData;	 Catch:{ all -> 0x006a }
        if (r1 == 0) goto L_0x001d;
    L_0x0016:
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r3 = 0;
        r1.startingData = r3;	 Catch:{ all -> 0x006a }
    L_0x001d:
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0022:
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r1 = r1.startingData;	 Catch:{ all -> 0x006a }
        if (r1 == 0) goto L_0x0053;
    L_0x002a:
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r0 = r1.startingSurface;	 Catch:{ all -> 0x006a }
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r3 = 0;
        r1.startingData = r3;	 Catch:{ all -> 0x006a }
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r3 = 0;
        r1.startingSurface = r3;	 Catch:{ all -> 0x006a }
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r3 = 0;
        r1.startingWindow = r3;	 Catch:{ all -> 0x006a }
        r1 = r5.mContainer;	 Catch:{ all -> 0x006a }
        r1 = (com.android.server.wm.AppWindowToken) r1;	 Catch:{ all -> 0x006a }
        r3 = 0;
        r1.startingDisplayed = r3;	 Catch:{ all -> 0x006a }
        if (r0 != 0) goto L_0x0058;
    L_0x004e:
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0053:
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0058:
        r1 = r5.mService;	 Catch:{ all -> 0x006a }
        r1 = r1.mAnimationHandler;	 Catch:{ all -> 0x006a }
        r3 = new com.android.server.wm.-$Lambda$aEpJ2RCAIjecjyIIYTv6ricEwh4;	 Catch:{ all -> 0x006a }
        r4 = 5;
        r3.<init>(r4, r0);	 Catch:{ all -> 0x006a }
        r1.post(r3);	 Catch:{ all -> 0x006a }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x006a:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.removeStartingWindow():void");
    }

    static /* synthetic */ void lambda$-com_android_server_wm_AppWindowContainerController_31045(StartingSurface surface) {
        try {
            surface.remove();
        } catch (Exception e) {
            Slog.w("WindowManager", "Exception when removing starting window", e);
        }
    }

    public void pauseKeyDispatching() {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer != null) {
                    this.mService.mInputMonitor.pauseDispatchingLw((WindowToken) this.mContainer);
                }
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public void resumeKeyDispatching() {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer != null) {
                    this.mService.mInputMonitor.resumeDispatchingLw((WindowToken) this.mContainer);
                }
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void notifyAppResumed(boolean r5) {
        /*
        r4 = this;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0037 }
        r0 = r4.mContainer;	 Catch:{ all -> 0x0037 }
        if (r0 != 0) goto L_0x002b;
    L_0x000a:
        r0 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0037 }
        r2.<init>();	 Catch:{ all -> 0x0037 }
        r3 = "Attempted to notify resumed of non-existing app token: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0037 }
        r3 = r4.mToken;	 Catch:{ all -> 0x0037 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0037 }
        r2 = r2.toString();	 Catch:{ all -> 0x0037 }
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x0037 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x002b:
        r0 = r4.mContainer;	 Catch:{ all -> 0x0037 }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x0037 }
        r0.notifyAppResumed(r5);	 Catch:{ all -> 0x0037 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0037:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.notifyAppResumed(boolean):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void notifyAppStopped() {
        /*
        r4 = this;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0037 }
        r0 = r4.mContainer;	 Catch:{ all -> 0x0037 }
        if (r0 != 0) goto L_0x002b;
    L_0x000a:
        r0 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0037 }
        r2.<init>();	 Catch:{ all -> 0x0037 }
        r3 = "Attempted to notify stopped of non-existing app token: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0037 }
        r3 = r4.mToken;	 Catch:{ all -> 0x0037 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0037 }
        r2 = r2.toString();	 Catch:{ all -> 0x0037 }
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x0037 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x002b:
        r0 = r4.mContainer;	 Catch:{ all -> 0x0037 }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x0037 }
        r0.notifyAppStopped();	 Catch:{ all -> 0x0037 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0037:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.notifyAppStopped():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void startFreezingScreen(int r5) {
        /*
        r4 = this;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0048 }
        r0 = r4.mContainer;	 Catch:{ all -> 0x0048 }
        if (r0 != 0) goto L_0x002b;
    L_0x000a:
        r0 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0048 }
        r2.<init>();	 Catch:{ all -> 0x0048 }
        r3 = "Attempted to freeze screen with non-existing app token: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0048 }
        r3 = r4.mContainer;	 Catch:{ all -> 0x0048 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0048 }
        r2 = r2.toString();	 Catch:{ all -> 0x0048 }
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x0048 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x002b:
        if (r5 != 0) goto L_0x003c;
    L_0x002d:
        r0 = r4.mContainer;	 Catch:{ all -> 0x0048 }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x0048 }
        r0 = r0.okToDisplay();	 Catch:{ all -> 0x0048 }
        if (r0 == 0) goto L_0x003c;
    L_0x0037:
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x003c:
        r0 = r4.mContainer;	 Catch:{ all -> 0x0048 }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x0048 }
        r0.startFreezingScreen();	 Catch:{ all -> 0x0048 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0048:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.startFreezingScreen(int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void stopFreezingScreen(boolean r4) {
        /*
        r3 = this;
        r1 = r3.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x001c }
        r0 = r3.mContainer;	 Catch:{ all -> 0x001c }
        if (r0 != 0) goto L_0x000f;
    L_0x000a:
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x000f:
        r0 = r3.mContainer;	 Catch:{ all -> 0x001c }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x001c }
        r2 = 1;
        r0.stopFreezingScreen(r2, r4);	 Catch:{ all -> 0x001c }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x001c:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.AppWindowContainerController.stopFreezingScreen(boolean):void");
    }

    public Bitmap screenshotApplications(int displayId, int width, int height, float frameScale) {
        try {
            Trace.traceBegin(32, "screenshotApplications");
            synchronized (this.mWindowMap) {
                WindowManagerService.boostPriorityForLockedSection();
                DisplayContent dc = this.mRoot.getDisplayContentOrCreate(displayId);
                if (dc == null) {
                    WindowManagerService.resetPriorityAfterLockedSection();
                    Trace.traceEnd(32);
                    return null;
                }
                WindowManagerService.resetPriorityAfterLockedSection();
                Bitmap screenshotApplications = dc.screenshotApplications(this.mToken.asBinder(), width, height, false, frameScale, Config.RGB_565, false, false);
                Trace.traceEnd(32);
                return screenshotApplications;
            }
        } catch (Throwable th) {
            Trace.traceEnd(32);
        }
    }

    void reportStartingWindowDrawn() {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2));
    }

    void reportWindowsDrawn() {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(1));
    }

    void reportWindowsVisible() {
        this.mHandler.post(this.mOnWindowsVisible);
    }

    void reportWindowsGone() {
        this.mHandler.post(this.mOnWindowsGone);
    }

    boolean keyDispatchingTimedOut(String reason, int windowPid) {
        return this.mListener != null ? ((AppWindowContainerListener) this.mListener).keyDispatchingTimedOut(reason, windowPid) : false;
    }

    public String toString() {
        return "AppWindowContainerController{ token=" + this.mToken + " mContainer=" + this.mContainer + " mListener=" + this.mListener + "}";
    }
}
