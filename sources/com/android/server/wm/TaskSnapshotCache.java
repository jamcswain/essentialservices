package com.android.server.wm;

import android.app.ActivityManager.TaskSnapshot;
import android.util.ArrayMap;
import java.io.PrintWriter;

class TaskSnapshotCache {
    private final ArrayMap<AppWindowToken, Integer> mAppTaskMap = new ArrayMap();
    private final TaskSnapshotLoader mLoader;
    private final ArrayMap<Integer, CacheEntry> mRunningCache = new ArrayMap();
    private final WindowManagerService mService;

    private static final class CacheEntry {
        final TaskSnapshot snapshot;
        final AppWindowToken topApp;

        CacheEntry(TaskSnapshot snapshot, AppWindowToken topApp) {
            this.snapshot = snapshot;
            this.topApp = topApp;
        }
    }

    TaskSnapshotCache(WindowManagerService service, TaskSnapshotLoader loader) {
        this.mService = service;
        this.mLoader = loader;
    }

    void putSnapshot(Task task, TaskSnapshot snapshot) {
        CacheEntry entry = (CacheEntry) this.mRunningCache.get(Integer.valueOf(task.mTaskId));
        if (entry != null) {
            this.mAppTaskMap.remove(entry.topApp);
        }
        this.mAppTaskMap.put((AppWindowToken) task.getTopChild(), Integer.valueOf(task.mTaskId));
        this.mRunningCache.put(Integer.valueOf(task.mTaskId), new CacheEntry(snapshot, (AppWindowToken) task.getTopChild()));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    android.app.ActivityManager.TaskSnapshot getSnapshot(int r6, int r7, boolean r8, boolean r9) {
        /*
        r5 = this;
        r4 = 0;
        r1 = r5.mService;
        r2 = r1.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0025 }
        r1 = r5.mRunningCache;	 Catch:{ all -> 0x0025 }
        r3 = java.lang.Integer.valueOf(r6);	 Catch:{ all -> 0x0025 }
        r0 = r1.get(r3);	 Catch:{ all -> 0x0025 }
        r0 = (com.android.server.wm.TaskSnapshotCache.CacheEntry) r0;	 Catch:{ all -> 0x0025 }
        if (r0 == 0) goto L_0x001e;
    L_0x0017:
        r1 = r0.snapshot;	 Catch:{ all -> 0x0025 }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r1;
    L_0x001e:
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        if (r8 != 0) goto L_0x002b;
    L_0x0024:
        return r4;
    L_0x0025:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
    L_0x002b:
        r1 = r5.tryRestoreFromDisk(r6, r7, r9);
        return r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskSnapshotCache.getSnapshot(int, int, boolean, boolean):android.app.ActivityManager$TaskSnapshot");
    }

    private TaskSnapshot tryRestoreFromDisk(int taskId, int userId, boolean reducedResolution) {
        TaskSnapshot snapshot = this.mLoader.loadTask(taskId, userId, reducedResolution);
        if (snapshot == null) {
            return null;
        }
        return snapshot;
    }

    void onAppRemoved(AppWindowToken wtoken) {
        Integer taskId = (Integer) this.mAppTaskMap.get(wtoken);
        if (taskId != null) {
            removeRunningEntry(taskId.intValue());
        }
    }

    void onAppDied(AppWindowToken wtoken) {
        Integer taskId = (Integer) this.mAppTaskMap.get(wtoken);
        if (taskId != null) {
            removeRunningEntry(taskId.intValue());
        }
    }

    void onTaskRemoved(int taskId) {
        removeRunningEntry(taskId);
    }

    private void removeRunningEntry(int taskId) {
        CacheEntry entry = (CacheEntry) this.mRunningCache.get(Integer.valueOf(taskId));
        if (entry != null) {
            this.mAppTaskMap.remove(entry.topApp);
            this.mRunningCache.remove(Integer.valueOf(taskId));
        }
    }

    void dump(PrintWriter pw, String prefix) {
        String doublePrefix = prefix + "  ";
        String triplePrefix = doublePrefix + "  ";
        pw.println(prefix + "SnapshotCache");
        for (int i = this.mRunningCache.size() - 1; i >= 0; i--) {
            CacheEntry entry = (CacheEntry) this.mRunningCache.valueAt(i);
            pw.println(doublePrefix + "Entry taskId=" + this.mRunningCache.keyAt(i));
            pw.println(triplePrefix + "topApp=" + entry.topApp);
            pw.println(triplePrefix + "snapshot=" + entry.snapshot);
        }
    }
}
