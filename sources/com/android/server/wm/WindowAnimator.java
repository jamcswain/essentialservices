package com.android.server.wm;

import android.content.Context;
import android.util.SparseArray;
import android.util.TimeUtils;
import android.view.Choreographer;
import android.view.Choreographer.FrameCallback;
import android.view.WindowManagerPolicy;
import com.android.server.AnimationThread;
import java.io.PrintWriter;

public class WindowAnimator {
    private static final String TAG = "WindowManager";
    int mAnimTransactionSequence;
    private boolean mAnimating;
    final FrameCallback mAnimationFrameCallback;
    private boolean mAnimationFrameCallbackScheduled;
    boolean mAppWindowAnimating;
    int mBulkUpdateParams = 0;
    private Choreographer mChoreographer;
    final Context mContext;
    long mCurrentTime;
    SparseArray<DisplayContentsAnimator> mDisplayContentsAnimators = new SparseArray(2);
    boolean mInitialized = false;
    private boolean mLastAnimating;
    Object mLastWindowFreezeSource;
    final WindowManagerPolicy mPolicy;
    private boolean mRemoveReplacedWindows = false;
    final WindowManagerService mService;
    WindowState mWindowDetachedWallpaper = null;
    private final WindowSurfacePlacer mWindowPlacerLocked;

    private class DisplayContentsAnimator {
        ScreenRotationAnimation mScreenRotationAnimation;

        private DisplayContentsAnimator() {
            this.mScreenRotationAnimation = null;
        }
    }

    WindowAnimator(WindowManagerService service) {
        this.mService = service;
        this.mContext = service.mContext;
        this.mPolicy = service.mPolicy;
        this.mWindowPlacerLocked = service.mWindowPlacerLocked;
        AnimationThread.getHandler().runWithScissors(new -$Lambda$aEpJ2RCAIjecjyIIYTv6ricEwh4((byte) 11, this), 0);
        this.mAnimationFrameCallback = new -$Lambda$OQfQhd_xsxt9hoLAjIbVfOwa-jY(this);
    }

    /* synthetic */ void lambda$-com_android_server_wm_WindowAnimator_3844() {
        this.mChoreographer = Choreographer.getSfInstance();
    }

    /* synthetic */ void lambda$-com_android_server_wm_WindowAnimator_3951(long frameTimeNs) {
        synchronized (this.mService.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                this.mAnimationFrameCallbackScheduled = false;
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        animate(frameTimeNs);
    }

    void addDisplayLocked(int displayId) {
        getDisplayContentsAnimatorLocked(displayId);
        if (displayId == 0) {
            this.mInitialized = true;
        }
    }

    void removeDisplayLocked(int displayId) {
        DisplayContentsAnimator displayAnimator = (DisplayContentsAnimator) this.mDisplayContentsAnimators.get(displayId);
        if (!(displayAnimator == null || displayAnimator.mScreenRotationAnimation == null)) {
            displayAnimator.mScreenRotationAnimation.kill();
            displayAnimator.mScreenRotationAnimation = null;
        }
        this.mDisplayContentsAnimators.delete(displayId);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void animate(long r20) {
        /*
        r19 = this;
        r0 = r19;
        r12 = r0.mService;
        r13 = r12.mWindowMap;
        monitor-enter(r13);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x00b5 }
        r0 = r19;
        r12 = r0.mInitialized;	 Catch:{ all -> 0x00b5 }
        if (r12 != 0) goto L_0x0015;
    L_0x0010:
        monitor-exit(r13);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0015:
        r19.scheduleAnimation();	 Catch:{ all -> 0x00b5 }
        monitor-exit(r13);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        r0 = r19;
        r12 = r0.mService;
        r12.executeEmptyAnimationTransaction();
        r0 = r19;
        r12 = r0.mService;
        r13 = r12.mWindowMap;
        monitor-enter(r13);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x022f }
        r14 = 1000000; // 0xf4240 float:1.401298E-39 double:4.940656E-318;
        r14 = r20 / r14;
        r0 = r19;
        r0.mCurrentTime = r14;	 Catch:{ all -> 0x022f }
        r12 = 8;
        r0 = r19;
        r0.mBulkUpdateParams = r12;	 Catch:{ all -> 0x022f }
        r12 = 0;
        r0 = r19;
        r0.mAnimating = r12;	 Catch:{ all -> 0x022f }
        r12 = 0;
        r0 = r19;
        r0.mAppWindowAnimating = r12;	 Catch:{ all -> 0x022f }
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12.openSurfaceTransaction();	 Catch:{ all -> 0x022f }
        r0 = r19;
        r12 = r0.mService;	 Catch:{ RuntimeException -> 0x00dd }
        r2 = r12.mAccessibilityController;	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r12 = r0.mDisplayContentsAnimators;	 Catch:{ RuntimeException -> 0x00dd }
        r10 = r12.size();	 Catch:{ RuntimeException -> 0x00dd }
        r9 = 0;
    L_0x005c:
        if (r9 >= r10) goto L_0x019a;
    L_0x005e:
        r0 = r19;
        r12 = r0.mDisplayContentsAnimators;	 Catch:{ RuntimeException -> 0x00dd }
        r5 = r12.keyAt(r9);	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r12 = r0.mService;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12.mRoot;	 Catch:{ RuntimeException -> 0x00dd }
        r3 = r12.getDisplayContentOrCreate(r5);	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r14 = r0.mCurrentTime;	 Catch:{ RuntimeException -> 0x00dd }
        r3.stepAppWindowsAnimation(r14);	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r12 = r0.mDisplayContentsAnimators;	 Catch:{ RuntimeException -> 0x00dd }
        r4 = r12.valueAt(r9);	 Catch:{ RuntimeException -> 0x00dd }
        r4 = (com.android.server.wm.WindowAnimator.DisplayContentsAnimator) r4;	 Catch:{ RuntimeException -> 0x00dd }
        r11 = r4.mScreenRotationAnimation;	 Catch:{ RuntimeException -> 0x00dd }
        if (r11 == 0) goto L_0x009b;
    L_0x0085:
        r12 = r11.isAnimating();	 Catch:{ RuntimeException -> 0x00dd }
        if (r12 == 0) goto L_0x009b;
    L_0x008b:
        r0 = r19;
        r14 = r0.mCurrentTime;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r11.stepAnimationLocked(r14);	 Catch:{ RuntimeException -> 0x00dd }
        if (r12 == 0) goto L_0x00bb;
    L_0x0095:
        r12 = 1;
        r0 = r19;
        r0.setAnimating(r12);	 Catch:{ RuntimeException -> 0x00dd }
    L_0x009b:
        r0 = r19;
        r12 = r0.mAnimTransactionSequence;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12 + 1;
        r0 = r19;
        r0.mAnimTransactionSequence = r12;	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r3.updateWindowsForAnimator(r0);	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r3.updateWallpaperForAnimator(r0);	 Catch:{ RuntimeException -> 0x00dd }
        r3.prepareWindowSurfaces();	 Catch:{ RuntimeException -> 0x00dd }
        r9 = r9 + 1;
        goto L_0x005c;
    L_0x00b5:
        r12 = move-exception;
        monitor-exit(r13);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r12;
    L_0x00bb:
        r0 = r19;
        r12 = r0.mBulkUpdateParams;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12 | 1;
        r0 = r19;
        r0.mBulkUpdateParams = r12;	 Catch:{ RuntimeException -> 0x00dd }
        r11.kill();	 Catch:{ RuntimeException -> 0x00dd }
        r12 = 0;
        r4.mScreenRotationAnimation = r12;	 Catch:{ RuntimeException -> 0x00dd }
        if (r2 == 0) goto L_0x009b;
    L_0x00cd:
        r12 = r3.isDefaultDisplay;	 Catch:{ RuntimeException -> 0x00dd }
        if (r12 == 0) goto L_0x009b;
    L_0x00d1:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12.getDefaultDisplayContentLocked();	 Catch:{ RuntimeException -> 0x00dd }
        r2.onRotationChangedLocked(r12);	 Catch:{ RuntimeException -> 0x00dd }
        goto L_0x009b;
    L_0x00dd:
        r7 = move-exception;
        r12 = TAG;	 Catch:{ all -> 0x0235 }
        r14 = "Unhandled exception in Window Manager";
        android.util.Slog.wtf(r12, r14, r7);	 Catch:{ all -> 0x0235 }
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12.closeSurfaceTransaction();	 Catch:{ all -> 0x022f }
    L_0x00ed:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12 = r12.mRoot;	 Catch:{ all -> 0x022f }
        r0 = r19;
        r8 = r12.hasPendingLayoutChanges(r0);	 Catch:{ all -> 0x022f }
        r6 = 0;
        r0 = r19;
        r12 = r0.mBulkUpdateParams;	 Catch:{ all -> 0x022f }
        if (r12 == 0) goto L_0x010a;
    L_0x0100:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12 = r12.mRoot;	 Catch:{ all -> 0x022f }
        r6 = r12.copyAnimToLayoutParams();	 Catch:{ all -> 0x022f }
    L_0x010a:
        if (r8 != 0) goto L_0x010e;
    L_0x010c:
        if (r6 == 0) goto L_0x0115;
    L_0x010e:
        r0 = r19;
        r12 = r0.mWindowPlacerLocked;	 Catch:{ all -> 0x022f }
        r12.requestTraversal();	 Catch:{ all -> 0x022f }
    L_0x0115:
        r0 = r19;
        r12 = r0.mAnimating;	 Catch:{ all -> 0x022f }
        if (r12 == 0) goto L_0x0139;
    L_0x011b:
        r0 = r19;
        r12 = r0.mLastAnimating;	 Catch:{ all -> 0x022f }
        r12 = r12 ^ 1;
        if (r12 == 0) goto L_0x0139;
    L_0x0123:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12 = r12.mTaskSnapshotController;	 Catch:{ all -> 0x022f }
        r14 = 1;
        r12.setPersisterPaused(r14);	 Catch:{ all -> 0x022f }
        r12 = "animating";
        r14 = 32;
        r16 = 0;
        r0 = r16;
        android.os.Trace.asyncTraceBegin(r14, r12, r0);	 Catch:{ all -> 0x022f }
    L_0x0139:
        r0 = r19;
        r12 = r0.mAnimating;	 Catch:{ all -> 0x022f }
        if (r12 != 0) goto L_0x0162;
    L_0x013f:
        r0 = r19;
        r12 = r0.mLastAnimating;	 Catch:{ all -> 0x022f }
        if (r12 == 0) goto L_0x0162;
    L_0x0145:
        r0 = r19;
        r12 = r0.mWindowPlacerLocked;	 Catch:{ all -> 0x022f }
        r12.requestTraversal();	 Catch:{ all -> 0x022f }
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12 = r12.mTaskSnapshotController;	 Catch:{ all -> 0x022f }
        r14 = 0;
        r12.setPersisterPaused(r14);	 Catch:{ all -> 0x022f }
        r12 = "animating";
        r14 = 32;
        r16 = 0;
        r0 = r16;
        android.os.Trace.asyncTraceEnd(r14, r12, r0);	 Catch:{ all -> 0x022f }
    L_0x0162:
        r0 = r19;
        r12 = r0.mAnimating;	 Catch:{ all -> 0x022f }
        r0 = r19;
        r0.mLastAnimating = r12;	 Catch:{ all -> 0x022f }
        r0 = r19;
        r12 = r0.mRemoveReplacedWindows;	 Catch:{ all -> 0x022f }
        if (r12 == 0) goto L_0x017e;
    L_0x0170:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12 = r12.mRoot;	 Catch:{ all -> 0x022f }
        r12.removeReplacedWindows();	 Catch:{ all -> 0x022f }
        r12 = 0;
        r0 = r19;
        r0.mRemoveReplacedWindows = r12;	 Catch:{ all -> 0x022f }
    L_0x017e:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12.stopUsingSavedSurfaceLocked();	 Catch:{ all -> 0x022f }
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12.destroyPreservedSurfaceLocked();	 Catch:{ all -> 0x022f }
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12 = r12.mWindowPlacerLocked;	 Catch:{ all -> 0x022f }
        r12.destroyPendingSurfaces();	 Catch:{ all -> 0x022f }
        monitor-exit(r13);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x019a:
        r9 = 0;
    L_0x019b:
        if (r9 >= r10) goto L_0x01e9;
    L_0x019d:
        r0 = r19;
        r12 = r0.mDisplayContentsAnimators;	 Catch:{ RuntimeException -> 0x00dd }
        r5 = r12.keyAt(r9);	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r12 = r0.mService;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12.mRoot;	 Catch:{ RuntimeException -> 0x00dd }
        r3 = r12.getDisplayContentOrCreate(r5);	 Catch:{ RuntimeException -> 0x00dd }
        r3.checkAppWindowsReadyToShow();	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r12 = r0.mDisplayContentsAnimators;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12.valueAt(r9);	 Catch:{ RuntimeException -> 0x00dd }
        r12 = (com.android.server.wm.WindowAnimator.DisplayContentsAnimator) r12;	 Catch:{ RuntimeException -> 0x00dd }
        r11 = r12.mScreenRotationAnimation;	 Catch:{ RuntimeException -> 0x00dd }
        if (r11 == 0) goto L_0x01c3;
    L_0x01c0:
        r11.updateSurfacesInTransaction();	 Catch:{ RuntimeException -> 0x00dd }
    L_0x01c3:
        r12 = r3.animateDimLayers();	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r0.orAnimating(r12);	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r3.getDockedDividerController();	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r14 = r0.mCurrentTime;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12.animate(r14);	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r0.orAnimating(r12);	 Catch:{ RuntimeException -> 0x00dd }
        if (r2 == 0) goto L_0x01e6;
    L_0x01df:
        r12 = r3.isDefaultDisplay;	 Catch:{ RuntimeException -> 0x00dd }
        if (r12 == 0) goto L_0x01e6;
    L_0x01e3:
        r2.drawMagnifiedRegionBorderIfNeededLocked();	 Catch:{ RuntimeException -> 0x00dd }
    L_0x01e6:
        r9 = r9 + 1;
        goto L_0x019b;
    L_0x01e9:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12.mDragState;	 Catch:{ RuntimeException -> 0x00dd }
        if (r12 == 0) goto L_0x020c;
    L_0x01f1:
        r0 = r19;
        r12 = r0.mAnimating;	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r14 = r0.mService;	 Catch:{ RuntimeException -> 0x00dd }
        r14 = r14.mDragState;	 Catch:{ RuntimeException -> 0x00dd }
        r0 = r19;
        r0 = r0.mCurrentTime;	 Catch:{ RuntimeException -> 0x00dd }
        r16 = r0;
        r0 = r16;
        r14 = r14.stepAnimationLocked(r0);	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12 | r14;
        r0 = r19;
        r0.mAnimating = r12;	 Catch:{ RuntimeException -> 0x00dd }
    L_0x020c:
        r0 = r19;
        r12 = r0.mAnimating;	 Catch:{ RuntimeException -> 0x00dd }
        if (r12 != 0) goto L_0x0215;
    L_0x0212:
        r19.cancelAnimation();	 Catch:{ RuntimeException -> 0x00dd }
    L_0x0215:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12.mWatermark;	 Catch:{ RuntimeException -> 0x00dd }
        if (r12 == 0) goto L_0x0226;
    L_0x021d:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ RuntimeException -> 0x00dd }
        r12 = r12.mWatermark;	 Catch:{ RuntimeException -> 0x00dd }
        r12.drawIfNeeded();	 Catch:{ RuntimeException -> 0x00dd }
    L_0x0226:
        r0 = r19;
        r12 = r0.mService;	 Catch:{ all -> 0x022f }
        r12.closeSurfaceTransaction();	 Catch:{ all -> 0x022f }
        goto L_0x00ed;
    L_0x022f:
        r12 = move-exception;
        monitor-exit(r13);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r12;
    L_0x0235:
        r12 = move-exception;
        r0 = r19;
        r14 = r0.mService;	 Catch:{ all -> 0x022f }
        r14.closeSurfaceTransaction();	 Catch:{ all -> 0x022f }
        throw r12;	 Catch:{ all -> 0x022f }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.WindowAnimator.animate(long):void");
    }

    private static String bulkUpdateParamsToString(int bulkUpdateParams) {
        StringBuilder builder = new StringBuilder(128);
        if ((bulkUpdateParams & 1) != 0) {
            builder.append(" UPDATE_ROTATION");
        }
        if ((bulkUpdateParams & 2) != 0) {
            builder.append(" WALLPAPER_MAY_CHANGE");
        }
        if ((bulkUpdateParams & 4) != 0) {
            builder.append(" FORCE_HIDING_CHANGED");
        }
        if ((bulkUpdateParams & 8) != 0) {
            builder.append(" ORIENTATION_CHANGE_COMPLETE");
        }
        if ((bulkUpdateParams & 16) != 0) {
            builder.append(" TURN_ON_SCREEN");
        }
        return builder.toString();
    }

    public void dumpLocked(PrintWriter pw, String prefix, boolean dumpAll) {
        String subPrefix = "  " + prefix;
        String subSubPrefix = "  " + subPrefix;
        for (int i = 0; i < this.mDisplayContentsAnimators.size(); i++) {
            pw.print(prefix);
            pw.print("DisplayContentsAnimator #");
            pw.print(this.mDisplayContentsAnimators.keyAt(i));
            pw.println(":");
            DisplayContentsAnimator displayAnimator = (DisplayContentsAnimator) this.mDisplayContentsAnimators.valueAt(i);
            this.mService.mRoot.getDisplayContentOrCreate(this.mDisplayContentsAnimators.keyAt(i)).dumpWindowAnimators(pw, subPrefix);
            if (displayAnimator.mScreenRotationAnimation != null) {
                pw.print(subPrefix);
                pw.println("mScreenRotationAnimation:");
                displayAnimator.mScreenRotationAnimation.printTo(subSubPrefix, pw);
            } else if (dumpAll) {
                pw.print(subPrefix);
                pw.println("no ScreenRotationAnimation ");
            }
            pw.println();
        }
        pw.println();
        if (dumpAll) {
            pw.print(prefix);
            pw.print("mAnimTransactionSequence=");
            pw.print(this.mAnimTransactionSequence);
            pw.print(prefix);
            pw.print("mCurrentTime=");
            pw.println(TimeUtils.formatUptime(this.mCurrentTime));
        }
        if (this.mBulkUpdateParams != 0) {
            pw.print(prefix);
            pw.print("mBulkUpdateParams=0x");
            pw.print(Integer.toHexString(this.mBulkUpdateParams));
            pw.println(bulkUpdateParamsToString(this.mBulkUpdateParams));
        }
        if (this.mWindowDetachedWallpaper != null) {
            pw.print(prefix);
            pw.print("mWindowDetachedWallpaper=");
            pw.println(this.mWindowDetachedWallpaper);
        }
    }

    int getPendingLayoutChanges(int displayId) {
        int i = 0;
        if (displayId < 0) {
            return 0;
        }
        DisplayContent displayContent = this.mService.mRoot.getDisplayContentOrCreate(displayId);
        if (displayContent != null) {
            i = displayContent.pendingLayoutChanges;
        }
        return i;
    }

    void setPendingLayoutChanges(int displayId, int changes) {
        if (displayId >= 0) {
            DisplayContent displayContent = this.mService.mRoot.getDisplayContentOrCreate(displayId);
            if (displayContent != null) {
                displayContent.pendingLayoutChanges |= changes;
            }
        }
    }

    private DisplayContentsAnimator getDisplayContentsAnimatorLocked(int displayId) {
        if (displayId < 0) {
            return null;
        }
        DisplayContentsAnimator displayAnimator = (DisplayContentsAnimator) this.mDisplayContentsAnimators.get(displayId);
        if (displayAnimator == null && this.mService.mRoot.getDisplayContent(displayId) != null) {
            displayAnimator = new DisplayContentsAnimator();
            this.mDisplayContentsAnimators.put(displayId, displayAnimator);
        }
        return displayAnimator;
    }

    void setScreenRotationAnimationLocked(int displayId, ScreenRotationAnimation animation) {
        DisplayContentsAnimator animator = getDisplayContentsAnimatorLocked(displayId);
        if (animator != null) {
            animator.mScreenRotationAnimation = animation;
        }
    }

    ScreenRotationAnimation getScreenRotationAnimationLocked(int displayId) {
        ScreenRotationAnimation screenRotationAnimation = null;
        if (displayId < 0) {
            return null;
        }
        DisplayContentsAnimator animator = getDisplayContentsAnimatorLocked(displayId);
        if (animator != null) {
            screenRotationAnimation = animator.mScreenRotationAnimation;
        }
        return screenRotationAnimation;
    }

    void requestRemovalOfReplacedWindows(WindowState win) {
        this.mRemoveReplacedWindows = true;
    }

    void scheduleAnimation() {
        if (!this.mAnimationFrameCallbackScheduled) {
            this.mAnimationFrameCallbackScheduled = true;
            this.mChoreographer.postFrameCallback(this.mAnimationFrameCallback);
        }
    }

    private void cancelAnimation() {
        if (this.mAnimationFrameCallbackScheduled) {
            this.mAnimationFrameCallbackScheduled = false;
            this.mChoreographer.removeFrameCallback(this.mAnimationFrameCallback);
        }
    }

    boolean isAnimating() {
        return this.mAnimating;
    }

    boolean isAnimationScheduled() {
        return this.mAnimationFrameCallbackScheduled;
    }

    Choreographer getChoreographer() {
        return this.mChoreographer;
    }

    void setAnimating(boolean animating) {
        this.mAnimating = animating;
    }

    void orAnimating(boolean animating) {
        this.mAnimating |= animating;
    }
}
