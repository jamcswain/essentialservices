package com.android.server.wm;

import android.app.ActivityManager.TaskDescription;
import android.app.ActivityManager.TaskSnapshot;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.EventLog;
import com.android.server.EventLogTags;
import java.lang.ref.WeakReference;

public class TaskWindowContainerController extends WindowContainerController<Task, TaskWindowContainerListener> {
    private final H mHandler;
    private final int mTaskId;

    private static final class H extends Handler {
        static final int REPORT_SNAPSHOT_CHANGED = 0;
        static final int REQUEST_RESIZE = 1;
        private final WeakReference<TaskWindowContainerController> mController;

        H(WeakReference<TaskWindowContainerController> controller, Looper looper) {
            super(looper);
            this.mController = controller;
        }

        public void handleMessage(Message msg) {
            TaskWindowContainerController controller = (TaskWindowContainerController) this.mController.get();
            TaskWindowContainerListener taskWindowContainerListener = controller != null ? (TaskWindowContainerListener) controller.mListener : null;
            if (taskWindowContainerListener != null) {
                switch (msg.what) {
                    case 0:
                        taskWindowContainerListener.onSnapshotChanged((TaskSnapshot) msg.obj);
                        break;
                    case 1:
                        taskWindowContainerListener.requestResize((Rect) msg.obj, msg.arg1);
                        break;
                }
            }
        }
    }

    public TaskWindowContainerController(int taskId, TaskWindowContainerListener listener, StackWindowController stackController, int userId, Rect bounds, Configuration overrideConfig, int resizeMode, boolean supportsPictureInPicture, boolean homeTask, boolean toTop, boolean showForAllUsers, TaskDescription taskDescription) {
        this(taskId, listener, stackController, userId, bounds, overrideConfig, resizeMode, supportsPictureInPicture, homeTask, toTop, showForAllUsers, taskDescription, WindowManagerService.getInstance());
    }

    public TaskWindowContainerController(int taskId, TaskWindowContainerListener listener, StackWindowController stackController, int userId, Rect bounds, Configuration overrideConfig, int resizeMode, boolean supportsPictureInPicture, boolean homeTask, boolean toTop, boolean showForAllUsers, TaskDescription taskDescription, WindowManagerService service) {
        super(listener, service);
        this.mTaskId = taskId;
        this.mHandler = new H(new WeakReference(this), service.mH.getLooper());
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                TaskStack stack = stackController.mContainer;
                if (stack == null) {
                    throw new IllegalArgumentException("TaskWindowContainerController: invalid stack=" + stackController);
                }
                EventLog.writeEvent(EventLogTags.WM_TASK_CREATED, new Object[]{Integer.valueOf(taskId), Integer.valueOf(stack.mStackId)});
                stack.addTask(createTask(taskId, stack, userId, bounds, overrideConfig, resizeMode, supportsPictureInPicture, homeTask, taskDescription), toTop ? Integer.MAX_VALUE : Integer.MIN_VALUE, showForAllUsers, toTop);
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    Task createTask(int taskId, TaskStack stack, int userId, Rect bounds, Configuration overrideConfig, int resizeMode, boolean supportsPictureInPicture, boolean homeTask, TaskDescription taskDescription) {
        return new Task(taskId, stack, userId, this.mService, bounds, overrideConfig, resizeMode, supportsPictureInPicture, homeTask, taskDescription, this);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void removeContainer() {
        /*
        r2 = this;
        r1 = r2.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x001e }
        r0 = r2.mContainer;	 Catch:{ all -> 0x001e }
        if (r0 != 0) goto L_0x000f;
    L_0x000a:
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x000f:
        r0 = r2.mContainer;	 Catch:{ all -> 0x001e }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x001e }
        r0.removeIfPossible();	 Catch:{ all -> 0x001e }
        super.removeContainer();	 Catch:{ all -> 0x001e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x001e:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskWindowContainerController.removeContainer():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void positionChildAt(com.android.server.wm.AppWindowContainerController r7, int r8) {
        /*
        r6 = this;
        r2 = r6.mService;
        r3 = r2.mWindowMap;
        monitor-enter(r3);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x004d }
        r0 = r7.mContainer;	 Catch:{ all -> 0x004d }
        r0 = (com.android.server.wm.AppWindowToken) r0;	 Catch:{ all -> 0x004d }
        if (r0 != 0) goto L_0x002d;
    L_0x000e:
        r2 = "WindowManager";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x004d }
        r4.<init>();	 Catch:{ all -> 0x004d }
        r5 = "Attempted to position of non-existing app : ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x004d }
        r4 = r4.append(r7);	 Catch:{ all -> 0x004d }
        r4 = r4.toString();	 Catch:{ all -> 0x004d }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x004d }
        monitor-exit(r3);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x002d:
        r1 = r6.mContainer;	 Catch:{ all -> 0x004d }
        r1 = (com.android.server.wm.Task) r1;	 Catch:{ all -> 0x004d }
        if (r1 != 0) goto L_0x0053;
    L_0x0033:
        r2 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x004d }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x004d }
        r4.<init>();	 Catch:{ all -> 0x004d }
        r5 = "positionChildAt: invalid task=";
        r4 = r4.append(r5);	 Catch:{ all -> 0x004d }
        r4 = r4.append(r6);	 Catch:{ all -> 0x004d }
        r4 = r4.toString();	 Catch:{ all -> 0x004d }
        r2.<init>(r4);	 Catch:{ all -> 0x004d }
        throw r2;	 Catch:{ all -> 0x004d }
    L_0x004d:
        r2 = move-exception;
        monitor-exit(r3);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r2;
    L_0x0053:
        r2 = 0;
        r1.positionChildAt(r8, r0, r2);	 Catch:{ all -> 0x004d }
        monitor-exit(r3);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskWindowContainerController.positionChildAt(com.android.server.wm.AppWindowContainerController, int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void reparent(com.android.server.wm.StackWindowController r6, int r7, boolean r8) {
        /*
        r5 = this;
        r2 = r5.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x002f }
        r1 = r5.mContainer;	 Catch:{ all -> 0x002f }
        if (r1 != 0) goto L_0x000f;
    L_0x000a:
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x000f:
        r0 = r6.mContainer;	 Catch:{ all -> 0x002f }
        r0 = (com.android.server.wm.TaskStack) r0;	 Catch:{ all -> 0x002f }
        if (r0 != 0) goto L_0x0035;
    L_0x0015:
        r1 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x002f }
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x002f }
        r3.<init>();	 Catch:{ all -> 0x002f }
        r4 = "reparent: could not find stack=";
        r3 = r3.append(r4);	 Catch:{ all -> 0x002f }
        r3 = r3.append(r6);	 Catch:{ all -> 0x002f }
        r3 = r3.toString();	 Catch:{ all -> 0x002f }
        r1.<init>(r3);	 Catch:{ all -> 0x002f }
        throw r1;	 Catch:{ all -> 0x002f }
    L_0x002f:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
    L_0x0035:
        r1 = r5.mContainer;	 Catch:{ all -> 0x002f }
        r1 = (com.android.server.wm.Task) r1;	 Catch:{ all -> 0x002f }
        r1.reparent(r0, r7, r8);	 Catch:{ all -> 0x002f }
        r1 = r5.mContainer;	 Catch:{ all -> 0x002f }
        r1 = (com.android.server.wm.Task) r1;	 Catch:{ all -> 0x002f }
        r1 = r1.getDisplayContent();	 Catch:{ all -> 0x002f }
        r1.layoutAndAssignWindowLayersIfNeeded();	 Catch:{ all -> 0x002f }
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskWindowContainerController.reparent(com.android.server.wm.StackWindowController, int, boolean):void");
    }

    public void setResizeable(int resizeMode) {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer != null) {
                    ((Task) this.mContainer).setResizeable(resizeMode);
                }
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    public void resize(Rect bounds, Configuration overrideConfig, boolean relayout, boolean forced) {
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer == null) {
                    throw new IllegalArgumentException("resizeTask: taskId " + this.mTaskId + " not found.");
                }
                if (((Task) this.mContainer).resizeLocked(bounds, overrideConfig, forced) && relayout) {
                    ((Task) this.mContainer).getDisplayContent().layoutAndAssignWindowLayersIfNeeded();
                }
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void getBounds(android.graphics.Rect r3) {
        /*
        r2 = this;
        r1 = r2.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x001e }
        r0 = r2.mContainer;	 Catch:{ all -> 0x001e }
        if (r0 == 0) goto L_0x0016;
    L_0x000a:
        r0 = r2.mContainer;	 Catch:{ all -> 0x001e }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x001e }
        r0.getBounds(r3);	 Catch:{ all -> 0x001e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0016:
        r3.setEmpty();	 Catch:{ all -> 0x001e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x001e:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskWindowContainerController.getBounds(android.graphics.Rect):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setTaskDockedResizing(boolean r5) {
        /*
        r4 = this;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x003f }
        r0 = r4.mContainer;	 Catch:{ all -> 0x003f }
        if (r0 != 0) goto L_0x0032;
    L_0x000a:
        r0 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003f }
        r2.<init>();	 Catch:{ all -> 0x003f }
        r3 = "setTaskDockedResizing: taskId ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x003f }
        r3 = r4.mTaskId;	 Catch:{ all -> 0x003f }
        r2 = r2.append(r3);	 Catch:{ all -> 0x003f }
        r3 = " not found.";
        r2 = r2.append(r3);	 Catch:{ all -> 0x003f }
        r2 = r2.toString();	 Catch:{ all -> 0x003f }
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x003f }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0032:
        r0 = r4.mContainer;	 Catch:{ all -> 0x003f }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x003f }
        r2 = 1;
        r0.setDragResizing(r5, r2);	 Catch:{ all -> 0x003f }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x003f:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskWindowContainerController.setTaskDockedResizing(boolean):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void cancelWindowTransition() {
        /*
        r4 = this;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x003e }
        r0 = r4.mContainer;	 Catch:{ all -> 0x003e }
        if (r0 != 0) goto L_0x0032;
    L_0x000a:
        r0 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003e }
        r2.<init>();	 Catch:{ all -> 0x003e }
        r3 = "cancelWindowTransition: taskId ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r3 = r4.mTaskId;	 Catch:{ all -> 0x003e }
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r3 = " not found.";
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r2 = r2.toString();	 Catch:{ all -> 0x003e }
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x003e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0032:
        r0 = r4.mContainer;	 Catch:{ all -> 0x003e }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x003e }
        r0.cancelTaskWindowTransition();	 Catch:{ all -> 0x003e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x003e:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskWindowContainerController.cancelWindowTransition():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void cancelThumbnailTransition() {
        /*
        r4 = this;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x003e }
        r0 = r4.mContainer;	 Catch:{ all -> 0x003e }
        if (r0 != 0) goto L_0x0032;
    L_0x000a:
        r0 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003e }
        r2.<init>();	 Catch:{ all -> 0x003e }
        r3 = "cancelThumbnailTransition: taskId ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r3 = r4.mTaskId;	 Catch:{ all -> 0x003e }
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r3 = " not found.";
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r2 = r2.toString();	 Catch:{ all -> 0x003e }
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x003e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0032:
        r0 = r4.mContainer;	 Catch:{ all -> 0x003e }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x003e }
        r0.cancelTaskThumbnailTransition();	 Catch:{ all -> 0x003e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x003e:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskWindowContainerController.cancelThumbnailTransition():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setTaskDescription(android.app.ActivityManager.TaskDescription r5) {
        /*
        r4 = this;
        r1 = r4.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x003e }
        r0 = r4.mContainer;	 Catch:{ all -> 0x003e }
        if (r0 != 0) goto L_0x0032;
    L_0x000a:
        r0 = "WindowManager";
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003e }
        r2.<init>();	 Catch:{ all -> 0x003e }
        r3 = "setTaskDescription: taskId ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r3 = r4.mTaskId;	 Catch:{ all -> 0x003e }
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r3 = " not found.";
        r2 = r2.append(r3);	 Catch:{ all -> 0x003e }
        r2 = r2.toString();	 Catch:{ all -> 0x003e }
        android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x003e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0032:
        r0 = r4.mContainer;	 Catch:{ all -> 0x003e }
        r0 = (com.android.server.wm.Task) r0;	 Catch:{ all -> 0x003e }
        r0.setTaskDescription(r5);	 Catch:{ all -> 0x003e }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x003e:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.TaskWindowContainerController.setTaskDescription(android.app.ActivityManager$TaskDescription):void");
    }

    void reportSnapshotChanged(TaskSnapshot snapshot) {
        this.mHandler.obtainMessage(0, snapshot).sendToTarget();
    }

    void requestResize(Rect bounds, int resizeMode) {
        this.mHandler.obtainMessage(1, resizeMode, 0, bounds).sendToTarget();
    }

    public String toString() {
        return "{TaskWindowContainerController taskId=" + this.mTaskId + "}";
    }
}
