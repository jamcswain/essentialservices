package com.android.server.wm;

import android.graphics.Rect;
import android.graphics.Region;
import android.hardware.input.InputManager;
import android.view.MotionEvent;
import android.view.WindowManagerPolicy.PointerEventListener;

public class TaskTapPointerEventListener implements PointerEventListener {
    private final DisplayContent mDisplayContent;
    private int mPointerIconType = 1;
    private final WindowManagerService mService;
    private final Rect mTmpRect = new Rect();
    private final Region mTouchExcludeRegion = new Region();

    public TaskTapPointerEventListener(WindowManagerService service, DisplayContent displayContent) {
        this.mService = service;
        this.mDisplayContent = displayContent;
    }

    public void onPointerEvent(MotionEvent motionEvent, int displayId) {
        if (displayId == getDisplayId()) {
            onPointerEvent(motionEvent);
        }
    }

    public void onPointerEvent(MotionEvent motionEvent) {
        int x;
        int y;
        switch (motionEvent.getAction() & 255) {
            case 0:
                x = (int) motionEvent.getX();
                y = (int) motionEvent.getY();
                synchronized (this) {
                    if (!this.mTouchExcludeRegion.contains(x, y)) {
                        this.mService.mH.obtainMessage(31, x, y, this.mDisplayContent).sendToTarget();
                    }
                }
                return;
            case 7:
                x = (int) motionEvent.getX();
                y = (int) motionEvent.getY();
                Task task = this.mDisplayContent.findTaskForResizePoint(x, y);
                int iconType = 1;
                if (task != null) {
                    task.getDimBounds(this.mTmpRect);
                    if (!(this.mTmpRect.isEmpty() || (this.mTmpRect.contains(x, y) ^ 1) == 0)) {
                        if (x < this.mTmpRect.left) {
                            iconType = y < this.mTmpRect.top ? 1017 : y > this.mTmpRect.bottom ? 1016 : 1014;
                        } else if (x > this.mTmpRect.right) {
                            iconType = y < this.mTmpRect.top ? 1016 : y > this.mTmpRect.bottom ? 1017 : 1014;
                        } else if (y < this.mTmpRect.top || y > this.mTmpRect.bottom) {
                            iconType = 1015;
                        }
                    }
                }
                if (this.mPointerIconType != iconType) {
                    this.mPointerIconType = iconType;
                    if (this.mPointerIconType == 1) {
                        this.mService.mH.obtainMessage(55, x, y, this.mDisplayContent).sendToTarget();
                        return;
                    } else {
                        InputManager.getInstance().setPointerIconType(this.mPointerIconType);
                        return;
                    }
                }
                return;
            default:
                return;
        }
    }

    void setTouchExcludeRegion(Region newRegion) {
        synchronized (this) {
            this.mTouchExcludeRegion.set(newRegion);
        }
    }

    private int getDisplayId() {
        return this.mDisplayContent.getDisplayId();
    }
}
