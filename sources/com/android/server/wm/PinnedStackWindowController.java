package com.android.server.wm;

import android.graphics.Rect;

public class PinnedStackWindowController extends StackWindowController {
    private Rect mTmpFromBounds = new Rect();
    private Rect mTmpToBounds = new Rect();

    public PinnedStackWindowController(int stackId, PinnedStackWindowListener listener, int displayId, boolean onTop, Rect outBounds) {
        super(stackId, listener, displayId, onTop, outBounds, WindowManagerService.getInstance());
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Rect getPictureInPictureBounds(float r6, android.graphics.Rect r7) {
        /*
        r5 = this;
        r4 = 0;
        r3 = r5.mWindowMap;
        monitor-enter(r3);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0044 }
        r2 = r5.mService;	 Catch:{ all -> 0x0044 }
        r2 = r2.mSupportsPictureInPicture;	 Catch:{ all -> 0x0044 }
        if (r2 == 0) goto L_0x0011;
    L_0x000d:
        r2 = r5.mContainer;	 Catch:{ all -> 0x0044 }
        if (r2 != 0) goto L_0x0016;
    L_0x0011:
        monitor-exit(r3);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r4;
    L_0x0016:
        r2 = r5.mContainer;	 Catch:{ all -> 0x0044 }
        r2 = (com.android.server.wm.TaskStack) r2;	 Catch:{ all -> 0x0044 }
        r0 = r2.getDisplayContent();	 Catch:{ all -> 0x0044 }
        if (r0 != 0) goto L_0x0025;
    L_0x0020:
        monitor-exit(r3);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r4;
    L_0x0025:
        r1 = r0.getPinnedStackController();	 Catch:{ all -> 0x0044 }
        if (r7 != 0) goto L_0x002f;
    L_0x002b:
        r7 = r1.getDefaultBounds();	 Catch:{ all -> 0x0044 }
    L_0x002f:
        r2 = r1.isValidPictureInPictureAspectRatio(r6);	 Catch:{ all -> 0x0044 }
        if (r2 == 0) goto L_0x003f;
    L_0x0035:
        r2 = 1;
        r2 = r1.transformBoundsToAspectRatio(r7, r6, r2);	 Catch:{ all -> 0x0044 }
        monitor-exit(r3);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r2;
    L_0x003f:
        monitor-exit(r3);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return r7;
    L_0x0044:
        r2 = move-exception;
        monitor-exit(r3);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.PinnedStackWindowController.getPictureInPictureBounds(float, android.graphics.Rect):android.graphics.Rect");
    }

    public void animateResizePinnedStack(Rect toBounds, Rect sourceHintBounds, int animationDuration, boolean fromFullscreen) {
        Throwable th;
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                if (this.mContainer == null) {
                    throw new IllegalArgumentException("Pinned stack container not found :(");
                }
                Rect fromBounds = new Rect();
                ((TaskStack) this.mContainer).getBounds(fromBounds);
                int schedulePipModeChangedState = 0;
                boolean toFullscreen = toBounds == null;
                if (toFullscreen) {
                    if (fromFullscreen) {
                        throw new IllegalArgumentException("Should not defer scheduling PiP mode change on animation to fullscreen.");
                    }
                    schedulePipModeChangedState = 1;
                    this.mService.getStackBounds(1, this.mTmpToBounds);
                    if (this.mTmpToBounds.isEmpty()) {
                        Rect toBounds2 = new Rect();
                        try {
                            ((TaskStack) this.mContainer).getDisplayContent().getLogicalDisplayRect(toBounds2);
                            toBounds = toBounds2;
                        } catch (Throwable th2) {
                            th = th2;
                            toBounds = toBounds2;
                            WindowManagerService.resetPriorityAfterLockedSection();
                            throw th;
                        }
                    }
                    toBounds = new Rect(this.mTmpToBounds);
                } else if (fromFullscreen) {
                    schedulePipModeChangedState = 2;
                }
                ((TaskStack) this.mContainer).setAnimationFinalBounds(sourceHintBounds, toBounds, toFullscreen);
                this.mService.mBoundsAnimationController.getHandler().post(new -$Lambda$Dd9IZYP_DnuZN905KeMl4-pzcAs(fromFullscreen, toFullscreen, animationDuration, schedulePipModeChangedState, this, fromBounds, toBounds));
                WindowManagerService.resetPriorityAfterLockedSection();
            } catch (Throwable th3) {
                th = th3;
                WindowManagerService.resetPriorityAfterLockedSection();
                throw th;
            }
        }
    }

    /* synthetic */ void lambda$-com_android_server_wm_PinnedStackWindowController_5117(Rect fromBounds, Rect finalToBounds, int animationDuration, int finalSchedulePipModeChangedState, boolean fromFullscreen, boolean toFullscreen) {
        if (this.mContainer != null) {
            this.mService.mBoundsAnimationController.animateBounds((BoundsAnimationTarget) this.mContainer, fromBounds, finalToBounds, animationDuration, finalSchedulePipModeChangedState, fromFullscreen, toFullscreen);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setPictureInPictureAspectRatio(float r7) {
        /*
        r6 = this;
        r2 = r6.mWindowMap;
        monitor-enter(r2);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0063 }
        r1 = r6.mService;	 Catch:{ all -> 0x0063 }
        r1 = r1.mSupportsPictureInPicture;	 Catch:{ all -> 0x0063 }
        if (r1 == 0) goto L_0x0010;
    L_0x000c:
        r1 = r6.mContainer;	 Catch:{ all -> 0x0063 }
        if (r1 != 0) goto L_0x0015;
    L_0x0010:
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0015:
        r1 = r6.mContainer;	 Catch:{ all -> 0x0063 }
        r1 = (com.android.server.wm.TaskStack) r1;	 Catch:{ all -> 0x0063 }
        r1 = r1.getDisplayContent();	 Catch:{ all -> 0x0063 }
        r0 = r1.getPinnedStackController();	 Catch:{ all -> 0x0063 }
        r1 = r0.getAspectRatio();	 Catch:{ all -> 0x0063 }
        r1 = java.lang.Float.compare(r7, r1);	 Catch:{ all -> 0x0063 }
        if (r1 == 0) goto L_0x005b;
    L_0x002b:
        r1 = r6.mContainer;	 Catch:{ all -> 0x0063 }
        r1 = (com.android.server.wm.TaskStack) r1;	 Catch:{ all -> 0x0063 }
        r3 = r6.mTmpFromBounds;	 Catch:{ all -> 0x0063 }
        r1.getAnimationOrCurrentBounds(r3);	 Catch:{ all -> 0x0063 }
        r1 = r6.mTmpToBounds;	 Catch:{ all -> 0x0063 }
        r3 = r6.mTmpFromBounds;	 Catch:{ all -> 0x0063 }
        r1.set(r3);	 Catch:{ all -> 0x0063 }
        r1 = r6.mTmpToBounds;	 Catch:{ all -> 0x0063 }
        r6.getPictureInPictureBounds(r7, r1);	 Catch:{ all -> 0x0063 }
        r1 = r6.mTmpToBounds;	 Catch:{ all -> 0x0063 }
        r3 = r6.mTmpFromBounds;	 Catch:{ all -> 0x0063 }
        r1 = r1.equals(r3);	 Catch:{ all -> 0x0063 }
        if (r1 != 0) goto L_0x0052;
    L_0x004a:
        r1 = r6.mTmpToBounds;	 Catch:{ all -> 0x0063 }
        r3 = 0;
        r4 = -1;
        r5 = 0;
        r6.animateResizePinnedStack(r1, r3, r4, r5);	 Catch:{ all -> 0x0063 }
    L_0x0052:
        r1 = r0.isValidPictureInPictureAspectRatio(r7);	 Catch:{ all -> 0x0063 }
        if (r1 == 0) goto L_0x0060;
    L_0x0058:
        r0.setAspectRatio(r7);	 Catch:{ all -> 0x0063 }
    L_0x005b:
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0060:
        r7 = -1082130432; // 0xffffffffbf800000 float:-1.0 double:NaN;
        goto L_0x0058;
    L_0x0063:
        r1 = move-exception;
        monitor-exit(r2);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.PinnedStackWindowController.setPictureInPictureAspectRatio(float):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setPictureInPictureActions(java.util.List<android.app.RemoteAction> r3) {
        /*
        r2 = this;
        r1 = r2.mWindowMap;
        monitor-enter(r1);
        com.android.server.wm.WindowManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0029 }
        r0 = r2.mService;	 Catch:{ all -> 0x0029 }
        r0 = r0.mSupportsPictureInPicture;	 Catch:{ all -> 0x0029 }
        if (r0 == 0) goto L_0x0010;
    L_0x000c:
        r0 = r2.mContainer;	 Catch:{ all -> 0x0029 }
        if (r0 != 0) goto L_0x0015;
    L_0x0010:
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0015:
        r0 = r2.mContainer;	 Catch:{ all -> 0x0029 }
        r0 = (com.android.server.wm.TaskStack) r0;	 Catch:{ all -> 0x0029 }
        r0 = r0.getDisplayContent();	 Catch:{ all -> 0x0029 }
        r0 = r0.getPinnedStackController();	 Catch:{ all -> 0x0029 }
        r0.setActions(r3);	 Catch:{ all -> 0x0029 }
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0029:
        r0 = move-exception;
        monitor-exit(r1);
        com.android.server.wm.WindowManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wm.PinnedStackWindowController.setPictureInPictureActions(java.util.List):void");
    }

    public boolean deferScheduleMultiWindowModeChanged() {
        boolean deferScheduleMultiWindowModeChanged;
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                deferScheduleMultiWindowModeChanged = ((TaskStack) this.mContainer).deferScheduleMultiWindowModeChanged();
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        return deferScheduleMultiWindowModeChanged;
    }

    public boolean isAnimatingBoundsToFullscreen() {
        boolean isAnimatingBoundsToFullscreen;
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                isAnimatingBoundsToFullscreen = ((TaskStack) this.mContainer).isAnimatingBoundsToFullscreen();
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        return isAnimatingBoundsToFullscreen;
    }

    public boolean pinnedStackResizeDisallowed() {
        boolean pinnedStackResizeDisallowed;
        synchronized (this.mWindowMap) {
            try {
                WindowManagerService.boostPriorityForLockedSection();
                pinnedStackResizeDisallowed = ((TaskStack) this.mContainer).pinnedStackResizeDisallowed();
            } finally {
                WindowManagerService.resetPriorityAfterLockedSection();
            }
        }
        return pinnedStackResizeDisallowed;
    }

    public void updatePictureInPictureModeForPinnedStackAnimation(Rect targetStackBounds, boolean forceUpdate) {
        if (this.mListener != null) {
            this.mListener.updatePictureInPictureModeForPinnedStackAnimation(targetStackBounds, forceUpdate);
        }
    }
}
