package com.android.server;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import android.os.Build;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.LocalLog;
import android.util.Slog;
import com.android.internal.util.Preconditions;
import com.android.server.Watchdog.Monitor;
import com.google.android.collect.Lists;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

final class NativeDaemonConnector implements Runnable, Callback, Monitor {
    private static final long DEFAULT_TIMEOUT = 60000;
    private static final boolean VDBG = false;
    private static final long WARN_EXECUTE_DELAY_MS = 500;
    private final int BUFFER_SIZE;
    private final String TAG;
    private Handler mCallbackHandler;
    private INativeDaemonConnectorCallbacks mCallbacks;
    private final Object mDaemonLock;
    private volatile boolean mDebug;
    private LocalLog mLocalLog;
    private final Looper mLooper;
    private OutputStream mOutputStream;
    private final ResponseQueue mResponseQueue;
    private AtomicInteger mSequenceNumber;
    private String mSocket;
    private final WakeLock mWakeLock;
    private volatile Object mWarnIfHeld;

    public static class Command {
        private ArrayList<Object> mArguments = Lists.newArrayList();
        private String mCmd;

        public Command(String cmd, Object... args) {
            this.mCmd = cmd;
            for (Object arg : args) {
                appendArg(arg);
            }
        }

        public Command appendArg(Object arg) {
            this.mArguments.add(arg);
            return this;
        }
    }

    private static class NativeDaemonArgumentException extends NativeDaemonConnectorException {
        public NativeDaemonArgumentException(String command, NativeDaemonEvent event) {
            super(command, event);
        }

        public IllegalArgumentException rethrowAsParcelableException() {
            throw new IllegalArgumentException(getMessage(), this);
        }
    }

    private static class NativeDaemonFailureException extends NativeDaemonConnectorException {
        public NativeDaemonFailureException(String command, NativeDaemonEvent event) {
            super(command, event);
        }
    }

    private static class ResponseQueue {
        private int mMaxCount;
        private final LinkedList<PendingCmd> mPendingCmds = new LinkedList();

        private static class PendingCmd {
            public int availableResponseCount;
            public final int cmdNum;
            public final String logCmd;
            public BlockingQueue<NativeDaemonEvent> responses = new ArrayBlockingQueue(10);

            public PendingCmd(int cmdNum, String logCmd) {
                this.cmdNum = cmdNum;
                this.logCmd = logCmd;
            }
        }

        ResponseQueue(int maxCount) {
            this.mMaxCount = maxCount;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void add(int r10, com.android.server.NativeDaemonEvent r11) {
            /*
            r9 = this;
            r1 = 0;
            r6 = r9.mPendingCmds;
            monitor-enter(r6);
            r5 = r9.mPendingCmds;	 Catch:{ all -> 0x00b5 }
            r4 = r5.iterator();	 Catch:{ all -> 0x00b5 }
        L_0x000a:
            r5 = r4.hasNext();	 Catch:{ all -> 0x00b5 }
            if (r5 == 0) goto L_0x00b9;
        L_0x0010:
            r3 = r4.next();	 Catch:{ all -> 0x00b5 }
            r3 = (com.android.server.NativeDaemonConnector.ResponseQueue.PendingCmd) r3;	 Catch:{ all -> 0x00b5 }
            r5 = r3.cmdNum;	 Catch:{ all -> 0x00b5 }
            if (r5 != r10) goto L_0x000a;
        L_0x001a:
            r1 = r3;
            r2 = r1;
        L_0x001c:
            if (r2 != 0) goto L_0x00b7;
        L_0x001e:
            r5 = r9.mPendingCmds;	 Catch:{ all -> 0x008e }
            r5 = r5.size();	 Catch:{ all -> 0x008e }
            r7 = r9.mMaxCount;	 Catch:{ all -> 0x008e }
            if (r5 < r7) goto L_0x0092;
        L_0x0028:
            r5 = "NativeDaemonConnector.ResponseQueue";
            r7 = new java.lang.StringBuilder;	 Catch:{ all -> 0x008e }
            r7.<init>();	 Catch:{ all -> 0x008e }
            r8 = "more buffered than allowed: ";
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r8 = r9.mPendingCmds;	 Catch:{ all -> 0x008e }
            r8 = r8.size();	 Catch:{ all -> 0x008e }
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r8 = " >= ";
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r8 = r9.mMaxCount;	 Catch:{ all -> 0x008e }
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r7 = r7.toString();	 Catch:{ all -> 0x008e }
            android.util.Slog.e(r5, r7);	 Catch:{ all -> 0x008e }
            r5 = r9.mPendingCmds;	 Catch:{ all -> 0x008e }
            r3 = r5.remove();	 Catch:{ all -> 0x008e }
            r3 = (com.android.server.NativeDaemonConnector.ResponseQueue.PendingCmd) r3;	 Catch:{ all -> 0x008e }
            r5 = "NativeDaemonConnector.ResponseQueue";
            r7 = new java.lang.StringBuilder;	 Catch:{ all -> 0x008e }
            r7.<init>();	 Catch:{ all -> 0x008e }
            r8 = "Removing request: ";
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r8 = r3.logCmd;	 Catch:{ all -> 0x008e }
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r8 = " (";
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r8 = r3.cmdNum;	 Catch:{ all -> 0x008e }
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r8 = ")";
            r7 = r7.append(r8);	 Catch:{ all -> 0x008e }
            r7 = r7.toString();	 Catch:{ all -> 0x008e }
            android.util.Slog.e(r5, r7);	 Catch:{ all -> 0x008e }
            goto L_0x001e;
        L_0x008e:
            r5 = move-exception;
            r1 = r2;
        L_0x0090:
            monitor-exit(r6);
            throw r5;
        L_0x0092:
            r1 = new com.android.server.NativeDaemonConnector$ResponseQueue$PendingCmd;	 Catch:{ all -> 0x008e }
            r5 = 0;
            r1.<init>(r10, r5);	 Catch:{ all -> 0x008e }
            r5 = r9.mPendingCmds;	 Catch:{ all -> 0x00b5 }
            r5.add(r1);	 Catch:{ all -> 0x00b5 }
        L_0x009d:
            r5 = r1.availableResponseCount;	 Catch:{ all -> 0x00b5 }
            r5 = r5 + 1;
            r1.availableResponseCount = r5;	 Catch:{ all -> 0x00b5 }
            r5 = r1.availableResponseCount;	 Catch:{ all -> 0x00b5 }
            if (r5 != 0) goto L_0x00ac;
        L_0x00a7:
            r5 = r9.mPendingCmds;	 Catch:{ all -> 0x00b5 }
            r5.remove(r1);	 Catch:{ all -> 0x00b5 }
        L_0x00ac:
            monitor-exit(r6);
            r5 = r1.responses;	 Catch:{ InterruptedException -> 0x00b3 }
            r5.put(r11);	 Catch:{ InterruptedException -> 0x00b3 }
        L_0x00b2:
            return;
        L_0x00b3:
            r0 = move-exception;
            goto L_0x00b2;
        L_0x00b5:
            r5 = move-exception;
            goto L_0x0090;
        L_0x00b7:
            r1 = r2;
            goto L_0x009d;
        L_0x00b9:
            r2 = r1;
            goto L_0x001c;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.NativeDaemonConnector.ResponseQueue.add(int, com.android.server.NativeDaemonEvent):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.android.server.NativeDaemonEvent remove(int r11, long r12, java.lang.String r14) {
            /*
            r10 = this;
            r2 = 0;
            r8 = r10.mPendingCmds;
            monitor-enter(r8);
            r7 = r10.mPendingCmds;	 Catch:{ all -> 0x0051 }
            r5 = r7.iterator();	 Catch:{ all -> 0x0051 }
        L_0x000a:
            r7 = r5.hasNext();	 Catch:{ all -> 0x0051 }
            if (r7 == 0) goto L_0x005b;
        L_0x0010:
            r4 = r5.next();	 Catch:{ all -> 0x0051 }
            r4 = (com.android.server.NativeDaemonConnector.ResponseQueue.PendingCmd) r4;	 Catch:{ all -> 0x0051 }
            r7 = r4.cmdNum;	 Catch:{ all -> 0x0051 }
            if (r7 != r11) goto L_0x000a;
        L_0x001a:
            r2 = r4;
            r3 = r2;
        L_0x001c:
            if (r3 != 0) goto L_0x0059;
        L_0x001e:
            r2 = new com.android.server.NativeDaemonConnector$ResponseQueue$PendingCmd;	 Catch:{ all -> 0x0056 }
            r2.<init>(r11, r14);	 Catch:{ all -> 0x0056 }
            r7 = r10.mPendingCmds;	 Catch:{ all -> 0x0051 }
            r7.add(r2);	 Catch:{ all -> 0x0051 }
        L_0x0028:
            r7 = r2.availableResponseCount;	 Catch:{ all -> 0x0051 }
            r7 = r7 + -1;
            r2.availableResponseCount = r7;	 Catch:{ all -> 0x0051 }
            r7 = r2.availableResponseCount;	 Catch:{ all -> 0x0051 }
            if (r7 != 0) goto L_0x0037;
        L_0x0032:
            r7 = r10.mPendingCmds;	 Catch:{ all -> 0x0051 }
            r7.remove(r2);	 Catch:{ all -> 0x0051 }
        L_0x0037:
            monitor-exit(r8);
            r6 = 0;
            r7 = r2.responses;	 Catch:{ InterruptedException -> 0x0054 }
            r8 = java.util.concurrent.TimeUnit.MILLISECONDS;	 Catch:{ InterruptedException -> 0x0054 }
            r7 = r7.poll(r12, r8);	 Catch:{ InterruptedException -> 0x0054 }
            r0 = r7;
            r0 = (com.android.server.NativeDaemonEvent) r0;	 Catch:{ InterruptedException -> 0x0054 }
            r6 = r0;
        L_0x0045:
            if (r6 != 0) goto L_0x0050;
        L_0x0047:
            r7 = "NativeDaemonConnector.ResponseQueue";
            r8 = "Timeout waiting for response";
            android.util.Slog.e(r7, r8);
        L_0x0050:
            return r6;
        L_0x0051:
            r7 = move-exception;
        L_0x0052:
            monitor-exit(r8);
            throw r7;
        L_0x0054:
            r1 = move-exception;
            goto L_0x0045;
        L_0x0056:
            r7 = move-exception;
            r2 = r3;
            goto L_0x0052;
        L_0x0059:
            r2 = r3;
            goto L_0x0028;
        L_0x005b:
            r3 = r2;
            goto L_0x001c;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.NativeDaemonConnector.ResponseQueue.remove(int, long, java.lang.String):com.android.server.NativeDaemonEvent");
        }

        public void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
            pw.println("Pending requests:");
            synchronized (this.mPendingCmds) {
                for (PendingCmd pendingCmd : this.mPendingCmds) {
                    pw.println("  Cmd " + pendingCmd.cmdNum + " - " + pendingCmd.logCmd);
                }
            }
        }
    }

    public static class SensitiveArg {
        private final Object mArg;

        public SensitiveArg(Object arg) {
            this.mArg = arg;
        }

        public String toString() {
            return String.valueOf(this.mArg);
        }
    }

    NativeDaemonConnector(INativeDaemonConnectorCallbacks callbacks, String socket, int responseQueueSize, String logTag, int maxLogSize, WakeLock wl) {
        this(callbacks, socket, responseQueueSize, logTag, maxLogSize, wl, FgThread.get().getLooper());
    }

    NativeDaemonConnector(INativeDaemonConnectorCallbacks callbacks, String socket, int responseQueueSize, String logTag, int maxLogSize, WakeLock wl, Looper looper) {
        this.mDebug = false;
        this.mDaemonLock = new Object();
        this.BUFFER_SIZE = 4096;
        this.mCallbacks = callbacks;
        this.mSocket = socket;
        this.mResponseQueue = new ResponseQueue(responseQueueSize);
        this.mWakeLock = wl;
        if (this.mWakeLock != null) {
            this.mWakeLock.setReferenceCounted(true);
        }
        this.mLooper = looper;
        this.mSequenceNumber = new AtomicInteger(0);
        if (logTag == null) {
            logTag = "NativeDaemonConnector";
        }
        this.TAG = logTag;
        this.mLocalLog = new LocalLog(maxLogSize);
    }

    public void setDebug(boolean debug) {
        this.mDebug = debug;
    }

    private int uptimeMillisInt() {
        return ((int) SystemClock.uptimeMillis()) & Integer.MAX_VALUE;
    }

    public void setWarnIfHeld(Object warnIfHeld) {
        Preconditions.checkState(this.mWarnIfHeld == null);
        this.mWarnIfHeld = Preconditions.checkNotNull(warnIfHeld);
    }

    public void run() {
        this.mCallbackHandler = new Handler(this.mLooper, this);
        while (true) {
            try {
                listenToSocket();
            } catch (Exception e) {
                loge("Error in NativeDaemonConnector: " + e);
                SystemClock.sleep(5000);
            }
        }
    }

    public boolean handleMessage(Message msg) {
        String event = msg.obj;
        int start = uptimeMillisInt();
        int sent = msg.arg1;
        int end;
        try {
            if (!this.mCallbacks.onEvent(msg.what, event, NativeDaemonEvent.unescapeArgs(event))) {
                log(String.format("Unhandled event '%s'", new Object[]{event}));
            }
            if (this.mCallbacks.onCheckHoldWakeLock(msg.what) && this.mWakeLock != null) {
                this.mWakeLock.release();
            }
            end = uptimeMillisInt();
            if (start > sent && ((long) (start - sent)) > 500) {
                loge(String.format("NDC event {%s} processed too late: %dms", new Object[]{event, Integer.valueOf(start - sent)}));
            }
            if (end > start && ((long) (end - start)) > 500) {
                loge(String.format("NDC event {%s} took too long: %dms", new Object[]{event, Integer.valueOf(end - start)}));
            }
        } catch (Exception e) {
            loge("Error handling '" + event + "': " + e);
            if (this.mCallbacks.onCheckHoldWakeLock(msg.what) && this.mWakeLock != null) {
                this.mWakeLock.release();
            }
            end = uptimeMillisInt();
            if (start > sent && ((long) (start - sent)) > 500) {
                loge(String.format("NDC event {%s} processed too late: %dms", new Object[]{event, Integer.valueOf(start - sent)}));
            }
            if (end > start && ((long) (end - start)) > 500) {
                loge(String.format("NDC event {%s} took too long: %dms", new Object[]{event, Integer.valueOf(end - start)}));
            }
        } catch (Throwable th) {
            if (this.mCallbacks.onCheckHoldWakeLock(msg.what) && this.mWakeLock != null) {
                this.mWakeLock.release();
            }
            end = uptimeMillisInt();
            if (start > sent && ((long) (start - sent)) > 500) {
                loge(String.format("NDC event {%s} processed too late: %dms", new Object[]{event, Integer.valueOf(start - sent)}));
            }
            if (end > start && ((long) (end - start)) > 500) {
                loge(String.format("NDC event {%s} took too long: %dms", new Object[]{event, Integer.valueOf(end - start)}));
            }
        }
        return true;
    }

    private LocalSocketAddress determineSocketAddress() {
        if (this.mSocket.startsWith("__test__") && Build.IS_DEBUGGABLE) {
            return new LocalSocketAddress(this.mSocket);
        }
        return new LocalSocketAddress(this.mSocket, Namespace.RESERVED);
    }

    private void listenToSocket() throws IOException {
        IOException ex;
        Throwable th;
        LocalSocket localSocket = null;
        try {
            LocalSocket socket = new LocalSocket();
            try {
                int count;
                socket.connect(determineSocketAddress());
                InputStream inputStream = socket.getInputStream();
                synchronized (this.mDaemonLock) {
                    this.mOutputStream = socket.getOutputStream();
                }
                this.mCallbacks.onDaemonConnected();
                byte[] buffer = new byte[4096];
                int start = 0;
                while (true) {
                    count = inputStream.read(buffer, start, 4096 - start);
                    if (count < 0) {
                        break;
                    }
                    FileDescriptor[] fdList = socket.getAncillaryFileDescriptors();
                    count += start;
                    start = 0;
                    for (int i = 0; i < count; i++) {
                        if (buffer[i] == (byte) 0) {
                            boolean releaseWl = false;
                            try {
                                NativeDaemonEvent event = NativeDaemonEvent.parseRawEvent(new String(buffer, start, i - start, StandardCharsets.UTF_8), fdList);
                                log("RCV <- {" + event + "}");
                                if (event.isClassUnsolicited()) {
                                    if (this.mCallbacks.onCheckHoldWakeLock(event.getCode()) && this.mWakeLock != null) {
                                        this.mWakeLock.acquire();
                                        releaseWl = true;
                                    }
                                    if (this.mCallbackHandler.sendMessage(this.mCallbackHandler.obtainMessage(event.getCode(), uptimeMillisInt(), 0, event.getRawEvent()))) {
                                        releaseWl = false;
                                    }
                                } else {
                                    this.mResponseQueue.add(event.getCmdNumber(), event);
                                }
                                if (releaseWl) {
                                    this.mWakeLock.release();
                                }
                            } catch (IllegalArgumentException e) {
                                log("Problem parsing message " + e);
                                if (null != null) {
                                    this.mWakeLock.release();
                                }
                            } catch (Throwable th2) {
                                if (null != null) {
                                    this.mWakeLock.release();
                                }
                            }
                            start = i + 1;
                        }
                    }
                    if (start == 0) {
                        log("RCV incomplete");
                    }
                    if (start != count) {
                        int remaining = 4096 - start;
                        System.arraycopy(buffer, start, buffer, 0, remaining);
                        start = remaining;
                    } else {
                        start = 0;
                    }
                }
                loge("got " + count + " reading with start = " + start);
                synchronized (this.mDaemonLock) {
                    if (this.mOutputStream != null) {
                        try {
                            loge("closing stream for " + this.mSocket);
                            this.mOutputStream.close();
                        } catch (IOException e2) {
                            loge("Failed closing output stream: " + e2);
                        }
                        this.mOutputStream = null;
                    }
                }
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException ex2) {
                        loge("Failed closing socket: " + ex2);
                    }
                }
            } catch (IOException e3) {
                ex2 = e3;
                localSocket = socket;
            } catch (Throwable th3) {
                th = th3;
                localSocket = socket;
            }
        } catch (IOException e4) {
            ex2 = e4;
            try {
                loge("Communications error: " + ex2);
                throw ex2;
            } catch (Throwable th4) {
                th = th4;
                synchronized (this.mDaemonLock) {
                    if (this.mOutputStream != null) {
                        try {
                            loge("closing stream for " + this.mSocket);
                            this.mOutputStream.close();
                        } catch (IOException e22) {
                            loge("Failed closing output stream: " + e22);
                        }
                        this.mOutputStream = null;
                    }
                }
                if (localSocket != null) {
                    try {
                        localSocket.close();
                    } catch (IOException ex22) {
                        loge("Failed closing socket: " + ex22);
                    }
                }
                throw th;
            }
        }
    }

    static void makeCommand(StringBuilder rawBuilder, StringBuilder logBuilder, int sequenceNumber, String cmd, Object... args) {
        if (cmd.indexOf(0) >= 0) {
            throw new IllegalArgumentException("Unexpected command: " + cmd);
        } else if (cmd.indexOf(32) >= 0) {
            throw new IllegalArgumentException("Arguments must be separate from command");
        } else {
            rawBuilder.append(sequenceNumber).append(' ').append(cmd);
            logBuilder.append(sequenceNumber).append(' ').append(cmd);
            for (Object arg : args) {
                String argString = String.valueOf(arg);
                if (argString.indexOf(0) >= 0) {
                    throw new IllegalArgumentException("Unexpected argument: " + arg);
                }
                rawBuilder.append(' ');
                logBuilder.append(' ');
                appendEscaped(rawBuilder, argString);
                if (arg instanceof SensitiveArg) {
                    logBuilder.append("[scrubbed]");
                } else {
                    appendEscaped(logBuilder, argString);
                }
            }
            rawBuilder.append('\u0000');
        }
    }

    public void waitForCallbacks() {
        if (Thread.currentThread() == this.mLooper.getThread()) {
            throw new IllegalStateException("Must not call this method on callback thread");
        }
        final CountDownLatch latch = new CountDownLatch(1);
        this.mCallbackHandler.post(new Runnable() {
            public void run() {
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            Slog.wtf(this.TAG, "Interrupted while waiting for unsolicited response handling", e);
        }
    }

    public NativeDaemonEvent execute(Command cmd) throws NativeDaemonConnectorException {
        return execute(cmd.mCmd, cmd.mArguments.toArray());
    }

    public NativeDaemonEvent execute(String cmd, Object... args) throws NativeDaemonConnectorException {
        return execute(60000, cmd, args);
    }

    public NativeDaemonEvent execute(long timeoutMs, String cmd, Object... args) throws NativeDaemonConnectorException {
        NativeDaemonEvent[] events = executeForList(timeoutMs, cmd, args);
        if (events.length == 1) {
            return events[0];
        }
        throw new NativeDaemonConnectorException("Expected exactly one response, but received " + events.length);
    }

    public NativeDaemonEvent[] executeForList(Command cmd) throws NativeDaemonConnectorException {
        return executeForList(cmd.mCmd, cmd.mArguments.toArray());
    }

    public NativeDaemonEvent[] executeForList(String cmd, Object... args) throws NativeDaemonConnectorException {
        return executeForList(60000, cmd, args);
    }

    public NativeDaemonEvent[] executeForList(long timeoutMs, String cmd, Object... args) throws NativeDaemonConnectorException {
        if (this.mWarnIfHeld != null && Thread.holdsLock(this.mWarnIfHeld)) {
            Slog.wtf(this.TAG, "Calling thread " + Thread.currentThread().getName() + " is holding 0x" + Integer.toHexString(System.identityHashCode(this.mWarnIfHeld)), new Throwable());
        }
        long startTime = SystemClock.elapsedRealtime();
        ArrayList<NativeDaemonEvent> events = Lists.newArrayList();
        StringBuilder rawBuilder = new StringBuilder();
        StringBuilder logBuilder = new StringBuilder();
        int sequenceNumber = this.mSequenceNumber.incrementAndGet();
        makeCommand(rawBuilder, logBuilder, sequenceNumber, cmd, args);
        String rawCmd = rawBuilder.toString();
        String logCmd = logBuilder.toString();
        log("SND -> {" + logCmd + "}");
        synchronized (this.mDaemonLock) {
            if (this.mOutputStream == null) {
                throw new NativeDaemonConnectorException("missing output stream");
            }
            try {
                this.mOutputStream.write(rawCmd.getBytes(StandardCharsets.UTF_8));
            } catch (Throwable e) {
                throw new NativeDaemonConnectorException("problem sending command", e);
            }
        }
        NativeDaemonEvent event;
        do {
            event = this.mResponseQueue.remove(sequenceNumber, timeoutMs, logCmd);
            if (event == null) {
                loge("timed-out waiting for response to " + logCmd);
                throw new NativeDaemonTimeoutException(logCmd, event);
            }
            events.add(event);
        } while (event.isClassContinue());
        long endTime = SystemClock.elapsedRealtime();
        if (endTime - startTime > 500) {
            loge("NDC Command {" + logCmd + "} took too long (" + (endTime - startTime) + "ms)");
        }
        if (event.isClassClientError()) {
            throw new NativeDaemonArgumentException(logCmd, event);
        } else if (!event.isClassServerError()) {
            return (NativeDaemonEvent[]) events.toArray(new NativeDaemonEvent[events.size()]);
        } else {
            throw new NativeDaemonFailureException(logCmd, event);
        }
    }

    static void appendEscaped(StringBuilder builder, String arg) {
        boolean hasSpaces = arg.indexOf(32) >= 0;
        if (hasSpaces) {
            builder.append('\"');
        }
        int length = arg.length();
        for (int i = 0; i < length; i++) {
            char c = arg.charAt(i);
            if (c == '\"') {
                builder.append("\\\"");
            } else if (c == '\\') {
                builder.append("\\\\");
            } else {
                builder.append(c);
            }
        }
        if (hasSpaces) {
            builder.append('\"');
        }
    }

    public void monitor() {
        synchronized (this.mDaemonLock) {
        }
    }

    public void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        this.mLocalLog.dump(fd, pw, args);
        pw.println();
        this.mResponseQueue.dump(fd, pw, args);
    }

    private void log(String logstring) {
        if (this.mDebug) {
            Slog.d(this.TAG, logstring);
        }
        this.mLocalLog.log(logstring);
    }

    private void loge(String logstring) {
        Slog.e(this.TAG, logstring);
        this.mLocalLog.log(logstring);
    }
}
