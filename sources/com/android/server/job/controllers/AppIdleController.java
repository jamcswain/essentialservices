package com.android.server.job.controllers;

import android.app.usage.UsageStatsManagerInternal;
import android.content.Context;
import android.os.UserHandle;
import com.android.server.LocalServices;
import com.android.server.job.JobSchedulerService;
import com.android.server.job.JobStore.JobStatusFunctor;
import java.io.PrintWriter;

public final class AppIdleController extends StateController {
    private static final boolean DEBUG = false;
    private static final String LOG_TAG = "AppIdleController";
    private static volatile AppIdleController sController;
    private static Object sCreationLock = new Object();
    boolean mAppIdleParoleOn = true;
    private boolean mInitializedParoleOn;
    private final JobSchedulerService mJobSchedulerService;
    private final UsageStatsManagerInternal mUsageStatsInternal = ((UsageStatsManagerInternal) LocalServices.getService(UsageStatsManagerInternal.class));

    private final class AppIdleStateChangeListener extends android.app.usage.UsageStatsManagerInternal.AppIdleStateChangeListener {
        private AppIdleStateChangeListener() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onAppIdleStateChanged(java.lang.String r5, int r6, boolean r7) {
            /*
            r4 = this;
            r0 = 0;
            r2 = com.android.server.job.controllers.AppIdleController.this;
            r3 = r2.mLock;
            monitor-enter(r3);
            r2 = com.android.server.job.controllers.AppIdleController.this;	 Catch:{ all -> 0x0030 }
            r2 = r2.mAppIdleParoleOn;	 Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x000e;
        L_0x000c:
            monitor-exit(r3);
            return;
        L_0x000e:
            r1 = new com.android.server.job.controllers.AppIdleController$PackageUpdateFunc;	 Catch:{ all -> 0x0030 }
            r1.<init>(r6, r5, r7);	 Catch:{ all -> 0x0030 }
            r2 = com.android.server.job.controllers.AppIdleController.this;	 Catch:{ all -> 0x0030 }
            r2 = r2.mJobSchedulerService;	 Catch:{ all -> 0x0030 }
            r2 = r2.getJobStore();	 Catch:{ all -> 0x0030 }
            r2.forEachJob(r1);	 Catch:{ all -> 0x0030 }
            r2 = r1.mChanged;	 Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x0025;
        L_0x0024:
            r0 = 1;
        L_0x0025:
            monitor-exit(r3);
            if (r0 == 0) goto L_0x002f;
        L_0x0028:
            r2 = com.android.server.job.controllers.AppIdleController.this;
            r2 = r2.mStateChangedListener;
            r2.onControllerStateChanged();
        L_0x002f:
            return;
        L_0x0030:
            r2 = move-exception;
            monitor-exit(r3);
            throw r2;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.job.controllers.AppIdleController.AppIdleStateChangeListener.onAppIdleStateChanged(java.lang.String, int, boolean):void");
        }

        public void onParoleStateChanged(boolean isParoleOn) {
            AppIdleController.this.setAppIdleParoleOn(isParoleOn);
        }
    }

    final class GlobalUpdateFunc implements JobStatusFunctor {
        boolean mChanged;

        GlobalUpdateFunc() {
        }

        public void process(JobStatus jobStatus) {
            if (jobStatus.setAppNotIdleConstraintSatisfied((!AppIdleController.this.mAppIdleParoleOn ? AppIdleController.this.mUsageStatsInternal.isAppIdle(jobStatus.getSourcePackageName(), jobStatus.getSourceUid(), jobStatus.getSourceUserId()) : 0) ^ 1)) {
                this.mChanged = true;
            }
        }
    }

    static final class PackageUpdateFunc implements JobStatusFunctor {
        boolean mChanged;
        final boolean mIdle;
        final String mPackage;
        final int mUserId;

        PackageUpdateFunc(int userId, String pkg, boolean idle) {
            this.mUserId = userId;
            this.mPackage = pkg;
            this.mIdle = idle;
        }

        public void process(JobStatus jobStatus) {
            if (jobStatus.getSourcePackageName().equals(this.mPackage) && jobStatus.getSourceUserId() == this.mUserId && jobStatus.setAppNotIdleConstraintSatisfied(this.mIdle ^ 1)) {
                this.mChanged = true;
            }
        }
    }

    public static AppIdleController get(JobSchedulerService service) {
        AppIdleController appIdleController;
        synchronized (sCreationLock) {
            if (sController == null) {
                sController = new AppIdleController(service, service.getContext(), service.getLock());
            }
            appIdleController = sController;
        }
        return appIdleController;
    }

    private AppIdleController(JobSchedulerService service, Context context, Object lock) {
        super(service, context, lock);
        this.mJobSchedulerService = service;
        this.mUsageStatsInternal.addAppIdleStateChangeListener(new AppIdleStateChangeListener());
    }

    public void maybeStartTrackingJobLocked(JobStatus jobStatus, JobStatus lastJob) {
        if (!this.mInitializedParoleOn) {
            this.mInitializedParoleOn = true;
            this.mAppIdleParoleOn = this.mUsageStatsInternal.isAppIdleParoleOn();
        }
        jobStatus.setAppNotIdleConstraintSatisfied((!this.mAppIdleParoleOn ? this.mUsageStatsInternal.isAppIdle(jobStatus.getSourcePackageName(), jobStatus.getSourceUid(), jobStatus.getSourceUserId()) : 0) ^ 1);
    }

    public void maybeStopTrackingJobLocked(JobStatus jobStatus, JobStatus incomingJob, boolean forUpdate) {
    }

    public void dumpControllerStateLocked(final PrintWriter pw, final int filterUid) {
        pw.print("AppIdle: parole on = ");
        pw.println(this.mAppIdleParoleOn);
        this.mJobSchedulerService.getJobStore().forEachJob(new JobStatusFunctor() {
            public void process(JobStatus jobStatus) {
                if (jobStatus.shouldDump(filterUid)) {
                    pw.print("  #");
                    jobStatus.printUniqueId(pw);
                    pw.print(" from ");
                    UserHandle.formatUid(pw, jobStatus.getSourceUid());
                    pw.print(": ");
                    pw.print(jobStatus.getSourcePackageName());
                    if ((jobStatus.satisfiedConstraints & 134217728) != 0) {
                        pw.println(" RUNNABLE");
                    } else {
                        pw.println(" WAITING");
                    }
                }
            }
        });
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void setAppIdleParoleOn(boolean r5) {
        /*
        r4 = this;
        r0 = 0;
        r3 = r4.mLock;
        monitor-enter(r3);
        r2 = r4.mAppIdleParoleOn;	 Catch:{ all -> 0x0028 }
        if (r2 != r5) goto L_0x000a;
    L_0x0008:
        monitor-exit(r3);
        return;
    L_0x000a:
        r4.mAppIdleParoleOn = r5;	 Catch:{ all -> 0x0028 }
        r1 = new com.android.server.job.controllers.AppIdleController$GlobalUpdateFunc;	 Catch:{ all -> 0x0028 }
        r1.<init>();	 Catch:{ all -> 0x0028 }
        r2 = r4.mJobSchedulerService;	 Catch:{ all -> 0x0028 }
        r2 = r2.getJobStore();	 Catch:{ all -> 0x0028 }
        r2.forEachJob(r1);	 Catch:{ all -> 0x0028 }
        r2 = r1.mChanged;	 Catch:{ all -> 0x0028 }
        if (r2 == 0) goto L_0x001f;
    L_0x001e:
        r0 = 1;
    L_0x001f:
        monitor-exit(r3);
        if (r0 == 0) goto L_0x0027;
    L_0x0022:
        r2 = r4.mStateChangedListener;
        r2.onControllerStateChanged();
    L_0x0027:
        return;
    L_0x0028:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.job.controllers.AppIdleController.setAppIdleParoleOn(boolean):void");
    }
}
