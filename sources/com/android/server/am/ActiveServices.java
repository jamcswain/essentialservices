package com.android.server.am;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.app.IApplicationThread;
import android.app.IServiceConnection;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ServiceStartArgs;
import android.content.ComponentName;
import android.content.IIntentSender;
import android.content.Intent;
import android.content.Intent.FilterComparison;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.ParceledListSlice;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteCallback;
import android.os.RemoteCallback.OnResultListener;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.TransactionTooLargeException;
import android.os.UserHandle;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.EventLog;
import android.util.PrintWriterPrinter;
import android.util.Slog;
import android.util.SparseArray;
import android.util.TimeUtils;
import android.webkit.WebViewZygote;
import com.android.internal.app.procstats.ServiceState;
import com.android.internal.os.BatteryStatsImpl;
import com.android.internal.os.BatteryStatsImpl.Uid.Pkg.Serv;
import com.android.internal.os.TransferPipe;
import com.android.server.job.controllers.JobStatus;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class ActiveServices {
    private static final boolean DEBUG_DELAYED_SERVICE = false;
    private static final boolean DEBUG_DELAYED_STARTS = false;
    static final int LAST_ANR_LIFETIME_DURATION_MSECS = 7200000;
    private static final boolean LOG_SERVICE_START_STOP = false;
    static final int SERVICE_BACKGROUND_TIMEOUT = 200000;
    static final int SERVICE_START_FOREGROUND_TIMEOUT = 5000;
    static final int SERVICE_TIMEOUT = 20000;
    private static final String TAG = "ActivityManager";
    private static final String TAG_MU = (TAG + "_MU");
    private static final String TAG_SERVICE = (TAG + ActivityManagerDebugConfig.POSTFIX_SERVICE);
    private static final String TAG_SERVICE_EXECUTING = (TAG + ActivityManagerDebugConfig.POSTFIX_SERVICE_EXECUTING);
    final ActivityManagerService mAm;
    final ArrayList<ServiceRecord> mDestroyingServices = new ArrayList();
    String mLastAnrDump;
    final Runnable mLastAnrDumpClearer = new Runnable() {
        public void run() {
            synchronized (ActiveServices.this.mAm) {
                try {
                    ActivityManagerService.boostPriorityForLockedSection();
                    ActiveServices.this.mLastAnrDump = null;
                } finally {
                    ActivityManagerService.resetPriorityAfterLockedSection();
                }
            }
        }
    };
    final int mMaxStartingBackground;
    final ArrayList<ServiceRecord> mPendingServices = new ArrayList();
    final ArrayList<ServiceRecord> mRestartingServices = new ArrayList();
    boolean mScreenOn = true;
    final ArrayMap<IBinder, ArrayList<ConnectionRecord>> mServiceConnections = new ArrayMap();
    final SparseArray<ServiceMap> mServiceMap = new SparseArray();
    private ArrayList<ServiceRecord> mTmpCollectionResults = null;

    static final class ActiveForegroundApp {
        boolean mAppOnTop;
        long mEndTime;
        long mHideTime;
        CharSequence mLabel;
        int mNumActive;
        String mPackageName;
        boolean mShownWhileScreenOn;
        boolean mShownWhileTop;
        long mStartTime;
        long mStartVisibleTime;
        int mUid;

        ActiveForegroundApp() {
        }
    }

    final class ServiceDumper {
        private final String[] args;
        private final boolean dumpAll;
        private final String dumpPackage;
        private final FileDescriptor fd;
        private final ItemMatcher matcher;
        private boolean needSep = false;
        private final long nowReal = SystemClock.elapsedRealtime();
        private boolean printed = false;
        private boolean printedAnything = false;
        private final PrintWriter pw;
        private final ArrayList<ServiceRecord> services = new ArrayList();

        ServiceDumper(FileDescriptor fd, PrintWriter pw, String[] args, int opti, boolean dumpAll, String dumpPackage) {
            this.fd = fd;
            this.pw = pw;
            this.args = args;
            this.dumpAll = dumpAll;
            this.dumpPackage = dumpPackage;
            this.matcher = new ItemMatcher();
            this.matcher.build(args, opti);
            for (int user : ActiveServices.this.mAm.mUserController.getUsers()) {
                ServiceMap smap = ActiveServices.this.getServiceMapLocked(user);
                if (smap.mServicesByName.size() > 0) {
                    for (int si = 0; si < smap.mServicesByName.size(); si++) {
                        ServiceRecord r = (ServiceRecord) smap.mServicesByName.valueAt(si);
                        if (this.matcher.match(r, r.name)) {
                            if (dumpPackage != null) {
                                if ((dumpPackage.equals(r.appInfo.packageName) ^ 1) != 0) {
                                }
                            }
                            this.services.add(r);
                        }
                    }
                }
            }
        }

        private void dumpHeaderLocked() {
            this.pw.println("ACTIVITY MANAGER SERVICES (dumpsys activity services)");
            if (ActiveServices.this.mLastAnrDump != null) {
                this.pw.println("  Last ANR service:");
                this.pw.print(ActiveServices.this.mLastAnrDump);
                this.pw.println();
            }
        }

        void dumpLocked() {
            dumpHeaderLocked();
            try {
                for (int user : ActiveServices.this.mAm.mUserController.getUsers()) {
                    int serviceIdx = 0;
                    while (serviceIdx < this.services.size() && ((ServiceRecord) this.services.get(serviceIdx)).userId != user) {
                        serviceIdx++;
                    }
                    this.printed = false;
                    if (serviceIdx < this.services.size()) {
                        this.needSep = false;
                        while (serviceIdx < this.services.size()) {
                            ServiceRecord r = (ServiceRecord) this.services.get(serviceIdx);
                            serviceIdx++;
                            if (r.userId != user) {
                                break;
                            }
                            dumpServiceLocalLocked(r);
                        }
                        this.needSep |= this.printed;
                    }
                    dumpUserRemainsLocked(user);
                }
            } catch (Exception e) {
                Slog.w(ActiveServices.TAG, "Exception in dumpServicesLocked", e);
            }
            dumpRemainsLocked();
        }

        void dumpWithClient() {
            synchronized (ActiveServices.this.mAm) {
                try {
                    ActivityManagerService.boostPriorityForLockedSection();
                    dumpHeaderLocked();
                } finally {
                    ActivityManagerService.resetPriorityAfterLockedSection();
                }
            }
            try {
                for (int user : ActiveServices.this.mAm.mUserController.getUsers()) {
                    int serviceIdx = 0;
                    while (serviceIdx < this.services.size() && ((ServiceRecord) this.services.get(serviceIdx)).userId != user) {
                        serviceIdx++;
                    }
                    this.printed = false;
                    if (serviceIdx < this.services.size()) {
                        this.needSep = false;
                        while (serviceIdx < this.services.size()) {
                            ServiceRecord r = (ServiceRecord) this.services.get(serviceIdx);
                            serviceIdx++;
                            if (r.userId != user) {
                                break;
                            }
                            synchronized (ActiveServices.this.mAm) {
                                ActivityManagerService.boostPriorityForLockedSection();
                                dumpServiceLocalLocked(r);
                            }
                            ActivityManagerService.resetPriorityAfterLockedSection();
                            dumpServiceClient(r);
                        }
                        this.needSep |= this.printed;
                    }
                    synchronized (ActiveServices.this.mAm) {
                        ActivityManagerService.boostPriorityForLockedSection();
                        dumpUserRemainsLocked(user);
                    }
                    ActivityManagerService.resetPriorityAfterLockedSection();
                }
            } catch (Exception e) {
                Slog.w(ActiveServices.TAG, "Exception in dumpServicesLocked", e);
            } catch (Throwable th) {
                ActivityManagerService.resetPriorityAfterLockedSection();
            }
            synchronized (ActiveServices.this.mAm) {
                try {
                    ActivityManagerService.boostPriorityForLockedSection();
                    dumpRemainsLocked();
                } finally {
                    ActivityManagerService.resetPriorityAfterLockedSection();
                }
            }
        }

        private void dumpUserHeaderLocked(int user) {
            if (!this.printed) {
                if (this.printedAnything) {
                    this.pw.println();
                }
                this.pw.println("  User " + user + " active services:");
                this.printed = true;
            }
            this.printedAnything = true;
            if (this.needSep) {
                this.pw.println();
            }
        }

        private void dumpServiceLocalLocked(ServiceRecord r) {
            dumpUserHeaderLocked(r.userId);
            this.pw.print("  * ");
            this.pw.println(r);
            if (this.dumpAll) {
                r.dump(this.pw, "    ");
                this.needSep = true;
                return;
            }
            this.pw.print("    app=");
            this.pw.println(r.app);
            this.pw.print("    created=");
            TimeUtils.formatDuration(r.createTime, this.nowReal, this.pw);
            this.pw.print(" started=");
            this.pw.print(r.startRequested);
            this.pw.print(" connections=");
            this.pw.println(r.connections.size());
            if (r.connections.size() > 0) {
                this.pw.println("    Connections:");
                for (int conni = 0; conni < r.connections.size(); conni++) {
                    ArrayList<ConnectionRecord> clist = (ArrayList) r.connections.valueAt(conni);
                    for (int i = 0; i < clist.size(); i++) {
                        String toShortString;
                        ConnectionRecord conn = (ConnectionRecord) clist.get(i);
                        this.pw.print("      ");
                        this.pw.print(conn.binding.intent.intent.getIntent().toShortString(false, false, false, false));
                        this.pw.print(" -> ");
                        ProcessRecord proc = conn.binding.client;
                        PrintWriter printWriter = this.pw;
                        if (proc != null) {
                            toShortString = proc.toShortString();
                        } else {
                            toShortString = "null";
                        }
                        printWriter.println(toShortString);
                    }
                }
            }
        }

        private void dumpServiceClient(ServiceRecord r) {
            ProcessRecord proc = r.app;
            if (proc != null) {
                IApplicationThread thread = proc.thread;
                if (thread != null) {
                    this.pw.println("    Client:");
                    this.pw.flush();
                    TransferPipe tp;
                    try {
                        tp = new TransferPipe();
                        thread.dumpService(tp.getWriteFd(), r, this.args);
                        tp.setBufferPrefix("      ");
                        tp.go(this.fd, 2000);
                        tp.kill();
                    } catch (IOException e) {
                        this.pw.println("      Failure while dumping the service: " + e);
                    } catch (RemoteException e2) {
                        this.pw.println("      Got a RemoteException while dumping the service");
                    } catch (Throwable th) {
                        tp.kill();
                    }
                    this.needSep = true;
                }
            }
        }

        private void dumpUserRemainsLocked(int user) {
            int si;
            ServiceMap smap = ActiveServices.this.getServiceMapLocked(user);
            this.printed = false;
            int SN = smap.mDelayedStartList.size();
            for (si = 0; si < SN; si++) {
                ServiceRecord r = (ServiceRecord) smap.mDelayedStartList.get(si);
                if (this.matcher.match(r, r.name) && (this.dumpPackage == null || (this.dumpPackage.equals(r.appInfo.packageName) ^ 1) == 0)) {
                    if (!this.printed) {
                        if (this.printedAnything) {
                            this.pw.println();
                        }
                        this.pw.println("  User " + user + " delayed start services:");
                        this.printed = true;
                    }
                    this.printedAnything = true;
                    this.pw.print("  * Delayed start ");
                    this.pw.println(r);
                }
            }
            this.printed = false;
            SN = smap.mStartingBackground.size();
            for (si = 0; si < SN; si++) {
                r = (ServiceRecord) smap.mStartingBackground.get(si);
                if (this.matcher.match(r, r.name) && (this.dumpPackage == null || (this.dumpPackage.equals(r.appInfo.packageName) ^ 1) == 0)) {
                    if (!this.printed) {
                        if (this.printedAnything) {
                            this.pw.println();
                        }
                        this.pw.println("  User " + user + " starting in background:");
                        this.printed = true;
                    }
                    this.printedAnything = true;
                    this.pw.print("  * Starting bg ");
                    this.pw.println(r);
                }
            }
        }

        private void dumpRemainsLocked() {
            int i;
            ServiceRecord r;
            if (ActiveServices.this.mPendingServices.size() > 0) {
                this.printed = false;
                for (i = 0; i < ActiveServices.this.mPendingServices.size(); i++) {
                    r = (ServiceRecord) ActiveServices.this.mPendingServices.get(i);
                    if (this.matcher.match(r, r.name) && (this.dumpPackage == null || (this.dumpPackage.equals(r.appInfo.packageName) ^ 1) == 0)) {
                        this.printedAnything = true;
                        if (!this.printed) {
                            if (this.needSep) {
                                this.pw.println();
                            }
                            this.needSep = true;
                            this.pw.println("  Pending services:");
                            this.printed = true;
                        }
                        this.pw.print("  * Pending ");
                        this.pw.println(r);
                        r.dump(this.pw, "    ");
                    }
                }
                this.needSep = true;
            }
            if (ActiveServices.this.mRestartingServices.size() > 0) {
                this.printed = false;
                for (i = 0; i < ActiveServices.this.mRestartingServices.size(); i++) {
                    r = (ServiceRecord) ActiveServices.this.mRestartingServices.get(i);
                    if (this.matcher.match(r, r.name) && (this.dumpPackage == null || (this.dumpPackage.equals(r.appInfo.packageName) ^ 1) == 0)) {
                        this.printedAnything = true;
                        if (!this.printed) {
                            if (this.needSep) {
                                this.pw.println();
                            }
                            this.needSep = true;
                            this.pw.println("  Restarting services:");
                            this.printed = true;
                        }
                        this.pw.print("  * Restarting ");
                        this.pw.println(r);
                        r.dump(this.pw, "    ");
                    }
                }
                this.needSep = true;
            }
            if (ActiveServices.this.mDestroyingServices.size() > 0) {
                this.printed = false;
                for (i = 0; i < ActiveServices.this.mDestroyingServices.size(); i++) {
                    r = (ServiceRecord) ActiveServices.this.mDestroyingServices.get(i);
                    if (this.matcher.match(r, r.name) && (this.dumpPackage == null || (this.dumpPackage.equals(r.appInfo.packageName) ^ 1) == 0)) {
                        this.printedAnything = true;
                        if (!this.printed) {
                            if (this.needSep) {
                                this.pw.println();
                            }
                            this.needSep = true;
                            this.pw.println("  Destroying services:");
                            this.printed = true;
                        }
                        this.pw.print("  * Destroy ");
                        this.pw.println(r);
                        r.dump(this.pw, "    ");
                    }
                }
                this.needSep = true;
            }
            if (this.dumpAll) {
                this.printed = false;
                for (int ic = 0; ic < ActiveServices.this.mServiceConnections.size(); ic++) {
                    ArrayList<ConnectionRecord> r2 = (ArrayList) ActiveServices.this.mServiceConnections.valueAt(ic);
                    for (i = 0; i < r2.size(); i++) {
                        ConnectionRecord cr = (ConnectionRecord) r2.get(i);
                        if (this.matcher.match(cr.binding.service, cr.binding.service.name) && (this.dumpPackage == null || (cr.binding.client != null && (this.dumpPackage.equals(cr.binding.client.info.packageName) ^ 1) == 0))) {
                            this.printedAnything = true;
                            if (!this.printed) {
                                if (this.needSep) {
                                    this.pw.println();
                                }
                                this.needSep = true;
                                this.pw.println("  Connection bindings to services:");
                                this.printed = true;
                            }
                            this.pw.print("  * ");
                            this.pw.println(cr);
                            cr.dump(this.pw, "    ");
                        }
                    }
                }
            }
            if (this.matcher.all) {
                long nowElapsed = SystemClock.elapsedRealtime();
                for (int user : ActiveServices.this.mAm.mUserController.getUsers()) {
                    boolean printedUser = false;
                    ServiceMap smap = (ServiceMap) ActiveServices.this.mServiceMap.get(user);
                    if (smap != null) {
                        for (i = smap.mActiveForegroundApps.size() - 1; i >= 0; i--) {
                            ActiveForegroundApp aa = (ActiveForegroundApp) smap.mActiveForegroundApps.valueAt(i);
                            if (this.dumpPackage == null || (this.dumpPackage.equals(aa.mPackageName) ^ 1) == 0) {
                                if (!printedUser) {
                                    printedUser = true;
                                    this.printedAnything = true;
                                    if (this.needSep) {
                                        this.pw.println();
                                    }
                                    this.needSep = true;
                                    this.pw.print("Active foreground apps - user ");
                                    this.pw.print(user);
                                    this.pw.println(":");
                                }
                                this.pw.print("  #");
                                this.pw.print(i);
                                this.pw.print(": ");
                                this.pw.println(aa.mPackageName);
                                if (aa.mLabel != null) {
                                    this.pw.print("    mLabel=");
                                    this.pw.println(aa.mLabel);
                                }
                                this.pw.print("    mNumActive=");
                                this.pw.print(aa.mNumActive);
                                this.pw.print(" mAppOnTop=");
                                this.pw.print(aa.mAppOnTop);
                                this.pw.print(" mShownWhileTop=");
                                this.pw.print(aa.mShownWhileTop);
                                this.pw.print(" mShownWhileScreenOn=");
                                this.pw.println(aa.mShownWhileScreenOn);
                                this.pw.print("    mStartTime=");
                                TimeUtils.formatDuration(aa.mStartTime - nowElapsed, this.pw);
                                this.pw.print(" mStartVisibleTime=");
                                TimeUtils.formatDuration(aa.mStartVisibleTime - nowElapsed, this.pw);
                                this.pw.println();
                                if (aa.mEndTime != 0) {
                                    this.pw.print("    mEndTime=");
                                    TimeUtils.formatDuration(aa.mEndTime - nowElapsed, this.pw);
                                    this.pw.println();
                                }
                            }
                        }
                        if (smap.hasMessagesOrCallbacks()) {
                            if (this.needSep) {
                                this.pw.println();
                            }
                            this.printedAnything = true;
                            this.needSep = true;
                            this.pw.print("  Handler - user ");
                            this.pw.print(user);
                            this.pw.println(":");
                            smap.dumpMine(new PrintWriterPrinter(this.pw), "    ");
                        }
                    }
                }
            }
            if (!this.printedAnything) {
                this.pw.println("  (nothing)");
            }
        }
    }

    private final class ServiceLookupResult {
        final String permission;
        final ServiceRecord record;

        ServiceLookupResult(ServiceRecord _record, String _permission) {
            this.record = _record;
            this.permission = _permission;
        }
    }

    final class ServiceMap extends Handler {
        static final int MSG_BG_START_TIMEOUT = 1;
        static final int MSG_UPDATE_FOREGROUND_APPS = 2;
        final ArrayMap<String, ActiveForegroundApp> mActiveForegroundApps = new ArrayMap();
        boolean mActiveForegroundAppsChanged;
        final ArrayList<ServiceRecord> mDelayedStartList = new ArrayList();
        final ArrayMap<FilterComparison, ServiceRecord> mServicesByIntent = new ArrayMap();
        final ArrayMap<ComponentName, ServiceRecord> mServicesByName = new ArrayMap();
        final ArrayList<ServiceRecord> mStartingBackground = new ArrayList();
        final int mUserId;

        ServiceMap(Looper looper, int userId) {
            super(looper);
            this.mUserId = userId;
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    synchronized (ActiveServices.this.mAm) {
                        try {
                            ActivityManagerService.boostPriorityForLockedSection();
                            rescheduleDelayedStartsLocked();
                        } finally {
                            ActivityManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                case 2:
                    ActiveServices.this.updateForegroundApps(this);
                    return;
                default:
                    return;
            }
        }

        void ensureNotStartingBackgroundLocked(ServiceRecord r) {
            if (this.mStartingBackground.remove(r)) {
                rescheduleDelayedStartsLocked();
            }
            boolean remove = this.mDelayedStartList.remove(r);
        }

        void rescheduleDelayedStartsLocked() {
            removeMessages(1);
            long now = SystemClock.uptimeMillis();
            int i = 0;
            int N = this.mStartingBackground.size();
            while (i < N) {
                ServiceRecord r = (ServiceRecord) this.mStartingBackground.get(i);
                if (r.startingBgTimeout <= now) {
                    Slog.i(ActiveServices.TAG, "Waited long enough for: " + r);
                    this.mStartingBackground.remove(i);
                    N--;
                    i--;
                }
                i++;
            }
            while (this.mDelayedStartList.size() > 0 && this.mStartingBackground.size() < ActiveServices.this.mMaxStartingBackground) {
                r = (ServiceRecord) this.mDelayedStartList.remove(0);
                if (r.pendingStarts.size() <= 0) {
                    Slog.w(ActiveServices.TAG, "**** NO PENDING STARTS! " + r + " startReq=" + r.startRequested + " delayedStop=" + r.delayedStop);
                }
                r.delayed = false;
                try {
                    ActiveServices.this.startServiceInnerLocked(this, ((StartItem) r.pendingStarts.get(0)).intent, r, false, true);
                } catch (TransactionTooLargeException e) {
                }
            }
            if (this.mStartingBackground.size() > 0) {
                ServiceRecord next = (ServiceRecord) this.mStartingBackground.get(0);
                sendMessageAtTime(obtainMessage(1), next.startingBgTimeout > now ? next.startingBgTimeout : now);
            }
            if (this.mStartingBackground.size() < ActiveServices.this.mMaxStartingBackground) {
                ActiveServices.this.mAm.backgroundServicesFinishedLocked(this.mUserId);
            }
        }
    }

    private class ServiceRestarter implements Runnable {
        private ServiceRecord mService;

        private ServiceRestarter() {
        }

        void setService(ServiceRecord service) {
            this.mService = service;
        }

        public void run() {
            synchronized (ActiveServices.this.mAm) {
                try {
                    ActivityManagerService.boostPriorityForLockedSection();
                    ActiveServices.this.performServiceRestartLocked(this.mService);
                } finally {
                    ActivityManagerService.resetPriorityAfterLockedSection();
                }
            }
        }
    }

    public ActiveServices(ActivityManagerService service) {
        this.mAm = service;
        int maxBg = 0;
        try {
            maxBg = Integer.parseInt(SystemProperties.get("ro.config.max_starting_bg", "0"));
        } catch (RuntimeException e) {
        }
        if (maxBg <= 0) {
            maxBg = ActivityManager.isLowRamDeviceStatic() ? 1 : 8;
        }
        this.mMaxStartingBackground = maxBg;
    }

    ServiceRecord getServiceByNameLocked(ComponentName name, int callingUser) {
        return (ServiceRecord) getServiceMapLocked(callingUser).mServicesByName.get(name);
    }

    boolean hasBackgroundServicesLocked(int callingUser) {
        ServiceMap smap = (ServiceMap) this.mServiceMap.get(callingUser);
        if (smap == null || smap.mStartingBackground.size() < this.mMaxStartingBackground) {
            return false;
        }
        return true;
    }

    private ServiceMap getServiceMapLocked(int callingUser) {
        ServiceMap smap = (ServiceMap) this.mServiceMap.get(callingUser);
        if (smap != null) {
            return smap;
        }
        smap = new ServiceMap(this.mAm.mHandler.getLooper(), callingUser);
        this.mServiceMap.put(callingUser, smap);
        return smap;
    }

    ArrayMap<ComponentName, ServiceRecord> getServicesLocked(int callingUser) {
        return getServiceMapLocked(callingUser).mServicesByName;
    }

    ComponentName startServiceLocked(IApplicationThread caller, Intent service, String resolvedType, int callingPid, int callingUid, boolean fgRequired, String callingPackage, int userId) throws TransactionTooLargeException {
        boolean callerFg;
        if (caller != null) {
            ProcessRecord callerApp = this.mAm.getRecordForAppLocked(caller);
            if (callerApp == null) {
                throw new SecurityException("Unable to find app for caller " + caller + " (pid=" + callingPid + ") when starting service " + service);
            }
            callerFg = callerApp.setSchedGroup != 0;
        } else {
            callerFg = true;
        }
        ServiceLookupResult res = retrieveServiceLocked(service, resolvedType, callingPackage, callingPid, callingUid, userId, true, callerFg, false);
        if (res == null) {
            return null;
        }
        if (res.record == null) {
            return new ComponentName("!", res.permission != null ? res.permission : "private to package");
        }
        ServiceRecord r = res.record;
        if (this.mAm.mUserController.exists(r.userId)) {
            if (!(r.startRequested || (fgRequired ^ 1) == 0)) {
                int allowed = this.mAm.getAppStartModeLocked(r.appInfo.uid, r.packageName, r.appInfo.targetSdkVersion, callingPid, false, false);
                if (allowed != 0) {
                    Slog.w(TAG, "Background start not allowed: service " + service + " to " + r.name.flattenToShortString() + " from pid=" + callingPid + " uid=" + callingUid + " pkg=" + callingPackage);
                    if (allowed == 1) {
                        return null;
                    }
                    return new ComponentName("?", "app is in background uid " + ((UidRecord) this.mAm.mActiveUids.get(r.appInfo.uid)));
                }
            }
            NeededUriGrants neededGrants = this.mAm.checkGrantUriPermissionFromIntentLocked(callingUid, r.packageName, service, service.getFlags(), null, r.userId);
            if (this.mAm.mPermissionReviewRequired && !requestStartTargetPermissionsReviewIfNeededLocked(r, callingPackage, callingUid, service, callerFg, userId)) {
                return null;
            }
            boolean unscheduleServiceRestartLocked = unscheduleServiceRestartLocked(r, callingUid, false);
            r.lastActivity = SystemClock.uptimeMillis();
            r.startRequested = true;
            r.delayedStop = false;
            r.fgRequired = fgRequired;
            r.pendingStarts.add(new StartItem(r, false, r.makeNextStartId(), service, neededGrants, callingUid));
            ServiceMap smap = getServiceMapLocked(r.userId);
            boolean addToStarting = false;
            if (!callerFg && (fgRequired ^ 1) != 0 && r.app == null && this.mAm.mUserController.hasStartedUserState(r.userId)) {
                ProcessRecord proc = this.mAm.getProcessRecordLocked(r.processName, r.appInfo.uid, false);
                if (proc == null || proc.curProcState > 12) {
                    if (r.delayed) {
                        return r.name;
                    }
                    if (smap.mStartingBackground.size() >= this.mMaxStartingBackground) {
                        Slog.i(TAG_SERVICE, "Delaying start of: " + r);
                        smap.mDelayedStartList.add(r);
                        r.delayed = true;
                        return r.name;
                    }
                    addToStarting = true;
                } else if (proc.curProcState >= 11) {
                    addToStarting = true;
                }
            }
            return startServiceInnerLocked(smap, service, r, callerFg, addToStarting);
        }
        Slog.w(TAG, "Trying to start service with non-existent user! " + r.userId);
        return null;
    }

    private boolean requestStartTargetPermissionsReviewIfNeededLocked(ServiceRecord r, String callingPackage, int callingUid, Intent service, boolean callerFg, int userId) {
        if (!this.mAm.getPackageManagerInternalLocked().isPermissionsReviewRequired(r.packageName, r.userId)) {
            return true;
        }
        if (callerFg) {
            ActivityManagerService activityManagerService = this.mAm;
            Intent[] intentArr = new Intent[]{service};
            String[] strArr = new String[1];
            strArr[0] = service.resolveType(this.mAm.mContext.getContentResolver());
            IIntentSender target = activityManagerService.getIntentSenderLocked(4, callingPackage, callingUid, userId, null, null, 0, intentArr, strArr, 1409286144, null);
            final Intent intent = new Intent("android.intent.action.REVIEW_PERMISSIONS");
            intent.addFlags(276824064);
            intent.putExtra("android.intent.extra.PACKAGE_NAME", r.packageName);
            intent.putExtra("android.intent.extra.INTENT", new IntentSender(target));
            final int i = userId;
            this.mAm.mHandler.post(new Runnable() {
                public void run() {
                    ActiveServices.this.mAm.mContext.startActivityAsUser(intent, new UserHandle(i));
                }
            });
            return false;
        }
        Slog.w(TAG, "u" + r.userId + " Starting a service in package" + r.packageName + " requires a permissions review");
        return false;
    }

    ComponentName startServiceInnerLocked(ServiceMap smap, Intent service, ServiceRecord r, boolean callerFg, boolean addToStarting) throws TransactionTooLargeException {
        ServiceState stracker = r.getTracker();
        if (stracker != null) {
            stracker.setStarted(true, this.mAm.mProcessStats.getMemFactorLocked(), r.lastActivity);
        }
        r.callStart = false;
        synchronized (r.stats.getBatteryStats()) {
            r.stats.startRunningLocked();
        }
        String error = bringUpServiceLocked(r, service.getFlags(), callerFg, false, false);
        if (error != null) {
            return new ComponentName("!!", error);
        }
        if (r.startRequested && addToStarting) {
            boolean first = smap.mStartingBackground.size() == 0;
            smap.mStartingBackground.add(r);
            r.startingBgTimeout = SystemClock.uptimeMillis() + this.mAm.mConstants.BG_START_TIMEOUT;
            if (first) {
                smap.rescheduleDelayedStartsLocked();
            }
        } else if (callerFg || r.fgRequired) {
            smap.ensureNotStartingBackgroundLocked(r);
        }
        return r.name;
    }

    private void stopServiceLocked(ServiceRecord service) {
        if (service.delayed) {
            service.delayedStop = true;
            return;
        }
        synchronized (service.stats.getBatteryStats()) {
            service.stats.stopRunningLocked();
        }
        service.startRequested = false;
        if (service.tracker != null) {
            service.tracker.setStarted(false, this.mAm.mProcessStats.getMemFactorLocked(), SystemClock.uptimeMillis());
        }
        service.callStart = false;
        bringDownServiceIfNeededLocked(service, false, false);
    }

    int stopServiceLocked(IApplicationThread caller, Intent service, String resolvedType, int userId) {
        ProcessRecord callerApp = this.mAm.getRecordForAppLocked(caller);
        if (caller == null || callerApp != null) {
            ServiceLookupResult r = retrieveServiceLocked(service, resolvedType, null, Binder.getCallingPid(), Binder.getCallingUid(), userId, false, false, false);
            if (r == null) {
                return 0;
            }
            if (r.record == null) {
                return -1;
            }
            long origId = Binder.clearCallingIdentity();
            try {
                stopServiceLocked(r.record);
                return 1;
            } finally {
                Binder.restoreCallingIdentity(origId);
            }
        } else {
            throw new SecurityException("Unable to find app for caller " + caller + " (pid=" + Binder.getCallingPid() + ") when stopping service " + service);
        }
    }

    void stopInBackgroundLocked(int uid) {
        ServiceMap services = (ServiceMap) this.mServiceMap.get(UserHandle.getUserId(uid));
        ArrayList stopping = null;
        if (services != null) {
            int i;
            ServiceRecord service;
            for (i = services.mServicesByName.size() - 1; i >= 0; i--) {
                service = (ServiceRecord) services.mServicesByName.valueAt(i);
                if (service.appInfo.uid == uid && service.startRequested && this.mAm.getAppStartModeLocked(service.appInfo.uid, service.packageName, service.appInfo.targetSdkVersion, -1, false, false) != 0) {
                    if (stopping == null) {
                        stopping = new ArrayList();
                    }
                    String compName = service.name.flattenToShortString();
                    EventLogTags.writeAmStopIdleService(service.appInfo.uid, compName);
                    StringBuilder sb = new StringBuilder(64);
                    sb.append("Stopping service due to app idle: ");
                    UserHandle.formatUid(sb, service.appInfo.uid);
                    sb.append(" ");
                    TimeUtils.formatDuration(service.createTime - SystemClock.elapsedRealtime(), sb);
                    sb.append(" ");
                    sb.append(compName);
                    Slog.w(TAG, sb.toString());
                    stopping.add(service);
                }
            }
            if (stopping != null) {
                for (i = stopping.size() - 1; i >= 0; i--) {
                    service = (ServiceRecord) stopping.get(i);
                    service.delayed = false;
                    services.ensureNotStartingBackgroundLocked(service);
                    stopServiceLocked(service);
                }
            }
        }
    }

    IBinder peekServiceLocked(Intent service, String resolvedType, String callingPackage) {
        ServiceLookupResult r = retrieveServiceLocked(service, resolvedType, callingPackage, Binder.getCallingPid(), Binder.getCallingUid(), UserHandle.getCallingUserId(), false, false, false);
        if (r == null) {
            return null;
        }
        if (r.record == null) {
            throw new SecurityException("Permission Denial: Accessing service from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " requires " + r.permission);
        }
        IntentBindRecord ib = (IntentBindRecord) r.record.bindings.get(r.record.intent);
        if (ib != null) {
            return ib.binder;
        }
        return null;
    }

    boolean stopServiceTokenLocked(ComponentName className, IBinder token, int startId) {
        ServiceRecord r = findServiceLocked(className, token, UserHandle.getCallingUserId());
        if (r == null) {
            return false;
        }
        if (startId >= 0) {
            StartItem si = r.findDeliveredStart(startId, false);
            if (si != null) {
                while (r.deliveredStarts.size() > 0) {
                    StartItem cur = (StartItem) r.deliveredStarts.remove(0);
                    cur.removeUriPermissionsLocked();
                    if (cur == si) {
                        break;
                    }
                }
            }
            if (r.getLastStartId() != startId) {
                return false;
            }
            if (r.deliveredStarts.size() > 0) {
                Slog.w(TAG, "stopServiceToken startId " + startId + " is last, but have " + r.deliveredStarts.size() + " remaining args");
            }
        }
        synchronized (r.stats.getBatteryStats()) {
            r.stats.stopRunningLocked();
        }
        r.startRequested = false;
        if (r.tracker != null) {
            r.tracker.setStarted(false, this.mAm.mProcessStats.getMemFactorLocked(), SystemClock.uptimeMillis());
        }
        r.callStart = false;
        long origId = Binder.clearCallingIdentity();
        bringDownServiceIfNeededLocked(r, false, false);
        Binder.restoreCallingIdentity(origId);
        return true;
    }

    public void setServiceForegroundLocked(ComponentName className, IBinder token, int id, Notification notification, int flags) {
        int userId = UserHandle.getCallingUserId();
        long origId = Binder.clearCallingIdentity();
        try {
            ServiceRecord r = findServiceLocked(className, token, userId);
            if (r != null) {
                setServiceForegroundInnerLocked(r, id, notification, flags);
            }
            Binder.restoreCallingIdentity(origId);
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(origId);
        }
    }

    boolean foregroundAppShownEnoughLocked(ActiveForegroundApp aa, long nowElapsed) {
        aa.mHideTime = JobStatus.NO_LATEST_RUNTIME;
        if (aa.mShownWhileTop) {
            return true;
        }
        long minTime;
        if (this.mScreenOn || aa.mShownWhileScreenOn) {
            long j;
            long j2 = aa.mStartVisibleTime;
            if (aa.mStartTime != aa.mStartVisibleTime) {
                j = this.mAm.mConstants.FGSERVICE_SCREEN_ON_AFTER_TIME;
            } else {
                j = this.mAm.mConstants.FGSERVICE_MIN_SHOWN_TIME;
            }
            minTime = j2 + j;
            if (nowElapsed >= minTime) {
                return true;
            }
            long reportTime = nowElapsed + this.mAm.mConstants.FGSERVICE_MIN_REPORT_TIME;
            if (reportTime <= minTime) {
                reportTime = minTime;
            }
            aa.mHideTime = reportTime;
            return false;
        }
        minTime = aa.mEndTime + this.mAm.mConstants.FGSERVICE_SCREEN_ON_BEFORE_TIME;
        if (nowElapsed >= minTime) {
            return true;
        }
        aa.mHideTime = minTime;
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void updateForegroundApps(com.android.server.am.ActiveServices.ServiceMap r37) {
        /*
        r36 = this;
        r11 = 0;
        r0 = r36;
        r7 = r0.mAm;
        monitor-enter(r7);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x006b }
        r24 = android.os.SystemClock.elapsedRealtime();	 Catch:{ all -> 0x006b }
        r20 = 9223372036854775807; // 0x7fffffffffffffff float:NaN double:NaN;
        if (r37 == 0) goto L_0x009b;
    L_0x0014:
        r0 = r37;
        r5 = r0.mActiveForegroundApps;	 Catch:{ all -> 0x006b }
        r5 = r5.size();	 Catch:{ all -> 0x006b }
        r16 = r5 + -1;
        r12 = r11;
    L_0x001f:
        if (r16 < 0) goto L_0x0071;
    L_0x0021:
        r0 = r37;
        r5 = r0.mActiveForegroundApps;	 Catch:{ all -> 0x0283 }
        r0 = r16;
        r10 = r5.valueAt(r0);	 Catch:{ all -> 0x0283 }
        r10 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r10;	 Catch:{ all -> 0x0283 }
        r8 = r10.mEndTime;	 Catch:{ all -> 0x0283 }
        r34 = 0;
        r5 = (r8 > r34 ? 1 : (r8 == r34 ? 0 : -1));
        if (r5 == 0) goto L_0x005c;
    L_0x0035:
        r0 = r36;
        r1 = r24;
        r14 = r0.foregroundAppShownEnoughLocked(r10, r1);	 Catch:{ all -> 0x0283 }
        if (r14 == 0) goto L_0x0052;
    L_0x003f:
        r0 = r37;
        r5 = r0.mActiveForegroundApps;	 Catch:{ all -> 0x0283 }
        r0 = r16;
        r5.removeAt(r0);	 Catch:{ all -> 0x0283 }
        r5 = 1;
        r0 = r37;
        r0.mActiveForegroundAppsChanged = r5;	 Catch:{ all -> 0x0283 }
        r11 = r12;
    L_0x004e:
        r16 = r16 + -1;
        r12 = r11;
        goto L_0x001f;
    L_0x0052:
        r8 = r10.mHideTime;	 Catch:{ all -> 0x0283 }
        r5 = (r8 > r20 ? 1 : (r8 == r20 ? 0 : -1));
        if (r5 >= 0) goto L_0x005c;
    L_0x0058:
        r0 = r10.mHideTime;	 Catch:{ all -> 0x0283 }
        r20 = r0;
    L_0x005c:
        r5 = r10.mAppOnTop;	 Catch:{ all -> 0x0283 }
        if (r5 != 0) goto L_0x028d;
    L_0x0060:
        if (r12 != 0) goto L_0x028a;
    L_0x0062:
        r11 = new java.util.ArrayList;	 Catch:{ all -> 0x0283 }
        r11.<init>();	 Catch:{ all -> 0x0283 }
    L_0x0067:
        r11.add(r10);	 Catch:{ all -> 0x006b }
        goto L_0x004e;
    L_0x006b:
        r5 = move-exception;
    L_0x006c:
        monitor-exit(r7);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r5;
    L_0x0071:
        r5 = 2;
        r0 = r37;
        r0.removeMessages(r5);	 Catch:{ all -> 0x0283 }
        r8 = 9223372036854775807; // 0x7fffffffffffffff float:NaN double:NaN;
        r5 = (r20 > r8 ? 1 : (r20 == r8 ? 0 : -1));
        if (r5 >= 0) goto L_0x0287;
    L_0x0080:
        r5 = 2;
        r0 = r37;
        r17 = r0.obtainMessage(r5);	 Catch:{ all -> 0x0283 }
        r8 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x0283 }
        r8 = r8 + r20;
        r34 = android.os.SystemClock.elapsedRealtime();	 Catch:{ all -> 0x0283 }
        r8 = r8 - r34;
        r0 = r37;
        r1 = r17;
        r0.sendMessageAtTime(r1, r8);	 Catch:{ all -> 0x0283 }
        r11 = r12;
    L_0x009b:
        r0 = r37;
        r5 = r0.mActiveForegroundAppsChanged;	 Catch:{ all -> 0x006b }
        if (r5 != 0) goto L_0x00a6;
    L_0x00a1:
        monitor-exit(r7);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x00a6:
        r5 = 0;
        r0 = r37;
        r0.mActiveForegroundAppsChanged = r5;	 Catch:{ all -> 0x006b }
        monitor-exit(r7);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        r0 = r36;
        r5 = r0.mAm;
        r5 = r5.mContext;
        r7 = "notification";
        r22 = r5.getSystemService(r7);
        r22 = (android.app.NotificationManager) r22;
        r0 = r36;
        r5 = r0.mAm;
        r4 = r5.mContext;
        if (r11 == 0) goto L_0x0270;
    L_0x00c6:
        r16 = 0;
    L_0x00c8:
        r5 = r11.size();
        r0 = r16;
        if (r0 >= r5) goto L_0x0100;
    L_0x00d0:
        r0 = r16;
        r10 = r11.get(r0);
        r10 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r10;
        r5 = r10.mLabel;
        if (r5 != 0) goto L_0x00f7;
    L_0x00dc:
        r31 = r4.getPackageManager();
        r5 = r10.mPackageName;	 Catch:{ NameNotFoundException -> 0x00fa }
        r0 = r37;
        r7 = r0.mUserId;	 Catch:{ NameNotFoundException -> 0x00fa }
        r8 = 4202496; // 0x402000 float:5.888951E-39 double:2.076309E-317;
        r0 = r31;
        r13 = r0.getApplicationInfoAsUser(r5, r8, r7);	 Catch:{ NameNotFoundException -> 0x00fa }
        r0 = r31;
        r5 = r13.loadLabel(r0);	 Catch:{ NameNotFoundException -> 0x00fa }
        r10.mLabel = r5;	 Catch:{ NameNotFoundException -> 0x00fa }
    L_0x00f7:
        r16 = r16 + 1;
        goto L_0x00c8;
    L_0x00fa:
        r15 = move-exception;
        r5 = r10.mPackageName;
        r10.mLabel = r5;
        goto L_0x00f7;
    L_0x0100:
        r26 = android.os.SystemClock.elapsedRealtime();
        r28 = r26;
        r5 = r11.size();
        r7 = 1;
        if (r5 != r7) goto L_0x01e3;
    L_0x010d:
        r6 = new android.content.Intent;
        r5 = "android.settings.APPLICATION_DETAILS_SETTINGS";
        r6.<init>(r5);
        r7 = "package";
        r5 = 0;
        r5 = r11.get(r5);
        r5 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r5;
        r5 = r5.mPackageName;
        r8 = 0;
        r5 = android.net.Uri.fromParts(r7, r5, r8);
        r6.setData(r5);
        r5 = 1;
        r7 = new java.lang.Object[r5];
        r5 = 0;
        r5 = r11.get(r5);
        r5 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r5;
        r5 = r5.mLabel;
        r8 = 0;
        r7[r8] = r5;
        r5 = 17039896; // 0x1040218 float:2.4246073E-38 double:8.418827E-317;
        r32 = r4.getString(r5, r7);
        r5 = 17039899; // 0x104021b float:2.4246082E-38 double:8.4188287E-317;
        r18 = r4.getString(r5);
        r5 = 1;
        r0 = new java.lang.String[r5];
        r30 = r0;
        r5 = 0;
        r5 = r11.get(r5);
        r5 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r5;
        r5 = r5.mPackageName;
        r7 = 0;
        r30[r7] = r5;
        r5 = 0;
        r5 = r11.get(r5);
        r5 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r5;
        r0 = r5.mStartTime;
        r28 = r0;
    L_0x0162:
        r23 = new android.os.Bundle;
        r23.<init>();
        r5 = "android.foregroundApps";
        r0 = r23;
        r1 = r30;
        r0.putStringArray(r5, r1);
        r5 = new android.app.Notification$Builder;
        r7 = com.android.internal.notification.SystemNotificationChannels.FOREGROUND_SERVICE;
        r5.<init>(r4, r7);
        r0 = r23;
        r5 = r5.addExtras(r0);
        r7 = 17303501; // 0x10807cd float:2.4984852E-38 double:8.5490654E-317;
        r5 = r5.setSmallIcon(r7);
        r7 = 1;
        r7 = r5.setOngoing(r7);
        r5 = (r28 > r26 ? 1 : (r28 == r26 ? 0 : -1));
        if (r5 >= 0) goto L_0x026d;
    L_0x018e:
        r5 = 1;
    L_0x018f:
        r5 = r7.setShowWhen(r5);
        r8 = java.lang.System.currentTimeMillis();
        r34 = r26 - r28;
        r8 = r8 - r34;
        r5 = r5.setWhen(r8);
        r7 = 17170763; // 0x106014b float:2.461284E-38 double:8.483484E-317;
        r7 = r4.getColor(r7);
        r5 = r5.setColor(r7);
        r0 = r32;
        r5 = r5.setContentTitle(r0);
        r0 = r18;
        r33 = r5.setContentText(r0);
        r9 = new android.os.UserHandle;
        r0 = r37;
        r5 = r0.mUserId;
        r9.<init>(r5);
        r5 = 0;
        r7 = 134217728; // 0x8000000 float:3.85186E-34 double:6.63123685E-316;
        r8 = 0;
        r5 = android.app.PendingIntent.getActivityAsUser(r4, r5, r6, r7, r8, r9);
        r0 = r33;
        r19 = r0.setContentIntent(r5);
        r5 = r19.build();
        r7 = new android.os.UserHandle;
        r0 = r37;
        r8 = r0.mUserId;
        r7.<init>(r8);
        r8 = 0;
        r9 = 40;
        r0 = r22;
        r0.notifyAsUser(r8, r9, r5, r7);
    L_0x01e2:
        return;
    L_0x01e3:
        r6 = new android.content.Intent;
        r5 = "android.settings.FOREGROUND_SERVICES_SETTINGS";
        r6.<init>(r5);
        r5 = r11.size();
        r0 = new java.lang.String[r5];
        r30 = r0;
        r16 = 0;
    L_0x01f5:
        r5 = r11.size();
        r0 = r16;
        if (r0 >= r5) goto L_0x021c;
    L_0x01fd:
        r0 = r16;
        r5 = r11.get(r0);
        r5 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r5;
        r5 = r5.mPackageName;
        r30[r16] = r5;
        r0 = r16;
        r5 = r11.get(r0);
        r5 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r5;
        r8 = r5.mStartTime;
        r0 = r28;
        r28 = java.lang.Math.min(r0, r8);
        r16 = r16 + 1;
        goto L_0x01f5;
    L_0x021c:
        r5 = "packages";
        r0 = r30;
        r6.putExtra(r5, r0);
        r5 = 1;
        r5 = new java.lang.Object[r5];
        r7 = r11.size();
        r7 = java.lang.Integer.valueOf(r7);
        r8 = 0;
        r5[r8] = r7;
        r7 = 17039897; // 0x1040219 float:2.4246076E-38 double:8.4188277E-317;
        r32 = r4.getString(r7, r5);
        r5 = 0;
        r5 = r11.get(r5);
        r5 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r5;
        r5 = r5.mLabel;
        r18 = r5.toString();
        r16 = 1;
    L_0x0248:
        r5 = r11.size();
        r0 = r16;
        if (r0 >= r5) goto L_0x0162;
    L_0x0250:
        r5 = 2;
        r7 = new java.lang.Object[r5];
        r5 = 0;
        r7[r5] = r18;
        r0 = r16;
        r5 = r11.get(r0);
        r5 = (com.android.server.am.ActiveServices.ActiveForegroundApp) r5;
        r5 = r5.mLabel;
        r8 = 1;
        r7[r8] = r5;
        r5 = 17039898; // 0x104021a float:2.424608E-38 double:8.418828E-317;
        r18 = r4.getString(r5, r7);
        r16 = r16 + 1;
        goto L_0x0248;
    L_0x026d:
        r5 = 0;
        goto L_0x018f;
    L_0x0270:
        r5 = new android.os.UserHandle;
        r0 = r37;
        r7 = r0.mUserId;
        r5.<init>(r7);
        r7 = 0;
        r8 = 40;
        r0 = r22;
        r0.cancelAsUser(r7, r8, r5);
        goto L_0x01e2;
    L_0x0283:
        r5 = move-exception;
        r11 = r12;
        goto L_0x006c;
    L_0x0287:
        r11 = r12;
        goto L_0x009b;
    L_0x028a:
        r11 = r12;
        goto L_0x0067;
    L_0x028d:
        r11 = r12;
        goto L_0x004e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.ActiveServices.updateForegroundApps(com.android.server.am.ActiveServices$ServiceMap):void");
    }

    private void requestUpdateActiveForegroundAppsLocked(ServiceMap smap, long timeElapsed) {
        Message msg = smap.obtainMessage(2);
        if (timeElapsed != 0) {
            smap.sendMessageAtTime(msg, (SystemClock.uptimeMillis() + timeElapsed) - SystemClock.elapsedRealtime());
            return;
        }
        smap.mActiveForegroundAppsChanged = true;
        smap.sendMessage(msg);
    }

    private void decActiveForegroundAppLocked(ServiceMap smap, ServiceRecord r) {
        ActiveForegroundApp active = (ActiveForegroundApp) smap.mActiveForegroundApps.get(r.packageName);
        if (active != null) {
            active.mNumActive--;
            if (active.mNumActive <= 0) {
                active.mEndTime = SystemClock.elapsedRealtime();
                if (foregroundAppShownEnoughLocked(active, active.mEndTime)) {
                    smap.mActiveForegroundApps.remove(r.packageName);
                    smap.mActiveForegroundAppsChanged = true;
                    requestUpdateActiveForegroundAppsLocked(smap, 0);
                } else if (active.mHideTime < JobStatus.NO_LATEST_RUNTIME) {
                    requestUpdateActiveForegroundAppsLocked(smap, active.mHideTime);
                }
            }
        }
    }

    void updateScreenStateLocked(boolean screenOn) {
        if (this.mScreenOn != screenOn) {
            this.mScreenOn = screenOn;
            if (screenOn) {
                long nowElapsed = SystemClock.elapsedRealtime();
                for (int i = this.mServiceMap.size() - 1; i >= 0; i--) {
                    ServiceMap smap = (ServiceMap) this.mServiceMap.valueAt(i);
                    long nextUpdateTime = JobStatus.NO_LATEST_RUNTIME;
                    boolean changed = false;
                    for (int j = smap.mActiveForegroundApps.size() - 1; j >= 0; j--) {
                        ActiveForegroundApp active = (ActiveForegroundApp) smap.mActiveForegroundApps.valueAt(j);
                        if (active.mEndTime != 0) {
                            if (!active.mShownWhileScreenOn && active.mStartVisibleTime == active.mStartTime) {
                                active.mStartVisibleTime = nowElapsed;
                                active.mEndTime = nowElapsed;
                            }
                            if (foregroundAppShownEnoughLocked(active, nowElapsed)) {
                                smap.mActiveForegroundApps.remove(active.mPackageName);
                                smap.mActiveForegroundAppsChanged = true;
                                changed = true;
                            } else if (active.mHideTime < nextUpdateTime) {
                                nextUpdateTime = active.mHideTime;
                            }
                        } else if (!active.mShownWhileScreenOn) {
                            active.mShownWhileScreenOn = true;
                            active.mStartVisibleTime = nowElapsed;
                        }
                    }
                    if (changed) {
                        requestUpdateActiveForegroundAppsLocked(smap, 0);
                    } else if (nextUpdateTime < JobStatus.NO_LATEST_RUNTIME) {
                        requestUpdateActiveForegroundAppsLocked(smap, nextUpdateTime);
                    }
                }
            }
        }
    }

    void foregroundServiceProcStateChangedLocked(UidRecord uidRec) {
        ServiceMap smap = (ServiceMap) this.mServiceMap.get(UserHandle.getUserId(uidRec.uid));
        if (smap != null) {
            boolean changed = false;
            for (int j = smap.mActiveForegroundApps.size() - 1; j >= 0; j--) {
                ActiveForegroundApp active = (ActiveForegroundApp) smap.mActiveForegroundApps.valueAt(j);
                if (active.mUid == uidRec.uid) {
                    if (uidRec.curProcState <= 2) {
                        if (!active.mAppOnTop) {
                            active.mAppOnTop = true;
                            changed = true;
                        }
                        active.mShownWhileTop = true;
                    } else if (active.mAppOnTop) {
                        active.mAppOnTop = false;
                        changed = true;
                    }
                }
            }
            if (changed) {
                requestUpdateActiveForegroundAppsLocked(smap, 0);
            }
        }
    }

    private void setServiceForegroundInnerLocked(ServiceRecord r, int id, Notification notification, int flags) {
        boolean z = false;
        ServiceMap smap;
        if (id == 0) {
            if (r.isForeground) {
                smap = getServiceMapLocked(r.userId);
                if (smap != null) {
                    decActiveForegroundAppLocked(smap, r);
                }
                r.isForeground = false;
                if (r.app != null) {
                    this.mAm.updateLruProcessLocked(r.app, false, null);
                    updateServiceForegroundLocked(r.app, true);
                }
            }
            if ((flags & 1) != 0) {
                cancelForegroundNotificationLocked(r);
                r.foregroundId = 0;
                r.foregroundNoti = null;
            } else if (r.appInfo.targetSdkVersion >= 21) {
                r.stripForegroundServiceFlagFromNotification();
                if ((flags & 2) != 0) {
                    r.foregroundId = 0;
                    r.foregroundNoti = null;
                }
            }
        } else if (notification == null) {
            throw new IllegalArgumentException("null notification");
        } else {
            if (r.appInfo.isInstantApp()) {
                switch (this.mAm.mAppOpsService.checkOperation(68, r.appInfo.uid, r.appInfo.packageName)) {
                    case 0:
                        break;
                    case 1:
                        Slog.w(TAG, "Instant app " + r.appInfo.packageName + " does not have permission to create foreground services" + ", ignoring.");
                        return;
                    case 2:
                        throw new SecurityException("Instant app " + r.appInfo.packageName + " does not have permission to create foreground services");
                    default:
                        try {
                            if (AppGlobals.getPackageManager().checkPermission("android.permission.INSTANT_APP_FOREGROUND_SERVICE", r.appInfo.packageName, UserHandle.getUserId(r.appInfo.uid)) != 0) {
                                throw new SecurityException("Instant app " + r.appInfo.packageName + " does not have permission to create foreground" + "services");
                            }
                        } catch (RemoteException e) {
                            throw new SecurityException("Failed to check instant app permission.", e);
                        }
                        break;
                }
            }
            if (r.fgRequired) {
                r.fgRequired = false;
                r.fgWaiting = false;
                this.mAm.mHandler.removeMessages(66, r);
            }
            if (r.foregroundId != id) {
                cancelForegroundNotificationLocked(r);
                r.foregroundId = id;
            }
            notification.flags |= 64;
            r.foregroundNoti = notification;
            if (!r.isForeground) {
                smap = getServiceMapLocked(r.userId);
                if (smap != null) {
                    ActiveForegroundApp active = (ActiveForegroundApp) smap.mActiveForegroundApps.get(r.packageName);
                    if (active == null) {
                        active = new ActiveForegroundApp();
                        active.mPackageName = r.packageName;
                        active.mUid = r.appInfo.uid;
                        active.mShownWhileScreenOn = this.mScreenOn;
                        if (r.app != null) {
                            if (r.app.uidRecord.curProcState <= 2) {
                                z = true;
                            }
                            active.mShownWhileTop = z;
                            active.mAppOnTop = z;
                        }
                        long elapsedRealtime = SystemClock.elapsedRealtime();
                        active.mStartVisibleTime = elapsedRealtime;
                        active.mStartTime = elapsedRealtime;
                        smap.mActiveForegroundApps.put(r.packageName, active);
                        requestUpdateActiveForegroundAppsLocked(smap, 0);
                    }
                    active.mNumActive++;
                }
                r.isForeground = true;
            }
            r.postNotification();
            if (r.app != null) {
                updateServiceForegroundLocked(r.app, true);
            }
            getServiceMapLocked(r.userId).ensureNotStartingBackgroundLocked(r);
            this.mAm.notifyPackageUse(r.serviceInfo.packageName, 2);
        }
    }

    private void cancelForegroundNotificationLocked(ServiceRecord r) {
        if (r.foregroundId != 0) {
            ServiceMap sm = getServiceMapLocked(r.userId);
            if (sm != null) {
                int i = sm.mServicesByName.size() - 1;
                while (i >= 0) {
                    ServiceRecord other = (ServiceRecord) sm.mServicesByName.valueAt(i);
                    if (other == r || other.foregroundId != r.foregroundId || !other.packageName.equals(r.packageName)) {
                        i--;
                    } else {
                        return;
                    }
                }
            }
            r.cancelNotification();
        }
    }

    private void updateServiceForegroundLocked(ProcessRecord proc, boolean oomAdj) {
        boolean anyForeground = false;
        for (int i = proc.services.size() - 1; i >= 0; i--) {
            ServiceRecord sr = (ServiceRecord) proc.services.valueAt(i);
            if (sr.isForeground || sr.fgRequired) {
                anyForeground = true;
                break;
            }
        }
        this.mAm.updateProcessForegroundLocked(proc, anyForeground, oomAdj);
    }

    private void updateWhitelistManagerLocked(ProcessRecord proc) {
        proc.whitelistManager = false;
        for (int i = proc.services.size() - 1; i >= 0; i--) {
            if (((ServiceRecord) proc.services.valueAt(i)).whitelistManager) {
                proc.whitelistManager = true;
                return;
            }
        }
    }

    public void updateServiceConnectionActivitiesLocked(ProcessRecord clientProc) {
        ArraySet updatedProcesses = null;
        for (int i = 0; i < clientProc.connections.size(); i++) {
            ProcessRecord proc = ((ConnectionRecord) clientProc.connections.valueAt(i)).binding.service.app;
            if (!(proc == null || proc == clientProc)) {
                if (updatedProcesses == null) {
                    updatedProcesses = new ArraySet();
                } else if (updatedProcesses.contains(proc)) {
                }
                updatedProcesses.add(proc);
                updateServiceClientActivitiesLocked(proc, null, false);
            }
        }
    }

    private boolean updateServiceClientActivitiesLocked(ProcessRecord proc, ConnectionRecord modCr, boolean updateLru) {
        if (modCr != null && modCr.binding.client != null && modCr.binding.client.activities.size() <= 0) {
            return false;
        }
        boolean anyClientActivities = false;
        for (int i = proc.services.size() - 1; i >= 0 && (anyClientActivities ^ 1) != 0; i--) {
            ServiceRecord sr = (ServiceRecord) proc.services.valueAt(i);
            for (int conni = sr.connections.size() - 1; conni >= 0 && (anyClientActivities ^ 1) != 0; conni--) {
                ArrayList<ConnectionRecord> clist = (ArrayList) sr.connections.valueAt(conni);
                for (int cri = clist.size() - 1; cri >= 0; cri--) {
                    ConnectionRecord cr = (ConnectionRecord) clist.get(cri);
                    if (cr.binding.client != null && cr.binding.client != proc && cr.binding.client.activities.size() > 0) {
                        anyClientActivities = true;
                        break;
                    }
                }
            }
        }
        if (anyClientActivities == proc.hasClientActivities) {
            return false;
        }
        proc.hasClientActivities = anyClientActivities;
        if (updateLru) {
            this.mAm.updateLruProcessLocked(proc, anyClientActivities, null);
        }
        return true;
    }

    int bindServiceLocked(IApplicationThread caller, IBinder token, Intent service, String resolvedType, IServiceConnection connection, int flags, String callingPackage, int userId) throws TransactionTooLargeException {
        ProcessRecord callerApp = this.mAm.getRecordForAppLocked(caller);
        if (callerApp == null) {
            throw new SecurityException("Unable to find app for caller " + caller + " (pid=" + Binder.getCallingPid() + ") when binding service " + service);
        }
        ActivityRecord activityRecord = null;
        if (token != null) {
            activityRecord = ActivityRecord.isInStackLocked(token);
            if (activityRecord == null) {
                Slog.w(TAG, "Binding with unknown activity: " + token);
                return 0;
            }
        }
        int clientLabel = 0;
        PendingIntent pendingIntent = null;
        boolean isCallerSystem = callerApp.info.uid == 1000;
        if (isCallerSystem) {
            service.setDefusable(true);
            pendingIntent = (PendingIntent) service.getParcelableExtra("android.intent.extra.client_intent");
            if (pendingIntent != null) {
                clientLabel = service.getIntExtra("android.intent.extra.client_label", 0);
                if (clientLabel != 0) {
                    service = service.cloneFilter();
                }
            }
        }
        if ((134217728 & flags) != 0) {
            this.mAm.enforceCallingPermission("android.permission.MANAGE_ACTIVITY_STACKS", "BIND_TREAT_LIKE_ACTIVITY");
        }
        if ((16777216 & flags) == 0 || (isCallerSystem ^ 1) == 0) {
            boolean callerFg = callerApp.setSchedGroup != 0;
            ServiceLookupResult res = retrieveServiceLocked(service, resolvedType, callingPackage, Binder.getCallingPid(), Binder.getCallingUid(), userId, true, callerFg, (Integer.MIN_VALUE & flags) != 0);
            if (res == null) {
                return 0;
            }
            if (res.record == null) {
                return -1;
            }
            final ServiceRecord s = res.record;
            boolean permissionsReviewRequired = false;
            if (this.mAm.mPermissionReviewRequired && this.mAm.getPackageManagerInternalLocked().isPermissionsReviewRequired(s.packageName, s.userId)) {
                permissionsReviewRequired = true;
                if (callerFg) {
                    ServiceRecord serviceRecord = s;
                    final Intent serviceIntent = service;
                    final boolean z = callerFg;
                    final IServiceConnection iServiceConnection = connection;
                    RemoteCallback remoteCallback = new RemoteCallback(new OnResultListener() {
                        public void onResult(Bundle result) {
                            synchronized (ActiveServices.this.mAm) {
                                long identity;
                                try {
                                    ActivityManagerService.boostPriorityForLockedSection();
                                    identity = Binder.clearCallingIdentity();
                                    if (ActiveServices.this.mPendingServices.contains(s)) {
                                        if (ActiveServices.this.mAm.getPackageManagerInternalLocked().isPermissionsReviewRequired(s.packageName, s.userId)) {
                                            ActiveServices.this.unbindServiceLocked(iServiceConnection);
                                        } else {
                                            try {
                                                ActiveServices.this.bringUpServiceLocked(s, serviceIntent.getFlags(), z, false, false);
                                            } catch (RemoteException e) {
                                            }
                                        }
                                        Binder.restoreCallingIdentity(identity);
                                        ActivityManagerService.resetPriorityAfterLockedSection();
                                        return;
                                    }
                                    Binder.restoreCallingIdentity(identity);
                                    ActivityManagerService.resetPriorityAfterLockedSection();
                                } catch (Throwable th) {
                                    ActivityManagerService.resetPriorityAfterLockedSection();
                                }
                            }
                        }
                    });
                    Intent intent = new Intent("android.intent.action.REVIEW_PERMISSIONS");
                    intent.addFlags(276824064);
                    intent.putExtra("android.intent.extra.PACKAGE_NAME", s.packageName);
                    intent.putExtra("android.intent.extra.REMOTE_CALLBACK", remoteCallback);
                    final Intent intent2 = intent;
                    final int i = userId;
                    this.mAm.mHandler.post(new Runnable() {
                        public void run() {
                            ActiveServices.this.mAm.mContext.startActivityAsUser(intent2, new UserHandle(i));
                        }
                    });
                } else {
                    Slog.w(TAG, "u" + s.userId + " Binding to a service in package" + s.packageName + " requires a permissions review");
                    return 0;
                }
            }
            long origId = Binder.clearCallingIdentity();
            boolean unscheduleServiceRestartLocked = unscheduleServiceRestartLocked(s, callerApp.info.uid, false);
            if ((flags & 1) != 0) {
                s.lastActivity = SystemClock.uptimeMillis();
                if (!s.hasAutoCreateConnections()) {
                    ServiceState stracker = s.getTracker();
                    if (stracker != null) {
                        stracker.setBound(true, this.mAm.mProcessStats.getMemFactorLocked(), s.lastActivity);
                    }
                }
            }
            this.mAm.startAssociationLocked(callerApp.uid, callerApp.processName, callerApp.curProcState, s.appInfo.uid, s.name, s.processName);
            this.mAm.grantEphemeralAccessLocked(callerApp.userId, service, s.appInfo.uid, UserHandle.getAppId(callerApp.uid));
            AppBindRecord b = s.retrieveAppBindingLocked(service, callerApp);
            ConnectionRecord c = new ConnectionRecord(b, activityRecord, connection, flags, clientLabel, pendingIntent);
            IBinder binder = connection.asBinder();
            ArrayList<ConnectionRecord> clist = (ArrayList) s.connections.get(binder);
            if (clist == null) {
                clist = new ArrayList();
                s.connections.put(binder, clist);
            }
            clist.add(c);
            b.connections.add(c);
            if (activityRecord != null) {
                if (activityRecord.connections == null) {
                    activityRecord.connections = new HashSet();
                }
                activityRecord.connections.add(c);
            }
            b.client.connections.add(c);
            if ((c.flags & 8) != 0) {
                b.client.hasAboveClient = true;
            }
            if ((c.flags & 16777216) != 0) {
                s.whitelistManager = true;
            }
            if (s.app != null) {
                updateServiceClientActivitiesLocked(s.app, c, true);
            }
            clist = (ArrayList) this.mServiceConnections.get(binder);
            if (clist == null) {
                clist = new ArrayList();
                this.mServiceConnections.put(binder, clist);
            }
            clist.add(c);
            if ((flags & 1) != 0) {
                s.lastActivity = SystemClock.uptimeMillis();
                if (bringUpServiceLocked(s, service.getFlags(), callerFg, false, permissionsReviewRequired) != null) {
                    Binder.restoreCallingIdentity(origId);
                    return 0;
                }
            }
            try {
                if (s.app != null) {
                    if ((134217728 & flags) != 0) {
                        s.app.treatLikeActivity = true;
                    }
                    if (s.whitelistManager) {
                        s.app.whitelistManager = true;
                    }
                    ActivityManagerService activityManagerService = this.mAm;
                    ProcessRecord processRecord = s.app;
                    if (s.app.hasClientActivities) {
                        unscheduleServiceRestartLocked = true;
                    } else {
                        unscheduleServiceRestartLocked = s.app.treatLikeActivity;
                    }
                    activityManagerService.updateLruProcessLocked(processRecord, unscheduleServiceRestartLocked, b.client);
                    this.mAm.updateOomAdjLocked(s.app, true);
                }
                if (s.app != null && b.intent.received) {
                    c.conn.connected(s.name, b.intent.binder, false);
                    if (b.intent.apps.size() == 1 && b.intent.doRebind) {
                        requestServiceBindingLocked(s, b.intent, callerFg, true);
                    }
                } else if (!b.intent.requested) {
                    requestServiceBindingLocked(s, b.intent, callerFg, false);
                }
            } catch (Throwable e) {
                Slog.w(TAG, "Failure sending service " + s.shortName + " to connection " + c.conn.asBinder() + " (in " + c.binding.client.processName + ")", e);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(origId);
            }
            getServiceMapLocked(s.userId).ensureNotStartingBackgroundLocked(s);
            Binder.restoreCallingIdentity(origId);
            return 1;
        }
        throw new SecurityException("Non-system caller " + caller + " (pid=" + Binder.getCallingPid() + ") set BIND_ALLOW_WHITELIST_MANAGEMENT when binding service " + service);
    }

    void publishServiceLocked(ServiceRecord r, Intent intent, IBinder service) {
        long origId = Binder.clearCallingIdentity();
        if (r != null) {
            FilterComparison filter = new FilterComparison(intent);
            IntentBindRecord b = (IntentBindRecord) r.bindings.get(filter);
            if (!(b == null || (b.received ^ 1) == 0)) {
                b.binder = service;
                b.requested = true;
                b.received = true;
                for (int conni = r.connections.size() - 1; conni >= 0; conni--) {
                    ArrayList<ConnectionRecord> clist = (ArrayList) r.connections.valueAt(conni);
                    for (int i = 0; i < clist.size(); i++) {
                        ConnectionRecord c = (ConnectionRecord) clist.get(i);
                        if (filter.equals(c.binding.intent.intent)) {
                            try {
                                c.conn.connected(r.name, service, false);
                            } catch (Exception e) {
                                Slog.w(TAG, "Failure sending service " + r.name + " to connection " + c.conn.asBinder() + " (in " + c.binding.client.processName + ")", e);
                            } catch (Throwable th) {
                                Binder.restoreCallingIdentity(origId);
                            }
                        }
                    }
                }
            }
            serviceDoneExecutingLocked(r, this.mDestroyingServices.contains(r), false);
        }
        Binder.restoreCallingIdentity(origId);
    }

    boolean unbindServiceLocked(IServiceConnection connection) {
        IBinder binder = connection.asBinder();
        ArrayList<ConnectionRecord> clist = (ArrayList) this.mServiceConnections.get(binder);
        if (clist == null) {
            Slog.w(TAG, "Unbind failed: could not find connection for " + connection.asBinder());
            return false;
        }
        long origId = Binder.clearCallingIdentity();
        while (clist.size() > 0) {
            try {
                ConnectionRecord r = (ConnectionRecord) clist.get(0);
                removeConnectionLocked(r, null, null);
                if (clist.size() > 0 && clist.get(0) == r) {
                    Slog.wtf(TAG, "Connection " + r + " not removed for binder " + binder);
                    clist.remove(0);
                }
                if (r.binding.service.app != null) {
                    if (r.binding.service.app.whitelistManager) {
                        updateWhitelistManagerLocked(r.binding.service.app);
                    }
                    if ((r.flags & 134217728) != 0) {
                        boolean z;
                        r.binding.service.app.treatLikeActivity = true;
                        ActivityManagerService activityManagerService = this.mAm;
                        ProcessRecord processRecord = r.binding.service.app;
                        if (r.binding.service.app.hasClientActivities) {
                            z = true;
                        } else {
                            z = r.binding.service.app.treatLikeActivity;
                        }
                        activityManagerService.updateLruProcessLocked(processRecord, z, null);
                    }
                    this.mAm.updateOomAdjLocked(r.binding.service.app, false);
                }
            } finally {
                Binder.restoreCallingIdentity(origId);
            }
        }
        this.mAm.updateOomAdjLocked();
        return true;
    }

    void unbindFinishedLocked(ServiceRecord r, Intent intent, boolean doRebind) {
        long origId = Binder.clearCallingIdentity();
        if (r != null) {
            try {
                IntentBindRecord b = (IntentBindRecord) r.bindings.get(new FilterComparison(intent));
                boolean inDestroying = this.mDestroyingServices.contains(r);
                if (b != null) {
                    if (b.apps.size() <= 0 || (inDestroying ^ 1) == 0) {
                        b.doRebind = true;
                    } else {
                        boolean inFg = false;
                        for (int i = b.apps.size() - 1; i >= 0; i--) {
                            ProcessRecord client = ((AppBindRecord) b.apps.valueAt(i)).client;
                            if (client != null && client.setSchedGroup != 0) {
                                inFg = true;
                                break;
                            }
                        }
                        try {
                            requestServiceBindingLocked(r, b, inFg, true);
                        } catch (TransactionTooLargeException e) {
                        }
                    }
                }
                serviceDoneExecutingLocked(r, inDestroying, false);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(origId);
            }
        }
        Binder.restoreCallingIdentity(origId);
    }

    private final ServiceRecord findServiceLocked(ComponentName name, IBinder token, int userId) {
        IBinder r = getServiceByNameLocked(name, userId);
        return r == token ? r : null;
    }

    private ServiceLookupResult retrieveServiceLocked(Intent service, String resolvedType, String callingPackage, int callingPid, int callingUid, int userId, boolean createIfNeeded, boolean callingFromFg, boolean isBindExternal) {
        ServiceRecord r;
        ServiceRecord r2;
        int opCode;
        userId = this.mAm.mUserController.handleIncomingUser(callingPid, callingUid, userId, false, 1, "service", null);
        ServiceMap smap = getServiceMapLocked(userId);
        ComponentName comp = service.getComponent();
        if (comp != null) {
            r = (ServiceRecord) smap.mServicesByName.get(comp);
        } else {
            r = null;
        }
        if (r == null && (isBindExternal ^ 1) != 0) {
            r = (ServiceRecord) smap.mServicesByIntent.get(new FilterComparison(service));
        }
        if (r == null || (r.serviceInfo.flags & 4) == 0) {
            r2 = r;
        } else {
            if ((callingPackage.equals(r.packageName) ^ 1) != 0) {
                r2 = null;
            } else {
                r2 = r;
            }
        }
        if (r2 == null) {
            try {
                ResolveInfo rInfo = this.mAm.getPackageManagerInternalLocked().resolveService(service, resolvedType, 268436480, userId, callingUid);
                ServiceInfo sInfo = rInfo != null ? rInfo.serviceInfo : null;
                if (sInfo == null) {
                    Slog.w(TAG_SERVICE, "Unable to start service " + service + " U=" + userId + ": not found");
                    return null;
                }
                ServiceInfo sInfo2;
                ComponentName name = new ComponentName(sInfo.applicationInfo.packageName, sInfo.name);
                if ((sInfo.flags & 4) != 0) {
                    if (!isBindExternal) {
                        throw new SecurityException("BIND_EXTERNAL_SERVICE required for " + name);
                    } else if (!sInfo.exported) {
                        throw new SecurityException("BIND_EXTERNAL_SERVICE failed, " + name + " is not exported");
                    } else if ((sInfo.flags & 2) == 0) {
                        throw new SecurityException("BIND_EXTERNAL_SERVICE failed, " + name + " is not an isolatedProcess");
                    } else {
                        ApplicationInfo aInfo = AppGlobals.getPackageManager().getApplicationInfo(callingPackage, 1024, userId);
                        if (aInfo == null) {
                            throw new SecurityException("BIND_EXTERNAL_SERVICE failed, could not resolve client package " + callingPackage);
                        }
                        sInfo2 = new ServiceInfo(sInfo);
                        sInfo2.applicationInfo = new ApplicationInfo(sInfo2.applicationInfo);
                        sInfo2.applicationInfo.packageName = aInfo.packageName;
                        sInfo2.applicationInfo.uid = aInfo.uid;
                        ComponentName componentName = new ComponentName(aInfo.packageName, name.getClassName());
                        service.setComponent(componentName);
                        name = componentName;
                        sInfo = sInfo2;
                    }
                } else if (isBindExternal) {
                    throw new SecurityException("BIND_EXTERNAL_SERVICE failed, " + name + " is not an externalService");
                }
                if (userId > 0) {
                    if (this.mAm.isSingleton(sInfo.processName, sInfo.applicationInfo, sInfo.name, sInfo.flags)) {
                        if (this.mAm.isValidSingletonCall(callingUid, sInfo.applicationInfo.uid)) {
                            userId = 0;
                            smap = getServiceMapLocked(0);
                        }
                    }
                    sInfo2 = new ServiceInfo(sInfo);
                    sInfo2.applicationInfo = this.mAm.getAppInfoForUser(sInfo2.applicationInfo, userId);
                } else {
                    sInfo2 = sInfo;
                }
                r2 = (ServiceRecord) smap.mServicesByName.get(name);
                if (r2 != null) {
                    r = r2;
                } else if (createIfNeeded) {
                    Serv ss;
                    FilterComparison filter = new FilterComparison(service.cloneFilter());
                    ActiveServices activeServices = this;
                    ServiceRestarter res = new ServiceRestarter();
                    BatteryStatsImpl stats = this.mAm.mBatteryStatsService.getActiveStatistics();
                    synchronized (stats) {
                        ss = stats.getServiceStatsLocked(sInfo2.applicationInfo.uid, sInfo2.packageName, sInfo2.name);
                    }
                    r = new ServiceRecord(this.mAm, ss, name, filter, sInfo2, callingFromFg, res);
                    try {
                        res.setService(r);
                        smap.mServicesByName.put(name, r);
                        smap.mServicesByIntent.put(filter, r);
                        for (int i = this.mPendingServices.size() - 1; i >= 0; i--) {
                            ServiceRecord pr = (ServiceRecord) this.mPendingServices.get(i);
                            if (pr.serviceInfo.applicationInfo.uid == sInfo2.applicationInfo.uid && pr.name.equals(name)) {
                                this.mPendingServices.remove(i);
                            }
                        }
                    } catch (RemoteException e) {
                    }
                }
                if (r != null) {
                    return null;
                }
                if (this.mAm.checkComponentPermission(r.permission, callingPid, callingUid, r.appInfo.uid, r.exported) != 0) {
                    if (!(r.permission == null || callingPackage == null)) {
                        opCode = AppOpsManager.permissionToOpCode(r.permission);
                        if (!(opCode == -1 || this.mAm.mAppOpsService.noteOperation(opCode, callingUid, callingPackage) == 0)) {
                            Slog.w(TAG, "Appop Denial: Accessing service " + r.name + " from pid=" + callingPid + ", uid=" + callingUid + " requires appop " + AppOpsManager.opToName(opCode));
                            return null;
                        }
                    }
                    if (this.mAm.mIntentFirewall.checkService(r.name, service, callingUid, callingPid, resolvedType, r.appInfo)) {
                        return new ServiceLookupResult(r, null);
                    }
                    return null;
                } else if (r.exported) {
                    Slog.w(TAG, "Permission Denial: Accessing service " + r.name + " from pid=" + callingPid + ", uid=" + callingUid + " that is not exported from uid " + r.appInfo.uid);
                    return new ServiceLookupResult(null, "not exported from uid " + r.appInfo.uid);
                } else {
                    Slog.w(TAG, "Permission Denial: Accessing service " + r.name + " from pid=" + callingPid + ", uid=" + callingUid + " requires " + r.permission);
                    return new ServiceLookupResult(null, r.permission);
                }
            } catch (RemoteException e2) {
                r = r2;
            }
        }
        r = r2;
        if (r != null) {
            return null;
        }
        if (this.mAm.checkComponentPermission(r.permission, callingPid, callingUid, r.appInfo.uid, r.exported) != 0) {
            opCode = AppOpsManager.permissionToOpCode(r.permission);
            Slog.w(TAG, "Appop Denial: Accessing service " + r.name + " from pid=" + callingPid + ", uid=" + callingUid + " requires appop " + AppOpsManager.opToName(opCode));
            return null;
        } else if (r.exported) {
            Slog.w(TAG, "Permission Denial: Accessing service " + r.name + " from pid=" + callingPid + ", uid=" + callingUid + " requires " + r.permission);
            return new ServiceLookupResult(null, r.permission);
        } else {
            Slog.w(TAG, "Permission Denial: Accessing service " + r.name + " from pid=" + callingPid + ", uid=" + callingUid + " that is not exported from uid " + r.appInfo.uid);
            return new ServiceLookupResult(null, "not exported from uid " + r.appInfo.uid);
        }
    }

    private final void bumpServiceExecutingLocked(ServiceRecord r, boolean fg, String why) {
        long now = SystemClock.uptimeMillis();
        if (r.executeNesting == 0) {
            r.executeFg = fg;
            ServiceState stracker = r.getTracker();
            if (stracker != null) {
                stracker.setExecuting(true, this.mAm.mProcessStats.getMemFactorLocked(), now);
            }
            if (r.app != null) {
                r.app.executingServices.add(r);
                ProcessRecord processRecord = r.app;
                processRecord.execServicesFg |= fg;
                if (r.app.executingServices.size() == 1) {
                    scheduleServiceTimeoutLocked(r.app);
                }
            }
        } else if (!(r.app == null || !fg || (r.app.execServicesFg ^ 1) == 0)) {
            r.app.execServicesFg = true;
            scheduleServiceTimeoutLocked(r.app);
        }
        r.executeFg |= fg;
        r.executeNesting++;
        r.executingStart = now;
    }

    private final boolean requestServiceBindingLocked(ServiceRecord r, IntentBindRecord i, boolean execInFg, boolean rebind) throws TransactionTooLargeException {
        boolean inDestroying;
        if (r.app == null || r.app.thread == null) {
            return false;
        }
        if ((!i.requested || rebind) && i.apps.size() > 0) {
            try {
                bumpServiceExecutingLocked(r, execInFg, "bind");
                r.app.forceProcessStateUpTo(11);
                r.app.thread.scheduleBindService(r, i.intent.getIntent(), rebind, r.app.repProcState);
                if (!rebind) {
                    i.requested = true;
                }
                i.hasBound = true;
                i.doRebind = false;
            } catch (TransactionTooLargeException e) {
                inDestroying = this.mDestroyingServices.contains(r);
                serviceDoneExecutingLocked(r, inDestroying, inDestroying);
                throw e;
            } catch (RemoteException e2) {
                inDestroying = this.mDestroyingServices.contains(r);
                serviceDoneExecutingLocked(r, inDestroying, inDestroying);
                return false;
            }
        }
        return true;
    }

    private final boolean scheduleServiceRestartLocked(ServiceRecord r, boolean allowCancel) {
        boolean canceled = false;
        if (this.mAm.isShuttingDownLocked()) {
            Slog.w(TAG, "Not scheduling restart of crashed service " + r.shortName + " - system is shutting down");
            return false;
        }
        ServiceMap smap = getServiceMapLocked(r.userId);
        if (smap.mServicesByName.get(r.name) != r) {
            Slog.wtf(TAG, "Attempting to schedule restart of " + r + " when found in map: " + ((ServiceRecord) smap.mServicesByName.get(r.name)));
            return false;
        }
        long now = SystemClock.uptimeMillis();
        if ((r.serviceInfo.applicationInfo.flags & 8) == 0) {
            int i;
            long minDuration = this.mAm.mConstants.SERVICE_RESTART_DURATION;
            long resetTime = this.mAm.mConstants.SERVICE_RESET_RUN_DURATION;
            int N = r.deliveredStarts.size();
            if (N > 0) {
                for (i = N - 1; i >= 0; i--) {
                    StartItem si = (StartItem) r.deliveredStarts.get(i);
                    si.removeUriPermissionsLocked();
                    if (si.intent != null) {
                        if (!allowCancel || (si.deliveryCount < 3 && si.doneExecutingCount < 6)) {
                            r.pendingStarts.add(0, si);
                            long dur = (SystemClock.uptimeMillis() - si.deliveredTime) * 2;
                            if (minDuration < dur) {
                                minDuration = dur;
                            }
                            if (resetTime < dur) {
                                resetTime = dur;
                            }
                        } else {
                            Slog.w(TAG, "Canceling start item " + si.intent + " in service " + r.name);
                            canceled = true;
                        }
                    }
                }
                r.deliveredStarts.clear();
            }
            r.totalRestartCount++;
            if (r.restartDelay == 0) {
                r.restartCount++;
                r.restartDelay = minDuration;
            } else if (r.crashCount > 1) {
                r.restartDelay = this.mAm.mConstants.BOUND_SERVICE_CRASH_RESTART_DURATION * ((long) (r.crashCount - 1));
            } else if (now > r.restartTime + resetTime) {
                r.restartCount = 1;
                r.restartDelay = minDuration;
            } else {
                r.restartDelay *= (long) this.mAm.mConstants.SERVICE_RESTART_DURATION_FACTOR;
                if (r.restartDelay < minDuration) {
                    r.restartDelay = minDuration;
                }
            }
            r.nextRestartTime = r.restartDelay + now;
            boolean repeat;
            do {
                repeat = false;
                long restartTimeBetween = this.mAm.mConstants.SERVICE_MIN_RESTART_TIME_BETWEEN;
                for (i = this.mRestartingServices.size() - 1; i >= 0; i--) {
                    ServiceRecord r2 = (ServiceRecord) this.mRestartingServices.get(i);
                    if (r2 != r && r.nextRestartTime >= r2.nextRestartTime - restartTimeBetween && r.nextRestartTime < r2.nextRestartTime + restartTimeBetween) {
                        r.nextRestartTime = r2.nextRestartTime + restartTimeBetween;
                        r.restartDelay = r.nextRestartTime - now;
                        repeat = true;
                        continue;
                        break;
                    }
                }
            } while (repeat);
        } else {
            r.totalRestartCount++;
            r.restartCount = 0;
            r.restartDelay = 0;
            r.nextRestartTime = now;
        }
        if (!this.mRestartingServices.contains(r)) {
            r.createdFromFg = false;
            this.mRestartingServices.add(r);
            r.makeRestarting(this.mAm.mProcessStats.getMemFactorLocked(), now);
        }
        cancelForegroundNotificationLocked(r);
        this.mAm.mHandler.removeCallbacks(r.restarter);
        this.mAm.mHandler.postAtTime(r.restarter, r.nextRestartTime);
        r.nextRestartTime = SystemClock.uptimeMillis() + r.restartDelay;
        Slog.w(TAG, "Scheduling restart of crashed service " + r.shortName + " in " + r.restartDelay + "ms");
        EventLog.writeEvent(EventLogTags.AM_SCHEDULE_SERVICE_RESTART, new Object[]{Integer.valueOf(r.userId), r.shortName, Long.valueOf(r.restartDelay)});
        return canceled;
    }

    final void performServiceRestartLocked(ServiceRecord r) {
        if (!this.mRestartingServices.contains(r)) {
            return;
        }
        if (isServiceNeededLocked(r, false, false)) {
            try {
                bringUpServiceLocked(r, r.intent.getIntent().getFlags(), r.createdFromFg, true, false);
            } catch (TransactionTooLargeException e) {
            }
            return;
        }
        Slog.wtf(TAG, "Restarting service that is not needed: " + r);
    }

    private final boolean unscheduleServiceRestartLocked(ServiceRecord r, int callingUid, boolean force) {
        if (!force && r.restartDelay == 0) {
            return false;
        }
        boolean removed = this.mRestartingServices.remove(r);
        if (removed || callingUid != r.appInfo.uid) {
            r.resetRestartCounter();
        }
        if (removed) {
            clearRestartingIfNeededLocked(r);
        }
        this.mAm.mHandler.removeCallbacks(r.restarter);
        return true;
    }

    private void clearRestartingIfNeededLocked(ServiceRecord r) {
        if (r.restartTracker != null) {
            boolean stillTracking = false;
            for (int i = this.mRestartingServices.size() - 1; i >= 0; i--) {
                if (((ServiceRecord) this.mRestartingServices.get(i)).restartTracker == r.restartTracker) {
                    stillTracking = true;
                    break;
                }
            }
            if (!stillTracking) {
                r.restartTracker.setRestarting(false, this.mAm.mProcessStats.getMemFactorLocked(), SystemClock.uptimeMillis());
                r.restartTracker = null;
            }
        }
    }

    private String bringUpServiceLocked(ServiceRecord r, int intentFlags, boolean execInFg, boolean whileRestarting, boolean permissionsReviewRequired) throws TransactionTooLargeException {
        if (r.app != null && r.app.thread != null) {
            sendServiceArgsLocked(r, execInFg, false);
            return null;
        } else if (!whileRestarting && this.mRestartingServices.contains(r)) {
            return null;
        } else {
            if (this.mRestartingServices.remove(r)) {
                clearRestartingIfNeededLocked(r);
            }
            if (r.delayed) {
                getServiceMapLocked(r.userId).mDelayedStartList.remove(r);
                r.delayed = false;
            }
            String msg;
            if (this.mAm.mUserController.hasStartedUserState(r.userId)) {
                ProcessRecord app;
                try {
                    AppGlobals.getPackageManager().setPackageStoppedState(r.packageName, false, r.userId);
                } catch (RemoteException e) {
                } catch (IllegalArgumentException e2) {
                    Slog.w(TAG, "Failed trying to unstop package " + r.packageName + ": " + e2);
                }
                boolean isolated = (r.serviceInfo.flags & 2) != 0;
                String procName = r.processName;
                String hostingType = "service";
                if (isolated) {
                    app = r.isolatedProc;
                    if (WebViewZygote.isMultiprocessEnabled() && r.serviceInfo.packageName.equals(WebViewZygote.getPackageName())) {
                        hostingType = "webview_service";
                    }
                } else {
                    app = this.mAm.getProcessRecordLocked(procName, r.appInfo.uid, false);
                    if (!(app == null || app.thread == null)) {
                        try {
                            app.addPackage(r.appInfo.packageName, r.appInfo.versionCode, this.mAm.mProcessStats);
                            realStartServiceLocked(r, app, execInFg);
                            return null;
                        } catch (TransactionTooLargeException e3) {
                            throw e3;
                        } catch (RemoteException e4) {
                            Slog.w(TAG, "Exception when starting service " + r.shortName, e4);
                        }
                    }
                }
                if (app == null && (permissionsReviewRequired ^ 1) != 0) {
                    app = this.mAm.startProcessLocked(procName, r.appInfo, true, intentFlags, hostingType, r.name, false, isolated, false);
                    if (app == null) {
                        msg = "Unable to launch app " + r.appInfo.packageName + "/" + r.appInfo.uid + " for service " + r.intent.getIntent() + ": process is bad";
                        Slog.w(TAG, msg);
                        bringDownServiceLocked(r);
                        return msg;
                    } else if (isolated) {
                        r.isolatedProc = app;
                    }
                }
                if (r.fgRequired) {
                    this.mAm.tempWhitelistUidLocked(r.appInfo.uid, 5000, "fg-service-launch");
                }
                if (!this.mPendingServices.contains(r)) {
                    this.mPendingServices.add(r);
                }
                if (r.delayedStop) {
                    r.delayedStop = false;
                    if (r.startRequested) {
                        stopServiceLocked(r);
                    }
                }
                return null;
            }
            msg = "Unable to launch app " + r.appInfo.packageName + "/" + r.appInfo.uid + " for service " + r.intent.getIntent() + ": user " + r.userId + " is stopped";
            Slog.w(TAG, msg);
            bringDownServiceLocked(r);
            return msg;
        }
    }

    private final void requestServiceBindingsLocked(ServiceRecord r, boolean execInFg) throws TransactionTooLargeException {
        int i = r.bindings.size() - 1;
        while (i >= 0 && requestServiceBindingLocked(r, (IntentBindRecord) r.bindings.valueAt(i), execInFg, false)) {
            i--;
        }
    }

    private final void realStartServiceLocked(ServiceRecord r, ProcessRecord app, boolean execInFg) throws RemoteException {
        if (app.thread == null) {
            throw new RemoteException();
        }
        r.app = app;
        long uptimeMillis = SystemClock.uptimeMillis();
        r.lastActivity = uptimeMillis;
        r.restartTime = uptimeMillis;
        boolean newService = app.services.add(r);
        bumpServiceExecutingLocked(r, execInFg, "create");
        this.mAm.updateLruProcessLocked(app, false, null);
        updateServiceForegroundLocked(r.app, false);
        this.mAm.updateOomAdjLocked();
        boolean inDestroying;
        try {
            synchronized (r.stats.getBatteryStats()) {
                r.stats.startLaunchedLocked();
            }
            this.mAm.notifyPackageUse(r.serviceInfo.packageName, 1);
            app.forceProcessStateUpTo(11);
            app.thread.scheduleCreateService(r, r.serviceInfo, this.mAm.compatibilityInfoForPackageLocked(r.serviceInfo.applicationInfo), app.repProcState);
            r.postNotification();
            if (!true) {
                inDestroying = this.mDestroyingServices.contains(r);
                serviceDoneExecutingLocked(r, inDestroying, inDestroying);
                if (newService) {
                    app.services.remove(r);
                    r.app = null;
                }
                if (!inDestroying) {
                    scheduleServiceRestartLocked(r, false);
                }
            }
            if (r.whitelistManager) {
                app.whitelistManager = true;
            }
            requestServiceBindingsLocked(r, execInFg);
            updateServiceClientActivitiesLocked(app, null, true);
            if (r.startRequested && r.callStart && r.pendingStarts.size() == 0) {
                r.pendingStarts.add(new StartItem(r, false, r.makeNextStartId(), null, null, 0));
            }
            sendServiceArgsLocked(r, execInFg, true);
            if (r.delayed) {
                getServiceMapLocked(r.userId).mDelayedStartList.remove(r);
                r.delayed = false;
            }
            if (r.delayedStop) {
                r.delayedStop = false;
                if (r.startRequested) {
                    stopServiceLocked(r);
                }
            }
        } catch (DeadObjectException e) {
            try {
                Slog.w(TAG, "Application dead when creating service " + r);
                this.mAm.appDiedLocked(app);
                throw e;
            } catch (Throwable th) {
                if (!false) {
                    inDestroying = this.mDestroyingServices.contains(r);
                    serviceDoneExecutingLocked(r, inDestroying, inDestroying);
                    if (newService) {
                        app.services.remove(r);
                        r.app = null;
                    }
                    if (!inDestroying) {
                        scheduleServiceRestartLocked(r, false);
                    }
                }
            }
        }
    }

    private final void sendServiceArgsLocked(ServiceRecord r, boolean execInFg, boolean oomAdjusted) throws TransactionTooLargeException {
        int N = r.pendingStarts.size();
        if (N != 0) {
            ArrayList<ServiceStartArgs> args = new ArrayList();
            while (r.pendingStarts.size() > 0) {
                StartItem si = (StartItem) r.pendingStarts.remove(0);
                if (si.intent != null || N <= 1) {
                    si.deliveredTime = SystemClock.uptimeMillis();
                    r.deliveredStarts.add(si);
                    si.deliveryCount++;
                    if (si.neededGrants != null) {
                        this.mAm.grantUriPermissionUncheckedFromIntentLocked(si.neededGrants, si.getUriPermissionsLocked());
                    }
                    this.mAm.grantEphemeralAccessLocked(r.userId, si.intent, r.appInfo.uid, UserHandle.getAppId(si.callingId));
                    bumpServiceExecutingLocked(r, execInFg, "start");
                    if (!oomAdjusted) {
                        oomAdjusted = true;
                        this.mAm.updateOomAdjLocked(r.app, true);
                    }
                    if (r.fgRequired && (r.fgWaiting ^ 1) != 0) {
                        if (r.isForeground) {
                            r.fgRequired = false;
                        } else {
                            scheduleServiceForegroundTransitionTimeoutLocked(r);
                        }
                    }
                    int flags = 0;
                    if (si.deliveryCount > 1) {
                        flags = 2;
                    }
                    if (si.doneExecutingCount > 0) {
                        flags |= 1;
                    }
                    args.add(new ServiceStartArgs(si.taskRemoved, si.id, flags, si.intent));
                }
            }
            ParceledListSlice<ServiceStartArgs> slice = new ParceledListSlice(args);
            slice.setInlineCountLimit(4);
            Exception caughtException = null;
            try {
                r.app.thread.scheduleServiceArgs(r, slice);
            } catch (Exception e) {
                Slog.w(TAG, "Failed delivering service starts", e);
                caughtException = e;
            } catch (Exception e2) {
                Slog.w(TAG, "Failed delivering service starts", e2);
                caughtException = e2;
            } catch (Exception e3) {
                Slog.w(TAG, "Unexpected exception", e3);
                caughtException = e3;
            }
            if (caughtException != null) {
                boolean inDestroying = this.mDestroyingServices.contains(r);
                for (int i = 0; i < args.size(); i++) {
                    serviceDoneExecutingLocked(r, inDestroying, inDestroying);
                }
                if (caughtException instanceof TransactionTooLargeException) {
                    throw ((TransactionTooLargeException) caughtException);
                }
            }
        }
    }

    private final boolean isServiceNeededLocked(ServiceRecord r, boolean knowConn, boolean hasConn) {
        if (r.startRequested) {
            return true;
        }
        if (!knowConn) {
            hasConn = r.hasAutoCreateConnections();
        }
        if (hasConn) {
            return true;
        }
        return false;
    }

    private final void bringDownServiceIfNeededLocked(ServiceRecord r, boolean knowConn, boolean hasConn) {
        if (!isServiceNeededLocked(r, knowConn, hasConn) && !this.mPendingServices.contains(r)) {
            bringDownServiceLocked(r);
        }
    }

    private final void bringDownServiceLocked(ServiceRecord r) {
        for (int conni = r.connections.size() - 1; conni >= 0; conni--) {
            int i;
            ArrayList<ConnectionRecord> c = (ArrayList) r.connections.valueAt(conni);
            for (i = 0; i < c.size(); i++) {
                ConnectionRecord cr = (ConnectionRecord) c.get(i);
                cr.serviceDead = true;
                try {
                    cr.conn.connected(r.name, null, true);
                } catch (Exception e) {
                    Slog.w(TAG, "Failure disconnecting service " + r.name + " to connection " + ((ConnectionRecord) c.get(i)).conn.asBinder() + " (in " + ((ConnectionRecord) c.get(i)).binding.client.processName + ")", e);
                }
            }
        }
        if (!(r.app == null || r.app.thread == null)) {
            for (i = r.bindings.size() - 1; i >= 0; i--) {
                IntentBindRecord ibr = (IntentBindRecord) r.bindings.valueAt(i);
                if (ibr.hasBound) {
                    try {
                        bumpServiceExecutingLocked(r, false, "bring down unbind");
                        this.mAm.updateOomAdjLocked(r.app, true);
                        ibr.hasBound = false;
                        ibr.requested = false;
                        r.app.thread.scheduleUnbindService(r, ibr.intent.getIntent());
                    } catch (Exception e2) {
                        Slog.w(TAG, "Exception when unbinding service " + r.shortName, e2);
                        serviceProcessGoneLocked(r);
                    }
                }
            }
        }
        if (r.fgRequired) {
            Slog.w(TAG_SERVICE, "Bringing down service while still waiting for start foreground: " + r);
            r.fgRequired = false;
            r.fgWaiting = false;
            this.mAm.mHandler.removeMessages(66, r);
            if (r.app != null) {
                Message msg = this.mAm.mHandler.obtainMessage(69);
                msg.obj = r.app;
                this.mAm.mHandler.sendMessage(msg);
            }
        }
        r.destroyTime = SystemClock.uptimeMillis();
        ServiceMap smap = getServiceMapLocked(r.userId);
        ServiceRecord found = (ServiceRecord) smap.mServicesByName.remove(r.name);
        if (found == null || found == r) {
            smap.mServicesByIntent.remove(r.intent);
            r.totalRestartCount = 0;
            unscheduleServiceRestartLocked(r, 0, true);
            for (i = this.mPendingServices.size() - 1; i >= 0; i--) {
                if (this.mPendingServices.get(i) == r) {
                    this.mPendingServices.remove(i);
                }
            }
            cancelForegroundNotificationLocked(r);
            if (r.isForeground) {
                decActiveForegroundAppLocked(smap, r);
            }
            r.isForeground = false;
            r.foregroundId = 0;
            r.foregroundNoti = null;
            r.clearDeliveredStartsLocked();
            r.pendingStarts.clear();
            if (r.app != null) {
                synchronized (r.stats.getBatteryStats()) {
                    r.stats.stopLaunchedLocked();
                }
                r.app.services.remove(r);
                if (r.whitelistManager) {
                    updateWhitelistManagerLocked(r.app);
                }
                if (r.app.thread != null) {
                    updateServiceForegroundLocked(r.app, false);
                    try {
                        bumpServiceExecutingLocked(r, false, "destroy");
                        this.mDestroyingServices.add(r);
                        r.destroying = true;
                        this.mAm.updateOomAdjLocked(r.app, true);
                        r.app.thread.scheduleStopService(r);
                    } catch (Exception e22) {
                        Slog.w(TAG, "Exception when destroying service " + r.shortName, e22);
                        serviceProcessGoneLocked(r);
                    }
                }
            }
            if (r.bindings.size() > 0) {
                r.bindings.clear();
            }
            if (r.restarter instanceof ServiceRestarter) {
                ((ServiceRestarter) r.restarter).setService(null);
            }
            int memFactor = this.mAm.mProcessStats.getMemFactorLocked();
            long now = SystemClock.uptimeMillis();
            if (r.tracker != null) {
                r.tracker.setStarted(false, memFactor, now);
                r.tracker.setBound(false, memFactor, now);
                if (r.executeNesting == 0) {
                    r.tracker.clearCurrentOwner(r, false);
                    r.tracker = null;
                }
            }
            smap.ensureNotStartingBackgroundLocked(r);
            return;
        }
        smap.mServicesByName.put(r.name, found);
        throw new IllegalStateException("Bringing down " + r + " but actually running " + found);
    }

    void removeConnectionLocked(ConnectionRecord c, ProcessRecord skipApp, ActivityRecord skipAct) {
        IBinder binder = c.conn.asBinder();
        AppBindRecord b = c.binding;
        ServiceRecord s = b.service;
        ArrayList<ConnectionRecord> clist = (ArrayList) s.connections.get(binder);
        if (clist != null) {
            clist.remove(c);
            if (clist.size() == 0) {
                s.connections.remove(binder);
            }
        }
        b.connections.remove(c);
        if (!(c.activity == null || c.activity == skipAct || c.activity.connections == null)) {
            c.activity.connections.remove(c);
        }
        if (b.client != skipApp) {
            b.client.connections.remove(c);
            if ((c.flags & 8) != 0) {
                b.client.updateHasAboveClientLocked();
            }
            if ((c.flags & 16777216) != 0) {
                s.updateWhitelistManager();
                if (!(s.whitelistManager || s.app == null)) {
                    updateWhitelistManagerLocked(s.app);
                }
            }
            if (s.app != null) {
                updateServiceClientActivitiesLocked(s.app, c, true);
            }
        }
        clist = (ArrayList) this.mServiceConnections.get(binder);
        if (clist != null) {
            clist.remove(c);
            if (clist.size() == 0) {
                this.mServiceConnections.remove(binder);
            }
        }
        this.mAm.stopAssociationLocked(b.client.uid, b.client.processName, s.appInfo.uid, s.name);
        if (b.connections.size() == 0) {
            b.intent.apps.remove(b.client);
        }
        if (!c.serviceDead) {
            if (s.app != null && s.app.thread != null && b.intent.apps.size() == 0 && b.intent.hasBound) {
                try {
                    bumpServiceExecutingLocked(s, false, "unbind");
                    if (b.client != s.app && (c.flags & 32) == 0 && s.app.setProcState <= 12) {
                        this.mAm.updateLruProcessLocked(s.app, false, null);
                    }
                    this.mAm.updateOomAdjLocked(s.app, true);
                    b.intent.hasBound = false;
                    b.intent.doRebind = false;
                    s.app.thread.scheduleUnbindService(s, b.intent.intent.getIntent());
                } catch (Exception e) {
                    Slog.w(TAG, "Exception when unbinding service " + s.shortName, e);
                    serviceProcessGoneLocked(s);
                }
            }
            this.mPendingServices.remove(s);
            if ((c.flags & 1) != 0) {
                boolean hasAutoCreate = s.hasAutoCreateConnections();
                if (!(hasAutoCreate || s.tracker == null)) {
                    s.tracker.setBound(false, this.mAm.mProcessStats.getMemFactorLocked(), SystemClock.uptimeMillis());
                }
                bringDownServiceIfNeededLocked(s, true, hasAutoCreate);
            }
        }
    }

    void serviceDoneExecutingLocked(ServiceRecord r, int type, int startId, int res) {
        boolean inDestroying = this.mDestroyingServices.contains(r);
        if (r != null) {
            if (type == 1) {
                r.callStart = true;
                switch (res) {
                    case 0:
                    case 1:
                        r.findDeliveredStart(startId, true);
                        r.stopIfKilled = false;
                        break;
                    case 2:
                        r.findDeliveredStart(startId, true);
                        if (r.getLastStartId() == startId) {
                            r.stopIfKilled = true;
                            break;
                        }
                        break;
                    case 3:
                        StartItem si = r.findDeliveredStart(startId, false);
                        if (si != null) {
                            si.deliveryCount = 0;
                            si.doneExecutingCount++;
                            r.stopIfKilled = true;
                            break;
                        }
                        break;
                    case 1000:
                        r.findDeliveredStart(startId, true);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown service start result: " + res);
                }
                if (res == 0) {
                    r.callStart = false;
                }
            } else if (type == 2) {
                if (inDestroying) {
                    if (r.executeNesting != 1) {
                        Slog.w(TAG, "Service done with onDestroy, but executeNesting=" + r.executeNesting + ": " + r);
                        r.executeNesting = 1;
                    }
                } else if (r.app != null) {
                    Slog.w(TAG, "Service done with onDestroy, but not inDestroying: " + r + ", app=" + r.app);
                }
            }
            long origId = Binder.clearCallingIdentity();
            serviceDoneExecutingLocked(r, inDestroying, inDestroying);
            Binder.restoreCallingIdentity(origId);
            return;
        }
        Slog.w(TAG, "Done executing unknown service from pid " + Binder.getCallingPid());
    }

    private void serviceProcessGoneLocked(ServiceRecord r) {
        if (r.tracker != null) {
            int memFactor = this.mAm.mProcessStats.getMemFactorLocked();
            long now = SystemClock.uptimeMillis();
            r.tracker.setExecuting(false, memFactor, now);
            r.tracker.setBound(false, memFactor, now);
            r.tracker.setStarted(false, memFactor, now);
        }
        serviceDoneExecutingLocked(r, true, true);
    }

    private void serviceDoneExecutingLocked(ServiceRecord r, boolean inDestroying, boolean finishing) {
        r.executeNesting--;
        if (r.executeNesting <= 0) {
            if (r.app != null) {
                r.app.execServicesFg = false;
                r.app.executingServices.remove(r);
                if (r.app.executingServices.size() == 0) {
                    this.mAm.mHandler.removeMessages(12, r.app);
                } else if (r.executeFg) {
                    for (int i = r.app.executingServices.size() - 1; i >= 0; i--) {
                        if (((ServiceRecord) r.app.executingServices.valueAt(i)).executeFg) {
                            r.app.execServicesFg = true;
                            break;
                        }
                    }
                }
                if (inDestroying) {
                    this.mDestroyingServices.remove(r);
                    r.bindings.clear();
                }
                this.mAm.updateOomAdjLocked(r.app, true);
            }
            r.executeFg = false;
            if (r.tracker != null) {
                r.tracker.setExecuting(false, this.mAm.mProcessStats.getMemFactorLocked(), SystemClock.uptimeMillis());
                if (finishing) {
                    r.tracker.clearCurrentOwner(r, false);
                    r.tracker = null;
                }
            }
            if (finishing) {
                if (!(r.app == null || (r.app.persistent ^ 1) == 0)) {
                    r.app.services.remove(r);
                    if (r.whitelistManager) {
                        updateWhitelistManagerLocked(r.app);
                    }
                }
                r.app = null;
            }
        }
    }

    boolean attachApplicationLocked(ProcessRecord proc, String processName) throws RemoteException {
        ServiceRecord sr;
        int i;
        boolean didSomething = false;
        if (this.mPendingServices.size() > 0) {
            sr = null;
            i = 0;
            while (i < this.mPendingServices.size()) {
                try {
                    sr = (ServiceRecord) this.mPendingServices.get(i);
                    if (proc == sr.isolatedProc || (proc.uid == sr.appInfo.uid && (processName.equals(sr.processName) ^ 1) == 0)) {
                        this.mPendingServices.remove(i);
                        i--;
                        proc.addPackage(sr.appInfo.packageName, sr.appInfo.versionCode, this.mAm.mProcessStats);
                        realStartServiceLocked(sr, proc, sr.createdFromFg);
                        didSomething = true;
                        if (!isServiceNeededLocked(sr, false, false)) {
                            bringDownServiceLocked(sr);
                        }
                    }
                    i++;
                } catch (RemoteException e) {
                    Slog.w(TAG, "Exception in new application when starting service " + sr.shortName, e);
                    throw e;
                }
            }
        }
        if (this.mRestartingServices.size() > 0) {
            for (i = 0; i < this.mRestartingServices.size(); i++) {
                sr = (ServiceRecord) this.mRestartingServices.get(i);
                if (proc == sr.isolatedProc || (proc.uid == sr.appInfo.uid && (processName.equals(sr.processName) ^ 1) == 0)) {
                    this.mAm.mHandler.removeCallbacks(sr.restarter);
                    this.mAm.mHandler.post(sr.restarter);
                }
            }
        }
        return didSomething;
    }

    void processStartTimedOutLocked(ProcessRecord proc) {
        int i = 0;
        while (i < this.mPendingServices.size()) {
            ServiceRecord sr = (ServiceRecord) this.mPendingServices.get(i);
            if ((proc.uid == sr.appInfo.uid && proc.processName.equals(sr.processName)) || sr.isolatedProc == proc) {
                Slog.w(TAG, "Forcing bringing down service: " + sr);
                sr.isolatedProc = null;
                this.mPendingServices.remove(i);
                i--;
                bringDownServiceLocked(sr);
            }
            i++;
        }
    }

    private boolean collectPackageServicesLocked(String packageName, Set<String> filterByClasses, boolean evenPersistent, boolean doit, boolean killProcess, ArrayMap<ComponentName, ServiceRecord> services) {
        boolean didSomething = false;
        for (int i = services.size() - 1; i >= 0; i--) {
            boolean sameComponent;
            ServiceRecord service = (ServiceRecord) services.valueAt(i);
            if (packageName == null) {
                sameComponent = true;
            } else if (!service.packageName.equals(packageName)) {
                sameComponent = false;
            } else if (filterByClasses != null) {
                sameComponent = filterByClasses.contains(service.name.getClassName());
            } else {
                sameComponent = true;
            }
            if (sameComponent && (service.app == null || evenPersistent || (service.app.persistent ^ 1) != 0)) {
                if (!doit) {
                    return true;
                }
                didSomething = true;
                Slog.i(TAG, "  Force stopping service " + service);
                if (service.app != null) {
                    service.app.removed = killProcess;
                    if (!service.app.persistent) {
                        service.app.services.remove(service);
                        if (service.whitelistManager) {
                            updateWhitelistManagerLocked(service.app);
                        }
                    }
                }
                service.app = null;
                service.isolatedProc = null;
                if (this.mTmpCollectionResults == null) {
                    this.mTmpCollectionResults = new ArrayList();
                }
                this.mTmpCollectionResults.add(service);
            }
        }
        return didSomething;
    }

    boolean bringDownDisabledPackageServicesLocked(String packageName, Set<String> filterByClasses, int userId, boolean evenPersistent, boolean killProcess, boolean doit) {
        int i;
        boolean z = false;
        if (this.mTmpCollectionResults != null) {
            this.mTmpCollectionResults.clear();
        }
        if (userId == -1) {
            for (i = this.mServiceMap.size() - 1; i >= 0; i--) {
                z |= collectPackageServicesLocked(packageName, filterByClasses, evenPersistent, doit, killProcess, ((ServiceMap) this.mServiceMap.valueAt(i)).mServicesByName);
                if (!doit && z) {
                    return true;
                }
                if (doit && filterByClasses == null) {
                    forceStopPackageLocked(packageName, ((ServiceMap) this.mServiceMap.valueAt(i)).mUserId);
                }
            }
        } else {
            ServiceMap smap = (ServiceMap) this.mServiceMap.get(userId);
            if (smap != null) {
                z = collectPackageServicesLocked(packageName, filterByClasses, evenPersistent, doit, killProcess, smap.mServicesByName);
            }
            if (doit && filterByClasses == null) {
                forceStopPackageLocked(packageName, userId);
            }
        }
        if (this.mTmpCollectionResults != null) {
            for (i = this.mTmpCollectionResults.size() - 1; i >= 0; i--) {
                bringDownServiceLocked((ServiceRecord) this.mTmpCollectionResults.get(i));
            }
            this.mTmpCollectionResults.clear();
        }
        return z;
    }

    void forceStopPackageLocked(String packageName, int userId) {
        ServiceMap smap = (ServiceMap) this.mServiceMap.get(userId);
        if (smap != null && smap.mActiveForegroundApps.size() > 0) {
            for (int i = smap.mActiveForegroundApps.size() - 1; i >= 0; i--) {
                if (((ActiveForegroundApp) smap.mActiveForegroundApps.valueAt(i)).mPackageName.equals(packageName)) {
                    smap.mActiveForegroundApps.removeAt(i);
                    smap.mActiveForegroundAppsChanged = true;
                }
            }
            if (smap.mActiveForegroundAppsChanged) {
                requestUpdateActiveForegroundAppsLocked(smap, 0);
            }
        }
    }

    void cleanUpRemovedTaskLocked(TaskRecord tr, ComponentName component, Intent baseIntent) {
        int i;
        ArrayList<ServiceRecord> services = new ArrayList();
        ArrayMap<ComponentName, ServiceRecord> alls = getServicesLocked(tr.userId);
        for (i = alls.size() - 1; i >= 0; i--) {
            ServiceRecord sr = (ServiceRecord) alls.valueAt(i);
            if (sr.packageName.equals(component.getPackageName())) {
                services.add(sr);
            }
        }
        for (i = services.size() - 1; i >= 0; i--) {
            sr = (ServiceRecord) services.get(i);
            if (sr.startRequested) {
                if ((sr.serviceInfo.flags & 1) != 0) {
                    Slog.i(TAG, "Stopping service " + sr.shortName + ": remove task");
                    stopServiceLocked(sr);
                } else {
                    sr.pendingStarts.add(new StartItem(sr, true, sr.makeNextStartId(), baseIntent, null, 0));
                    if (!(sr.app == null || sr.app.thread == null)) {
                        try {
                            sendServiceArgsLocked(sr, true, false);
                        } catch (TransactionTooLargeException e) {
                        }
                    }
                }
            }
        }
    }

    final void killServicesLocked(ProcessRecord app, boolean allowRestart) {
        int i;
        for (i = app.connections.size() - 1; i >= 0; i--) {
            removeConnectionLocked((ConnectionRecord) app.connections.valueAt(i), app, null);
        }
        updateServiceConnectionActivitiesLocked(app);
        app.connections.clear();
        app.whitelistManager = false;
        for (i = app.services.size() - 1; i >= 0; i--) {
            ServiceRecord sr = (ServiceRecord) app.services.valueAt(i);
            synchronized (sr.stats.getBatteryStats()) {
                sr.stats.stopLaunchedLocked();
            }
            if (!(sr.app == app || sr.app == null || (sr.app.persistent ^ 1) == 0)) {
                sr.app.services.remove(sr);
            }
            sr.app = null;
            sr.isolatedProc = null;
            sr.executeNesting = 0;
            sr.forceClearTracker();
            boolean remove = this.mDestroyingServices.remove(sr);
            for (int bindingi = sr.bindings.size() - 1; bindingi >= 0; bindingi--) {
                IntentBindRecord b = (IntentBindRecord) sr.bindings.valueAt(bindingi);
                b.binder = null;
                b.hasBound = false;
                b.received = false;
                b.requested = false;
                for (int appi = b.apps.size() - 1; appi >= 0; appi--) {
                    ProcessRecord proc = (ProcessRecord) b.apps.keyAt(appi);
                    if (!proc.killedByAm && proc.thread != null) {
                        AppBindRecord abind = (AppBindRecord) b.apps.valueAt(appi);
                        for (int conni = abind.connections.size() - 1; conni >= 0; conni--) {
                            if ((((ConnectionRecord) abind.connections.valueAt(conni)).flags & 49) == 1) {
                                break;
                            }
                        }
                    }
                }
            }
        }
        ServiceMap smap = getServiceMapLocked(app.userId);
        for (i = app.services.size() - 1; i >= 0; i--) {
            sr = (ServiceRecord) app.services.valueAt(i);
            if (!app.persistent) {
                app.services.removeAt(i);
            }
            ServiceRecord curRec = (ServiceRecord) smap.mServicesByName.get(sr.name);
            if (curRec != sr) {
                if (curRec != null) {
                    Slog.wtf(TAG, "Service " + sr + " in process " + app + " not same as in map: " + curRec);
                }
            } else if (allowRestart && ((long) sr.crashCount) >= this.mAm.mConstants.BOUND_SERVICE_MAX_CRASH_RETRY && (sr.serviceInfo.applicationInfo.flags & 8) == 0) {
                Slog.w(TAG, "Service crashed " + sr.crashCount + " times, stopping: " + sr);
                EventLog.writeEvent(EventLogTags.AM_SERVICE_CRASHED_TOO_MUCH, new Object[]{Integer.valueOf(sr.userId), Integer.valueOf(sr.crashCount), sr.shortName, Integer.valueOf(app.pid)});
                bringDownServiceLocked(sr);
            } else if (allowRestart && (this.mAm.mUserController.isUserRunningLocked(sr.userId, 0) ^ 1) == 0) {
                boolean canceled = scheduleServiceRestartLocked(sr, true);
                if (sr.startRequested && ((sr.stopIfKilled || canceled) && sr.pendingStarts.size() == 0)) {
                    sr.startRequested = false;
                    if (sr.tracker != null) {
                        sr.tracker.setStarted(false, this.mAm.mProcessStats.getMemFactorLocked(), SystemClock.uptimeMillis());
                    }
                    if (!sr.hasAutoCreateConnections()) {
                        bringDownServiceLocked(sr);
                    }
                }
            } else {
                bringDownServiceLocked(sr);
            }
        }
        if (!allowRestart) {
            ServiceRecord r;
            app.services.clear();
            for (i = this.mRestartingServices.size() - 1; i >= 0; i--) {
                r = (ServiceRecord) this.mRestartingServices.get(i);
                if (r.processName.equals(app.processName) && r.serviceInfo.applicationInfo.uid == app.info.uid) {
                    this.mRestartingServices.remove(i);
                    clearRestartingIfNeededLocked(r);
                }
            }
            for (i = this.mPendingServices.size() - 1; i >= 0; i--) {
                r = (ServiceRecord) this.mPendingServices.get(i);
                if (r.processName.equals(app.processName) && r.serviceInfo.applicationInfo.uid == app.info.uid) {
                    this.mPendingServices.remove(i);
                }
            }
        }
        i = this.mDestroyingServices.size();
        while (i > 0) {
            i--;
            sr = (ServiceRecord) this.mDestroyingServices.get(i);
            if (sr.app == app) {
                sr.forceClearTracker();
                this.mDestroyingServices.remove(i);
            }
        }
        app.executingServices.clear();
    }

    RunningServiceInfo makeRunningServiceInfoLocked(ServiceRecord r) {
        RunningServiceInfo info = new RunningServiceInfo();
        info.service = r.name;
        if (r.app != null) {
            info.pid = r.app.pid;
        }
        info.uid = r.appInfo.uid;
        info.process = r.processName;
        info.foreground = r.isForeground;
        info.activeSince = r.createTime;
        info.started = r.startRequested;
        info.clientCount = r.connections.size();
        info.crashCount = r.crashCount;
        info.lastActivityTime = r.lastActivity;
        if (r.isForeground) {
            info.flags |= 2;
        }
        if (r.startRequested) {
            info.flags |= 1;
        }
        if (r.app != null && r.app.pid == ActivityManagerService.MY_PID) {
            info.flags |= 4;
        }
        if (r.app != null && r.app.persistent) {
            info.flags |= 8;
        }
        for (int conni = r.connections.size() - 1; conni >= 0; conni--) {
            ArrayList<ConnectionRecord> connl = (ArrayList) r.connections.valueAt(conni);
            for (int i = 0; i < connl.size(); i++) {
                ConnectionRecord conn = (ConnectionRecord) connl.get(i);
                if (conn.clientLabel != 0) {
                    info.clientPackage = conn.binding.client.info.packageName;
                    info.clientLabel = conn.clientLabel;
                    return info;
                }
            }
        }
        return info;
    }

    List<RunningServiceInfo> getRunningServiceInfoLocked(int maxNum, int flags, int callingUid, boolean allowed, boolean canInteractAcrossUsers) {
        ArrayList<RunningServiceInfo> res = new ArrayList();
        long ident = Binder.clearCallingIdentity();
        ArrayMap<ComponentName, ServiceRecord> alls;
        int i;
        ServiceRecord r;
        RunningServiceInfo info;
        if (canInteractAcrossUsers) {
            try {
                int[] users = this.mAm.mUserController.getUsers();
                for (int ui = 0; ui < users.length && res.size() < maxNum; ui++) {
                    alls = getServicesLocked(users[ui]);
                    for (i = 0; i < alls.size() && res.size() < maxNum; i++) {
                        res.add(makeRunningServiceInfoLocked((ServiceRecord) alls.valueAt(i)));
                    }
                }
                for (i = 0; i < this.mRestartingServices.size() && res.size() < maxNum; i++) {
                    r = (ServiceRecord) this.mRestartingServices.get(i);
                    info = makeRunningServiceInfoLocked(r);
                    info.restarting = r.nextRestartTime;
                    res.add(info);
                }
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        } else {
            int userId = UserHandle.getUserId(callingUid);
            alls = getServicesLocked(userId);
            for (i = 0; i < alls.size() && res.size() < maxNum; i++) {
                ServiceRecord sr = (ServiceRecord) alls.valueAt(i);
                if (allowed || (sr.app != null && sr.app.uid == callingUid)) {
                    res.add(makeRunningServiceInfoLocked(sr));
                }
            }
            for (i = 0; i < this.mRestartingServices.size() && res.size() < maxNum; i++) {
                r = (ServiceRecord) this.mRestartingServices.get(i);
                if (r.userId == userId && (allowed || (r.app != null && r.app.uid == callingUid))) {
                    info = makeRunningServiceInfoLocked(r);
                    info.restarting = r.nextRestartTime;
                    res.add(info);
                }
            }
        }
        Binder.restoreCallingIdentity(ident);
        return res;
    }

    public PendingIntent getRunningServiceControlPanelLocked(ComponentName name) {
        ServiceRecord r = getServiceByNameLocked(name, UserHandle.getUserId(Binder.getCallingUid()));
        if (r != null) {
            for (int conni = r.connections.size() - 1; conni >= 0; conni--) {
                ArrayList<ConnectionRecord> conn = (ArrayList) r.connections.valueAt(conni);
                for (int i = 0; i < conn.size(); i++) {
                    if (((ConnectionRecord) conn.get(i)).clientIntent != null) {
                        return ((ConnectionRecord) conn.get(i)).clientIntent;
                    }
                }
            }
        }
        return null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void serviceTimeout(com.android.server.am.ProcessRecord r23) {
        /*
        r22 = this;
        r7 = 0;
        r0 = r22;
        r4 = r0.mAm;
        monitor-enter(r4);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x012d }
        r0 = r23;
        r2 = r0.executingServices;	 Catch:{ all -> 0x012d }
        r2 = r2.size();	 Catch:{ all -> 0x012d }
        if (r2 == 0) goto L_0x0019;
    L_0x0013:
        r0 = r23;
        r2 = r0.thread;	 Catch:{ all -> 0x012d }
        if (r2 != 0) goto L_0x001e;
    L_0x0019:
        monitor-exit(r4);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x001e:
        r14 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x012d }
        r0 = r23;
        r2 = r0.execServicesFg;	 Catch:{ all -> 0x012d }
        if (r2 == 0) goto L_0x00f5;
    L_0x0028:
        r2 = 20000; // 0x4e20 float:2.8026E-41 double:9.8813E-320;
    L_0x002a:
        r2 = (long) r2;	 Catch:{ all -> 0x012d }
        r10 = r14 - r2;
        r19 = 0;
        r12 = 0;
        r0 = r23;
        r2 = r0.executingServices;	 Catch:{ all -> 0x012d }
        r2 = r2.size();	 Catch:{ all -> 0x012d }
        r8 = r2 + -1;
    L_0x003b:
        if (r8 < 0) goto L_0x0051;
    L_0x003d:
        r0 = r23;
        r2 = r0.executingServices;	 Catch:{ all -> 0x012d }
        r17 = r2.valueAt(r8);	 Catch:{ all -> 0x012d }
        r17 = (com.android.server.am.ServiceRecord) r17;	 Catch:{ all -> 0x012d }
        r0 = r17;
        r2 = r0.executingStart;	 Catch:{ all -> 0x012d }
        r2 = (r2 > r10 ? 1 : (r2 == r10 ? 0 : -1));
        if (r2 >= 0) goto L_0x00fa;
    L_0x004f:
        r19 = r17;
    L_0x0051:
        if (r19 == 0) goto L_0x010a;
    L_0x0053:
        r0 = r22;
        r2 = r0.mAm;	 Catch:{ all -> 0x012d }
        r2 = r2.mLruProcesses;	 Catch:{ all -> 0x012d }
        r0 = r23;
        r2 = r2.contains(r0);	 Catch:{ all -> 0x012d }
        if (r2 == 0) goto L_0x010a;
    L_0x0061:
        r2 = TAG;	 Catch:{ all -> 0x012d }
        r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x012d }
        r3.<init>();	 Catch:{ all -> 0x012d }
        r5 = "Timeout executing service: ";
        r3 = r3.append(r5);	 Catch:{ all -> 0x012d }
        r0 = r19;
        r3 = r3.append(r0);	 Catch:{ all -> 0x012d }
        r3 = r3.toString();	 Catch:{ all -> 0x012d }
        android.util.Slog.w(r2, r3);	 Catch:{ all -> 0x012d }
        r18 = new java.io.StringWriter;	 Catch:{ all -> 0x012d }
        r18.<init>();	 Catch:{ all -> 0x012d }
        r16 = new com.android.internal.util.FastPrintWriter;	 Catch:{ all -> 0x012d }
        r2 = 0;
        r3 = 1024; // 0x400 float:1.435E-42 double:5.06E-321;
        r0 = r16;
        r1 = r18;
        r0.<init>(r1, r2, r3);	 Catch:{ all -> 0x012d }
        r0 = r16;
        r1 = r19;
        r0.println(r1);	 Catch:{ all -> 0x012d }
        r2 = "    ";
        r0 = r19;
        r1 = r16;
        r0.dump(r1, r2);	 Catch:{ all -> 0x012d }
        r16.close();	 Catch:{ all -> 0x012d }
        r2 = r18.toString();	 Catch:{ all -> 0x012d }
        r0 = r22;
        r0.mLastAnrDump = r2;	 Catch:{ all -> 0x012d }
        r0 = r22;
        r2 = r0.mAm;	 Catch:{ all -> 0x012d }
        r2 = r2.mHandler;	 Catch:{ all -> 0x012d }
        r0 = r22;
        r3 = r0.mLastAnrDumpClearer;	 Catch:{ all -> 0x012d }
        r2.removeCallbacks(r3);	 Catch:{ all -> 0x012d }
        r0 = r22;
        r2 = r0.mAm;	 Catch:{ all -> 0x012d }
        r2 = r2.mHandler;	 Catch:{ all -> 0x012d }
        r0 = r22;
        r3 = r0.mLastAnrDumpClearer;	 Catch:{ all -> 0x012d }
        r20 = 7200000; // 0x6ddd00 float:1.0089349E-38 double:3.5572727E-317;
        r0 = r20;
        r2.postDelayed(r3, r0);	 Catch:{ all -> 0x012d }
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x012d }
        r2.<init>();	 Catch:{ all -> 0x012d }
        r3 = "executing service ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x012d }
        r0 = r19;
        r3 = r0.shortName;	 Catch:{ all -> 0x012d }
        r2 = r2.append(r3);	 Catch:{ all -> 0x012d }
        r7 = r2.toString();	 Catch:{ all -> 0x012d }
    L_0x00e0:
        monitor-exit(r4);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        if (r7 == 0) goto L_0x00f4;
    L_0x00e6:
        r0 = r22;
        r2 = r0.mAm;
        r2 = r2.mAppErrors;
        r4 = 0;
        r5 = 0;
        r6 = 0;
        r3 = r23;
        r2.appNotResponding(r3, r4, r5, r6, r7);
    L_0x00f4:
        return;
    L_0x00f5:
        r2 = 200000; // 0x30d40 float:2.8026E-40 double:9.8813E-319;
        goto L_0x002a;
    L_0x00fa:
        r0 = r17;
        r2 = r0.executingStart;	 Catch:{ all -> 0x012d }
        r2 = (r2 > r12 ? 1 : (r2 == r12 ? 0 : -1));
        if (r2 <= 0) goto L_0x0106;
    L_0x0102:
        r0 = r17;
        r12 = r0.executingStart;	 Catch:{ all -> 0x012d }
    L_0x0106:
        r8 = r8 + -1;
        goto L_0x003b;
    L_0x010a:
        r0 = r22;
        r2 = r0.mAm;	 Catch:{ all -> 0x012d }
        r2 = r2.mHandler;	 Catch:{ all -> 0x012d }
        r3 = 12;
        r9 = r2.obtainMessage(r3);	 Catch:{ all -> 0x012d }
        r0 = r23;
        r9.obj = r0;	 Catch:{ all -> 0x012d }
        r0 = r22;
        r2 = r0.mAm;	 Catch:{ all -> 0x012d }
        r5 = r2.mHandler;	 Catch:{ all -> 0x012d }
        r0 = r23;
        r2 = r0.execServicesFg;	 Catch:{ all -> 0x012d }
        if (r2 == 0) goto L_0x0133;
    L_0x0126:
        r2 = 20000; // 0x4e20 float:2.8026E-41 double:9.8813E-320;
        r2 = r2 + r12;
    L_0x0129:
        r5.sendMessageAtTime(r9, r2);	 Catch:{ all -> 0x012d }
        goto L_0x00e0;
    L_0x012d:
        r2 = move-exception;
        monitor-exit(r4);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r2;
    L_0x0133:
        r2 = 200000; // 0x30d40 float:2.8026E-40 double:9.8813E-319;
        r2 = r2 + r12;
        goto L_0x0129;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.ActiveServices.serviceTimeout(com.android.server.am.ProcessRecord):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void serviceForegroundTimeout(com.android.server.am.ServiceRecord r7) {
        /*
        r6 = this;
        r4 = 0;
        r2 = 0;
        r3 = r6.mAm;
        monitor-enter(r3);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x002f }
        r0 = r7.fgRequired;	 Catch:{ all -> 0x002f }
        if (r0 == 0) goto L_0x0010;
    L_0x000c:
        r0 = r7.destroying;	 Catch:{ all -> 0x002f }
        if (r0 == 0) goto L_0x0015;
    L_0x0010:
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0015:
        r1 = r7.app;	 Catch:{ all -> 0x002f }
        r0 = 0;
        r7.fgWaiting = r0;	 Catch:{ all -> 0x002f }
        r6.stopServiceLocked(r7);	 Catch:{ all -> 0x002f }
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        if (r1 == 0) goto L_0x002e;
    L_0x0023:
        r0 = r6.mAm;
        r0 = r0.mAppErrors;
        r5 = "Context.startForegroundService() did not then call Service.startForeground()";
        r3 = r2;
        r0.appNotResponding(r1, r2, r3, r4, r5);
    L_0x002e:
        return;
    L_0x002f:
        r0 = move-exception;
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.ActiveServices.serviceForegroundTimeout(com.android.server.am.ServiceRecord):void");
    }

    void serviceForegroundCrash(ProcessRecord app) {
        this.mAm.crashApplication(app.uid, app.pid, app.info.packageName, app.userId, "Context.startForegroundService() did not then call Service.startForeground()");
    }

    void scheduleServiceTimeoutLocked(ProcessRecord proc) {
        if (proc.executingServices.size() != 0 && proc.thread != null) {
            Message msg = this.mAm.mHandler.obtainMessage(12);
            msg.obj = proc;
            this.mAm.mHandler.sendMessageDelayed(msg, (long) (proc.execServicesFg ? SERVICE_TIMEOUT : SERVICE_BACKGROUND_TIMEOUT));
        }
    }

    void scheduleServiceForegroundTransitionTimeoutLocked(ServiceRecord r) {
        if (r.app.executingServices.size() != 0 && r.app.thread != null) {
            Message msg = this.mAm.mHandler.obtainMessage(66);
            msg.obj = r;
            r.fgWaiting = true;
            this.mAm.mHandler.sendMessageDelayed(msg, 5000);
        }
    }

    ServiceDumper newServiceDumperLocked(FileDescriptor fd, PrintWriter pw, String[] args, int opti, boolean dumpAll, String dumpPackage) {
        return new ServiceDumper(fd, pw, args, opti, dumpAll, dumpPackage);
    }

    protected boolean dumpService(FileDescriptor fd, PrintWriter pw, String name, String[] args, int opti, boolean dumpAll) {
        ArrayList<ServiceRecord> services = new ArrayList();
        synchronized (this.mAm) {
            int i;
            try {
                ActivityManagerService.boostPriorityForLockedSection();
                int[] users = this.mAm.mUserController.getUsers();
                ServiceMap smap;
                ArrayMap<ComponentName, ServiceRecord> alls;
                if ("all".equals(name)) {
                    for (int user : users) {
                        smap = (ServiceMap) this.mServiceMap.get(user);
                        if (smap != null) {
                            alls = smap.mServicesByName;
                            for (i = 0; i < alls.size(); i++) {
                                services.add((ServiceRecord) alls.valueAt(i));
                            }
                        }
                    }
                } else {
                    Object unflattenFromString = name != null ? ComponentName.unflattenFromString(name) : null;
                    int objectId = 0;
                    if (unflattenFromString == null) {
                        objectId = Integer.parseInt(name, 16);
                        name = null;
                        unflattenFromString = null;
                    }
                    for (int user2 : users) {
                        smap = (ServiceMap) this.mServiceMap.get(user2);
                        if (smap != null) {
                            alls = smap.mServicesByName;
                            for (i = 0; i < alls.size(); i++) {
                                ServiceRecord r1 = (ServiceRecord) alls.valueAt(i);
                                if (unflattenFromString != null) {
                                    if (r1.name.equals(unflattenFromString)) {
                                        services.add(r1);
                                    }
                                } else if (name != null) {
                                    if (r1.name.flattenToString().contains(name)) {
                                        services.add(r1);
                                    }
                                } else if (System.identityHashCode(r1) == objectId) {
                                    services.add(r1);
                                }
                            }
                            continue;
                        }
                    }
                }
            } catch (RuntimeException e) {
            } catch (Throwable th) {
                ActivityManagerService.resetPriorityAfterLockedSection();
            }
        }
        ActivityManagerService.resetPriorityAfterLockedSection();
        if (services.size() <= 0) {
            return false;
        }
        boolean needSep = false;
        for (i = 0; i < services.size(); i++) {
            if (needSep) {
                pw.println();
            }
            needSep = true;
            dumpService("", fd, pw, (ServiceRecord) services.get(i), args, dumpAll);
        }
        return true;
    }

    private void dumpService(String prefix, FileDescriptor fd, PrintWriter pw, ServiceRecord r, String[] args, boolean dumpAll) {
        String innerPrefix = prefix + "  ";
        synchronized (this.mAm) {
            try {
                ActivityManagerService.boostPriorityForLockedSection();
                pw.print(prefix);
                pw.print("SERVICE ");
                pw.print(r.shortName);
                pw.print(" ");
                pw.print(Integer.toHexString(System.identityHashCode(r)));
                pw.print(" pid=");
                if (r.app != null) {
                    pw.println(r.app.pid);
                } else {
                    pw.println("(not running)");
                }
                if (dumpAll) {
                    r.dump(pw, innerPrefix);
                }
            } finally {
                ActivityManagerService.resetPriorityAfterLockedSection();
            }
        }
        if (r.app != null && r.app.thread != null) {
            pw.print(prefix);
            pw.println("  Client:");
            pw.flush();
            TransferPipe tp;
            try {
                tp = new TransferPipe();
                r.app.thread.dumpService(tp.getWriteFd(), r, args);
                tp.setBufferPrefix(prefix + "    ");
                tp.go(fd);
                tp.kill();
            } catch (IOException e) {
                pw.println(prefix + "    Failure while dumping the service: " + e);
            } catch (RemoteException e2) {
                pw.println(prefix + "    Got a RemoteException while dumping the service");
            } catch (Throwable th) {
                tp.kill();
            }
        }
    }
}
