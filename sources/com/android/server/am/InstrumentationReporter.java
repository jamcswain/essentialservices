package com.android.server.am;

import android.app.IInstrumentationWatcher;
import android.content.ComponentName;
import android.os.Bundle;
import java.util.ArrayList;

public class InstrumentationReporter {
    static final boolean DEBUG = false;
    static final int REPORT_TYPE_FINISHED = 1;
    static final int REPORT_TYPE_STATUS = 0;
    static final String TAG = "ActivityManager";
    final Object mLock = new Object();
    ArrayList<Report> mPendingReports;
    Thread mThread;

    final class MyThread extends Thread {
        public MyThread() {
            super("InstrumentationReporter");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r10 = this;
            r6 = 0;
            android.os.Process.setThreadPriority(r6);
            r5 = 0;
        L_0x0005:
            r6 = com.android.server.am.InstrumentationReporter.this;
            r7 = r6.mLock;
            monitor-enter(r7);
            r6 = com.android.server.am.InstrumentationReporter.this;	 Catch:{ all -> 0x0051 }
            r4 = r6.mPendingReports;	 Catch:{ all -> 0x0051 }
            r6 = com.android.server.am.InstrumentationReporter.this;	 Catch:{ all -> 0x0051 }
            r8 = 0;
            r6.mPendingReports = r8;	 Catch:{ all -> 0x0051 }
            if (r4 == 0) goto L_0x001b;
        L_0x0015:
            r6 = r4.isEmpty();	 Catch:{ all -> 0x0051 }
            if (r6 == 0) goto L_0x0030;
        L_0x001b:
            if (r5 != 0) goto L_0x0029;
        L_0x001d:
            r6 = com.android.server.am.InstrumentationReporter.this;	 Catch:{ InterruptedException -> 0x008b }
            r6 = r6.mLock;	 Catch:{ InterruptedException -> 0x008b }
            r8 = 10000; // 0x2710 float:1.4013E-41 double:4.9407E-320;
            r6.wait(r8);	 Catch:{ InterruptedException -> 0x008b }
        L_0x0026:
            r5 = 1;
            monitor-exit(r7);
            goto L_0x0005;
        L_0x0029:
            r6 = com.android.server.am.InstrumentationReporter.this;	 Catch:{ all -> 0x0051 }
            r8 = 0;
            r6.mThread = r8;	 Catch:{ all -> 0x0051 }
            monitor-exit(r7);
            return;
        L_0x0030:
            monitor-exit(r7);
            r5 = 0;
            r2 = 0;
        L_0x0033:
            r6 = r4.size();
            if (r2 >= r6) goto L_0x0005;
        L_0x0039:
            r3 = r4.get(r2);
            r3 = (com.android.server.am.InstrumentationReporter.Report) r3;
            r6 = r3.mType;	 Catch:{ RemoteException -> 0x0060 }
            if (r6 != 0) goto L_0x0054;
        L_0x0043:
            r6 = r3.mWatcher;	 Catch:{ RemoteException -> 0x0060 }
            r7 = r3.mName;	 Catch:{ RemoteException -> 0x0060 }
            r8 = r3.mResultCode;	 Catch:{ RemoteException -> 0x0060 }
            r9 = r3.mResults;	 Catch:{ RemoteException -> 0x0060 }
            r6.instrumentationStatus(r7, r8, r9);	 Catch:{ RemoteException -> 0x0060 }
        L_0x004e:
            r2 = r2 + 1;
            goto L_0x0033;
        L_0x0051:
            r6 = move-exception;
            monitor-exit(r7);
            throw r6;
        L_0x0054:
            r6 = r3.mWatcher;	 Catch:{ RemoteException -> 0x0060 }
            r7 = r3.mName;	 Catch:{ RemoteException -> 0x0060 }
            r8 = r3.mResultCode;	 Catch:{ RemoteException -> 0x0060 }
            r9 = r3.mResults;	 Catch:{ RemoteException -> 0x0060 }
            r6.instrumentationFinished(r7, r8, r9);	 Catch:{ RemoteException -> 0x0060 }
            goto L_0x004e;
        L_0x0060:
            r0 = move-exception;
            r6 = "ActivityManager";
            r7 = new java.lang.StringBuilder;
            r7.<init>();
            r8 = "Failure reporting to instrumentation watcher: comp=";
            r7 = r7.append(r8);
            r8 = r3.mName;
            r7 = r7.append(r8);
            r8 = " results=";
            r7 = r7.append(r8);
            r8 = r3.mResults;
            r7 = r7.append(r8);
            r7 = r7.toString();
            android.util.Slog.i(r6, r7);
            goto L_0x004e;
        L_0x008b:
            r1 = move-exception;
            goto L_0x0026;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.InstrumentationReporter.MyThread.run():void");
        }
    }

    final class Report {
        final ComponentName mName;
        final int mResultCode;
        final Bundle mResults;
        final int mType;
        final IInstrumentationWatcher mWatcher;

        Report(int type, IInstrumentationWatcher watcher, ComponentName name, int resultCode, Bundle results) {
            this.mType = type;
            this.mWatcher = watcher;
            this.mName = name;
            this.mResultCode = resultCode;
            this.mResults = results;
        }
    }

    public void reportStatus(IInstrumentationWatcher watcher, ComponentName name, int resultCode, Bundle results) {
        report(new Report(0, watcher, name, resultCode, results));
    }

    public void reportFinished(IInstrumentationWatcher watcher, ComponentName name, int resultCode, Bundle results) {
        report(new Report(1, watcher, name, resultCode, results));
    }

    private void report(Report report) {
        synchronized (this.mLock) {
            if (this.mThread == null) {
                this.mThread = new MyThread();
                this.mThread.start();
            }
            if (this.mPendingReports == null) {
                this.mPendingReports = new ArrayList();
            }
            this.mPendingReports.add(report);
            this.mLock.notifyAll();
        }
    }
}
