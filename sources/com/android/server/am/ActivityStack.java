package com.android.server.am;

import android.app.ActivityManager.RunningTaskInfo;
import android.app.ActivityManager.StackId;
import android.app.ActivityOptions;
import android.app.AppGlobals;
import android.app.IActivityController;
import android.app.IApplicationThread;
import android.app.ResultInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ActivityInfo.WindowLayout;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.Trace;
import android.os.UserHandle;
import android.service.voice.IVoiceInteractionSession;
import android.util.ArraySet;
import android.util.EventLog;
import android.util.IntArray;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import com.android.internal.app.IVoiceInteractor;
import com.android.internal.os.BatteryStatsImpl;
import com.android.internal.os.BatteryStatsImpl.Uid.Proc;
import com.android.server.Watchdog;
import com.android.server.job.controllers.JobStatus;
import com.android.server.wm.StackWindowController;
import com.android.server.wm.StackWindowListener;
import com.android.server.wm.WindowManagerService;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

class ActivityStack<T extends StackWindowController> extends ConfigurationContainer implements StackWindowListener {
    private static final /* synthetic */ int[] -com-android-server-am-ActivityStack$ActivityStateSwitchesValues = null;
    private static final long ACTIVITY_INACTIVE_RESET_TIME = 0;
    static final int DESTROY_ACTIVITIES_MSG = 105;
    private static final int DESTROY_TIMEOUT = 10000;
    static final int DESTROY_TIMEOUT_MSG = 102;
    static final int FINISH_AFTER_PAUSE = 1;
    static final int FINISH_AFTER_VISIBLE = 2;
    static final int FINISH_IMMEDIATELY = 0;
    private static final int FIT_WITHIN_BOUNDS_DIVIDER = 3;
    static final int LAUNCH_TICK = 500;
    static final int LAUNCH_TICK_MSG = 103;
    private static final int MAX_STOPPING_TO_FORCE = 3;
    private static final int PAUSE_TIMEOUT = 500;
    static final int PAUSE_TIMEOUT_MSG = 101;
    protected static final int REMOVE_TASK_MODE_DESTROYING = 0;
    static final int REMOVE_TASK_MODE_MOVING = 1;
    static final int REMOVE_TASK_MODE_MOVING_TO_TOP = 2;
    private static final boolean SHOW_APP_STARTING_PREVIEW = true;
    static final int STACK_INVISIBLE = 0;
    static final int STACK_VISIBLE = 1;
    private static final int STOP_TIMEOUT = 10000;
    static final int STOP_TIMEOUT_MSG = 104;
    private static final String TAG = "ActivityManager";
    private static final String TAG_ADD_REMOVE = (TAG + ActivityManagerDebugConfig.POSTFIX_ADD_REMOVE);
    private static final String TAG_APP = (TAG + ActivityManagerDebugConfig.POSTFIX_APP);
    private static final String TAG_CLEANUP = (TAG + ActivityManagerDebugConfig.POSTFIX_CLEANUP);
    private static final String TAG_CONTAINERS = (TAG + ActivityManagerDebugConfig.POSTFIX_CONTAINERS);
    private static final String TAG_PAUSE = (TAG + ActivityManagerDebugConfig.POSTFIX_PAUSE);
    private static final String TAG_RELEASE = (TAG + ActivityManagerDebugConfig.POSTFIX_RELEASE);
    private static final String TAG_RESULTS = (TAG + ActivityManagerDebugConfig.POSTFIX_RESULTS);
    private static final String TAG_SAVED_STATE = (TAG + ActivityManagerDebugConfig.POSTFIX_SAVED_STATE);
    private static final String TAG_STACK = (TAG + ActivityManagerDebugConfig.POSTFIX_STACK);
    private static final String TAG_STATES = (TAG + ActivityManagerDebugConfig.POSTFIX_STATES);
    private static final String TAG_SWITCH = (TAG + ActivityManagerDebugConfig.POSTFIX_SWITCH);
    private static final String TAG_TASKS = (TAG + ActivityManagerDebugConfig.POSTFIX_TASKS);
    private static final String TAG_TRANSITION = (TAG + ActivityManagerDebugConfig.POSTFIX_TRANSITION);
    private static final String TAG_USER_LEAVING = (TAG + ActivityManagerDebugConfig.POSTFIX_USER_LEAVING);
    private static final String TAG_VISIBILITY = (TAG + ActivityManagerDebugConfig.POSTFIX_VISIBILITY);
    private static final long TRANSLUCENT_CONVERSION_TIMEOUT = 2000;
    static final int TRANSLUCENT_TIMEOUT_MSG = 106;
    Rect mBounds = null;
    boolean mConfigWillChange;
    int mCurrentUser;
    private final Rect mDeferredBounds = new Rect();
    private final Rect mDeferredTaskBounds = new Rect();
    private final Rect mDeferredTaskInsetBounds = new Rect();
    int mDisplayId;
    boolean mForceHidden = false;
    boolean mFullscreen = true;
    long mFullyDrawnStartTime = 0;
    final Handler mHandler;
    final ArrayList<ActivityRecord> mLRUActivities = new ArrayList();
    ActivityRecord mLastNoHistoryActivity = null;
    ActivityRecord mLastPausedActivity = null;
    long mLaunchStartTime = 0;
    final ArrayList<ActivityRecord> mNoAnimActivities = new ArrayList();
    ActivityRecord mPausingActivity = null;
    private final RecentTasks mRecentTasks;
    ActivityRecord mResumedActivity = null;
    final ActivityManagerService mService;
    final int mStackId;
    protected final ActivityStackSupervisor mStackSupervisor;
    ArrayList<ActivityStack> mStacks;
    private final ArrayList<TaskRecord> mTaskHistory = new ArrayList();
    private final LaunchingTaskPositioner mTaskPositioner;
    private final SparseArray<Rect> mTmpBounds = new SparseArray();
    private final SparseArray<Configuration> mTmpConfigs = new SparseArray();
    private final SparseArray<Rect> mTmpInsetBounds = new SparseArray();
    private final Rect mTmpRect2 = new Rect();
    private boolean mTopActivityOccludesKeyguard;
    private ActivityRecord mTopDismissingKeyguardActivity;
    ActivityRecord mTranslucentActivityWaiting = null;
    ArrayList<ActivityRecord> mUndrawnActivitiesBelowTopTranslucent = new ArrayList();
    private boolean mUpdateBoundsDeferred;
    private boolean mUpdateBoundsDeferredCalled;
    T mWindowContainerController;
    private final WindowManagerService mWindowManager;

    private class ActivityStackHandler extends Handler {
        ActivityStackHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            IBinder iBinder = null;
            ActivityRecord r;
            switch (msg.what) {
                case 101:
                    r = msg.obj;
                    Slog.w(ActivityStack.TAG, "Activity pause timeout for " + r);
                    synchronized (ActivityStack.this.mService) {
                        try {
                            ActivityManagerService.boostPriorityForLockedSection();
                            if (r.app != null) {
                                ActivityStack.this.mService.logAppTooSlow(r.app, r.pauseTime, "pausing " + r);
                            }
                            ActivityStack.this.activityPausedLocked(r.appToken, true);
                        } finally {
                            ActivityManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                case 102:
                    r = (ActivityRecord) msg.obj;
                    Slog.w(ActivityStack.TAG, "Activity destroy timeout for " + r);
                    synchronized (ActivityStack.this.mService) {
                        try {
                            ActivityManagerService.boostPriorityForLockedSection();
                            ActivityStack activityStack = ActivityStack.this;
                            if (r != null) {
                                iBinder = r.appToken;
                            }
                            activityStack.activityDestroyedLocked(iBinder, "destroyTimeout");
                        } finally {
                            ActivityManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                case 103:
                    r = (ActivityRecord) msg.obj;
                    synchronized (ActivityStack.this.mService) {
                        try {
                            ActivityManagerService.boostPriorityForLockedSection();
                            if (r.continueLaunchTickingLocked()) {
                                ActivityStack.this.mService.logAppTooSlow(r.app, r.launchTickTime, "launching " + r);
                            }
                        } finally {
                            ActivityManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                case 104:
                    r = (ActivityRecord) msg.obj;
                    Slog.w(ActivityStack.TAG, "Activity stop timeout for " + r);
                    synchronized (ActivityStack.this.mService) {
                        try {
                            ActivityManagerService.boostPriorityForLockedSection();
                            if (r.isInHistory()) {
                                r.activityStoppedLocked(null, null, null);
                            }
                        } finally {
                            ActivityManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                case 105:
                    ScheduleDestroyArgs args = msg.obj;
                    synchronized (ActivityStack.this.mService) {
                        try {
                            ActivityManagerService.boostPriorityForLockedSection();
                            ActivityStack.this.destroyActivitiesLocked(args.mOwner, args.mReason);
                        } finally {
                            ActivityManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                case 106:
                    synchronized (ActivityStack.this.mService) {
                        try {
                            ActivityManagerService.boostPriorityForLockedSection();
                            ActivityStack.this.notifyActivityDrawnLocked(null);
                        } finally {
                            ActivityManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                default:
                    return;
            }
        }
    }

    enum ActivityState {
        INITIALIZING,
        RESUMED,
        PAUSING,
        PAUSED,
        STOPPING,
        STOPPED,
        FINISHING,
        DESTROYING,
        DESTROYED
    }

    private static class ScheduleDestroyArgs {
        final ProcessRecord mOwner;
        final String mReason;

        ScheduleDestroyArgs(ProcessRecord owner, String reason) {
            this.mOwner = owner;
            this.mReason = reason;
        }
    }

    private static /* synthetic */ int[] -getcom-android-server-am-ActivityStack$ActivityStateSwitchesValues() {
        if (-com-android-server-am-ActivityStack$ActivityStateSwitchesValues != null) {
            return -com-android-server-am-ActivityStack$ActivityStateSwitchesValues;
        }
        int[] iArr = new int[ActivityState.values().length];
        try {
            iArr[ActivityState.DESTROYED.ordinal()] = 7;
        } catch (NoSuchFieldError e) {
        }
        try {
            iArr[ActivityState.DESTROYING.ordinal()] = 8;
        } catch (NoSuchFieldError e2) {
        }
        try {
            iArr[ActivityState.FINISHING.ordinal()] = 9;
        } catch (NoSuchFieldError e3) {
        }
        try {
            iArr[ActivityState.INITIALIZING.ordinal()] = 1;
        } catch (NoSuchFieldError e4) {
        }
        try {
            iArr[ActivityState.PAUSED.ordinal()] = 2;
        } catch (NoSuchFieldError e5) {
        }
        try {
            iArr[ActivityState.PAUSING.ordinal()] = 3;
        } catch (NoSuchFieldError e6) {
        }
        try {
            iArr[ActivityState.RESUMED.ordinal()] = 4;
        } catch (NoSuchFieldError e7) {
        }
        try {
            iArr[ActivityState.STOPPED.ordinal()] = 5;
        } catch (NoSuchFieldError e8) {
        }
        try {
            iArr[ActivityState.STOPPING.ordinal()] = 6;
        } catch (NoSuchFieldError e9) {
        }
        -com-android-server-am-ActivityStack$ActivityStateSwitchesValues = iArr;
        return iArr;
    }

    protected int getChildCount() {
        return this.mTaskHistory.size();
    }

    protected ConfigurationContainer getChildAt(int index) {
        return (ConfigurationContainer) this.mTaskHistory.get(index);
    }

    protected ConfigurationContainer getParent() {
        return getDisplay();
    }

    void onParentChanged() {
        super.onParentChanged();
        this.mStackSupervisor.updateUIDsPresentOnDisplay();
    }

    int numActivities() {
        int count = 0;
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            count += ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities.size();
        }
        return count;
    }

    ActivityStack(ActivityDisplay display, int stackId, ActivityStackSupervisor supervisor, RecentTasks recentTasks, boolean onTop) {
        Rect rect = null;
        this.mStackSupervisor = supervisor;
        this.mService = supervisor.mService;
        this.mHandler = new ActivityStackHandler(this.mService.mHandler.getLooper());
        this.mWindowManager = this.mService.mWindowManager;
        this.mStackId = stackId;
        this.mCurrentUser = this.mService.mUserController.getCurrentUserIdLocked();
        this.mRecentTasks = recentTasks;
        this.mTaskPositioner = this.mStackId == 2 ? new LaunchingTaskPositioner() : null;
        this.mTmpRect2.setEmpty();
        this.mWindowContainerController = createStackWindowController(display.mDisplayId, onTop, this.mTmpRect2);
        this.mStackSupervisor.mStacks.put(this.mStackId, this);
        if (!this.mTmpRect2.isEmpty()) {
            rect = this.mTmpRect2;
        }
        postAddToDisplay(display, rect, onTop);
    }

    T createStackWindowController(int displayId, boolean onTop, Rect outBounds) {
        return new StackWindowController(this.mStackId, this, displayId, onTop, outBounds);
    }

    T getWindowContainerController() {
        return this.mWindowContainerController;
    }

    void reparent(ActivityDisplay activityDisplay, boolean onTop) {
        removeFromDisplay();
        this.mTmpRect2.setEmpty();
        postAddToDisplay(activityDisplay, this.mTmpRect2.isEmpty() ? null : this.mTmpRect2, onTop);
        adjustFocusToNextFocusableStackLocked("reparent", true);
        this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
        this.mStackSupervisor.ensureActivitiesVisibleLocked(null, 0, false);
        this.mWindowContainerController.reparent(activityDisplay.mDisplayId, this.mTmpRect2, onTop);
    }

    private void postAddToDisplay(ActivityDisplay activityDisplay, Rect bounds, boolean onTop) {
        this.mDisplayId = activityDisplay.mDisplayId;
        this.mStacks = activityDisplay.mStacks;
        this.mBounds = bounds != null ? new Rect(bounds) : null;
        this.mFullscreen = this.mBounds == null;
        if (this.mTaskPositioner != null) {
            this.mTaskPositioner.setDisplay(activityDisplay.mDisplay);
            this.mTaskPositioner.configure(this.mBounds);
        }
        onParentChanged();
        activityDisplay.attachStack(this, findStackInsertIndex(onTop));
        if (this.mStackId == 3) {
            this.mStackSupervisor.resizeDockedStackLocked(this.mBounds, null, null, null, null, true);
        }
    }

    private void removeFromDisplay() {
        ActivityDisplay display = getDisplay();
        if (display != null) {
            display.detachStack(this);
        }
        this.mDisplayId = -1;
        this.mStacks = null;
        if (this.mTaskPositioner != null) {
            this.mTaskPositioner.reset();
        }
        if (this.mStackId == 3) {
            this.mStackSupervisor.resizeDockedStackLocked(null, null, null, null, null, true);
        }
    }

    void remove() {
        removeFromDisplay();
        this.mStackSupervisor.mStacks.remove(this.mStackId);
        this.mWindowContainerController.removeContainer();
        this.mWindowContainerController = null;
        onParentChanged();
    }

    ActivityDisplay getDisplay() {
        return this.mStackSupervisor.getActivityDisplay(this.mDisplayId);
    }

    void getDisplaySize(Point out) {
        getDisplay().mDisplay.getSize(out);
    }

    void getStackDockedModeBounds(Rect currentTempTaskBounds, Rect outStackBounds, Rect outTempTaskBounds, boolean ignoreVisibility) {
        this.mWindowContainerController.getStackDockedModeBounds(currentTempTaskBounds, outStackBounds, outTempTaskBounds, ignoreVisibility);
    }

    void prepareFreezingTaskBounds() {
        this.mWindowContainerController.prepareFreezingTaskBounds();
    }

    void getWindowContainerBounds(Rect outBounds) {
        if (this.mWindowContainerController != null) {
            this.mWindowContainerController.getBounds(outBounds);
        } else {
            outBounds.setEmpty();
        }
    }

    void getBoundsForNewConfiguration(Rect outBounds) {
        this.mWindowContainerController.getBoundsForNewConfiguration(outBounds);
    }

    void positionChildWindowContainerAtTop(TaskRecord child) {
        this.mWindowContainerController.positionChildAtTop(child.getWindowContainerController(), true);
    }

    boolean deferScheduleMultiWindowModeChanged() {
        return false;
    }

    void deferUpdateBounds() {
        if (!this.mUpdateBoundsDeferred) {
            this.mUpdateBoundsDeferred = true;
            this.mUpdateBoundsDeferredCalled = false;
        }
    }

    void continueUpdateBounds() {
        Rect rect = null;
        boolean wasDeferred = this.mUpdateBoundsDeferred;
        this.mUpdateBoundsDeferred = false;
        if (wasDeferred && this.mUpdateBoundsDeferredCalled) {
            Rect rect2 = this.mDeferredBounds.isEmpty() ? null : this.mDeferredBounds;
            Rect rect3 = this.mDeferredTaskBounds.isEmpty() ? null : this.mDeferredTaskBounds;
            if (!this.mDeferredTaskInsetBounds.isEmpty()) {
                rect = this.mDeferredTaskInsetBounds;
            }
            resize(rect2, rect3, rect);
        }
    }

    boolean updateBoundsAllowed(Rect bounds, Rect tempTaskBounds, Rect tempTaskInsetBounds) {
        if (!this.mUpdateBoundsDeferred) {
            return true;
        }
        if (bounds != null) {
            this.mDeferredBounds.set(bounds);
        } else {
            this.mDeferredBounds.setEmpty();
        }
        if (tempTaskBounds != null) {
            this.mDeferredTaskBounds.set(tempTaskBounds);
        } else {
            this.mDeferredTaskBounds.setEmpty();
        }
        if (tempTaskInsetBounds != null) {
            this.mDeferredTaskInsetBounds.set(tempTaskInsetBounds);
        } else {
            this.mDeferredTaskInsetBounds.setEmpty();
        }
        this.mUpdateBoundsDeferredCalled = true;
        return false;
    }

    void setBounds(Rect bounds) {
        Rect rect = null;
        if (!this.mFullscreen) {
            rect = new Rect(bounds);
        }
        this.mBounds = rect;
        if (this.mTaskPositioner != null) {
            this.mTaskPositioner.configure(bounds);
        }
    }

    ActivityRecord topRunningActivityLocked() {
        return topRunningActivityLocked(false);
    }

    void getAllRunningVisibleActivitiesLocked(ArrayList<ActivityRecord> outActivities) {
        outActivities.clear();
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            ((TaskRecord) this.mTaskHistory.get(taskNdx)).getAllRunningVisibleActivitiesLocked(outActivities);
        }
    }

    private ActivityRecord topRunningActivityLocked(boolean focusableOnly) {
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            ActivityRecord r = ((TaskRecord) this.mTaskHistory.get(taskNdx)).topRunningActivityLocked();
            if (r != null && (!focusableOnly || r.isFocusable())) {
                return r;
            }
        }
        return null;
    }

    ActivityRecord topRunningNonOverlayTaskActivity() {
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                if (!r.finishing && (r.mTaskOverlay ^ 1) != 0) {
                    return r;
                }
            }
        }
        return null;
    }

    ActivityRecord topRunningNonDelayedActivityLocked(ActivityRecord notTop) {
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                if (!r.finishing && (r.delayedResume ^ 1) != 0 && r != notTop && r.okToShowLocked()) {
                    return r;
                }
            }
        }
        return null;
    }

    final ActivityRecord topRunningActivityLocked(IBinder token, int taskId) {
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
            if (task.taskId != taskId) {
                ArrayList<ActivityRecord> activities = task.mActivities;
                for (int i = activities.size() - 1; i >= 0; i--) {
                    ActivityRecord r = (ActivityRecord) activities.get(i);
                    if (!r.finishing && token != r.appToken && r.okToShowLocked()) {
                        return r;
                    }
                }
                continue;
            }
        }
        return null;
    }

    final ActivityRecord topActivity() {
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                if (!r.finishing) {
                    return r;
                }
            }
        }
        return null;
    }

    final TaskRecord topTask() {
        int size = this.mTaskHistory.size();
        if (size > 0) {
            return (TaskRecord) this.mTaskHistory.get(size - 1);
        }
        return null;
    }

    final TaskRecord bottomTask() {
        if (this.mTaskHistory.isEmpty()) {
            return null;
        }
        return (TaskRecord) this.mTaskHistory.get(0);
    }

    TaskRecord taskForIdLocked(int id) {
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
            if (task.taskId == id) {
                return task;
            }
        }
        return null;
    }

    ActivityRecord isInStackLocked(IBinder token) {
        return isInStackLocked(ActivityRecord.forTokenLocked(token));
    }

    ActivityRecord isInStackLocked(ActivityRecord r) {
        if (r == null) {
            return null;
        }
        TaskRecord task = r.getTask();
        ActivityStack stack = r.getStack();
        if (stack == null || !task.mActivities.contains(r) || !this.mTaskHistory.contains(task)) {
            return null;
        }
        if (stack != this) {
            Slog.w(TAG, "Illegal state! task does not point to stack it is in.");
        }
        return r;
    }

    boolean isInStackLocked(TaskRecord task) {
        return this.mTaskHistory.contains(task);
    }

    boolean isUidPresent(int uid) {
        for (TaskRecord task : this.mTaskHistory) {
            for (ActivityRecord r : task.mActivities) {
                if (r.getUid() == uid) {
                    return true;
                }
            }
        }
        return false;
    }

    void getPresentUIDs(IntArray presentUIDs) {
        for (TaskRecord task : this.mTaskHistory) {
            for (ActivityRecord r : task.mActivities) {
                presentUIDs.add(r.getUid());
            }
        }
    }

    final void removeActivitiesFromLRUListLocked(TaskRecord task) {
        for (ActivityRecord r : task.mActivities) {
            this.mLRUActivities.remove(r);
        }
    }

    final boolean updateLRUListLocked(ActivityRecord r) {
        boolean hadit = this.mLRUActivities.remove(r);
        this.mLRUActivities.add(r);
        return hadit;
    }

    final boolean isHomeStack() {
        return this.mStackId == 0;
    }

    final boolean isRecentsStack() {
        return this.mStackId == 5;
    }

    final boolean isHomeOrRecentsStack() {
        return StackId.isHomeOrRecentsStack(this.mStackId);
    }

    final boolean isDockedStack() {
        return this.mStackId == 3;
    }

    final boolean isPinnedStack() {
        return this.mStackId == 4;
    }

    final boolean isAssistantStack() {
        return this.mStackId == 6;
    }

    final boolean isOnHomeDisplay() {
        return isAttached() && this.mDisplayId == 0;
    }

    void moveToFront(String reason) {
        moveToFront(reason, null);
    }

    void moveToFront(String reason, TaskRecord task) {
        if (isAttached()) {
            this.mStacks.remove(this);
            this.mStacks.add(findStackInsertIndex(true), this);
            this.mStackSupervisor.setFocusStackUnchecked(reason, this);
            if (task != null) {
                insertTaskAtTop(task, null);
                return;
            }
            task = topTask();
            if (task != null) {
                this.mWindowContainerController.positionChildAtTop(task.getWindowContainerController(), true);
            }
        }
    }

    private void moveToBack(TaskRecord task) {
        if (isAttached()) {
            this.mStacks.remove(this);
            this.mStacks.add(0, this);
            if (task != null) {
                this.mTaskHistory.remove(task);
                this.mTaskHistory.add(0, task);
                updateTaskMovement(task, false);
                this.mWindowContainerController.positionChildAtBottom(task.getWindowContainerController());
            }
        }
    }

    private int findStackInsertIndex(boolean onTop) {
        if (!onTop) {
            return 0;
        }
        int addIndex = this.mStacks.size();
        if (addIndex > 0) {
            ActivityStack topStack = (ActivityStack) this.mStacks.get(addIndex - 1);
            if (StackId.isAlwaysOnTop(topStack.mStackId) && topStack != this) {
                addIndex--;
            }
        }
        return addIndex;
    }

    boolean isFocusable() {
        if (StackId.canReceiveKeys(this.mStackId)) {
            return true;
        }
        ActivityRecord r = topRunningActivityLocked();
        return r != null ? r.isFocusable() : false;
    }

    final boolean isAttached() {
        return this.mStacks != null;
    }

    void findTaskLocked(ActivityRecord target, FindTaskResult result) {
        Intent intent = target.intent;
        ActivityInfo info = target.info;
        ComponentName cls = intent.getComponent();
        if (info.targetActivity != null) {
            cls = new ComponentName(info.packageName, info.targetActivity);
        }
        int userId = UserHandle.getUserId(info.applicationInfo.uid);
        boolean isDocument = (intent != null ? 1 : 0) & intent.isDocument();
        Object data = isDocument ? intent.getData() : null;
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
            if (task.voiceSession == null && task.userId == userId) {
                ActivityRecord r = task.getTopActivity(false);
                if (!(r == null || r.finishing || r.userId != userId || r.launchMode == 3 || r.mActivityType != target.mActivityType)) {
                    Intent taskIntent = task.intent;
                    Intent affinityIntent = task.affinityIntent;
                    boolean taskIsDocument;
                    Object data2;
                    if (taskIntent != null && taskIntent.isDocument()) {
                        taskIsDocument = true;
                        data2 = taskIntent.getData();
                    } else if (affinityIntent == null || !affinityIntent.isDocument()) {
                        taskIsDocument = false;
                        data2 = null;
                    } else {
                        taskIsDocument = true;
                        data2 = affinityIntent.getData();
                    }
                    if (taskIntent != null && taskIntent.getComponent() != null && taskIntent.getComponent().compareTo(cls) == 0 && Objects.equals(data, r9)) {
                        result.r = r;
                        result.matchedByRootAffinity = false;
                        return;
                    } else if (affinityIntent != null && affinityIntent.getComponent() != null && affinityIntent.getComponent().compareTo(cls) == 0 && Objects.equals(data, r9)) {
                        result.r = r;
                        result.matchedByRootAffinity = false;
                        return;
                    } else if (!(isDocument || (taskIsDocument ^ 1) == 0 || result.r != null || task.rootAffinity == null || !task.rootAffinity.equals(target.taskAffinity))) {
                        result.r = r;
                        result.matchedByRootAffinity = true;
                    }
                }
            }
        }
    }

    ActivityRecord findActivityLocked(Intent intent, ActivityInfo info, boolean compareIntentFilters) {
        ComponentName cls = intent.getComponent();
        if (info.targetActivity != null) {
            cls = new ComponentName(info.packageName, info.targetActivity);
        }
        int userId = UserHandle.getUserId(info.applicationInfo.uid);
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                if (r.okToShowLocked() && !r.finishing && r.userId == userId) {
                    if (compareIntentFilters) {
                        if (r.intent.filterEquals(intent)) {
                            return r;
                        }
                    } else if (r.intent.getComponent().equals(cls)) {
                        return r;
                    }
                }
            }
        }
        return null;
    }

    final void switchUserLocked(int userId) {
        if (this.mCurrentUser != userId) {
            this.mCurrentUser = userId;
            int index = this.mTaskHistory.size();
            int i = 0;
            while (i < index) {
                TaskRecord task = (TaskRecord) this.mTaskHistory.get(i);
                if (task.okToShowLocked()) {
                    this.mTaskHistory.remove(i);
                    this.mTaskHistory.add(task);
                    index--;
                } else {
                    i++;
                }
            }
        }
    }

    void minimalResumeActivityLocked(ActivityRecord r) {
        setResumedActivityLocked(r, "minimalResumeActivityLocked");
        r.completeResumeLocked();
        setLaunchTime(r);
    }

    void addRecentActivityLocked(ActivityRecord r) {
        if (r != null) {
            TaskRecord task = r.getTask();
            this.mRecentTasks.addLocked(task);
            task.touchActiveTime();
        }
    }

    private void startLaunchTraces(String packageName) {
        if (this.mFullyDrawnStartTime != 0) {
            Trace.asyncTraceEnd(64, "drawing", 0);
        }
        Trace.asyncTraceBegin(64, "launching: " + packageName, 0);
        Trace.asyncTraceBegin(64, "drawing", 0);
    }

    private void stopFullyDrawnTraceIfNeeded() {
        if (this.mFullyDrawnStartTime != 0 && this.mLaunchStartTime == 0) {
            Trace.asyncTraceEnd(64, "drawing", 0);
            this.mFullyDrawnStartTime = 0;
        }
    }

    void setLaunchTime(ActivityRecord r) {
        long uptimeMillis;
        if (r.displayStartTime == 0) {
            uptimeMillis = SystemClock.uptimeMillis();
            r.displayStartTime = uptimeMillis;
            r.fullyDrawnStartTime = uptimeMillis;
            if (this.mLaunchStartTime == 0) {
                startLaunchTraces(r.packageName);
                uptimeMillis = r.displayStartTime;
                this.mFullyDrawnStartTime = uptimeMillis;
                this.mLaunchStartTime = uptimeMillis;
            }
        } else if (this.mLaunchStartTime == 0) {
            startLaunchTraces(r.packageName);
            uptimeMillis = SystemClock.uptimeMillis();
            this.mFullyDrawnStartTime = uptimeMillis;
            this.mLaunchStartTime = uptimeMillis;
        }
    }

    private void clearLaunchTime(ActivityRecord r) {
        if (this.mStackSupervisor.mWaitingActivityLaunched.isEmpty()) {
            r.fullyDrawnStartTime = 0;
            r.displayStartTime = 0;
            return;
        }
        this.mStackSupervisor.removeTimeoutsForActivityLocked(r);
        this.mStackSupervisor.scheduleIdleTimeoutLocked(r);
    }

    void awakeFromSleepingLocked() {
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                ((ActivityRecord) activities.get(activityNdx)).setSleeping(false);
            }
        }
        if (this.mPausingActivity != null) {
            Slog.d(TAG, "awakeFromSleepingLocked: previously pausing activity didn't pause");
            activityPausedLocked(this.mPausingActivity.appToken, true);
        }
    }

    void updateActivityApplicationInfoLocked(ApplicationInfo aInfo) {
        String packageName = aInfo.packageName;
        int userId = UserHandle.getUserId(aInfo.uid);
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            List<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                ActivityRecord ar = (ActivityRecord) activities.get(activityNdx);
                if (userId == ar.userId && packageName.equals(ar.packageName)) {
                    ar.info.applicationInfo = aInfo;
                }
            }
        }
    }

    void checkReadyForSleep() {
        if (shouldSleepActivities() && goToSleepIfPossible(false)) {
            this.mStackSupervisor.checkReadyForSleepLocked(true);
        }
    }

    boolean goToSleepIfPossible(boolean shuttingDown) {
        boolean shouldSleep = true;
        if (this.mResumedActivity != null) {
            if (!this.mStackSupervisor.inResumeTopActivity) {
                startPausingLocked(false, true, null, false);
            }
            shouldSleep = false;
        } else if (this.mPausingActivity != null) {
            shouldSleep = false;
        }
        if (!shuttingDown) {
            if (containsActivityFromStack(this.mStackSupervisor.mStoppingActivities)) {
                this.mStackSupervisor.scheduleIdleLocked();
                shouldSleep = false;
            }
            if (containsActivityFromStack(this.mStackSupervisor.mGoingToSleepActivities)) {
                shouldSleep = false;
            }
        }
        if (shouldSleep) {
            goToSleep();
        }
        return shouldSleep;
    }

    void goToSleep() {
        ensureActivitiesVisibleLocked(null, 0, false);
        for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
            ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                if (!(r.state == ActivityState.STOPPING || r.state == ActivityState.STOPPED || r.state == ActivityState.PAUSED)) {
                    if (r.state != ActivityState.PAUSING) {
                    }
                }
                r.setSleeping(true);
            }
        }
    }

    private boolean containsActivityFromStack(List<ActivityRecord> rs) {
        for (ActivityRecord r : rs) {
            if (r.getStack() == this) {
                return true;
            }
        }
        return false;
    }

    private void schedulePauseTimeout(ActivityRecord r) {
        Message msg = this.mHandler.obtainMessage(101);
        msg.obj = r;
        r.pauseTime = SystemClock.uptimeMillis();
        this.mHandler.sendMessageDelayed(msg, 500);
    }

    final boolean startPausingLocked(boolean userLeaving, boolean uiSleeping, ActivityRecord resuming, boolean pauseImmediately) {
        if (this.mPausingActivity != null) {
            Slog.wtf(TAG, "Going to pause when pause is already pending for " + this.mPausingActivity + " state=" + this.mPausingActivity.state);
            if (!shouldSleepActivities()) {
                completePauseLocked(false, resuming);
            }
        }
        ActivityRecord prev = this.mResumedActivity;
        if (prev == null) {
            if (resuming == null) {
                Slog.wtf(TAG, "Trying to pause when nothing is resumed");
                this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
            }
            return false;
        }
        this.mResumedActivity = null;
        this.mPausingActivity = prev;
        this.mLastPausedActivity = prev;
        ActivityRecord activityRecord = ((prev.intent.getFlags() & 1073741824) == 0 && (prev.info.flags & 128) == 0) ? null : prev;
        this.mLastNoHistoryActivity = activityRecord;
        prev.state = ActivityState.PAUSING;
        prev.getTask().touchActiveTime();
        clearLaunchTime(prev);
        ActivityRecord next = this.mStackSupervisor.topRunningActivityLocked();
        if (this.mService.mHasRecents) {
            if (!(next == null || next.noDisplay || next.getTask() != prev.getTask())) {
                if (uiSleeping) {
                }
            }
            prev.mUpdateTaskThumbnailWhenHidden = true;
        }
        stopFullyDrawnTraceIfNeeded();
        this.mService.updateCpuStats();
        if (prev.app == null || prev.app.thread == null) {
            this.mPausingActivity = null;
            this.mLastPausedActivity = null;
            this.mLastNoHistoryActivity = null;
        } else {
            try {
                EventLog.writeEvent(EventLogTags.AM_PAUSE_ACTIVITY, new Object[]{Integer.valueOf(prev.userId), Integer.valueOf(System.identityHashCode(prev)), prev.shortComponentName});
                this.mService.updateUsageStats(prev, false);
                prev.app.thread.schedulePauseActivity(prev.appToken, prev.finishing, userLeaving, prev.configChangeFlags, pauseImmediately);
            } catch (Exception e) {
                Slog.w(TAG, "Exception thrown during pause", e);
                this.mPausingActivity = null;
                this.mLastPausedActivity = null;
                this.mLastNoHistoryActivity = null;
            }
        }
        if (!(uiSleeping || (this.mService.isSleepingOrShuttingDownLocked() ^ 1) == 0)) {
            this.mStackSupervisor.acquireLaunchWakelock();
        }
        if (this.mPausingActivity != null) {
            if (!uiSleeping) {
                prev.pauseKeyDispatchingLocked();
            }
            if (pauseImmediately) {
                completePauseLocked(false, resuming);
                return false;
            }
            schedulePauseTimeout(prev);
            return true;
        }
        if (resuming == null) {
            this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
        }
        return false;
    }

    final void activityPausedLocked(IBinder token, boolean timeout) {
        ActivityRecord r = isInStackLocked(token);
        if (r != null) {
            this.mHandler.removeMessages(101, r);
            if (this.mPausingActivity == r) {
                this.mService.mWindowManager.deferSurfaceLayout();
                try {
                    completePauseLocked(true, null);
                    return;
                } finally {
                    this.mService.mWindowManager.continueSurfaceLayout();
                }
            } else {
                Object[] objArr = new Object[4];
                objArr[0] = Integer.valueOf(r.userId);
                objArr[1] = Integer.valueOf(System.identityHashCode(r));
                objArr[2] = r.shortComponentName;
                objArr[3] = this.mPausingActivity != null ? this.mPausingActivity.shortComponentName : "(none)";
                EventLog.writeEvent(EventLogTags.AM_FAILED_TO_PAUSE, objArr);
                if (r.state == ActivityState.PAUSING) {
                    r.state = ActivityState.PAUSED;
                    if (r.finishing) {
                        finishCurrentActivityLocked(r, 2, false);
                    }
                }
            }
        }
        this.mStackSupervisor.ensureActivitiesVisibleLocked(null, 0, false);
    }

    private void completePauseLocked(boolean resumeNext, ActivityRecord resuming) {
        ActivityRecord prev = this.mPausingActivity;
        if (prev != null) {
            boolean wasStopping = prev.state == ActivityState.STOPPING;
            prev.state = ActivityState.PAUSED;
            if (prev.finishing) {
                prev = finishCurrentActivityLocked(prev, 2, false);
            } else if (prev.app != null) {
                boolean remove = this.mStackSupervisor.mActivitiesWaitingForVisibleActivity.remove(prev);
                if (prev.deferRelaunchUntilPaused) {
                    prev.relaunchActivityLocked(false, prev.preserveWindowOnDeferredRelaunch);
                } else if (wasStopping) {
                    prev.state = ActivityState.STOPPING;
                } else if (!prev.visible || shouldSleepOrShutDownActivities()) {
                    prev.setDeferHidingClient(false);
                    addToStopping(prev, true, false);
                }
            } else {
                prev = null;
            }
            if (prev != null) {
                prev.stopFreezingScreenLocked(true);
            }
            this.mPausingActivity = null;
        }
        if (resumeNext) {
            ActivityStack topStack = this.mStackSupervisor.getFocusedStack();
            if (topStack.shouldSleepOrShutDownActivities()) {
                checkReadyForSleep();
                ActivityRecord top = topStack.topRunningActivityLocked();
                if (top == null || !(prev == null || top == prev)) {
                    this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                }
            } else {
                this.mStackSupervisor.resumeFocusedStackTopActivityLocked(topStack, prev, null);
            }
        }
        if (prev != null) {
            prev.resumeKeyDispatchingLocked();
            if (prev.app != null && prev.cpuTimeAtResume > 0 && this.mService.mBatteryStatsService.isOnBattery()) {
                long diff = this.mService.mProcessCpuTracker.getCpuTimeForPid(prev.app.pid) - prev.cpuTimeAtResume;
                if (diff > 0) {
                    BatteryStatsImpl bsi = this.mService.mBatteryStatsService.getActiveStatistics();
                    synchronized (bsi) {
                        Proc ps = bsi.getProcessStatsLocked(prev.info.applicationInfo.uid, prev.info.packageName);
                        if (ps != null) {
                            ps.addForegroundTimeLocked(diff);
                        }
                    }
                }
            }
            prev.cpuTimeAtResume = 0;
        }
        if (this.mStackSupervisor.mAppVisibilitiesChangedSinceLastPause || this.mService.mStackSupervisor.getStack(4) != null) {
            this.mService.mTaskChangeNotificationController.notifyTaskStackChanged();
            this.mStackSupervisor.mAppVisibilitiesChangedSinceLastPause = false;
        }
        this.mStackSupervisor.ensureActivitiesVisibleLocked(resuming, 0, false);
    }

    void addToStopping(ActivityRecord r, boolean scheduleIdle, boolean idleDelayed) {
        if (!this.mStackSupervisor.mStoppingActivities.contains(r)) {
            this.mStackSupervisor.mStoppingActivities.add(r);
        }
        boolean forceIdle = this.mStackSupervisor.mStoppingActivities.size() <= 3 ? r.frontOfTask && this.mTaskHistory.size() <= 1 : true;
        if (!scheduleIdle && !forceIdle) {
            checkReadyForSleep();
        } else if (idleDelayed) {
            this.mStackSupervisor.scheduleIdleTimeoutLocked(r);
        } else {
            this.mStackSupervisor.scheduleIdleLocked();
        }
    }

    ActivityRecord findNextTranslucentActivity(ActivityRecord r) {
        TaskRecord task = r.getTask();
        if (task == null) {
            return null;
        }
        ActivityStack stack = task.getStack();
        if (stack == null) {
            return null;
        }
        int taskNdx = stack.mTaskHistory.indexOf(task);
        int activityNdx = task.mActivities.indexOf(r) + 1;
        int numStacks = this.mStacks.size();
        for (int stackNdx = this.mStacks.indexOf(stack); stackNdx < numStacks; stackNdx++) {
            ActivityStack historyStack = (ActivityStack) this.mStacks.get(stackNdx);
            ArrayList<TaskRecord> tasks = historyStack.mTaskHistory;
            int numTasks = tasks.size();
            for (taskNdx = 
/*
Method generation error in method: com.android.server.am.ActivityStack.findNextTranslucentActivity(com.android.server.am.ActivityRecord):com.android.server.am.ActivityRecord, dex: 
jadx.core.utils.exceptions.CodegenException: Error generate insn: PHI: (r11_1 'taskNdx' int) = (r11_0 'taskNdx' int), (r11_4 'taskNdx' int) binds: {(r11_0 'taskNdx' int)=B:8:0x0010, (r11_4 'taskNdx' int)=B:26:0x0069} in method: com.android.server.am.ActivityStack.findNextTranslucentActivity(com.android.server.am.ActivityRecord):com.android.server.am.ActivityRecord, dex: 
	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:226)
	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:184)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:61)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:93)
	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:190)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:61)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:187)
	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:320)
	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:257)
	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:220)
	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:75)
	at jadx.core.codegen.CodeGen.visit(CodeGen.java:12)
	at jadx.core.ProcessClass.process(ProcessClass.java:40)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
Caused by: jadx.core.utils.exceptions.CodegenException: PHI can be used only in fallback mode
	at jadx.core.codegen.InsnGen.fallbackOnlyInsn(InsnGen.java:537)
	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:509)
	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
	... 28 more

*/

            private boolean hasFullscreenTask() {
                for (int i = this.mTaskHistory.size() - 1; i >= 0; i--) {
                    if (((TaskRecord) this.mTaskHistory.get(i)).mFullscreen) {
                        return true;
                    }
                }
                return false;
            }

            private boolean isStackTranslucent(ActivityRecord starting, int stackBehindId) {
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
                    ArrayList<ActivityRecord> activities = task.mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        if (!r.finishing && (r.visible || r == starting)) {
                            if (r.fullscreen) {
                                return false;
                            }
                            if (!(isHomeOrRecentsStack() || !r.frontOfTask || !task.isOverHomeStack() || (StackId.isHomeOrRecentsStack(stackBehindId) ^ 1) == 0 || (isAssistantStack() ^ 1) == 0)) {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }

            boolean isVisible() {
                if (this.mWindowContainerController == null || !this.mWindowContainerController.isVisible()) {
                    return false;
                }
                return this.mForceHidden ^ 1;
            }

            int shouldBeVisible(ActivityRecord starting) {
                int i = 1;
                if (!isAttached() || this.mForceHidden) {
                    return 0;
                }
                if (this.mStackSupervisor.isFrontStackOnDisplay(this) || this.mStackSupervisor.isFocusedStack(this)) {
                    return 1;
                }
                int stackIndex = this.mStacks.indexOf(this);
                if (stackIndex == this.mStacks.size() - 1) {
                    Slog.wtf(TAG, "Stack=" + this + " isn't front stack but is at the top of the stack list");
                    return 0;
                }
                ActivityStack topStack = getTopStackOnDisplay();
                int topStackId = topStack.mStackId;
                if (this.mStackId != 3) {
                    if (this.mStackId == 0) {
                        int dockedStackIndex = this.mStacks.indexOf(this.mStackSupervisor.getStack(3));
                        if (dockedStackIndex > stackIndex && stackIndex != dockedStackIndex - 1) {
                            return 0;
                        }
                    }
                    int stackBehindTopIndex = this.mStacks.indexOf(topStack) - 1;
                    while (stackBehindTopIndex >= 0 && ((ActivityStack) this.mStacks.get(stackBehindTopIndex)).topRunningActivityLocked() == null) {
                        stackBehindTopIndex--;
                    }
                    int stackBehindTopId = stackBehindTopIndex >= 0 ? ((ActivityStack) this.mStacks.get(stackBehindTopIndex)).mStackId : -1;
                    if (topStackId == 3 || StackId.isAlwaysOnTop(topStackId)) {
                        if (stackIndex == stackBehindTopIndex) {
                            return 1;
                        }
                        if (StackId.isAlwaysOnTop(topStackId) && stackIndex == stackBehindTopIndex - 1) {
                            if (stackBehindTopId == 3) {
                                return 1;
                            }
                            if (stackBehindTopId == 6) {
                                if (!((ActivityStack) this.mStacks.get(stackBehindTopIndex)).isStackTranslucent(starting, this.mStackId)) {
                                    i = 0;
                                }
                                return i;
                            }
                        }
                    }
                    if (StackId.isBackdropToTranslucentActivity(topStackId) && topStack.isStackTranslucent(starting, stackBehindTopId)) {
                        if (stackIndex == stackBehindTopIndex) {
                            return 1;
                        }
                        if (stackBehindTopIndex >= 0 && ((stackBehindTopId == 3 || stackBehindTopId == 4) && stackIndex == stackBehindTopIndex - 1)) {
                            return 1;
                        }
                    }
                    if (StackId.isStaticStack(this.mStackId)) {
                        return 0;
                    }
                    for (int i2 = stackIndex + 1; i2 < this.mStacks.size(); i2++) {
                        ActivityStack stack = (ActivityStack) this.mStacks.get(i2);
                        if ((stack.mFullscreen || (stack.hasFullscreenTask() ^ 1) == 0) && (!StackId.isDynamicStacksVisibleBehindAllowed(stack.mStackId) || !stack.isStackTranslucent(starting, -1))) {
                            return 0;
                        }
                    }
                    return 1;
                } else if (!topStack.isAssistantStack()) {
                    return 1;
                } else {
                    return topStack.isStackTranslucent(starting, 3) ? 1 : 0;
                }
            }

            final int rankTaskLayers(int baseLayer) {
                int taskNdx = this.mTaskHistory.size() - 1;
                int layer = 0;
                while (taskNdx >= 0) {
                    int layer2;
                    TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
                    ActivityRecord r = task.topRunningActivityLocked();
                    if (r == null || r.finishing || (r.visible ^ 1) != 0) {
                        task.mLayerRank = -1;
                        layer2 = layer;
                    } else {
                        layer2 = layer + 1;
                        task.mLayerRank = baseLayer + layer;
                    }
                    taskNdx--;
                    layer = layer2;
                }
                return layer;
            }

            final void ensureActivitiesVisibleLocked(ActivityRecord starting, int configChanges, boolean preserveWindows) {
                this.mTopActivityOccludesKeyguard = false;
                this.mTopDismissingKeyguardActivity = null;
                this.mStackSupervisor.mKeyguardController.beginActivityVisibilityUpdate();
                try {
                    ActivityRecord top = topRunningActivityLocked();
                    if (top != null) {
                        checkTranslucentActivityWaiting(top);
                    }
                    boolean aboveTop = top != null;
                    int stackVisibility = shouldBeVisible(starting);
                    boolean stackInvisible = stackVisibility != 1;
                    boolean behindFullscreenActivity = stackInvisible;
                    boolean resumeNextActivity = this.mStackSupervisor.isFocusedStack(this) ? isInStackLocked(starting) == null : false;
                    for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                        TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
                        ArrayList<ActivityRecord> activities = task.mActivities;
                        int activityNdx = activities.size() - 1;
                        while (activityNdx >= 0) {
                            ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                            if (!r.finishing) {
                                boolean isTop = r == top;
                                if (!aboveTop || (isTop ^ 1) == 0) {
                                    aboveTop = false;
                                    boolean visibleIgnoringKeyguard = r.shouldBeVisibleIgnoringKeyguard(behindFullscreenActivity);
                                    r.visibleIgnoringKeyguard = visibleIgnoringKeyguard;
                                    boolean reallyVisible = checkKeyguardVisibility(r, visibleIgnoringKeyguard, isTop);
                                    if (visibleIgnoringKeyguard) {
                                        behindFullscreenActivity = updateBehindFullscreen(stackInvisible, behindFullscreenActivity, task, r);
                                        if (behindFullscreenActivity && (r.fullscreen ^ 1) != 0) {
                                        }
                                    }
                                    if (reallyVisible) {
                                        if (r != starting) {
                                            r.ensureActivityConfigurationLocked(0, preserveWindows);
                                        }
                                        if (r.app == null || r.app.thread == null) {
                                            if (makeVisibleAndRestartIfNeeded(starting, configChanges, isTop, resumeNextActivity, r)) {
                                                if (activityNdx >= activities.size()) {
                                                    activityNdx = activities.size() - 1;
                                                } else {
                                                    resumeNextActivity = false;
                                                }
                                            }
                                        } else if (!r.visible) {
                                            r.makeVisibleIfNeeded(starting);
                                        } else if (r.handleAlreadyVisible()) {
                                            resumeNextActivity = false;
                                        }
                                        configChanges |= r.configChangeFlags;
                                    } else {
                                        makeInvisible(r);
                                    }
                                }
                            } else if (r.mUpdateTaskThumbnailWhenHidden) {
                                r.updateThumbnailLocked(r.screenshotActivityLocked(), null);
                                r.mUpdateTaskThumbnailWhenHidden = false;
                            }
                            activityNdx--;
                        }
                        if (this.mStackId == 2) {
                            if (stackVisibility == 0) {
                                behindFullscreenActivity = true;
                            } else {
                                behindFullscreenActivity = false;
                            }
                        } else if (this.mStackId == 0) {
                            if (task.isHomeTask()) {
                                behindFullscreenActivity = true;
                            } else if (task.isRecentsTask() && task.getTaskToReturnTo() == 0) {
                                behindFullscreenActivity = true;
                            }
                        } else if (!(this.mStackId != 1 || task.topRunningActivityLocked() == null || task.getTaskToReturnTo() == 0)) {
                            behindFullscreenActivity = true;
                        }
                    }
                    if (this.mTranslucentActivityWaiting != null && this.mUndrawnActivitiesBelowTopTranslucent.isEmpty()) {
                        notifyActivityDrawnLocked(null);
                    }
                    this.mStackSupervisor.mKeyguardController.endActivityVisibilityUpdate();
                } catch (Throwable th) {
                    this.mStackSupervisor.mKeyguardController.endActivityVisibilityUpdate();
                }
            }

            void addStartingWindowsForVisibleActivities(boolean taskSwitch) {
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ((TaskRecord) this.mTaskHistory.get(taskNdx)).addStartingWindowsForVisibleActivities(taskSwitch);
                }
            }

            boolean topActivityOccludesKeyguard() {
                return this.mTopActivityOccludesKeyguard;
            }

            ActivityRecord getTopDismissingKeyguardActivity() {
                return this.mTopDismissingKeyguardActivity;
            }

            boolean checkKeyguardVisibility(ActivityRecord r, boolean shouldBeVisible, boolean isTop) {
                int i;
                boolean z = false;
                boolean isInPinnedStack = r.getStack().getStackId() == 4;
                KeyguardController keyguardController = this.mStackSupervisor.mKeyguardController;
                if (this.mDisplayId != -1) {
                    i = this.mDisplayId;
                } else {
                    i = 0;
                }
                boolean keyguardShowing = keyguardController.isKeyguardShowing(i);
                boolean keyguardLocked = this.mStackSupervisor.mKeyguardController.isKeyguardLocked();
                boolean z2 = r.canShowWhenLocked() ? isInPinnedStack ^ 1 : false;
                boolean dismissKeyguard = r.hasDismissKeyguardWindows();
                if (shouldBeVisible) {
                    boolean canDismissKeyguard;
                    if (dismissKeyguard && this.mTopDismissingKeyguardActivity == null) {
                        this.mTopDismissingKeyguardActivity = r;
                    }
                    if (isTop) {
                        this.mTopActivityOccludesKeyguard |= z2;
                    }
                    if (canShowWithInsecureKeyguard()) {
                        canDismissKeyguard = this.mStackSupervisor.mKeyguardController.canDismissKeyguard();
                    } else {
                        canDismissKeyguard = false;
                    }
                    if (canDismissKeyguard) {
                        return true;
                    }
                }
                if (keyguardShowing) {
                    if (shouldBeVisible) {
                        z = this.mStackSupervisor.mKeyguardController.canShowActivityWhileKeyguardShowing(r, dismissKeyguard);
                    }
                    return z;
                } else if (!keyguardLocked) {
                    return shouldBeVisible;
                } else {
                    if (shouldBeVisible) {
                        z = this.mStackSupervisor.mKeyguardController.canShowWhileOccluded(dismissKeyguard, z2);
                    }
                    return z;
                }
            }

            private boolean canShowWithInsecureKeyguard() {
                ActivityDisplay activityDisplay = getDisplay();
                if (activityDisplay == null) {
                    throw new IllegalStateException("Stack is not attached to any display, stackId=" + this.mStackId);
                } else if ((activityDisplay.mDisplay.getFlags() & 32) != 0) {
                    return true;
                } else {
                    return false;
                }
            }

            private void checkTranslucentActivityWaiting(ActivityRecord top) {
                if (this.mTranslucentActivityWaiting != top) {
                    this.mUndrawnActivitiesBelowTopTranslucent.clear();
                    if (this.mTranslucentActivityWaiting != null) {
                        notifyActivityDrawnLocked(null);
                        this.mTranslucentActivityWaiting = null;
                    }
                    this.mHandler.removeMessages(106);
                }
            }

            private boolean makeVisibleAndRestartIfNeeded(ActivityRecord starting, int configChanges, boolean isTop, boolean andResume, ActivityRecord r) {
                if (isTop || (r.visible ^ 1) != 0) {
                    if (r != starting) {
                        r.startFreezingScreenLocked(r.app, configChanges);
                    }
                    if (!r.visible || r.mLaunchTaskBehind) {
                        r.setVisible(true);
                    }
                    if (r != starting) {
                        this.mStackSupervisor.startSpecificActivityLocked(r, andResume, false);
                        return true;
                    }
                }
                return false;
            }

            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            private void makeInvisible(com.android.server.am.ActivityRecord r7) {
                /*
                r6 = this;
                r3 = r7.visible;
                if (r3 != 0) goto L_0x0005;
            L_0x0004:
                return;
            L_0x0005:
                r3 = "makeInvisible";
                r4 = 1;
                r0 = r7.checkEnterPictureInPictureState(r3, r4);	 Catch:{ Exception -> 0x0055 }
                if (r0 == 0) goto L_0x003b;
            L_0x000f:
                r3 = r7.state;	 Catch:{ Exception -> 0x0055 }
                r4 = com.android.server.am.ActivityStack.ActivityState.STOPPING;	 Catch:{ Exception -> 0x0055 }
                if (r3 == r4) goto L_0x003b;
            L_0x0015:
                r3 = r7.state;	 Catch:{ Exception -> 0x0055 }
                r4 = com.android.server.am.ActivityStack.ActivityState.STOPPED;	 Catch:{ Exception -> 0x0055 }
                if (r3 == r4) goto L_0x003b;
            L_0x001b:
                r3 = r7.state;	 Catch:{ Exception -> 0x0055 }
                r4 = com.android.server.am.ActivityStack.ActivityState.PAUSED;	 Catch:{ Exception -> 0x0055 }
                if (r3 == r4) goto L_0x0039;
            L_0x0021:
                r1 = 1;
            L_0x0022:
                r7.setDeferHidingClient(r1);	 Catch:{ Exception -> 0x0055 }
                r3 = 0;
                r7.setVisible(r3);	 Catch:{ Exception -> 0x0055 }
                r3 = -getcom-android-server-am-ActivityStack$ActivityStateSwitchesValues();	 Catch:{ Exception -> 0x0055 }
                r4 = r7.state;	 Catch:{ Exception -> 0x0055 }
                r4 = r4.ordinal();	 Catch:{ Exception -> 0x0055 }
                r3 = r3[r4];	 Catch:{ Exception -> 0x0055 }
                switch(r3) {
                    case 1: goto L_0x0076;
                    case 2: goto L_0x0076;
                    case 3: goto L_0x0076;
                    case 4: goto L_0x0076;
                    case 5: goto L_0x003d;
                    case 6: goto L_0x003d;
                    default: goto L_0x0038;
                };	 Catch:{ Exception -> 0x0055 }
            L_0x0038:
                return;
            L_0x0039:
                r1 = 0;
                goto L_0x0022;
            L_0x003b:
                r1 = 0;
                goto L_0x0022;
            L_0x003d:
                r3 = r7.app;	 Catch:{ Exception -> 0x0055 }
                if (r3 == 0) goto L_0x0051;
            L_0x0041:
                r3 = r7.app;	 Catch:{ Exception -> 0x0055 }
                r3 = r3.thread;	 Catch:{ Exception -> 0x0055 }
                if (r3 == 0) goto L_0x0051;
            L_0x0047:
                r3 = r7.app;	 Catch:{ Exception -> 0x0055 }
                r3 = r3.thread;	 Catch:{ Exception -> 0x0055 }
                r4 = r7.appToken;	 Catch:{ Exception -> 0x0055 }
                r5 = 0;
                r3.scheduleWindowVisibility(r4, r5);	 Catch:{ Exception -> 0x0055 }
            L_0x0051:
                r3 = 0;
                r7.supportsEnterPipOnTaskSwitch = r3;	 Catch:{ Exception -> 0x0055 }
                goto L_0x0038;
            L_0x0055:
                r2 = move-exception;
                r3 = TAG;
                r4 = new java.lang.StringBuilder;
                r4.<init>();
                r5 = "Exception thrown making hidden: ";
                r4 = r4.append(r5);
                r5 = r7.intent;
                r5 = r5.getComponent();
                r4 = r4.append(r5);
                r4 = r4.toString();
                android.util.Slog.w(r3, r4, r2);
                goto L_0x0038;
            L_0x0076:
                r3 = 1;
                r6.addToStopping(r7, r3, r0);	 Catch:{ Exception -> 0x0055 }
                goto L_0x0038;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.ActivityStack.makeInvisible(com.android.server.am.ActivityRecord):void");
            }

            private boolean updateBehindFullscreen(boolean stackInvisible, boolean behindFullscreenActivity, TaskRecord task, ActivityRecord r) {
                if (r.fullscreen) {
                    return true;
                }
                if (!isHomeOrRecentsStack() && r.frontOfTask && task.isOverHomeStack()) {
                    return true;
                }
                return behindFullscreenActivity;
            }

            void convertActivityToTranslucent(ActivityRecord r) {
                this.mTranslucentActivityWaiting = r;
                this.mUndrawnActivitiesBelowTopTranslucent.clear();
                this.mHandler.sendEmptyMessageDelayed(106, TRANSLUCENT_CONVERSION_TIMEOUT);
            }

            void clearOtherAppTimeTrackers(AppTimeTracker except) {
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        if (r.appTimeTracker != except) {
                            r.appTimeTracker = null;
                        }
                    }
                }
            }

            void notifyActivityDrawnLocked(ActivityRecord r) {
                boolean z = false;
                if (r == null || (this.mUndrawnActivitiesBelowTopTranslucent.remove(r) && this.mUndrawnActivitiesBelowTopTranslucent.isEmpty())) {
                    ActivityRecord waitingActivity = this.mTranslucentActivityWaiting;
                    this.mTranslucentActivityWaiting = null;
                    this.mUndrawnActivitiesBelowTopTranslucent.clear();
                    this.mHandler.removeMessages(106);
                    if (waitingActivity != null) {
                        this.mWindowManager.setWindowOpaque(waitingActivity.appToken, false);
                        if (waitingActivity.app != null && waitingActivity.app.thread != null) {
                            try {
                                IApplicationThread iApplicationThread = waitingActivity.app.thread;
                                IBinder iBinder = waitingActivity.appToken;
                                if (r != null) {
                                    z = true;
                                }
                                iApplicationThread.scheduleTranslucentConversionComplete(iBinder, z);
                            } catch (RemoteException e) {
                            }
                        }
                    }
                }
            }

            void cancelInitializingActivities() {
                ActivityRecord topActivity = topRunningActivityLocked();
                boolean aboveTop = true;
                boolean behindFullscreenActivity = false;
                if (shouldBeVisible(null) == 0) {
                    aboveTop = false;
                    behindFullscreenActivity = true;
                }
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        if (aboveTop) {
                            if (r == topActivity) {
                                aboveTop = false;
                            }
                            behindFullscreenActivity |= r.fullscreen;
                        } else {
                            r.removeOrphanedStartingWindow(behindFullscreenActivity);
                            behindFullscreenActivity |= r.fullscreen;
                        }
                    }
                }
            }

            boolean resumeTopActivityUncheckedLocked(ActivityRecord prev, ActivityOptions options) {
                if (this.mStackSupervisor.inResumeTopActivity) {
                    return false;
                }
                boolean z = false;
                try {
                    this.mStackSupervisor.inResumeTopActivity = true;
                    z = resumeTopActivityInnerLocked(prev, options);
                    ActivityRecord next = topRunningActivityLocked(true);
                    if (next == null || (next.canTurnScreenOn() ^ 1) != 0) {
                        checkReadyForSleep();
                    }
                    return z;
                } finally {
                    this.mStackSupervisor.inResumeTopActivity = false;
                }
            }

            void setResumedActivityLocked(ActivityRecord r, String reason) {
                this.mResumedActivity = r;
                r.state = ActivityState.RESUMED;
                this.mService.setResumedActivityUncheckLocked(r, reason);
                TaskRecord task = r.getTask();
                task.touchActiveTime();
                this.mRecentTasks.addLocked(task);
            }

            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            private boolean resumeTopActivityInnerLocked(com.android.server.am.ActivityRecord r36, android.app.ActivityOptions r37) {
                /*
                r35 = this;
                r0 = r35;
                r3 = r0.mService;
                r3 = r3.mBooting;
                if (r3 != 0) goto L_0x0014;
            L_0x0008:
                r0 = r35;
                r3 = r0.mService;
                r3 = r3.mBooted;
                r3 = r3 ^ 1;
                if (r3 == 0) goto L_0x0014;
            L_0x0012:
                r3 = 0;
                return r3;
            L_0x0014:
                r3 = 1;
                r0 = r35;
                r24 = r0.topRunningActivityLocked(r3);
                if (r24 == 0) goto L_0x0029;
            L_0x001d:
                r16 = 1;
            L_0x001f:
                if (r16 == 0) goto L_0x002c;
            L_0x0021:
                r3 = r35.getDisplay();
                if (r3 != 0) goto L_0x002c;
            L_0x0027:
                r3 = 0;
                return r3;
            L_0x0029:
                r16 = 0;
                goto L_0x001f;
            L_0x002c:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3.cancelInitializingActivities();
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r0 = r3.mUserLeaving;
                r34 = r0;
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r4 = 0;
                r3.mUserLeaving = r4;
                if (r16 != 0) goto L_0x0052;
            L_0x0044:
                r3 = "noMoreActivities";
                r0 = r35;
                r1 = r36;
                r2 = r37;
                r3 = r0.resumeTopActivityInNextFocusableStack(r1, r2, r3);
                return r3;
            L_0x0052:
                r3 = 0;
                r0 = r24;
                r0.delayedResume = r3;
                r0 = r35;
                r3 = r0.mResumedActivity;
                r0 = r24;
                if (r3 != r0) goto L_0x007a;
            L_0x005f:
                r0 = r24;
                r3 = r0.state;
                r4 = com.android.server.am.ActivityStack.ActivityState.RESUMED;
                if (r3 != r4) goto L_0x007a;
            L_0x0067:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.allResumedActivitiesComplete();
                if (r3 == 0) goto L_0x007a;
            L_0x0071:
                r0 = r35;
                r1 = r37;
                r0.executeAppTransition(r1);
                r3 = 0;
                return r3;
            L_0x007a:
                r26 = r24.getTask();
                if (r36 == 0) goto L_0x00ca;
            L_0x0080:
                r30 = r36.getTask();
            L_0x0084:
                if (r30 == 0) goto L_0x00a9;
            L_0x0086:
                r3 = r30.getStack();
                r0 = r35;
                if (r3 != r0) goto L_0x00a9;
            L_0x008e:
                r3 = r30.isOverHomeStack();
                if (r3 == 0) goto L_0x00a9;
            L_0x0094:
                r0 = r36;
                r3 = r0.finishing;
                if (r3 == 0) goto L_0x00a9;
            L_0x009a:
                r0 = r36;
                r3 = r0.frontOfTask;
                if (r3 == 0) goto L_0x00a9;
            L_0x00a0:
                r0 = r30;
                r1 = r26;
                if (r0 != r1) goto L_0x00cd;
            L_0x00a6:
                r30.setFrontOfTask();
            L_0x00a9:
                r3 = r35.shouldSleepOrShutDownActivities();
                if (r3 == 0) goto L_0x0116;
            L_0x00af:
                r0 = r35;
                r3 = r0.mLastPausedActivity;
                r0 = r24;
                if (r3 != r0) goto L_0x0116;
            L_0x00b7:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.allPausedActivitiesComplete();
                if (r3 == 0) goto L_0x0116;
            L_0x00c1:
                r0 = r35;
                r1 = r37;
                r0.executeAppTransition(r1);
                r3 = 0;
                return r3;
            L_0x00ca:
                r30 = 0;
                goto L_0x0084;
            L_0x00cd:
                r3 = r35.topTask();
                r0 = r30;
                if (r0 == r3) goto L_0x00f2;
            L_0x00d5:
                r0 = r35;
                r3 = r0.mTaskHistory;
                r0 = r30;
                r3 = r3.indexOf(r0);
                r33 = r3 + 1;
                r0 = r35;
                r3 = r0.mTaskHistory;
                r0 = r33;
                r3 = r3.get(r0);
                r3 = (com.android.server.am.TaskRecord) r3;
                r4 = 1;
                r3.setTaskToReturnTo(r4);
                goto L_0x00a9;
            L_0x00f2:
                r3 = r35.isOnHomeDisplay();
                if (r3 != 0) goto L_0x00fa;
            L_0x00f8:
                r3 = 0;
                return r3;
            L_0x00fa:
                r3 = r35.isHomeStack();
                if (r3 != 0) goto L_0x00a9;
            L_0x0100:
                r3 = r35.isOnHomeDisplay();
                if (r3 == 0) goto L_0x0114;
            L_0x0106:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r4 = "prevFinished";
                r0 = r36;
                r3 = r3.resumeHomeStackTask(r0, r4);
            L_0x0113:
                return r3;
            L_0x0114:
                r3 = 0;
                goto L_0x0113;
            L_0x0116:
                r0 = r35;
                r3 = r0.mService;
                r3 = r3.mUserController;
                r0 = r24;
                r4 = r0.userId;
                r3 = r3.hasStartedUserState(r4);
                if (r3 != 0) goto L_0x0159;
            L_0x0126:
                r3 = TAG;
                r4 = new java.lang.StringBuilder;
                r4.<init>();
                r5 = "Skipping resume of top activity ";
                r4 = r4.append(r5);
                r0 = r24;
                r4 = r4.append(r0);
                r5 = ": user ";
                r4 = r4.append(r5);
                r0 = r24;
                r5 = r0.userId;
                r4 = r4.append(r5);
                r5 = " is stopped";
                r4 = r4.append(r5);
                r4 = r4.toString();
                android.util.Slog.w(r3, r4);
                r3 = 0;
                return r3;
            L_0x0159:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.mStoppingActivities;
                r0 = r24;
                r3.remove(r0);
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.mGoingToSleepActivities;
                r0 = r24;
                r3.remove(r0);
                r3 = 0;
                r0 = r24;
                r0.sleeping = r3;
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.mActivitiesWaitingForVisibleActivity;
                r0 = r24;
                r3.remove(r0);
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.allPausedActivitiesComplete();
                if (r3 != 0) goto L_0x018b;
            L_0x0189:
                r3 = 0;
                return r3;
            L_0x018b:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r0 = r24;
                r4 = r0.info;
                r4 = r4.applicationInfo;
                r4 = r4.uid;
                r3.setLaunchSource(r4);
                r21 = 0;
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r18 = r3.getLastStack();
                if (r18 == 0) goto L_0x01bf;
            L_0x01a6:
                r0 = r18;
                r1 = r35;
                if (r0 == r1) goto L_0x01bf;
            L_0x01ac:
                r0 = r18;
                r0 = r0.mResumedActivity;
                r19 = r0;
                if (r19 == 0) goto L_0x020f;
            L_0x01b4:
                r3 = "resumeTopActivity";
                r0 = r19;
                r1 = r34;
                r21 = r0.checkEnterPictureInPictureState(r3, r1);
            L_0x01bf:
                r0 = r24;
                r3 = r0.info;
                r3 = r3.flags;
                r3 = r3 & 16384;
                if (r3 == 0) goto L_0x0212;
            L_0x01c9:
                r32 = r21 ^ 1;
            L_0x01cb:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r4 = 0;
                r0 = r34;
                r1 = r24;
                r29 = r3.pauseBackStacks(r0, r1, r4);
                r0 = r35;
                r3 = r0.mResumedActivity;
                if (r3 == 0) goto L_0x01ec;
            L_0x01de:
                r3 = 0;
                r4 = 0;
                r0 = r35;
                r1 = r34;
                r2 = r24;
                r3 = r0.startPausingLocked(r1, r3, r2, r4);
                r29 = r29 | r3;
            L_0x01ec:
                if (r29 == 0) goto L_0x0215;
            L_0x01ee:
                r3 = r32 ^ 1;
                if (r3 == 0) goto L_0x0215;
            L_0x01f2:
                r0 = r24;
                r3 = r0.app;
                if (r3 == 0) goto L_0x020d;
            L_0x01f8:
                r0 = r24;
                r3 = r0.app;
                r3 = r3.thread;
                if (r3 == 0) goto L_0x020d;
            L_0x0200:
                r0 = r35;
                r3 = r0.mService;
                r0 = r24;
                r4 = r0.app;
                r5 = 1;
                r6 = 0;
                r3.updateLruProcessLocked(r4, r5, r6);
            L_0x020d:
                r3 = 1;
                return r3;
            L_0x020f:
                r21 = 0;
                goto L_0x01bf;
            L_0x0212:
                r32 = 0;
                goto L_0x01cb;
            L_0x0215:
                r0 = r35;
                r3 = r0.mResumedActivity;
                r0 = r24;
                if (r3 != r0) goto L_0x0238;
            L_0x021d:
                r0 = r24;
                r3 = r0.state;
                r4 = com.android.server.am.ActivityStack.ActivityState.RESUMED;
                if (r3 != r4) goto L_0x0238;
            L_0x0225:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.allResumedActivitiesComplete();
                if (r3 == 0) goto L_0x0238;
            L_0x022f:
                r0 = r35;
                r1 = r37;
                r0.executeAppTransition(r1);
                r3 = 1;
                return r3;
            L_0x0238:
                r3 = r35.shouldSleepActivities();
                if (r3 == 0) goto L_0x0264;
            L_0x023e:
                r0 = r35;
                r3 = r0.mLastNoHistoryActivity;
                if (r3 == 0) goto L_0x0264;
            L_0x0244:
                r0 = r35;
                r3 = r0.mLastNoHistoryActivity;
                r3 = r3.finishing;
                r3 = r3 ^ 1;
                if (r3 == 0) goto L_0x0264;
            L_0x024e:
                r0 = r35;
                r3 = r0.mLastNoHistoryActivity;
                r4 = r3.appToken;
                r7 = "resume-no-history";
                r5 = 0;
                r6 = 0;
                r8 = 0;
                r3 = r35;
                r3.requestFinishActivityLocked(r4, r5, r6, r7, r8);
                r3 = 0;
                r0 = r35;
                r0.mLastNoHistoryActivity = r3;
            L_0x0264:
                if (r36 == 0) goto L_0x028f;
            L_0x0266:
                r0 = r36;
                r1 = r24;
                if (r0 == r1) goto L_0x028f;
            L_0x026c:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.mActivitiesWaitingForVisibleActivity;
                r0 = r36;
                r3 = r3.contains(r0);
                if (r3 != 0) goto L_0x03e6;
            L_0x027a:
                if (r24 == 0) goto L_0x03e6;
            L_0x027c:
                r0 = r24;
                r3 = r0.nowVisible;
                r3 = r3 ^ 1;
                if (r3 == 0) goto L_0x03e6;
            L_0x0284:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r3 = r3.mActivitiesWaitingForVisibleActivity;
                r0 = r36;
                r3.add(r0);
            L_0x028f:
                r3 = android.app.AppGlobals.getPackageManager();	 Catch:{ RemoteException -> 0x061d, IllegalArgumentException -> 0x03f4 }
                r0 = r24;
                r4 = r0.packageName;	 Catch:{ RemoteException -> 0x061d, IllegalArgumentException -> 0x03f4 }
                r0 = r24;
                r5 = r0.userId;	 Catch:{ RemoteException -> 0x061d, IllegalArgumentException -> 0x03f4 }
                r6 = 0;
                r3.setPackageStoppedState(r4, r6, r5);	 Catch:{ RemoteException -> 0x061d, IllegalArgumentException -> 0x03f4 }
            L_0x029f:
                r11 = 1;
                if (r36 == 0) goto L_0x0470;
            L_0x02a2:
                r0 = r36;
                r3 = r0.finishing;
                if (r3 == 0) goto L_0x0437;
            L_0x02a8:
                r0 = r35;
                r3 = r0.mNoAnimActivities;
                r0 = r36;
                r3 = r3.contains(r0);
                if (r3 == 0) goto L_0x041f;
            L_0x02b4:
                r11 = 0;
                r0 = r35;
                r3 = r0.mWindowManager;
                r4 = 0;
                r5 = 0;
                r3.prepareAppTransition(r4, r5);
            L_0x02be:
                r3 = 0;
                r0 = r36;
                r0.setVisibility(r3);
            L_0x02c4:
                r31 = 0;
                if (r11 == 0) goto L_0x0493;
            L_0x02c8:
                r28 = r24.getOptionsForTargetActivityLocked();
                if (r28 == 0) goto L_0x02d2;
            L_0x02ce:
                r31 = r28.toBundle();
            L_0x02d2:
                r24.applyOptionsLocked();
            L_0x02d5:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r22 = r3.getLastStack();
                r0 = r24;
                r3 = r0.app;
                if (r3 == 0) goto L_0x05fc;
            L_0x02e3:
                r0 = r24;
                r3 = r0.app;
                r3 = r3.thread;
                if (r3 == 0) goto L_0x05fc;
            L_0x02eb:
                if (r22 == 0) goto L_0x04a0;
            L_0x02ed:
                r0 = r22;
                r3 = r0.mFullscreen;
                if (r3 == 0) goto L_0x0498;
            L_0x02f3:
                r0 = r22;
                r3 = r0.mLastPausedActivity;
                if (r3 == 0) goto L_0x049c;
            L_0x02f9:
                r0 = r22;
                r3 = r0.mLastPausedActivity;
                r3 = r3.fullscreen;
                r17 = r3 ^ 1;
            L_0x0301:
                r0 = r35;
                r3 = r0.mWindowManager;
                r4 = r3.getWindowManagerLock();
                monitor-enter(r4);
                r0 = r24;
                r3 = r0.visible;	 Catch:{ all -> 0x05cc }
                if (r3 == 0) goto L_0x0318;
            L_0x0310:
                r0 = r24;
                r3 = r0.stopped;	 Catch:{ all -> 0x05cc }
                if (r3 != 0) goto L_0x0318;
            L_0x0316:
                if (r17 == 0) goto L_0x031e;
            L_0x0318:
                r3 = 1;
                r0 = r24;
                r0.setVisibility(r3);	 Catch:{ all -> 0x05cc }
            L_0x031e:
                r24.startLaunchTickingLocked();	 Catch:{ all -> 0x05cc }
                if (r22 != 0) goto L_0x04a4;
            L_0x0323:
                r20 = 0;
            L_0x0325:
                r0 = r24;
                r0 = r0.state;	 Catch:{ all -> 0x05cc }
                r23 = r0;
                r0 = r35;
                r3 = r0.mService;	 Catch:{ all -> 0x05cc }
                r3.updateCpuStats();	 Catch:{ all -> 0x05cc }
                r3 = "resumeTopActivityInnerLocked";
                r0 = r35;
                r1 = r24;
                r0.setResumedActivityLocked(r1, r3);	 Catch:{ all -> 0x05cc }
                r0 = r35;
                r3 = r0.mService;	 Catch:{ all -> 0x05cc }
                r0 = r24;
                r5 = r0.app;	 Catch:{ all -> 0x05cc }
                r6 = 1;
                r7 = 0;
                r3.updateLruProcessLocked(r5, r6, r7);	 Catch:{ all -> 0x05cc }
                r0 = r35;
                r1 = r24;
                r0.updateLRUListLocked(r1);	 Catch:{ all -> 0x05cc }
                r0 = r35;
                r3 = r0.mService;	 Catch:{ all -> 0x05cc }
                r3.updateOomAdjLocked();	 Catch:{ all -> 0x05cc }
                r27 = 1;
                r0 = r35;
                r3 = r0.mStackSupervisor;	 Catch:{ all -> 0x05cc }
                r0 = r35;
                r3 = r3.isFocusedStack(r0);	 Catch:{ all -> 0x05cc }
                if (r3 == 0) goto L_0x03bb;
            L_0x0365:
                r0 = r35;
                r3 = r0.mStackSupervisor;	 Catch:{ all -> 0x05cc }
                r3 = r3.mKeyguardController;	 Catch:{ all -> 0x05cc }
                r3 = r3.isKeyguardLocked();	 Catch:{ all -> 0x05cc }
                if (r3 == 0) goto L_0x037b;
            L_0x0371:
                r0 = r35;
                r3 = r0.mStackSupervisor;	 Catch:{ all -> 0x05cc }
                r5 = 0;
                r6 = 0;
                r7 = 0;
                r3.ensureActivitiesVisibleLocked(r5, r6, r7);	 Catch:{ all -> 0x05cc }
            L_0x037b:
                r0 = r35;
                r5 = r0.mWindowManager;	 Catch:{ all -> 0x05cc }
                r0 = r35;
                r3 = r0.mStackSupervisor;	 Catch:{ all -> 0x05cc }
                r0 = r35;
                r6 = r0.mDisplayId;	 Catch:{ all -> 0x05cc }
                r6 = r3.getDisplayOverrideConfiguration(r6);	 Catch:{ all -> 0x05cc }
                r0 = r24;
                r3 = r0.app;	 Catch:{ all -> 0x05cc }
                r0 = r24;
                r3 = r0.mayFreezeScreenLocked(r3);	 Catch:{ all -> 0x05cc }
                if (r3 == 0) goto L_0x04ac;
            L_0x0397:
                r0 = r24;
                r3 = r0.appToken;	 Catch:{ all -> 0x05cc }
            L_0x039b:
                r0 = r35;
                r7 = r0.mDisplayId;	 Catch:{ all -> 0x05cc }
                r12 = r5.updateOrientationFromAppTokens(r6, r3, r7);	 Catch:{ all -> 0x05cc }
                if (r12 == 0) goto L_0x03aa;
            L_0x03a5:
                r3 = 1;
                r0 = r24;
                r0.frozenBeforeDestroy = r3;	 Catch:{ all -> 0x05cc }
            L_0x03aa:
                r0 = r35;
                r3 = r0.mService;	 Catch:{ all -> 0x05cc }
                r0 = r35;
                r5 = r0.mDisplayId;	 Catch:{ all -> 0x05cc }
                r6 = 0;
                r0 = r24;
                r3 = r3.updateDisplayOverrideConfigurationLocked(r12, r0, r6, r5);	 Catch:{ all -> 0x05cc }
                r27 = r3 ^ 1;
            L_0x03bb:
                if (r27 == 0) goto L_0x04af;
            L_0x03bd:
                r25 = r35.topRunningActivityLocked();	 Catch:{ all -> 0x05cc }
                r0 = r25;
                r1 = r24;
                if (r0 == r1) goto L_0x03ce;
            L_0x03c7:
                r0 = r35;
                r3 = r0.mStackSupervisor;	 Catch:{ all -> 0x05cc }
                r3.scheduleResumeTopActivities();	 Catch:{ all -> 0x05cc }
            L_0x03ce:
                r0 = r24;
                r3 = r0.visible;	 Catch:{ all -> 0x05cc }
                if (r3 == 0) goto L_0x03da;
            L_0x03d4:
                r0 = r24;
                r3 = r0.stopped;	 Catch:{ all -> 0x05cc }
                if (r3 == 0) goto L_0x03e0;
            L_0x03da:
                r3 = 1;
                r0 = r24;
                r0.setVisibility(r3);	 Catch:{ all -> 0x05cc }
            L_0x03e0:
                r24.completeResumeLocked();	 Catch:{ all -> 0x05cc }
                r3 = 1;
                monitor-exit(r4);
                return r3;
            L_0x03e6:
                r0 = r36;
                r3 = r0.finishing;
                if (r3 == 0) goto L_0x028f;
            L_0x03ec:
                r3 = 0;
                r0 = r36;
                r0.setVisibility(r3);
                goto L_0x028f;
            L_0x03f4:
                r14 = move-exception;
                r3 = TAG;
                r4 = new java.lang.StringBuilder;
                r4.<init>();
                r5 = "Failed trying to unstop package ";
                r4 = r4.append(r5);
                r0 = r24;
                r5 = r0.packageName;
                r4 = r4.append(r5);
                r5 = ": ";
                r4 = r4.append(r5);
                r4 = r4.append(r14);
                r4 = r4.toString();
                android.util.Slog.w(r3, r4);
                goto L_0x029f;
            L_0x041f:
                r0 = r35;
                r4 = r0.mWindowManager;
                r3 = r36.getTask();
                r5 = r24.getTask();
                if (r3 != r5) goto L_0x0434;
            L_0x042d:
                r3 = 7;
            L_0x042e:
                r5 = 0;
                r4.prepareAppTransition(r3, r5);
                goto L_0x02be;
            L_0x0434:
                r3 = 9;
                goto L_0x042e;
            L_0x0437:
                r0 = r35;
                r3 = r0.mNoAnimActivities;
                r0 = r24;
                r3 = r3.contains(r0);
                if (r3 == 0) goto L_0x044f;
            L_0x0443:
                r11 = 0;
                r0 = r35;
                r3 = r0.mWindowManager;
                r4 = 0;
                r5 = 0;
                r3.prepareAppTransition(r4, r5);
                goto L_0x02c4;
            L_0x044f:
                r0 = r35;
                r4 = r0.mWindowManager;
                r3 = r36.getTask();
                r5 = r24.getTask();
                if (r3 != r5) goto L_0x0464;
            L_0x045d:
                r3 = 6;
            L_0x045e:
                r5 = 0;
                r4.prepareAppTransition(r3, r5);
                goto L_0x02c4;
            L_0x0464:
                r0 = r24;
                r3 = r0.mLaunchTaskBehind;
                if (r3 == 0) goto L_0x046d;
            L_0x046a:
                r3 = 16;
                goto L_0x045e;
            L_0x046d:
                r3 = 8;
                goto L_0x045e;
            L_0x0470:
                r0 = r35;
                r3 = r0.mNoAnimActivities;
                r0 = r24;
                r3 = r3.contains(r0);
                if (r3 == 0) goto L_0x0488;
            L_0x047c:
                r11 = 0;
                r0 = r35;
                r3 = r0.mWindowManager;
                r4 = 0;
                r5 = 0;
                r3.prepareAppTransition(r4, r5);
                goto L_0x02c4;
            L_0x0488:
                r0 = r35;
                r3 = r0.mWindowManager;
                r4 = 6;
                r5 = 0;
                r3.prepareAppTransition(r4, r5);
                goto L_0x02c4;
            L_0x0493:
                r24.clearOptionsLocked();
                goto L_0x02d5;
            L_0x0498:
                r17 = 1;
                goto L_0x0301;
            L_0x049c:
                r17 = 0;
                goto L_0x0301;
            L_0x04a0:
                r17 = 0;
                goto L_0x0301;
            L_0x04a4:
                r0 = r22;
                r0 = r0.mResumedActivity;	 Catch:{ all -> 0x05cc }
                r20 = r0;
                goto L_0x0325;
            L_0x04ac:
                r3 = 0;
                goto L_0x039b;
            L_0x04af:
                r0 = r24;
                r10 = r0.results;	 Catch:{ Exception -> 0x0572 }
                if (r10 == 0) goto L_0x04ce;
            L_0x04b5:
                r9 = r10.size();	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r3 = r0.finishing;	 Catch:{ Exception -> 0x0572 }
                if (r3 != 0) goto L_0x04ce;
            L_0x04bf:
                if (r9 <= 0) goto L_0x04ce;
            L_0x04c1:
                r0 = r24;
                r3 = r0.app;	 Catch:{ Exception -> 0x0572 }
                r3 = r3.thread;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r5 = r0.appToken;	 Catch:{ Exception -> 0x0572 }
                r3.scheduleSendResult(r5, r10);	 Catch:{ Exception -> 0x0572 }
            L_0x04ce:
                r0 = r24;
                r3 = r0.newIntents;	 Catch:{ Exception -> 0x0572 }
                if (r3 == 0) goto L_0x04e6;
            L_0x04d4:
                r0 = r24;
                r3 = r0.app;	 Catch:{ Exception -> 0x0572 }
                r3 = r3.thread;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r5 = r0.newIntents;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r6 = r0.appToken;	 Catch:{ Exception -> 0x0572 }
                r7 = 0;
                r3.scheduleNewIntent(r5, r6, r7);	 Catch:{ Exception -> 0x0572 }
            L_0x04e6:
                r0 = r24;
                r3 = r0.stopped;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r0.notifyAppResumed(r3);	 Catch:{ Exception -> 0x0572 }
                r3 = 4;
                r3 = new java.lang.Object[r3];	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r5 = r0.userId;	 Catch:{ Exception -> 0x0572 }
                r5 = java.lang.Integer.valueOf(r5);	 Catch:{ Exception -> 0x0572 }
                r6 = 0;
                r3[r6] = r5;	 Catch:{ Exception -> 0x0572 }
                r5 = java.lang.System.identityHashCode(r24);	 Catch:{ Exception -> 0x0572 }
                r5 = java.lang.Integer.valueOf(r5);	 Catch:{ Exception -> 0x0572 }
                r6 = 1;
                r3[r6] = r5;	 Catch:{ Exception -> 0x0572 }
                r5 = r24.getTask();	 Catch:{ Exception -> 0x0572 }
                r5 = r5.taskId;	 Catch:{ Exception -> 0x0572 }
                r5 = java.lang.Integer.valueOf(r5);	 Catch:{ Exception -> 0x0572 }
                r6 = 2;
                r3[r6] = r5;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r5 = r0.shortComponentName;	 Catch:{ Exception -> 0x0572 }
                r6 = 3;
                r3[r6] = r5;	 Catch:{ Exception -> 0x0572 }
                r5 = 30007; // 0x7537 float:4.2049E-41 double:1.48254E-319;
                android.util.EventLog.writeEvent(r5, r3);	 Catch:{ Exception -> 0x0572 }
                r3 = 0;
                r0 = r24;
                r0.sleeping = r3;	 Catch:{ Exception -> 0x0572 }
                r0 = r35;
                r3 = r0.mService;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r3.showUnsupportedZoomDialogIfNeededLocked(r0);	 Catch:{ Exception -> 0x0572 }
                r0 = r35;
                r3 = r0.mService;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r3.showAskCompatModeDialogLocked(r0);	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r3 = r0.app;	 Catch:{ Exception -> 0x0572 }
                r5 = 1;
                r3.pendingUiClean = r5;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r3 = r0.app;	 Catch:{ Exception -> 0x0572 }
                r0 = r35;
                r5 = r0.mService;	 Catch:{ Exception -> 0x0572 }
                r5 = r5.mTopProcessState;	 Catch:{ Exception -> 0x0572 }
                r3.forceProcessStateUpTo(r5);	 Catch:{ Exception -> 0x0572 }
                r24.clearOptionsLocked();	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r3 = r0.app;	 Catch:{ Exception -> 0x0572 }
                r3 = r3.thread;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r5 = r0.appToken;	 Catch:{ Exception -> 0x0572 }
                r0 = r24;
                r6 = r0.app;	 Catch:{ Exception -> 0x0572 }
                r6 = r6.repProcState;	 Catch:{ Exception -> 0x0572 }
                r0 = r35;
                r7 = r0.mService;	 Catch:{ Exception -> 0x0572 }
                r7 = r7.isNextTransitionForward();	 Catch:{ Exception -> 0x0572 }
                r0 = r31;
                r3.scheduleResumeActivity(r5, r6, r7, r0);	 Catch:{ Exception -> 0x0572 }
                monitor-exit(r4);
                r24.completeResumeLocked();	 Catch:{ Exception -> 0x05cf }
            L_0x0570:
                r3 = 1;
                return r3;
            L_0x0572:
                r13 = move-exception;
                r0 = r23;
                r1 = r24;
                r1.state = r0;	 Catch:{ all -> 0x05cc }
                if (r22 == 0) goto L_0x0581;
            L_0x057b:
                r0 = r20;
                r1 = r22;
                r1.mResumedActivity = r0;	 Catch:{ all -> 0x05cc }
            L_0x0581:
                r3 = TAG;	 Catch:{ all -> 0x05cc }
                r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x05cc }
                r5.<init>();	 Catch:{ all -> 0x05cc }
                r6 = "Restarting because process died: ";
                r5 = r5.append(r6);	 Catch:{ all -> 0x05cc }
                r0 = r24;
                r5 = r5.append(r0);	 Catch:{ all -> 0x05cc }
                r5 = r5.toString();	 Catch:{ all -> 0x05cc }
                android.util.Slog.i(r3, r5);	 Catch:{ all -> 0x05cc }
                r0 = r24;
                r3 = r0.hasBeenLaunched;	 Catch:{ all -> 0x05cc }
                if (r3 != 0) goto L_0x05b5;
            L_0x05a2:
                r3 = 1;
                r0 = r24;
                r0.hasBeenLaunched = r3;	 Catch:{ all -> 0x05cc }
            L_0x05a7:
                r0 = r35;
                r3 = r0.mStackSupervisor;	 Catch:{ all -> 0x05cc }
                r5 = 1;
                r6 = 0;
                r0 = r24;
                r3.startSpecificActivityLocked(r0, r5, r6);	 Catch:{ all -> 0x05cc }
                r3 = 1;
                monitor-exit(r4);
                return r3;
            L_0x05b5:
                if (r22 == 0) goto L_0x05a7;
            L_0x05b7:
                r0 = r35;
                r3 = r0.mStackSupervisor;	 Catch:{ all -> 0x05cc }
                r0 = r22;
                r3 = r3.isFrontStackOnDisplay(r0);	 Catch:{ all -> 0x05cc }
                if (r3 == 0) goto L_0x05a7;
            L_0x05c3:
                r3 = 0;
                r5 = 0;
                r6 = 0;
                r0 = r24;
                r0.showStartingWindow(r3, r5, r6);	 Catch:{ all -> 0x05cc }
                goto L_0x05a7;
            L_0x05cc:
                r3 = move-exception;
                monitor-exit(r4);
                throw r3;
            L_0x05cf:
                r13 = move-exception;
                r3 = TAG;
                r4 = new java.lang.StringBuilder;
                r4.<init>();
                r5 = "Exception thrown during resume of ";
                r4 = r4.append(r5);
                r0 = r24;
                r4 = r4.append(r0);
                r4 = r4.toString();
                android.util.Slog.w(r3, r4, r13);
                r0 = r24;
                r4 = r0.appToken;
                r7 = "resume-exception";
                r5 = 0;
                r6 = 0;
                r8 = 1;
                r3 = r35;
                r3.requestFinishActivityLocked(r4, r5, r6, r7, r8);
                r3 = 1;
                return r3;
            L_0x05fc:
                r0 = r24;
                r3 = r0.hasBeenLaunched;
                if (r3 != 0) goto L_0x0614;
            L_0x0602:
                r3 = 1;
                r0 = r24;
                r0.hasBeenLaunched = r3;
            L_0x0607:
                r0 = r35;
                r3 = r0.mStackSupervisor;
                r4 = 1;
                r5 = 1;
                r0 = r24;
                r3.startSpecificActivityLocked(r0, r4, r5);
                goto L_0x0570;
            L_0x0614:
                r3 = 0;
                r4 = 0;
                r5 = 0;
                r0 = r24;
                r0.showStartingWindow(r3, r4, r5);
                goto L_0x0607;
            L_0x061d:
                r15 = move-exception;
                goto L_0x029f;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.ActivityStack.resumeTopActivityInnerLocked(com.android.server.am.ActivityRecord, android.app.ActivityOptions):boolean");
            }

            private boolean resumeTopActivityInNextFocusableStack(ActivityRecord prev, ActivityOptions options, String reason) {
                if ((!this.mFullscreen || (isOnHomeDisplay() ^ 1) != 0) && adjustFocusToNextFocusableStackLocked(reason)) {
                    return this.mStackSupervisor.resumeFocusedStackTopActivityLocked(this.mStackSupervisor.getFocusedStack(), prev, null);
                }
                boolean resumeHomeStackTask;
                ActivityOptions.abort(options);
                if (isOnHomeDisplay()) {
                    resumeHomeStackTask = this.mStackSupervisor.resumeHomeStackTask(prev, reason);
                } else {
                    resumeHomeStackTask = false;
                }
                return resumeHomeStackTask;
            }

            private TaskRecord getNextTask(TaskRecord targetTask) {
                int index = this.mTaskHistory.indexOf(targetTask);
                if (index >= 0) {
                    int numTasks = this.mTaskHistory.size();
                    for (int i = index + 1; i < numTasks; i++) {
                        TaskRecord task = (TaskRecord) this.mTaskHistory.get(i);
                        if (task.userId == targetTask.userId) {
                            return task;
                        }
                    }
                }
                return null;
            }

            int getAdjustedPositionForTask(TaskRecord task, int suggestedPosition, ActivityRecord starting) {
                int maxPosition = this.mTaskHistory.size();
                if ((starting != null && starting.okToShowLocked()) || (starting == null && task.okToShowLocked())) {
                    return Math.min(suggestedPosition, maxPosition);
                }
                while (maxPosition > 0) {
                    TaskRecord tmpTask = (TaskRecord) this.mTaskHistory.get(maxPosition - 1);
                    if (!this.mStackSupervisor.isCurrentProfileLocked(tmpTask.userId) || tmpTask.topRunningActivityLocked() == null) {
                        break;
                    }
                    maxPosition--;
                }
                return Math.min(suggestedPosition, maxPosition);
            }

            private void insertTaskAtPosition(TaskRecord task, int position) {
                if (position >= this.mTaskHistory.size()) {
                    insertTaskAtTop(task, null);
                    return;
                }
                position = getAdjustedPositionForTask(task, position, null);
                this.mTaskHistory.remove(task);
                this.mTaskHistory.add(position, task);
                this.mWindowContainerController.positionChildAt(task.getWindowContainerController(), position, task.mBounds, task.getOverrideConfiguration());
                updateTaskMovement(task, true);
            }

            private void insertTaskAtTop(TaskRecord task, ActivityRecord starting) {
                updateTaskReturnToForTopInsertion(task);
                this.mTaskHistory.remove(task);
                this.mTaskHistory.add(getAdjustedPositionForTask(task, this.mTaskHistory.size(), starting), task);
                updateTaskMovement(task, true);
                this.mWindowContainerController.positionChildAtTop(task.getWindowContainerController(), true);
            }

            private void updateTaskReturnToForTopInsertion(TaskRecord task) {
                boolean isLastTaskOverHome = false;
                if (task.isOverHomeStack() || task.isOverAssistantStack()) {
                    TaskRecord nextTask = getNextTask(task);
                    if (nextTask != null) {
                        nextTask.setTaskToReturnTo(task.getTaskToReturnTo());
                    } else {
                        isLastTaskOverHome = true;
                    }
                }
                if (isOnHomeDisplay()) {
                    ActivityStack lastStack = this.mStackSupervisor.getLastStack();
                    if (lastStack != null) {
                        if (lastStack.isAssistantStack()) {
                            task.setTaskToReturnTo(3);
                            return;
                        }
                        boolean fromHomeOrRecents = lastStack.isHomeOrRecentsStack();
                        TaskRecord topTask = lastStack.topTask();
                        if (!isHomeOrRecentsStack() && (fromHomeOrRecents || topTask() != task)) {
                            int returnToType = isLastTaskOverHome ? task.getTaskToReturnTo() : 0;
                            if (fromHomeOrRecents && StackId.allowTopTaskToReturnHome(this.mStackId)) {
                                returnToType = topTask == null ? 1 : topTask.taskType;
                            }
                            task.setTaskToReturnTo(returnToType);
                        }
                        return;
                    }
                    return;
                }
                task.setTaskToReturnTo(0);
            }

            final void startActivityLocked(ActivityRecord r, ActivityRecord focusedTopActivity, boolean newTask, boolean keepCurTransition, ActivityOptions options) {
                TaskRecord rTask = r.getTask();
                int taskId = rTask.taskId;
                if (!r.mLaunchTaskBehind && (taskForIdLocked(taskId) == null || newTask)) {
                    insertTaskAtTop(rTask, r);
                }
                TaskRecord taskRecord = null;
                if (!newTask) {
                    boolean startIt = true;
                    for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                        taskRecord = (TaskRecord) this.mTaskHistory.get(taskNdx);
                        if (taskRecord.getTopActivity() != null) {
                            if (taskRecord == rTask) {
                                if (!startIt) {
                                    r.createWindowContainer();
                                    ActivityOptions.abort(options);
                                    return;
                                }
                            } else if (taskRecord.numFullscreen > 0) {
                                startIt = false;
                            }
                        }
                    }
                }
                TaskRecord activityTask = r.getTask();
                if (taskRecord == activityTask && this.mTaskHistory.indexOf(taskRecord) != this.mTaskHistory.size() - 1) {
                    this.mStackSupervisor.mUserLeaving = false;
                }
                taskRecord = activityTask;
                if (r.getWindowContainerController() == null) {
                    r.createWindowContainer();
                }
                activityTask.setFrontOfTask();
                if (!isHomeOrRecentsStack() || numActivities() > 0) {
                    if ((r.intent.getFlags() & 65536) != 0) {
                        this.mWindowManager.prepareAppTransition(0, keepCurTransition);
                        this.mNoAnimActivities.add(r);
                    } else {
                        int transit = 6;
                        if (newTask) {
                            if (r.mLaunchTaskBehind) {
                                transit = 16;
                            } else {
                                if (canEnterPipOnTaskSwitch(focusedTopActivity, null, r, options)) {
                                    focusedTopActivity.supportsEnterPipOnTaskSwitch = true;
                                }
                                transit = 8;
                            }
                        }
                        this.mWindowManager.prepareAppTransition(transit, keepCurTransition);
                        this.mNoAnimActivities.remove(r);
                    }
                    boolean doShow = true;
                    if (newTask) {
                        if ((r.intent.getFlags() & DumpState.DUMP_COMPILER_STATS) != 0) {
                            resetTaskIfNeededLocked(r, r);
                            doShow = topRunningNonDelayedActivityLocked(null) == r;
                        }
                    } else if (options != null && options.getAnimationType() == 5) {
                        doShow = false;
                    }
                    if (r.mLaunchTaskBehind) {
                        r.setVisibility(true);
                        ensureActivitiesVisibleLocked(null, 0, false);
                    } else if (doShow) {
                        TaskRecord prevTask = r.getTask();
                        ActivityRecord prev = prevTask.topRunningActivityWithStartingWindowLocked();
                        if (prev != null) {
                            if (prev.getTask() != prevTask) {
                                prev = null;
                            } else if (prev.nowVisible) {
                                prev = null;
                            }
                        }
                        r.showStartingWindow(prev, newTask, isTaskSwitch(r, focusedTopActivity));
                    }
                } else {
                    ActivityOptions.abort(options);
                }
            }

            private boolean canEnterPipOnTaskSwitch(ActivityRecord pipCandidate, TaskRecord toFrontTask, ActivityRecord toFrontActivity, ActivityOptions opts) {
                if ((opts != null && opts.disallowEnterPictureInPictureWhileLaunching()) || pipCandidate == null || pipCandidate.getStackId() == 4) {
                    return false;
                }
                int targetStackId;
                if (toFrontTask != null) {
                    targetStackId = toFrontTask.getStackId();
                } else {
                    targetStackId = toFrontActivity.getStackId();
                }
                if (targetStackId == 6) {
                    return false;
                }
                return true;
            }

            private boolean isTaskSwitch(ActivityRecord r, ActivityRecord topFocusedActivity) {
                return (topFocusedActivity == null || r.getTask() == topFocusedActivity.getTask()) ? false : true;
            }

            private ActivityOptions resetTargetTaskIfNeededLocked(TaskRecord task, boolean forceReset) {
                ActivityOptions topOptions = null;
                int replyChainEnd = -1;
                boolean canMoveOptions = true;
                ArrayList<ActivityRecord> activities = task.mActivities;
                int numActivities = activities.size();
                int rootActivityNdx = task.findEffectiveRootIndex();
                for (int i = numActivities - 1; i > rootActivityNdx; i--) {
                    ActivityRecord target = (ActivityRecord) activities.get(i);
                    if (target.frontOfTask) {
                        break;
                    }
                    int flags = target.info.flags;
                    boolean finishOnTaskLaunch = (flags & 2) != 0;
                    boolean allowTaskReparenting = (flags & 64) != 0;
                    boolean clearWhenTaskReset = (target.intent.getFlags() & DumpState.DUMP_FROZEN) != 0;
                    if (finishOnTaskLaunch || (clearWhenTaskReset ^ 1) == 0 || target.resultTo == null) {
                        boolean noOptions;
                        int srcPos;
                        ActivityRecord p;
                        if (!finishOnTaskLaunch && (clearWhenTaskReset ^ 1) != 0 && allowTaskReparenting && target.taskAffinity != null && (target.taskAffinity.equals(task.affinity) ^ 1) != 0) {
                            TaskRecord targetTask;
                            ActivityRecord activityRecord = (this.mTaskHistory.isEmpty() || (((TaskRecord) this.mTaskHistory.get(0)).mActivities.isEmpty() ^ 1) == 0) ? null : (ActivityRecord) ((TaskRecord) this.mTaskHistory.get(0)).mActivities.get(0);
                            if (activityRecord == null || target.taskAffinity == null || !target.taskAffinity.equals(activityRecord.getTask().affinity)) {
                                targetTask = createTaskRecord(this.mStackSupervisor.getNextTaskIdForUserLocked(target.userId), target.info, null, null, null, false, target.mActivityType);
                                targetTask.affinityIntent = target.intent;
                            } else {
                                targetTask = activityRecord.getTask();
                            }
                            noOptions = canMoveOptions;
                            for (srcPos = replyChainEnd < 0 ? i : replyChainEnd; srcPos >= i; srcPos--) {
                                p = (ActivityRecord) activities.get(srcPos);
                                if (!p.finishing) {
                                    canMoveOptions = false;
                                    if (noOptions && r27 == null) {
                                        topOptions = p.takeOptionsLocked();
                                        if (topOptions != null) {
                                            noOptions = false;
                                        }
                                    }
                                    p.reparent(targetTask, 0, "resetTargetTaskIfNeeded");
                                }
                            }
                            this.mWindowContainerController.positionChildAtBottom(targetTask.getWindowContainerController());
                            replyChainEnd = -1;
                        } else if (forceReset || finishOnTaskLaunch || clearWhenTaskReset) {
                            int end;
                            if (clearWhenTaskReset) {
                                end = activities.size() - 1;
                            } else if (replyChainEnd < 0) {
                                end = i;
                            } else {
                                end = replyChainEnd;
                            }
                            noOptions = canMoveOptions;
                            srcPos = i;
                            while (srcPos <= end) {
                                p = (ActivityRecord) activities.get(srcPos);
                                if (!p.finishing) {
                                    canMoveOptions = false;
                                    if (noOptions && r27 == null) {
                                        topOptions = p.takeOptionsLocked();
                                        if (topOptions != null) {
                                            noOptions = false;
                                        }
                                    }
                                    if (finishActivityLocked(p, 0, null, "reset-task", false)) {
                                        end--;
                                        srcPos--;
                                    }
                                }
                                srcPos++;
                            }
                            replyChainEnd = -1;
                        } else {
                            replyChainEnd = -1;
                        }
                    } else if (replyChainEnd < 0) {
                        replyChainEnd = i;
                    }
                }
                return topOptions;
            }

            private int resetAffinityTaskIfNeededLocked(TaskRecord affinityTask, TaskRecord task, boolean topTaskIsHigher, boolean forceReset, int taskInsertionPoint) {
                int replyChainEnd = -1;
                int taskId = task.taskId;
                String taskAffinity = task.affinity;
                ArrayList<ActivityRecord> activities = affinityTask.mActivities;
                int numActivities = activities.size();
                int rootActivityNdx = affinityTask.findEffectiveRootIndex();
                for (int i = numActivities - 1; i > rootActivityNdx; i--) {
                    ActivityRecord target = (ActivityRecord) activities.get(i);
                    if (target.frontOfTask) {
                        break;
                    }
                    int flags = target.info.flags;
                    boolean finishOnTaskLaunch = (flags & 2) != 0;
                    boolean allowTaskReparenting = (flags & 64) != 0;
                    if (target.resultTo != null) {
                        if (replyChainEnd < 0) {
                            replyChainEnd = i;
                        }
                    } else if (topTaskIsHigher && allowTaskReparenting && taskAffinity != null) {
                        if (taskAffinity.equals(target.taskAffinity)) {
                            int srcPos;
                            ActivityRecord p;
                            if (forceReset || finishOnTaskLaunch) {
                                for (srcPos = replyChainEnd >= 0 ? replyChainEnd : i; srcPos >= i; srcPos--) {
                                    p = (ActivityRecord) activities.get(srcPos);
                                    if (!p.finishing) {
                                        finishActivityLocked(p, 0, null, "move-affinity", false);
                                    }
                                }
                            } else {
                                if (taskInsertionPoint < 0) {
                                    taskInsertionPoint = task.mActivities.size();
                                }
                                for (srcPos = replyChainEnd >= 0 ? replyChainEnd : i; srcPos >= i; srcPos--) {
                                    ((ActivityRecord) activities.get(srcPos)).reparent(task, taskInsertionPoint, "resetAffinityTaskIfNeededLocked");
                                }
                                this.mWindowContainerController.positionChildAtTop(task.getWindowContainerController(), true);
                                if (target.info.launchMode == 1) {
                                    ArrayList<ActivityRecord> taskActivities = task.mActivities;
                                    int targetNdx = taskActivities.indexOf(target);
                                    if (targetNdx > 0) {
                                        p = (ActivityRecord) taskActivities.get(targetNdx - 1);
                                        if (p.intent.getComponent().equals(target.intent.getComponent())) {
                                            finishActivityLocked(p, 0, null, "replace", false);
                                        }
                                    }
                                }
                            }
                            replyChainEnd = -1;
                        }
                    }
                }
                return taskInsertionPoint;
            }

            final ActivityRecord resetTaskIfNeededLocked(ActivityRecord taskTop, ActivityRecord newActivity) {
                boolean forceReset = (newActivity.info.flags & 4) != 0;
                TaskRecord task = taskTop.getTask();
                boolean taskFound = false;
                ActivityOptions topOptions = null;
                int reparentInsertionPoint = -1;
                for (int i = this.mTaskHistory.size() - 1; i >= 0; i--) {
                    TaskRecord targetTask = (TaskRecord) this.mTaskHistory.get(i);
                    if (targetTask == task) {
                        topOptions = resetTargetTaskIfNeededLocked(task, forceReset);
                        taskFound = true;
                    } else {
                        reparentInsertionPoint = resetAffinityTaskIfNeededLocked(targetTask, task, taskFound, forceReset, reparentInsertionPoint);
                    }
                }
                int taskNdx = this.mTaskHistory.indexOf(task);
                if (taskNdx >= 0) {
                    while (true) {
                        int taskNdx2 = taskNdx - 1;
                        taskTop = ((TaskRecord) this.mTaskHistory.get(taskNdx)).getTopActivity();
                        if (taskTop != null || taskNdx2 < 0) {
                        } else {
                            taskNdx = taskNdx2;
                        }
                    }
                }
                if (topOptions != null) {
                    if (taskTop != null) {
                        taskTop.updateOptionsLocked(topOptions);
                    } else {
                        topOptions.abort();
                    }
                }
                return taskTop;
            }

            void sendActivityResultLocked(int callingUid, ActivityRecord r, String resultWho, int requestCode, int resultCode, Intent data) {
                if (callingUid > 0) {
                    this.mService.grantUriPermissionFromIntentLocked(callingUid, r.packageName, data, r.getUriPermissionsLocked(), r.userId);
                }
                if (!(this.mResumedActivity != r || r.app == null || r.app.thread == null)) {
                    try {
                        ArrayList<ResultInfo> list = new ArrayList();
                        list.add(new ResultInfo(resultWho, requestCode, resultCode, data));
                        r.app.thread.scheduleSendResult(r.appToken, list);
                        return;
                    } catch (Exception e) {
                        Slog.w(TAG, "Exception thrown sending result to " + r, e);
                    }
                }
                r.addResultLocked(null, resultWho, requestCode, resultCode, data);
            }

            boolean isATopFinishingTask(TaskRecord task) {
                for (int i = this.mTaskHistory.size() - 1; i >= 0; i--) {
                    TaskRecord current = (TaskRecord) this.mTaskHistory.get(i);
                    if (current.topRunningActivityLocked() != null) {
                        return false;
                    }
                    if (current == task) {
                        return true;
                    }
                }
                return false;
            }

            private void adjustFocusedActivityStackLocked(ActivityRecord r, String reason) {
                if (this.mStackSupervisor.isFocusedStack(this) && (this.mResumedActivity == r || this.mResumedActivity == null)) {
                    ActivityRecord next = topRunningActivityLocked();
                    String myReason = reason + " adjustFocus";
                    if (next != r) {
                        if (next == null || !StackId.keepFocusInStackIfPossible(this.mStackId) || !isFocusable()) {
                            TaskRecord task = r.getTask();
                            if (task == null) {
                                throw new IllegalStateException("activity no longer associated with task:" + r);
                            }
                            boolean isAssistantOrOverAssistant;
                            if (task.getStack().isAssistantStack()) {
                                isAssistantOrOverAssistant = true;
                            } else {
                                isAssistantOrOverAssistant = task.isOverAssistantStack();
                            }
                            if (r.frontOfTask && isATopFinishingTask(task) && (task.isOverHomeStack() || r0)) {
                                if ((this.mFullscreen && !r0) || !adjustFocusToNextFocusableStackLocked(myReason)) {
                                    if (task.isOverHomeStack() && this.mStackSupervisor.moveHomeStackTaskToTop(myReason)) {
                                        return;
                                    }
                                }
                                return;
                            }
                        }
                        return;
                    }
                    this.mStackSupervisor.moveFocusableActivityStackToFrontLocked(this.mStackSupervisor.topRunningActivityLocked(), myReason);
                }
            }

            private boolean adjustFocusToNextFocusableStackLocked(String reason) {
                return adjustFocusToNextFocusableStackLocked(reason, false);
            }

            private boolean adjustFocusToNextFocusableStackLocked(String reason, boolean allowFocusSelf) {
                ActivityStack activityStack = null;
                if (isAssistantStack() && bottomTask() != null && bottomTask().getTaskToReturnTo() == 1) {
                    return this.mStackSupervisor.moveHomeStackTaskToTop(reason);
                }
                ActivityStackSupervisor activityStackSupervisor = this.mStackSupervisor;
                if (!allowFocusSelf) {
                    activityStack = this;
                }
                ActivityStack stack = activityStackSupervisor.getNextFocusableStackLocked(activityStack);
                String myReason = reason + " adjustFocusToNextFocusableStack";
                if (stack == null) {
                    return false;
                }
                ActivityRecord top = stack.topRunningActivityLocked();
                if (stack.isHomeOrRecentsStack() && (top == null || (top.visible ^ 1) != 0)) {
                    return this.mStackSupervisor.moveHomeStackTaskToTop(reason);
                }
                if (stack.isAssistantStack() && top != null && top.getTask().getTaskToReturnTo() == 1) {
                    this.mStackSupervisor.moveHomeStackTaskToTop("adjustAssistantReturnToHome");
                }
                stack.moveToFront(myReason);
                return true;
            }

            final void stopActivityLocked(ActivityRecord r) {
                if (((r.intent.getFlags() & 1073741824) == 0 && (r.info.flags & 128) == 0) || r.finishing || shouldSleepActivities() || !requestFinishActivityLocked(r.appToken, 0, null, "stop-no-history", false)) {
                    if (!(r.app == null || r.app.thread == null)) {
                        adjustFocusedActivityStackLocked(r, "stopActivity");
                        r.resumeKeyDispatchingLocked();
                        try {
                            r.stopped = false;
                            r.state = ActivityState.STOPPING;
                            if (!r.visible) {
                                r.setVisible(false);
                            }
                            EventLogTags.writeAmStopActivity(r.userId, System.identityHashCode(r), r.shortComponentName);
                            r.app.thread.scheduleStopActivity(r.appToken, r.visible, r.configChangeFlags);
                            if (shouldSleepOrShutDownActivities()) {
                                r.setSleeping(true);
                            }
                            this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(104, r), JobStatus.DEFAULT_TRIGGER_UPDATE_DELAY);
                        } catch (Exception e) {
                            Slog.w(TAG, "Exception thrown during pause", e);
                            r.stopped = true;
                            r.state = ActivityState.STOPPED;
                            if (r.deferRelaunchUntilPaused) {
                                destroyActivityLocked(r, true, "stop-except");
                            }
                        }
                    }
                    return;
                }
                r.resumeKeyDispatchingLocked();
            }

            final boolean requestFinishActivityLocked(IBinder token, int resultCode, Intent resultData, String reason, boolean oomAdj) {
                ActivityRecord r = isInStackLocked(token);
                if (r == null) {
                    return false;
                }
                finishActivityLocked(r, resultCode, resultData, reason, oomAdj);
                return true;
            }

            final void finishSubActivityLocked(ActivityRecord self, String resultWho, int requestCode) {
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        if (r.resultTo == self && r.requestCode == requestCode) {
                            if (!(r.resultWho == null && resultWho == null)) {
                                if (r.resultWho != null && r.resultWho.equals(resultWho)) {
                                }
                            }
                            finishActivityLocked(r, 0, null, "request-sub", false);
                        }
                    }
                }
                this.mService.updateOomAdjLocked();
            }

            final TaskRecord finishTopRunningActivityLocked(ProcessRecord app, String reason) {
                ActivityRecord r = topRunningActivityLocked();
                if (r == null || r.app != app) {
                    return null;
                }
                Slog.w(TAG, "  Force finishing activity " + r.intent.getComponent().flattenToShortString());
                TaskRecord finishedTask = r.getTask();
                int taskNdx = this.mTaskHistory.indexOf(finishedTask);
                TaskRecord task = finishedTask;
                int activityNdx = finishedTask.mActivities.indexOf(r);
                finishActivityLocked(r, 0, null, reason, false);
                activityNdx--;
                if (activityNdx < 0) {
                    while (true) {
                        taskNdx--;
                        if (taskNdx >= 0) {
                            activityNdx = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities.size() - 1;
                            if (activityNdx >= 0) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (activityNdx >= 0) {
                    r = (ActivityRecord) ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities.get(activityNdx);
                    if (!(r.state == ActivityState.RESUMED || r.state == ActivityState.PAUSING)) {
                        if (r.state == ActivityState.PAUSED) {
                        }
                    }
                    if (!(r.isHomeActivity() && this.mService.mHomeProcess == r.app)) {
                        Slog.w(TAG, "  Force finishing activity " + r.intent.getComponent().flattenToShortString());
                        finishActivityLocked(r, 0, null, reason, false);
                    }
                }
                return finishedTask;
            }

            final void finishVoiceTask(IVoiceInteractionSession session) {
                IBinder sessionBinder = session.asBinder();
                boolean didOne = false;
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    TaskRecord tr = (TaskRecord) this.mTaskHistory.get(taskNdx);
                    int activityNdx;
                    ActivityRecord r;
                    if (tr.voiceSession == null || tr.voiceSession.asBinder() != sessionBinder) {
                        for (activityNdx = tr.mActivities.size() - 1; activityNdx >= 0; activityNdx--) {
                            r = (ActivityRecord) tr.mActivities.get(activityNdx);
                            if (r.voiceSession != null && r.voiceSession.asBinder() == sessionBinder) {
                                r.clearVoiceSessionLocked();
                                try {
                                    r.app.thread.scheduleLocalVoiceInteractionStarted(r.appToken, null);
                                } catch (RemoteException e) {
                                }
                                this.mService.finishRunningVoiceLocked();
                                break;
                            }
                        }
                    } else {
                        for (activityNdx = tr.mActivities.size() - 1; activityNdx >= 0; activityNdx--) {
                            r = (ActivityRecord) tr.mActivities.get(activityNdx);
                            if (!r.finishing) {
                                finishActivityLocked(r, 0, null, "finish-voice", false);
                                didOne = true;
                            }
                        }
                    }
                }
                if (didOne) {
                    this.mService.updateOomAdjLocked();
                }
            }

            final boolean finishActivityAffinityLocked(ActivityRecord r) {
                ArrayList<ActivityRecord> activities = r.getTask().mActivities;
                for (int index = activities.indexOf(r); index >= 0; index--) {
                    ActivityRecord cur = (ActivityRecord) activities.get(index);
                    if (!Objects.equals(cur.taskAffinity, r.taskAffinity)) {
                        break;
                    }
                    finishActivityLocked(cur, 0, null, "request-affinity", true);
                }
                return true;
            }

            private void finishActivityResultsLocked(ActivityRecord r, int resultCode, Intent resultData) {
                ActivityRecord resultTo = r.resultTo;
                if (resultTo != null) {
                    if (!(resultTo.userId == r.userId || resultData == null)) {
                        resultData.prepareToLeaveUser(r.userId);
                    }
                    if (r.info.applicationInfo.uid > 0) {
                        this.mService.grantUriPermissionFromIntentLocked(r.info.applicationInfo.uid, resultTo.packageName, resultData, resultTo.getUriPermissionsLocked(), resultTo.userId);
                    }
                    resultTo.addResultLocked(r, r.resultWho, r.requestCode, resultCode, resultData);
                    r.resultTo = null;
                }
                r.results = null;
                r.pendingResults = null;
                r.newIntents = null;
                r.icicle = null;
            }

            final boolean finishActivityLocked(ActivityRecord r, int resultCode, Intent resultData, String reason, boolean oomAdj) {
                return finishActivityLocked(r, resultCode, resultData, reason, oomAdj, false);
            }

            final boolean finishActivityLocked(ActivityRecord r, int resultCode, Intent resultData, String reason, boolean oomAdj, boolean pauseImmediately) {
                if (r.finishing) {
                    Slog.w(TAG, "Duplicate finish request for " + r);
                    return false;
                }
                this.mWindowManager.deferSurfaceLayout();
                try {
                    r.makeFinishingLocked();
                    TaskRecord task = r.getTask();
                    EventLog.writeEvent(EventLogTags.AM_FINISH_ACTIVITY, new Object[]{Integer.valueOf(r.userId), Integer.valueOf(System.identityHashCode(r)), Integer.valueOf(task.taskId), r.shortComponentName, reason});
                    ArrayList<ActivityRecord> activities = task.mActivities;
                    int index = activities.indexOf(r);
                    if (index < activities.size() - 1) {
                        task.setFrontOfTask();
                        if ((r.intent.getFlags() & DumpState.DUMP_FROZEN) != 0) {
                            ((ActivityRecord) activities.get(index + 1)).intent.addFlags(DumpState.DUMP_FROZEN);
                        }
                    }
                    r.pauseKeyDispatchingLocked();
                    adjustFocusedActivityStackLocked(r, "finishActivity");
                    finishActivityResultsLocked(r, resultCode, resultData);
                    int isClearingToReuseTask = index <= 0 ? task.isClearingToReuseTask() ^ 1 : 0;
                    int transit = isClearingToReuseTask != 0 ? 9 : 7;
                    if (this.mResumedActivity == r) {
                        if (isClearingToReuseTask != 0) {
                            this.mService.mTaskChangeNotificationController.notifyTaskRemovalStarted(task.taskId);
                        }
                        this.mWindowManager.prepareAppTransition(transit, false);
                        r.setVisibility(false);
                        if (this.mPausingActivity == null) {
                            startPausingLocked(false, false, null, pauseImmediately);
                        }
                        if (isClearingToReuseTask != 0) {
                            this.mStackSupervisor.removeLockedTaskLocked(task);
                        }
                    } else if (r.state != ActivityState.PAUSING) {
                        int finishMode;
                        if (r.visible) {
                            prepareActivityHideTransitionAnimation(r, transit);
                        }
                        if (r.visible || r.nowVisible) {
                            finishMode = 2;
                        } else {
                            finishMode = 1;
                        }
                        boolean removedActivity = finishCurrentActivityLocked(r, finishMode, oomAdj) == null;
                        if (task.onlyHasTaskOverlayActivities(true)) {
                            for (ActivityRecord taskOverlay : task.mActivities) {
                                if (taskOverlay.mTaskOverlay) {
                                    prepareActivityHideTransitionAnimation(taskOverlay, transit);
                                }
                            }
                        }
                        this.mWindowManager.continueSurfaceLayout();
                        return removedActivity;
                    }
                    this.mWindowManager.continueSurfaceLayout();
                    return false;
                } catch (Throwable th) {
                    this.mWindowManager.continueSurfaceLayout();
                }
            }

            private void prepareActivityHideTransitionAnimation(ActivityRecord r, int transit) {
                this.mWindowManager.prepareAppTransition(transit, false);
                r.setVisibility(false);
                this.mWindowManager.executeAppTransition();
                if (!this.mStackSupervisor.mActivitiesWaitingForVisibleActivity.contains(r)) {
                    this.mStackSupervisor.mActivitiesWaitingForVisibleActivity.add(r);
                }
            }

            final ActivityRecord finishCurrentActivityLocked(ActivityRecord r, int mode, boolean oomAdj) {
                ActivityRecord next = this.mStackSupervisor.topRunningActivityLocked();
                if (mode != 2 || (!(r.visible || r.nowVisible) || next == null || (next.nowVisible ^ 1) == 0)) {
                    this.mStackSupervisor.mStoppingActivities.remove(r);
                    this.mStackSupervisor.mGoingToSleepActivities.remove(r);
                    this.mStackSupervisor.mActivitiesWaitingForVisibleActivity.remove(r);
                    if (this.mResumedActivity == r) {
                        this.mResumedActivity = null;
                    }
                    ActivityState prevState = r.state;
                    r.state = ActivityState.FINISHING;
                    boolean finishingActivityInNonFocusedStack = (r.getStack() == this.mStackSupervisor.getFocusedStack() || prevState != ActivityState.PAUSED) ? false : mode == 2;
                    if (mode == 0 || ((prevState == ActivityState.PAUSED && (mode == 1 || this.mStackId == 4)) || finishingActivityInNonFocusedStack || prevState == ActivityState.STOPPING || prevState == ActivityState.STOPPED || prevState == ActivityState.INITIALIZING)) {
                        r.makeFinishingLocked();
                        boolean activityRemoved = destroyActivityLocked(r, true, "finish-imm");
                        if (finishingActivityInNonFocusedStack) {
                            this.mStackSupervisor.ensureActivitiesVisibleLocked(null, 0, false);
                        }
                        if (activityRemoved) {
                            this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                        }
                        if (activityRemoved) {
                            r = null;
                        }
                        return r;
                    }
                    this.mStackSupervisor.mFinishingActivities.add(r);
                    r.resumeKeyDispatchingLocked();
                    this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                    return r;
                }
                if (!this.mStackSupervisor.mStoppingActivities.contains(r)) {
                    addToStopping(r, false, false);
                }
                r.state = ActivityState.STOPPING;
                if (oomAdj) {
                    this.mService.updateOomAdjLocked();
                }
                return r;
            }

            void finishAllActivitiesLocked(boolean immediately) {
                boolean noActivitiesInStack = true;
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        noActivitiesInStack = false;
                        if (!r.finishing || (immediately ^ 1) == 0) {
                            Slog.d(TAG, "finishAllActivitiesLocked: finishing " + r + " immediately");
                            finishCurrentActivityLocked(r, 0, false);
                        }
                    }
                }
                if (noActivitiesInStack) {
                    remove();
                }
            }

            final boolean shouldUpRecreateTaskLocked(ActivityRecord srec, String destAffinity) {
                if (srec == null || srec.getTask().affinity == null || (srec.getTask().affinity.equals(destAffinity) ^ 1) != 0) {
                    return true;
                }
                TaskRecord task = srec.getTask();
                if (srec.frontOfTask && task != null && task.getBaseIntent() != null && task.getBaseIntent().isDocument()) {
                    if (task.getTaskToReturnTo() != 0) {
                        return true;
                    }
                    int taskIdx = this.mTaskHistory.indexOf(task);
                    if (taskIdx <= 0) {
                        Slog.w(TAG, "shouldUpRecreateTask: task not in history for " + srec);
                        return false;
                    } else if (taskIdx == 0) {
                        return true;
                    } else {
                        if (!task.affinity.equals(((TaskRecord) this.mTaskHistory.get(taskIdx)).affinity)) {
                            return true;
                        }
                    }
                }
                return false;
            }

            final boolean navigateUpToLocked(ActivityRecord srec, Intent destIntent, int resultCode, Intent resultData) {
                TaskRecord task = srec.getTask();
                ArrayList<ActivityRecord> activities = task.mActivities;
                int start = activities.indexOf(srec);
                if (!this.mTaskHistory.contains(task) || start < 0) {
                    return false;
                }
                int i;
                int finishTo = start - 1;
                ActivityRecord activityRecord = finishTo < 0 ? null : (ActivityRecord) activities.get(finishTo);
                boolean foundParentInTask = false;
                ComponentName dest = destIntent.getComponent();
                if (start > 0 && dest != null) {
                    for (i = finishTo; i >= 0; i--) {
                        ActivityRecord r = (ActivityRecord) activities.get(i);
                        if (r.info.packageName.equals(dest.getPackageName()) && r.info.name.equals(dest.getClassName())) {
                            finishTo = i;
                            activityRecord = r;
                            foundParentInTask = true;
                            break;
                        }
                    }
                }
                IActivityController controller = this.mService.mController;
                if (controller != null) {
                    ActivityRecord next = topRunningActivityLocked(srec.appToken, 0);
                    if (next != null) {
                        boolean resumeOK = true;
                        try {
                            resumeOK = controller.activityResuming(next.packageName);
                        } catch (RemoteException e) {
                            this.mService.mController = null;
                            Watchdog.getInstance().setActivityController(null);
                        }
                        if (!resumeOK) {
                            return false;
                        }
                    }
                }
                long origId = Binder.clearCallingIdentity();
                for (i = start; i > finishTo; i--) {
                    requestFinishActivityLocked(((ActivityRecord) activities.get(i)).appToken, resultCode, resultData, "navigate-up", true);
                    resultCode = 0;
                    resultData = null;
                }
                if (activityRecord != null && foundParentInTask) {
                    int parentLaunchMode = activityRecord.info.launchMode;
                    int destIntentFlags = destIntent.getFlags();
                    if (parentLaunchMode == 3 || parentLaunchMode == 2 || parentLaunchMode == 1 || (67108864 & destIntentFlags) != 0) {
                        activityRecord.deliverNewIntentLocked(srec.info.applicationInfo.uid, destIntent, srec.packageName);
                    } else {
                        try {
                            foundParentInTask = this.mService.mActivityStarter.startActivityLocked(srec.app.thread, destIntent, null, null, AppGlobals.getPackageManager().getActivityInfo(destIntent.getComponent(), 0, srec.userId), null, null, null, activityRecord.appToken, null, 0, -1, activityRecord.launchedFromUid, activityRecord.launchedFromPackage, -1, activityRecord.launchedFromUid, 0, null, false, true, null, null, "navigateUpTo") == 0;
                        } catch (RemoteException e2) {
                            foundParentInTask = false;
                        }
                        requestFinishActivityLocked(activityRecord.appToken, resultCode, resultData, "navigate-top", true);
                    }
                }
                Binder.restoreCallingIdentity(origId);
                return foundParentInTask;
            }

            void onActivityRemovedFromStack(ActivityRecord r) {
                if (this.mResumedActivity == r) {
                    this.mResumedActivity = null;
                }
                if (this.mPausingActivity == r) {
                    this.mPausingActivity = null;
                }
                removeTimeoutsForActivityLocked(r);
            }

            private void cleanUpActivityLocked(ActivityRecord r, boolean cleanServices, boolean setState) {
                onActivityRemovedFromStack(r);
                r.deferRelaunchUntilPaused = false;
                r.frozenBeforeDestroy = false;
                if (setState) {
                    r.state = ActivityState.DESTROYED;
                    r.app = null;
                }
                this.mStackSupervisor.cleanupActivity(r);
                if (r.finishing && r.pendingResults != null) {
                    for (WeakReference<PendingIntentRecord> apr : r.pendingResults) {
                        PendingIntentRecord rec = (PendingIntentRecord) apr.get();
                        if (rec != null) {
                            this.mService.cancelIntentSenderLocked(rec, false);
                        }
                    }
                    r.pendingResults = null;
                }
                if (cleanServices) {
                    cleanUpActivityServicesLocked(r);
                }
                removeTimeoutsForActivityLocked(r);
                this.mWindowManager.notifyAppRelaunchesCleared(r.appToken);
            }

            void removeTimeoutsForActivityLocked(ActivityRecord r) {
                this.mStackSupervisor.removeTimeoutsForActivityLocked(r);
                this.mHandler.removeMessages(101, r);
                this.mHandler.removeMessages(104, r);
                this.mHandler.removeMessages(102, r);
                r.finishLaunchTickingLocked();
            }

            private void removeActivityFromHistoryLocked(ActivityRecord r, String reason) {
                finishActivityResultsLocked(r, 0, null);
                r.makeFinishingLocked();
                r.takeFromHistory();
                removeTimeoutsForActivityLocked(r);
                r.state = ActivityState.DESTROYED;
                r.app = null;
                r.removeWindowContainer();
                TaskRecord task = r.getTask();
                boolean removeActivity = task != null ? task.removeActivity(r) : false;
                boolean onlyHasTaskOverlayActivities = task != null ? task.onlyHasTaskOverlayActivities(false) : false;
                if (removeActivity || onlyHasTaskOverlayActivities) {
                    if (this.mStackSupervisor.isFocusedStack(this) && task == topTask() && task.isOverHomeStack()) {
                        this.mStackSupervisor.moveHomeStackTaskToTop(reason);
                    }
                    if (onlyHasTaskOverlayActivities) {
                        this.mStackSupervisor.removeTaskByIdLocked(task.taskId, false, false, true);
                    }
                    if (removeActivity) {
                        removeTask(task, reason, 0);
                    }
                }
                cleanUpActivityServicesLocked(r);
                r.removeUriPermissionsLocked();
            }

            private void cleanUpActivityServicesLocked(ActivityRecord r) {
                if (r.connections != null) {
                    Iterator<ConnectionRecord> it = r.connections.iterator();
                    while (it.hasNext()) {
                        this.mService.mServices.removeConnectionLocked((ConnectionRecord) it.next(), null, r);
                    }
                    r.connections = null;
                }
            }

            final void scheduleDestroyActivities(ProcessRecord owner, String reason) {
                Message msg = this.mHandler.obtainMessage(105);
                msg.obj = new ScheduleDestroyArgs(owner, reason);
                this.mHandler.sendMessage(msg);
            }

            private void destroyActivitiesLocked(ProcessRecord owner, String reason) {
                boolean lastIsOpaque = false;
                boolean activityRemoved = false;
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        if (!r.finishing) {
                            if (r.fullscreen) {
                                lastIsOpaque = true;
                            }
                            if ((owner == null || r.app == owner) && lastIsOpaque && r.isDestroyable() && destroyActivityLocked(r, true, reason)) {
                                activityRemoved = true;
                            }
                        }
                    }
                }
                if (activityRemoved) {
                    this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                }
            }

            final boolean safelyDestroyActivityLocked(ActivityRecord r, String reason) {
                if (r.isDestroyable()) {
                    return destroyActivityLocked(r, true, reason);
                }
                return false;
            }

            final int releaseSomeActivitiesLocked(ProcessRecord app, ArraySet<TaskRecord> tasks, String reason) {
                int maxTasks = tasks.size() / 4;
                if (maxTasks < 1) {
                    maxTasks = 1;
                }
                int numReleased = 0;
                int taskNdx = 0;
                while (taskNdx < this.mTaskHistory.size() && maxTasks > 0) {
                    TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
                    if (tasks.contains(task)) {
                        int curNum = 0;
                        ArrayList<ActivityRecord> activities = task.mActivities;
                        int actNdx = 0;
                        while (actNdx < activities.size()) {
                            ActivityRecord activity = (ActivityRecord) activities.get(actNdx);
                            if (activity.app == app && activity.isDestroyable()) {
                                destroyActivityLocked(activity, true, reason);
                                if (activities.get(actNdx) != activity) {
                                    actNdx--;
                                }
                                curNum++;
                            }
                            actNdx++;
                        }
                        if (curNum > 0) {
                            numReleased += curNum;
                            maxTasks--;
                            if (this.mTaskHistory.get(taskNdx) != task) {
                                taskNdx--;
                            }
                        }
                    }
                    taskNdx++;
                }
                return numReleased;
            }

            final boolean destroyActivityLocked(ActivityRecord r, boolean removeFromApp, String reason) {
                EventLog.writeEvent(EventLogTags.AM_DESTROY_ACTIVITY, new Object[]{Integer.valueOf(r.userId), Integer.valueOf(System.identityHashCode(r)), Integer.valueOf(r.getTask().taskId), r.shortComponentName, reason});
                boolean removedFromHistory = false;
                cleanUpActivityLocked(r, false, false);
                boolean hadApp = r.app != null;
                if (hadApp) {
                    if (removeFromApp) {
                        r.app.activities.remove(r);
                        if (this.mService.mHeavyWeightProcess == r.app && r.app.activities.size() <= 0) {
                            this.mService.mHeavyWeightProcess = null;
                            this.mService.mHandler.sendEmptyMessage(25);
                        }
                        if (r.app.activities.isEmpty()) {
                            this.mService.mServices.updateServiceConnectionActivitiesLocked(r.app);
                            this.mService.updateLruProcessLocked(r.app, false, null);
                            this.mService.updateOomAdjLocked();
                        }
                    }
                    boolean skipDestroy = false;
                    try {
                        r.app.thread.scheduleDestroyActivity(r.appToken, r.finishing, r.configChangeFlags);
                    } catch (Exception e) {
                        if (r.finishing) {
                            removeActivityFromHistoryLocked(r, reason + " exceptionInScheduleDestroy");
                            removedFromHistory = true;
                            skipDestroy = true;
                        }
                    }
                    r.nowVisible = false;
                    if (!r.finishing || (skipDestroy ^ 1) == 0) {
                        r.state = ActivityState.DESTROYED;
                        r.app = null;
                    } else {
                        r.state = ActivityState.DESTROYING;
                        this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(102, r), JobStatus.DEFAULT_TRIGGER_UPDATE_DELAY);
                    }
                } else if (r.finishing) {
                    removeActivityFromHistoryLocked(r, reason + " hadNoApp");
                    removedFromHistory = true;
                } else {
                    r.state = ActivityState.DESTROYED;
                    r.app = null;
                }
                r.configChangeFlags = 0;
                if (!this.mLRUActivities.remove(r) && hadApp) {
                    Slog.w(TAG, "Activity " + r + " being finished, but not in LRU list");
                }
                return removedFromHistory;
            }

            final void activityDestroyedLocked(IBinder token, String reason) {
                long origId = Binder.clearCallingIdentity();
                try {
                    ActivityRecord r = ActivityRecord.forTokenLocked(token);
                    if (r != null) {
                        this.mHandler.removeMessages(102, r);
                    }
                    if (isInStackLocked(r) != null && r.state == ActivityState.DESTROYING) {
                        cleanUpActivityLocked(r, true, false);
                        removeActivityFromHistoryLocked(r, reason);
                    }
                    this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                } finally {
                    Binder.restoreCallingIdentity(origId);
                }
            }

            private void removeHistoryRecordsForAppLocked(ArrayList<ActivityRecord> list, ProcessRecord app, String listName) {
                int i = list.size();
                while (i > 0) {
                    i--;
                    ActivityRecord r = (ActivityRecord) list.get(i);
                    if (r.app == app) {
                        list.remove(i);
                        removeTimeoutsForActivityLocked(r);
                    }
                }
            }

            private boolean removeHistoryRecordsForAppLocked(ProcessRecord app) {
                removeHistoryRecordsForAppLocked(this.mLRUActivities, app, "mLRUActivities");
                removeHistoryRecordsForAppLocked(this.mStackSupervisor.mStoppingActivities, app, "mStoppingActivities");
                removeHistoryRecordsForAppLocked(this.mStackSupervisor.mGoingToSleepActivities, app, "mGoingToSleepActivities");
                removeHistoryRecordsForAppLocked(this.mStackSupervisor.mActivitiesWaitingForVisibleActivity, app, "mActivitiesWaitingForVisibleActivity");
                removeHistoryRecordsForAppLocked(this.mStackSupervisor.mFinishingActivities, app, "mFinishingActivities");
                boolean hasVisibleActivities = false;
                int i = numActivities();
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        i--;
                        if (r.app == app) {
                            boolean remove;
                            if (r.visible) {
                                hasVisibleActivities = true;
                            }
                            if ((!r.haveState && (r.stateNotNeeded ^ 1) != 0) || r.finishing) {
                                remove = true;
                            } else if (r.visible || r.launchCount <= 2 || r.lastLaunchTime <= SystemClock.uptimeMillis() - 60000) {
                                remove = false;
                            } else {
                                remove = true;
                            }
                            if (!remove) {
                                r.app = null;
                                r.nowVisible = r.visible;
                                if (!r.haveState) {
                                    r.icicle = null;
                                }
                            } else if (!r.finishing) {
                                Slog.w(TAG, "Force removing " + r + ": app died, no saved state");
                                EventLog.writeEvent(EventLogTags.AM_FINISH_ACTIVITY, new Object[]{Integer.valueOf(r.userId), Integer.valueOf(System.identityHashCode(r)), Integer.valueOf(r.getTask().taskId), r.shortComponentName, "proc died without state saved"});
                                if (r.state == ActivityState.RESUMED) {
                                    this.mService.updateUsageStats(r, false);
                                }
                            }
                            cleanUpActivityLocked(r, true, true);
                            if (remove) {
                                removeActivityFromHistoryLocked(r, "appDied");
                            }
                        }
                    }
                }
                return hasVisibleActivities;
            }

            private void updateTransitLocked(int transit, ActivityOptions options) {
                if (options != null) {
                    ActivityRecord r = topRunningActivityLocked();
                    if (r == null || r.state == ActivityState.RESUMED) {
                        ActivityOptions.abort(options);
                    } else {
                        r.updateOptionsLocked(options);
                    }
                }
                this.mWindowManager.prepareAppTransition(transit, false);
            }

            private void updateTaskMovement(TaskRecord task, boolean toFront) {
                if (task.isPersistable) {
                    task.mLastTimeMoved = System.currentTimeMillis();
                    if (!toFront) {
                        task.mLastTimeMoved *= -1;
                    }
                }
                this.mStackSupervisor.invalidateTaskLayers();
            }

            void moveHomeStackTaskToTop() {
                int top = this.mTaskHistory.size() - 1;
                for (int taskNdx = top; taskNdx >= 0; taskNdx--) {
                    TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
                    if (task.taskType == 1) {
                        this.mTaskHistory.remove(taskNdx);
                        this.mTaskHistory.add(top, task);
                        updateTaskMovement(task, true);
                        return;
                    }
                }
            }

            final void moveTaskToFrontLocked(TaskRecord tr, boolean noAnimation, ActivityOptions options, AppTimeTracker timeTracker, String reason) {
                ActivityStack topStack = getTopStackOnDisplay();
                ActivityRecord topActivity = topStack != null ? topStack.topActivity() : null;
                int numTasks = this.mTaskHistory.size();
                int index = this.mTaskHistory.indexOf(tr);
                if (numTasks == 0 || index < 0) {
                    if (noAnimation) {
                        ActivityOptions.abort(options);
                    } else {
                        updateTransitLocked(10, options);
                    }
                    return;
                }
                if (timeTracker != null) {
                    for (int i = tr.mActivities.size() - 1; i >= 0; i--) {
                        ((ActivityRecord) tr.mActivities.get(i)).appTimeTracker = timeTracker;
                    }
                }
                insertTaskAtTop(tr, null);
                ActivityRecord top = tr.getTopActivity();
                if (top == null || (top.okToShowLocked() ^ 1) != 0) {
                    addRecentActivityLocked(top);
                    ActivityOptions.abort(options);
                    return;
                }
                ActivityRecord r = topRunningActivityLocked();
                this.mStackSupervisor.moveFocusableActivityStackToFrontLocked(r, reason);
                if (noAnimation) {
                    this.mWindowManager.prepareAppTransition(0, false);
                    if (r != null) {
                        this.mNoAnimActivities.add(r);
                    }
                    ActivityOptions.abort(options);
                } else {
                    updateTransitLocked(10, options);
                }
                if (canEnterPipOnTaskSwitch(topActivity, tr, null, options)) {
                    topActivity.supportsEnterPipOnTaskSwitch = true;
                }
                this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                EventLog.writeEvent(EventLogTags.AM_TASK_TO_FRONT, new Object[]{Integer.valueOf(tr.userId), Integer.valueOf(tr.taskId)});
                this.mService.mTaskChangeNotificationController.notifyTaskMovedToFront(tr.taskId);
            }

            final boolean moveTaskToBackLocked(int taskId) {
                TaskRecord tr = taskForIdLocked(taskId);
                if (tr == null) {
                    Slog.i(TAG, "moveTaskToBack: bad taskId=" + taskId);
                    return false;
                }
                Slog.i(TAG, "moveTaskToBack: " + tr);
                if (this.mStackSupervisor.isLockedTask(tr)) {
                    this.mStackSupervisor.showLockTaskToast();
                    return false;
                }
                if (this.mStackSupervisor.isFrontStackOnDisplay(this) && this.mService.mController != null) {
                    ActivityRecord next = topRunningActivityLocked(null, taskId);
                    if (next == null) {
                        next = topRunningActivityLocked(null, 0);
                    }
                    if (next != null) {
                        boolean moveOK = true;
                        try {
                            moveOK = this.mService.mController.activityResuming(next.packageName);
                        } catch (RemoteException e) {
                            this.mService.mController = null;
                            Watchdog.getInstance().setActivityController(null);
                        }
                        if (!moveOK) {
                            return false;
                        }
                    }
                }
                boolean prevIsHome = false;
                boolean isOverHomeStack = !tr.isHomeTask() ? tr.isOverHomeStack() : false;
                if (isOverHomeStack) {
                    TaskRecord nextTask = getNextTask(tr);
                    if (nextTask != null) {
                        nextTask.setTaskToReturnTo(tr.getTaskToReturnTo());
                    } else {
                        prevIsHome = true;
                    }
                }
                if (this.mTaskHistory.indexOf(tr) != 0) {
                    this.mTaskHistory.remove(tr);
                    this.mTaskHistory.add(0, tr);
                    updateTaskMovement(tr, false);
                    this.mWindowManager.prepareAppTransition(11, false);
                    this.mWindowContainerController.positionChildAtBottom(tr.getWindowContainerController());
                }
                if (this.mStackId == 4) {
                    this.mStackSupervisor.removeStackLocked(4);
                    return true;
                }
                TaskRecord task;
                int numTasks = this.mTaskHistory.size();
                for (int taskNdx = numTasks - 1; taskNdx >= 1; taskNdx--) {
                    task = (TaskRecord) this.mTaskHistory.get(taskNdx);
                    if (task.isOverHomeStack()) {
                        break;
                    }
                    if (taskNdx == 1) {
                        task.setTaskToReturnTo(1);
                    }
                }
                task = this.mResumedActivity != null ? this.mResumedActivity.getTask() : null;
                if (!prevIsHome && ((task != tr || !isOverHomeStack) && (numTasks > 1 || !isOnHomeDisplay()))) {
                    this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                    return true;
                } else if (!this.mService.mBooting && (this.mService.mBooted ^ 1) != 0) {
                    return false;
                } else {
                    tr.setTaskToReturnTo(0);
                    return this.mStackSupervisor.resumeHomeStackTask(null, "moveTaskToBack");
                }
            }

            private ActivityStack getTopStackOnDisplay() {
                ArrayList<ActivityStack> stacks = getDisplay().mStacks;
                return stacks.isEmpty() ? null : (ActivityStack) stacks.get(stacks.size() - 1);
            }

            static void logStartActivity(int tag, ActivityRecord r, TaskRecord task) {
                Uri data = r.intent.getData();
                String toSafeString = data != null ? data.toSafeString() : null;
                EventLog.writeEvent(tag, new Object[]{Integer.valueOf(r.userId), Integer.valueOf(System.identityHashCode(r)), Integer.valueOf(task.taskId), r.shortComponentName, r.intent.getAction(), r.intent.getType(), toSafeString, Integer.valueOf(r.intent.getFlags())});
            }

            void ensureVisibleActivitiesConfigurationLocked(ActivityRecord start, boolean preserveWindow) {
                if (start != null && (start.visible ^ 1) == 0) {
                    boolean behindFullscreen = false;
                    boolean updatedConfig = false;
                    for (int taskIndex = this.mTaskHistory.indexOf(start.getTask()); taskIndex >= 0; taskIndex--) {
                        TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskIndex);
                        ArrayList<ActivityRecord> activities = task.mActivities;
                        int activityIndex = start.getTask() == task ? activities.indexOf(start) : activities.size() - 1;
                        while (activityIndex >= 0) {
                            ActivityRecord r = (ActivityRecord) activities.get(activityIndex);
                            updatedConfig |= r.ensureActivityConfigurationLocked(0, preserveWindow);
                            if (r.fullscreen) {
                                behindFullscreen = true;
                                break;
                            }
                            activityIndex--;
                        }
                        if (behindFullscreen) {
                            break;
                        }
                    }
                    if (updatedConfig) {
                        this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                    }
                }
            }

            public void requestResize(Rect bounds) {
                this.mService.resizeStack(this.mStackId, bounds, true, false, false, -1);
            }

            void resize(Rect bounds, Rect tempTaskBounds, Rect tempTaskInsetBounds) {
                bounds = TaskRecord.validateBounds(bounds);
                if (updateBoundsAllowed(bounds, tempTaskBounds, tempTaskInsetBounds)) {
                    Rect taskBounds = tempTaskBounds != null ? tempTaskBounds : bounds;
                    Rect insetBounds = tempTaskInsetBounds != null ? tempTaskInsetBounds : taskBounds;
                    this.mTmpBounds.clear();
                    this.mTmpConfigs.clear();
                    this.mTmpInsetBounds.clear();
                    for (int i = this.mTaskHistory.size() - 1; i >= 0; i--) {
                        TaskRecord task = (TaskRecord) this.mTaskHistory.get(i);
                        if (task.isResizeable()) {
                            if (this.mStackId == 2) {
                                this.mTmpRect2.set(task.mBounds);
                                fitWithinBounds(this.mTmpRect2, bounds);
                                task.updateOverrideConfiguration(this.mTmpRect2);
                            } else {
                                task.updateOverrideConfiguration(taskBounds, insetBounds);
                            }
                        }
                        this.mTmpConfigs.put(task.taskId, task.getOverrideConfiguration());
                        this.mTmpBounds.put(task.taskId, task.mBounds);
                        if (tempTaskInsetBounds != null) {
                            this.mTmpInsetBounds.put(task.taskId, tempTaskInsetBounds);
                        }
                    }
                    this.mFullscreen = this.mWindowContainerController.resize(bounds, this.mTmpConfigs, this.mTmpBounds, this.mTmpInsetBounds);
                    setBounds(bounds);
                }
            }

            private static void fitWithinBounds(Rect bounds, Rect stackBounds) {
                if (stackBounds != null && !stackBounds.contains(bounds)) {
                    if (bounds.left < stackBounds.left || bounds.right > stackBounds.right) {
                        int maxRight = stackBounds.right - (stackBounds.width() / 3);
                        int horizontalDiff = stackBounds.left - bounds.left;
                        if (horizontalDiff >= 0 || bounds.left < maxRight) {
                            if (bounds.left + horizontalDiff >= maxRight) {
                            }
                            bounds.left += horizontalDiff;
                            bounds.right += horizontalDiff;
                        }
                        horizontalDiff = maxRight - bounds.left;
                        bounds.left += horizontalDiff;
                        bounds.right += horizontalDiff;
                    }
                    if (bounds.top < stackBounds.top || bounds.bottom > stackBounds.bottom) {
                        int maxBottom = stackBounds.bottom - (stackBounds.height() / 3);
                        int verticalDiff = stackBounds.top - bounds.top;
                        if (verticalDiff >= 0 || bounds.top < maxBottom) {
                            if (bounds.top + verticalDiff >= maxBottom) {
                            }
                            bounds.top += verticalDiff;
                            bounds.bottom += verticalDiff;
                        }
                        verticalDiff = maxBottom - bounds.top;
                        bounds.top += verticalDiff;
                        bounds.bottom += verticalDiff;
                    }
                }
            }

            boolean willActivityBeVisibleLocked(IBinder token) {
                ActivityRecord r;
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        r = (ActivityRecord) activities.get(activityNdx);
                        if (r.appToken == token) {
                            return true;
                        }
                        if (r.fullscreen && (r.finishing ^ 1) != 0) {
                            return false;
                        }
                    }
                }
                r = ActivityRecord.forTokenLocked(token);
                if (r == null) {
                    return false;
                }
                if (r.finishing) {
                    Slog.e(TAG, "willActivityBeVisibleLocked: Returning false, would have returned true for r=" + r);
                }
                return r.finishing ^ 1;
            }

            void closeSystemDialogsLocked() {
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        if ((r.info.flags & 256) != 0) {
                            finishActivityLocked(r, 0, null, "close-sys", true);
                        }
                    }
                }
            }

            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            boolean finishDisabledPackageActivitiesLocked(java.lang.String r16, java.util.Set<java.lang.String> r17, boolean r18, boolean r19, int r20) {
                /*
                r15 = this;
                r9 = 0;
                r11 = 0;
                r10 = 0;
                r1 = r15.mTaskHistory;
                r1 = r1.size();
                r14 = r1 + -1;
            L_0x000b:
                if (r14 < 0) goto L_0x00e5;
            L_0x000d:
                r1 = r15.mTaskHistory;
                r1 = r1.get(r14);
                r1 = (com.android.server.am.TaskRecord) r1;
                r7 = r1.mActivities;
                r12 = r7.size();
                r8 = 0;
            L_0x001c:
                if (r8 >= r12) goto L_0x00e1;
            L_0x001e:
                r2 = r7.get(r8);
                r2 = (com.android.server.am.ActivityRecord) r2;
                r1 = r2.packageName;
                r0 = r16;
                r1 = r1.equals(r0);
                if (r1 == 0) goto L_0x003e;
            L_0x002e:
                if (r17 == 0) goto L_0x0071;
            L_0x0030:
                r1 = r2.realActivity;
                r1 = r1.getClassName();
                r0 = r17;
                r1 = r0.contains(r1);
                if (r1 != 0) goto L_0x0071;
            L_0x003e:
                if (r16 != 0) goto L_0x0073;
            L_0x0040:
                r1 = r2.userId;
                r0 = r20;
                if (r1 != r0) goto L_0x0073;
            L_0x0046:
                r13 = 1;
            L_0x0047:
                r1 = -1;
                r0 = r20;
                if (r0 == r1) goto L_0x0052;
            L_0x004c:
                r1 = r2.userId;
                r0 = r20;
                if (r1 != r0) goto L_0x006e;
            L_0x0052:
                if (r13 != 0) goto L_0x005a;
            L_0x0054:
                r1 = r2.getTask();
                if (r1 != r11) goto L_0x006e;
            L_0x005a:
                r1 = r2.app;
                if (r1 == 0) goto L_0x0068;
            L_0x005e:
                if (r19 != 0) goto L_0x0068;
            L_0x0060:
                r1 = r2.app;
                r1 = r1.persistent;
                r1 = r1 ^ 1;
                if (r1 == 0) goto L_0x006e;
            L_0x0068:
                if (r18 != 0) goto L_0x0077;
            L_0x006a:
                r1 = r2.finishing;
                if (r1 == 0) goto L_0x0075;
            L_0x006e:
                r8 = r8 + 1;
                goto L_0x001c;
            L_0x0071:
                r13 = 1;
                goto L_0x0047;
            L_0x0073:
                r13 = 0;
                goto L_0x0047;
            L_0x0075:
                r1 = 1;
                return r1;
            L_0x0077:
                r1 = r2.isHomeActivity();
                if (r1 == 0) goto L_0x00a3;
            L_0x007d:
                if (r10 == 0) goto L_0x00a1;
            L_0x007f:
                r1 = r2.realActivity;
                r1 = r10.equals(r1);
                if (r1 == 0) goto L_0x00a1;
            L_0x0087:
                r1 = TAG;
                r3 = new java.lang.StringBuilder;
                r3.<init>();
                r4 = "Skip force-stop again ";
                r3 = r3.append(r4);
                r3 = r3.append(r2);
                r3 = r3.toString();
                android.util.Slog.i(r1, r3);
                goto L_0x006e;
            L_0x00a1:
                r10 = r2.realActivity;
            L_0x00a3:
                r9 = 1;
                r1 = TAG;
                r3 = new java.lang.StringBuilder;
                r3.<init>();
                r4 = "  Force finishing activity ";
                r3 = r3.append(r4);
                r3 = r3.append(r2);
                r3 = r3.toString();
                android.util.Slog.i(r1, r3);
                if (r13 == 0) goto L_0x00cb;
            L_0x00bf:
                r1 = r2.app;
                if (r1 == 0) goto L_0x00c8;
            L_0x00c3:
                r1 = r2.app;
                r3 = 1;
                r1.removed = r3;
            L_0x00c8:
                r1 = 0;
                r2.app = r1;
            L_0x00cb:
                r11 = r2.getTask();
                r5 = "force-stop";
                r3 = 0;
                r4 = 0;
                r6 = 1;
                r1 = r15;
                r1 = r1.finishActivityLocked(r2, r3, r4, r5, r6);
                if (r1 == 0) goto L_0x006e;
            L_0x00dc:
                r12 = r12 + -1;
                r8 = r8 + -1;
                goto L_0x006e;
            L_0x00e1:
                r14 = r14 + -1;
                goto L_0x000b;
            L_0x00e5:
                return r9;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.ActivityStack.finishDisabledPackageActivitiesLocked(java.lang.String, java.util.Set, boolean, boolean, int):boolean");
            }

            void getTasksLocked(List<RunningTaskInfo> list, int callingUid, boolean allowed) {
                boolean focusedStack = this.mStackSupervisor.getFocusedStack() == this;
                boolean topTask = true;
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
                    if (task.getTopActivity() != null) {
                        ActivityRecord r = null;
                        ActivityRecord top = null;
                        int numActivities = 0;
                        int numRunning = 0;
                        ArrayList<ActivityRecord> activities = task.mActivities;
                        if (allowed || (task.isHomeTask() ^ 1) == 0 || task.effectiveUid == callingUid) {
                            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                                ActivityRecord tmp = (ActivityRecord) activities.get(activityNdx);
                                if (!tmp.finishing) {
                                    r = tmp;
                                    if (top == null || top.state == ActivityState.INITIALIZING) {
                                        top = tmp;
                                        numRunning = 0;
                                        numActivities = 0;
                                    }
                                    numActivities++;
                                    if (!(tmp.app == null || tmp.app.thread == null)) {
                                        numRunning++;
                                    }
                                }
                            }
                            RunningTaskInfo ci = new RunningTaskInfo();
                            ci.id = task.taskId;
                            ci.stackId = this.mStackId;
                            ci.baseActivity = r.intent.getComponent();
                            ci.topActivity = top.intent.getComponent();
                            ci.lastActiveTime = task.lastActiveTime;
                            if (focusedStack && topTask) {
                                ci.lastActiveTime = System.currentTimeMillis();
                                topTask = false;
                            }
                            if (top.getTask() != null) {
                                ci.description = top.getTask().lastDescription;
                            }
                            ci.numActivities = numActivities;
                            ci.numRunning = numRunning;
                            ci.supportsSplitScreenMultiWindow = task.supportsSplitScreen();
                            ci.resizeMode = task.mResizeMode;
                            list.add(ci);
                        }
                    }
                }
            }

            void unhandledBackLocked() {
                int top = this.mTaskHistory.size() - 1;
                if (top >= 0) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(top)).mActivities;
                    int activityTop = activities.size() - 1;
                    if (activityTop >= 0) {
                        finishActivityLocked((ActivityRecord) activities.get(activityTop), 0, null, "unhandled-back", true);
                    }
                }
            }

            boolean handleAppDiedLocked(ProcessRecord app) {
                if (this.mPausingActivity != null && this.mPausingActivity.app == app) {
                    this.mPausingActivity = null;
                }
                if (this.mLastPausedActivity != null && this.mLastPausedActivity.app == app) {
                    this.mLastPausedActivity = null;
                    this.mLastNoHistoryActivity = null;
                }
                return removeHistoryRecordsForAppLocked(app);
            }

            void handleAppCrashLocked(ProcessRecord app) {
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                        if (r.app == app) {
                            Slog.w(TAG, "  Force finishing activity " + r.intent.getComponent().flattenToShortString());
                            r.app = null;
                            finishCurrentActivityLocked(r, 0, false);
                        }
                    }
                }
            }

            boolean dumpActivitiesLocked(FileDescriptor fd, PrintWriter pw, boolean dumpAll, boolean dumpClient, String dumpPackage, boolean needSep, String header) {
                boolean printed = false;
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    TaskRecord task = (TaskRecord) this.mTaskHistory.get(taskNdx);
                    boolean z = dumpAll ^ 1;
                    String str = "    Task id #" + task.taskId + "\n" + "    mFullscreen=" + task.mFullscreen + "\n" + "    mBounds=" + task.mBounds + "\n" + "    mMinWidth=" + task.mMinWidth + "\n" + "    mMinHeight=" + task.mMinHeight + "\n" + "    mLastNonFullscreenBounds=" + task.mLastNonFullscreenBounds;
                    printed |= ActivityStackSupervisor.dumpHistoryList(fd, pw, ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities, "    ", "Hist", true, z, dumpClient, dumpPackage, needSep, header, str);
                    if (printed) {
                        header = null;
                    }
                }
                return printed;
            }

            ArrayList<ActivityRecord> getDumpActivitiesLocked(String name) {
                ArrayList<ActivityRecord> activities = new ArrayList();
                int taskNdx;
                if ("all".equals(name)) {
                    for (taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                        activities.addAll(((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities);
                    }
                } else if ("top".equals(name)) {
                    int top = this.mTaskHistory.size() - 1;
                    if (top >= 0) {
                        ArrayList<ActivityRecord> list = ((TaskRecord) this.mTaskHistory.get(top)).mActivities;
                        int listTop = list.size() - 1;
                        if (listTop >= 0) {
                            activities.add((ActivityRecord) list.get(listTop));
                        }
                    }
                } else {
                    ItemMatcher matcher = new ItemMatcher();
                    matcher.build(name);
                    for (taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                        for (ActivityRecord r1 : ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities) {
                            if (matcher.match(r1, r1.intent.getComponent())) {
                                activities.add(r1);
                            }
                        }
                    }
                }
                return activities;
            }

            ActivityRecord restartPackage(String packageName) {
                ActivityRecord starting = topRunningActivityLocked();
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ArrayList<ActivityRecord> activities = ((TaskRecord) this.mTaskHistory.get(taskNdx)).mActivities;
                    for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                        ActivityRecord a = (ActivityRecord) activities.get(activityNdx);
                        if (a.info.packageName.equals(packageName)) {
                            a.forceNewConfig = true;
                            if (starting != null && a == starting && a.visible) {
                                a.startFreezingScreenLocked(starting.app, 256);
                            }
                        }
                    }
                }
                return starting;
            }

            void removeTask(TaskRecord task, String reason, int mode) {
                for (ActivityRecord record : task.mActivities) {
                    onActivityRemovedFromStack(record);
                }
                int taskNdx = this.mTaskHistory.indexOf(task);
                int topTaskNdx = this.mTaskHistory.size() - 1;
                if (task.isOverHomeStack() && taskNdx < topTaskNdx) {
                    TaskRecord nextTask = (TaskRecord) this.mTaskHistory.get(taskNdx + 1);
                    if (!(nextTask.isOverHomeStack() || (nextTask.isOverAssistantStack() ^ 1) == 0)) {
                        nextTask.setTaskToReturnTo(1);
                    }
                }
                this.mTaskHistory.remove(task);
                removeActivitiesFromLRUListLocked(task);
                updateTaskMovement(task, true);
                if (mode == 0 && task.mActivities.isEmpty()) {
                    boolean isVoiceSession = task.voiceSession != null;
                    if (isVoiceSession) {
                        try {
                            task.voiceSession.taskFinished(task.intent, task.taskId);
                        } catch (RemoteException e) {
                        }
                    }
                    if (task.autoRemoveFromRecents() || isVoiceSession) {
                        this.mRecentTasks.remove(task);
                        task.removedFromRecents();
                    }
                    task.removeWindowContainer();
                }
                if (this.mTaskHistory.isEmpty()) {
                    if (isOnHomeDisplay() && mode != 2 && this.mStackSupervisor.isFocusedStack(this)) {
                        String myReason = reason + " leftTaskHistoryEmpty";
                        if (this.mFullscreen || (adjustFocusToNextFocusableStackLocked(myReason) ^ 1) != 0) {
                            this.mStackSupervisor.moveHomeStackToFront(myReason);
                        }
                    }
                    if (this.mStacks != null) {
                        this.mStacks.remove(this);
                        this.mStacks.add(0, this);
                    }
                    if (!isHomeOrRecentsStack()) {
                        remove();
                    }
                }
                task.setStack(null);
                if (this.mStackId == 4) {
                    this.mService.mTaskChangeNotificationController.notifyActivityUnpinned();
                }
            }

            TaskRecord createTaskRecord(int taskId, ActivityInfo info, Intent intent, IVoiceInteractionSession voiceSession, IVoiceInteractor voiceInteractor, boolean toTop, int type) {
                TaskRecord task = new TaskRecord(this.mService, taskId, info, intent, voiceSession, voiceInteractor, type);
                addTask(task, toTop, "createTaskRecord");
                boolean isLockscreenShown = this.mService.mStackSupervisor.mKeyguardController.isKeyguardShowing(this.mDisplayId != -1 ? this.mDisplayId : 0);
                if (!(layoutTaskInStack(task, info.windowLayout) || this.mBounds == null || !task.isResizeable() || (isLockscreenShown ^ 1) == 0)) {
                    task.updateOverrideConfiguration(this.mBounds);
                }
                task.createWindowContainer(toTop, (info.flags & 1024) != 0);
                return task;
            }

            boolean layoutTaskInStack(TaskRecord task, WindowLayout windowLayout) {
                if (this.mTaskPositioner == null) {
                    return false;
                }
                this.mTaskPositioner.updateDefaultBounds(task, this.mTaskHistory, windowLayout);
                return true;
            }

            ArrayList<TaskRecord> getAllTasks() {
                return new ArrayList(this.mTaskHistory);
            }

            void addTask(TaskRecord task, boolean toTop, String reason) {
                addTask(task, toTop ? Integer.MAX_VALUE : 0, true, reason);
                if (toTop) {
                    this.mWindowContainerController.positionChildAtTop(task.getWindowContainerController(), true);
                }
            }

            void addTask(TaskRecord task, int position, boolean schedulePictureInPictureModeChange, String reason) {
                this.mTaskHistory.remove(task);
                position = getAdjustedPositionForTask(task, position, null);
                boolean toTop = position >= this.mTaskHistory.size();
                ActivityStack prevStack = preAddTask(task, reason, toTop);
                this.mTaskHistory.add(position, task);
                task.setStack(this);
                if (toTop) {
                    updateTaskReturnToForTopInsertion(task);
                }
                updateTaskMovement(task, toTop);
                postAddTask(task, prevStack, schedulePictureInPictureModeChange);
            }

            void positionChildAt(TaskRecord task, int index) {
                if (task.getStack() != this) {
                    throw new IllegalArgumentException("AS.positionChildAt: task=" + task + " is not a child of stack=" + this + " current parent=" + task.getStack());
                }
                task.updateOverrideConfigurationForStack(this);
                ActivityRecord topRunningActivity = task.topRunningActivityLocked();
                boolean wasResumed = topRunningActivity == task.getStack().mResumedActivity;
                insertTaskAtPosition(task, index);
                task.setStack(this);
                postAddTask(task, null, true);
                if (wasResumed) {
                    if (this.mResumedActivity != null) {
                        Log.wtf(TAG, "mResumedActivity was already set when moving mResumedActivity from other stack to this stack mResumedActivity=" + this.mResumedActivity + " other mResumedActivity=" + topRunningActivity);
                    }
                    this.mResumedActivity = topRunningActivity;
                }
                ensureActivitiesVisibleLocked(null, 0, false);
                this.mStackSupervisor.resumeFocusedStackTopActivityLocked();
            }

            private ActivityStack preAddTask(TaskRecord task, String reason, boolean toTop) {
                ActivityStack prevStack = task.getStack();
                if (!(prevStack == null || prevStack == this)) {
                    prevStack.removeTask(task, reason, toTop ? 2 : 1);
                }
                return prevStack;
            }

            private void postAddTask(TaskRecord task, ActivityStack prevStack, boolean schedulePictureInPictureModeChange) {
                if (schedulePictureInPictureModeChange && prevStack != null) {
                    this.mStackSupervisor.scheduleUpdatePictureInPictureModeIfNeeded(task, prevStack);
                } else if (task.voiceSession != null) {
                    try {
                        task.voiceSession.taskStarted(task.intent, task.taskId);
                    } catch (RemoteException e) {
                    }
                }
            }

            void moveToFrontAndResumeStateIfNeeded(ActivityRecord r, boolean moveToFront, boolean setResume, boolean setPause, String reason) {
                if (moveToFront) {
                    if (setResume) {
                        this.mResumedActivity = r;
                        updateLRUListLocked(r);
                    }
                    if (setPause) {
                        this.mPausingActivity = r;
                        schedulePauseTimeout(r);
                    }
                    moveToFront(reason);
                }
            }

            public int getStackId() {
                return this.mStackId;
            }

            public String toString() {
                return "ActivityStack{" + Integer.toHexString(System.identityHashCode(this)) + " stackId=" + this.mStackId + ", " + this.mTaskHistory.size() + " tasks}";
            }

            void onLockTaskPackagesUpdatedLocked() {
                for (int taskNdx = this.mTaskHistory.size() - 1; taskNdx >= 0; taskNdx--) {
                    ((TaskRecord) this.mTaskHistory.get(taskNdx)).setLockTaskAuth();
                }
            }

            void executeAppTransition(ActivityOptions options) {
                this.mWindowManager.executeAppTransition();
                this.mNoAnimActivities.clear();
                ActivityOptions.abort(options);
            }

            boolean shouldSleepActivities() {
                ActivityDisplay display = getDisplay();
                return display != null ? display.isSleeping() : this.mService.isSleepingLocked();
            }

            boolean shouldSleepOrShutDownActivities() {
                return !shouldSleepActivities() ? this.mService.isShuttingDownLocked() : true;
            }
        }
