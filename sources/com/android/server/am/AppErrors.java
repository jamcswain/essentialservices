package com.android.server.am;

import android.app.ActivityManager.ProcessErrorStateInfo;
import android.app.ActivityThread;
import android.app.ApplicationErrorReport;
import android.app.ApplicationErrorReport.AnrInfo;
import android.app.ApplicationErrorReport.CrashInfo;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Binder;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.Settings.Secure;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.EventLog;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.util.TimeUtils;
import com.android.internal.app.ProcessMap;
import com.android.server.Watchdog;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

class AppErrors {
    private static final String TAG = "ActivityManager";
    private ArraySet<String> mAppsNotReportingCrashes;
    private final ProcessMap<BadProcessInfo> mBadProcesses = new ProcessMap();
    private final Context mContext;
    private final ProcessMap<Long> mProcessCrashTimes = new ProcessMap();
    private final ProcessMap<Long> mProcessCrashTimesPersistent = new ProcessMap();
    private final ActivityManagerService mService;
    SimpleDateFormat mTraceDateFormat = new SimpleDateFormat("dd_MMM_HH_mm_ss.SSS");

    static final class BadProcessInfo {
        final String longMsg;
        final String shortMsg;
        final String stack;
        final long time;

        BadProcessInfo(long time, String shortMsg, String longMsg, String stack) {
            this.time = time;
            this.shortMsg = shortMsg;
            this.longMsg = longMsg;
            this.stack = stack;
        }
    }

    AppErrors(Context context, ActivityManagerService service) {
        context.assertRuntimeOverlayThemable();
        this.mService = service;
        this.mContext = context;
    }

    boolean dumpLocked(FileDescriptor fd, PrintWriter pw, boolean needSep, String dumpPackage) {
        boolean printed;
        int processCount;
        int ip;
        String pname;
        int uidCount;
        int i;
        int puid;
        if (!this.mProcessCrashTimes.getMap().isEmpty()) {
            printed = false;
            long now = SystemClock.uptimeMillis();
            ArrayMap<String, SparseArray<Long>> pmap = this.mProcessCrashTimes.getMap();
            processCount = pmap.size();
            for (ip = 0; ip < processCount; ip++) {
                pname = (String) pmap.keyAt(ip);
                SparseArray<Long> uids = (SparseArray) pmap.valueAt(ip);
                uidCount = uids.size();
                for (i = 0; i < uidCount; i++) {
                    puid = uids.keyAt(i);
                    ProcessRecord r = (ProcessRecord) this.mService.mProcessNames.get(pname, puid);
                    if (dumpPackage == null || (r != null && (r.pkgList.containsKey(dumpPackage) ^ 1) == 0)) {
                        if (!printed) {
                            if (needSep) {
                                pw.println();
                            }
                            needSep = true;
                            pw.println("  Time since processes crashed:");
                            printed = true;
                        }
                        pw.print("    Process ");
                        pw.print(pname);
                        pw.print(" uid ");
                        pw.print(puid);
                        pw.print(": last crashed ");
                        TimeUtils.formatDuration(now - ((Long) uids.valueAt(i)).longValue(), pw);
                        pw.println(" ago");
                    }
                }
            }
        }
        if (!this.mBadProcesses.getMap().isEmpty()) {
            printed = false;
            ArrayMap<String, SparseArray<BadProcessInfo>> pmap2 = this.mBadProcesses.getMap();
            processCount = pmap2.size();
            for (ip = 0; ip < processCount; ip++) {
                pname = (String) pmap2.keyAt(ip);
                SparseArray<BadProcessInfo> uids2 = (SparseArray) pmap2.valueAt(ip);
                uidCount = uids2.size();
                for (i = 0; i < uidCount; i++) {
                    puid = uids2.keyAt(i);
                    r = (ProcessRecord) this.mService.mProcessNames.get(pname, puid);
                    if (dumpPackage == null || (r != null && (r.pkgList.containsKey(dumpPackage) ^ 1) == 0)) {
                        if (!printed) {
                            if (needSep) {
                                pw.println();
                            }
                            needSep = true;
                            pw.println("  Bad processes:");
                            printed = true;
                        }
                        BadProcessInfo info = (BadProcessInfo) uids2.valueAt(i);
                        pw.print("    Bad process ");
                        pw.print(pname);
                        pw.print(" uid ");
                        pw.print(puid);
                        pw.print(": crashed at time ");
                        pw.println(info.time);
                        if (info.shortMsg != null) {
                            pw.print("      Short msg: ");
                            pw.println(info.shortMsg);
                        }
                        if (info.longMsg != null) {
                            pw.print("      Long msg: ");
                            pw.println(info.longMsg);
                        }
                        if (info.stack != null) {
                            pw.println("      Stack:");
                            int lastPos = 0;
                            for (int pos = 0; pos < info.stack.length(); pos++) {
                                if (info.stack.charAt(pos) == '\n') {
                                    pw.print("        ");
                                    pw.write(info.stack, lastPos, pos - lastPos);
                                    pw.println();
                                    lastPos = pos + 1;
                                }
                            }
                            if (lastPos < info.stack.length()) {
                                pw.print("        ");
                                pw.write(info.stack, lastPos, info.stack.length() - lastPos);
                                pw.println();
                            }
                        }
                    }
                }
            }
        }
        return needSep;
    }

    boolean isBadProcessLocked(ApplicationInfo info) {
        return this.mBadProcesses.get(info.processName, info.uid) != null;
    }

    void clearBadProcessLocked(ApplicationInfo info) {
        this.mBadProcesses.remove(info.processName, info.uid);
    }

    void resetProcessCrashTimeLocked(ApplicationInfo info) {
        this.mProcessCrashTimes.remove(info.processName, info.uid);
    }

    void resetProcessCrashTimeLocked(boolean resetEntireUser, int appId, int userId) {
        ArrayMap<String, SparseArray<Long>> pmap = this.mProcessCrashTimes.getMap();
        for (int ip = pmap.size() - 1; ip >= 0; ip--) {
            SparseArray<Long> ba = (SparseArray) pmap.valueAt(ip);
            for (int i = ba.size() - 1; i >= 0; i--) {
                boolean remove = false;
                int entUid = ba.keyAt(i);
                if (resetEntireUser) {
                    if (UserHandle.getUserId(entUid) == userId) {
                        remove = true;
                    }
                } else if (userId == -1) {
                    if (UserHandle.getAppId(entUid) == appId) {
                        remove = true;
                    }
                } else if (entUid == UserHandle.getUid(userId, appId)) {
                    remove = true;
                }
                if (remove) {
                    ba.removeAt(i);
                }
            }
            if (ba.size() == 0) {
                pmap.removeAt(ip);
            }
        }
    }

    void loadAppsNotReportingCrashesFromConfigLocked(String appsNotReportingCrashesConfig) {
        if (appsNotReportingCrashesConfig != null) {
            String[] split = appsNotReportingCrashesConfig.split(",");
            if (split.length > 0) {
                this.mAppsNotReportingCrashes = new ArraySet();
                Collections.addAll(this.mAppsNotReportingCrashes, split);
            }
        }
    }

    void killAppAtUserRequestLocked(ProcessRecord app, Dialog fromDialog) {
        app.crashing = false;
        app.crashingReport = null;
        app.notResponding = false;
        app.notRespondingReport = null;
        if (app.anrDialog == fromDialog) {
            app.anrDialog = null;
        }
        if (app.waitDialog == fromDialog) {
            app.waitDialog = null;
        }
        if (app.pid > 0 && app.pid != ActivityManagerService.MY_PID) {
            handleAppCrashLocked(app, "user-terminated", null, null, null, null);
            app.kill("user request after error", true);
        }
    }

    void scheduleAppCrashLocked(int uid, int initialPid, String packageName, int userId, String message) {
        ProcessRecord proc = null;
        synchronized (this.mService.mPidsSelfLocked) {
            for (int i = 0; i < this.mService.mPidsSelfLocked.size(); i++) {
                ProcessRecord p = (ProcessRecord) this.mService.mPidsSelfLocked.valueAt(i);
                if (uid < 0 || p.uid == uid) {
                    if (p.pid == initialPid) {
                        proc = p;
                        break;
                    } else if (p.pkgList.containsKey(packageName) && (userId < 0 || p.userId == userId)) {
                        proc = p;
                    }
                }
            }
        }
        if (proc == null) {
            Slog.w(TAG, "crashApplication: nothing for uid=" + uid + " initialPid=" + initialPid + " packageName=" + packageName + " userId=" + userId);
        } else {
            proc.scheduleCrash(message);
        }
    }

    void crashApplication(ProcessRecord r, CrashInfo crashInfo) {
        int callingPid = Binder.getCallingPid();
        int callingUid = Binder.getCallingUid();
        long origId = Binder.clearCallingIdentity();
        try {
            crashApplicationInner(r, crashInfo, callingPid, callingUid);
        } finally {
            Binder.restoreCallingIdentity(origId);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void crashApplicationInner(com.android.server.am.ProcessRecord r42, android.app.ApplicationErrorReport.CrashInfo r43, int r44, int r45) {
        /*
        r41 = this;
        r10 = java.lang.System.currentTimeMillis();
        r0 = r43;
        r7 = r0.exceptionClassName;
        r0 = r43;
        r8 = r0.exceptionMessage;
        r0 = r43;
        r9 = r0.stackTrace;
        if (r7 == 0) goto L_0x0063;
    L_0x0012:
        if (r8 == 0) goto L_0x0063;
    L_0x0014:
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r4 = r4.append(r7);
        r5 = ": ";
        r4 = r4.append(r5);
        r4 = r4.append(r8);
        r8 = r4.toString();
    L_0x002c:
        if (r42 == 0) goto L_0x003f;
    L_0x002e:
        r0 = r42;
        r4 = r0.persistent;
        if (r4 == 0) goto L_0x003f;
    L_0x0034:
        r0 = r41;
        r4 = r0.mContext;
        r0 = r42;
        r5 = r0.uid;
        com.android.server.RescueParty.notePersistentAppCrash(r4, r5);
    L_0x003f:
        r39 = new com.android.server.am.AppErrorResult;
        r39.<init>();
        r0 = r41;
        r0 = r0.mService;
        r18 = r0;
        monitor-enter(r18);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x01ac }
        r4 = r41;
        r5 = r42;
        r6 = r43;
        r12 = r44;
        r13 = r45;
        r4 = r4.handleAppCrashInActivityController(r5, r6, r7, r8, r9, r10, r12, r13);	 Catch:{ all -> 0x01ac }
        if (r4 == 0) goto L_0x0067;
    L_0x005e:
        monitor-exit(r18);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0063:
        if (r7 == 0) goto L_0x002c;
    L_0x0065:
        r8 = r7;
        goto L_0x002c;
    L_0x0067:
        if (r42 == 0) goto L_0x0074;
    L_0x0069:
        r0 = r42;
        r4 = r0.instr;	 Catch:{ all -> 0x01ac }
        if (r4 == 0) goto L_0x0074;
    L_0x006f:
        monitor-exit(r18);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0074:
        if (r42 == 0) goto L_0x0087;
    L_0x0076:
        r0 = r41;
        r4 = r0.mService;	 Catch:{ all -> 0x01ac }
        r4 = r4.mBatteryStatsService;	 Catch:{ all -> 0x01ac }
        r0 = r42;
        r5 = r0.processName;	 Catch:{ all -> 0x01ac }
        r0 = r42;
        r6 = r0.uid;	 Catch:{ all -> 0x01ac }
        r4.noteProcessCrash(r5, r6);	 Catch:{ all -> 0x01ac }
    L_0x0087:
        r17 = new com.android.server.am.AppErrorDialog$Data;	 Catch:{ all -> 0x01ac }
        r17.<init>();	 Catch:{ all -> 0x01ac }
        r0 = r39;
        r1 = r17;
        r1.result = r0;	 Catch:{ all -> 0x01ac }
        r0 = r42;
        r1 = r17;
        r1.proc = r0;	 Catch:{ all -> 0x01ac }
        if (r42 == 0) goto L_0x00aa;
    L_0x009a:
        r12 = r41;
        r13 = r42;
        r14 = r7;
        r15 = r8;
        r16 = r9;
        r4 = r12.makeAppCrashingLocked(r13, r14, r15, r16, r17);	 Catch:{ all -> 0x01ac }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x00af;
    L_0x00aa:
        monitor-exit(r18);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x00af:
        r35 = android.os.Message.obtain();	 Catch:{ all -> 0x01ac }
        r4 = 1;
        r0 = r35;
        r0.what = r4;	 Catch:{ all -> 0x01ac }
        r0 = r17;
        r0 = r0.task;	 Catch:{ all -> 0x01ac }
        r40 = r0;
        r0 = r17;
        r1 = r35;
        r1.obj = r0;	 Catch:{ all -> 0x01ac }
        r0 = r41;
        r4 = r0.mService;	 Catch:{ all -> 0x01ac }
        r4 = r4.mUiHandler;	 Catch:{ all -> 0x01ac }
        r0 = r35;
        r4.sendMessage(r0);	 Catch:{ all -> 0x01ac }
        monitor-exit(r18);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        r38 = r39.get();
        r31 = 0;
        r0 = r41;
        r4 = r0.mContext;
        r5 = 316; // 0x13c float:4.43E-43 double:1.56E-321;
        r0 = r38;
        com.android.internal.logging.MetricsLogger.action(r4, r5, r0);
        r4 = 6;
        r0 = r38;
        if (r0 == r4) goto L_0x00ee;
    L_0x00e9:
        r4 = 7;
        r0 = r38;
        if (r0 != r4) goto L_0x00f0;
    L_0x00ee:
        r38 = 1;
    L_0x00f0:
        r0 = r41;
        r5 = r0.mService;
        monitor-enter(r5);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0202 }
        r4 = 5;
        r0 = r38;
        if (r0 != r4) goto L_0x0100;
    L_0x00fd:
        r41.stopReportingCrashesLocked(r42);	 Catch:{ all -> 0x0202 }
    L_0x0100:
        r4 = 3;
        r0 = r38;
        if (r0 != r4) goto L_0x0128;
    L_0x0105:
        r0 = r41;
        r4 = r0.mService;	 Catch:{ all -> 0x0202 }
        r6 = "crash";
        r12 = 0;
        r13 = 1;
        r0 = r42;
        r4.removeProcessLocked(r0, r12, r13, r6);	 Catch:{ all -> 0x0202 }
        if (r40 == 0) goto L_0x0128;
    L_0x0115:
        r0 = r41;
        r4 = r0.mService;	 Catch:{ IllegalArgumentException -> 0x01b2 }
        r0 = r40;
        r6 = r0.taskId;	 Catch:{ IllegalArgumentException -> 0x01b2 }
        r12 = android.app.ActivityOptions.makeBasic();	 Catch:{ IllegalArgumentException -> 0x01b2 }
        r12 = r12.toBundle();	 Catch:{ IllegalArgumentException -> 0x01b2 }
        r4.startActivityFromRecents(r6, r12);	 Catch:{ IllegalArgumentException -> 0x01b2 }
    L_0x0128:
        r4 = 1;
        r0 = r38;
        if (r0 != r4) goto L_0x015c;
    L_0x012d:
        r36 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x0202 }
        r0 = r41;
        r4 = r0.mService;	 Catch:{ all -> 0x0208 }
        r4 = r4.mStackSupervisor;	 Catch:{ all -> 0x0208 }
        r0 = r42;
        r4.handleAppCrashLocked(r0);	 Catch:{ all -> 0x0208 }
        r0 = r42;
        r4 = r0.persistent;	 Catch:{ all -> 0x0208 }
        if (r4 != 0) goto L_0x0159;
    L_0x0142:
        r0 = r41;
        r4 = r0.mService;	 Catch:{ all -> 0x0208 }
        r6 = "crash";
        r12 = 0;
        r13 = 0;
        r0 = r42;
        r4.removeProcessLocked(r0, r12, r13, r6);	 Catch:{ all -> 0x0208 }
        r0 = r41;
        r4 = r0.mService;	 Catch:{ all -> 0x0208 }
        r4 = r4.mStackSupervisor;	 Catch:{ all -> 0x0208 }
        r4.resumeFocusedStackTopActivityLocked();	 Catch:{ all -> 0x0208 }
    L_0x0159:
        android.os.Binder.restoreCallingIdentity(r36);	 Catch:{ all -> 0x0202 }
    L_0x015c:
        r4 = 2;
        r0 = r38;
        if (r0 != r4) goto L_0x016b;
    L_0x0161:
        r0 = r41;
        r1 = r42;
        r2 = r43;
        r31 = r0.createAppErrorIntentLocked(r1, r10, r2);	 Catch:{ all -> 0x0202 }
    L_0x016b:
        if (r42 == 0) goto L_0x0193;
    L_0x016d:
        r0 = r42;
        r4 = r0.isolated;	 Catch:{ all -> 0x0202 }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x0193;
    L_0x0175:
        r4 = 3;
        r0 = r38;
        if (r0 == r4) goto L_0x0193;
    L_0x017a:
        r0 = r41;
        r4 = r0.mProcessCrashTimes;	 Catch:{ all -> 0x0202 }
        r0 = r42;
        r6 = r0.info;	 Catch:{ all -> 0x0202 }
        r6 = r6.processName;	 Catch:{ all -> 0x0202 }
        r0 = r42;
        r12 = r0.uid;	 Catch:{ all -> 0x0202 }
        r14 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x0202 }
        r13 = java.lang.Long.valueOf(r14);	 Catch:{ all -> 0x0202 }
        r4.put(r6, r12, r13);	 Catch:{ all -> 0x0202 }
    L_0x0193:
        monitor-exit(r5);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        if (r31 == 0) goto L_0x01ab;
    L_0x0199:
        r0 = r41;
        r4 = r0.mContext;	 Catch:{ ActivityNotFoundException -> 0x020d }
        r5 = new android.os.UserHandle;	 Catch:{ ActivityNotFoundException -> 0x020d }
        r0 = r42;
        r6 = r0.userId;	 Catch:{ ActivityNotFoundException -> 0x020d }
        r5.<init>(r6);	 Catch:{ ActivityNotFoundException -> 0x020d }
        r0 = r31;
        r4.startActivityAsUser(r0, r5);	 Catch:{ ActivityNotFoundException -> 0x020d }
    L_0x01ab:
        return;
    L_0x01ac:
        r4 = move-exception;
        monitor-exit(r18);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r4;
    L_0x01b2:
        r34 = move-exception;
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x0202 }
        r32 = r4.getCategories();	 Catch:{ all -> 0x0202 }
        if (r32 == 0) goto L_0x0128;
    L_0x01bd:
        r4 = "android.intent.category.LAUNCHER";
        r0 = r32;
        r4 = r0.contains(r4);	 Catch:{ all -> 0x0202 }
        if (r4 == 0) goto L_0x0128;
    L_0x01c8:
        r0 = r41;
        r0 = r0.mService;	 Catch:{ all -> 0x0202 }
        r18 = r0;
        r0 = r40;
        r0 = r0.mCallingUid;	 Catch:{ all -> 0x0202 }
        r19 = r0;
        r0 = r40;
        r0 = r0.mCallingPackage;	 Catch:{ all -> 0x0202 }
        r20 = r0;
        r0 = r40;
        r0 = r0.intent;	 Catch:{ all -> 0x0202 }
        r21 = r0;
        r4 = android.app.ActivityOptions.makeBasic();	 Catch:{ all -> 0x0202 }
        r27 = r4.toBundle();	 Catch:{ all -> 0x0202 }
        r0 = r40;
        r0 = r0.userId;	 Catch:{ all -> 0x0202 }
        r28 = r0;
        r30 = "AppErrors";
        r22 = 0;
        r23 = 0;
        r24 = 0;
        r25 = 0;
        r26 = 0;
        r29 = 0;
        r18.startActivityInPackage(r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30);	 Catch:{ all -> 0x0202 }
        goto L_0x0128;
    L_0x0202:
        r4 = move-exception;
        monitor-exit(r5);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r4;
    L_0x0208:
        r4 = move-exception;
        android.os.Binder.restoreCallingIdentity(r36);	 Catch:{ all -> 0x0202 }
        throw r4;	 Catch:{ all -> 0x0202 }
    L_0x020d:
        r33 = move-exception;
        r4 = TAG;
        r5 = "bug report receiver dissappeared";
        r0 = r33;
        android.util.Slog.w(r4, r5, r0);
        goto L_0x01ab;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.AppErrors.crashApplicationInner(com.android.server.am.ProcessRecord, android.app.ApplicationErrorReport$CrashInfo, int, int):void");
    }

    private boolean handleAppCrashInActivityController(ProcessRecord r, CrashInfo crashInfo, String shortMsg, String longMsg, String stackTrace, long timeMillis, int callingPid, int callingUid) {
        if (this.mService.mController == null) {
            return false;
        }
        String str;
        if (r != null) {
            try {
                str = r.processName;
            } catch (RemoteException e) {
                this.mService.mController = null;
                Watchdog.getInstance().setActivityController(null);
            }
        } else {
            str = null;
        }
        int pid = r != null ? r.pid : callingPid;
        int uid = r != null ? r.info.uid : callingUid;
        if (!this.mService.mController.appCrashed(str, pid, shortMsg, longMsg, timeMillis, crashInfo.stackTrace)) {
            if ("1".equals(SystemProperties.get("ro.debuggable", "0")) && "Native crash".equals(crashInfo.exceptionClassName)) {
                Slog.w(TAG, "Skip killing native crashed app " + str + "(" + pid + ") during testing");
            } else {
                Slog.w(TAG, "Force-killing crashed app " + str + " at watcher's request");
                if (r == null) {
                    Process.killProcess(pid);
                    ActivityManagerService.killProcessGroup(uid, pid);
                } else if (!makeAppCrashingLocked(r, shortMsg, longMsg, stackTrace, null)) {
                    r.kill("crash", true);
                }
            }
            return true;
        }
        return false;
    }

    private boolean makeAppCrashingLocked(ProcessRecord app, String shortMsg, String longMsg, String stackTrace, Data data) {
        app.crashing = true;
        app.crashingReport = generateProcessError(app, 1, null, shortMsg, longMsg, stackTrace);
        startAppProblemLocked(app);
        app.stopFreezingAllLocked();
        return handleAppCrashLocked(app, "force-crash", shortMsg, longMsg, stackTrace, data);
    }

    void startAppProblemLocked(ProcessRecord app) {
        app.errorReportReceiver = null;
        for (int userId : this.mService.mUserController.getCurrentProfileIdsLocked()) {
            if (app.userId == userId) {
                app.errorReportReceiver = ApplicationErrorReport.getErrorReportReceiver(this.mContext, app.info.packageName, app.info.flags);
            }
        }
        this.mService.skipCurrentReceiverLocked(app);
    }

    private ProcessErrorStateInfo generateProcessError(ProcessRecord app, int condition, String activity, String shortMsg, String longMsg, String stackTrace) {
        ProcessErrorStateInfo report = new ProcessErrorStateInfo();
        report.condition = condition;
        report.processName = app.processName;
        report.pid = app.pid;
        report.uid = app.info.uid;
        report.tag = activity;
        report.shortMsg = shortMsg;
        report.longMsg = longMsg;
        report.stackTrace = stackTrace;
        return report;
    }

    Intent createAppErrorIntentLocked(ProcessRecord r, long timeMillis, CrashInfo crashInfo) {
        ApplicationErrorReport report = createAppErrorReportLocked(r, timeMillis, crashInfo);
        if (report == null) {
            return null;
        }
        Intent result = new Intent("android.intent.action.APP_ERROR");
        result.setComponent(r.errorReportReceiver);
        result.putExtra("android.intent.extra.BUG_REPORT", report);
        result.addFlags(268435456);
        return result;
    }

    private ApplicationErrorReport createAppErrorReportLocked(ProcessRecord r, long timeMillis, CrashInfo crashInfo) {
        boolean z = false;
        if (r.errorReportReceiver == null) {
            return null;
        }
        if (!r.crashing && (r.notResponding ^ 1) != 0 && (r.forceCrashReport ^ 1) != 0) {
            return null;
        }
        ApplicationErrorReport report = new ApplicationErrorReport();
        report.packageName = r.info.packageName;
        report.installerPackageName = r.errorReportReceiver.getPackageName();
        report.processName = r.processName;
        report.time = timeMillis;
        if ((r.info.flags & 1) != 0) {
            z = true;
        }
        report.systemApp = z;
        if (r.crashing || r.forceCrashReport) {
            report.type = 1;
            report.crashInfo = crashInfo;
        } else if (r.notResponding) {
            report.type = 2;
            report.anrInfo = new AnrInfo();
            report.anrInfo.activity = r.notRespondingReport.tag;
            report.anrInfo.cause = r.notRespondingReport.shortMsg;
            report.anrInfo.info = r.notRespondingReport.longMsg;
        }
        return report;
    }

    boolean handleAppCrashLocked(ProcessRecord app, String reason, String shortMsg, String longMsg, String stackTrace, Data data) {
        Long crashTime;
        long now = SystemClock.uptimeMillis();
        boolean showBackground = Secure.getInt(this.mContext.getContentResolver(), "anr_show_background", 0) != 0;
        boolean procIsBoundForeground = app.curProcState == 3;
        boolean tryAgain = false;
        Long crashTimePersistent;
        if (app.isolated) {
            crashTimePersistent = null;
            crashTime = null;
        } else {
            crashTime = (Long) this.mProcessCrashTimes.get(app.info.processName, app.uid);
            crashTimePersistent = (Long) this.mProcessCrashTimesPersistent.get(app.info.processName, app.uid);
        }
        for (int i = app.services.size() - 1; i >= 0; i--) {
            ServiceRecord sr = (ServiceRecord) app.services.valueAt(i);
            if (now > sr.restartTime + 60000) {
                sr.crashCount = 1;
            } else {
                sr.crashCount++;
            }
            if (((long) sr.crashCount) < this.mService.mConstants.BOUND_SERVICE_MAX_CRASH_RETRY && (sr.isForeground || procIsBoundForeground)) {
                tryAgain = true;
            }
        }
        if (crashTime == null || now >= crashTime.longValue() + 60000) {
            TaskRecord affectedTask = this.mService.mStackSupervisor.finishTopRunningActivityLocked(app, reason);
            if (data != null) {
                data.task = affectedTask;
            }
            if (!(data == null || r15 == null || now >= r15.longValue() + 60000)) {
                data.repeating = true;
            }
        } else {
            Slog.w(TAG, "Process " + app.info.processName + " has crashed too many times: killing!");
            EventLog.writeEvent(EventLogTags.AM_PROCESS_CRASHED_TOO_MUCH, new Object[]{Integer.valueOf(app.userId), app.info.processName, Integer.valueOf(app.uid)});
            this.mService.mStackSupervisor.handleAppCrashLocked(app);
            if (!app.persistent) {
                EventLog.writeEvent(EventLogTags.AM_PROC_BAD, new Object[]{Integer.valueOf(app.userId), Integer.valueOf(app.uid), app.info.processName});
                if (!app.isolated) {
                    this.mBadProcesses.put(app.info.processName, app.uid, new BadProcessInfo(now, shortMsg, longMsg, stackTrace));
                    this.mProcessCrashTimes.remove(app.info.processName, app.uid);
                }
                app.bad = true;
                app.removed = true;
                this.mService.removeProcessLocked(app, false, tryAgain, "crash");
                this.mService.mStackSupervisor.resumeFocusedStackTopActivityLocked();
                if (!showBackground) {
                    return false;
                }
            }
            this.mService.mStackSupervisor.resumeFocusedStackTopActivityLocked();
        }
        if (data != null && tryAgain) {
            data.isRestartableForService = true;
        }
        ArrayList<ActivityRecord> activities = app.activities;
        if (app == this.mService.mHomeProcess && activities.size() > 0 && (this.mService.mHomeProcess.info.flags & 1) == 0) {
            for (int activityNdx = activities.size() - 1; activityNdx >= 0; activityNdx--) {
                ActivityRecord r = (ActivityRecord) activities.get(activityNdx);
                if (r.isHomeActivity()) {
                    Log.i(TAG, "Clearing package preferred activities from " + r.packageName);
                    try {
                        ActivityThread.getPackageManager().clearPackagePreferredActivities(r.packageName);
                    } catch (RemoteException e) {
                    }
                }
            }
        }
        if (!app.isolated) {
            this.mProcessCrashTimes.put(app.info.processName, app.uid, Long.valueOf(now));
            this.mProcessCrashTimesPersistent.put(app.info.processName, app.uid, Long.valueOf(now));
        }
        if (app.crashHandler != null) {
            this.mService.mHandler.post(app.crashHandler);
        }
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void handleShowAppErrorUi(android.os.Message r14) {
        /*
        r13 = this;
        r8 = 0;
        r1 = r14.obj;
        r1 = (com.android.server.am.AppErrorDialog.Data) r1;
        r7 = r13.mContext;
        r7 = r7.getContentResolver();
        r9 = "anr_show_background";
        r7 = android.provider.Settings.Secure.getInt(r7, r9, r8);
        if (r7 == 0) goto L_0x004a;
    L_0x0014:
        r5 = 1;
    L_0x0015:
        r10 = r13.mService;
        monitor-enter(r10);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x011e }
        r3 = r1.proc;	 Catch:{ all -> 0x011e }
        r4 = r1.result;	 Catch:{ all -> 0x011e }
        if (r3 == 0) goto L_0x004c;
    L_0x0021:
        r7 = r3.crashDialog;	 Catch:{ all -> 0x011e }
        if (r7 == 0) goto L_0x004c;
    L_0x0025:
        r7 = TAG;	 Catch:{ all -> 0x011e }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x011e }
        r8.<init>();	 Catch:{ all -> 0x011e }
        r9 = "App already has crash dialog: ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x011e }
        r8 = r8.append(r3);	 Catch:{ all -> 0x011e }
        r8 = r8.toString();	 Catch:{ all -> 0x011e }
        android.util.Slog.e(r7, r8);	 Catch:{ all -> 0x011e }
        if (r4 == 0) goto L_0x0045;
    L_0x0040:
        r7 = com.android.server.am.AppErrorDialog.ALREADY_SHOWING;	 Catch:{ all -> 0x011e }
        r4.set(r7);	 Catch:{ all -> 0x011e }
    L_0x0045:
        monitor-exit(r10);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x004a:
        r5 = 0;
        goto L_0x0015;
    L_0x004c:
        r7 = r3.uid;	 Catch:{ all -> 0x011e }
        r7 = android.os.UserHandle.getAppId(r7);	 Catch:{ all -> 0x011e }
        r9 = 10000; // 0x2710 float:1.4013E-41 double:4.9407E-320;
        if (r7 < r9) goto L_0x0077;
    L_0x0056:
        r7 = r3.pid;	 Catch:{ all -> 0x011e }
        r9 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x011e }
        if (r7 == r9) goto L_0x0075;
    L_0x005c:
        r2 = 1;
    L_0x005d:
        r7 = r13.mService;	 Catch:{ all -> 0x011e }
        r7 = r7.mUserController;	 Catch:{ all -> 0x011e }
        r11 = r7.getCurrentProfileIdsLocked();	 Catch:{ all -> 0x011e }
        r12 = r11.length;	 Catch:{ all -> 0x011e }
        r9 = r8;
    L_0x0067:
        if (r9 >= r12) goto L_0x007b;
    L_0x0069:
        r6 = r11[r9];	 Catch:{ all -> 0x011e }
        r7 = r3.userId;	 Catch:{ all -> 0x011e }
        if (r7 == r6) goto L_0x0079;
    L_0x006f:
        r7 = 1;
    L_0x0070:
        r2 = r2 & r7;
        r7 = r9 + 1;
        r9 = r7;
        goto L_0x0067;
    L_0x0075:
        r2 = 0;
        goto L_0x005d;
    L_0x0077:
        r2 = 0;
        goto L_0x005d;
    L_0x0079:
        r7 = r8;
        goto L_0x0070;
    L_0x007b:
        if (r2 == 0) goto L_0x00ad;
    L_0x007d:
        r7 = r5 ^ 1;
        if (r7 == 0) goto L_0x00ad;
    L_0x0081:
        r7 = TAG;	 Catch:{ all -> 0x011e }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x011e }
        r8.<init>();	 Catch:{ all -> 0x011e }
        r9 = "Skipping crash dialog of ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x011e }
        r8 = r8.append(r3);	 Catch:{ all -> 0x011e }
        r9 = ": background";
        r8 = r8.append(r9);	 Catch:{ all -> 0x011e }
        r8 = r8.toString();	 Catch:{ all -> 0x011e }
        android.util.Slog.w(r7, r8);	 Catch:{ all -> 0x011e }
        if (r4 == 0) goto L_0x00a8;
    L_0x00a3:
        r7 = com.android.server.am.AppErrorDialog.BACKGROUND_USER;	 Catch:{ all -> 0x011e }
        r4.set(r7);	 Catch:{ all -> 0x011e }
    L_0x00a8:
        monitor-exit(r10);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x00ad:
        r7 = r13.mAppsNotReportingCrashes;	 Catch:{ all -> 0x011e }
        if (r7 == 0) goto L_0x0114;
    L_0x00b1:
        r7 = r13.mAppsNotReportingCrashes;	 Catch:{ all -> 0x011e }
        r8 = r3.info;	 Catch:{ all -> 0x011e }
        r8 = r8.packageName;	 Catch:{ all -> 0x011e }
        r0 = r7.contains(r8);	 Catch:{ all -> 0x011e }
    L_0x00bb:
        r7 = r13.mService;	 Catch:{ all -> 0x011e }
        r7 = r7.canShowErrorDialogs();	 Catch:{ all -> 0x011e }
        if (r7 != 0) goto L_0x00c5;
    L_0x00c3:
        if (r5 == 0) goto L_0x0116;
    L_0x00c5:
        r7 = r0 ^ 1;
        if (r7 == 0) goto L_0x0116;
    L_0x00c9:
        r7 = new com.android.server.am.AppErrorDialog;	 Catch:{ all -> 0x011e }
        r8 = r13.mContext;	 Catch:{ all -> 0x011e }
        r9 = r13.mService;	 Catch:{ all -> 0x011e }
        r7.<init>(r8, r9, r1);	 Catch:{ all -> 0x011e }
        r3.crashDialog = r7;	 Catch:{ all -> 0x011e }
    L_0x00d4:
        monitor-exit(r10);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        r7 = r1.proc;
        r7 = r7.crashDialog;
        if (r7 == 0) goto L_0x0113;
    L_0x00de:
        r7 = TAG;
        r8 = new java.lang.StringBuilder;
        r8.<init>();
        r9 = "Showing crash dialog for package ";
        r8 = r8.append(r9);
        r9 = r1.proc;
        r9 = r9.info;
        r9 = r9.packageName;
        r8 = r8.append(r9);
        r9 = " u";
        r8 = r8.append(r9);
        r9 = r1.proc;
        r9 = r9.userId;
        r8 = r8.append(r9);
        r8 = r8.toString();
        android.util.Slog.i(r7, r8);
        r7 = r1.proc;
        r7 = r7.crashDialog;
        r7.show();
    L_0x0113:
        return;
    L_0x0114:
        r0 = 0;
        goto L_0x00bb;
    L_0x0116:
        if (r4 == 0) goto L_0x00d4;
    L_0x0118:
        r7 = com.android.server.am.AppErrorDialog.CANT_SHOW;	 Catch:{ all -> 0x011e }
        r4.set(r7);	 Catch:{ all -> 0x011e }
        goto L_0x00d4;
    L_0x011e:
        r7 = move-exception;
        monitor-exit(r10);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r7;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.AppErrors.handleShowAppErrorUi(android.os.Message):void");
    }

    void stopReportingCrashesLocked(ProcessRecord proc) {
        if (this.mAppsNotReportingCrashes == null) {
            this.mAppsNotReportingCrashes = new ArraySet();
        }
        this.mAppsNotReportingCrashes.add(proc.info.packageName);
    }

    static boolean isInterestingForBackgroundTraces(ProcessRecord app) {
        boolean z = true;
        if (app.pid == ActivityManagerService.MY_PID) {
            return true;
        }
        if (!app.isInterestingToUserLocked() && ((app.info == null || !"com.android.systemui".equals(app.info.packageName)) && !app.hasTopUi)) {
            z = app.hasOverlayUi;
        }
        return z;
    }

    final void appNotResponding(com.android.server.am.ProcessRecord r36, com.android.server.am.ActivityRecord r37, com.android.server.am.ActivityRecord r38, boolean r39, java.lang.String r40) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r19_0 'lastPids' android.util.SparseArray<java.lang.Boolean>) in PHI: PHI: (r19_2 android.util.SparseArray<java.lang.Boolean>) = (r19_0 'lastPids' android.util.SparseArray<java.lang.Boolean>), (r19_1 'lastPids' android.util.SparseArray<java.lang.Boolean>) binds: {(r19_0 'lastPids' android.util.SparseArray<java.lang.Boolean>)=B:134:0x0375, (r19_1 'lastPids' android.util.SparseArray<java.lang.Boolean>)=B:135:0x0377}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:79)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:60)
	at jadx.core.ProcessClass.process(ProcessClass.java:39)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r35 = this;
        r15 = new java.util.ArrayList;
        r2 = 5;
        r15.<init>(r2);
        r19 = new android.util.SparseArray;
        r2 = 20;
        r0 = r19;
        r0.<init>(r2);
        r0 = r35;
        r2 = r0.mService;
        r2 = r2.mController;
        if (r2 == 0) goto L_0x003e;
    L_0x0017:
        r0 = r35;	 Catch:{ RemoteException -> 0x009a }
        r2 = r0.mService;	 Catch:{ RemoteException -> 0x009a }
        r2 = r2.mController;	 Catch:{ RemoteException -> 0x009a }
        r0 = r36;	 Catch:{ RemoteException -> 0x009a }
        r3 = r0.processName;	 Catch:{ RemoteException -> 0x009a }
        r0 = r36;	 Catch:{ RemoteException -> 0x009a }
        r4 = r0.pid;	 Catch:{ RemoteException -> 0x009a }
        r0 = r40;	 Catch:{ RemoteException -> 0x009a }
        r31 = r2.appEarlyNotResponding(r3, r4, r0);	 Catch:{ RemoteException -> 0x009a }
        if (r31 >= 0) goto L_0x003e;	 Catch:{ RemoteException -> 0x009a }
    L_0x002d:
        r0 = r36;	 Catch:{ RemoteException -> 0x009a }
        r2 = r0.pid;	 Catch:{ RemoteException -> 0x009a }
        r3 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ RemoteException -> 0x009a }
        if (r2 == r3) goto L_0x003e;	 Catch:{ RemoteException -> 0x009a }
    L_0x0035:
        r2 = "anr";	 Catch:{ RemoteException -> 0x009a }
        r3 = 1;	 Catch:{ RemoteException -> 0x009a }
        r0 = r36;	 Catch:{ RemoteException -> 0x009a }
        r0.kill(r2, r3);	 Catch:{ RemoteException -> 0x009a }
    L_0x003e:
        r12 = android.os.SystemClock.uptimeMillis();
        r0 = r35;
        r2 = r0.mService;
        r2.updateCpuStatsNow();
        r0 = r35;
        r2 = r0.mContext;
        r2 = r2.getContentResolver();
        r3 = "anr_show_background";
        r4 = 0;
        r2 = android.provider.Settings.Secure.getInt(r2, r3, r4);
        if (r2 == 0) goto L_0x00ab;
    L_0x005b:
        r32 = 1;
    L_0x005d:
        r0 = r35;
        r3 = r0.mService;
        monitor-enter(r3);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x026f }
        r0 = r35;	 Catch:{ all -> 0x026f }
        r2 = r0.mService;	 Catch:{ all -> 0x026f }
        r2 = r2.mShuttingDown;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x00ae;	 Catch:{ all -> 0x026f }
    L_0x006d:
        r2 = TAG;	 Catch:{ all -> 0x026f }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x026f }
        r4.<init>();	 Catch:{ all -> 0x026f }
        r5 = "During shutdown skipping ANR: ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r5 = " ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r40;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r4 = r4.toString();	 Catch:{ all -> 0x026f }
        android.util.Slog.i(r2, r4);	 Catch:{ all -> 0x026f }
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x009a:
        r14 = move-exception;
        r0 = r35;
        r2 = r0.mService;
        r3 = 0;
        r2.mController = r3;
        r2 = com.android.server.Watchdog.getInstance();
        r3 = 0;
        r2.setActivityController(r3);
        goto L_0x003e;
    L_0x00ab:
        r32 = 0;
        goto L_0x005d;
    L_0x00ae:
        r0 = r36;	 Catch:{ all -> 0x026f }
        r2 = r0.notResponding;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x00e1;	 Catch:{ all -> 0x026f }
    L_0x00b4:
        r2 = TAG;	 Catch:{ all -> 0x026f }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x026f }
        r4.<init>();	 Catch:{ all -> 0x026f }
        r5 = "Skipping duplicate ANR: ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r5 = " ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r40;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r4 = r4.toString();	 Catch:{ all -> 0x026f }
        android.util.Slog.i(r2, r4);	 Catch:{ all -> 0x026f }
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x00e1:
        r0 = r36;	 Catch:{ all -> 0x026f }
        r2 = r0.crashing;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x0114;	 Catch:{ all -> 0x026f }
    L_0x00e7:
        r2 = TAG;	 Catch:{ all -> 0x026f }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x026f }
        r4.<init>();	 Catch:{ all -> 0x026f }
        r5 = "Crashing app skipping ANR: ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r5 = " ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r40;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r4 = r4.toString();	 Catch:{ all -> 0x026f }
        android.util.Slog.i(r2, r4);	 Catch:{ all -> 0x026f }
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0114:
        r0 = r36;	 Catch:{ all -> 0x026f }
        r2 = r0.killedByAm;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x0147;	 Catch:{ all -> 0x026f }
    L_0x011a:
        r2 = TAG;	 Catch:{ all -> 0x026f }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x026f }
        r4.<init>();	 Catch:{ all -> 0x026f }
        r5 = "App already killed by AM skipping ANR: ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r5 = " ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r40;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r4 = r4.toString();	 Catch:{ all -> 0x026f }
        android.util.Slog.i(r2, r4);	 Catch:{ all -> 0x026f }
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0147:
        r0 = r36;	 Catch:{ all -> 0x026f }
        r2 = r0.killed;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x017a;	 Catch:{ all -> 0x026f }
    L_0x014d:
        r2 = TAG;	 Catch:{ all -> 0x026f }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x026f }
        r4.<init>();	 Catch:{ all -> 0x026f }
        r5 = "Skipping died app ANR: ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r5 = " ";	 Catch:{ all -> 0x026f }
        r4 = r4.append(r5);	 Catch:{ all -> 0x026f }
        r0 = r40;	 Catch:{ all -> 0x026f }
        r4 = r4.append(r0);	 Catch:{ all -> 0x026f }
        r4 = r4.toString();	 Catch:{ all -> 0x026f }
        android.util.Slog.i(r2, r4);	 Catch:{ all -> 0x026f }
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x017a:
        r2 = 1;
        r0 = r36;	 Catch:{ all -> 0x026f }
        r0.notResponding = r2;	 Catch:{ all -> 0x026f }
        r2 = 5;	 Catch:{ all -> 0x026f }
        r2 = new java.lang.Object[r2];	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r0.userId;	 Catch:{ all -> 0x026f }
        r4 = java.lang.Integer.valueOf(r4);	 Catch:{ all -> 0x026f }
        r5 = 0;	 Catch:{ all -> 0x026f }
        r2[r5] = r4;	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r0.pid;	 Catch:{ all -> 0x026f }
        r4 = java.lang.Integer.valueOf(r4);	 Catch:{ all -> 0x026f }
        r5 = 1;	 Catch:{ all -> 0x026f }
        r2[r5] = r4;	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r0.processName;	 Catch:{ all -> 0x026f }
        r5 = 2;	 Catch:{ all -> 0x026f }
        r2[r5] = r4;	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r0.info;	 Catch:{ all -> 0x026f }
        r4 = r4.flags;	 Catch:{ all -> 0x026f }
        r4 = java.lang.Integer.valueOf(r4);	 Catch:{ all -> 0x026f }
        r5 = 3;	 Catch:{ all -> 0x026f }
        r2[r5] = r4;	 Catch:{ all -> 0x026f }
        r4 = 4;	 Catch:{ all -> 0x026f }
        r2[r4] = r40;	 Catch:{ all -> 0x026f }
        r4 = 30008; // 0x7538 float:4.205E-41 double:1.4826E-319;	 Catch:{ all -> 0x026f }
        android.util.EventLog.writeEvent(r4, r2);	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r2 = r0.pid;	 Catch:{ all -> 0x026f }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ all -> 0x026f }
        r15.add(r2);	 Catch:{ all -> 0x026f }
        if (r32 != 0) goto L_0x025d;	 Catch:{ all -> 0x026f }
    L_0x01c1:
        r2 = isInterestingForBackgroundTraces(r36);	 Catch:{ all -> 0x026f }
        r18 = r2 ^ 1;	 Catch:{ all -> 0x026f }
    L_0x01c7:
        if (r18 != 0) goto L_0x027f;	 Catch:{ all -> 0x026f }
    L_0x01c9:
        r0 = r36;	 Catch:{ all -> 0x026f }
        r0 = r0.pid;	 Catch:{ all -> 0x026f }
        r26 = r0;	 Catch:{ all -> 0x026f }
        if (r38 == 0) goto L_0x01e7;	 Catch:{ all -> 0x026f }
    L_0x01d1:
        r0 = r38;	 Catch:{ all -> 0x026f }
        r2 = r0.app;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x01e7;	 Catch:{ all -> 0x026f }
    L_0x01d7:
        r0 = r38;	 Catch:{ all -> 0x026f }
        r2 = r0.app;	 Catch:{ all -> 0x026f }
        r2 = r2.pid;	 Catch:{ all -> 0x026f }
        if (r2 <= 0) goto L_0x01e7;	 Catch:{ all -> 0x026f }
    L_0x01df:
        r0 = r38;	 Catch:{ all -> 0x026f }
        r2 = r0.app;	 Catch:{ all -> 0x026f }
        r0 = r2.pid;	 Catch:{ all -> 0x026f }
        r26 = r0;	 Catch:{ all -> 0x026f }
    L_0x01e7:
        r0 = r36;	 Catch:{ all -> 0x026f }
        r2 = r0.pid;	 Catch:{ all -> 0x026f }
        r0 = r26;	 Catch:{ all -> 0x026f }
        if (r0 == r2) goto L_0x01f6;	 Catch:{ all -> 0x026f }
    L_0x01ef:
        r2 = java.lang.Integer.valueOf(r26);	 Catch:{ all -> 0x026f }
        r15.add(r2);	 Catch:{ all -> 0x026f }
    L_0x01f6:
        r2 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x026f }
        r0 = r36;	 Catch:{ all -> 0x026f }
        r4 = r0.pid;	 Catch:{ all -> 0x026f }
        if (r2 == r4) goto L_0x020d;	 Catch:{ all -> 0x026f }
    L_0x01fe:
        r2 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x026f }
        r0 = r26;	 Catch:{ all -> 0x026f }
        if (r2 == r0) goto L_0x020d;	 Catch:{ all -> 0x026f }
    L_0x0204:
        r2 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x026f }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ all -> 0x026f }
        r15.add(r2);	 Catch:{ all -> 0x026f }
    L_0x020d:
        r0 = r35;	 Catch:{ all -> 0x026f }
        r2 = r0.mService;	 Catch:{ all -> 0x026f }
        r2 = r2.mLruProcesses;	 Catch:{ all -> 0x026f }
        r2 = r2.size();	 Catch:{ all -> 0x026f }
        r16 = r2 + -1;	 Catch:{ all -> 0x026f }
    L_0x0219:
        if (r16 < 0) goto L_0x027f;	 Catch:{ all -> 0x026f }
    L_0x021b:
        r0 = r35;	 Catch:{ all -> 0x026f }
        r2 = r0.mService;	 Catch:{ all -> 0x026f }
        r2 = r2.mLruProcesses;	 Catch:{ all -> 0x026f }
        r0 = r16;	 Catch:{ all -> 0x026f }
        r30 = r2.get(r0);	 Catch:{ all -> 0x026f }
        r30 = (com.android.server.am.ProcessRecord) r30;	 Catch:{ all -> 0x026f }
        if (r30 == 0) goto L_0x025a;	 Catch:{ all -> 0x026f }
    L_0x022b:
        r0 = r30;	 Catch:{ all -> 0x026f }
        r2 = r0.thread;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x025a;	 Catch:{ all -> 0x026f }
    L_0x0231:
        r0 = r30;	 Catch:{ all -> 0x026f }
        r0 = r0.pid;	 Catch:{ all -> 0x026f }
        r27 = r0;	 Catch:{ all -> 0x026f }
        if (r27 <= 0) goto L_0x025a;	 Catch:{ all -> 0x026f }
    L_0x0239:
        r0 = r36;	 Catch:{ all -> 0x026f }
        r2 = r0.pid;	 Catch:{ all -> 0x026f }
        r0 = r27;	 Catch:{ all -> 0x026f }
        if (r0 == r2) goto L_0x025a;	 Catch:{ all -> 0x026f }
    L_0x0241:
        r0 = r27;	 Catch:{ all -> 0x026f }
        r1 = r26;	 Catch:{ all -> 0x026f }
        if (r0 == r1) goto L_0x025a;	 Catch:{ all -> 0x026f }
    L_0x0247:
        r2 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x026f }
        r0 = r27;	 Catch:{ all -> 0x026f }
        if (r0 == r2) goto L_0x025a;	 Catch:{ all -> 0x026f }
    L_0x024d:
        r0 = r30;	 Catch:{ all -> 0x026f }
        r2 = r0.persistent;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x0261;	 Catch:{ all -> 0x026f }
    L_0x0253:
        r2 = java.lang.Integer.valueOf(r27);	 Catch:{ all -> 0x026f }
        r15.add(r2);	 Catch:{ all -> 0x026f }
    L_0x025a:
        r16 = r16 + -1;	 Catch:{ all -> 0x026f }
        goto L_0x0219;	 Catch:{ all -> 0x026f }
    L_0x025d:
        r18 = 0;	 Catch:{ all -> 0x026f }
        goto L_0x01c7;	 Catch:{ all -> 0x026f }
    L_0x0261:
        r0 = r30;	 Catch:{ all -> 0x026f }
        r2 = r0.treatLikeActivity;	 Catch:{ all -> 0x026f }
        if (r2 == 0) goto L_0x0275;	 Catch:{ all -> 0x026f }
    L_0x0267:
        r2 = java.lang.Integer.valueOf(r27);	 Catch:{ all -> 0x026f }
        r15.add(r2);	 Catch:{ all -> 0x026f }
        goto L_0x025a;
    L_0x026f:
        r2 = move-exception;
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r2;
    L_0x0275:
        r2 = java.lang.Boolean.TRUE;	 Catch:{ all -> 0x026f }
        r0 = r19;	 Catch:{ all -> 0x026f }
        r1 = r27;	 Catch:{ all -> 0x026f }
        r0.put(r1, r2);	 Catch:{ all -> 0x026f }
        goto L_0x025a;
    L_0x027f:
        monitor-exit(r3);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        r17 = new java.lang.StringBuilder;
        r17.<init>();
        r2 = 0;
        r0 = r17;
        r0.setLength(r2);
        r2 = "ANR in ";
        r0 = r17;
        r2 = r0.append(r2);
        r0 = r36;
        r3 = r0.processName;
        r2.append(r3);
        if (r37 == 0) goto L_0x02bd;
    L_0x02a0:
        r0 = r37;
        r2 = r0.shortComponentName;
        if (r2 == 0) goto L_0x02bd;
    L_0x02a6:
        r2 = " (";
        r0 = r17;
        r2 = r0.append(r2);
        r0 = r37;
        r3 = r0.shortComponentName;
        r2 = r2.append(r3);
        r3 = ")";
        r2.append(r3);
    L_0x02bd:
        r2 = "\n";
        r0 = r17;
        r0.append(r2);
        r2 = "PID: ";
        r0 = r17;
        r2 = r0.append(r2);
        r0 = r36;
        r3 = r0.pid;
        r2 = r2.append(r3);
        r3 = "\n";
        r2.append(r3);
        if (r40 == 0) goto L_0x02f3;
    L_0x02de:
        r2 = "Reason: ";
        r0 = r17;
        r2 = r0.append(r2);
        r0 = r40;
        r2 = r2.append(r0);
        r3 = "\n";
        r2.append(r3);
    L_0x02f3:
        if (r38 == 0) goto L_0x0312;
    L_0x02f5:
        r0 = r38;
        r1 = r37;
        if (r0 == r1) goto L_0x0312;
    L_0x02fb:
        r2 = "Parent: ";
        r0 = r17;
        r2 = r0.append(r2);
        r0 = r38;
        r3 = r0.shortComponentName;
        r2 = r2.append(r3);
        r3 = "\n";
        r2.append(r3);
    L_0x0312:
        r29 = new com.android.internal.os.ProcessCpuTracker;
        r2 = 1;
        r0 = r29;
        r0.<init>(r2);
        r24 = 0;
        if (r18 == 0) goto L_0x036a;
    L_0x031e:
        r16 = 0;
    L_0x0320:
        r2 = com.android.server.Watchdog.NATIVE_STACKS_OF_INTEREST;
        r2 = r2.length;
        r0 = r16;
        if (r0 >= r2) goto L_0x0341;
    L_0x0327:
        r2 = com.android.server.Watchdog.NATIVE_STACKS_OF_INTEREST;
        r2 = r2[r16];
        r0 = r36;
        r3 = r0.processName;
        r2 = r2.equals(r3);
        if (r2 == 0) goto L_0x0367;
    L_0x0335:
        r2 = 1;
        r0 = new java.lang.String[r2];
        r24 = r0;
        r0 = r36;
        r2 = r0.processName;
        r3 = 0;
        r24[r3] = r2;
    L_0x0341:
        if (r24 != 0) goto L_0x036d;
    L_0x0343:
        r28 = 0;
    L_0x0345:
        r23 = 0;
        if (r28 == 0) goto L_0x0372;
    L_0x0349:
        r23 = new java.util.ArrayList;
        r0 = r28;
        r2 = r0.length;
        r0 = r23;
        r0.<init>(r2);
        r2 = 0;
        r0 = r28;
        r3 = r0.length;
    L_0x0357:
        if (r2 >= r3) goto L_0x0372;
    L_0x0359:
        r16 = r28[r2];
        r4 = java.lang.Integer.valueOf(r16);
        r0 = r23;
        r0.add(r4);
        r2 = r2 + 1;
        goto L_0x0357;
    L_0x0367:
        r16 = r16 + 1;
        goto L_0x0320;
    L_0x036a:
        r24 = com.android.server.Watchdog.NATIVE_STACKS_OF_INTEREST;
        goto L_0x0341;
    L_0x036d:
        r28 = android.os.Process.getPidsForCommands(r24);
        goto L_0x0345;
    L_0x0372:
        if (r18 == 0) goto L_0x0413;
    L_0x0374:
        r2 = 0;
    L_0x0375:
        if (r18 == 0) goto L_0x0379;
    L_0x0377:
        r19 = 0;
    L_0x0379:
        r3 = 1;
        r0 = r19;
        r1 = r23;
        r10 = com.android.server.am.ActivityManagerService.dumpStackTraces(r3, r15, r2, r0, r1);
        r9 = 0;
        r0 = r35;
        r2 = r0.mService;
        r2.updateCpuStatsNow();
        r0 = r35;
        r2 = r0.mService;
        r3 = r2.mProcessCpuTracker;
        monitor-enter(r3);
        r0 = r35;	 Catch:{ all -> 0x0417 }
        r2 = r0.mService;	 Catch:{ all -> 0x0417 }
        r2 = r2.mProcessCpuTracker;	 Catch:{ all -> 0x0417 }
        r9 = r2.printCurrentState(r12);	 Catch:{ all -> 0x0417 }
        monitor-exit(r3);
        r2 = r29.printCurrentLoad();
        r0 = r17;
        r0.append(r2);
        r0 = r17;
        r0.append(r9);
        r0 = r29;
        r2 = r0.printCurrentState(r12);
        r0 = r17;
        r0.append(r2);
        r2 = TAG;
        r3 = r17.toString();
        android.util.Slog.e(r2, r3);
        if (r10 != 0) goto L_0x03c8;
    L_0x03c0:
        r0 = r36;
        r2 = r0.pid;
        r3 = 3;
        android.os.Process.sendSignal(r2, r3);
    L_0x03c8:
        r0 = r35;
        r2 = r0.mService;
        r3 = "anr";
        r0 = r36;
        r5 = r0.processName;
        r11 = 0;
        r4 = r36;
        r6 = r37;
        r7 = r38;
        r8 = r40;
        r2.addErrorToDropBox(r3, r4, r5, r6, r7, r8, r9, r10, r11);
        r0 = r35;
        r2 = r0.mService;
        r2 = r2.mController;
        if (r2 == 0) goto L_0x0442;
    L_0x03e7:
        r0 = r35;	 Catch:{ RemoteException -> 0x0432 }
        r2 = r0.mService;	 Catch:{ RemoteException -> 0x0432 }
        r2 = r2.mController;	 Catch:{ RemoteException -> 0x0432 }
        r0 = r36;	 Catch:{ RemoteException -> 0x0432 }
        r3 = r0.processName;	 Catch:{ RemoteException -> 0x0432 }
        r0 = r36;	 Catch:{ RemoteException -> 0x0432 }
        r4 = r0.pid;	 Catch:{ RemoteException -> 0x0432 }
        r5 = r17.toString();	 Catch:{ RemoteException -> 0x0432 }
        r31 = r2.appNotResponding(r3, r4, r5);	 Catch:{ RemoteException -> 0x0432 }
        if (r31 == 0) goto L_0x0442;	 Catch:{ RemoteException -> 0x0432 }
    L_0x03ff:
        if (r31 >= 0) goto L_0x041a;	 Catch:{ RemoteException -> 0x0432 }
    L_0x0401:
        r0 = r36;	 Catch:{ RemoteException -> 0x0432 }
        r2 = r0.pid;	 Catch:{ RemoteException -> 0x0432 }
        r3 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ RemoteException -> 0x0432 }
        if (r2 == r3) goto L_0x041a;	 Catch:{ RemoteException -> 0x0432 }
    L_0x0409:
        r2 = "anr";	 Catch:{ RemoteException -> 0x0432 }
        r3 = 1;	 Catch:{ RemoteException -> 0x0432 }
        r0 = r36;	 Catch:{ RemoteException -> 0x0432 }
        r0.kill(r2, r3);	 Catch:{ RemoteException -> 0x0432 }
    L_0x0412:
        return;
    L_0x0413:
        r2 = r29;
        goto L_0x0375;
    L_0x0417:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
    L_0x041a:
        r0 = r35;	 Catch:{ RemoteException -> 0x0432 }
        r3 = r0.mService;	 Catch:{ RemoteException -> 0x0432 }
        monitor-enter(r3);	 Catch:{ RemoteException -> 0x0432 }
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x046b }
        r0 = r35;	 Catch:{ all -> 0x046b }
        r2 = r0.mService;	 Catch:{ all -> 0x046b }
        r2 = r2.mServices;	 Catch:{ all -> 0x046b }
        r0 = r36;	 Catch:{ all -> 0x046b }
        r2.scheduleServiceTimeoutLocked(r0);	 Catch:{ all -> 0x046b }
        monitor-exit(r3);	 Catch:{ RemoteException -> 0x0432 }
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();	 Catch:{ RemoteException -> 0x0432 }
        goto L_0x0412;
    L_0x0432:
        r14 = move-exception;
        r0 = r35;
        r2 = r0.mService;
        r3 = 0;
        r2.mController = r3;
        r2 = com.android.server.Watchdog.getInstance();
        r3 = 0;
        r2.setActivityController(r3);
    L_0x0442:
        r0 = r35;
        r4 = r0.mService;
        monitor-enter(r4);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x0583 }
        r0 = r35;	 Catch:{ all -> 0x0583 }
        r2 = r0.mService;	 Catch:{ all -> 0x0583 }
        r2 = r2.mBatteryStatsService;	 Catch:{ all -> 0x0583 }
        r0 = r36;	 Catch:{ all -> 0x0583 }
        r3 = r0.processName;	 Catch:{ all -> 0x0583 }
        r0 = r36;	 Catch:{ all -> 0x0583 }
        r5 = r0.uid;	 Catch:{ all -> 0x0583 }
        r2.noteProcessAnr(r3, r5);	 Catch:{ all -> 0x0583 }
        if (r18 == 0) goto L_0x0471;	 Catch:{ all -> 0x0583 }
    L_0x045d:
        r2 = "bg anr";	 Catch:{ all -> 0x0583 }
        r3 = 1;	 Catch:{ all -> 0x0583 }
        r0 = r36;	 Catch:{ all -> 0x0583 }
        r0.kill(r2, r3);	 Catch:{ all -> 0x0583 }
        monitor-exit(r4);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x046b:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ RemoteException -> 0x0432 }
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();	 Catch:{ RemoteException -> 0x0432 }
        throw r2;	 Catch:{ RemoteException -> 0x0432 }
    L_0x0471:
        if (r37 == 0) goto L_0x0559;
    L_0x0473:
        r0 = r37;	 Catch:{ all -> 0x0583 }
        r2 = r0.shortComponentName;	 Catch:{ all -> 0x0583 }
        r3 = r2;	 Catch:{ all -> 0x0583 }
    L_0x0478:
        if (r40 == 0) goto L_0x055d;	 Catch:{ all -> 0x0583 }
    L_0x047a:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0583 }
        r2.<init>();	 Catch:{ all -> 0x0583 }
        r5 = "ANR ";	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r5);	 Catch:{ all -> 0x0583 }
        r0 = r40;	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r0);	 Catch:{ all -> 0x0583 }
        r2 = r2.toString();	 Catch:{ all -> 0x0583 }
    L_0x0490:
        r5 = r17.toString();	 Catch:{ all -> 0x0583 }
        r0 = r35;	 Catch:{ all -> 0x0583 }
        r1 = r36;	 Catch:{ all -> 0x0583 }
        r0.makeAppNotRespondingLocked(r1, r3, r2, r5);	 Catch:{ all -> 0x0583 }
        r2 = "dalvik.vm.stack-trace-file";	 Catch:{ all -> 0x0583 }
        r3 = 0;	 Catch:{ all -> 0x0583 }
        r34 = android.os.SystemProperties.get(r2, r3);	 Catch:{ all -> 0x0583 }
        if (r34 == 0) goto L_0x0518;	 Catch:{ all -> 0x0583 }
    L_0x04a5:
        r2 = r34.length();	 Catch:{ all -> 0x0583 }
        if (r2 == 0) goto L_0x0518;	 Catch:{ all -> 0x0583 }
    L_0x04ab:
        r33 = new java.io.File;	 Catch:{ all -> 0x0583 }
        r33.<init>(r34);	 Catch:{ all -> 0x0583 }
        r2 = ".";	 Catch:{ all -> 0x0583 }
        r0 = r34;	 Catch:{ all -> 0x0583 }
        r20 = r0.lastIndexOf(r2);	 Catch:{ all -> 0x0583 }
        r2 = -1;	 Catch:{ all -> 0x0583 }
        r0 = r20;	 Catch:{ all -> 0x0583 }
        if (r2 == r0) goto L_0x0562;	 Catch:{ all -> 0x0583 }
    L_0x04be:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0583 }
        r2.<init>();	 Catch:{ all -> 0x0583 }
        r3 = 0;	 Catch:{ all -> 0x0583 }
        r0 = r34;	 Catch:{ all -> 0x0583 }
        r1 = r20;	 Catch:{ all -> 0x0583 }
        r3 = r0.substring(r3, r1);	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0583 }
        r3 = "_";	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0583 }
        r0 = r36;	 Catch:{ all -> 0x0583 }
        r3 = r0.processName;	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0583 }
        r3 = "_";	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0583 }
        r0 = r35;	 Catch:{ all -> 0x0583 }
        r3 = r0.mTraceDateFormat;	 Catch:{ all -> 0x0583 }
        r5 = new java.util.Date;	 Catch:{ all -> 0x0583 }
        r5.<init>();	 Catch:{ all -> 0x0583 }
        r3 = r3.format(r5);	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0583 }
        r0 = r34;	 Catch:{ all -> 0x0583 }
        r1 = r20;	 Catch:{ all -> 0x0583 }
        r3 = r0.substring(r1);	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0583 }
        r25 = r2.toString();	 Catch:{ all -> 0x0583 }
    L_0x0507:
        r2 = new java.io.File;	 Catch:{ all -> 0x0583 }
        r0 = r25;	 Catch:{ all -> 0x0583 }
        r2.<init>(r0);	 Catch:{ all -> 0x0583 }
        r0 = r33;	 Catch:{ all -> 0x0583 }
        r0.renameTo(r2);	 Catch:{ all -> 0x0583 }
        r2 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;	 Catch:{ all -> 0x0583 }
        android.os.SystemClock.sleep(r2);	 Catch:{ all -> 0x0583 }
    L_0x0518:
        r22 = android.os.Message.obtain();	 Catch:{ all -> 0x0583 }
        r21 = new java.util.HashMap;	 Catch:{ all -> 0x0583 }
        r21.<init>();	 Catch:{ all -> 0x0583 }
        r2 = 2;	 Catch:{ all -> 0x0583 }
        r0 = r22;	 Catch:{ all -> 0x0583 }
        r0.what = r2;	 Catch:{ all -> 0x0583 }
        r0 = r21;	 Catch:{ all -> 0x0583 }
        r1 = r22;	 Catch:{ all -> 0x0583 }
        r1.obj = r0;	 Catch:{ all -> 0x0583 }
        if (r39 == 0) goto L_0x0581;	 Catch:{ all -> 0x0583 }
    L_0x052e:
        r2 = 1;	 Catch:{ all -> 0x0583 }
    L_0x052f:
        r0 = r22;	 Catch:{ all -> 0x0583 }
        r0.arg1 = r2;	 Catch:{ all -> 0x0583 }
        r2 = "app";	 Catch:{ all -> 0x0583 }
        r0 = r21;	 Catch:{ all -> 0x0583 }
        r1 = r36;	 Catch:{ all -> 0x0583 }
        r0.put(r2, r1);	 Catch:{ all -> 0x0583 }
        if (r37 == 0) goto L_0x0549;	 Catch:{ all -> 0x0583 }
    L_0x053f:
        r2 = "activity";	 Catch:{ all -> 0x0583 }
        r0 = r21;	 Catch:{ all -> 0x0583 }
        r1 = r37;	 Catch:{ all -> 0x0583 }
        r0.put(r2, r1);	 Catch:{ all -> 0x0583 }
    L_0x0549:
        r0 = r35;	 Catch:{ all -> 0x0583 }
        r2 = r0.mService;	 Catch:{ all -> 0x0583 }
        r2 = r2.mUiHandler;	 Catch:{ all -> 0x0583 }
        r0 = r22;	 Catch:{ all -> 0x0583 }
        r2.sendMessage(r0);	 Catch:{ all -> 0x0583 }
        monitor-exit(r4);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0559:
        r2 = 0;
        r3 = r2;
        goto L_0x0478;
    L_0x055d:
        r2 = "ANR";	 Catch:{ all -> 0x0583 }
        goto L_0x0490;	 Catch:{ all -> 0x0583 }
    L_0x0562:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0583 }
        r2.<init>();	 Catch:{ all -> 0x0583 }
        r0 = r34;	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r0);	 Catch:{ all -> 0x0583 }
        r3 = "_";	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0583 }
        r0 = r36;	 Catch:{ all -> 0x0583 }
        r3 = r0.processName;	 Catch:{ all -> 0x0583 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0583 }
        r25 = r2.toString();	 Catch:{ all -> 0x0583 }
        goto L_0x0507;
    L_0x0581:
        r2 = 0;
        goto L_0x052f;
    L_0x0583:
        r2 = move-exception;
        monitor-exit(r4);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.AppErrors.appNotResponding(com.android.server.am.ProcessRecord, com.android.server.am.ActivityRecord, com.android.server.am.ActivityRecord, boolean, java.lang.String):void");
    }

    private void makeAppNotRespondingLocked(ProcessRecord app, String activity, String shortMsg, String longMsg) {
        app.notResponding = true;
        app.notRespondingReport = generateProcessError(app, 2, activity, shortMsg, longMsg, null);
        startAppProblemLocked(app);
        app.stopFreezingAllLocked();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void handleShowAnrUi(android.os.Message r24) {
        /*
        r23 = this;
        r18 = 0;
        r0 = r23;
        r0 = r0.mService;
        r22 = r0;
        monitor-enter(r22);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x00ea }
        r0 = r24;
        r0 = r0.obj;	 Catch:{ all -> 0x00ea }
        r19 = r0;
        r19 = (java.util.HashMap) r19;	 Catch:{ all -> 0x00ea }
        r1 = "app";
        r0 = r19;
        r20 = r0.get(r1);	 Catch:{ all -> 0x00ea }
        r20 = (com.android.server.am.ProcessRecord) r20;	 Catch:{ all -> 0x00ea }
        if (r20 == 0) goto L_0x0051;
    L_0x0021:
        r0 = r20;
        r1 = r0.anrDialog;	 Catch:{ all -> 0x00ea }
        if (r1 == 0) goto L_0x0051;
    L_0x0027:
        r1 = TAG;	 Catch:{ all -> 0x00ea }
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00ea }
        r2.<init>();	 Catch:{ all -> 0x00ea }
        r3 = "App already has anr dialog: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x00ea }
        r0 = r20;
        r2 = r2.append(r0);	 Catch:{ all -> 0x00ea }
        r2 = r2.toString();	 Catch:{ all -> 0x00ea }
        android.util.Slog.e(r1, r2);	 Catch:{ all -> 0x00ea }
        r0 = r23;
        r1 = r0.mContext;	 Catch:{ all -> 0x00ea }
        r2 = 317; // 0x13d float:4.44E-43 double:1.566E-321;
        r3 = -2;
        com.android.internal.logging.MetricsLogger.action(r1, r2, r3);	 Catch:{ all -> 0x00ea }
        monitor-exit(r22);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0051:
        r4 = new android.content.Intent;	 Catch:{ all -> 0x00ea }
        r1 = "android.intent.action.ANR";
        r4.<init>(r1);	 Catch:{ all -> 0x00ea }
        r0 = r23;
        r1 = r0.mService;	 Catch:{ all -> 0x00ea }
        r1 = r1.mProcessesReady;	 Catch:{ all -> 0x00ea }
        if (r1 != 0) goto L_0x0066;
    L_0x0061:
        r1 = 1342177280; // 0x50000000 float:8.5899346E9 double:6.631236847E-315;
        r4.addFlags(r1);	 Catch:{ all -> 0x00ea }
    L_0x0066:
        r0 = r23;
        r1 = r0.mService;	 Catch:{ all -> 0x00ea }
        r15 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x00ea }
        r2 = 0;
        r3 = 0;
        r5 = 0;
        r6 = 0;
        r7 = 0;
        r8 = 0;
        r9 = 0;
        r10 = 0;
        r11 = -1;
        r12 = 0;
        r13 = 0;
        r14 = 0;
        r16 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r17 = 0;
        r1.broadcastIntentLocked(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17);	 Catch:{ all -> 0x00ea }
        r0 = r23;
        r1 = r0.mContext;	 Catch:{ all -> 0x00ea }
        r1 = r1.getContentResolver();	 Catch:{ all -> 0x00ea }
        r2 = "anr_show_background";
        r3 = 0;
        r1 = android.provider.Settings.Secure.getInt(r1, r2, r3);	 Catch:{ all -> 0x00ea }
        if (r1 == 0) goto L_0x00ce;
    L_0x0091:
        r21 = 1;
    L_0x0093:
        r0 = r23;
        r1 = r0.mService;	 Catch:{ all -> 0x00ea }
        r1 = r1.canShowErrorDialogs();	 Catch:{ all -> 0x00ea }
        if (r1 != 0) goto L_0x009f;
    L_0x009d:
        if (r21 == 0) goto L_0x00d3;
    L_0x009f:
        r5 = new com.android.server.am.AppNotRespondingDialog;	 Catch:{ all -> 0x00ea }
        r0 = r23;
        r6 = r0.mService;	 Catch:{ all -> 0x00ea }
        r0 = r23;
        r7 = r0.mContext;	 Catch:{ all -> 0x00ea }
        r1 = "activity";
        r0 = r19;
        r9 = r0.get(r1);	 Catch:{ all -> 0x00ea }
        r9 = (com.android.server.am.ActivityRecord) r9;	 Catch:{ all -> 0x00ea }
        r0 = r24;
        r1 = r0.arg1;	 Catch:{ all -> 0x00ea }
        if (r1 == 0) goto L_0x00d1;
    L_0x00ba:
        r10 = 1;
    L_0x00bb:
        r8 = r20;
        r5.<init>(r6, r7, r8, r9, r10);	 Catch:{ all -> 0x00ea }
        r0 = r20;
        r0.anrDialog = r5;	 Catch:{ all -> 0x00f2 }
    L_0x00c4:
        monitor-exit(r22);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        if (r5 == 0) goto L_0x00cd;
    L_0x00ca:
        r5.show();
    L_0x00cd:
        return;
    L_0x00ce:
        r21 = 0;
        goto L_0x0093;
    L_0x00d1:
        r10 = 0;
        goto L_0x00bb;
    L_0x00d3:
        r0 = r23;
        r1 = r0.mContext;	 Catch:{ all -> 0x00ea }
        r2 = 317; // 0x13d float:4.44E-43 double:1.566E-321;
        r3 = -1;
        com.android.internal.logging.MetricsLogger.action(r1, r2, r3);	 Catch:{ all -> 0x00ea }
        r0 = r23;
        r1 = r0.mService;	 Catch:{ all -> 0x00ea }
        r2 = 0;
        r0 = r20;
        r1.killAppAtUsersRequest(r0, r2);	 Catch:{ all -> 0x00ea }
        r5 = r18;
        goto L_0x00c4;
    L_0x00ea:
        r1 = move-exception;
        r5 = r18;
    L_0x00ed:
        monitor-exit(r22);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r1;
    L_0x00f2:
        r1 = move-exception;
        goto L_0x00ed;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.AppErrors.handleShowAnrUi(android.os.Message):void");
    }
}
