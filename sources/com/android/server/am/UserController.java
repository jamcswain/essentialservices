package com.android.server.am;

import android.app.AppGlobals;
import android.app.IStopUserCallback;
import android.app.IUserSwitchObserver;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.pm.IPackageManager;
import android.content.pm.UserInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IProgressListener;
import android.os.IRemoteCallback;
import android.os.IUserManager.Stub;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.UserHandle;
import android.os.UserManagerInternal;
import android.os.storage.IStorageManager;
import android.os.storage.StorageManager;
import android.util.ArraySet;
import android.util.IntArray;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseIntArray;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.Preconditions;
import com.android.internal.widget.LockPatternUtils;
import com.android.server.LocalServices;
import com.android.server.pm.UserManagerService;
import com.android.server.wm.WindowManagerService;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

final class UserController {
    static final int MAX_RUNNING_USERS = 3;
    private static final String TAG = "ActivityManager";
    static final int USER_SWITCH_TIMEOUT = 3000;
    @GuardedBy("mLock")
    private volatile ArraySet<String> mCurWaitingUserSwitchCallbacks;
    @GuardedBy("mLock")
    private int[] mCurrentProfileIds;
    @GuardedBy("mLock")
    private volatile int mCurrentUserId;
    private final Handler mHandler;
    private final Injector mInjector;
    private final Object mLock;
    private final LockPatternUtils mLockPatternUtils;
    @GuardedBy("mLock")
    private int[] mStartedUserArray;
    @GuardedBy("mLock")
    private final SparseArray<UserState> mStartedUsers;
    @GuardedBy("mLock")
    private volatile int mTargetUserId;
    @GuardedBy("mLock")
    private final ArrayList<Integer> mUserLru;
    private volatile UserManagerService mUserManager;
    private final SparseIntArray mUserProfileGroupIdsSelfLocked;
    private final RemoteCallbackList<IUserSwitchObserver> mUserSwitchObservers;
    boolean mUserSwitchUiEnabled;

    static class Injector {
        private final ActivityManagerService mService;
        private UserManagerService mUserManager;
        private UserManagerInternal mUserManagerInternal;

        Injector(ActivityManagerService service) {
            this.mService = service;
        }

        protected Object getLock() {
            return this.mService;
        }

        protected Handler getHandler() {
            return this.mService.mHandler;
        }

        protected Context getContext() {
            return this.mService.mContext;
        }

        protected LockPatternUtils getLockPatternUtils() {
            return new LockPatternUtils(getContext());
        }

        protected int broadcastIntentLocked(Intent intent, String resolvedType, IIntentReceiver resultTo, int resultCode, String resultData, Bundle resultExtras, String[] requiredPermissions, int appOp, Bundle bOptions, boolean ordered, boolean sticky, int callingPid, int callingUid, int userId) {
            return this.mService.broadcastIntentLocked(null, null, intent, resolvedType, resultTo, resultCode, resultData, resultExtras, requiredPermissions, appOp, bOptions, ordered, sticky, callingPid, callingUid, userId);
        }

        int checkCallingPermission(String permission) {
            return this.mService.checkCallingPermission(permission);
        }

        WindowManagerService getWindowManager() {
            return this.mService.mWindowManager;
        }

        void activityManagerOnUserStopped(int userId) {
            this.mService.onUserStoppedLocked(userId);
        }

        void systemServiceManagerCleanupUser(int userId) {
            this.mService.mSystemServiceManager.cleanupUser(userId);
        }

        protected UserManagerService getUserManager() {
            if (this.mUserManager == null) {
                this.mUserManager = (UserManagerService) Stub.asInterface(ServiceManager.getService("user"));
            }
            return this.mUserManager;
        }

        UserManagerInternal getUserManagerInternal() {
            if (this.mUserManagerInternal == null) {
                this.mUserManagerInternal = (UserManagerInternal) LocalServices.getService(UserManagerInternal.class);
            }
            return this.mUserManagerInternal;
        }

        KeyguardManager getKeyguardManager() {
            return (KeyguardManager) this.mService.mContext.getSystemService(KeyguardManager.class);
        }

        void batteryStatsServiceNoteEvent(int code, String name, int uid) {
            this.mService.mBatteryStatsService.noteEvent(code, name, uid);
        }

        void systemServiceManagerStopUser(int userId) {
            this.mService.mSystemServiceManager.stopUser(userId);
        }

        boolean isRuntimeRestarted() {
            return this.mService.mSystemServiceManager.isRuntimeRestarted();
        }

        boolean isFirstBootOrUpgrade() {
            IPackageManager pm = AppGlobals.getPackageManager();
            try {
                return !pm.isFirstBoot() ? pm.isUpgrade() : true;
            } catch (RemoteException e) {
                throw e.rethrowFromSystemServer();
            }
        }

        void sendPreBootBroadcast(int userId, boolean quiet, Runnable onFinish) {
            final Runnable runnable = onFinish;
            new PreBootBroadcaster(this.mService, userId, null, quiet) {
                public void onFinished() {
                    runnable.run();
                }
            }.sendNext();
        }

        void activityManagerForceStopPackageLocked(int userId, String reason) {
            this.mService.forceStopPackageLocked(null, -1, false, false, true, false, false, userId, reason);
        }

        int checkComponentPermission(String permission, int pid, int uid, int owningUid, boolean exported) {
            return this.mService.checkComponentPermission(permission, pid, uid, owningUid, exported);
        }

        void startHomeActivityLocked(int userId, String reason) {
            this.mService.startHomeActivityLocked(userId, reason);
        }

        void updateUserConfigurationLocked() {
            this.mService.updateUserConfigurationLocked();
        }

        void clearBroadcastQueueForUserLocked(int userId) {
            this.mService.clearBroadcastQueueForUserLocked(userId);
        }

        void enforceShellRestriction(String restriction, int userId) {
            this.mService.enforceShellRestriction(restriction, userId);
        }

        void showUserSwitchingDialog(UserInfo fromUser, UserInfo toUser) {
            new UserSwitchingDialog(this.mService, this.mService.mContext, fromUser, toUser, true).show();
        }

        ActivityStackSupervisor getActivityStackSupervisor() {
            return this.mService.mStackSupervisor;
        }
    }

    UserController(ActivityManagerService service) {
        this(new Injector(service));
    }

    UserController(Injector injector) {
        this.mCurrentUserId = 0;
        this.mTargetUserId = -10000;
        this.mStartedUsers = new SparseArray();
        this.mUserLru = new ArrayList();
        this.mStartedUserArray = new int[]{0};
        this.mCurrentProfileIds = new int[0];
        this.mUserProfileGroupIdsSelfLocked = new SparseIntArray();
        this.mUserSwitchObservers = new RemoteCallbackList();
        this.mUserSwitchUiEnabled = true;
        this.mInjector = injector;
        this.mLock = injector.getLock();
        this.mHandler = injector.getHandler();
        this.mStartedUsers.put(0, new UserState(UserHandle.SYSTEM));
        this.mUserLru.add(Integer.valueOf(0));
        this.mLockPatternUtils = this.mInjector.getLockPatternUtils();
        updateStartedUserArrayLocked();
    }

    void finishUserSwitch(UserState uss) {
        synchronized (this.mLock) {
            finishUserBoot(uss);
            startProfilesLocked();
            stopRunningUsersLocked(3);
        }
    }

    void stopRunningUsersLocked(int maxRunningUsers) {
        int num = this.mUserLru.size();
        int i = 0;
        while (num > maxRunningUsers && i < this.mUserLru.size()) {
            Integer oldUserId = (Integer) this.mUserLru.get(i);
            UserState oldUss = (UserState) this.mStartedUsers.get(oldUserId.intValue());
            if (oldUss == null) {
                this.mUserLru.remove(i);
                num--;
            } else if (oldUss.state == 4 || oldUss.state == 5) {
                num--;
                i++;
            } else if (oldUserId.intValue() == 0 || oldUserId.intValue() == this.mCurrentUserId) {
                if (UserInfo.isSystemOnly(oldUserId.intValue())) {
                    num--;
                }
                i++;
            } else {
                if (stopUsersLocked(oldUserId.intValue(), false, null) != 0) {
                    num--;
                }
                num--;
                i++;
            }
        }
    }

    private void finishUserBoot(UserState uss) {
        finishUserBoot(uss, null);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void finishUserBoot(com.android.server.am.UserState r23, android.content.IIntentReceiver r24) {
        /*
        r22 = this;
        r0 = r23;
        r2 = r0.mHandle;
        r16 = r2.getIdentifier();
        r2 = TAG;
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "Finishing user boot ";
        r4 = r4.append(r5);
        r0 = r16;
        r4 = r4.append(r0);
        r4 = r4.toString();
        android.util.Slog.d(r2, r4);
        r0 = r22;
        r0 = r0.mLock;
        r21 = r0;
        monitor-enter(r21);
        r0 = r22;
        r2 = r0.mStartedUsers;	 Catch:{ all -> 0x0191 }
        r0 = r16;
        r2 = r2.get(r0);	 Catch:{ all -> 0x0191 }
        r0 = r23;
        if (r2 == r0) goto L_0x003a;
    L_0x0038:
        monitor-exit(r21);
        return;
    L_0x003a:
        r2 = 0;
        r4 = 1;
        r0 = r23;
        r2 = r0.setState(r2, r4);	 Catch:{ all -> 0x0191 }
        if (r2 == 0) goto L_0x00f5;
    L_0x0044:
        r0 = r22;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0191 }
        r2 = r2.getUserManagerInternal();	 Catch:{ all -> 0x0191 }
        r0 = r23;
        r4 = r0.state;	 Catch:{ all -> 0x0191 }
        r0 = r16;
        r2.setUserState(r0, r4);	 Catch:{ all -> 0x0191 }
        if (r16 != 0) goto L_0x00ad;
    L_0x0057:
        r0 = r22;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0191 }
        r2 = r2.isRuntimeRestarted();	 Catch:{ all -> 0x0191 }
        r2 = r2 ^ 1;
        if (r2 == 0) goto L_0x00ad;
    L_0x0063:
        r0 = r22;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0191 }
        r2 = r2.isFirstBootOrUpgrade();	 Catch:{ all -> 0x0191 }
        r2 = r2 ^ 1;
        if (r2 == 0) goto L_0x00ad;
    L_0x006f:
        r4 = android.os.SystemClock.elapsedRealtime();	 Catch:{ all -> 0x0191 }
        r6 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r4 = r4 / r6;
        r0 = (int) r4;	 Catch:{ all -> 0x0191 }
        r20 = r0;
        r0 = r22;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0191 }
        r2 = r2.getContext();	 Catch:{ all -> 0x0191 }
        r4 = "framework_locked_boot_completed";
        r0 = r20;
        com.android.internal.logging.MetricsLogger.histogram(r2, r4, r0);	 Catch:{ all -> 0x0191 }
        r17 = 120; // 0x78 float:1.68E-43 double:5.93E-322;
        r2 = 120; // 0x78 float:1.68E-43 double:5.93E-322;
        r0 = r20;
        if (r0 <= r2) goto L_0x00ad;
    L_0x0091:
        r2 = "SystemServerTiming";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0191 }
        r4.<init>();	 Catch:{ all -> 0x0191 }
        r5 = "finishUserBoot took too long. uptimeSeconds=";
        r4 = r4.append(r5);	 Catch:{ all -> 0x0191 }
        r0 = r20;
        r4 = r4.append(r0);	 Catch:{ all -> 0x0191 }
        r4 = r4.toString();	 Catch:{ all -> 0x0191 }
        android.util.Slog.wtf(r2, r4);	 Catch:{ all -> 0x0191 }
    L_0x00ad:
        r0 = r22;
        r2 = r0.mHandler;	 Catch:{ all -> 0x0191 }
        r0 = r22;
        r4 = r0.mHandler;	 Catch:{ all -> 0x0191 }
        r5 = 64;
        r6 = 0;
        r0 = r16;
        r4 = r4.obtainMessage(r5, r0, r6);	 Catch:{ all -> 0x0191 }
        r2.sendMessage(r4);	 Catch:{ all -> 0x0191 }
        r3 = new android.content.Intent;	 Catch:{ all -> 0x0191 }
        r2 = "android.intent.action.LOCKED_BOOT_COMPLETED";
        r4 = 0;
        r3.<init>(r2, r4);	 Catch:{ all -> 0x0191 }
        r2 = "android.intent.extra.user_handle";
        r0 = r16;
        r3.putExtra(r2, r0);	 Catch:{ all -> 0x0191 }
        r2 = 150994944; // 0x9000000 float:1.540744E-33 double:7.46014145E-316;
        r3.addFlags(r2);	 Catch:{ all -> 0x0191 }
        r0 = r22;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0191 }
        r4 = 1;
        r9 = new java.lang.String[r4];	 Catch:{ all -> 0x0191 }
        r4 = "android.permission.RECEIVE_BOOT_COMPLETED";
        r5 = 0;
        r9[r5] = r4;	 Catch:{ all -> 0x0191 }
        r14 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x0191 }
        r4 = 0;
        r6 = 0;
        r7 = 0;
        r8 = 0;
        r10 = -1;
        r11 = 0;
        r12 = 1;
        r13 = 0;
        r15 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r5 = r24;
        r2.broadcastIntentLocked(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16);	 Catch:{ all -> 0x0191 }
    L_0x00f5:
        r0 = r22;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0191 }
        r2 = r2.getUserManager();	 Catch:{ all -> 0x0191 }
        r0 = r16;
        r2 = r2.isManagedProfile(r0);	 Catch:{ all -> 0x0191 }
        if (r2 == 0) goto L_0x019d;
    L_0x0105:
        r0 = r22;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0191 }
        r2 = r2.getUserManager();	 Catch:{ all -> 0x0191 }
        r0 = r16;
        r18 = r2.getProfileParent(r0);	 Catch:{ all -> 0x0191 }
        if (r18 == 0) goto L_0x015c;
    L_0x0115:
        r0 = r18;
        r2 = r0.id;	 Catch:{ all -> 0x0191 }
        r4 = 4;
        r0 = r22;
        r2 = r0.isUserRunningLocked(r2, r4);	 Catch:{ all -> 0x0191 }
        if (r2 == 0) goto L_0x015c;
    L_0x0122:
        r2 = TAG;	 Catch:{ all -> 0x0191 }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0191 }
        r4.<init>();	 Catch:{ all -> 0x0191 }
        r5 = "User ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x0191 }
        r0 = r16;
        r4 = r4.append(r0);	 Catch:{ all -> 0x0191 }
        r5 = " (parent ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x0191 }
        r0 = r18;
        r5 = r0.id;	 Catch:{ all -> 0x0191 }
        r4 = r4.append(r5);	 Catch:{ all -> 0x0191 }
        r5 = "): attempting unlock because parent is unlocked";
        r4 = r4.append(r5);	 Catch:{ all -> 0x0191 }
        r4 = r4.toString();	 Catch:{ all -> 0x0191 }
        android.util.Slog.d(r2, r4);	 Catch:{ all -> 0x0191 }
        r0 = r22;
        r1 = r16;
        r0.maybeUnlockUser(r1);	 Catch:{ all -> 0x0191 }
    L_0x015a:
        monitor-exit(r21);
        return;
    L_0x015c:
        if (r18 != 0) goto L_0x0194;
    L_0x015e:
        r19 = "<null>";
    L_0x0161:
        r2 = TAG;	 Catch:{ all -> 0x0191 }
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0191 }
        r4.<init>();	 Catch:{ all -> 0x0191 }
        r5 = "User ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x0191 }
        r0 = r16;
        r4 = r4.append(r0);	 Catch:{ all -> 0x0191 }
        r5 = " (parent ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x0191 }
        r0 = r19;
        r4 = r4.append(r0);	 Catch:{ all -> 0x0191 }
        r5 = "): delaying unlock because parent is locked";
        r4 = r4.append(r5);	 Catch:{ all -> 0x0191 }
        r4 = r4.toString();	 Catch:{ all -> 0x0191 }
        android.util.Slog.d(r2, r4);	 Catch:{ all -> 0x0191 }
        goto L_0x015a;
    L_0x0191:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x0194:
        r0 = r18;
        r2 = r0.id;	 Catch:{ all -> 0x0191 }
        r19 = java.lang.String.valueOf(r2);	 Catch:{ all -> 0x0191 }
        goto L_0x0161;
    L_0x019d:
        r0 = r22;
        r1 = r16;
        r0.maybeUnlockUser(r1);	 Catch:{ all -> 0x0191 }
        goto L_0x015a;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.UserController.finishUserBoot(com.android.server.am.UserState, android.content.IIntentReceiver):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void finishUserUnlocking(com.android.server.am.UserState r6) {
        /*
        r5 = this;
        r2 = r6.mHandle;
        r1 = r2.getIdentifier();
        r0 = 0;
        r3 = r5.mLock;
        monitor-enter(r3);
        r2 = r5.mStartedUsers;	 Catch:{ all -> 0x006e }
        r4 = r6.mHandle;	 Catch:{ all -> 0x006e }
        r4 = r4.getIdentifier();	 Catch:{ all -> 0x006e }
        r2 = r2.get(r4);	 Catch:{ all -> 0x006e }
        if (r2 == r6) goto L_0x001a;
    L_0x0018:
        monitor-exit(r3);
        return;
    L_0x001a:
        r2 = android.os.storage.StorageManager.isUserKeyUnlocked(r1);	 Catch:{ all -> 0x006e }
        if (r2 != 0) goto L_0x0022;
    L_0x0020:
        monitor-exit(r3);
        return;
    L_0x0022:
        r2 = 1;
        r4 = 2;
        r2 = r6.setState(r2, r4);	 Catch:{ all -> 0x006e }
        if (r2 == 0) goto L_0x0036;
    L_0x002a:
        r2 = r5.mInjector;	 Catch:{ all -> 0x006e }
        r2 = r2.getUserManagerInternal();	 Catch:{ all -> 0x006e }
        r4 = r6.state;	 Catch:{ all -> 0x006e }
        r2.setUserState(r1, r4);	 Catch:{ all -> 0x006e }
        r0 = 1;
    L_0x0036:
        monitor-exit(r3);
        if (r0 == 0) goto L_0x006d;
    L_0x0039:
        r2 = r6.mUnlockProgress;
        r2.start();
        r2 = r6.mUnlockProgress;
        r3 = r5.mInjector;
        r3 = r3.getContext();
        r4 = 17039467; // 0x104006b float:2.424487E-38 double:8.4186153E-317;
        r3 = r3.getString(r4);
        r4 = 5;
        r2.setProgress(r4, r3);
        r2 = r5.mInjector;
        r2 = r2.getUserManager();
        r2.onBeforeUnlockUser(r1);
        r2 = r6.mUnlockProgress;
        r3 = 20;
        r2.setProgress(r3);
        r2 = r5.mHandler;
        r3 = 59;
        r4 = 0;
        r2 = r2.obtainMessage(r3, r1, r4, r6);
        r2.sendToTarget();
    L_0x006d:
        return;
    L_0x006e:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.UserController.finishUserUnlocking(com.android.server.am.UserState):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void finishUserUnlocked(com.android.server.am.UserState r37) {
        /*
        r36 = this;
        r0 = r37;
        r2 = r0.mHandle;
        r16 = r2.getIdentifier();
        r0 = r36;
        r0 = r0.mLock;
        r35 = r0;
        monitor-enter(r35);
        r0 = r36;
        r2 = r0.mStartedUsers;	 Catch:{ all -> 0x0127 }
        r0 = r37;
        r4 = r0.mHandle;	 Catch:{ all -> 0x0127 }
        r4 = r4.getIdentifier();	 Catch:{ all -> 0x0127 }
        r2 = r2.get(r4);	 Catch:{ all -> 0x0127 }
        r0 = r37;
        if (r2 == r0) goto L_0x0025;
    L_0x0023:
        monitor-exit(r35);
        return;
    L_0x0025:
        r2 = android.os.storage.StorageManager.isUserKeyUnlocked(r16);	 Catch:{ all -> 0x0127 }
        if (r2 != 0) goto L_0x002d;
    L_0x002b:
        monitor-exit(r35);
        return;
    L_0x002d:
        r2 = 2;
        r4 = 3;
        r0 = r37;
        r2 = r0.setState(r2, r4);	 Catch:{ all -> 0x0127 }
        if (r2 == 0) goto L_0x011b;
    L_0x0037:
        r0 = r36;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0127 }
        r2 = r2.getUserManagerInternal();	 Catch:{ all -> 0x0127 }
        r0 = r37;
        r4 = r0.state;	 Catch:{ all -> 0x0127 }
        r0 = r16;
        r2.setUserState(r0, r4);	 Catch:{ all -> 0x0127 }
        r0 = r37;
        r2 = r0.mUnlockProgress;	 Catch:{ all -> 0x0127 }
        r2.finish();	 Catch:{ all -> 0x0127 }
        r3 = new android.content.Intent;	 Catch:{ all -> 0x0127 }
        r2 = "android.intent.action.USER_UNLOCKED";
        r3.<init>(r2);	 Catch:{ all -> 0x0127 }
        r2 = "android.intent.extra.user_handle";
        r0 = r16;
        r3.putExtra(r2, r0);	 Catch:{ all -> 0x0127 }
        r2 = 1342177280; // 0x50000000 float:8.5899346E9 double:6.631236847E-315;
        r3.addFlags(r2);	 Catch:{ all -> 0x0127 }
        r0 = r36;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0127 }
        r14 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x0127 }
        r4 = 0;
        r5 = 0;
        r6 = 0;
        r7 = 0;
        r8 = 0;
        r9 = 0;
        r10 = -1;
        r11 = 0;
        r12 = 0;
        r13 = 0;
        r15 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r2.broadcastIntentLocked(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16);	 Catch:{ all -> 0x0127 }
        r0 = r36;
        r1 = r16;
        r2 = r0.getUserInfo(r1);	 Catch:{ all -> 0x0127 }
        r2 = r2.isManagedProfile();	 Catch:{ all -> 0x0127 }
        if (r2 == 0) goto L_0x00db;
    L_0x0087:
        r0 = r36;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0127 }
        r2 = r2.getUserManager();	 Catch:{ all -> 0x0127 }
        r0 = r16;
        r33 = r2.getProfileParent(r0);	 Catch:{ all -> 0x0127 }
        if (r33 == 0) goto L_0x00db;
    L_0x0097:
        r18 = new android.content.Intent;	 Catch:{ all -> 0x0127 }
        r2 = "android.intent.action.MANAGED_PROFILE_UNLOCKED";
        r0 = r18;
        r0.<init>(r2);	 Catch:{ all -> 0x0127 }
        r2 = "android.intent.extra.USER";
        r4 = android.os.UserHandle.of(r16);	 Catch:{ all -> 0x0127 }
        r0 = r18;
        r0.putExtra(r2, r4);	 Catch:{ all -> 0x0127 }
        r2 = 1342177280; // 0x50000000 float:8.5899346E9 double:6.631236847E-315;
        r0 = r18;
        r0.addFlags(r2);	 Catch:{ all -> 0x0127 }
        r0 = r36;
        r0 = r0.mInjector;	 Catch:{ all -> 0x0127 }
        r17 = r0;
        r29 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x0127 }
        r0 = r33;
        r0 = r0.id;	 Catch:{ all -> 0x0127 }
        r31 = r0;
        r19 = 0;
        r20 = 0;
        r21 = 0;
        r22 = 0;
        r23 = 0;
        r24 = 0;
        r25 = -1;
        r26 = 0;
        r27 = 0;
        r28 = 0;
        r30 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r17.broadcastIntentLocked(r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31);	 Catch:{ all -> 0x0127 }
    L_0x00db:
        r0 = r36;
        r1 = r16;
        r32 = r0.getUserInfo(r1);	 Catch:{ all -> 0x0127 }
        r0 = r32;
        r2 = r0.lastLoggedInFingerprint;	 Catch:{ all -> 0x0127 }
        r4 = android.os.Build.FINGERPRINT;	 Catch:{ all -> 0x0127 }
        r2 = java.util.Objects.equals(r2, r4);	 Catch:{ all -> 0x0127 }
        if (r2 != 0) goto L_0x0123;
    L_0x00ef:
        r2 = r32.isManagedProfile();	 Catch:{ all -> 0x0127 }
        if (r2 == 0) goto L_0x0120;
    L_0x00f5:
        r0 = r37;
        r2 = r0.tokenProvided;	 Catch:{ all -> 0x0127 }
        if (r2 == 0) goto L_0x011d;
    L_0x00fb:
        r0 = r36;
        r2 = r0.mLockPatternUtils;	 Catch:{ all -> 0x0127 }
        r0 = r16;
        r2 = r2.isSeparateProfileChallengeEnabled(r0);	 Catch:{ all -> 0x0127 }
        r34 = r2 ^ 1;
    L_0x0107:
        r0 = r36;
        r2 = r0.mInjector;	 Catch:{ all -> 0x0127 }
        r4 = new com.android.server.am.-$Lambda$-wbdEBNBIl8hthLGGkbuzj1haLA;	 Catch:{ all -> 0x0127 }
        r0 = r36;
        r1 = r37;
        r4.<init>(r0, r1);	 Catch:{ all -> 0x0127 }
        r0 = r16;
        r1 = r34;
        r2.sendPreBootBroadcast(r0, r1, r4);	 Catch:{ all -> 0x0127 }
    L_0x011b:
        monitor-exit(r35);
        return;
    L_0x011d:
        r34 = 1;
        goto L_0x0107;
    L_0x0120:
        r34 = 0;
        goto L_0x0107;
    L_0x0123:
        r36.finishUserUnlockedCompleted(r37);	 Catch:{ all -> 0x0127 }
        goto L_0x011b;
    L_0x0127:
        r2 = move-exception;
        monitor-exit(r35);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.UserController.finishUserUnlocked(com.android.server.am.UserState):void");
    }

    /* synthetic */ void lambda$-com_android_server_am_UserController_17384(UserState uss) {
        finishUserUnlockedCompleted(uss);
    }

    private void finishUserUnlockedCompleted(UserState uss) {
        int userId = uss.mHandle.getIdentifier();
        synchronized (this.mLock) {
            if (this.mStartedUsers.get(uss.mHandle.getIdentifier()) != uss) {
                return;
            }
            UserInfo userInfo = getUserInfo(userId);
            if (userInfo == null) {
            } else if (StorageManager.isUserKeyUnlocked(userId)) {
                this.mInjector.getUserManager().onUserLoggedIn(userId);
                if (!(userInfo.isInitialized() || userId == 0)) {
                    Slog.d(TAG, "Initializing user #" + userId);
                    Intent intent = new Intent("android.intent.action.USER_INITIALIZE");
                    intent.addFlags(285212672);
                    final UserInfo userInfo2 = userInfo;
                    this.mInjector.broadcastIntentLocked(intent, null, new IIntentReceiver.Stub() {
                        public void performReceive(Intent intent, int resultCode, String data, Bundle extras, boolean ordered, boolean sticky, int sendingUser) {
                            UserController.this.mInjector.getUserManager().makeInitialized(userInfo2.id);
                        }
                    }, 0, null, null, null, -1, null, true, false, ActivityManagerService.MY_PID, 1000, userId);
                }
                Slog.i(TAG, "Sending BOOT_COMPLETE user #" + userId);
                if (!(userId != 0 || (this.mInjector.isRuntimeRestarted() ^ 1) == 0 || (this.mInjector.isFirstBootOrUpgrade() ^ 1) == 0)) {
                    MetricsLogger.histogram(this.mInjector.getContext(), "framework_boot_completed", (int) (SystemClock.elapsedRealtime() / 1000));
                }
                Intent intent2 = new Intent("android.intent.action.BOOT_COMPLETED", null);
                intent2.putExtra("android.intent.extra.user_handle", userId);
                intent2.addFlags(150994944);
                final int i = userId;
                this.mInjector.broadcastIntentLocked(intent2, null, new IIntentReceiver.Stub() {
                    public void performReceive(Intent intent, int resultCode, String data, Bundle extras, boolean ordered, boolean sticky, int sendingUser) throws RemoteException {
                        Slog.i(UserController.TAG, "Finished processing BOOT_COMPLETED for u" + i);
                    }
                }, 0, null, null, new String[]{"android.permission.RECEIVE_BOOT_COMPLETED"}, -1, null, true, false, ActivityManagerService.MY_PID, 1000, userId);
            }
        }
    }

    int restartUser(int userId, final boolean foreground) {
        return stopUser(userId, true, new IStopUserCallback.Stub() {
            /* synthetic */ void lambda$-com_android_server_am_UserController$3_21318(int userId, boolean foreground) {
                UserController.this.startUser(userId, foreground);
            }

            public void userStopped(int userId) {
                UserController.this.mHandler.post(new -$Lambda$5yQSwWrsRDcxoFuTXgyaBIqPvDw((byte) 1, foreground, userId, this));
            }

            public void userStopAborted(int userId) {
            }
        });
    }

    int stopUser(int userId, boolean force, IStopUserCallback callback) {
        if (this.mInjector.checkCallingPermission("android.permission.INTERACT_ACROSS_USERS_FULL") != 0) {
            String msg = "Permission Denial: switchUser() from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " requires " + "android.permission.INTERACT_ACROSS_USERS_FULL";
            Slog.w(TAG, msg);
            throw new SecurityException(msg);
        } else if (userId < 0 || userId == 0) {
            throw new IllegalArgumentException("Can't stop system user " + userId);
        } else {
            int stopUsersLocked;
            this.mInjector.enforceShellRestriction("no_debugging_features", userId);
            synchronized (this.mLock) {
                stopUsersLocked = stopUsersLocked(userId, force, callback);
            }
            return stopUsersLocked;
        }
    }

    private int stopUsersLocked(int userId, boolean force, IStopUserCallback callback) {
        if (userId == 0) {
            return -3;
        }
        if (isCurrentUserLocked(userId)) {
            return -2;
        }
        int[] usersToStop = getUsersToStopLocked(userId);
        int i = 0;
        while (i < usersToStop.length) {
            int relatedUserId = usersToStop[i];
            if (relatedUserId != 0 && !isCurrentUserLocked(relatedUserId)) {
                i++;
            } else if (!force) {
                return -4;
            } else {
                Slog.i(TAG, "Force stop user " + userId + ". Related users will not be stopped");
                stopSingleUserLocked(userId, callback);
                return 0;
            }
        }
        for (int userIdToStop : usersToStop) {
            stopSingleUserLocked(userIdToStop, userIdToStop == userId ? callback : null);
        }
        return 0;
    }

    private void stopSingleUserLocked(int userId, IStopUserCallback callback) {
        UserState uss = (UserState) this.mStartedUsers.get(userId);
        if (uss == null) {
            if (callback != null) {
                final IStopUserCallback iStopUserCallback = callback;
                final int i = userId;
                this.mHandler.post(new Runnable() {
                    public void run() {
                        try {
                            iStopUserCallback.userStopped(i);
                        } catch (RemoteException e) {
                        }
                    }
                });
            }
            return;
        }
        if (callback != null) {
            uss.mStopCallbacks.add(callback);
        }
        if (!(uss.state == 4 || uss.state == 5)) {
            uss.setState(4);
            this.mInjector.getUserManagerInternal().setUserState(userId, uss.state);
            updateStartedUserArrayLocked();
            long ident = Binder.clearCallingIdentity();
            try {
                Intent stoppingIntent = new Intent("android.intent.action.USER_STOPPING");
                stoppingIntent.addFlags(1073741824);
                stoppingIntent.putExtra("android.intent.extra.user_handle", userId);
                stoppingIntent.putExtra("android.intent.extra.SHUTDOWN_USERSPACE_ONLY", true);
                final int i2 = userId;
                final UserState userState = uss;
                IIntentReceiver stoppingReceiver = new IIntentReceiver.Stub() {
                    public void performReceive(Intent intent, int resultCode, String data, Bundle extras, boolean ordered, boolean sticky, int sendingUser) {
                        Handler -get2 = UserController.this.mHandler;
                        final int i = i2;
                        final UserState userState = userState;
                        -get2.post(new Runnable() {
                            public void run() {
                                UserController.this.finishUserStopping(i, userState);
                            }
                        });
                    }
                };
                this.mInjector.clearBroadcastQueueForUserLocked(userId);
                this.mInjector.broadcastIntentLocked(stoppingIntent, null, stoppingReceiver, 0, null, null, new String[]{"android.permission.INTERACT_ACROSS_USERS"}, -1, null, true, false, ActivityManagerService.MY_PID, 1000, -1);
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    void finishUserStopping(int userId, UserState uss) {
        Intent shutdownIntent = new Intent("android.intent.action.ACTION_SHUTDOWN");
        shutdownIntent.addFlags(16777216);
        final UserState userState = uss;
        IIntentReceiver shutdownReceiver = new IIntentReceiver.Stub() {
            public void performReceive(Intent intent, int resultCode, String data, Bundle extras, boolean ordered, boolean sticky, int sendingUser) {
                Handler -get2 = UserController.this.mHandler;
                final UserState userState = userState;
                -get2.post(new Runnable() {
                    public void run() {
                        UserController.this.finishUserStopped(userState);
                    }
                });
            }
        };
        synchronized (this.mLock) {
            if (uss.state != 4) {
                return;
            }
            uss.setState(5);
            this.mInjector.getUserManagerInternal().setUserState(userId, uss.state);
            this.mInjector.batteryStatsServiceNoteEvent(16391, Integer.toString(userId), userId);
            this.mInjector.systemServiceManagerStopUser(userId);
            synchronized (this.mLock) {
                this.mInjector.broadcastIntentLocked(shutdownIntent, null, shutdownReceiver, 0, null, null, null, -1, null, true, false, ActivityManagerService.MY_PID, 1000, userId);
            }
        }
    }

    void finishUserStopped(UserState uss) {
        boolean stopped;
        int userId = uss.mHandle.getIdentifier();
        synchronized (this.mLock) {
            ArrayList<IStopUserCallback> callbacks = new ArrayList(uss.mStopCallbacks);
            if (this.mStartedUsers.get(userId) != uss) {
                stopped = false;
            } else if (uss.state != 5) {
                stopped = false;
            } else {
                stopped = true;
                this.mStartedUsers.remove(userId);
                this.mInjector.getUserManagerInternal().removeUserState(userId);
                this.mUserLru.remove(Integer.valueOf(userId));
                updateStartedUserArrayLocked();
                this.mInjector.activityManagerOnUserStopped(userId);
                forceStopUserLocked(userId, "finish user");
            }
        }
        for (int i = 0; i < callbacks.size(); i++) {
            if (stopped) {
                try {
                    ((IStopUserCallback) callbacks.get(i)).userStopped(userId);
                } catch (RemoteException e) {
                }
            } else {
                ((IStopUserCallback) callbacks.get(i)).userStopAborted(userId);
            }
        }
        if (stopped) {
            this.mInjector.systemServiceManagerCleanupUser(userId);
            synchronized (this.mLock) {
                this.mInjector.getActivityStackSupervisor().removeUserLocked(userId);
            }
            if (getUserInfo(userId).isEphemeral()) {
                this.mInjector.getUserManager().removeUser(userId);
            }
            try {
                getStorageManager().lockUserKey(userId);
            } catch (RemoteException re) {
                throw re.rethrowAsRuntimeException();
            }
        }
    }

    private int[] getUsersToStopLocked(int userId) {
        int startedUsersSize = this.mStartedUsers.size();
        IntArray userIds = new IntArray();
        userIds.add(userId);
        synchronized (this.mUserProfileGroupIdsSelfLocked) {
            int userGroupId = this.mUserProfileGroupIdsSelfLocked.get(userId, -10000);
            for (int i = 0; i < startedUsersSize; i++) {
                int startedUserId = ((UserState) this.mStartedUsers.valueAt(i)).mHandle.getIdentifier();
                boolean sameGroup = userGroupId != -10000 ? userGroupId == this.mUserProfileGroupIdsSelfLocked.get(startedUserId, -10000) : false;
                boolean sameUserId = startedUserId == userId;
                if (sameGroup && !sameUserId) {
                    userIds.add(startedUserId);
                }
            }
        }
        return userIds.toArray();
    }

    private void forceStopUserLocked(int userId, String reason) {
        this.mInjector.activityManagerForceStopPackageLocked(userId, reason);
        Intent intent = new Intent("android.intent.action.USER_STOPPED");
        intent.addFlags(1342177280);
        intent.putExtra("android.intent.extra.user_handle", userId);
        this.mInjector.broadcastIntentLocked(intent, null, null, 0, null, null, null, -1, null, false, false, ActivityManagerService.MY_PID, 1000, -1);
    }

    private void stopGuestOrEphemeralUserIfBackground() {
        synchronized (this.mLock) {
            int num = this.mUserLru.size();
            for (int i = 0; i < num; i++) {
                Integer oldUserId = (Integer) this.mUserLru.get(i);
                UserState oldUss = (UserState) this.mStartedUsers.get(oldUserId.intValue());
                if (!(oldUserId.intValue() == 0 || oldUserId.intValue() == this.mCurrentUserId || oldUss.state == 4 || oldUss.state == 5)) {
                    UserInfo userInfo = getUserInfo(oldUserId.intValue());
                    if (userInfo.isEphemeral()) {
                        ((UserManagerInternal) LocalServices.getService(UserManagerInternal.class)).onEphemeralUserStop(oldUserId.intValue());
                    }
                    if (userInfo.isGuest() || userInfo.isEphemeral()) {
                        stopUsersLocked(oldUserId.intValue(), true, null);
                        break;
                    }
                }
            }
        }
    }

    void startProfilesLocked() {
        List<UserInfo> profiles = this.mInjector.getUserManager().getProfiles(this.mCurrentUserId, false);
        List<UserInfo> profilesToStart = new ArrayList(profiles.size());
        for (UserInfo user : profiles) {
            if (!((user.flags & 16) != 16 || user.id == this.mCurrentUserId || (user.isQuietModeEnabled() ^ 1) == 0)) {
                profilesToStart.add(user);
            }
        }
        int profilesToStartSize = profilesToStart.size();
        int i = 0;
        while (i < profilesToStartSize && i < 2) {
            startUser(((UserInfo) profilesToStart.get(i)).id, false);
            i++;
        }
        if (i < profilesToStartSize) {
            Slog.w(TAG, "More profiles than MAX_RUNNING_USERS");
        }
    }

    private IStorageManager getStorageManager() {
        return IStorageManager.Stub.asInterface(ServiceManager.getService("mount"));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    boolean startUser(int r31, boolean r32) {
        /*
        r30 = this;
        r0 = r30;
        r4 = r0.mInjector;
        r6 = "android.permission.INTERACT_ACROSS_USERS_FULL";
        r4 = r4.checkCallingPermission(r6);
        if (r4 == 0) goto L_0x0051;
    L_0x000d:
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r6 = "Permission Denial: switchUser() from pid=";
        r4 = r4.append(r6);
        r6 = android.os.Binder.getCallingPid();
        r4 = r4.append(r6);
        r6 = ", uid=";
        r4 = r4.append(r6);
        r6 = android.os.Binder.getCallingUid();
        r4 = r4.append(r6);
        r6 = " requires ";
        r4 = r4.append(r6);
        r6 = "android.permission.INTERACT_ACROSS_USERS_FULL";
        r4 = r4.append(r6);
        r22 = r4.toString();
        r4 = TAG;
        r0 = r22;
        android.util.Slog.w(r4, r0);
        r4 = new java.lang.SecurityException;
        r0 = r22;
        r4.<init>(r0);
        throw r4;
    L_0x0051:
        r4 = TAG;
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Starting userid:";
        r6 = r6.append(r7);
        r0 = r31;
        r6 = r6.append(r0);
        r7 = " fg:";
        r6 = r6.append(r7);
        r0 = r32;
        r6 = r6.append(r0);
        r6 = r6.toString();
        android.util.Slog.i(r4, r6);
        r20 = android.os.Binder.clearCallingIdentity();
        r0 = r30;
        r0 = r0.mLock;	 Catch:{ all -> 0x0310 }
        r29 = r0;
        monitor-enter(r29);	 Catch:{ all -> 0x0310 }
        r0 = r30;
        r0 = r0.mCurrentUserId;	 Catch:{ all -> 0x030d }
        r24 = r0;
        r0 = r24;
        r1 = r31;
        if (r0 != r1) goto L_0x0096;
    L_0x0090:
        monitor-exit(r29);	 Catch:{ all -> 0x0310 }
        r4 = 1;
        android.os.Binder.restoreCallingIdentity(r20);
        return r4;
    L_0x0096:
        if (r32 == 0) goto L_0x00a9;
    L_0x0098:
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getActivityStackSupervisor();	 Catch:{ all -> 0x030d }
        r6 = "startUser";
        r7 = 0;
        r8 = 0;
        r9 = 0;
        r4.setLockTaskModeLocked(r7, r8, r6, r9);	 Catch:{ all -> 0x030d }
    L_0x00a9:
        r26 = r30.getUserInfo(r31);	 Catch:{ all -> 0x030d }
        if (r26 != 0) goto L_0x00d0;
    L_0x00af:
        r4 = TAG;	 Catch:{ all -> 0x030d }
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x030d }
        r6.<init>();	 Catch:{ all -> 0x030d }
        r7 = "No user info for user #";
        r6 = r6.append(r7);	 Catch:{ all -> 0x030d }
        r0 = r31;
        r6 = r6.append(r0);	 Catch:{ all -> 0x030d }
        r6 = r6.toString();	 Catch:{ all -> 0x030d }
        android.util.Slog.w(r4, r6);	 Catch:{ all -> 0x030d }
        monitor-exit(r29);	 Catch:{ all -> 0x0310 }
        r4 = 0;
        android.os.Binder.restoreCallingIdentity(r20);
        return r4;
    L_0x00d0:
        if (r32 == 0) goto L_0x0100;
    L_0x00d2:
        r4 = r26.isManagedProfile();	 Catch:{ all -> 0x030d }
        if (r4 == 0) goto L_0x0100;
    L_0x00d8:
        r4 = TAG;	 Catch:{ all -> 0x030d }
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x030d }
        r6.<init>();	 Catch:{ all -> 0x030d }
        r7 = "Cannot switch to User #";
        r6 = r6.append(r7);	 Catch:{ all -> 0x030d }
        r0 = r31;
        r6 = r6.append(r0);	 Catch:{ all -> 0x030d }
        r7 = ": not a full user";
        r6 = r6.append(r7);	 Catch:{ all -> 0x030d }
        r6 = r6.toString();	 Catch:{ all -> 0x030d }
        android.util.Slog.w(r4, r6);	 Catch:{ all -> 0x030d }
        monitor-exit(r29);	 Catch:{ all -> 0x0310 }
        r4 = 0;
        android.os.Binder.restoreCallingIdentity(r20);
        return r4;
    L_0x0100:
        if (r32 == 0) goto L_0x0119;
    L_0x0102:
        r0 = r30;
        r4 = r0.mUserSwitchUiEnabled;	 Catch:{ all -> 0x030d }
        if (r4 == 0) goto L_0x0119;
    L_0x0108:
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getWindowManager();	 Catch:{ all -> 0x030d }
        r6 = 17432706; // 0x10a0082 float:2.534696E-38 double:8.612901E-317;
        r7 = 17432705; // 0x10a0081 float:2.5346958E-38 double:8.6129007E-317;
        r4.startFreezingScreen(r6, r7);	 Catch:{ all -> 0x030d }
    L_0x0119:
        r23 = 0;
        r0 = r30;
        r4 = r0.mStartedUsers;	 Catch:{ all -> 0x030d }
        r0 = r31;
        r4 = r4.get(r0);	 Catch:{ all -> 0x030d }
        if (r4 != 0) goto L_0x0153;
    L_0x0127:
        r27 = new com.android.server.am.UserState;	 Catch:{ all -> 0x030d }
        r4 = android.os.UserHandle.of(r31);	 Catch:{ all -> 0x030d }
        r0 = r27;
        r0.<init>(r4);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mStartedUsers;	 Catch:{ all -> 0x030d }
        r0 = r31;
        r1 = r27;
        r4.put(r0, r1);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getUserManagerInternal();	 Catch:{ all -> 0x030d }
        r0 = r27;
        r6 = r0.state;	 Catch:{ all -> 0x030d }
        r0 = r31;
        r4.setUserState(r0, r6);	 Catch:{ all -> 0x030d }
        r30.updateStartedUserArrayLocked();	 Catch:{ all -> 0x030d }
        r23 = 1;
    L_0x0153:
        r0 = r30;
        r4 = r0.mStartedUsers;	 Catch:{ all -> 0x030d }
        r0 = r31;
        r28 = r4.get(r0);	 Catch:{ all -> 0x030d }
        r28 = (com.android.server.am.UserState) r28;	 Catch:{ all -> 0x030d }
        r25 = java.lang.Integer.valueOf(r31);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mUserLru;	 Catch:{ all -> 0x030d }
        r0 = r25;
        r4.remove(r0);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mUserLru;	 Catch:{ all -> 0x030d }
        r0 = r25;
        r4.add(r0);	 Catch:{ all -> 0x030d }
        if (r32 == 0) goto L_0x02df;
    L_0x0177:
        r0 = r31;
        r1 = r30;
        r1.mCurrentUserId = r0;	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4.updateUserConfigurationLocked();	 Catch:{ all -> 0x030d }
        r4 = -10000; // 0xffffffffffffd8f0 float:NaN double:NaN;
        r0 = r30;
        r0.mTargetUserId = r4;	 Catch:{ all -> 0x030d }
        r30.updateCurrentProfileIdsLocked();	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getWindowManager();	 Catch:{ all -> 0x030d }
        r0 = r30;
        r6 = r0.mCurrentProfileIds;	 Catch:{ all -> 0x030d }
        r0 = r31;
        r4.setCurrentUser(r0, r6);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mUserSwitchUiEnabled;	 Catch:{ all -> 0x030d }
        if (r4 == 0) goto L_0x01bc;
    L_0x01a4:
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getWindowManager();	 Catch:{ all -> 0x030d }
        r6 = 1;
        r4.setSwitchingUser(r6);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getWindowManager();	 Catch:{ all -> 0x030d }
        r6 = 0;
        r4.lockNow(r6);	 Catch:{ all -> 0x030d }
    L_0x01bc:
        r0 = r28;
        r4 = r0.state;	 Catch:{ all -> 0x030d }
        r6 = 4;
        if (r4 != r6) goto L_0x0315;
    L_0x01c3:
        r0 = r28;
        r4 = r0.lastState;	 Catch:{ all -> 0x030d }
        r0 = r28;
        r0.setState(r4);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getUserManagerInternal();	 Catch:{ all -> 0x030d }
        r0 = r28;
        r6 = r0.state;	 Catch:{ all -> 0x030d }
        r0 = r31;
        r4.setUserState(r0, r6);	 Catch:{ all -> 0x030d }
        r30.updateStartedUserArrayLocked();	 Catch:{ all -> 0x030d }
        r23 = 1;
    L_0x01e2:
        r0 = r28;
        r4 = r0.state;	 Catch:{ all -> 0x030d }
        if (r4 != 0) goto L_0x0209;
    L_0x01e8:
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getUserManager();	 Catch:{ all -> 0x030d }
        r0 = r31;
        r4.onBeforeStartUser(r0);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r0 = r30;
        r6 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r7 = 42;
        r8 = 0;
        r0 = r31;
        r6 = r6.obtainMessage(r7, r0, r8);	 Catch:{ all -> 0x030d }
        r4.sendMessage(r6);	 Catch:{ all -> 0x030d }
    L_0x0209:
        if (r32 == 0) goto L_0x0262;
    L_0x020b:
        r0 = r30;
        r4 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r0 = r30;
        r6 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r7 = 43;
        r0 = r31;
        r1 = r24;
        r6 = r6.obtainMessage(r7, r0, r1);	 Catch:{ all -> 0x030d }
        r4.sendMessage(r6);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r6 = 34;
        r4.removeMessages(r6);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r6 = 36;
        r4.removeMessages(r6);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r0 = r30;
        r6 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r7 = 34;
        r0 = r24;
        r1 = r31;
        r2 = r28;
        r6 = r6.obtainMessage(r7, r0, r1, r2);	 Catch:{ all -> 0x030d }
        r4.sendMessage(r6);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r0 = r30;
        r6 = r0.mHandler;	 Catch:{ all -> 0x030d }
        r7 = 36;
        r0 = r24;
        r1 = r31;
        r2 = r28;
        r6 = r6.obtainMessage(r7, r0, r1, r2);	 Catch:{ all -> 0x030d }
        r8 = 3000; // 0xbb8 float:4.204E-42 double:1.482E-320;
        r4.sendMessageDelayed(r6, r8);	 Catch:{ all -> 0x030d }
    L_0x0262:
        if (r23 == 0) goto L_0x0290;
    L_0x0264:
        r5 = new android.content.Intent;	 Catch:{ all -> 0x030d }
        r4 = "android.intent.action.USER_STARTED";
        r5.<init>(r4);	 Catch:{ all -> 0x030d }
        r4 = 1342177280; // 0x50000000 float:8.5899346E9 double:6.631236847E-315;
        r5.addFlags(r4);	 Catch:{ all -> 0x030d }
        r4 = "android.intent.extra.user_handle";
        r0 = r31;
        r5.putExtra(r4, r0);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r16 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x030d }
        r6 = 0;
        r7 = 0;
        r8 = 0;
        r9 = 0;
        r10 = 0;
        r11 = 0;
        r12 = -1;
        r13 = 0;
        r14 = 0;
        r15 = 0;
        r17 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r18 = r31;
        r4.broadcastIntentLocked(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18);	 Catch:{ all -> 0x030d }
    L_0x0290:
        if (r32 == 0) goto L_0x033a;
    L_0x0292:
        r0 = r30;
        r1 = r28;
        r2 = r24;
        r3 = r31;
        r0.moveUserToForegroundLocked(r1, r2, r3);	 Catch:{ all -> 0x030d }
    L_0x029d:
        if (r23 == 0) goto L_0x02d9;
    L_0x029f:
        r5 = new android.content.Intent;	 Catch:{ all -> 0x030d }
        r4 = "android.intent.action.USER_STARTING";
        r5.<init>(r4);	 Catch:{ all -> 0x030d }
        r4 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r5.addFlags(r4);	 Catch:{ all -> 0x030d }
        r4 = "android.intent.extra.user_handle";
        r0 = r31;
        r5.putExtra(r4, r0);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r7 = new com.android.server.am.UserController$7;	 Catch:{ all -> 0x030d }
        r0 = r30;
        r7.<init>();	 Catch:{ all -> 0x030d }
        r6 = 1;
        r11 = new java.lang.String[r6];	 Catch:{ all -> 0x030d }
        r6 = "android.permission.INTERACT_ACROSS_USERS";
        r8 = 0;
        r11[r8] = r6;	 Catch:{ all -> 0x030d }
        r16 = com.android.server.am.ActivityManagerService.MY_PID;	 Catch:{ all -> 0x030d }
        r6 = 0;
        r8 = 0;
        r9 = 0;
        r10 = 0;
        r12 = -1;
        r13 = 0;
        r14 = 1;
        r15 = 0;
        r17 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r18 = -1;
        r4.broadcastIntentLocked(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18);	 Catch:{ all -> 0x030d }
    L_0x02d9:
        monitor-exit(r29);	 Catch:{ all -> 0x0310 }
        android.os.Binder.restoreCallingIdentity(r20);
        r4 = 1;
        return r4;
    L_0x02df:
        r0 = r30;
        r4 = r0.mCurrentUserId;	 Catch:{ all -> 0x030d }
        r19 = java.lang.Integer.valueOf(r4);	 Catch:{ all -> 0x030d }
        r30.updateCurrentProfileIdsLocked();	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getWindowManager();	 Catch:{ all -> 0x030d }
        r0 = r30;
        r6 = r0.mCurrentProfileIds;	 Catch:{ all -> 0x030d }
        r4.setCurrentProfileIds(r6);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mUserLru;	 Catch:{ all -> 0x030d }
        r0 = r19;
        r4.remove(r0);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mUserLru;	 Catch:{ all -> 0x030d }
        r0 = r19;
        r4.add(r0);	 Catch:{ all -> 0x030d }
        goto L_0x01bc;
    L_0x030d:
        r4 = move-exception;
        monitor-exit(r29);	 Catch:{ all -> 0x0310 }
        throw r4;	 Catch:{ all -> 0x0310 }
    L_0x0310:
        r4 = move-exception;
        android.os.Binder.restoreCallingIdentity(r20);
        throw r4;
    L_0x0315:
        r0 = r28;
        r4 = r0.state;	 Catch:{ all -> 0x030d }
        r6 = 5;
        if (r4 != r6) goto L_0x01e2;
    L_0x031c:
        r4 = 0;
        r0 = r28;
        r0.setState(r4);	 Catch:{ all -> 0x030d }
        r0 = r30;
        r4 = r0.mInjector;	 Catch:{ all -> 0x030d }
        r4 = r4.getUserManagerInternal();	 Catch:{ all -> 0x030d }
        r0 = r28;
        r6 = r0.state;	 Catch:{ all -> 0x030d }
        r0 = r31;
        r4.setUserState(r0, r6);	 Catch:{ all -> 0x030d }
        r30.updateStartedUserArrayLocked();	 Catch:{ all -> 0x030d }
        r23 = 1;
        goto L_0x01e2;
    L_0x033a:
        r0 = r30;
        r1 = r28;
        r0.finishUserBoot(r1);	 Catch:{ all -> 0x030d }
        goto L_0x029d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.UserController.startUser(int, boolean):boolean");
    }

    void startUserInForeground(int targetUserId) {
        if (!startUser(targetUserId, true)) {
            this.mInjector.getWindowManager().setSwitchingUser(false);
        }
    }

    boolean unlockUser(int userId, byte[] token, byte[] secret, IProgressListener listener) {
        if (this.mInjector.checkCallingPermission("android.permission.INTERACT_ACROSS_USERS_FULL") != 0) {
            String msg = "Permission Denial: unlockUser() from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " requires " + "android.permission.INTERACT_ACROSS_USERS_FULL";
            Slog.w(TAG, msg);
            throw new SecurityException(msg);
        }
        long binderToken = Binder.clearCallingIdentity();
        try {
            boolean unlockUserCleared = unlockUserCleared(userId, token, secret, listener);
            return unlockUserCleared;
        } finally {
            Binder.restoreCallingIdentity(binderToken);
        }
    }

    boolean maybeUnlockUser(int userId) {
        return unlockUserCleared(userId, null, null, null);
    }

    private static void notifyFinished(int userId, IProgressListener listener) {
        if (listener != null) {
            try {
                listener.onFinished(userId, null);
            } catch (RemoteException e) {
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    boolean unlockUserCleared(int r17, byte[] r18, byte[] r19, android.os.IProgressListener r20) {
        /*
        r16 = this;
        r0 = r16;
        r13 = r0.mLock;
        monitor-enter(r13);
        r12 = android.os.storage.StorageManager.isUserKeyUnlocked(r17);	 Catch:{ all -> 0x0055 }
        if (r12 != 0) goto L_0x001e;
    L_0x000b:
        r10 = r16.getUserInfo(r17);	 Catch:{ all -> 0x0055 }
        r8 = r16.getStorageManager();	 Catch:{ all -> 0x0055 }
        r12 = r10.serialNumber;	 Catch:{ RemoteException -> 0x0036, RemoteException -> 0x0036 }
        r0 = r17;
        r1 = r18;
        r2 = r19;
        r8.unlockUserKey(r0, r12, r1, r2);	 Catch:{ RemoteException -> 0x0036, RemoteException -> 0x0036 }
    L_0x001e:
        r0 = r16;
        r12 = r0.mStartedUsers;	 Catch:{ all -> 0x0055 }
        r0 = r17;
        r11 = r12.get(r0);	 Catch:{ all -> 0x0055 }
        r11 = (com.android.server.am.UserState) r11;	 Catch:{ all -> 0x0055 }
        if (r11 != 0) goto L_0x0058;
    L_0x002c:
        r0 = r17;
        r1 = r20;
        notifyFinished(r0, r1);	 Catch:{ all -> 0x0055 }
        r12 = 0;
        monitor-exit(r13);
        return r12;
    L_0x0036:
        r4 = move-exception;
        r12 = TAG;	 Catch:{ all -> 0x0055 }
        r14 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0055 }
        r14.<init>();	 Catch:{ all -> 0x0055 }
        r15 = "Failed to unlock: ";
        r14 = r14.append(r15);	 Catch:{ all -> 0x0055 }
        r15 = r4.getMessage();	 Catch:{ all -> 0x0055 }
        r14 = r14.append(r15);	 Catch:{ all -> 0x0055 }
        r14 = r14.toString();	 Catch:{ all -> 0x0055 }
        android.util.Slog.w(r12, r14);	 Catch:{ all -> 0x0055 }
        goto L_0x001e;
    L_0x0055:
        r12 = move-exception;
        monitor-exit(r13);
        throw r12;
    L_0x0058:
        r12 = r11.mUnlockProgress;	 Catch:{ all -> 0x0055 }
        r0 = r20;
        r12.addListener(r0);	 Catch:{ all -> 0x0055 }
        if (r18 == 0) goto L_0x00d6;
    L_0x0061:
        r12 = 1;
    L_0x0062:
        r11.tokenProvided = r12;	 Catch:{ all -> 0x0055 }
        monitor-exit(r13);
        r0 = r16;
        r0.finishUserUnlocking(r11);
        r3 = new android.util.ArraySet;
        r3.<init>();
        r0 = r16;
        r13 = r0.mLock;
        monitor-enter(r13);
        r5 = 0;
    L_0x0075:
        r0 = r16;
        r12 = r0.mStartedUsers;	 Catch:{ all -> 0x00f2 }
        r12 = r12.size();	 Catch:{ all -> 0x00f2 }
        if (r5 >= r12) goto L_0x00d8;
    L_0x007f:
        r0 = r16;
        r12 = r0.mStartedUsers;	 Catch:{ all -> 0x00f2 }
        r9 = r12.keyAt(r5);	 Catch:{ all -> 0x00f2 }
        r0 = r16;
        r12 = r0.mInjector;	 Catch:{ all -> 0x00f2 }
        r12 = r12.getUserManager();	 Catch:{ all -> 0x00f2 }
        r6 = r12.getProfileParent(r9);	 Catch:{ all -> 0x00f2 }
        if (r6 == 0) goto L_0x00d3;
    L_0x0095:
        r12 = r6.id;	 Catch:{ all -> 0x00f2 }
        r0 = r17;
        if (r12 != r0) goto L_0x00d3;
    L_0x009b:
        r0 = r17;
        if (r9 == r0) goto L_0x00d3;
    L_0x009f:
        r12 = TAG;	 Catch:{ all -> 0x00f2 }
        r14 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00f2 }
        r14.<init>();	 Catch:{ all -> 0x00f2 }
        r15 = "User ";
        r14 = r14.append(r15);	 Catch:{ all -> 0x00f2 }
        r14 = r14.append(r9);	 Catch:{ all -> 0x00f2 }
        r15 = " (parent ";
        r14 = r14.append(r15);	 Catch:{ all -> 0x00f2 }
        r15 = r6.id;	 Catch:{ all -> 0x00f2 }
        r14 = r14.append(r15);	 Catch:{ all -> 0x00f2 }
        r15 = "): attempting unlock because parent was just unlocked";
        r14 = r14.append(r15);	 Catch:{ all -> 0x00f2 }
        r14 = r14.toString();	 Catch:{ all -> 0x00f2 }
        android.util.Slog.d(r12, r14);	 Catch:{ all -> 0x00f2 }
        r12 = java.lang.Integer.valueOf(r9);	 Catch:{ all -> 0x00f2 }
        r3.add(r12);	 Catch:{ all -> 0x00f2 }
    L_0x00d3:
        r5 = r5 + 1;
        goto L_0x0075;
    L_0x00d6:
        r12 = 0;
        goto L_0x0062;
    L_0x00d8:
        monitor-exit(r13);
        r7 = r3.size();
        r5 = 0;
    L_0x00de:
        if (r5 >= r7) goto L_0x00f5;
    L_0x00e0:
        r12 = r3.valueAt(r5);
        r12 = (java.lang.Integer) r12;
        r12 = r12.intValue();
        r0 = r16;
        r0.maybeUnlockUser(r12);
        r5 = r5 + 1;
        goto L_0x00de;
    L_0x00f2:
        r12 = move-exception;
        monitor-exit(r13);
        throw r12;
    L_0x00f5:
        r12 = 1;
        return r12;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.UserController.unlockUserCleared(int, byte[], byte[], android.os.IProgressListener):boolean");
    }

    void showUserSwitchDialog(Pair<UserInfo, UserInfo> fromToUserPair) {
        this.mInjector.showUserSwitchingDialog((UserInfo) fromToUserPair.first, (UserInfo) fromToUserPair.second);
    }

    void dispatchForegroundProfileChanged(int userId) {
        int observerCount = this.mUserSwitchObservers.beginBroadcast();
        for (int i = 0; i < observerCount; i++) {
            try {
                ((IUserSwitchObserver) this.mUserSwitchObservers.getBroadcastItem(i)).onForegroundProfileSwitch(userId);
            } catch (RemoteException e) {
            }
        }
        this.mUserSwitchObservers.finishBroadcast();
    }

    void dispatchUserSwitchComplete(int userId) {
        this.mInjector.getWindowManager().setSwitchingUser(false);
        int observerCount = this.mUserSwitchObservers.beginBroadcast();
        for (int i = 0; i < observerCount; i++) {
            try {
                ((IUserSwitchObserver) this.mUserSwitchObservers.getBroadcastItem(i)).onUserSwitchComplete(userId);
            } catch (RemoteException e) {
            }
        }
        this.mUserSwitchObservers.finishBroadcast();
    }

    void dispatchLockedBootComplete(int userId) {
        int observerCount = this.mUserSwitchObservers.beginBroadcast();
        for (int i = 0; i < observerCount; i++) {
            try {
                ((IUserSwitchObserver) this.mUserSwitchObservers.getBroadcastItem(i)).onLockedBootComplete(userId);
            } catch (RemoteException e) {
            }
        }
        this.mUserSwitchObservers.finishBroadcast();
    }

    private void stopBackgroundUsersIfEnforced(int oldUserId) {
        if (oldUserId != 0 && hasUserRestriction("no_run_in_background", oldUserId)) {
            synchronized (this.mLock) {
                stopUsersLocked(oldUserId, false, null);
            }
        }
    }

    void timeoutUserSwitch(UserState uss, int oldUserId, int newUserId) {
        synchronized (this.mLock) {
            Slog.wtf(TAG, "User switch timeout: from " + oldUserId + " to " + newUserId);
            sendContinueUserSwitchLocked(uss, oldUserId, newUserId);
        }
    }

    void dispatchUserSwitch(UserState uss, int oldUserId, int newUserId) {
        Slog.d(TAG, "Dispatch onUserSwitching oldUser #" + oldUserId + " newUser #" + newUserId);
        int observerCount = this.mUserSwitchObservers.beginBroadcast();
        if (observerCount > 0) {
            final ArraySet<String> curWaitingUserSwitchCallbacks = new ArraySet();
            synchronized (this.mLock) {
                uss.switching = true;
                this.mCurWaitingUserSwitchCallbacks = curWaitingUserSwitchCallbacks;
            }
            final AtomicInteger waitingCallbacksCount = new AtomicInteger(observerCount);
            final long dispatchStartedTime = SystemClock.elapsedRealtime();
            for (int i = 0; i < observerCount; i++) {
                try {
                    final String name = "#" + i + " " + this.mUserSwitchObservers.getBroadcastCookie(i);
                    synchronized (this.mLock) {
                        curWaitingUserSwitchCallbacks.add(name);
                    }
                    final UserState userState = uss;
                    final int i2 = oldUserId;
                    final int i3 = newUserId;
                    ((IUserSwitchObserver) this.mUserSwitchObservers.getBroadcastItem(i)).onUserSwitching(newUserId, new IRemoteCallback.Stub() {
                        /* JADX WARNING: inconsistent code. */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public void sendResult(android.os.Bundle r9) throws android.os.RemoteException {
                            /*
                            r8 = this;
                            r2 = com.android.server.am.UserController.this;
                            r3 = r2.mLock;
                            monitor-enter(r3);
                            r4 = android.os.SystemClock.elapsedRealtime();	 Catch:{ all -> 0x006c }
                            r6 = r4;	 Catch:{ all -> 0x006c }
                            r0 = r4 - r6;
                            r4 = 3000; // 0xbb8 float:4.204E-42 double:1.482E-320;
                            r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1));
                            if (r2 <= 0) goto L_0x0044;
                        L_0x0015:
                            r2 = com.android.server.am.UserController.TAG;	 Catch:{ all -> 0x006c }
                            r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x006c }
                            r4.<init>();	 Catch:{ all -> 0x006c }
                            r5 = "User switch timeout: observer ";
                            r4 = r4.append(r5);	 Catch:{ all -> 0x006c }
                            r5 = r6;	 Catch:{ all -> 0x006c }
                            r4 = r4.append(r5);	 Catch:{ all -> 0x006c }
                            r5 = " sent result after ";
                            r4 = r4.append(r5);	 Catch:{ all -> 0x006c }
                            r4 = r4.append(r0);	 Catch:{ all -> 0x006c }
                            r5 = " ms";
                            r4 = r4.append(r5);	 Catch:{ all -> 0x006c }
                            r4 = r4.toString();	 Catch:{ all -> 0x006c }
                            android.util.Slog.wtf(r2, r4);	 Catch:{ all -> 0x006c }
                        L_0x0044:
                            r2 = r7;	 Catch:{ all -> 0x006c }
                            r4 = com.android.server.am.UserController.this;	 Catch:{ all -> 0x006c }
                            r4 = r4.mCurWaitingUserSwitchCallbacks;	 Catch:{ all -> 0x006c }
                            if (r2 == r4) goto L_0x0050;
                        L_0x004e:
                            monitor-exit(r3);
                            return;
                        L_0x0050:
                            r2 = r7;	 Catch:{ all -> 0x006c }
                            r4 = r6;	 Catch:{ all -> 0x006c }
                            r2.remove(r4);	 Catch:{ all -> 0x006c }
                            r2 = r8;	 Catch:{ all -> 0x006c }
                            r2 = r2.decrementAndGet();	 Catch:{ all -> 0x006c }
                            if (r2 != 0) goto L_0x006a;
                        L_0x005f:
                            r2 = com.android.server.am.UserController.this;	 Catch:{ all -> 0x006c }
                            r4 = r9;	 Catch:{ all -> 0x006c }
                            r5 = r10;	 Catch:{ all -> 0x006c }
                            r6 = r11;	 Catch:{ all -> 0x006c }
                            r2.sendContinueUserSwitchLocked(r4, r5, r6);	 Catch:{ all -> 0x006c }
                        L_0x006a:
                            monitor-exit(r3);
                            return;
                        L_0x006c:
                            r2 = move-exception;
                            monitor-exit(r3);
                            throw r2;
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.UserController.8.sendResult(android.os.Bundle):void");
                        }
                    });
                } catch (RemoteException e) {
                }
            }
        } else {
            synchronized (this.mLock) {
                sendContinueUserSwitchLocked(uss, oldUserId, newUserId);
            }
        }
        this.mUserSwitchObservers.finishBroadcast();
    }

    void sendContinueUserSwitchLocked(UserState uss, int oldUserId, int newUserId) {
        this.mCurWaitingUserSwitchCallbacks = null;
        this.mHandler.removeMessages(36);
        this.mHandler.sendMessage(this.mHandler.obtainMessage(35, oldUserId, newUserId, uss));
    }

    void continueUserSwitch(UserState uss, int oldUserId, int newUserId) {
        Slog.d(TAG, "Continue user switch oldUser #" + oldUserId + ", newUser #" + newUserId);
        if (this.mUserSwitchUiEnabled) {
            synchronized (this.mLock) {
                this.mInjector.getWindowManager().stopFreezingScreen();
            }
        }
        uss.switching = false;
        this.mHandler.removeMessages(55);
        this.mHandler.sendMessage(this.mHandler.obtainMessage(55, newUserId, 0));
        stopGuestOrEphemeralUserIfBackground();
        stopBackgroundUsersIfEnforced(oldUserId);
    }

    void moveUserToForegroundLocked(UserState uss, int oldUserId, int newUserId) {
        if (this.mInjector.getActivityStackSupervisor().switchUserLocked(newUserId, uss)) {
            this.mInjector.startHomeActivityLocked(newUserId, "moveUserToForeground");
        } else {
            this.mInjector.getActivityStackSupervisor().resumeFocusedStackTopActivityLocked();
        }
        EventLogTags.writeAmSwitchUser(newUserId);
        sendUserSwitchBroadcastsLocked(oldUserId, newUserId);
    }

    void sendUserSwitchBroadcastsLocked(int oldUserId, int newUserId) {
        List<UserInfo> profiles;
        int count;
        int i;
        long ident = Binder.clearCallingIdentity();
        if (oldUserId >= 0) {
            try {
                profiles = this.mInjector.getUserManager().getProfiles(oldUserId, false);
                count = profiles.size();
                for (i = 0; i < count; i++) {
                    int profileUserId = ((UserInfo) profiles.get(i)).id;
                    Intent intent = new Intent("android.intent.action.USER_BACKGROUND");
                    intent.addFlags(1342177280);
                    intent.putExtra("android.intent.extra.user_handle", profileUserId);
                    this.mInjector.broadcastIntentLocked(intent, null, null, 0, null, null, null, -1, null, false, false, ActivityManagerService.MY_PID, 1000, profileUserId);
                }
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        }
        if (newUserId >= 0) {
            profiles = this.mInjector.getUserManager().getProfiles(newUserId, false);
            count = profiles.size();
            for (i = 0; i < count; i++) {
                profileUserId = ((UserInfo) profiles.get(i)).id;
                intent = new Intent("android.intent.action.USER_FOREGROUND");
                intent.addFlags(1342177280);
                intent.putExtra("android.intent.extra.user_handle", profileUserId);
                this.mInjector.broadcastIntentLocked(intent, null, null, 0, null, null, null, -1, null, false, false, ActivityManagerService.MY_PID, 1000, profileUserId);
            }
            intent = new Intent("android.intent.action.USER_SWITCHED");
            intent.addFlags(1342177280);
            intent.putExtra("android.intent.extra.user_handle", newUserId);
            Intent intent2 = intent;
            this.mInjector.broadcastIntentLocked(intent2, null, null, 0, null, null, new String[]{"android.permission.MANAGE_USERS"}, -1, null, false, false, ActivityManagerService.MY_PID, 1000, -1);
        }
        Binder.restoreCallingIdentity(ident);
    }

    int handleIncomingUser(int callingPid, int callingUid, int userId, boolean allowAll, int allowMode, String name, String callerPackage) {
        int callingUserId = UserHandle.getUserId(callingUid);
        if (callingUserId == userId) {
            return userId;
        }
        int targetUserId = unsafeConvertIncomingUserLocked(userId);
        if (!(callingUid == 0 || callingUid == 1000)) {
            boolean z;
            if (this.mInjector.checkComponentPermission("android.permission.INTERACT_ACROSS_USERS_FULL", callingPid, callingUid, -1, true) == 0) {
                z = true;
            } else if (allowMode == 2) {
                z = false;
            } else if (this.mInjector.checkComponentPermission("android.permission.INTERACT_ACROSS_USERS", callingPid, callingUid, -1, true) != 0) {
                z = false;
            } else if (allowMode == 0) {
                z = true;
            } else if (allowMode == 1) {
                z = isSameProfileGroup(callingUserId, targetUserId);
            } else {
                throw new IllegalArgumentException("Unknown mode: " + allowMode);
            }
            if (!z) {
                if (userId == -3) {
                    targetUserId = callingUserId;
                } else {
                    StringBuilder builder = new StringBuilder(128);
                    builder.append("Permission Denial: ");
                    builder.append(name);
                    if (callerPackage != null) {
                        builder.append(" from ");
                        builder.append(callerPackage);
                    }
                    builder.append(" asks to run as user ");
                    builder.append(userId);
                    builder.append(" but is calling from user ");
                    builder.append(UserHandle.getUserId(callingUid));
                    builder.append("; this requires ");
                    builder.append("android.permission.INTERACT_ACROSS_USERS_FULL");
                    if (allowMode != 2) {
                        builder.append(" or ");
                        builder.append("android.permission.INTERACT_ACROSS_USERS");
                    }
                    String msg = builder.toString();
                    Slog.w(TAG, msg);
                    throw new SecurityException(msg);
                }
            }
        }
        if (!allowAll && targetUserId < 0) {
            throw new IllegalArgumentException("Call does not support special user #" + targetUserId);
        } else if (callingUid != 2000 || targetUserId < 0 || !hasUserRestriction("no_debugging_features", targetUserId)) {
            return targetUserId;
        } else {
            throw new SecurityException("Shell does not have permission to access user " + targetUserId + "\n " + Debug.getCallers(3));
        }
    }

    int unsafeConvertIncomingUserLocked(int userId) {
        if (userId == -2 || userId == -3) {
            return getCurrentUserIdLocked();
        }
        return userId;
    }

    void registerUserSwitchObserver(IUserSwitchObserver observer, String name) {
        Preconditions.checkNotNull(name, "Observer name cannot be null");
        if (this.mInjector.checkCallingPermission("android.permission.INTERACT_ACROSS_USERS_FULL") != 0) {
            String msg = "Permission Denial: registerUserSwitchObserver() from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " requires " + "android.permission.INTERACT_ACROSS_USERS_FULL";
            Slog.w(TAG, msg);
            throw new SecurityException(msg);
        }
        this.mUserSwitchObservers.register(observer, name);
    }

    void unregisterUserSwitchObserver(IUserSwitchObserver observer) {
        this.mUserSwitchObservers.unregister(observer);
    }

    UserState getStartedUserStateLocked(int userId) {
        return (UserState) this.mStartedUsers.get(userId);
    }

    boolean hasStartedUserState(int userId) {
        return this.mStartedUsers.get(userId) != null;
    }

    private void updateStartedUserArrayLocked() {
        int i;
        int num = 0;
        for (i = 0; i < this.mStartedUsers.size(); i++) {
            UserState uss = (UserState) this.mStartedUsers.valueAt(i);
            if (!(uss.state == 4 || uss.state == 5)) {
                num++;
            }
        }
        this.mStartedUserArray = new int[num];
        num = 0;
        for (i = 0; i < this.mStartedUsers.size(); i++) {
            uss = (UserState) this.mStartedUsers.valueAt(i);
            if (!(uss.state == 4 || uss.state == 5)) {
                int num2 = num + 1;
                this.mStartedUserArray[num] = this.mStartedUsers.keyAt(i);
                num = num2;
            }
        }
    }

    void sendBootCompletedLocked(IIntentReceiver resultTo) {
        for (int i = 0; i < this.mStartedUsers.size(); i++) {
            finishUserBoot((UserState) this.mStartedUsers.valueAt(i), resultTo);
        }
    }

    void onSystemReady() {
        updateCurrentProfileIdsLocked();
    }

    private void updateCurrentProfileIdsLocked() {
        int i;
        List<UserInfo> profiles = this.mInjector.getUserManager().getProfiles(this.mCurrentUserId, false);
        int[] currentProfileIds = new int[profiles.size()];
        for (i = 0; i < currentProfileIds.length; i++) {
            currentProfileIds[i] = ((UserInfo) profiles.get(i)).id;
        }
        this.mCurrentProfileIds = currentProfileIds;
        synchronized (this.mUserProfileGroupIdsSelfLocked) {
            this.mUserProfileGroupIdsSelfLocked.clear();
            List<UserInfo> users = this.mInjector.getUserManager().getUsers(false);
            for (i = 0; i < users.size(); i++) {
                UserInfo user = (UserInfo) users.get(i);
                if (user.profileGroupId != -10000) {
                    this.mUserProfileGroupIdsSelfLocked.put(user.id, user.profileGroupId);
                }
            }
        }
    }

    int[] getStartedUserArrayLocked() {
        return this.mStartedUserArray;
    }

    boolean isUserRunningLocked(int userId, int flags) {
        boolean z = true;
        UserState state = getStartedUserStateLocked(userId);
        if (state == null) {
            return false;
        }
        if ((flags & 1) != 0) {
            return true;
        }
        if ((flags & 2) != 0) {
            switch (state.state) {
                case 0:
                case 1:
                    return true;
                default:
                    return false;
            }
        } else if ((flags & 8) != 0) {
            switch (state.state) {
                case 2:
                case 3:
                    return true;
                case 4:
                case 5:
                    return StorageManager.isUserKeyUnlocked(userId);
                default:
                    return false;
            }
        } else if ((flags & 4) != 0) {
            switch (state.state) {
                case 3:
                    return true;
                case 4:
                case 5:
                    return StorageManager.isUserKeyUnlocked(userId);
                default:
                    return false;
            }
        } else {
            if (state.state == 4 || state.state == 5) {
                z = false;
            }
            return z;
        }
    }

    UserInfo getCurrentUser() {
        if (this.mInjector.checkCallingPermission("android.permission.INTERACT_ACROSS_USERS") != 0 && this.mInjector.checkCallingPermission("android.permission.INTERACT_ACROSS_USERS_FULL") != 0) {
            String msg = "Permission Denial: getCurrentUser() from pid=" + Binder.getCallingPid() + ", uid=" + Binder.getCallingUid() + " requires " + "android.permission.INTERACT_ACROSS_USERS";
            Slog.w(TAG, msg);
            throw new SecurityException(msg);
        } else if (this.mTargetUserId == -10000) {
            return getUserInfo(this.mCurrentUserId);
        } else {
            UserInfo currentUserLocked;
            synchronized (this.mLock) {
                currentUserLocked = getCurrentUserLocked();
            }
            return currentUserLocked;
        }
    }

    UserInfo getCurrentUserLocked() {
        return getUserInfo(this.mTargetUserId != -10000 ? this.mTargetUserId : this.mCurrentUserId);
    }

    int getCurrentOrTargetUserIdLocked() {
        return this.mTargetUserId != -10000 ? this.mTargetUserId : this.mCurrentUserId;
    }

    int getCurrentUserIdLocked() {
        return this.mCurrentUserId;
    }

    private boolean isCurrentUserLocked(int userId) {
        return userId == getCurrentOrTargetUserIdLocked();
    }

    int setTargetUserIdLocked(int targetUserId) {
        this.mTargetUserId = targetUserId;
        return targetUserId;
    }

    int[] getUsers() {
        UserManagerService ums = this.mInjector.getUserManager();
        if (ums != null) {
            return ums.getUserIds();
        }
        return new int[]{0};
    }

    UserInfo getUserInfo(int userId) {
        return this.mInjector.getUserManager().getUserInfo(userId);
    }

    int[] getUserIds() {
        return this.mInjector.getUserManager().getUserIds();
    }

    boolean exists(int userId) {
        return this.mInjector.getUserManager().exists(userId);
    }

    boolean hasUserRestriction(String restriction, int userId) {
        return this.mInjector.getUserManager().hasUserRestriction(restriction, userId);
    }

    Set<Integer> getProfileIds(int userId) {
        Set<Integer> userIds = new HashSet();
        for (UserInfo user : this.mInjector.getUserManager().getProfiles(userId, false)) {
            userIds.add(Integer.valueOf(user.id));
        }
        return userIds;
    }

    boolean isSameProfileGroup(int callingUserId, int targetUserId) {
        boolean z = true;
        if (callingUserId == targetUserId) {
            return true;
        }
        synchronized (this.mUserProfileGroupIdsSelfLocked) {
            int callingProfile = this.mUserProfileGroupIdsSelfLocked.get(callingUserId, -10000);
            int targetProfile = this.mUserProfileGroupIdsSelfLocked.get(targetUserId, -10000);
            if (callingProfile == -10000) {
                z = false;
            } else if (callingProfile != targetProfile) {
                z = false;
            }
        }
        return z;
    }

    boolean isCurrentProfileLocked(int userId) {
        return ArrayUtils.contains(this.mCurrentProfileIds, userId);
    }

    int[] getCurrentProfileIdsLocked() {
        return this.mCurrentProfileIds;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    boolean shouldConfirmCredentials(int r5) {
        /*
        r4 = this;
        r1 = 0;
        r2 = r4.mLock;
        monitor-enter(r2);
        r3 = r4.mStartedUsers;	 Catch:{ all -> 0x0018 }
        r3 = r3.get(r5);	 Catch:{ all -> 0x0018 }
        if (r3 != 0) goto L_0x000e;
    L_0x000c:
        monitor-exit(r2);
        return r1;
    L_0x000e:
        monitor-exit(r2);
        r2 = r4.mLockPatternUtils;
        r2 = r2.isSeparateProfileChallengeEnabled(r5);
        if (r2 != 0) goto L_0x001b;
    L_0x0017:
        return r1;
    L_0x0018:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
    L_0x001b:
        r2 = r4.mInjector;
        r0 = r2.getKeyguardManager();
        r2 = r0.isDeviceLocked(r5);
        if (r2 == 0) goto L_0x002b;
    L_0x0027:
        r1 = r0.isDeviceSecure(r5);
    L_0x002b:
        return r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.UserController.shouldConfirmCredentials(int):boolean");
    }

    boolean isLockScreenDisabled(int userId) {
        return this.mLockPatternUtils.isLockScreenDisabled(userId);
    }

    void dump(PrintWriter pw, boolean dumpAll) {
        int i;
        pw.println("  mStartedUsers:");
        for (i = 0; i < this.mStartedUsers.size(); i++) {
            UserState uss = (UserState) this.mStartedUsers.valueAt(i);
            pw.print("    User #");
            pw.print(uss.mHandle.getIdentifier());
            pw.print(": ");
            uss.dump("", pw);
        }
        pw.print("  mStartedUserArray: [");
        for (i = 0; i < this.mStartedUserArray.length; i++) {
            if (i > 0) {
                pw.print(", ");
            }
            pw.print(this.mStartedUserArray[i]);
        }
        pw.println("]");
        pw.print("  mUserLru: [");
        for (i = 0; i < this.mUserLru.size(); i++) {
            if (i > 0) {
                pw.print(", ");
            }
            pw.print(this.mUserLru.get(i));
        }
        pw.println("]");
        if (dumpAll) {
            pw.print("  mStartedUserArray: ");
            pw.println(Arrays.toString(this.mStartedUserArray));
        }
        synchronized (this.mUserProfileGroupIdsSelfLocked) {
            if (this.mUserProfileGroupIdsSelfLocked.size() > 0) {
                pw.println("  mUserProfileGroupIds:");
                for (i = 0; i < this.mUserProfileGroupIdsSelfLocked.size(); i++) {
                    pw.print("    User #");
                    pw.print(this.mUserProfileGroupIdsSelfLocked.keyAt(i));
                    pw.print(" -> profile #");
                    pw.println(this.mUserProfileGroupIdsSelfLocked.valueAt(i));
                }
            }
        }
    }
}
