package com.android.server.am;

import android.app.ActivityManager;
import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.content.IIntentReceiver;
import android.content.IIntentSender;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.IPackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.Trace;
import android.os.UserHandle;
import android.util.EventLog;
import android.util.Slog;
import com.android.server.display.DisplayTransformManager;
import java.util.ArrayList;
import java.util.Set;

public final class BroadcastQueue {
    static final int BROADCAST_INTENT_MSG = 200;
    static final int BROADCAST_TIMEOUT_MSG = 201;
    static final int MAX_BROADCAST_HISTORY = (ActivityManager.isLowRamDeviceStatic() ? 10 : 50);
    static final int MAX_BROADCAST_SUMMARY_HISTORY = (ActivityManager.isLowRamDeviceStatic() ? 25 : DisplayTransformManager.LEVEL_COLOR_MATRIX_INVERT_COLOR);
    private static final String TAG = "BroadcastQueue";
    private static final String TAG_BROADCAST = (TAG + ActivityManagerDebugConfig.POSTFIX_BROADCAST);
    private static final String TAG_MU = "BroadcastQueue_MU";
    final BroadcastRecord[] mBroadcastHistory = new BroadcastRecord[MAX_BROADCAST_HISTORY];
    final Intent[] mBroadcastSummaryHistory = new Intent[MAX_BROADCAST_SUMMARY_HISTORY];
    boolean mBroadcastsScheduled = false;
    final boolean mDelayBehindServices;
    final BroadcastHandler mHandler;
    int mHistoryNext = 0;
    final ArrayList<BroadcastRecord> mOrderedBroadcasts = new ArrayList();
    final ArrayList<BroadcastRecord> mParallelBroadcasts = new ArrayList();
    BroadcastRecord mPendingBroadcast = null;
    int mPendingBroadcastRecvIndex;
    boolean mPendingBroadcastTimeoutMessage;
    final String mQueueName;
    final ActivityManagerService mService;
    final long[] mSummaryHistoryDispatchTime = new long[MAX_BROADCAST_SUMMARY_HISTORY];
    final long[] mSummaryHistoryEnqueueTime = new long[MAX_BROADCAST_SUMMARY_HISTORY];
    final long[] mSummaryHistoryFinishTime = new long[MAX_BROADCAST_SUMMARY_HISTORY];
    int mSummaryHistoryNext = 0;
    final long mTimeoutPeriod;

    private final class BroadcastHandler extends Handler {
        public BroadcastHandler(Looper looper) {
            super(looper, null, true);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 200:
                    BroadcastQueue.this.processNextBroadcast(true);
                    return;
                case BroadcastQueue.BROADCAST_TIMEOUT_MSG /*201*/:
                    synchronized (BroadcastQueue.this.mService) {
                        try {
                            ActivityManagerService.boostPriorityForLockedSection();
                            BroadcastQueue.this.broadcastTimeoutLocked(true);
                        } finally {
                            ActivityManagerService.resetPriorityAfterLockedSection();
                        }
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private final class AppNotResponding implements Runnable {
        private final String mAnnotation;
        private final ProcessRecord mApp;

        public AppNotResponding(ProcessRecord app, String annotation) {
            this.mApp = app;
            this.mAnnotation = annotation;
        }

        public void run() {
            BroadcastQueue.this.mService.mAppErrors.appNotResponding(this.mApp, null, null, false, this.mAnnotation);
        }
    }

    BroadcastQueue(ActivityManagerService service, Handler handler, String name, long timeoutPeriod, boolean allowDelayBehindServices) {
        this.mService = service;
        this.mHandler = new BroadcastHandler(handler.getLooper());
        this.mQueueName = name;
        this.mTimeoutPeriod = timeoutPeriod;
        this.mDelayBehindServices = allowDelayBehindServices;
    }

    public String toString() {
        return this.mQueueName;
    }

    public boolean isPendingBroadcastProcessLocked(int pid) {
        return this.mPendingBroadcast != null && this.mPendingBroadcast.curApp.pid == pid;
    }

    public void enqueueParallelBroadcastLocked(BroadcastRecord r) {
        this.mParallelBroadcasts.add(r);
        enqueueBroadcastHelper(r);
    }

    public void enqueueOrderedBroadcastLocked(BroadcastRecord r) {
        this.mOrderedBroadcasts.add(r);
        enqueueBroadcastHelper(r);
    }

    private void enqueueBroadcastHelper(BroadcastRecord r) {
        r.enqueueClockTime = System.currentTimeMillis();
        if (Trace.isTagEnabled(64)) {
            Trace.asyncTraceBegin(64, createBroadcastTraceTitle(r, 0), System.identityHashCode(r));
        }
    }

    public final BroadcastRecord replaceParallelBroadcastLocked(BroadcastRecord r) {
        return replaceBroadcastLocked(this.mParallelBroadcasts, r, "PARALLEL");
    }

    public final BroadcastRecord replaceOrderedBroadcastLocked(BroadcastRecord r) {
        return replaceBroadcastLocked(this.mOrderedBroadcasts, r, "ORDERED");
    }

    private BroadcastRecord replaceBroadcastLocked(ArrayList<BroadcastRecord> queue, BroadcastRecord r, String typeForLogging) {
        Intent intent = r.intent;
        for (int i = queue.size() - 1; i > 0; i--) {
            BroadcastRecord old = (BroadcastRecord) queue.get(i);
            if (old.userId == r.userId && intent.filterEquals(old.intent)) {
                queue.set(i, r);
                return old;
            }
        }
        return null;
    }

    private final void processCurBroadcastLocked(BroadcastRecord r, ProcessRecord app) throws RemoteException {
        if (app.thread == null) {
            throw new RemoteException();
        } else if (app.inFullBackup) {
            skipReceiverLocked(r);
        } else {
            r.receiver = app.thread.asBinder();
            r.curApp = app;
            app.curReceivers.add(r);
            app.forceProcessStateUpTo(12);
            this.mService.updateLruProcessLocked(app, false, null);
            this.mService.updateOomAdjLocked();
            r.intent.setComponent(r.curComponent);
            boolean started = false;
            try {
                this.mService.notifyPackageUse(r.intent.getComponent().getPackageName(), 3);
                app.thread.scheduleReceiver(new Intent(r.intent), r.curReceiver, this.mService.compatibilityInfoForPackageLocked(r.curReceiver.applicationInfo), r.resultCode, r.resultData, r.resultExtras, r.ordered, r.userId, app.repProcState);
                started = true;
            } finally {
                if (!started) {
                    r.receiver = null;
                    r.curApp = null;
                    app.curReceivers.remove(r);
                }
            }
        }
    }

    public boolean sendPendingBroadcastsLocked(ProcessRecord app) {
        boolean didSomething = false;
        BroadcastRecord br = this.mPendingBroadcast;
        if (br != null && br.curApp.pid == app.pid) {
            if (br.curApp != app) {
                Slog.e(TAG, "App mismatch when sending pending broadcast to " + app.processName + ", intended target is " + br.curApp.processName);
                return false;
            }
            try {
                this.mPendingBroadcast = null;
                processCurBroadcastLocked(br, app);
                didSomething = true;
            } catch (Exception e) {
                Slog.w(TAG, "Exception in new application when starting receiver " + br.curComponent.flattenToShortString(), e);
                logBroadcastReceiverDiscardLocked(br);
                finishReceiverLocked(br, br.resultCode, br.resultData, br.resultExtras, br.resultAbort, false);
                scheduleBroadcastsLocked();
                br.state = 0;
                throw new RuntimeException(e.getMessage());
            }
        }
        return didSomething;
    }

    public void skipPendingBroadcastLocked(int pid) {
        BroadcastRecord br = this.mPendingBroadcast;
        if (br != null && br.curApp.pid == pid) {
            br.state = 0;
            br.nextReceiver = this.mPendingBroadcastRecvIndex;
            this.mPendingBroadcast = null;
            scheduleBroadcastsLocked();
        }
    }

    public void skipCurrentReceiverLocked(ProcessRecord app) {
        BroadcastRecord r = null;
        if (this.mOrderedBroadcasts.size() > 0) {
            BroadcastRecord br = (BroadcastRecord) this.mOrderedBroadcasts.get(0);
            if (br.curApp == app) {
                r = br;
            }
        }
        if (r == null && this.mPendingBroadcast != null && this.mPendingBroadcast.curApp == app) {
            r = this.mPendingBroadcast;
        }
        if (r != null) {
            skipReceiverLocked(r);
        }
    }

    private void skipReceiverLocked(BroadcastRecord r) {
        logBroadcastReceiverDiscardLocked(r);
        finishReceiverLocked(r, r.resultCode, r.resultData, r.resultExtras, r.resultAbort, false);
        scheduleBroadcastsLocked();
    }

    public void scheduleBroadcastsLocked() {
        if (!this.mBroadcastsScheduled) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(200, this));
            this.mBroadcastsScheduled = true;
        }
    }

    public BroadcastRecord getMatchingOrderedReceiver(IBinder receiver) {
        if (this.mOrderedBroadcasts.size() > 0) {
            BroadcastRecord r = (BroadcastRecord) this.mOrderedBroadcasts.get(0);
            if (r == null || r.receiver != receiver) {
                return null;
            }
            return r;
        }
        return null;
    }

    public boolean finishReceiverLocked(BroadcastRecord r, int resultCode, String resultData, Bundle resultExtras, boolean resultAbort, boolean waitForServices) {
        int state = r.state;
        ActivityInfo receiver = r.curReceiver;
        r.state = 0;
        if (state == 0) {
            Slog.w(TAG, "finishReceiver [" + this.mQueueName + "] called but state is IDLE");
        }
        r.receiver = null;
        r.intent.setComponent(null);
        if (r.curApp != null && r.curApp.curReceivers.contains(r)) {
            r.curApp.curReceivers.remove(r);
        }
        if (r.curFilter != null) {
            r.curFilter.receiverList.curBroadcast = null;
        }
        r.curFilter = null;
        r.curReceiver = null;
        r.curApp = null;
        this.mPendingBroadcast = null;
        r.resultCode = resultCode;
        r.resultData = resultData;
        r.resultExtras = resultExtras;
        if (resultAbort && (r.intent.getFlags() & 134217728) == 0) {
            r.resultAbort = resultAbort;
        } else {
            r.resultAbort = false;
        }
        if (waitForServices && r.curComponent != null && r.queue.mDelayBehindServices && r.queue.mOrderedBroadcasts.size() > 0 && r.queue.mOrderedBroadcasts.get(0) == r) {
            ActivityInfo activityInfo;
            if (r.nextReceiver < r.receivers.size()) {
                Object obj = r.receivers.get(r.nextReceiver);
                activityInfo = obj instanceof ActivityInfo ? (ActivityInfo) obj : null;
            } else {
                activityInfo = null;
            }
            if ((receiver == null || r0 == null || receiver.applicationInfo.uid != r0.applicationInfo.uid || (receiver.processName.equals(r0.processName) ^ 1) != 0) && this.mService.mServices.hasBackgroundServicesLocked(r.userId)) {
                Slog.i(TAG, "Delay finish: " + r.curComponent.flattenToShortString());
                r.state = 4;
                return false;
            }
        }
        r.curComponent = null;
        boolean z = state != 1 ? state == 3 : true;
        return z;
    }

    public void backgroundServicesFinishedLocked(int userId) {
        if (this.mOrderedBroadcasts.size() > 0) {
            BroadcastRecord br = (BroadcastRecord) this.mOrderedBroadcasts.get(0);
            if (br.userId == userId && br.state == 4) {
                Slog.i(TAG, "Resuming delayed broadcast");
                br.curComponent = null;
                br.state = 0;
                processNextBroadcast(false);
            }
        }
    }

    void performReceiveLocked(ProcessRecord app, IIntentReceiver receiver, Intent intent, int resultCode, String data, Bundle extras, boolean ordered, boolean sticky, int sendingUser) throws RemoteException {
        if (app == null) {
            receiver.performReceive(intent, resultCode, data, extras, ordered, sticky, sendingUser);
        } else if (app.thread != null) {
            try {
                app.thread.scheduleRegisteredReceiver(receiver, intent, resultCode, data, extras, ordered, sticky, sendingUser, app.repProcState);
            } catch (RemoteException ex) {
                synchronized (this.mService) {
                    ActivityManagerService.boostPriorityForLockedSection();
                    Slog.w(TAG, "Can't deliver broadcast to " + app.processName + " (pid " + app.pid + "). Crashing it.");
                    app.scheduleCrash("can't deliver broadcast");
                    throw ex;
                }
            } finally {
                ActivityManagerService.resetPriorityAfterLockedSection();
            }
        } else {
            throw new RemoteException("app.thread must not be null");
        }
    }

    private void deliverToRegisteredReceiverLocked(BroadcastRecord r, BroadcastFilter filter, boolean ordered, int index) {
        boolean skip = false;
        if (filter.requiredPermission != null) {
            if (this.mService.checkComponentPermission(filter.requiredPermission, r.callingPid, r.callingUid, -1, true) != 0) {
                Slog.w(TAG, "Permission Denial: broadcasting " + r.intent.toString() + " from " + r.callerPackage + " (pid=" + r.callingPid + ", uid=" + r.callingUid + ")" + " requires " + filter.requiredPermission + " due to registered receiver " + filter);
                skip = true;
            } else {
                int opCode = AppOpsManager.permissionToOpCode(filter.requiredPermission);
                if (opCode != -1) {
                    if (this.mService.mAppOpsService.noteOperation(opCode, r.callingUid, r.callerPackage) != 0) {
                        Slog.w(TAG, "Appop Denial: broadcasting " + r.intent.toString() + " from " + r.callerPackage + " (pid=" + r.callingPid + ", uid=" + r.callingUid + ")" + " requires appop " + AppOpsManager.permissionToOp(filter.requiredPermission) + " due to registered receiver " + filter);
                        skip = true;
                    }
                }
            }
        }
        if (!skip && r.requiredPermissions != null && r.requiredPermissions.length > 0) {
            int i = 0;
            while (i < r.requiredPermissions.length) {
                String requiredPermission = r.requiredPermissions[i];
                if (this.mService.checkComponentPermission(requiredPermission, filter.receiverList.pid, filter.receiverList.uid, -1, true) == 0) {
                    int appOp = AppOpsManager.permissionToOpCode(requiredPermission);
                    if (appOp != -1 && appOp != r.appOp && this.mService.mAppOpsService.noteOperation(appOp, filter.receiverList.uid, filter.packageName) != 0) {
                        Slog.w(TAG, "Appop Denial: receiving " + r.intent.toString() + " to " + filter.receiverList.app + " (pid=" + filter.receiverList.pid + ", uid=" + filter.receiverList.uid + ")" + " requires appop " + AppOpsManager.permissionToOp(requiredPermission) + " due to sender " + r.callerPackage + " (uid " + r.callingUid + ")");
                        skip = true;
                        break;
                    }
                    i++;
                } else {
                    Slog.w(TAG, "Permission Denial: receiving " + r.intent.toString() + " to " + filter.receiverList.app + " (pid=" + filter.receiverList.pid + ", uid=" + filter.receiverList.uid + ")" + " requires " + requiredPermission + " due to sender " + r.callerPackage + " (uid " + r.callingUid + ")");
                    skip = true;
                    break;
                }
            }
        }
        if (!skip && ((r.requiredPermissions == null || r.requiredPermissions.length == 0) && this.mService.checkComponentPermission(null, filter.receiverList.pid, filter.receiverList.uid, -1, true) != 0)) {
            Slog.w(TAG, "Permission Denial: security check failed when receiving " + r.intent.toString() + " to " + filter.receiverList.app + " (pid=" + filter.receiverList.pid + ", uid=" + filter.receiverList.uid + ")" + " due to sender " + r.callerPackage + " (uid " + r.callingUid + ")");
            skip = true;
        }
        if (!(skip || r.appOp == -1 || this.mService.mAppOpsService.noteOperation(r.appOp, filter.receiverList.uid, filter.packageName) == 0)) {
            Slog.w(TAG, "Appop Denial: receiving " + r.intent.toString() + " to " + filter.receiverList.app + " (pid=" + filter.receiverList.pid + ", uid=" + filter.receiverList.uid + ")" + " requires appop " + AppOpsManager.opToName(r.appOp) + " due to sender " + r.callerPackage + " (uid " + r.callingUid + ")");
            skip = true;
        }
        if (!this.mService.mIntentFirewall.checkBroadcast(r.intent, r.callingUid, r.callingPid, r.resolvedType, filter.receiverList.uid)) {
            skip = true;
        }
        if (!skip && (filter.receiverList.app == null || filter.receiverList.app.killed || filter.receiverList.app.crashing)) {
            Slog.w(TAG, "Skipping deliver [" + this.mQueueName + "] " + r + " to " + filter.receiverList + ": process gone or crashing");
            skip = true;
        }
        boolean visibleToInstantApps = (r.intent.getFlags() & DumpState.DUMP_COMPILER_STATS) != 0;
        if (!(skip || (visibleToInstantApps ^ 1) == 0 || !filter.instantApp || filter.receiverList.uid == r.callingUid)) {
            Slog.w(TAG, "Instant App Denial: receiving " + r.intent.toString() + " to " + filter.receiverList.app + " (pid=" + filter.receiverList.pid + ", uid=" + filter.receiverList.uid + ")" + " due to sender " + r.callerPackage + " (uid " + r.callingUid + ")" + " not specifying FLAG_RECEIVER_VISIBLE_TO_INSTANT_APPS");
            skip = true;
        }
        if (!(skip || (filter.visibleToInstantApp ^ 1) == 0 || !r.callerInstantApp || filter.receiverList.uid == r.callingUid)) {
            Slog.w(TAG, "Instant App Denial: receiving " + r.intent.toString() + " to " + filter.receiverList.app + " (pid=" + filter.receiverList.pid + ", uid=" + filter.receiverList.uid + ")" + " requires receiver be visible to instant apps" + " due to sender " + r.callerPackage + " (uid " + r.callingUid + ")");
            skip = true;
        }
        if (skip) {
            r.delivery[index] = 2;
            return;
        }
        if (this.mService.mPermissionReviewRequired) {
            if (!requestStartTargetPermissionsReviewIfNeededLocked(r, filter.packageName, filter.owningUserId)) {
                r.delivery[index] = 2;
                return;
            }
        }
        r.delivery[index] = 1;
        if (ordered) {
            r.receiver = filter.receiverList.receiver.asBinder();
            r.curFilter = filter;
            filter.receiverList.curBroadcast = r;
            r.state = 2;
            if (filter.receiverList.app != null) {
                r.curApp = filter.receiverList.app;
                filter.receiverList.app.curReceivers.add(r);
                this.mService.updateOomAdjLocked(r.curApp, true);
            }
        }
        try {
            if (filter.receiverList.app == null || !filter.receiverList.app.inFullBackup) {
                performReceiveLocked(filter.receiverList.app, filter.receiverList.receiver, new Intent(r.intent), r.resultCode, r.resultData, r.resultExtras, r.ordered, r.initialSticky, r.userId);
            } else if (ordered) {
                skipReceiverLocked(r);
            }
            if (ordered) {
                r.state = 3;
            }
        } catch (RemoteException e) {
            Slog.w(TAG, "Failure sending broadcast " + r.intent, e);
            if (ordered) {
                r.receiver = null;
                r.curFilter = null;
                filter.receiverList.curBroadcast = null;
                if (filter.receiverList.app != null) {
                    filter.receiverList.app.curReceivers.remove(r);
                }
            }
        }
    }

    private boolean requestStartTargetPermissionsReviewIfNeededLocked(BroadcastRecord receiverRecord, String receivingPackageName, int receivingUserId) {
        if (!this.mService.getPackageManagerInternalLocked().isPermissionsReviewRequired(receivingPackageName, receivingUserId)) {
            return true;
        }
        boolean callerForeground = receiverRecord.callerApp != null ? receiverRecord.callerApp.setSchedGroup != 0 : true;
        if (!callerForeground || receiverRecord.intent.getComponent() == null) {
            Slog.w(TAG, "u" + receivingUserId + " Receiving a broadcast in package" + receivingPackageName + " requires a permissions review");
        } else {
            IIntentSender target = this.mService.getIntentSenderLocked(1, receiverRecord.callerPackage, receiverRecord.callingUid, receiverRecord.userId, null, null, 0, new Intent[]{receiverRecord.intent}, new String[]{receiverRecord.intent.resolveType(this.mService.mContext.getContentResolver())}, 1409286144, null);
            final Intent intent = new Intent("android.intent.action.REVIEW_PERMISSIONS");
            intent.addFlags(276824064);
            intent.putExtra("android.intent.extra.PACKAGE_NAME", receivingPackageName);
            intent.putExtra("android.intent.extra.INTENT", new IntentSender(target));
            final int i = receivingUserId;
            this.mHandler.post(new Runnable() {
                public void run() {
                    BroadcastQueue.this.mService.mContext.startActivityAsUser(intent, new UserHandle(i));
                }
            });
        }
        return false;
    }

    final void scheduleTempWhitelistLocked(int uid, long duration, BroadcastRecord r) {
        if (duration > 2147483647L) {
            duration = 2147483647L;
        }
        StringBuilder b = new StringBuilder();
        b.append("broadcast:");
        UserHandle.formatUid(b, r.callingUid);
        b.append(":");
        if (r.intent.getAction() != null) {
            b.append(r.intent.getAction());
        } else if (r.intent.getComponent() != null) {
            r.intent.getComponent().appendShortString(b);
        } else if (r.intent.getData() != null) {
            b.append(r.intent.getData());
        }
        this.mService.tempWhitelistUidLocked(uid, duration, b.toString());
    }

    final boolean isSignaturePerm(String[] perms) {
        if (perms == null) {
            return false;
        }
        IPackageManager pm = AppGlobals.getPackageManager();
        int i = perms.length - 1;
        while (i >= 0) {
            try {
                if ((pm.getPermissionInfo(perms[i], "android", 0).protectionLevel & 31) != 2) {
                    return false;
                }
                i--;
            } catch (RemoteException e) {
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    final void processNextBroadcast(boolean r51) {
        /*
        r50 = this;
        r0 = r50;
        r0 = r0.mService;
        r47 = r0;
        monitor-enter(r47);
        com.android.server.am.ActivityManagerService.boostPriorityForLockedSection();	 Catch:{ all -> 0x009b }
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4.updateCpuStats();	 Catch:{ all -> 0x009b }
        if (r51 == 0) goto L_0x0018;
    L_0x0013:
        r4 = 0;
        r0 = r50;
        r0.mBroadcastsScheduled = r4;	 Catch:{ all -> 0x009b }
    L_0x0018:
        r0 = r50;
        r4 = r0.mParallelBroadcasts;	 Catch:{ all -> 0x009b }
        r4 = r4.size();	 Catch:{ all -> 0x009b }
        if (r4 <= 0) goto L_0x00a1;
    L_0x0022:
        r0 = r50;
        r4 = r0.mParallelBroadcasts;	 Catch:{ all -> 0x009b }
        r5 = 0;
        r40 = r4.remove(r5);	 Catch:{ all -> 0x009b }
        r40 = (com.android.server.am.BroadcastRecord) r40;	 Catch:{ all -> 0x009b }
        r4 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x009b }
        r0 = r40;
        r0.dispatchTime = r4;	 Catch:{ all -> 0x009b }
        r4 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x009b }
        r0 = r40;
        r0.dispatchClockTime = r4;	 Catch:{ all -> 0x009b }
        r4 = 64;
        r4 = android.os.Trace.isTagEnabled(r4);	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0069;
    L_0x0045:
        r4 = 0;
        r0 = r50;
        r1 = r40;
        r4 = r0.createBroadcastTraceTitle(r1, r4);	 Catch:{ all -> 0x009b }
        r5 = java.lang.System.identityHashCode(r40);	 Catch:{ all -> 0x009b }
        r6 = 64;
        android.os.Trace.asyncTraceEnd(r6, r4, r5);	 Catch:{ all -> 0x009b }
        r4 = 1;
        r0 = r50;
        r1 = r40;
        r4 = r0.createBroadcastTraceTitle(r1, r4);	 Catch:{ all -> 0x009b }
        r5 = java.lang.System.identityHashCode(r40);	 Catch:{ all -> 0x009b }
        r6 = 64;
        android.os.Trace.asyncTraceBegin(r6, r4, r5);	 Catch:{ all -> 0x009b }
    L_0x0069:
        r0 = r40;
        r4 = r0.receivers;	 Catch:{ all -> 0x009b }
        r14 = r4.size();	 Catch:{ all -> 0x009b }
        r27 = 0;
    L_0x0073:
        r0 = r27;
        if (r0 >= r14) goto L_0x0092;
    L_0x0077:
        r0 = r40;
        r4 = r0.receivers;	 Catch:{ all -> 0x009b }
        r0 = r27;
        r45 = r4.get(r0);	 Catch:{ all -> 0x009b }
        r45 = (com.android.server.am.BroadcastFilter) r45;	 Catch:{ all -> 0x009b }
        r4 = 0;
        r0 = r50;
        r1 = r40;
        r2 = r45;
        r3 = r27;
        r0.deliverToRegisteredReceiverLocked(r1, r2, r4, r3);	 Catch:{ all -> 0x009b }
        r27 = r27 + 1;
        goto L_0x0073;
    L_0x0092:
        r0 = r50;
        r1 = r40;
        r0.addBroadcastToHistoryLocked(r1);	 Catch:{ all -> 0x009b }
        goto L_0x0018;
    L_0x009b:
        r4 = move-exception;
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        throw r4;
    L_0x00a1:
        r0 = r50;
        r4 = r0.mPendingBroadcast;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0124;
    L_0x00a7:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r5 = r4.mPidsSelfLocked;	 Catch:{ all -> 0x009b }
        monitor-enter(r5);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x00d5 }
        r4 = r4.mPidsSelfLocked;	 Catch:{ all -> 0x00d5 }
        r0 = r50;
        r6 = r0.mPendingBroadcast;	 Catch:{ all -> 0x00d5 }
        r6 = r6.curApp;	 Catch:{ all -> 0x00d5 }
        r6 = r6.pid;	 Catch:{ all -> 0x00d5 }
        r39 = r4.get(r6);	 Catch:{ all -> 0x00d5 }
        r39 = (com.android.server.am.ProcessRecord) r39;	 Catch:{ all -> 0x00d5 }
        if (r39 == 0) goto L_0x00d2;
    L_0x00c4:
        r0 = r39;
        r0 = r0.crashing;	 Catch:{ all -> 0x00d5 }
        r30 = r0;
    L_0x00ca:
        monitor-exit(r5);	 Catch:{ all -> 0x009b }
        if (r30 != 0) goto L_0x00d8;
    L_0x00cd:
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x00d2:
        r30 = 1;
        goto L_0x00ca;
    L_0x00d5:
        r4 = move-exception;
        monitor-exit(r5);	 Catch:{ all -> 0x009b }
        throw r4;	 Catch:{ all -> 0x009b }
    L_0x00d8:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "pending app  [";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r6 = r0.mQueueName;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = "]";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r6 = r0.mPendingBroadcast;	 Catch:{ all -> 0x009b }
        r6 = r6.curApp;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " died before responding to broadcast";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r4 = r0.mPendingBroadcast;	 Catch:{ all -> 0x009b }
        r5 = 0;
        r4.state = r5;	 Catch:{ all -> 0x009b }
        r0 = r50;
        r4 = r0.mPendingBroadcast;	 Catch:{ all -> 0x009b }
        r0 = r50;
        r5 = r0.mPendingBroadcastRecvIndex;	 Catch:{ all -> 0x009b }
        r4.nextReceiver = r5;	 Catch:{ all -> 0x009b }
        r4 = 0;
        r0 = r50;
        r0.mPendingBroadcast = r4;	 Catch:{ all -> 0x009b }
    L_0x0124:
        r32 = 0;
    L_0x0126:
        r0 = r50;
        r4 = r0.mOrderedBroadcasts;	 Catch:{ all -> 0x009b }
        r4 = r4.size();	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x0145;
    L_0x0130:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4.scheduleAppGcsLocked();	 Catch:{ all -> 0x009b }
        if (r32 == 0) goto L_0x0140;
    L_0x0139:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4.updateOomAdjLocked();	 Catch:{ all -> 0x009b }
    L_0x0140:
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0145:
        r0 = r50;
        r4 = r0.mOrderedBroadcasts;	 Catch:{ all -> 0x009b }
        r5 = 0;
        r40 = r4.get(r5);	 Catch:{ all -> 0x009b }
        r40 = (com.android.server.am.BroadcastRecord) r40;	 Catch:{ all -> 0x009b }
        r26 = 0;
        r0 = r40;
        r4 = r0.receivers;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x022e;
    L_0x0158:
        r0 = r40;
        r4 = r0.receivers;	 Catch:{ all -> 0x009b }
        r36 = r4.size();	 Catch:{ all -> 0x009b }
    L_0x0160:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4 = r4.mProcessesReady;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0223;
    L_0x0168:
        r0 = r40;
        r4 = r0.dispatchTime;	 Catch:{ all -> 0x009b }
        r6 = 0;
        r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
        if (r4 <= 0) goto L_0x0223;
    L_0x0172:
        r34 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x009b }
        if (r36 <= 0) goto L_0x0223;
    L_0x0178:
        r0 = r40;
        r4 = r0.dispatchTime;	 Catch:{ all -> 0x009b }
        r0 = r50;
        r6 = r0.mTimeoutPeriod;	 Catch:{ all -> 0x009b }
        r8 = 2;
        r6 = r6 * r8;
        r0 = r36;
        r8 = (long) r0;	 Catch:{ all -> 0x009b }
        r6 = r6 * r8;
        r4 = r4 + r6;
        r4 = (r34 > r4 ? 1 : (r34 == r4 ? 0 : -1));
        if (r4 <= 0) goto L_0x0223;
    L_0x018c:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Hung broadcast [";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r6 = r0.mQueueName;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = "] discarded after timeout failure:";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " now=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r34;
        r5 = r5.append(r0);	 Catch:{ all -> 0x009b }
        r6 = " dispatchTime=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.dispatchTime;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " startTime=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.receiverTime;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " intent=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " numReceivers=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r36;
        r5 = r5.append(r0);	 Catch:{ all -> 0x009b }
        r6 = " nextReceiver=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.nextReceiver;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " state=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.state;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r4 = 0;
        r0 = r50;
        r0.broadcastTimeoutLocked(r4);	 Catch:{ all -> 0x009b }
        r26 = 1;
        r4 = 0;
        r0 = r40;
        r0.state = r4;	 Catch:{ all -> 0x009b }
    L_0x0223:
        r0 = r40;
        r4 = r0.state;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0232;
    L_0x0229:
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x022e:
        r36 = 0;
        goto L_0x0160;
    L_0x0232:
        r0 = r40;
        r4 = r0.receivers;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0240;
    L_0x0238:
        r0 = r40;
        r4 = r0.nextReceiver;	 Catch:{ all -> 0x009b }
        r0 = r36;
        if (r4 < r0) goto L_0x037f;
    L_0x0240:
        r0 = r40;
        r4 = r0.resultTo;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0273;
    L_0x0246:
        r0 = r40;
        r5 = r0.callerApp;	 Catch:{ RemoteException -> 0x0389 }
        r0 = r40;
        r6 = r0.resultTo;	 Catch:{ RemoteException -> 0x0389 }
        r7 = new android.content.Intent;	 Catch:{ RemoteException -> 0x0389 }
        r0 = r40;
        r4 = r0.intent;	 Catch:{ RemoteException -> 0x0389 }
        r7.<init>(r4);	 Catch:{ RemoteException -> 0x0389 }
        r0 = r40;
        r8 = r0.resultCode;	 Catch:{ RemoteException -> 0x0389 }
        r0 = r40;
        r9 = r0.resultData;	 Catch:{ RemoteException -> 0x0389 }
        r0 = r40;
        r10 = r0.resultExtras;	 Catch:{ RemoteException -> 0x0389 }
        r0 = r40;
        r13 = r0.userId;	 Catch:{ RemoteException -> 0x0389 }
        r11 = 0;
        r12 = 0;
        r4 = r50;
        r4.performReceiveLocked(r5, r6, r7, r8, r9, r10, r11, r12, r13);	 Catch:{ RemoteException -> 0x0389 }
        r4 = 0;
        r0 = r40;
        r0.resultTo = r4;	 Catch:{ RemoteException -> 0x0389 }
    L_0x0273:
        r50.cancelBroadcastTimeoutLocked();	 Catch:{ all -> 0x009b }
        r0 = r50;
        r1 = r40;
        r0.addBroadcastToHistoryLocked(r1);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x009b }
        r4 = r4.getComponent();	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x02c2;
    L_0x0287:
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x009b }
        r4 = r4.getPackage();	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x02c2;
    L_0x0291:
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x009b }
        r4 = r4.getFlags();	 Catch:{ all -> 0x009b }
        r5 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r4 = r4 & r5;
        if (r4 != 0) goto L_0x02c2;
    L_0x029e:
        r0 = r50;
        r5 = r0.mService;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x009b }
        r6 = r4.getAction();	 Catch:{ all -> 0x009b }
        r0 = r40;
        r7 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r8 = r0.manifestCount;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r9 = r0.manifestSkipCount;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r10 = r0.finishTime;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r12 = r0.dispatchTime;	 Catch:{ all -> 0x009b }
        r10 = r10 - r12;
        r5.addBroadcastStatLocked(r6, r7, r8, r9, r10);	 Catch:{ all -> 0x009b }
    L_0x02c2:
        r0 = r50;
        r4 = r0.mOrderedBroadcasts;	 Catch:{ all -> 0x009b }
        r5 = 0;
        r4.remove(r5);	 Catch:{ all -> 0x009b }
        r40 = 0;
        r32 = 1;
    L_0x02ce:
        if (r40 == 0) goto L_0x0126;
    L_0x02d0:
        r0 = r40;
        r0 = r0.nextReceiver;	 Catch:{ all -> 0x009b }
        r41 = r0;
        r4 = r41 + 1;
        r0 = r40;
        r0.nextReceiver = r4;	 Catch:{ all -> 0x009b }
        r4 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x009b }
        r0 = r40;
        r0.receiverTime = r4;	 Catch:{ all -> 0x009b }
        if (r41 != 0) goto L_0x0322;
    L_0x02e6:
        r0 = r40;
        r4 = r0.receiverTime;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r0.dispatchTime = r4;	 Catch:{ all -> 0x009b }
        r4 = java.lang.System.currentTimeMillis();	 Catch:{ all -> 0x009b }
        r0 = r40;
        r0.dispatchClockTime = r4;	 Catch:{ all -> 0x009b }
        r4 = 64;
        r4 = android.os.Trace.isTagEnabled(r4);	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0322;
    L_0x02fe:
        r4 = 0;
        r0 = r50;
        r1 = r40;
        r4 = r0.createBroadcastTraceTitle(r1, r4);	 Catch:{ all -> 0x009b }
        r5 = java.lang.System.identityHashCode(r40);	 Catch:{ all -> 0x009b }
        r6 = 64;
        android.os.Trace.asyncTraceEnd(r6, r4, r5);	 Catch:{ all -> 0x009b }
        r4 = 1;
        r0 = r50;
        r1 = r40;
        r4 = r0.createBroadcastTraceTitle(r1, r4);	 Catch:{ all -> 0x009b }
        r5 = java.lang.System.identityHashCode(r40);	 Catch:{ all -> 0x009b }
        r6 = 64;
        android.os.Trace.asyncTraceBegin(r6, r4, r5);	 Catch:{ all -> 0x009b }
    L_0x0322:
        r0 = r50;
        r4 = r0.mPendingBroadcastTimeoutMessage;	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x0339;
    L_0x0328:
        r0 = r40;
        r4 = r0.receiverTime;	 Catch:{ all -> 0x009b }
        r0 = r50;
        r6 = r0.mTimeoutPeriod;	 Catch:{ all -> 0x009b }
        r48 = r4 + r6;
        r0 = r50;
        r1 = r48;
        r0.setBroadcastTimeoutLocked(r1);	 Catch:{ all -> 0x009b }
    L_0x0339:
        r0 = r40;
        r0 = r0.options;	 Catch:{ all -> 0x009b }
        r18 = r0;
        r0 = r40;
        r4 = r0.receivers;	 Catch:{ all -> 0x009b }
        r0 = r41;
        r33 = r4.get(r0);	 Catch:{ all -> 0x009b }
        r0 = r33;
        r4 = r0 instanceof com.android.server.am.BroadcastFilter;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x03dc;
    L_0x034f:
        r0 = r33;
        r0 = (com.android.server.am.BroadcastFilter) r0;	 Catch:{ all -> 0x009b }
        r25 = r0;
        r0 = r40;
        r4 = r0.ordered;	 Catch:{ all -> 0x009b }
        r0 = r50;
        r1 = r40;
        r2 = r25;
        r3 = r41;
        r0.deliverToRegisteredReceiverLocked(r1, r2, r4, r3);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r4 = r0.receiver;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0372;
    L_0x036a:
        r0 = r40;
        r4 = r0.ordered;	 Catch:{ all -> 0x009b }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x03c0;
    L_0x0372:
        r4 = 0;
        r0 = r40;
        r0.state = r4;	 Catch:{ all -> 0x009b }
        r50.scheduleBroadcastsLocked();	 Catch:{ all -> 0x009b }
    L_0x037a:
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x037f:
        r0 = r40;
        r4 = r0.resultAbort;	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x0240;
    L_0x0385:
        if (r26 == 0) goto L_0x02ce;
    L_0x0387:
        goto L_0x0240;
    L_0x0389:
        r20 = move-exception;
        r4 = 0;
        r0 = r40;
        r0.resultTo = r4;	 Catch:{ all -> 0x009b }
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Failure [";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r6 = r0.mQueueName;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = "] sending broadcast result of ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        r0 = r20;
        android.util.Slog.w(r4, r5, r0);	 Catch:{ all -> 0x009b }
        goto L_0x0273;
    L_0x03c0:
        if (r18 == 0) goto L_0x037a;
    L_0x03c2:
        r4 = r18.getTemporaryAppWhitelistDuration();	 Catch:{ all -> 0x009b }
        r6 = 0;
        r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
        if (r4 <= 0) goto L_0x037a;
    L_0x03cc:
        r0 = r25;
        r4 = r0.owningUid;	 Catch:{ all -> 0x009b }
        r6 = r18.getTemporaryAppWhitelistDuration();	 Catch:{ all -> 0x009b }
        r0 = r50;
        r1 = r40;
        r0.scheduleTempWhitelistLocked(r4, r6, r1);	 Catch:{ all -> 0x009b }
        goto L_0x037a;
    L_0x03dc:
        r0 = r33;
        r0 = (android.content.pm.ResolveInfo) r0;	 Catch:{ all -> 0x009b }
        r28 = r0;
        r19 = new android.content.ComponentName;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.applicationInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.packageName;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.name;	 Catch:{ all -> 0x009b }
        r0 = r19;
        r0.<init>(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 0;
        if (r18 == 0) goto L_0x0419;
    L_0x03fb:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.applicationInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.targetSdkVersion;	 Catch:{ all -> 0x009b }
        r5 = r18.getMinManifestReceiverApiLevel();	 Catch:{ all -> 0x009b }
        if (r4 < r5) goto L_0x0417;
    L_0x0409:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.applicationInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.targetSdkVersion;	 Catch:{ all -> 0x009b }
        r5 = r18.getMaxManifestReceiverApiLevel();	 Catch:{ all -> 0x009b }
        if (r4 <= r5) goto L_0x0419;
    L_0x0417:
        r44 = 1;
    L_0x0419:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.permission;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingPid;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r7 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r8 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r8 = r8.applicationInfo;	 Catch:{ all -> 0x009b }
        r8 = r8.uid;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r9 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r9 = r9.exported;	 Catch:{ all -> 0x009b }
        r38 = r4.checkComponentPermission(r5, r6, r7, r8, r9);	 Catch:{ all -> 0x009b }
        if (r44 != 0) goto L_0x096b;
    L_0x043f:
        if (r38 == 0) goto L_0x096b;
    L_0x0441:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.exported;	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x08f3;
    L_0x0449:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Permission Denial: broadcasting ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r6 = r6.toString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " from ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " (pid=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingPid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ", uid=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ")";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " is not exported from uid ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.applicationInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.uid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " due to receiver ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
    L_0x04c1:
        r44 = 1;
    L_0x04c3:
        if (r44 != 0) goto L_0x0570;
    L_0x04c5:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.applicationInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.uid;	 Catch:{ all -> 0x009b }
        r5 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        if (r4 == r5) goto L_0x0570;
    L_0x04d1:
        r0 = r40;
        r4 = r0.requiredPermissions;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0570;
    L_0x04d7:
        r0 = r40;
        r4 = r0.requiredPermissions;	 Catch:{ all -> 0x009b }
        r4 = r4.length;	 Catch:{ all -> 0x009b }
        if (r4 <= 0) goto L_0x0570;
    L_0x04de:
        r27 = 0;
    L_0x04e0:
        r0 = r40;
        r4 = r0.requiredPermissions;	 Catch:{ all -> 0x009b }
        r4 = r4.length;	 Catch:{ all -> 0x009b }
        r0 = r27;
        if (r0 >= r4) goto L_0x0570;
    L_0x04e9:
        r0 = r40;
        r4 = r0.requiredPermissions;	 Catch:{ all -> 0x009b }
        r43 = r4[r27];	 Catch:{ all -> 0x009b }
        r4 = android.app.AppGlobals.getPackageManager();	 Catch:{ RemoteException -> 0x0a18 }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ RemoteException -> 0x0a18 }
        r5 = r5.applicationInfo;	 Catch:{ RemoteException -> 0x0a18 }
        r5 = r5.packageName;	 Catch:{ RemoteException -> 0x0a18 }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ RemoteException -> 0x0a18 }
        r6 = r6.applicationInfo;	 Catch:{ RemoteException -> 0x0a18 }
        r6 = r6.uid;	 Catch:{ RemoteException -> 0x0a18 }
        r6 = android.os.UserHandle.getUserId(r6);	 Catch:{ RemoteException -> 0x0a18 }
        r0 = r43;
        r38 = r4.checkPermission(r0, r5, r6);	 Catch:{ RemoteException -> 0x0a18 }
    L_0x050d:
        if (r38 == 0) goto L_0x0a1d;
    L_0x050f:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Permission Denial: receiving ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " requires ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r43;
        r5 = r5.append(r0);	 Catch:{ all -> 0x009b }
        r6 = " due to sender ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " (uid ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ")";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
    L_0x0570:
        if (r44 != 0) goto L_0x05fe;
    L_0x0572:
        r0 = r40;
        r4 = r0.appOp;	 Catch:{ all -> 0x009b }
        r5 = -1;
        if (r4 == r5) goto L_0x05fe;
    L_0x0579:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4 = r4.mAppOpsService;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r5 = r0.appOp;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.applicationInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.uid;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r7 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r7 = r7.packageName;	 Catch:{ all -> 0x009b }
        r4 = r4.noteOperation(r5, r6, r7);	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x05fe;
    L_0x0597:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Appop Denial: receiving ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " requires appop ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.appOp;	 Catch:{ all -> 0x009b }
        r6 = android.app.AppOpsManager.opToName(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " due to sender ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " (uid ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ")";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
    L_0x05fe:
        if (r44 != 0) goto L_0x0624;
    L_0x0600:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4 = r4.mIntentFirewall;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r5 = r0.intent;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r7 = r0.callingPid;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r8 = r0.resolvedType;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r9 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r9 = r9.applicationInfo;	 Catch:{ all -> 0x009b }
        r9 = r9.uid;	 Catch:{ all -> 0x009b }
        r4 = r4.checkBroadcast(r5, r6, r7, r8, r9);	 Catch:{ all -> 0x009b }
        r44 = r4 ^ 1;
    L_0x0624:
        r31 = 0;
        r0 = r50;
        r4 = r0.mService;	 Catch:{ SecurityException -> 0x0ab3 }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ SecurityException -> 0x0ab3 }
        r5 = r5.processName;	 Catch:{ SecurityException -> 0x0ab3 }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ SecurityException -> 0x0ab3 }
        r6 = r6.applicationInfo;	 Catch:{ SecurityException -> 0x0ab3 }
        r0 = r28;
        r7 = r0.activityInfo;	 Catch:{ SecurityException -> 0x0ab3 }
        r7 = r7.name;	 Catch:{ SecurityException -> 0x0ab3 }
        r0 = r28;
        r8 = r0.activityInfo;	 Catch:{ SecurityException -> 0x0ab3 }
        r8 = r8.flags;	 Catch:{ SecurityException -> 0x0ab3 }
        r31 = r4.isSingleton(r5, r6, r7, r8);	 Catch:{ SecurityException -> 0x0ab3 }
    L_0x0646:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.flags;	 Catch:{ all -> 0x009b }
        r5 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r4 = r4 & r5;
        if (r4 == 0) goto L_0x0690;
    L_0x0651:
        r4 = "android.permission.INTERACT_ACROSS_USERS";
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.applicationInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.uid;	 Catch:{ all -> 0x009b }
        r4 = android.app.ActivityManager.checkUidPermission(r4, r5);	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0690;
    L_0x0662:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Permission Denial: Receiver ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " requests FLAG_SINGLE_USER, but app does not hold ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = "android.permission.INTERACT_ACROSS_USERS";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
    L_0x0690:
        if (r44 != 0) goto L_0x0707;
    L_0x0692:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.applicationInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.isInstantApp();	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0707;
    L_0x069e:
        r0 = r40;
        r4 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.applicationInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.uid;	 Catch:{ all -> 0x009b }
        if (r4 == r5) goto L_0x0707;
    L_0x06ac:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Instant App Denial: receiving ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " due to sender ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " (uid ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ")";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " Instant Apps do not support manifest receivers";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
    L_0x0707:
        if (r44 != 0) goto L_0x0783;
    L_0x0709:
        r0 = r40;
        r4 = r0.callerInstantApp;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0783;
    L_0x070f:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.flags;	 Catch:{ all -> 0x009b }
        r5 = 1048576; // 0x100000 float:1.469368E-39 double:5.180654E-318;
        r4 = r4 & r5;
        if (r4 != 0) goto L_0x0783;
    L_0x071a:
        r0 = r40;
        r4 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.applicationInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.uid;	 Catch:{ all -> 0x009b }
        if (r4 == r5) goto L_0x0783;
    L_0x0728:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Instant App Denial: receiving ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " requires receiver have visibleToInstantApps set";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " due to sender ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " (uid ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ")";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
    L_0x0783:
        if (r44 != 0) goto L_0x0ac2;
    L_0x0785:
        r0 = r40;
        r4 = r0.manifestCount;	 Catch:{ all -> 0x009b }
        r4 = r4 + 1;
        r0 = r40;
        r0.manifestCount = r4;	 Catch:{ all -> 0x009b }
    L_0x078f:
        r0 = r40;
        r4 = r0.curApp;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x07e0;
    L_0x0795:
        r0 = r40;
        r4 = r0.curApp;	 Catch:{ all -> 0x009b }
        r4 = r4.crashing;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x07e0;
    L_0x079d:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Skipping deliver ordered [";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r6 = r0.mQueueName;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = "] ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r5 = r5.append(r0);	 Catch:{ all -> 0x009b }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.curApp;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ": process crashing";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
    L_0x07e0:
        if (r44 != 0) goto L_0x0802;
    L_0x07e2:
        r29 = 0;
        r4 = android.app.AppGlobals.getPackageManager();	 Catch:{ Exception -> 0x0ace }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ Exception -> 0x0ace }
        r5 = r5.packageName;	 Catch:{ Exception -> 0x0ace }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ Exception -> 0x0ace }
        r6 = r6.applicationInfo;	 Catch:{ Exception -> 0x0ace }
        r6 = r6.uid;	 Catch:{ Exception -> 0x0ace }
        r6 = android.os.UserHandle.getUserId(r6);	 Catch:{ Exception -> 0x0ace }
        r29 = r4.isPackageAvailable(r5, r6);	 Catch:{ Exception -> 0x0ace }
    L_0x07fe:
        if (r29 != 0) goto L_0x0802;
    L_0x0800:
        r44 = 1;
    L_0x0802:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4 = r4.mPermissionReviewRequired;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x082c;
    L_0x080a:
        r4 = r44 ^ 1;
        if (r4 == 0) goto L_0x082c;
    L_0x080e:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.packageName;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.applicationInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.uid;	 Catch:{ all -> 0x009b }
        r5 = android.os.UserHandle.getUserId(r5);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r1 = r40;
        r4 = r0.requestStartTargetPermissionsReviewIfNeededLocked(r1, r4, r5);	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x082c;
    L_0x082a:
        r44 = 1;
    L_0x082c:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.applicationInfo;	 Catch:{ all -> 0x009b }
        r0 = r4.uid;	 Catch:{ all -> 0x009b }
        r42 = r0;
        r0 = r40;
        r4 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        if (r4 == r5) goto L_0x0861;
    L_0x083e:
        if (r31 == 0) goto L_0x0861;
    L_0x0840:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r5 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r0 = r42;
        r4 = r4.isValidSingletonCall(r5, r0);	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0861;
    L_0x0850:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = 0;
        r4 = r4.getActivityInfoForUser(r5, r6);	 Catch:{ all -> 0x009b }
        r0 = r28;
        r0.activityInfo = r4;	 Catch:{ all -> 0x009b }
    L_0x0861:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r0 = r4.processName;	 Catch:{ all -> 0x009b }
        r46 = r0;
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.applicationInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.uid;	 Catch:{ all -> 0x009b }
        r6 = 0;
        r0 = r46;
        r16 = r4.getProcessRecordLocked(r0, r5, r6);	 Catch:{ all -> 0x009b }
        if (r44 != 0) goto L_0x08d3;
    L_0x087e:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.applicationInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.uid;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.packageName;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r7 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r7 = r7.applicationInfo;	 Catch:{ all -> 0x009b }
        r7 = r7.targetSdkVersion;	 Catch:{ all -> 0x009b }
        r8 = -1;
        r9 = 1;
        r10 = 0;
        r15 = r4.getAppStartModeLocked(r5, r6, r7, r8, r9, r10);	 Catch:{ all -> 0x009b }
        if (r15 == 0) goto L_0x08d3;
    L_0x08a1:
        r4 = 3;
        if (r15 != r4) goto L_0x0af3;
    L_0x08a4:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Background execution disabled: receiving ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
    L_0x08d3:
        if (r44 == 0) goto L_0x0b73;
    L_0x08d5:
        r0 = r40;
        r4 = r0.delivery;	 Catch:{ all -> 0x009b }
        r5 = 2;
        r4[r41] = r5;	 Catch:{ all -> 0x009b }
        r4 = 0;
        r0 = r40;
        r0.receiver = r4;	 Catch:{ all -> 0x009b }
        r4 = 0;
        r0 = r40;
        r0.curFilter = r4;	 Catch:{ all -> 0x009b }
        r4 = 0;
        r0 = r40;
        r0.state = r4;	 Catch:{ all -> 0x009b }
        r50.scheduleBroadcastsLocked();	 Catch:{ all -> 0x009b }
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x08f3:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Permission Denial: broadcasting ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r6 = r6.toString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " from ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " (pid=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingPid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ", uid=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ")";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " requires ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.permission;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " due to receiver ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        goto L_0x04c1;
    L_0x096b:
        if (r44 != 0) goto L_0x04c3;
    L_0x096d:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.permission;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x04c3;
    L_0x0975:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r4 = r4.permission;	 Catch:{ all -> 0x009b }
        r37 = android.app.AppOpsManager.permissionToOpCode(r4);	 Catch:{ all -> 0x009b }
        r4 = -1;
        r0 = r37;
        if (r0 == r4) goto L_0x04c3;
    L_0x0984:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4 = r4.mAppOpsService;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r5 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r0 = r37;
        r4 = r4.noteOperation(r0, r5, r6);	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x04c3;
    L_0x099a:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Appop Denial: broadcasting ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r6 = r6.toString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " from ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " (pid=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingPid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ", uid=";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ")";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " requires appop ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.permission;	 Catch:{ all -> 0x009b }
        r6 = android.app.AppOpsManager.permissionToOp(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " due to registered receiver ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
        goto L_0x04c3;
    L_0x0a18:
        r20 = move-exception;
        r38 = -1;
        goto L_0x050d;
    L_0x0a1d:
        r17 = android.app.AppOpsManager.permissionToOpCode(r43);	 Catch:{ all -> 0x009b }
        r4 = -1;
        r0 = r17;
        if (r0 == r4) goto L_0x0aaf;
    L_0x0a26:
        r0 = r40;
        r4 = r0.appOp;	 Catch:{ all -> 0x009b }
        r0 = r17;
        if (r0 == r4) goto L_0x0aaf;
    L_0x0a2e:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r4 = r4.mAppOpsService;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.applicationInfo;	 Catch:{ all -> 0x009b }
        r5 = r5.uid;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.packageName;	 Catch:{ all -> 0x009b }
        r0 = r17;
        r4 = r4.noteOperation(r0, r5, r6);	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0aaf;
    L_0x0a4a:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Appop Denial: receiving ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " requires appop ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = android.app.AppOpsManager.permissionToOp(r43);	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " due to sender ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callerPackage;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " (uid ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ")";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
        goto L_0x0570;
    L_0x0aaf:
        r27 = r27 + 1;
        goto L_0x04e0;
    L_0x0ab3:
        r24 = move-exception;
        r4 = "BroadcastQueue";
        r5 = r24.getMessage();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
        goto L_0x0646;
    L_0x0ac2:
        r0 = r40;
        r4 = r0.manifestSkipCount;	 Catch:{ all -> 0x009b }
        r4 = r4 + 1;
        r0 = r40;
        r0.manifestSkipCount = r4;	 Catch:{ all -> 0x009b }
        goto L_0x078f;
    L_0x0ace:
        r21 = move-exception;
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Exception getting recipient info for ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.packageName;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        r0 = r21;
        android.util.Slog.w(r4, r5, r0);	 Catch:{ all -> 0x009b }
        goto L_0x07fe;
    L_0x0af3:
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x009b }
        r4 = r4.getFlags();	 Catch:{ all -> 0x009b }
        r5 = 8388608; // 0x800000 float:1.17549435E-38 double:4.144523E-317;
        r4 = r4 & r5;
        if (r4 != 0) goto L_0x0b2f;
    L_0x0b00:
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x009b }
        r4 = r4.getComponent();	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x08d3;
    L_0x0b0a:
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x009b }
        r4 = r4.getPackage();	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x08d3;
    L_0x0b14:
        r0 = r40;
        r4 = r0.intent;	 Catch:{ all -> 0x009b }
        r4 = r4.getFlags();	 Catch:{ all -> 0x009b }
        r5 = 16777216; // 0x1000000 float:2.3509887E-38 double:8.289046E-317;
        r4 = r4 & r5;
        if (r4 != 0) goto L_0x08d3;
    L_0x0b21:
        r0 = r40;
        r4 = r0.requiredPermissions;	 Catch:{ all -> 0x009b }
        r0 = r50;
        r4 = r0.isSignaturePerm(r4);	 Catch:{ all -> 0x009b }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x08d3;
    L_0x0b2f:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r5 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.getAction();	 Catch:{ all -> 0x009b }
        r6 = r19.getPackageName();	 Catch:{ all -> 0x009b }
        r4.addBackgroundCheckViolationLocked(r5, r6);	 Catch:{ all -> 0x009b }
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Background execution not allowed: receiving ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = r19.flattenToShortString();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r44 = 1;
        goto L_0x08d3;
    L_0x0b73:
        r0 = r40;
        r4 = r0.delivery;	 Catch:{ all -> 0x009b }
        r5 = 1;
        r4[r41] = r5;	 Catch:{ all -> 0x009b }
        r4 = 1;
        r0 = r40;
        r0.state = r4;	 Catch:{ all -> 0x009b }
        r0 = r19;
        r1 = r40;
        r1.curComponent = r0;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r0.curReceiver = r4;	 Catch:{ all -> 0x009b }
        if (r18 == 0) goto L_0x0ba6;
    L_0x0b8f:
        r4 = r18.getTemporaryAppWhitelistDuration();	 Catch:{ all -> 0x009b }
        r6 = 0;
        r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
        if (r4 <= 0) goto L_0x0ba6;
    L_0x0b99:
        r4 = r18.getTemporaryAppWhitelistDuration();	 Catch:{ all -> 0x009b }
        r0 = r50;
        r1 = r42;
        r2 = r40;
        r0.scheduleTempWhitelistLocked(r1, r4, r2);	 Catch:{ all -> 0x009b }
    L_0x0ba6:
        r4 = android.app.AppGlobals.getPackageManager();	 Catch:{ RemoteException -> 0x0d66, IllegalArgumentException -> 0x0bf5 }
        r0 = r40;
        r5 = r0.curComponent;	 Catch:{ RemoteException -> 0x0d66, IllegalArgumentException -> 0x0bf5 }
        r5 = r5.getPackageName();	 Catch:{ RemoteException -> 0x0d66, IllegalArgumentException -> 0x0bf5 }
        r0 = r40;
        r6 = r0.callingUid;	 Catch:{ RemoteException -> 0x0d66, IllegalArgumentException -> 0x0bf5 }
        r6 = android.os.UserHandle.getUserId(r6);	 Catch:{ RemoteException -> 0x0d66, IllegalArgumentException -> 0x0bf5 }
        r7 = 0;
        r4.setPackageStoppedState(r5, r7, r6);	 Catch:{ RemoteException -> 0x0d66, IllegalArgumentException -> 0x0bf5 }
    L_0x0bbe:
        if (r16 == 0) goto L_0x0ca3;
    L_0x0bc0:
        r0 = r16;
        r4 = r0.thread;	 Catch:{ all -> 0x009b }
        if (r4 == 0) goto L_0x0ca3;
    L_0x0bc6:
        r0 = r16;
        r4 = r0.killed;	 Catch:{ all -> 0x009b }
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x0ca3;
    L_0x0bce:
        r0 = r28;
        r4 = r0.activityInfo;	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        r4 = r4.packageName;	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        r5 = r5.applicationInfo;	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        r5 = r5.versionCode;	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        r0 = r50;
        r6 = r0.mService;	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        r6 = r6.mProcessStats;	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        r0 = r16;
        r0.addPackage(r4, r5, r6);	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        r0 = r50;
        r1 = r40;
        r2 = r16;
        r0.processCurBroadcastLocked(r1, r2);	 Catch:{ RemoteException -> 0x0c82, RuntimeException -> 0x0c26 }
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0bf5:
        r22 = move-exception;
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Failed trying to unstop package ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.curComponent;	 Catch:{ all -> 0x009b }
        r6 = r6.getPackageName();	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ": ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r22;
        r5 = r5.append(r0);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        goto L_0x0bbe;
    L_0x0c26:
        r23 = move-exception;
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Failed sending broadcast to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.curComponent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " with ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        r0 = r23;
        android.util.Slog.wtf(r4, r5, r0);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r1 = r40;
        r0.logBroadcastReceiverDiscardLocked(r1);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.resultCode;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r7 = r0.resultData;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r8 = r0.resultExtras;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r9 = r0.resultAbort;	 Catch:{ all -> 0x009b }
        r10 = 0;
        r4 = r50;
        r5 = r40;
        r4.finishReceiverLocked(r5, r6, r7, r8, r9, r10);	 Catch:{ all -> 0x009b }
        r50.scheduleBroadcastsLocked();	 Catch:{ all -> 0x009b }
        r4 = 0;
        r0 = r40;
        r0.state = r4;	 Catch:{ all -> 0x009b }
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0c82:
        r20 = move-exception;
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Exception when sending broadcast to ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.curComponent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        r0 = r20;
        android.util.Slog.w(r4, r5, r0);	 Catch:{ all -> 0x009b }
    L_0x0ca3:
        r0 = r50;
        r4 = r0.mService;	 Catch:{ all -> 0x009b }
        r0 = r28;
        r5 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r5.applicationInfo;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r5 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.getFlags();	 Catch:{ all -> 0x009b }
        r8 = r5 | 4;
        r9 = "broadcast";
        r0 = r40;
        r10 = r0.curComponent;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r5 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.getFlags();	 Catch:{ all -> 0x009b }
        r7 = 33554432; // 0x2000000 float:9.403955E-38 double:1.6578092E-316;
        r5 = r5 & r7;
        if (r5 == 0) goto L_0x0d52;
    L_0x0ccb:
        r11 = 1;
    L_0x0ccc:
        r7 = 1;
        r12 = 0;
        r13 = 0;
        r5 = r46;
        r4 = r4.startProcessLocked(r5, r6, r7, r8, r9, r10, r11, r12, r13);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r0.curApp = r4;	 Catch:{ all -> 0x009b }
        if (r4 != 0) goto L_0x0d55;
    L_0x0cdb:
        r4 = "BroadcastQueue";
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x009b }
        r5.<init>();	 Catch:{ all -> 0x009b }
        r6 = "Unable to launch app ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.applicationInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.packageName;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = "/";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r28;
        r6 = r0.activityInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.applicationInfo;	 Catch:{ all -> 0x009b }
        r6 = r6.uid;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = " for broadcast ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.intent;	 Catch:{ all -> 0x009b }
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r6 = ": process is bad";
        r5 = r5.append(r6);	 Catch:{ all -> 0x009b }
        r5 = r5.toString();	 Catch:{ all -> 0x009b }
        android.util.Slog.w(r4, r5);	 Catch:{ all -> 0x009b }
        r0 = r50;
        r1 = r40;
        r0.logBroadcastReceiverDiscardLocked(r1);	 Catch:{ all -> 0x009b }
        r0 = r40;
        r6 = r0.resultCode;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r7 = r0.resultData;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r8 = r0.resultExtras;	 Catch:{ all -> 0x009b }
        r0 = r40;
        r9 = r0.resultAbort;	 Catch:{ all -> 0x009b }
        r10 = 0;
        r4 = r50;
        r5 = r40;
        r4.finishReceiverLocked(r5, r6, r7, r8, r9, r10);	 Catch:{ all -> 0x009b }
        r50.scheduleBroadcastsLocked();	 Catch:{ all -> 0x009b }
        r4 = 0;
        r0 = r40;
        r0.state = r4;	 Catch:{ all -> 0x009b }
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0d52:
        r11 = 0;
        goto L_0x0ccc;
    L_0x0d55:
        r0 = r40;
        r1 = r50;
        r1.mPendingBroadcast = r0;	 Catch:{ all -> 0x009b }
        r0 = r41;
        r1 = r50;
        r1.mPendingBroadcastRecvIndex = r0;	 Catch:{ all -> 0x009b }
        monitor-exit(r47);
        com.android.server.am.ActivityManagerService.resetPriorityAfterLockedSection();
        return;
    L_0x0d66:
        r20 = move-exception;
        goto L_0x0bbe;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.BroadcastQueue.processNextBroadcast(boolean):void");
    }

    final void setBroadcastTimeoutLocked(long timeoutTime) {
        if (!this.mPendingBroadcastTimeoutMessage) {
            this.mHandler.sendMessageAtTime(this.mHandler.obtainMessage(BROADCAST_TIMEOUT_MSG, this), timeoutTime);
            this.mPendingBroadcastTimeoutMessage = true;
        }
    }

    final void cancelBroadcastTimeoutLocked() {
        if (this.mPendingBroadcastTimeoutMessage) {
            this.mHandler.removeMessages(BROADCAST_TIMEOUT_MSG, this);
            this.mPendingBroadcastTimeoutMessage = false;
        }
    }

    final void broadcastTimeoutLocked(boolean fromMsg) {
        if (fromMsg) {
            this.mPendingBroadcastTimeoutMessage = false;
        }
        if (this.mOrderedBroadcasts.size() != 0) {
            long now = SystemClock.uptimeMillis();
            BroadcastRecord r = (BroadcastRecord) this.mOrderedBroadcasts.get(0);
            if (fromMsg) {
                if (this.mService.mProcessesReady) {
                    long timeoutTime = r.receiverTime + this.mTimeoutPeriod;
                    if (timeoutTime > now) {
                        setBroadcastTimeoutLocked(timeoutTime);
                        return;
                    }
                }
                return;
            }
            BroadcastRecord br = (BroadcastRecord) this.mOrderedBroadcasts.get(0);
            if (br.state == 4) {
                Slog.i(TAG, "Waited long enough for: " + (br.curComponent != null ? br.curComponent.flattenToShortString() : "(null)"));
                br.curComponent = null;
                br.state = 0;
                processNextBroadcast(false);
                return;
            }
            BroadcastFilter curReceiver;
            Slog.w(TAG, "Timeout of broadcast " + r + " - receiver=" + r.receiver + ", started " + (now - r.receiverTime) + "ms ago");
            r.receiverTime = now;
            r.anrCount++;
            ProcessRecord app = null;
            String anrMessage = null;
            if (r.nextReceiver > 0) {
                curReceiver = r.receivers.get(r.nextReceiver - 1);
                r.delivery[r.nextReceiver - 1] = 3;
            } else {
                curReceiver = r.curReceiver;
            }
            Slog.w(TAG, "Receiver during timeout of " + r + " : " + curReceiver);
            logBroadcastReceiverDiscardLocked(r);
            if (curReceiver == null || !(curReceiver instanceof BroadcastFilter)) {
                app = r.curApp;
            } else {
                BroadcastFilter bf = curReceiver;
                if (!(bf.receiverList.pid == 0 || bf.receiverList.pid == ActivityManagerService.MY_PID)) {
                    synchronized (this.mService.mPidsSelfLocked) {
                        app = (ProcessRecord) this.mService.mPidsSelfLocked.get(bf.receiverList.pid);
                    }
                }
            }
            if (app != null) {
                anrMessage = "Broadcast of " + r.intent.toString();
            }
            if (this.mPendingBroadcast == r) {
                this.mPendingBroadcast = null;
            }
            finishReceiverLocked(r, r.resultCode, r.resultData, r.resultExtras, r.resultAbort, false);
            scheduleBroadcastsLocked();
            if (anrMessage != null) {
                this.mHandler.post(new AppNotResponding(app, anrMessage));
            }
        }
    }

    private final int ringAdvance(int x, int increment, int ringSize) {
        x += increment;
        if (x < 0) {
            return ringSize - 1;
        }
        if (x >= ringSize) {
            return 0;
        }
        return x;
    }

    private final void addBroadcastToHistoryLocked(BroadcastRecord original) {
        if (original.callingUid >= 0) {
            original.finishTime = SystemClock.uptimeMillis();
            if (Trace.isTagEnabled(64)) {
                Trace.asyncTraceEnd(64, createBroadcastTraceTitle(original, 1), System.identityHashCode(original));
            }
            BroadcastRecord historyRecord = original.maybeStripForHistory();
            this.mBroadcastHistory[this.mHistoryNext] = historyRecord;
            this.mHistoryNext = ringAdvance(this.mHistoryNext, 1, MAX_BROADCAST_HISTORY);
            this.mBroadcastSummaryHistory[this.mSummaryHistoryNext] = historyRecord.intent;
            this.mSummaryHistoryEnqueueTime[this.mSummaryHistoryNext] = historyRecord.enqueueClockTime;
            this.mSummaryHistoryDispatchTime[this.mSummaryHistoryNext] = historyRecord.dispatchClockTime;
            this.mSummaryHistoryFinishTime[this.mSummaryHistoryNext] = System.currentTimeMillis();
            this.mSummaryHistoryNext = ringAdvance(this.mSummaryHistoryNext, 1, MAX_BROADCAST_SUMMARY_HISTORY);
        }
    }

    boolean cleanupDisabledPackageReceiversLocked(String packageName, Set<String> filterByClasses, int userId, boolean doit) {
        int i;
        boolean didSomething = false;
        for (i = this.mParallelBroadcasts.size() - 1; i >= 0; i--) {
            didSomething |= ((BroadcastRecord) this.mParallelBroadcasts.get(i)).cleanupDisabledPackageReceiversLocked(packageName, filterByClasses, userId, doit);
            if (!doit && didSomething) {
                return true;
            }
        }
        for (i = this.mOrderedBroadcasts.size() - 1; i >= 0; i--) {
            didSomething |= ((BroadcastRecord) this.mOrderedBroadcasts.get(i)).cleanupDisabledPackageReceiversLocked(packageName, filterByClasses, userId, doit);
            if (!doit && didSomething) {
                return true;
            }
        }
        return didSomething;
    }

    final void logBroadcastReceiverDiscardLocked(BroadcastRecord r) {
        int logIndex = r.nextReceiver - 1;
        if (logIndex < 0 || logIndex >= r.receivers.size()) {
            if (logIndex < 0) {
                Slog.w(TAG, "Discarding broadcast before first receiver is invoked: " + r);
            }
            EventLog.writeEvent(EventLogTags.AM_BROADCAST_DISCARD_APP, new Object[]{Integer.valueOf(-1), Integer.valueOf(System.identityHashCode(r)), r.intent.getAction(), Integer.valueOf(r.nextReceiver), "NONE"});
            return;
        }
        BroadcastFilter curReceiver = r.receivers.get(logIndex);
        if (curReceiver instanceof BroadcastFilter) {
            BroadcastFilter bf = curReceiver;
            EventLog.writeEvent(EventLogTags.AM_BROADCAST_DISCARD_FILTER, new Object[]{Integer.valueOf(bf.owningUserId), Integer.valueOf(System.identityHashCode(r)), r.intent.getAction(), Integer.valueOf(logIndex), Integer.valueOf(System.identityHashCode(bf))});
            return;
        }
        ResolveInfo ri = (ResolveInfo) curReceiver;
        EventLog.writeEvent(EventLogTags.AM_BROADCAST_DISCARD_APP, new Object[]{Integer.valueOf(UserHandle.getUserId(ri.activityInfo.applicationInfo.uid)), Integer.valueOf(System.identityHashCode(r)), r.intent.getAction(), Integer.valueOf(logIndex), ri.toString()});
    }

    private String createBroadcastTraceTitle(BroadcastRecord record, int state) {
        String str = "Broadcast %s from %s (%s) %s";
        Object[] objArr = new Object[4];
        objArr[0] = state == 0 ? "in queue" : "dispatched";
        objArr[1] = record.callerPackage == null ? "" : record.callerPackage;
        objArr[2] = record.callerApp == null ? "process unknown" : record.callerApp.toShortString();
        objArr[3] = record.intent == null ? "" : record.intent.getAction();
        return String.format(str, objArr);
    }

    final boolean isIdle() {
        if (this.mParallelBroadcasts.isEmpty() && this.mOrderedBroadcasts.isEmpty() && this.mPendingBroadcast == null) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    final boolean dumpLocked(java.io.FileDescriptor r19, java.io.PrintWriter r20, java.lang.String[] r21, int r22, boolean r23, java.lang.String r24, boolean r25) {
        /*
        r18 = this;
        r12 = new java.text.SimpleDateFormat;
        r13 = "yyyy-MM-dd HH:mm:ss";
        r12.<init>(r13);
        r0 = r18;
        r13 = r0.mParallelBroadcasts;
        r13 = r13.size();
        if (r13 > 0) goto L_0x001c;
    L_0x0012:
        r0 = r18;
        r13 = r0.mOrderedBroadcasts;
        r13 = r13.size();
        if (r13 <= 0) goto L_0x0044;
    L_0x001c:
        r8 = 0;
        r0 = r18;
        r13 = r0.mParallelBroadcasts;
        r13 = r13.size();
        r4 = r13 + -1;
    L_0x0027:
        if (r4 < 0) goto L_0x00ec;
    L_0x0029:
        r0 = r18;
        r13 = r0.mParallelBroadcasts;
        r2 = r13.get(r4);
        r2 = (com.android.server.am.BroadcastRecord) r2;
        if (r24 == 0) goto L_0x0085;
    L_0x0035:
        r13 = r2.callerPackage;
        r0 = r24;
        r13 = r0.equals(r13);
        r13 = r13 ^ 1;
        if (r13 == 0) goto L_0x0085;
    L_0x0041:
        r4 = r4 + -1;
        goto L_0x0027;
    L_0x0044:
        r0 = r18;
        r13 = r0.mPendingBroadcast;
        if (r13 != 0) goto L_0x001c;
    L_0x004a:
        r8 = 0;
        r4 = -1;
        r0 = r18;
        r7 = r0.mHistoryNext;
        r10 = r7;
    L_0x0051:
        r13 = MAX_BROADCAST_HISTORY;
        r14 = -1;
        r0 = r18;
        r10 = r0.ringAdvance(r10, r14, r13);
        r0 = r18;
        r13 = r0.mBroadcastHistory;
        r9 = r13[r10];
        if (r9 != 0) goto L_0x01e4;
    L_0x0062:
        if (r10 != r7) goto L_0x0051;
    L_0x0064:
        if (r24 != 0) goto L_0x0084;
    L_0x0066:
        r0 = r18;
        r10 = r0.mSummaryHistoryNext;
        r7 = r10;
        if (r23 == 0) goto L_0x02ce;
    L_0x006d:
        r8 = 0;
        r4 = -1;
        r11 = r10;
    L_0x0070:
        r13 = MAX_BROADCAST_SUMMARY_HISTORY;
        r14 = -1;
        r0 = r18;
        r11 = r0.ringAdvance(r11, r14, r13);
        r0 = r18;
        r13 = r0.mBroadcastSummaryHistory;
        r5 = r13[r11];
        if (r5 != 0) goto L_0x02e8;
    L_0x0081:
        if (r11 != r10) goto L_0x0070;
    L_0x0083:
        r10 = r11;
    L_0x0084:
        return r25;
    L_0x0085:
        if (r8 != 0) goto L_0x00b3;
    L_0x0087:
        if (r25 == 0) goto L_0x008c;
    L_0x0089:
        r20.println();
    L_0x008c:
        r25 = 1;
        r8 = 1;
        r13 = new java.lang.StringBuilder;
        r13.<init>();
        r14 = "  Active broadcasts [";
        r13 = r13.append(r14);
        r0 = r18;
        r14 = r0.mQueueName;
        r13 = r13.append(r14);
        r14 = "]:";
        r13 = r13.append(r14);
        r13 = r13.toString();
        r0 = r20;
        r0.println(r13);
    L_0x00b3:
        r13 = new java.lang.StringBuilder;
        r13.<init>();
        r14 = "  Active Broadcast ";
        r13 = r13.append(r14);
        r0 = r18;
        r14 = r0.mQueueName;
        r13 = r13.append(r14);
        r14 = " #";
        r13 = r13.append(r14);
        r13 = r13.append(r4);
        r14 = ":";
        r13 = r13.append(r14);
        r13 = r13.toString();
        r0 = r20;
        r0.println(r13);
        r13 = "    ";
        r0 = r20;
        r2.dump(r0, r13, r12);
        goto L_0x0041;
    L_0x00ec:
        r8 = 0;
        r25 = 1;
        r0 = r18;
        r13 = r0.mOrderedBroadcasts;
        r13 = r13.size();
        r4 = r13 + -1;
    L_0x00f9:
        if (r4 < 0) goto L_0x0186;
    L_0x00fb:
        r0 = r18;
        r13 = r0.mOrderedBroadcasts;
        r2 = r13.get(r4);
        r2 = (com.android.server.am.BroadcastRecord) r2;
        if (r24 == 0) goto L_0x0116;
    L_0x0107:
        r13 = r2.callerPackage;
        r0 = r24;
        r13 = r0.equals(r13);
        r13 = r13 ^ 1;
        if (r13 == 0) goto L_0x0116;
    L_0x0113:
        r4 = r4 + -1;
        goto L_0x00f9;
    L_0x0116:
        if (r8 != 0) goto L_0x0144;
    L_0x0118:
        if (r25 == 0) goto L_0x011d;
    L_0x011a:
        r20.println();
    L_0x011d:
        r25 = 1;
        r8 = 1;
        r13 = new java.lang.StringBuilder;
        r13.<init>();
        r14 = "  Active ordered broadcasts [";
        r13 = r13.append(r14);
        r0 = r18;
        r14 = r0.mQueueName;
        r13 = r13.append(r14);
        r14 = "]:";
        r13 = r13.append(r14);
        r13 = r13.toString();
        r0 = r20;
        r0.println(r13);
    L_0x0144:
        r13 = new java.lang.StringBuilder;
        r13.<init>();
        r14 = "  Active Ordered Broadcast ";
        r13 = r13.append(r14);
        r0 = r18;
        r14 = r0.mQueueName;
        r13 = r13.append(r14);
        r14 = " #";
        r13 = r13.append(r14);
        r13 = r13.append(r4);
        r14 = ":";
        r13 = r13.append(r14);
        r13 = r13.toString();
        r0 = r20;
        r0.println(r13);
        r0 = r18;
        r13 = r0.mOrderedBroadcasts;
        r13 = r13.get(r4);
        r13 = (com.android.server.am.BroadcastRecord) r13;
        r14 = "    ";
        r0 = r20;
        r13.dump(r0, r14, r12);
        goto L_0x0113;
    L_0x0186:
        if (r24 == 0) goto L_0x019c;
    L_0x0188:
        r0 = r18;
        r13 = r0.mPendingBroadcast;
        if (r13 == 0) goto L_0x004a;
    L_0x018e:
        r0 = r18;
        r13 = r0.mPendingBroadcast;
        r13 = r13.callerPackage;
        r0 = r24;
        r13 = r0.equals(r13);
        if (r13 == 0) goto L_0x004a;
    L_0x019c:
        if (r25 == 0) goto L_0x01a1;
    L_0x019e:
        r20.println();
    L_0x01a1:
        r13 = new java.lang.StringBuilder;
        r13.<init>();
        r14 = "  Pending broadcast [";
        r13 = r13.append(r14);
        r0 = r18;
        r14 = r0.mQueueName;
        r13 = r13.append(r14);
        r14 = "]:";
        r13 = r13.append(r14);
        r13 = r13.toString();
        r0 = r20;
        r0.println(r13);
        r0 = r18;
        r13 = r0.mPendingBroadcast;
        if (r13 == 0) goto L_0x01db;
    L_0x01cb:
        r0 = r18;
        r13 = r0.mPendingBroadcast;
        r14 = "    ";
        r0 = r20;
        r13.dump(r0, r14, r12);
    L_0x01d7:
        r25 = 1;
        goto L_0x004a;
    L_0x01db:
        r13 = "    (null)";
        r0 = r20;
        r0.println(r13);
        goto L_0x01d7;
    L_0x01e4:
        r4 = r4 + 1;
        if (r24 == 0) goto L_0x01f4;
    L_0x01e8:
        r13 = r9.callerPackage;
        r0 = r24;
        r13 = r0.equals(r13);
        r13 = r13 ^ 1;
        if (r13 != 0) goto L_0x0062;
    L_0x01f4:
        if (r8 != 0) goto L_0x0222;
    L_0x01f6:
        if (r25 == 0) goto L_0x01fb;
    L_0x01f8:
        r20.println();
    L_0x01fb:
        r25 = 1;
        r13 = new java.lang.StringBuilder;
        r13.<init>();
        r14 = "  Historical broadcasts [";
        r13 = r13.append(r14);
        r0 = r18;
        r14 = r0.mQueueName;
        r13 = r13.append(r14);
        r14 = "]:";
        r13 = r13.append(r14);
        r13 = r13.toString();
        r0 = r20;
        r0.println(r13);
        r8 = 1;
    L_0x0222:
        if (r23 == 0) goto L_0x025f;
    L_0x0224:
        r13 = new java.lang.StringBuilder;
        r13.<init>();
        r14 = "  Historical Broadcast ";
        r13 = r13.append(r14);
        r0 = r18;
        r14 = r0.mQueueName;
        r13 = r13.append(r14);
        r14 = " #";
        r13 = r13.append(r14);
        r13 = r13.toString();
        r0 = r20;
        r0.print(r13);
        r0 = r20;
        r0.print(r4);
        r13 = ":";
        r0 = r20;
        r0.println(r13);
        r13 = "    ";
        r0 = r20;
        r9.dump(r0, r13, r12);
        goto L_0x0062;
    L_0x025f:
        r13 = "  #";
        r0 = r20;
        r0.print(r13);
        r0 = r20;
        r0.print(r4);
        r13 = ": ";
        r0 = r20;
        r0.print(r13);
        r0 = r20;
        r0.println(r9);
        r13 = "    ";
        r0 = r20;
        r0.print(r13);
        r13 = r9.intent;
        r14 = 0;
        r15 = 1;
        r16 = 1;
        r17 = 0;
        r13 = r13.toShortString(r14, r15, r16, r17);
        r0 = r20;
        r0.println(r13);
        r13 = r9.targetComp;
        if (r13 == 0) goto L_0x02b3;
    L_0x0296:
        r13 = r9.targetComp;
        r14 = r9.intent;
        r14 = r14.getComponent();
        if (r13 == r14) goto L_0x02b3;
    L_0x02a0:
        r13 = "    targetComp: ";
        r0 = r20;
        r0.print(r13);
        r13 = r9.targetComp;
        r13 = r13.toShortString();
        r0 = r20;
        r0.println(r13);
    L_0x02b3:
        r13 = r9.intent;
        r3 = r13.getExtras();
        if (r3 == 0) goto L_0x0062;
    L_0x02bb:
        r13 = "    extras: ";
        r0 = r20;
        r0.print(r13);
        r13 = r3.toString();
        r0 = r20;
        r0.println(r13);
        goto L_0x0062;
    L_0x02ce:
        r6 = r4;
        r11 = r10;
    L_0x02d0:
        if (r6 <= 0) goto L_0x0070;
    L_0x02d2:
        if (r11 == r10) goto L_0x0070;
    L_0x02d4:
        r13 = MAX_BROADCAST_SUMMARY_HISTORY;
        r14 = -1;
        r0 = r18;
        r11 = r0.ringAdvance(r11, r14, r13);
        r0 = r18;
        r13 = r0.mBroadcastHistory;
        r9 = r13[r11];
        if (r9 == 0) goto L_0x02d0;
    L_0x02e5:
        r6 = r6 + -1;
        goto L_0x02d0;
    L_0x02e8:
        if (r8 != 0) goto L_0x0316;
    L_0x02ea:
        if (r25 == 0) goto L_0x02ef;
    L_0x02ec:
        r20.println();
    L_0x02ef:
        r25 = 1;
        r13 = new java.lang.StringBuilder;
        r13.<init>();
        r14 = "  Historical broadcasts summary [";
        r13 = r13.append(r14);
        r0 = r18;
        r14 = r0.mQueueName;
        r13 = r13.append(r14);
        r14 = "]:";
        r13 = r13.append(r14);
        r13 = r13.toString();
        r0 = r20;
        r0.println(r13);
        r8 = 1;
    L_0x0316:
        if (r23 != 0) goto L_0x0327;
    L_0x0318:
        r13 = 50;
        if (r4 < r13) goto L_0x0327;
    L_0x031c:
        r13 = "  ...";
        r0 = r20;
        r0.println(r13);
        r10 = r11;
        goto L_0x0084;
    L_0x0327:
        r4 = r4 + 1;
        r13 = "  #";
        r0 = r20;
        r0.print(r13);
        r0 = r20;
        r0.print(r4);
        r13 = ": ";
        r0 = r20;
        r0.print(r13);
        r13 = 0;
        r14 = 1;
        r15 = 1;
        r16 = 0;
        r0 = r16;
        r13 = r5.toShortString(r13, r14, r15, r0);
        r0 = r20;
        r0.println(r13);
        r13 = "    ";
        r0 = r20;
        r0.print(r13);
        r0 = r18;
        r13 = r0.mSummaryHistoryDispatchTime;
        r14 = r13[r11];
        r0 = r18;
        r13 = r0.mSummaryHistoryEnqueueTime;
        r16 = r13[r11];
        r14 = r14 - r16;
        r0 = r20;
        android.util.TimeUtils.formatDuration(r14, r0);
        r13 = " dispatch ";
        r0 = r20;
        r0.print(r13);
        r0 = r18;
        r13 = r0.mSummaryHistoryFinishTime;
        r14 = r13[r11];
        r0 = r18;
        r13 = r0.mSummaryHistoryDispatchTime;
        r16 = r13[r11];
        r14 = r14 - r16;
        r0 = r20;
        android.util.TimeUtils.formatDuration(r14, r0);
        r13 = " finish";
        r0 = r20;
        r0.println(r13);
        r13 = "    enq=";
        r0 = r20;
        r0.print(r13);
        r13 = new java.util.Date;
        r0 = r18;
        r14 = r0.mSummaryHistoryEnqueueTime;
        r14 = r14[r11];
        r13.<init>(r14);
        r13 = r12.format(r13);
        r0 = r20;
        r0.print(r13);
        r13 = " disp=";
        r0 = r20;
        r0.print(r13);
        r13 = new java.util.Date;
        r0 = r18;
        r14 = r0.mSummaryHistoryDispatchTime;
        r14 = r14[r11];
        r13.<init>(r14);
        r13 = r12.format(r13);
        r0 = r20;
        r0.print(r13);
        r13 = " fin=";
        r0 = r20;
        r0.print(r13);
        r13 = new java.util.Date;
        r0 = r18;
        r14 = r0.mSummaryHistoryFinishTime;
        r14 = r14[r11];
        r13.<init>(r14);
        r13 = r12.format(r13);
        r0 = r20;
        r0.println(r13);
        r3 = r5.getExtras();
        if (r3 == 0) goto L_0x0081;
    L_0x03e6:
        r13 = "    extras: ";
        r0 = r20;
        r0.print(r13);
        r13 = r3.toString();
        r0 = r20;
        r0.println(r13);
        goto L_0x0081;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.am.BroadcastQueue.dumpLocked(java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[], int, boolean, java.lang.String, boolean):boolean");
    }
}
