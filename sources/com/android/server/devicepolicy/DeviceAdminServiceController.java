package com.android.server.devicepolicy;

import android.app.admin.IDeviceAdminService;
import android.app.admin.IDeviceAdminService.Stub;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ParceledListSlice;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.os.BackgroundThread;
import com.android.server.am.PersistentConnection;
import java.io.PrintWriter;
import java.util.List;

public class DeviceAdminServiceController {
    static final boolean DEBUG = false;
    static final String TAG = "DevicePolicyManager";
    @GuardedBy("mLock")
    private final SparseArray<DevicePolicyServiceConnection> mConnections = new SparseArray();
    private final DevicePolicyConstants mConstants;
    final Context mContext;
    private final Handler mHandler;
    private final Injector mInjector;
    final Object mLock = new Object();
    private final DevicePolicyManagerService mService;

    private class DevicePolicyServiceConnection extends PersistentConnection<IDeviceAdminService> {
        public DevicePolicyServiceConnection(int userId, ComponentName componentName) {
            super(DeviceAdminServiceController.TAG, DeviceAdminServiceController.this.mContext, DeviceAdminServiceController.this.mHandler, userId, componentName, DeviceAdminServiceController.this.mConstants.DAS_DIED_SERVICE_RECONNECT_BACKOFF_SEC, DeviceAdminServiceController.this.mConstants.DAS_DIED_SERVICE_RECONNECT_BACKOFF_INCREASE, DeviceAdminServiceController.this.mConstants.DAS_DIED_SERVICE_RECONNECT_MAX_BACKOFF_SEC);
        }

        protected IDeviceAdminService asInterface(IBinder binder) {
            return Stub.asInterface(binder);
        }
    }

    static void debug(String format, Object... args) {
    }

    public DeviceAdminServiceController(DevicePolicyManagerService service, DevicePolicyConstants constants) {
        this.mService = service;
        this.mInjector = service.mInjector;
        this.mContext = this.mInjector.mContext;
        this.mHandler = new Handler(BackgroundThread.get().getLooper());
        this.mConstants = constants;
    }

    private ServiceInfo findService(String packageName, int userId) {
        Intent intent = new Intent("android.app.action.DEVICE_ADMIN_SERVICE");
        intent.setPackage(packageName);
        try {
            ParceledListSlice<ResolveInfo> pls = this.mInjector.getIPackageManager().queryIntentServices(intent, null, 0, userId);
            if (pls == null) {
                return null;
            }
            List<ResolveInfo> list = pls.getList();
            if (list.size() == 0) {
                return null;
            }
            if (list.size() > 1) {
                Log.e(TAG, "More than one DeviceAdminService's found in package " + packageName + ".  They'll all be ignored.");
                return null;
            }
            ServiceInfo si = ((ResolveInfo) list.get(0)).serviceInfo;
            if ("android.permission.BIND_DEVICE_ADMIN".equals(si.permission)) {
                return si;
            }
            Log.e(TAG, "DeviceAdminService " + si.getComponentName().flattenToShortString() + " must be protected with " + "android.permission.BIND_DEVICE_ADMIN" + ".");
            return null;
        } catch (RemoteException e) {
            return null;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void startServiceForOwner(java.lang.String r11, int r12, java.lang.String r13) {
        /*
        r10 = this;
        r3 = r10.mInjector;
        r4 = r3.binderClearCallingIdentity();
        r6 = r10.mLock;	 Catch:{ all -> 0x0088 }
        monitor-enter(r6);	 Catch:{ all -> 0x0088 }
        r2 = r10.findService(r11, r12);	 Catch:{ all -> 0x0085 }
        if (r2 != 0) goto L_0x002c;
    L_0x000f:
        r3 = "Owner package %s on u%d has no service.";
        r7 = 2;
        r7 = new java.lang.Object[r7];	 Catch:{ all -> 0x0085 }
        r8 = 0;
        r7[r8] = r11;	 Catch:{ all -> 0x0085 }
        r8 = java.lang.Integer.valueOf(r12);	 Catch:{ all -> 0x0085 }
        r9 = 1;
        r7[r9] = r8;	 Catch:{ all -> 0x0085 }
        debug(r3, r7);	 Catch:{ all -> 0x0085 }
        r10.disconnectServiceOnUserLocked(r12, r13);	 Catch:{ all -> 0x0085 }
        monitor-exit(r6);	 Catch:{ all -> 0x0088 }
        r3 = r10.mInjector;
        r3.binderRestoreCallingIdentity(r4);
        return;
    L_0x002c:
        r3 = r10.mConnections;	 Catch:{ all -> 0x0085 }
        r1 = r3.get(r12);	 Catch:{ all -> 0x0085 }
        r1 = (com.android.server.am.PersistentConnection) r1;	 Catch:{ all -> 0x0085 }
        if (r1 == 0) goto L_0x004c;
    L_0x0036:
        r3 = "Disconnecting from existing service connection.";
        r7 = 2;
        r7 = new java.lang.Object[r7];	 Catch:{ all -> 0x0085 }
        r8 = 0;
        r7[r8] = r11;	 Catch:{ all -> 0x0085 }
        r8 = java.lang.Integer.valueOf(r12);	 Catch:{ all -> 0x0085 }
        r9 = 1;
        r7[r9] = r8;	 Catch:{ all -> 0x0085 }
        debug(r3, r7);	 Catch:{ all -> 0x0085 }
        r10.disconnectServiceOnUserLocked(r12, r13);	 Catch:{ all -> 0x0085 }
    L_0x004c:
        r3 = "Owner package %s on u%d has service %s for %s";
        r7 = 4;
        r7 = new java.lang.Object[r7];	 Catch:{ all -> 0x0085 }
        r8 = 0;
        r7[r8] = r11;	 Catch:{ all -> 0x0085 }
        r8 = java.lang.Integer.valueOf(r12);	 Catch:{ all -> 0x0085 }
        r9 = 1;
        r7[r9] = r8;	 Catch:{ all -> 0x0085 }
        r8 = r2.getComponentName();	 Catch:{ all -> 0x0085 }
        r8 = r8.flattenToShortString();	 Catch:{ all -> 0x0085 }
        r9 = 2;
        r7[r9] = r8;	 Catch:{ all -> 0x0085 }
        r8 = 3;
        r7[r8] = r13;	 Catch:{ all -> 0x0085 }
        debug(r3, r7);	 Catch:{ all -> 0x0085 }
        r0 = new com.android.server.devicepolicy.DeviceAdminServiceController$DevicePolicyServiceConnection;	 Catch:{ all -> 0x0085 }
        r3 = r2.getComponentName();	 Catch:{ all -> 0x0085 }
        r0.<init>(r12, r3);	 Catch:{ all -> 0x0085 }
        r3 = r10.mConnections;	 Catch:{ all -> 0x0085 }
        r3.put(r12, r0);	 Catch:{ all -> 0x0085 }
        r0.bind();	 Catch:{ all -> 0x0085 }
        monitor-exit(r6);	 Catch:{ all -> 0x0088 }
        r3 = r10.mInjector;
        r3.binderRestoreCallingIdentity(r4);
        return;
    L_0x0085:
        r3 = move-exception;
        monitor-exit(r6);	 Catch:{ all -> 0x0088 }
        throw r3;	 Catch:{ all -> 0x0088 }
    L_0x0088:
        r3 = move-exception;
        r6 = r10.mInjector;
        r6.binderRestoreCallingIdentity(r4);
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.devicepolicy.DeviceAdminServiceController.startServiceForOwner(java.lang.String, int, java.lang.String):void");
    }

    public void stopServiceForOwner(int userId, String actionForLog) {
        long token = this.mInjector.binderClearCallingIdentity();
        try {
            synchronized (this.mLock) {
                disconnectServiceOnUserLocked(userId, actionForLog);
            }
        } finally {
            this.mInjector.binderRestoreCallingIdentity(token);
        }
    }

    private void disconnectServiceOnUserLocked(int userId, String actionForLog) {
        DevicePolicyServiceConnection conn = (DevicePolicyServiceConnection) this.mConnections.get(userId);
        if (conn != null) {
            debug("Stopping service for u%d if already running for %s.", Integer.valueOf(userId), actionForLog);
            conn.unbind();
            this.mConnections.remove(userId);
        }
    }

    public void dump(String prefix, PrintWriter pw) {
        synchronized (this.mLock) {
            if (this.mConnections.size() == 0) {
                return;
            }
            pw.println();
            pw.print(prefix);
            pw.println("Owner Services:");
            for (int i = 0; i < this.mConnections.size(); i++) {
                int userId = this.mConnections.keyAt(i);
                pw.print(prefix);
                pw.print("  ");
                pw.print("User: ");
                pw.println(userId);
                ((DevicePolicyServiceConnection) this.mConnections.valueAt(i)).dump(prefix + "    ", pw);
            }
            pw.println();
        }
    }
}
