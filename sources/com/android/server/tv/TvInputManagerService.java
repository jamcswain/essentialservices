package com.android.server.tv;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.graphics.Rect;
import android.hardware.hdmi.HdmiDeviceInfo;
import android.media.PlaybackParams;
import android.media.tv.DvbDeviceInfo;
import android.media.tv.ITvInputClient;
import android.media.tv.ITvInputHardware;
import android.media.tv.ITvInputHardwareCallback;
import android.media.tv.ITvInputManager.Stub;
import android.media.tv.ITvInputManagerCallback;
import android.media.tv.ITvInputService;
import android.media.tv.ITvInputServiceCallback;
import android.media.tv.ITvInputSession;
import android.media.tv.ITvInputSessionCallback;
import android.media.tv.TvContentRating;
import android.media.tv.TvContentRatingSystemInfo;
import android.media.tv.TvContract.WatchedPrograms;
import android.media.tv.TvInputHardwareInfo;
import android.media.tv.TvInputInfo;
import android.media.tv.TvInputInfo.Builder;
import android.media.tv.TvStreamConfig;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.Slog;
import android.util.SparseArray;
import android.view.InputChannel;
import android.view.Surface;
import com.android.internal.content.PackageMonitor;
import com.android.internal.os.SomeArgs;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.IndentingPrintWriter;
import com.android.server.IoThread;
import com.android.server.SystemService;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class TvInputManagerService extends SystemService {
    private static final boolean DEBUG = false;
    private static final String DVB_DIRECTORY = "/dev/dvb";
    private static final String TAG = "TvInputManagerService";
    private static final Pattern sAdapterDirPattern = Pattern.compile("^adapter([0-9]+)$");
    private static final Pattern sFrontEndDevicePattern = Pattern.compile("^dvb([0-9]+)\\.frontend([0-9]+)$");
    private static final Pattern sFrontEndInAdapterDirPattern = Pattern.compile("^frontend([0-9]+)$");
    private final Context mContext;
    private int mCurrentUserId = 0;
    private final Object mLock = new Object();
    private final TvInputHardwareManager mTvInputHardwareManager;
    private final SparseArray<UserState> mUserStates = new SparseArray();
    private final WatchLogHandler mWatchLogHandler;

    private final class BinderService extends Stub {
        private BinderService() {
        }

        public List<TvInputInfo> getTvInputList(int userId) {
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "getTvInputList");
            long identity = Binder.clearCallingIdentity();
            try {
                List<TvInputInfo> inputList;
                synchronized (TvInputManagerService.this.mLock) {
                    UserState userState = TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId);
                    inputList = new ArrayList();
                    for (TvInputState state : userState.inputMap.values()) {
                        inputList.add(state.info);
                    }
                }
                return inputList;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public TvInputInfo getTvInputInfo(String inputId, int userId) {
            TvInputInfo tvInputInfo = null;
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "getTvInputInfo");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    TvInputState state = (TvInputState) TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).inputMap.get(inputId);
                    if (state != null) {
                        tvInputInfo = state.info;
                    }
                }
                return tvInputInfo;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void updateTvInputInfo(TvInputInfo inputInfo, int userId) {
            String inputInfoPackageName = inputInfo.getServiceInfo().packageName;
            String callingPackageName = getCallingPackageName();
            if (TextUtils.equals(inputInfoPackageName, callingPackageName) || TvInputManagerService.this.mContext.checkCallingPermission("android.permission.WRITE_SECURE_SETTINGS") == 0) {
                int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "updateTvInputInfo");
                long identity = Binder.clearCallingIdentity();
                try {
                    synchronized (TvInputManagerService.this.mLock) {
                        TvInputManagerService.this.updateTvInputInfoLocked(TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId), inputInfo);
                    }
                } finally {
                    Binder.restoreCallingIdentity(identity);
                }
            } else {
                throw new IllegalArgumentException("calling package " + callingPackageName + " is not allowed to change TvInputInfo for " + inputInfoPackageName);
            }
        }

        private String getCallingPackageName() {
            String[] packages = TvInputManagerService.this.mContext.getPackageManager().getPackagesForUid(Binder.getCallingUid());
            if (packages == null || packages.length <= 0) {
                return Shell.NIGHT_MODE_STR_UNKNOWN;
            }
            return packages[0];
        }

        public int getTvInputState(String inputId, int userId) {
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "getTvInputState");
            long identity = Binder.clearCallingIdentity();
            try {
                int -get1;
                synchronized (TvInputManagerService.this.mLock) {
                    TvInputState state = (TvInputState) TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).inputMap.get(inputId);
                    -get1 = state == null ? 0 : state.state;
                }
                return -get1;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public List<TvContentRatingSystemInfo> getTvContentRatingSystemList(int userId) {
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "getTvContentRatingSystemList");
            long identity = Binder.clearCallingIdentity();
            try {
                List<TvContentRatingSystemInfo> -get2;
                synchronized (TvInputManagerService.this.mLock) {
                    -get2 = TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).contentRatingSystemList;
                }
                return -get2;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void sendTvInputNotifyIntent(Intent intent, int userId) {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.NOTIFY_TV_INPUTS") != 0) {
                throw new SecurityException("The caller: " + getCallingPackageName() + " doesn't have permission: " + "android.permission.NOTIFY_TV_INPUTS");
            } else if (TextUtils.isEmpty(intent.getPackage())) {
                throw new IllegalArgumentException("Must specify package name to notify.");
            } else {
                String action = intent.getAction();
                if (action.equals("android.media.tv.action.PREVIEW_PROGRAM_BROWSABLE_DISABLED")) {
                    if (intent.getLongExtra("android.media.tv.extra.PREVIEW_PROGRAM_ID", -1) < 0) {
                        throw new IllegalArgumentException("Invalid preview program ID.");
                    }
                } else if (action.equals("android.media.tv.action.WATCH_NEXT_PROGRAM_BROWSABLE_DISABLED")) {
                    if (intent.getLongExtra("android.media.tv.extra.WATCH_NEXT_PROGRAM_ID", -1) < 0) {
                        throw new IllegalArgumentException("Invalid watch next program ID.");
                    }
                } else if (!action.equals("android.media.tv.action.PREVIEW_PROGRAM_ADDED_TO_WATCH_NEXT")) {
                    throw new IllegalArgumentException("Invalid TV input notifying action: " + intent.getAction());
                } else if (intent.getLongExtra("android.media.tv.extra.PREVIEW_PROGRAM_ID", -1) < 0) {
                    throw new IllegalArgumentException("Invalid preview program ID.");
                } else if (intent.getLongExtra("android.media.tv.extra.WATCH_NEXT_PROGRAM_ID", -1) < 0) {
                    throw new IllegalArgumentException("Invalid watch next program ID.");
                }
                int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "sendTvInputNotifyIntent");
                long identity = Binder.clearCallingIdentity();
                try {
                    TvInputManagerService.this.getContext().sendBroadcastAsUser(intent, new UserHandle(resolvedUserId));
                } finally {
                    Binder.restoreCallingIdentity(identity);
                }
            }
        }

        public void registerCallback(final ITvInputManagerCallback callback, int userId) {
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "registerCallback");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    final UserState userState = TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId);
                    userState.callbackSet.add(callback);
                    try {
                        callback.asBinder().linkToDeath(new DeathRecipient() {
                            public void binderDied() {
                                synchronized (TvInputManagerService.this.mLock) {
                                    if (userState.callbackSet != null) {
                                        userState.callbackSet.remove(callback);
                                    }
                                }
                            }
                        }, 0);
                    } catch (RemoteException e) {
                        Slog.e(TvInputManagerService.TAG, "client process has already died", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void unregisterCallback(ITvInputManagerCallback callback, int userId) {
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "unregisterCallback");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).callbackSet.remove(callback);
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public boolean isParentalControlsEnabled(int userId) {
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "isParentalControlsEnabled");
            long identity = Binder.clearCallingIdentity();
            try {
                boolean isParentalControlsEnabled;
                synchronized (TvInputManagerService.this.mLock) {
                    isParentalControlsEnabled = TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).persistentDataStore.isParentalControlsEnabled();
                }
                return isParentalControlsEnabled;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void setParentalControlsEnabled(boolean enabled, int userId) {
            ensureParentalControlsPermission();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "setParentalControlsEnabled");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).persistentDataStore.setParentalControlsEnabled(enabled);
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public boolean isRatingBlocked(String rating, int userId) {
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "isRatingBlocked");
            long identity = Binder.clearCallingIdentity();
            try {
                boolean isRatingBlocked;
                synchronized (TvInputManagerService.this.mLock) {
                    isRatingBlocked = TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).persistentDataStore.isRatingBlocked(TvContentRating.unflattenFromString(rating));
                }
                return isRatingBlocked;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public List<String> getBlockedRatings(int userId) {
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "getBlockedRatings");
            long identity = Binder.clearCallingIdentity();
            try {
                List<String> ratings;
                synchronized (TvInputManagerService.this.mLock) {
                    UserState userState = TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId);
                    ratings = new ArrayList();
                    for (TvContentRating rating : userState.persistentDataStore.getBlockedRatings()) {
                        ratings.add(rating.flattenToString());
                    }
                }
                return ratings;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void addBlockedRating(String rating, int userId) {
            ensureParentalControlsPermission();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "addBlockedRating");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).persistentDataStore.addBlockedRating(TvContentRating.unflattenFromString(rating));
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void removeBlockedRating(String rating, int userId) {
            ensureParentalControlsPermission();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "removeBlockedRating");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    TvInputManagerService.this.getOrCreateUserStateLocked(resolvedUserId).persistentDataStore.removeBlockedRating(TvContentRating.unflattenFromString(rating));
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        private void ensureParentalControlsPermission() {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.MODIFY_PARENTAL_CONTROLS") != 0) {
                throw new SecurityException("The caller does not have parental controls permission");
            }
        }

        public void createSession(android.media.tv.ITvInputClient r21, java.lang.String r22, boolean r23, int r24, int r25) {
            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r17_2 'serviceState' com.android.server.tv.TvInputManagerService$ServiceState) in PHI: PHI: (r17_3 'serviceState' com.android.server.tv.TvInputManagerService$ServiceState) = (r17_1 'serviceState' com.android.server.tv.TvInputManagerService$ServiceState), (r17_2 'serviceState' com.android.server.tv.TvInputManagerService$ServiceState) binds: {(r17_1 'serviceState' com.android.server.tv.TvInputManagerService$ServiceState)=B:23:0x009e, (r17_2 'serviceState' com.android.server.tv.TvInputManagerService$ServiceState)=B:24:0x00a0}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:79)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
            /*
            r20 = this;
            r10 = android.os.Binder.getCallingUid();
            r0 = r20;
            r3 = com.android.server.tv.TvInputManagerService.this;
            r5 = android.os.Binder.getCallingPid();
            r6 = "createSession";
            r0 = r25;
            r11 = r3.resolveCallingUserId(r5, r10, r0, r6);
            r14 = android.os.Binder.clearCallingIdentity();
            r0 = r20;	 Catch:{ all -> 0x0127 }
            r3 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0127 }
            r19 = r3.mLock;	 Catch:{ all -> 0x0127 }
            monitor-enter(r19);	 Catch:{ all -> 0x0127 }
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r3 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r3 = r3.mCurrentUserId;	 Catch:{ all -> 0x0124 }
            r0 = r25;	 Catch:{ all -> 0x0124 }
            if (r0 == r3) goto L_0x0046;	 Catch:{ all -> 0x0124 }
        L_0x002e:
            r3 = r23 ^ 1;	 Catch:{ all -> 0x0124 }
            if (r3 == 0) goto L_0x0046;	 Catch:{ all -> 0x0124 }
        L_0x0032:
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r2 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r5 = 0;	 Catch:{ all -> 0x0124 }
            r6 = 0;	 Catch:{ all -> 0x0124 }
            r3 = r21;	 Catch:{ all -> 0x0124 }
            r4 = r22;	 Catch:{ all -> 0x0124 }
            r7 = r24;	 Catch:{ all -> 0x0124 }
            r2.sendSessionTokenToClientLocked(r3, r4, r5, r6, r7);	 Catch:{ all -> 0x0124 }
            monitor-exit(r19);	 Catch:{ all -> 0x0127 }
            android.os.Binder.restoreCallingIdentity(r14);
            return;
        L_0x0046:
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r3 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r18 = r3.getOrCreateUserStateLocked(r11);	 Catch:{ all -> 0x0124 }
            r3 = r18.inputMap;	 Catch:{ all -> 0x0124 }
            r0 = r22;	 Catch:{ all -> 0x0124 }
            r16 = r3.get(r0);	 Catch:{ all -> 0x0124 }
            r16 = (com.android.server.tv.TvInputManagerService.TvInputState) r16;	 Catch:{ all -> 0x0124 }
            if (r16 != 0) goto L_0x008c;	 Catch:{ all -> 0x0124 }
        L_0x005c:
            r3 = "TvInputManagerService";	 Catch:{ all -> 0x0124 }
            r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0124 }
            r5.<init>();	 Catch:{ all -> 0x0124 }
            r6 = "Failed to find input state for inputId=";	 Catch:{ all -> 0x0124 }
            r5 = r5.append(r6);	 Catch:{ all -> 0x0124 }
            r0 = r22;	 Catch:{ all -> 0x0124 }
            r5 = r5.append(r0);	 Catch:{ all -> 0x0124 }
            r5 = r5.toString();	 Catch:{ all -> 0x0124 }
            android.util.Slog.w(r3, r5);	 Catch:{ all -> 0x0124 }
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r2 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r5 = 0;	 Catch:{ all -> 0x0124 }
            r6 = 0;	 Catch:{ all -> 0x0124 }
            r3 = r21;	 Catch:{ all -> 0x0124 }
            r4 = r22;	 Catch:{ all -> 0x0124 }
            r7 = r24;	 Catch:{ all -> 0x0124 }
            r2.sendSessionTokenToClientLocked(r3, r4, r5, r6, r7);	 Catch:{ all -> 0x0124 }
            monitor-exit(r19);	 Catch:{ all -> 0x0127 }
            android.os.Binder.restoreCallingIdentity(r14);
            return;
        L_0x008c:
            r13 = r16.info;	 Catch:{ all -> 0x0124 }
            r3 = r18.serviceStateMap;	 Catch:{ all -> 0x0124 }
            r5 = r13.getComponent();	 Catch:{ all -> 0x0124 }
            r17 = r3.get(r5);	 Catch:{ all -> 0x0124 }
            r17 = (com.android.server.tv.TvInputManagerService.ServiceState) r17;	 Catch:{ all -> 0x0124 }
            if (r17 != 0) goto L_0x00bd;	 Catch:{ all -> 0x0124 }
        L_0x00a0:
            r17 = new com.android.server.tv.TvInputManagerService$ServiceState;	 Catch:{ all -> 0x0124 }
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r3 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r5 = r13.getComponent();	 Catch:{ all -> 0x0124 }
            r6 = 0;	 Catch:{ all -> 0x0124 }
            r0 = r17;	 Catch:{ all -> 0x0124 }
            r0.<init>(r5, r11);	 Catch:{ all -> 0x0124 }
            r3 = r18.serviceStateMap;	 Catch:{ all -> 0x0124 }
            r5 = r13.getComponent();	 Catch:{ all -> 0x0124 }
            r0 = r17;	 Catch:{ all -> 0x0124 }
            r3.put(r5, r0);	 Catch:{ all -> 0x0124 }
        L_0x00bd:
            r3 = r17.reconnecting;	 Catch:{ all -> 0x0124 }
            if (r3 == 0) goto L_0x00d7;	 Catch:{ all -> 0x0124 }
        L_0x00c3:
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r2 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r5 = 0;	 Catch:{ all -> 0x0124 }
            r6 = 0;	 Catch:{ all -> 0x0124 }
            r3 = r21;	 Catch:{ all -> 0x0124 }
            r4 = r22;	 Catch:{ all -> 0x0124 }
            r7 = r24;	 Catch:{ all -> 0x0124 }
            r2.sendSessionTokenToClientLocked(r3, r4, r5, r6, r7);	 Catch:{ all -> 0x0124 }
            monitor-exit(r19);	 Catch:{ all -> 0x0127 }
            android.os.Binder.restoreCallingIdentity(r14);
            return;
        L_0x00d7:
            r4 = new android.os.Binder;	 Catch:{ all -> 0x0124 }
            r4.<init>();	 Catch:{ all -> 0x0124 }
            r2 = new com.android.server.tv.TvInputManagerService$SessionState;	 Catch:{ all -> 0x0124 }
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r3 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r5 = r13.getId();	 Catch:{ all -> 0x0124 }
            r6 = r13.getComponent();	 Catch:{ all -> 0x0124 }
            r12 = 0;	 Catch:{ all -> 0x0124 }
            r7 = r23;	 Catch:{ all -> 0x0124 }
            r8 = r21;	 Catch:{ all -> 0x0124 }
            r9 = r24;	 Catch:{ all -> 0x0124 }
            r2.<init>(r4, r5, r6, r7, r8, r9, r10, r11);	 Catch:{ all -> 0x0124 }
            r3 = r18.sessionStateMap;	 Catch:{ all -> 0x0124 }
            r3.put(r4, r2);	 Catch:{ all -> 0x0124 }
            r3 = r17.sessionTokens;	 Catch:{ all -> 0x0124 }
            r3.add(r4);	 Catch:{ all -> 0x0124 }
            r3 = r17.service;	 Catch:{ all -> 0x0124 }
            if (r3 == 0) goto L_0x0118;	 Catch:{ all -> 0x0124 }
        L_0x0108:
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r3 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r5 = r17.service;	 Catch:{ all -> 0x0124 }
            r3.createSessionInternalLocked(r5, r4, r11);	 Catch:{ all -> 0x0124 }
        L_0x0113:
            monitor-exit(r19);	 Catch:{ all -> 0x0127 }
            android.os.Binder.restoreCallingIdentity(r14);
            return;
        L_0x0118:
            r0 = r20;	 Catch:{ all -> 0x0124 }
            r3 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0124 }
            r5 = r13.getComponent();	 Catch:{ all -> 0x0124 }
            r3.updateServiceConnectionLocked(r5, r11);	 Catch:{ all -> 0x0124 }
            goto L_0x0113;
        L_0x0124:
            r3 = move-exception;
            monitor-exit(r19);	 Catch:{ all -> 0x0127 }
            throw r3;	 Catch:{ all -> 0x0127 }
        L_0x0127:
            r3 = move-exception;
            android.os.Binder.restoreCallingIdentity(r14);
            throw r3;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.BinderService.createSession(android.media.tv.ITvInputClient, java.lang.String, boolean, int, int):void");
        }

        public void releaseSession(IBinder sessionToken, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "releaseSession");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    TvInputManagerService.this.releaseSessionLocked(sessionToken, callingUid, resolvedUserId);
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void setMainSession(android.os.IBinder r11, int r12) {
            /*
            r10 = this;
            r6 = com.android.server.tv.TvInputManagerService.this;
            r6 = r6.mContext;
            r7 = "android.permission.CHANGE_HDMI_CEC_ACTIVE_SOURCE";
            r6 = r6.checkCallingPermission(r7);
            if (r6 == 0) goto L_0x0018;
        L_0x000f:
            r6 = new java.lang.SecurityException;
            r7 = "The caller does not have CHANGE_HDMI_CEC_ACTIVE_SOURCE permission";
            r6.<init>(r7);
            throw r6;
        L_0x0018:
            r0 = android.os.Binder.getCallingUid();
            r6 = com.android.server.tv.TvInputManagerService.this;
            r7 = android.os.Binder.getCallingPid();
            r8 = "setMainSession";
            r4 = r6.resolveCallingUserId(r7, r0, r12, r8);
            r2 = android.os.Binder.clearCallingIdentity();
            r6 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0066 }
            r7 = r6.mLock;	 Catch:{ all -> 0x0066 }
            monitor-enter(r7);	 Catch:{ all -> 0x0066 }
            r6 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0063 }
            r5 = r6.getOrCreateUserStateLocked(r4);	 Catch:{ all -> 0x0063 }
            r6 = r5.mainSessionToken;	 Catch:{ all -> 0x0063 }
            if (r6 != r11) goto L_0x0045;
        L_0x0040:
            monitor-exit(r7);	 Catch:{ all -> 0x0066 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x0045:
            r1 = r5.mainSessionToken;	 Catch:{ all -> 0x0063 }
            r5.mainSessionToken = r11;	 Catch:{ all -> 0x0063 }
            if (r11 == 0) goto L_0x0054;
        L_0x004e:
            r6 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0063 }
            r8 = 1;
            r6.setMainLocked(r11, r8, r0, r12);	 Catch:{ all -> 0x0063 }
        L_0x0054:
            if (r1 == 0) goto L_0x005e;
        L_0x0056:
            r6 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0063 }
            r8 = 0;
            r9 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
            r6.setMainLocked(r1, r8, r9, r12);	 Catch:{ all -> 0x0063 }
        L_0x005e:
            monitor-exit(r7);	 Catch:{ all -> 0x0066 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x0063:
            r6 = move-exception;
            monitor-exit(r7);	 Catch:{ all -> 0x0066 }
            throw r6;	 Catch:{ all -> 0x0066 }
        L_0x0066:
            r6 = move-exception;
            android.os.Binder.restoreCallingIdentity(r2);
            throw r6;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.BinderService.setMainSession(android.os.IBinder, int):void");
        }

        public void setSurface(IBinder sessionToken, Surface surface, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "setSurface");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        SessionState sessionState = TvInputManagerService.this.getSessionStateLocked(sessionToken, callingUid, resolvedUserId);
                        if (sessionState.hardwareSessionToken == null) {
                            TvInputManagerService.this.getSessionLocked(sessionState).setSurface(surface);
                        } else {
                            TvInputManagerService.this.getSessionLocked(sessionState.hardwareSessionToken, 1000, resolvedUserId).setSurface(surface);
                        }
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in setSurface", e);
                    }
                }
            } finally {
                if (surface != null) {
                    surface.release();
                }
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void dispatchSurfaceChanged(IBinder sessionToken, int format, int width, int height, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "dispatchSurfaceChanged");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        SessionState sessionState = TvInputManagerService.this.getSessionStateLocked(sessionToken, callingUid, resolvedUserId);
                        TvInputManagerService.this.getSessionLocked(sessionState).dispatchSurfaceChanged(format, width, height);
                        if (sessionState.hardwareSessionToken != null) {
                            TvInputManagerService.this.getSessionLocked(sessionState.hardwareSessionToken, 1000, resolvedUserId).dispatchSurfaceChanged(format, width, height);
                        }
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in dispatchSurfaceChanged", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void setVolume(IBinder sessionToken, float volume, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "setVolume");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        SessionState sessionState = TvInputManagerService.this.getSessionStateLocked(sessionToken, callingUid, resolvedUserId);
                        TvInputManagerService.this.getSessionLocked(sessionState).setVolume(volume);
                        if (sessionState.hardwareSessionToken != null) {
                            TvInputManagerService.this.getSessionLocked(sessionState.hardwareSessionToken, 1000, resolvedUserId).setVolume(volume > 0.0f ? 1.0f : 0.0f);
                        }
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in setVolume", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void tune(android.os.IBinder r15, android.net.Uri r16, android.os.Bundle r17, int r18) {
            /*
            r14 = this;
            r3 = android.os.Binder.getCallingUid();
            r10 = com.android.server.tv.TvInputManagerService.this;
            r11 = android.os.Binder.getCallingPid();
            r12 = "tune";
            r0 = r18;
            r5 = r10.resolveCallingUserId(r11, r3, r0, r12);
            r6 = android.os.Binder.clearCallingIdentity();
            r10 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x009a }
            r11 = r10.mLock;	 Catch:{ all -> 0x009a }
            monitor-enter(r11);	 Catch:{ all -> 0x009a }
            r10 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = r10.getSessionLocked(r15, r3, r5);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r0 = r16;
            r1 = r17;
            r10.tune(r0, r1);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = android.media.tv.TvContract.isChannelUriForPassthroughInput(r16);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            if (r10 == 0) goto L_0x0036;
        L_0x0031:
            monitor-exit(r11);	 Catch:{ all -> 0x009a }
            android.os.Binder.restoreCallingIdentity(r6);
            return;
        L_0x0036:
            r10 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r9 = r10.getOrCreateUserStateLocked(r5);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = r9.sessionStateMap;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r8 = r10.get(r15);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r8 = (com.android.server.tv.TvInputManagerService.SessionState) r8;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = r8.isRecordingSession;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            if (r10 == 0) goto L_0x0051;
        L_0x004c:
            monitor-exit(r11);	 Catch:{ all -> 0x009a }
            android.os.Binder.restoreCallingIdentity(r6);
            return;
        L_0x0051:
            r2 = com.android.internal.os.SomeArgs.obtain();	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = r8.componentName;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = r10.getPackageName();	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r2.arg1 = r10;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r12 = java.lang.System.currentTimeMillis();	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = java.lang.Long.valueOf(r12);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r2.arg2 = r10;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r12 = android.content.ContentUris.parseId(r16);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = java.lang.Long.valueOf(r12);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r2.arg3 = r10;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r0 = r17;
            r2.arg4 = r0;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r2.arg5 = r15;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10 = r10.mWatchLogHandler;	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r12 = 1;
            r10 = r10.obtainMessage(r12, r2);	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
            r10.sendToTarget();	 Catch:{ RemoteException -> 0x008c, RemoteException -> 0x008c }
        L_0x0087:
            monitor-exit(r11);	 Catch:{ all -> 0x009a }
            android.os.Binder.restoreCallingIdentity(r6);
            return;
        L_0x008c:
            r4 = move-exception;
            r10 = "TvInputManagerService";
            r12 = "error in tune";
            android.util.Slog.e(r10, r12, r4);	 Catch:{ all -> 0x0097 }
            goto L_0x0087;
        L_0x0097:
            r10 = move-exception;
            monitor-exit(r11);	 Catch:{ all -> 0x009a }
            throw r10;	 Catch:{ all -> 0x009a }
        L_0x009a:
            r10 = move-exception;
            android.os.Binder.restoreCallingIdentity(r6);
            throw r10;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.BinderService.tune(android.os.IBinder, android.net.Uri, android.os.Bundle, int):void");
        }

        public void unblockContent(IBinder sessionToken, String unblockedRating, int userId) {
            ensureParentalControlsPermission();
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "unblockContent");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).unblockContent(unblockedRating);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in unblockContent", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void setCaptionEnabled(IBinder sessionToken, boolean enabled, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "setCaptionEnabled");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).setCaptionEnabled(enabled);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in setCaptionEnabled", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void selectTrack(IBinder sessionToken, int type, String trackId, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "selectTrack");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).selectTrack(type, trackId);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in selectTrack", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void sendAppPrivateCommand(IBinder sessionToken, String command, Bundle data, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "sendAppPrivateCommand");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).appPrivateCommand(command, data);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in appPrivateCommand", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void createOverlayView(IBinder sessionToken, IBinder windowToken, Rect frame, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "createOverlayView");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).createOverlayView(windowToken, frame);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in createOverlayView", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void relayoutOverlayView(IBinder sessionToken, Rect frame, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "relayoutOverlayView");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).relayoutOverlayView(frame);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in relayoutOverlayView", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void removeOverlayView(IBinder sessionToken, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "removeOverlayView");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).removeOverlayView();
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in removeOverlayView", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void timeShiftPlay(IBinder sessionToken, Uri recordedProgramUri, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "timeShiftPlay");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).timeShiftPlay(recordedProgramUri);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in timeShiftPlay", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void timeShiftPause(IBinder sessionToken, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "timeShiftPause");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).timeShiftPause();
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in timeShiftPause", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void timeShiftResume(IBinder sessionToken, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "timeShiftResume");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).timeShiftResume();
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in timeShiftResume", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void timeShiftSeekTo(IBinder sessionToken, long timeMs, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "timeShiftSeekTo");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).timeShiftSeekTo(timeMs);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in timeShiftSeekTo", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void timeShiftSetPlaybackParams(IBinder sessionToken, PlaybackParams params, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "timeShiftSetPlaybackParams");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).timeShiftSetPlaybackParams(params);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in timeShiftSetPlaybackParams", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void timeShiftEnablePositionTracking(IBinder sessionToken, boolean enable, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "timeShiftEnablePositionTracking");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).timeShiftEnablePositionTracking(enable);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in timeShiftEnablePositionTracking", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void startRecording(IBinder sessionToken, Uri programUri, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "startRecording");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).startRecording(programUri);
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in startRecording", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void stopRecording(IBinder sessionToken, int userId) {
            int callingUid = Binder.getCallingUid();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "stopRecording");
            long identity = Binder.clearCallingIdentity();
            try {
                synchronized (TvInputManagerService.this.mLock) {
                    try {
                        TvInputManagerService.this.getSessionLocked(sessionToken, callingUid, resolvedUserId).stopRecording();
                    } catch (Exception e) {
                        Slog.e(TvInputManagerService.TAG, "error in stopRecording", e);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public List<TvInputHardwareInfo> getHardwareList() throws RemoteException {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.TV_INPUT_HARDWARE") != 0) {
                return null;
            }
            long identity = Binder.clearCallingIdentity();
            try {
                List<TvInputHardwareInfo> hardwareList = TvInputManagerService.this.mTvInputHardwareManager.getHardwareList();
                return hardwareList;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public ITvInputHardware acquireTvInputHardware(int deviceId, ITvInputHardwareCallback callback, TvInputInfo info, int userId) throws RemoteException {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.TV_INPUT_HARDWARE") != 0) {
                return null;
            }
            long identity = Binder.clearCallingIdentity();
            int callingUid = Binder.getCallingUid();
            try {
                ITvInputHardware acquireHardware = TvInputManagerService.this.mTvInputHardwareManager.acquireHardware(deviceId, callback, info, callingUid, TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "acquireTvInputHardware"));
                return acquireHardware;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public void releaseTvInputHardware(int deviceId, ITvInputHardware hardware, int userId) throws RemoteException {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.TV_INPUT_HARDWARE") == 0) {
                long identity = Binder.clearCallingIdentity();
                int callingUid = Binder.getCallingUid();
                try {
                    TvInputManagerService.this.mTvInputHardwareManager.releaseHardware(deviceId, hardware, callingUid, TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "releaseTvInputHardware"));
                } finally {
                    Binder.restoreCallingIdentity(identity);
                }
            }
        }

        public List<DvbDeviceInfo> getDvbDeviceList() throws RemoteException {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.DVB_DEVICE") != 0) {
                throw new SecurityException("Requires DVB_DEVICE permission");
            }
            long identity = Binder.clearCallingIdentity();
            try {
                ArrayList<DvbDeviceInfo> deviceInfosFromPattern1 = new ArrayList();
                boolean dvbDirectoryFound = false;
                for (String fileName : new File("/dev").list()) {
                    Matcher matcher = TvInputManagerService.sFrontEndDevicePattern.matcher(fileName);
                    if (matcher.find()) {
                        deviceInfosFromPattern1.add(new DvbDeviceInfo(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2))));
                    }
                    if (TextUtils.equals("dvb", fileName)) {
                        dvbDirectoryFound = true;
                    }
                }
                List<DvbDeviceInfo> unmodifiableList;
                if (dvbDirectoryFound) {
                    File dvbDirectory = new File(TvInputManagerService.DVB_DIRECTORY);
                    ArrayList<DvbDeviceInfo> deviceInfosFromPattern2 = new ArrayList();
                    for (String fileNameInDvb : dvbDirectory.list()) {
                        Matcher adapterMatcher = TvInputManagerService.sAdapterDirPattern.matcher(fileNameInDvb);
                        if (adapterMatcher.find()) {
                            int adapterId = Integer.parseInt(adapterMatcher.group(1));
                            for (String fileNameInAdapter : new File("/dev/dvb/" + fileNameInDvb).list()) {
                                Matcher frontendMatcher = TvInputManagerService.sFrontEndInAdapterDirPattern.matcher(fileNameInAdapter);
                                if (frontendMatcher.find()) {
                                    deviceInfosFromPattern2.add(new DvbDeviceInfo(adapterId, Integer.parseInt(frontendMatcher.group(1))));
                                }
                            }
                        }
                    }
                    if (deviceInfosFromPattern2.isEmpty()) {
                        unmodifiableList = Collections.unmodifiableList(deviceInfosFromPattern1);
                    } else {
                        unmodifiableList = Collections.unmodifiableList(deviceInfosFromPattern2);
                    }
                    Binder.restoreCallingIdentity(identity);
                    return unmodifiableList;
                }
                unmodifiableList = Collections.unmodifiableList(deviceInfosFromPattern1);
                return unmodifiableList;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        public ParcelFileDescriptor openDvbDevice(DvbDeviceInfo info, int device) throws RemoteException {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.DVB_DEVICE") != 0) {
                throw new SecurityException("Requires DVB_DEVICE permission");
            }
            String deviceFileName;
            boolean dvbDeviceFound = false;
            for (String fileName : new File("/dev").list()) {
                int i;
                if (TextUtils.equals("dvb", fileName)) {
                    for (String fileNameInDvb : new File(TvInputManagerService.DVB_DIRECTORY).list()) {
                        if (TvInputManagerService.sAdapterDirPattern.matcher(fileNameInDvb).find()) {
                            for (String fileNameInAdapter : new File("/dev/dvb/" + fileNameInDvb).list()) {
                                if (TvInputManagerService.sFrontEndInAdapterDirPattern.matcher(fileNameInAdapter).find()) {
                                    dvbDeviceFound = true;
                                    break;
                                }
                            }
                        }
                        if (dvbDeviceFound) {
                            break;
                        }
                    }
                }
                if (dvbDeviceFound) {
                    break;
                }
            }
            long identity = Binder.clearCallingIdentity();
            switch (device) {
                case 0:
                    deviceFileName = String.format(dvbDeviceFound ? "/dev/dvb/adapter%d/demux%d" : "/dev/dvb%d.demux%d", new Object[]{Integer.valueOf(info.getAdapterId()), Integer.valueOf(info.getDeviceId())});
                    break;
                case 1:
                    deviceFileName = String.format(dvbDeviceFound ? "/dev/dvb/adapter%d/dvr%d" : "/dev/dvb%d.dvr%d", new Object[]{Integer.valueOf(info.getAdapterId()), Integer.valueOf(info.getDeviceId())});
                    break;
                case 2:
                    deviceFileName = String.format(dvbDeviceFound ? "/dev/dvb/adapter%d/frontend%d" : "/dev/dvb%d.frontend%d", new Object[]{Integer.valueOf(info.getAdapterId()), Integer.valueOf(info.getDeviceId())});
                    break;
                default:
                    try {
                        throw new IllegalArgumentException("Invalid DVB device: " + device);
                    } catch (Throwable th) {
                        Binder.restoreCallingIdentity(identity);
                    }
            }
            try {
                File file = new File(deviceFileName);
                if (2 == device) {
                    i = 805306368;
                } else {
                    i = 268435456;
                }
                ParcelFileDescriptor open = ParcelFileDescriptor.open(file, i);
                Binder.restoreCallingIdentity(identity);
                return open;
            } catch (FileNotFoundException e) {
                Binder.restoreCallingIdentity(identity);
                return null;
            }
        }

        public List<TvStreamConfig> getAvailableTvStreamConfigList(String inputId, int userId) throws RemoteException {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.CAPTURE_TV_INPUT") != 0) {
                throw new SecurityException("Requires CAPTURE_TV_INPUT permission");
            }
            long identity = Binder.clearCallingIdentity();
            int callingUid = Binder.getCallingUid();
            try {
                List<TvStreamConfig> availableTvStreamConfigList = TvInputManagerService.this.mTvInputHardwareManager.getAvailableTvStreamConfigList(inputId, callingUid, TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), callingUid, userId, "getAvailableTvStreamConfigList"));
                return availableTvStreamConfigList;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean captureFrame(java.lang.String r15, android.view.Surface r16, android.media.tv.TvStreamConfig r17, int r18) throws android.os.RemoteException {
            /*
            r14 = this;
            r2 = com.android.server.tv.TvInputManagerService.this;
            r2 = r2.mContext;
            r3 = "android.permission.CAPTURE_TV_INPUT";
            r2 = r2.checkCallingPermission(r3);
            if (r2 == 0) goto L_0x0018;
        L_0x000f:
            r2 = new java.lang.SecurityException;
            r3 = "Requires CAPTURE_TV_INPUT permission";
            r2.<init>(r3);
            throw r2;
        L_0x0018:
            r10 = android.os.Binder.clearCallingIdentity();
            r6 = android.os.Binder.getCallingUid();
            r2 = com.android.server.tv.TvInputManagerService.this;
            r3 = android.os.Binder.getCallingPid();
            r4 = "captureFrame";
            r0 = r18;
            r7 = r2.resolveCallingUserId(r3, r6, r0, r4);
            r8 = 0;
            r2 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x00ba }
            r3 = r2.mLock;	 Catch:{ all -> 0x00ba }
            monitor-enter(r3);	 Catch:{ all -> 0x00ba }
            r2 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x00b7 }
            r13 = r2.getOrCreateUserStateLocked(r7);	 Catch:{ all -> 0x00b7 }
            r2 = r13.inputMap;	 Catch:{ all -> 0x00b7 }
            r2 = r2.get(r15);	 Catch:{ all -> 0x00b7 }
            if (r2 != 0) goto L_0x0067;
        L_0x0047:
            r2 = "TvInputManagerService";
            r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00b7 }
            r4.<init>();	 Catch:{ all -> 0x00b7 }
            r5 = "input not found for ";
            r4 = r4.append(r5);	 Catch:{ all -> 0x00b7 }
            r4 = r4.append(r15);	 Catch:{ all -> 0x00b7 }
            r4 = r4.toString();	 Catch:{ all -> 0x00b7 }
            android.util.Slog.e(r2, r4);	 Catch:{ all -> 0x00b7 }
            monitor-exit(r3);	 Catch:{ all -> 0x00ba }
            r2 = 0;
            android.os.Binder.restoreCallingIdentity(r10);
            return r2;
        L_0x0067:
            r2 = r13.sessionStateMap;	 Catch:{ all -> 0x00b7 }
            r2 = r2.values();	 Catch:{ all -> 0x00b7 }
            r12 = r2.iterator();	 Catch:{ all -> 0x00b7 }
        L_0x0073:
            r2 = r12.hasNext();	 Catch:{ all -> 0x00b7 }
            if (r2 == 0) goto L_0x00a1;
        L_0x0079:
            r9 = r12.next();	 Catch:{ all -> 0x00b7 }
            r9 = (com.android.server.tv.TvInputManagerService.SessionState) r9;	 Catch:{ all -> 0x00b7 }
            r2 = r9.inputId;	 Catch:{ all -> 0x00b7 }
            r2 = r2.equals(r15);	 Catch:{ all -> 0x00b7 }
            if (r2 == 0) goto L_0x0073;
        L_0x0089:
            r2 = r9.hardwareSessionToken;	 Catch:{ all -> 0x00b7 }
            if (r2 == 0) goto L_0x0073;
        L_0x008f:
            r2 = r13.sessionStateMap;	 Catch:{ all -> 0x00b7 }
            r4 = r9.hardwareSessionToken;	 Catch:{ all -> 0x00b7 }
            r2 = r2.get(r4);	 Catch:{ all -> 0x00b7 }
            r2 = (com.android.server.tv.TvInputManagerService.SessionState) r2;	 Catch:{ all -> 0x00b7 }
            r8 = r2.inputId;	 Catch:{ all -> 0x00b7 }
        L_0x00a1:
            monitor-exit(r3);	 Catch:{ all -> 0x00ba }
            r2 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x00ba }
            r2 = r2.mTvInputHardwareManager;	 Catch:{ all -> 0x00ba }
            if (r8 == 0) goto L_0x00bf;
        L_0x00aa:
            r3 = r8;
        L_0x00ab:
            r4 = r16;
            r5 = r17;
            r2 = r2.captureFrame(r3, r4, r5, r6, r7);	 Catch:{ all -> 0x00ba }
            android.os.Binder.restoreCallingIdentity(r10);
            return r2;
        L_0x00b7:
            r2 = move-exception;
            monitor-exit(r3);	 Catch:{ all -> 0x00ba }
            throw r2;	 Catch:{ all -> 0x00ba }
        L_0x00ba:
            r2 = move-exception;
            android.os.Binder.restoreCallingIdentity(r10);
            throw r2;
        L_0x00bf:
            r3 = r15;
            goto L_0x00ab;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.BinderService.captureFrame(java.lang.String, android.view.Surface, android.media.tv.TvStreamConfig, int):boolean");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean isSingleSessionActive(int r13) throws android.os.RemoteException {
            /*
            r12 = this;
            r11 = 2;
            r10 = 0;
            r9 = 1;
            r2 = android.os.Binder.clearCallingIdentity();
            r0 = android.os.Binder.getCallingUid();
            r6 = com.android.server.tv.TvInputManagerService.this;
            r7 = android.os.Binder.getCallingPid();
            r8 = "isSingleSessionActive";
            r1 = r6.resolveCallingUserId(r7, r0, r13, r8);
            r6 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x006e }
            r7 = r6.mLock;	 Catch:{ all -> 0x006e }
            monitor-enter(r7);	 Catch:{ all -> 0x006e }
            r6 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x006b }
            r5 = r6.getOrCreateUserStateLocked(r1);	 Catch:{ all -> 0x006b }
            r6 = r5.sessionStateMap;	 Catch:{ all -> 0x006b }
            r6 = r6.size();	 Catch:{ all -> 0x006b }
            if (r6 != r9) goto L_0x0034;
        L_0x002f:
            monitor-exit(r7);	 Catch:{ all -> 0x006e }
            android.os.Binder.restoreCallingIdentity(r2);
            return r9;
        L_0x0034:
            r6 = r5.sessionStateMap;	 Catch:{ all -> 0x006b }
            r6 = r6.size();	 Catch:{ all -> 0x006b }
            if (r6 != r11) goto L_0x0066;
        L_0x003e:
            r6 = r5.sessionStateMap;	 Catch:{ all -> 0x006b }
            r6 = r6.values();	 Catch:{ all -> 0x006b }
            r8 = 2;
            r8 = new com.android.server.tv.TvInputManagerService.SessionState[r8];	 Catch:{ all -> 0x006b }
            r4 = r6.toArray(r8);	 Catch:{ all -> 0x006b }
            r4 = (com.android.server.tv.TvInputManagerService.SessionState[]) r4;	 Catch:{ all -> 0x006b }
            r6 = 0;
            r6 = r4[r6];	 Catch:{ all -> 0x006b }
            r6 = r6.hardwareSessionToken;	 Catch:{ all -> 0x006b }
            if (r6 != 0) goto L_0x0061;
        L_0x0058:
            r6 = 1;
            r6 = r4[r6];	 Catch:{ all -> 0x006b }
            r6 = r6.hardwareSessionToken;	 Catch:{ all -> 0x006b }
            if (r6 == 0) goto L_0x0066;
        L_0x0061:
            monitor-exit(r7);	 Catch:{ all -> 0x006e }
            android.os.Binder.restoreCallingIdentity(r2);
            return r9;
        L_0x0066:
            monitor-exit(r7);	 Catch:{ all -> 0x006e }
            android.os.Binder.restoreCallingIdentity(r2);
            return r10;
        L_0x006b:
            r6 = move-exception;
            monitor-exit(r7);	 Catch:{ all -> 0x006e }
            throw r6;	 Catch:{ all -> 0x006e }
        L_0x006e:
            r6 = move-exception;
            android.os.Binder.restoreCallingIdentity(r2);
            throw r6;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.BinderService.isSingleSessionActive(int):boolean");
        }

        public void requestChannelBrowsable(Uri channelUri, int userId) throws RemoteException {
            String callingPackageName = getCallingPackageName();
            long identity = Binder.clearCallingIdentity();
            int resolvedUserId = TvInputManagerService.this.resolveCallingUserId(Binder.getCallingPid(), Binder.getCallingUid(), userId, "requestChannelBrowsable");
            try {
                Intent intent = new Intent("android.media.tv.action.CHANNEL_BROWSABLE_REQUESTED");
                List<ResolveInfo> list = TvInputManagerService.this.getContext().getPackageManager().queryBroadcastReceivers(intent, 0);
                if (list != null) {
                    for (ResolveInfo info : list) {
                        String receiverPackageName = info.activityInfo.packageName;
                        intent.putExtra("android.media.tv.extra.CHANNEL_ID", ContentUris.parseId(channelUri));
                        intent.putExtra("android.media.tv.extra.PACKAGE_NAME", callingPackageName);
                        intent.setPackage(receiverPackageName);
                        TvInputManagerService.this.getContext().sendBroadcastAsUser(intent, new UserHandle(resolvedUserId));
                    }
                }
                Binder.restoreCallingIdentity(identity);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(identity);
            }
        }

        protected void dump(FileDescriptor fd, PrintWriter writer, String[] args) {
            IndentingPrintWriter pw = new IndentingPrintWriter(writer, "  ");
            if (DumpUtils.checkDumpPermission(TvInputManagerService.this.mContext, TvInputManagerService.TAG, pw)) {
                synchronized (TvInputManagerService.this.mLock) {
                    int i;
                    pw.println("User Ids (Current user: " + TvInputManagerService.this.mCurrentUserId + "):");
                    pw.increaseIndent();
                    for (i = 0; i < TvInputManagerService.this.mUserStates.size(); i++) {
                        pw.println(Integer.valueOf(TvInputManagerService.this.mUserStates.keyAt(i)));
                    }
                    pw.decreaseIndent();
                    for (i = 0; i < TvInputManagerService.this.mUserStates.size(); i++) {
                        int userId = TvInputManagerService.this.mUserStates.keyAt(i);
                        UserState userState = TvInputManagerService.this.getOrCreateUserStateLocked(userId);
                        pw.println("UserState (" + userId + "):");
                        pw.increaseIndent();
                        pw.println("inputMap: inputId -> TvInputState");
                        pw.increaseIndent();
                        for (Entry<String, TvInputState> entry : userState.inputMap.entrySet()) {
                            pw.println(((String) entry.getKey()) + ": " + entry.getValue());
                        }
                        pw.decreaseIndent();
                        pw.println("packageSet:");
                        pw.increaseIndent();
                        for (String packageName : userState.packageSet) {
                            pw.println(packageName);
                        }
                        pw.decreaseIndent();
                        pw.println("clientStateMap: ITvInputClient -> ClientState");
                        pw.increaseIndent();
                        for (Entry<IBinder, ClientState> entry2 : userState.clientStateMap.entrySet()) {
                            ClientState client = (ClientState) entry2.getValue();
                            pw.println(entry2.getKey() + ": " + client);
                            pw.increaseIndent();
                            pw.println("sessionTokens:");
                            pw.increaseIndent();
                            for (IBinder token : client.sessionTokens) {
                                pw.println("" + token);
                            }
                            pw.decreaseIndent();
                            pw.println("clientTokens: " + client.clientToken);
                            pw.println("userId: " + client.userId);
                            pw.decreaseIndent();
                        }
                        pw.decreaseIndent();
                        pw.println("serviceStateMap: ComponentName -> ServiceState");
                        pw.increaseIndent();
                        for (Entry<ComponentName, ServiceState> entry3 : userState.serviceStateMap.entrySet()) {
                            ServiceState service = (ServiceState) entry3.getValue();
                            pw.println(entry3.getKey() + ": " + service);
                            pw.increaseIndent();
                            pw.println("sessionTokens:");
                            pw.increaseIndent();
                            for (IBinder token2 : service.sessionTokens) {
                                pw.println("" + token2);
                            }
                            pw.decreaseIndent();
                            pw.println("service: " + service.service);
                            pw.println("callback: " + service.callback);
                            pw.println("bound: " + service.bound);
                            pw.println("reconnecting: " + service.reconnecting);
                            pw.decreaseIndent();
                        }
                        pw.decreaseIndent();
                        pw.println("sessionStateMap: ITvInputSession -> SessionState");
                        pw.increaseIndent();
                        for (Entry<IBinder, SessionState> entry4 : userState.sessionStateMap.entrySet()) {
                            SessionState session = (SessionState) entry4.getValue();
                            pw.println(entry4.getKey() + ": " + session);
                            pw.increaseIndent();
                            pw.println("inputId: " + session.inputId);
                            pw.println("client: " + session.client);
                            pw.println("seq: " + session.seq);
                            pw.println("callingUid: " + session.callingUid);
                            pw.println("userId: " + session.userId);
                            pw.println("sessionToken: " + session.sessionToken);
                            pw.println("session: " + session.session);
                            pw.println("logUri: " + session.logUri);
                            pw.println("hardwareSessionToken: " + session.hardwareSessionToken);
                            pw.decreaseIndent();
                        }
                        pw.decreaseIndent();
                        pw.println("callbackSet:");
                        pw.increaseIndent();
                        for (ITvInputManagerCallback callback : userState.callbackSet) {
                            pw.println(callback.toString());
                        }
                        pw.decreaseIndent();
                        pw.println("mainSessionToken: " + userState.mainSessionToken);
                        pw.decreaseIndent();
                    }
                }
                TvInputManagerService.this.mTvInputHardwareManager.dump(fd, writer, args);
            }
        }
    }

    private final class ClientState implements DeathRecipient {
        private IBinder clientToken;
        private final List<IBinder> sessionTokens = new ArrayList();
        private final int userId;

        ClientState(IBinder clientToken, int userId) {
            this.clientToken = clientToken;
            this.userId = userId;
        }

        public boolean isEmpty() {
            return this.sessionTokens.isEmpty();
        }

        public void binderDied() {
            synchronized (TvInputManagerService.this.mLock) {
                ClientState clientState = (ClientState) TvInputManagerService.this.getOrCreateUserStateLocked(this.userId).clientStateMap.get(this.clientToken);
                if (clientState != null) {
                    while (clientState.sessionTokens.size() > 0) {
                        TvInputManagerService.this.releaseSessionLocked((IBinder) clientState.sessionTokens.get(0), 1000, this.userId);
                    }
                }
                this.clientToken = null;
            }
        }
    }

    private final class HardwareListener implements Listener {
        private HardwareListener() {
        }

        public void onStateChanged(String inputId, int state) {
            synchronized (TvInputManagerService.this.mLock) {
                TvInputManagerService.this.setStateLocked(inputId, state, TvInputManagerService.this.mCurrentUserId);
            }
        }

        public void onHardwareDeviceAdded(TvInputHardwareInfo info) {
            synchronized (TvInputManagerService.this.mLock) {
                for (ServiceState serviceState : TvInputManagerService.this.getOrCreateUserStateLocked(TvInputManagerService.this.mCurrentUserId).serviceStateMap.values()) {
                    if (serviceState.isHardware && serviceState.service != null) {
                        try {
                            serviceState.service.notifyHardwareAdded(info);
                        } catch (RemoteException e) {
                            Slog.e(TvInputManagerService.TAG, "error in notifyHardwareAdded", e);
                        }
                    }
                }
            }
        }

        public void onHardwareDeviceRemoved(TvInputHardwareInfo info) {
            synchronized (TvInputManagerService.this.mLock) {
                for (ServiceState serviceState : TvInputManagerService.this.getOrCreateUserStateLocked(TvInputManagerService.this.mCurrentUserId).serviceStateMap.values()) {
                    if (serviceState.isHardware && serviceState.service != null) {
                        try {
                            serviceState.service.notifyHardwareRemoved(info);
                        } catch (RemoteException e) {
                            Slog.e(TvInputManagerService.TAG, "error in notifyHardwareRemoved", e);
                        }
                    }
                }
            }
        }

        public void onHdmiDeviceAdded(HdmiDeviceInfo deviceInfo) {
            synchronized (TvInputManagerService.this.mLock) {
                for (ServiceState serviceState : TvInputManagerService.this.getOrCreateUserStateLocked(TvInputManagerService.this.mCurrentUserId).serviceStateMap.values()) {
                    if (serviceState.isHardware && serviceState.service != null) {
                        try {
                            serviceState.service.notifyHdmiDeviceAdded(deviceInfo);
                        } catch (RemoteException e) {
                            Slog.e(TvInputManagerService.TAG, "error in notifyHdmiDeviceAdded", e);
                        }
                    }
                }
            }
        }

        public void onHdmiDeviceRemoved(HdmiDeviceInfo deviceInfo) {
            synchronized (TvInputManagerService.this.mLock) {
                for (ServiceState serviceState : TvInputManagerService.this.getOrCreateUserStateLocked(TvInputManagerService.this.mCurrentUserId).serviceStateMap.values()) {
                    if (serviceState.isHardware && serviceState.service != null) {
                        try {
                            serviceState.service.notifyHdmiDeviceRemoved(deviceInfo);
                        } catch (RemoteException e) {
                            Slog.e(TvInputManagerService.TAG, "error in notifyHdmiDeviceRemoved", e);
                        }
                    }
                }
            }
        }

        public void onHdmiDeviceUpdated(String inputId, HdmiDeviceInfo deviceInfo) {
            synchronized (TvInputManagerService.this.mLock) {
                Integer state;
                switch (deviceInfo.getDevicePowerStatus()) {
                    case 0:
                        state = Integer.valueOf(0);
                        break;
                    case 1:
                    case 2:
                    case 3:
                        state = Integer.valueOf(1);
                        break;
                    default:
                        state = null;
                        break;
                }
                if (state != null) {
                    TvInputManagerService.this.setStateLocked(inputId, state.intValue(), TvInputManagerService.this.mCurrentUserId);
                }
            }
        }
    }

    private final class InputServiceConnection implements ServiceConnection {
        private final ComponentName mComponent;
        private final int mUserId;

        private InputServiceConnection(ComponentName component, int userId) {
            this.mComponent = component;
            this.mUserId = userId;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onServiceConnected(android.content.ComponentName r18, android.os.IBinder r19) {
            /*
            r17 = this;
            r0 = r17;
            r12 = com.android.server.tv.TvInputManagerService.this;
            r13 = r12.mLock;
            monitor-enter(r13);
            r0 = r17;
            r12 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0094 }
            r12 = r12.mUserStates;	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r14 = r0.mUserId;	 Catch:{ all -> 0x0094 }
            r11 = r12.get(r14);	 Catch:{ all -> 0x0094 }
            r11 = (com.android.server.tv.TvInputManagerService.UserState) r11;	 Catch:{ all -> 0x0094 }
            if (r11 != 0) goto L_0x002c;
        L_0x001d:
            r0 = r17;
            r12 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0094 }
            r12 = r12.mContext;	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r12.unbindService(r0);	 Catch:{ all -> 0x0094 }
            monitor-exit(r13);
            return;
        L_0x002c:
            r12 = r11.serviceStateMap;	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r14 = r0.mComponent;	 Catch:{ all -> 0x0094 }
            r8 = r12.get(r14);	 Catch:{ all -> 0x0094 }
            r8 = (com.android.server.tv.TvInputManagerService.ServiceState) r8;	 Catch:{ all -> 0x0094 }
            r12 = android.media.tv.ITvInputService.Stub.asInterface(r19);	 Catch:{ all -> 0x0094 }
            r8.service = r12;	 Catch:{ all -> 0x0094 }
            r12 = r8.isHardware;	 Catch:{ all -> 0x0094 }
            if (r12 == 0) goto L_0x0070;
        L_0x0047:
            r12 = r8.callback;	 Catch:{ all -> 0x0094 }
            if (r12 != 0) goto L_0x0070;
        L_0x004d:
            r12 = new com.android.server.tv.TvInputManagerService$ServiceCallback;	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r14 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r15 = r0.mComponent;	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r0 = r0.mUserId;	 Catch:{ all -> 0x0094 }
            r16 = r0;
            r0 = r16;
            r12.<init>(r15, r0);	 Catch:{ all -> 0x0094 }
            r8.callback = r12;	 Catch:{ all -> 0x0094 }
            r12 = r8.service;	 Catch:{ RemoteException -> 0x0097 }
            r14 = r8.callback;	 Catch:{ RemoteException -> 0x0097 }
            r12.registerCallback(r14);	 Catch:{ RemoteException -> 0x0097 }
        L_0x0070:
            r12 = r8.sessionTokens;	 Catch:{ all -> 0x0094 }
            r10 = r12.iterator();	 Catch:{ all -> 0x0094 }
        L_0x0078:
            r12 = r10.hasNext();	 Catch:{ all -> 0x0094 }
            if (r12 == 0) goto L_0x00a2;
        L_0x007e:
            r9 = r10.next();	 Catch:{ all -> 0x0094 }
            r9 = (android.os.IBinder) r9;	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r12 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0094 }
            r14 = r8.service;	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r15 = r0.mUserId;	 Catch:{ all -> 0x0094 }
            r12.createSessionInternalLocked(r14, r9, r15);	 Catch:{ all -> 0x0094 }
            goto L_0x0078;
        L_0x0094:
            r12 = move-exception;
            monitor-exit(r13);
            throw r12;
        L_0x0097:
            r3 = move-exception;
            r12 = "TvInputManagerService";
            r14 = "error in registerCallback";
            android.util.Slog.e(r12, r14, r3);	 Catch:{ all -> 0x0094 }
            goto L_0x0070;
        L_0x00a2:
            r12 = r11.inputMap;	 Catch:{ all -> 0x0094 }
            r12 = r12.values();	 Catch:{ all -> 0x0094 }
            r7 = r12.iterator();	 Catch:{ all -> 0x0094 }
        L_0x00ae:
            r12 = r7.hasNext();	 Catch:{ all -> 0x0094 }
            if (r12 == 0) goto L_0x00e8;
        L_0x00b4:
            r6 = r7.next();	 Catch:{ all -> 0x0094 }
            r6 = (com.android.server.tv.TvInputManagerService.TvInputState) r6;	 Catch:{ all -> 0x0094 }
            r12 = r6.info;	 Catch:{ all -> 0x0094 }
            r12 = r12.getComponent();	 Catch:{ all -> 0x0094 }
            r0 = r18;
            r12 = r12.equals(r0);	 Catch:{ all -> 0x0094 }
            if (r12 == 0) goto L_0x00ae;
        L_0x00ca:
            r12 = r6.state;	 Catch:{ all -> 0x0094 }
            if (r12 == 0) goto L_0x00ae;
        L_0x00d0:
            r0 = r17;
            r12 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0094 }
            r14 = r6.info;	 Catch:{ all -> 0x0094 }
            r14 = r14.getId();	 Catch:{ all -> 0x0094 }
            r15 = r6.state;	 Catch:{ all -> 0x0094 }
            r16 = 0;
            r0 = r16;
            r12.notifyInputStateChangedLocked(r11, r14, r15, r0);	 Catch:{ all -> 0x0094 }
            goto L_0x00ae;
        L_0x00e8:
            r12 = r8.isHardware;	 Catch:{ all -> 0x0094 }
            if (r12 == 0) goto L_0x0153;
        L_0x00ee:
            r12 = r8.hardwareInputMap;	 Catch:{ all -> 0x0094 }
            r12.clear();	 Catch:{ all -> 0x0094 }
            r0 = r17;
            r12 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0094 }
            r12 = r12.mTvInputHardwareManager;	 Catch:{ all -> 0x0094 }
            r12 = r12.getHardwareList();	 Catch:{ all -> 0x0094 }
            r5 = r12.iterator();	 Catch:{ all -> 0x0094 }
        L_0x0105:
            r12 = r5.hasNext();	 Catch:{ all -> 0x0094 }
            if (r12 == 0) goto L_0x0124;
        L_0x010b:
            r4 = r5.next();	 Catch:{ all -> 0x0094 }
            r4 = (android.media.tv.TvInputHardwareInfo) r4;	 Catch:{ all -> 0x0094 }
            r12 = r8.service;	 Catch:{ RemoteException -> 0x0119 }
            r12.notifyHardwareAdded(r4);	 Catch:{ RemoteException -> 0x0119 }
            goto L_0x0105;
        L_0x0119:
            r3 = move-exception;
            r12 = "TvInputManagerService";
            r14 = "error in notifyHardwareAdded";
            android.util.Slog.e(r12, r14, r3);	 Catch:{ all -> 0x0094 }
            goto L_0x0105;
        L_0x0124:
            r0 = r17;
            r12 = com.android.server.tv.TvInputManagerService.this;	 Catch:{ all -> 0x0094 }
            r12 = r12.mTvInputHardwareManager;	 Catch:{ all -> 0x0094 }
            r12 = r12.getHdmiDeviceList();	 Catch:{ all -> 0x0094 }
            r2 = r12.iterator();	 Catch:{ all -> 0x0094 }
        L_0x0134:
            r12 = r2.hasNext();	 Catch:{ all -> 0x0094 }
            if (r12 == 0) goto L_0x0153;
        L_0x013a:
            r1 = r2.next();	 Catch:{ all -> 0x0094 }
            r1 = (android.hardware.hdmi.HdmiDeviceInfo) r1;	 Catch:{ all -> 0x0094 }
            r12 = r8.service;	 Catch:{ RemoteException -> 0x0148 }
            r12.notifyHdmiDeviceAdded(r1);	 Catch:{ RemoteException -> 0x0148 }
            goto L_0x0134;
        L_0x0148:
            r3 = move-exception;
            r12 = "TvInputManagerService";
            r14 = "error in notifyHdmiDeviceAdded";
            android.util.Slog.e(r12, r14, r3);	 Catch:{ all -> 0x0094 }
            goto L_0x0134;
        L_0x0153:
            monitor-exit(r13);
            return;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.InputServiceConnection.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
        }

        public void onServiceDisconnected(ComponentName component) {
            if (this.mComponent.equals(component)) {
                synchronized (TvInputManagerService.this.mLock) {
                    ServiceState serviceState = (ServiceState) TvInputManagerService.this.getOrCreateUserStateLocked(this.mUserId).serviceStateMap.get(this.mComponent);
                    if (serviceState != null) {
                        serviceState.reconnecting = true;
                        serviceState.bound = false;
                        serviceState.service = null;
                        serviceState.callback = null;
                        TvInputManagerService.this.abortPendingCreateSessionRequestsLocked(serviceState, null, this.mUserId);
                    }
                }
                return;
            }
            throw new IllegalArgumentException("Mismatched ComponentName: " + this.mComponent + " (expected), " + component + " (actual).");
        }
    }

    private final class ServiceCallback extends ITvInputServiceCallback.Stub {
        private final ComponentName mComponent;
        private final int mUserId;

        ServiceCallback(ComponentName component, int userId) {
            this.mComponent = component;
            this.mUserId = userId;
        }

        private void ensureHardwarePermission() {
            if (TvInputManagerService.this.mContext.checkCallingPermission("android.permission.TV_INPUT_HARDWARE") != 0) {
                throw new SecurityException("The caller does not have hardware permission");
            }
        }

        private void ensureValidInput(TvInputInfo inputInfo) {
            if (inputInfo.getId() == null || (this.mComponent.equals(inputInfo.getComponent()) ^ 1) != 0) {
                throw new IllegalArgumentException("Invalid TvInputInfo");
            }
        }

        private void addHardwareInputLocked(TvInputInfo inputInfo) {
            TvInputManagerService.this.getServiceStateLocked(this.mComponent, this.mUserId).hardwareInputMap.put(inputInfo.getId(), inputInfo);
            TvInputManagerService.this.buildTvInputListLocked(this.mUserId, null);
        }

        public void addHardwareInput(int deviceId, TvInputInfo inputInfo) {
            ensureHardwarePermission();
            ensureValidInput(inputInfo);
            synchronized (TvInputManagerService.this.mLock) {
                TvInputManagerService.this.mTvInputHardwareManager.addHardwareInput(deviceId, inputInfo);
                addHardwareInputLocked(inputInfo);
            }
        }

        public void addHdmiInput(int id, TvInputInfo inputInfo) {
            ensureHardwarePermission();
            ensureValidInput(inputInfo);
            synchronized (TvInputManagerService.this.mLock) {
                TvInputManagerService.this.mTvInputHardwareManager.addHdmiInput(id, inputInfo);
                addHardwareInputLocked(inputInfo);
            }
        }

        public void removeHardwareInput(String inputId) {
            ensureHardwarePermission();
            synchronized (TvInputManagerService.this.mLock) {
                if (TvInputManagerService.this.getServiceStateLocked(this.mComponent, this.mUserId).hardwareInputMap.remove(inputId) != null) {
                    TvInputManagerService.this.buildTvInputListLocked(this.mUserId, null);
                    TvInputManagerService.this.mTvInputHardwareManager.removeHardwareInput(inputId);
                } else {
                    Slog.e(TvInputManagerService.TAG, "failed to remove input " + inputId);
                }
            }
        }
    }

    private final class ServiceState {
        private boolean bound;
        private ServiceCallback callback;
        private final ComponentName component;
        private final ServiceConnection connection;
        private final Map<String, TvInputInfo> hardwareInputMap;
        private final boolean isHardware;
        private boolean reconnecting;
        private ITvInputService service;
        private final List<IBinder> sessionTokens;

        private ServiceState(ComponentName component, int userId) {
            this.sessionTokens = new ArrayList();
            this.hardwareInputMap = new HashMap();
            this.component = component;
            this.connection = new InputServiceConnection(component, userId);
            this.isHardware = TvInputManagerService.hasHardwarePermission(TvInputManagerService.this.mContext.getPackageManager(), component);
        }
    }

    private final class SessionCallback extends ITvInputSessionCallback.Stub {
        private final InputChannel[] mChannels;
        private final SessionState mSessionState;

        SessionCallback(SessionState sessionState, InputChannel[] channels) {
            this.mSessionState = sessionState;
            this.mChannels = channels;
        }

        public void onSessionCreated(ITvInputSession session, IBinder hardwareSessionToken) {
            synchronized (TvInputManagerService.this.mLock) {
                this.mSessionState.session = session;
                this.mSessionState.hardwareSessionToken = hardwareSessionToken;
                if (session == null || !addSessionTokenToClientStateLocked(session)) {
                    TvInputManagerService.this.removeSessionStateLocked(this.mSessionState.sessionToken, this.mSessionState.userId);
                    TvInputManagerService.this.sendSessionTokenToClientLocked(this.mSessionState.client, this.mSessionState.inputId, null, null, this.mSessionState.seq);
                } else {
                    TvInputManagerService.this.sendSessionTokenToClientLocked(this.mSessionState.client, this.mSessionState.inputId, this.mSessionState.sessionToken, this.mChannels[0], this.mSessionState.seq);
                }
                this.mChannels[0].dispose();
            }
        }

        private boolean addSessionTokenToClientStateLocked(ITvInputSession session) {
            try {
                session.asBinder().linkToDeath(this.mSessionState, 0);
                IBinder clientToken = this.mSessionState.client.asBinder();
                UserState userState = TvInputManagerService.this.getOrCreateUserStateLocked(this.mSessionState.userId);
                ClientState clientState = (ClientState) userState.clientStateMap.get(clientToken);
                if (clientState == null) {
                    clientState = new ClientState(clientToken, this.mSessionState.userId);
                    try {
                        clientToken.linkToDeath(clientState, 0);
                        userState.clientStateMap.put(clientToken, clientState);
                    } catch (RemoteException e) {
                        Slog.e(TvInputManagerService.TAG, "client process has already died", e);
                        return false;
                    }
                }
                clientState.sessionTokens.add(this.mSessionState.sessionToken);
                return true;
            } catch (RemoteException e2) {
                Slog.e(TvInputManagerService.TAG, "session process has already died", e2);
                return false;
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onChannelRetuned(android.net.Uri r5) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onChannelRetuned(r5, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onChannelRetuned";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onChannelRetuned(android.net.Uri):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onTracksChanged(java.util.List<android.media.tv.TvTrackInfo> r5) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onTracksChanged(r5, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onTracksChanged";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onTracksChanged(java.util.List):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onTrackSelected(int r5, java.lang.String r6) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onTrackSelected(r5, r6, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onTrackSelected";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onTrackSelected(int, java.lang.String):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onVideoAvailable() {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onVideoAvailable(r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onVideoAvailable";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onVideoAvailable():void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onVideoUnavailable(int r5) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onVideoUnavailable(r5, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onVideoUnavailable";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onVideoUnavailable(int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onContentAllowed() {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onContentAllowed(r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onContentAllowed";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onContentAllowed():void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onContentBlocked(java.lang.String r5) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onContentBlocked(r5, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onContentBlocked";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onContentBlocked(java.lang.String):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onLayoutSurface(int r9, int r10, int r11, int r12) {
            /*
            r8 = this;
            r0 = com.android.server.tv.TvInputManagerService.this;
            r7 = r0.mLock;
            monitor-enter(r7);
            r0 = r8.mSessionState;	 Catch:{ all -> 0x0039 }
            r0 = r0.session;	 Catch:{ all -> 0x0039 }
            if (r0 == 0) goto L_0x0017;
        L_0x000f:
            r0 = r8.mSessionState;	 Catch:{ all -> 0x0039 }
            r0 = r0.client;	 Catch:{ all -> 0x0039 }
            if (r0 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r7);
            return;
        L_0x0019:
            r0 = r8.mSessionState;	 Catch:{ RemoteException -> 0x002e }
            r0 = r0.client;	 Catch:{ RemoteException -> 0x002e }
            r1 = r8.mSessionState;	 Catch:{ RemoteException -> 0x002e }
            r5 = r1.seq;	 Catch:{ RemoteException -> 0x002e }
            r1 = r9;
            r2 = r10;
            r3 = r11;
            r4 = r12;
            r0.onLayoutSurface(r1, r2, r3, r4, r5);	 Catch:{ RemoteException -> 0x002e }
        L_0x002c:
            monitor-exit(r7);
            return;
        L_0x002e:
            r6 = move-exception;
            r0 = "TvInputManagerService";
            r1 = "error in onLayoutSurface";
            android.util.Slog.e(r0, r1, r6);	 Catch:{ all -> 0x0039 }
            goto L_0x002c;
        L_0x0039:
            r0 = move-exception;
            monitor-exit(r7);
            throw r0;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onLayoutSurface(int, int, int, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onSessionEvent(java.lang.String r5, android.os.Bundle r6) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onSessionEvent(r5, r6, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onSessionEvent";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onSessionEvent(java.lang.String, android.os.Bundle):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onTimeShiftStatusChanged(int r5) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onTimeShiftStatusChanged(r5, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onTimeShiftStatusChanged";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onTimeShiftStatusChanged(int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onTimeShiftStartPositionChanged(long r6) {
            /*
            r5 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r5.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r5.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r5.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r5.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onTimeShiftStartPositionChanged(r6, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onTimeShiftStartPositionChanged";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onTimeShiftStartPositionChanged(long):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onTimeShiftCurrentPositionChanged(long r6) {
            /*
            r5 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r5.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r5.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r5.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r5.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onTimeShiftCurrentPositionChanged(r6, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onTimeShiftCurrentPositionChanged";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onTimeShiftCurrentPositionChanged(long):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onTuned(android.net.Uri r5) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onTuned(r3, r5);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onTuned";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onTuned(android.net.Uri):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onRecordingStopped(android.net.Uri r5) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onRecordingStopped(r5, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onRecordingStopped";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onRecordingStopped(android.net.Uri):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onError(int r5) {
            /*
            r4 = this;
            r1 = com.android.server.tv.TvInputManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.session;	 Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0017;
        L_0x000f:
            r1 = r4.mSessionState;	 Catch:{ all -> 0x0035 }
            r1 = r1.client;	 Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r2);
            return;
        L_0x0019:
            r1 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r1 = r1.client;	 Catch:{ RemoteException -> 0x002a }
            r3 = r4.mSessionState;	 Catch:{ RemoteException -> 0x002a }
            r3 = r3.seq;	 Catch:{ RemoteException -> 0x002a }
            r1.onError(r5, r3);	 Catch:{ RemoteException -> 0x002a }
        L_0x0028:
            monitor-exit(r2);
            return;
        L_0x002a:
            r0 = move-exception;
            r1 = "TvInputManagerService";
            r3 = "error in onError";
            android.util.Slog.e(r1, r3, r0);	 Catch:{ all -> 0x0035 }
            goto L_0x0028;
        L_0x0035:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.SessionCallback.onError(int):void");
        }
    }

    private static class SessionNotFoundException extends IllegalArgumentException {
        public SessionNotFoundException(String name) {
            super(name);
        }
    }

    private final class SessionState implements DeathRecipient {
        private final int callingUid;
        private final ITvInputClient client;
        private final ComponentName componentName;
        private IBinder hardwareSessionToken;
        private final String inputId;
        private final boolean isRecordingSession;
        private Uri logUri;
        private final int seq;
        private ITvInputSession session;
        private final IBinder sessionToken;
        private final int userId;

        private SessionState(IBinder sessionToken, String inputId, ComponentName componentName, boolean isRecordingSession, ITvInputClient client, int seq, int callingUid, int userId) {
            this.sessionToken = sessionToken;
            this.inputId = inputId;
            this.componentName = componentName;
            this.isRecordingSession = isRecordingSession;
            this.client = client;
            this.seq = seq;
            this.callingUid = callingUid;
            this.userId = userId;
        }

        public void binderDied() {
            synchronized (TvInputManagerService.this.mLock) {
                this.session = null;
                TvInputManagerService.this.clearSessionAndNotifyClientLocked(this);
            }
        }
    }

    private static final class TvInputState {
        private TvInputInfo info;
        private int state;

        private TvInputState() {
            this.state = 0;
        }

        public String toString() {
            return "info: " + this.info + "; state: " + this.state;
        }
    }

    private static final class UserState {
        private final Set<ITvInputManagerCallback> callbackSet;
        private final Map<IBinder, ClientState> clientStateMap;
        private final List<TvContentRatingSystemInfo> contentRatingSystemList;
        private Map<String, TvInputState> inputMap;
        private IBinder mainSessionToken;
        private final Set<String> packageSet;
        private final PersistentDataStore persistentDataStore;
        private final Map<ComponentName, ServiceState> serviceStateMap;
        private final Map<IBinder, SessionState> sessionStateMap;

        private UserState(Context context, int userId) {
            this.inputMap = new HashMap();
            this.packageSet = new HashSet();
            this.contentRatingSystemList = new ArrayList();
            this.clientStateMap = new HashMap();
            this.serviceStateMap = new HashMap();
            this.sessionStateMap = new HashMap();
            this.callbackSet = new HashSet();
            this.mainSessionToken = null;
            this.persistentDataStore = new PersistentDataStore(context, userId);
        }
    }

    private static final class WatchLogHandler extends Handler {
        static final int MSG_LOG_WATCH_END = 2;
        static final int MSG_LOG_WATCH_START = 1;
        static final int MSG_SWITCH_CONTENT_RESOLVER = 3;
        private ContentResolver mContentResolver;

        WatchLogHandler(ContentResolver contentResolver, Looper looper) {
            super(looper);
            this.mContentResolver = contentResolver;
        }

        public void handleMessage(Message msg) {
            SomeArgs args;
            IBinder sessionToken;
            ContentValues values;
            switch (msg.what) {
                case 1:
                    args = msg.obj;
                    String packageName = args.arg1;
                    long watchStartTime = ((Long) args.arg2).longValue();
                    long channelId = ((Long) args.arg3).longValue();
                    Bundle tuneParams = args.arg4;
                    sessionToken = args.arg5;
                    values = new ContentValues();
                    values.put("package_name", packageName);
                    values.put("watch_start_time_utc_millis", Long.valueOf(watchStartTime));
                    values.put("channel_id", Long.valueOf(channelId));
                    if (tuneParams != null) {
                        values.put("tune_params", encodeTuneParams(tuneParams));
                    }
                    values.put("session_token", sessionToken.toString());
                    this.mContentResolver.insert(WatchedPrograms.CONTENT_URI, values);
                    args.recycle();
                    return;
                case 2:
                    args = (SomeArgs) msg.obj;
                    sessionToken = (IBinder) args.arg1;
                    long watchEndTime = ((Long) args.arg2).longValue();
                    values = new ContentValues();
                    values.put("watch_end_time_utc_millis", Long.valueOf(watchEndTime));
                    values.put("session_token", sessionToken.toString());
                    this.mContentResolver.insert(WatchedPrograms.CONTENT_URI, values);
                    args.recycle();
                    return;
                case 3:
                    this.mContentResolver = (ContentResolver) msg.obj;
                    return;
                default:
                    Slog.w(TvInputManagerService.TAG, "unhandled message code: " + msg.what);
                    return;
            }
        }

        private String encodeTuneParams(Bundle tuneParams) {
            StringBuilder builder = new StringBuilder();
            Iterator<String> it = tuneParams.keySet().iterator();
            while (it.hasNext()) {
                String key = (String) it.next();
                Object value = tuneParams.get(key);
                if (value != null) {
                    builder.append(replaceEscapeCharacters(key));
                    builder.append("=");
                    builder.append(replaceEscapeCharacters(value.toString()));
                    if (it.hasNext()) {
                        builder.append(", ");
                    }
                }
            }
            return builder.toString();
        }

        private String replaceEscapeCharacters(String src) {
            String ENCODING_TARGET_CHARACTERS = "%=,";
            StringBuilder builder = new StringBuilder();
            for (char ch : src.toCharArray()) {
                if ("%=,".indexOf(ch) >= 0) {
                    builder.append('%');
                }
                builder.append(ch);
            }
            return builder.toString();
        }
    }

    public TvInputManagerService(Context context) {
        super(context);
        this.mContext = context;
        this.mWatchLogHandler = new WatchLogHandler(this.mContext.getContentResolver(), IoThread.get().getLooper());
        this.mTvInputHardwareManager = new TvInputHardwareManager(context, new HardwareListener());
        synchronized (this.mLock) {
            getOrCreateUserStateLocked(this.mCurrentUserId);
        }
    }

    public void onStart() {
        publishBinderService("tv_input", new BinderService());
    }

    public void onBootPhase(int phase) {
        if (phase == SystemService.PHASE_SYSTEM_SERVICES_READY) {
            registerBroadcastReceivers();
        } else if (phase == 600) {
            synchronized (this.mLock) {
                buildTvInputListLocked(this.mCurrentUserId, null);
                buildTvContentRatingSystemListLocked(this.mCurrentUserId);
            }
        }
        this.mTvInputHardwareManager.onBootPhase(phase);
    }

    public void onUnlockUser(int userHandle) {
        synchronized (this.mLock) {
            if (this.mCurrentUserId != userHandle) {
                return;
            }
            buildTvInputListLocked(this.mCurrentUserId, null);
            buildTvContentRatingSystemListLocked(this.mCurrentUserId);
        }
    }

    private void registerBroadcastReceivers() {
        new PackageMonitor() {
            private void buildTvInputList(String[] packages) {
                synchronized (TvInputManagerService.this.mLock) {
                    if (TvInputManagerService.this.mCurrentUserId == getChangingUserId()) {
                        TvInputManagerService.this.buildTvInputListLocked(TvInputManagerService.this.mCurrentUserId, packages);
                        TvInputManagerService.this.buildTvContentRatingSystemListLocked(TvInputManagerService.this.mCurrentUserId);
                    }
                }
            }

            public void onPackageUpdateFinished(String packageName, int uid) {
                buildTvInputList(new String[]{packageName});
            }

            public void onPackagesAvailable(String[] packages) {
                if (isReplacing()) {
                    buildTvInputList(packages);
                }
            }

            public void onPackagesUnavailable(String[] packages) {
                if (isReplacing()) {
                    buildTvInputList(packages);
                }
            }

            public void onSomePackagesChanged() {
                if (!isReplacing()) {
                    buildTvInputList(null);
                }
            }

            public boolean onPackageChanged(String packageName, int uid, String[] components) {
                return true;
            }
        }.register(this.mContext, null, UserHandle.ALL, true);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.USER_SWITCHED");
        intentFilter.addAction("android.intent.action.USER_REMOVED");
        this.mContext.registerReceiverAsUser(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if ("android.intent.action.USER_SWITCHED".equals(action)) {
                    TvInputManagerService.this.switchUser(intent.getIntExtra("android.intent.extra.user_handle", 0));
                } else if ("android.intent.action.USER_REMOVED".equals(action)) {
                    TvInputManagerService.this.removeUser(intent.getIntExtra("android.intent.extra.user_handle", 0));
                }
            }
        }, UserHandle.ALL, intentFilter, null, null);
    }

    private static boolean hasHardwarePermission(PackageManager pm, ComponentName component) {
        return pm.checkPermission("android.permission.TV_INPUT_HARDWARE", component.getPackageName()) == 0;
    }

    private void buildTvInputListLocked(int userId, String[] updatedPackages) {
        UserState userState = getOrCreateUserStateLocked(userId);
        userState.packageSet.clear();
        PackageManager pm = this.mContext.getPackageManager();
        List<ResolveInfo> services = pm.queryIntentServicesAsUser(new Intent("android.media.tv.TvInputService"), 132, userId);
        List<TvInputInfo> inputList = new ArrayList();
        for (ResolveInfo ri : services) {
            ComponentName component;
            ServiceState serviceState;
            ServiceInfo si = ri.serviceInfo;
            if ("android.permission.BIND_TV_INPUT".equals(si.permission)) {
                component = new ComponentName(si.packageName, si.name);
                if (hasHardwarePermission(pm, component)) {
                    serviceState = (ServiceState) userState.serviceStateMap.get(component);
                    if (serviceState == null) {
                        userState.serviceStateMap.put(component, new ServiceState(component, userId));
                        updateServiceConnectionLocked(component, userId);
                    } else {
                        inputList.addAll(serviceState.hardwareInputMap.values());
                    }
                } else {
                    try {
                        inputList.add(new Builder(this.mContext, ri).build());
                    } catch (Exception e) {
                        Slog.e(TAG, "failed to load TV input " + si.name, e);
                    }
                }
                userState.packageSet.add(si.packageName);
            } else {
                Slog.w(TAG, "Skipping TV input " + si.name + ": it does not require the permission " + "android.permission.BIND_TV_INPUT");
            }
        }
        Map<String, TvInputState> inputMap = new HashMap();
        for (TvInputInfo info : inputList) {
            TvInputState inputState = (TvInputState) userState.inputMap.get(info.getId());
            if (inputState == null) {
                inputState = new TvInputState();
            }
            inputState.info = info;
            inputMap.put(info.getId(), inputState);
        }
        for (String inputId : inputMap.keySet()) {
            if (!userState.inputMap.containsKey(inputId)) {
                notifyInputAddedLocked(userState, inputId);
            } else if (updatedPackages != null) {
                component = ((TvInputState) inputMap.get(inputId)).info.getComponent();
                for (String updatedPackage : updatedPackages) {
                    if (component.getPackageName().equals(updatedPackage)) {
                        updateServiceConnectionLocked(component, userId);
                        notifyInputUpdatedLocked(userState, inputId);
                        break;
                    }
                }
            }
        }
        for (String inputId2 : userState.inputMap.keySet()) {
            if (!inputMap.containsKey(inputId2)) {
                serviceState = (ServiceState) userState.serviceStateMap.get(((TvInputState) userState.inputMap.get(inputId2)).info.getComponent());
                if (serviceState != null) {
                    abortPendingCreateSessionRequestsLocked(serviceState, inputId2, userId);
                }
                notifyInputRemovedLocked(userState, inputId2);
            }
        }
        userState.inputMap.clear();
        userState.inputMap = inputMap;
    }

    private void buildTvContentRatingSystemListLocked(int userId) {
        UserState userState = getOrCreateUserStateLocked(userId);
        userState.contentRatingSystemList.clear();
        for (ResolveInfo resolveInfo : this.mContext.getPackageManager().queryBroadcastReceivers(new Intent("android.media.tv.action.QUERY_CONTENT_RATING_SYSTEMS"), 128)) {
            ActivityInfo receiver = resolveInfo.activityInfo;
            Bundle metaData = receiver.metaData;
            if (metaData != null) {
                int xmlResId = metaData.getInt("android.media.tv.metadata.CONTENT_RATING_SYSTEMS");
                if (xmlResId == 0) {
                    Slog.w(TAG, "Missing meta-data 'android.media.tv.metadata.CONTENT_RATING_SYSTEMS' on receiver " + receiver.packageName + "/" + receiver.name);
                } else {
                    userState.contentRatingSystemList.add(TvContentRatingSystemInfo.createTvContentRatingSystemInfo(xmlResId, receiver.applicationInfo));
                }
            }
        }
    }

    private void switchUser(int userId) {
        synchronized (this.mLock) {
            if (this.mCurrentUserId == userId) {
                return;
            }
            UserState userState = (UserState) this.mUserStates.get(this.mCurrentUserId);
            List<SessionState> sessionStatesToRelease = new ArrayList();
            for (SessionState sessionState : userState.sessionStateMap.values()) {
                if (!(sessionState.session == null || (sessionState.isRecordingSession ^ 1) == 0)) {
                    sessionStatesToRelease.add(sessionState);
                }
            }
            for (SessionState sessionState2 : sessionStatesToRelease) {
                try {
                    sessionState2.session.release();
                } catch (RemoteException e) {
                    Slog.e(TAG, "error in release", e);
                }
                clearSessionAndNotifyClientLocked(sessionState2);
            }
            Iterator<ComponentName> it = userState.serviceStateMap.keySet().iterator();
            while (it.hasNext()) {
                ServiceState serviceState = (ServiceState) userState.serviceStateMap.get((ComponentName) it.next());
                if (serviceState != null && serviceState.sessionTokens.isEmpty()) {
                    if (serviceState.callback != null) {
                        try {
                            serviceState.service.unregisterCallback(serviceState.callback);
                        } catch (RemoteException e2) {
                            Slog.e(TAG, "error in unregisterCallback", e2);
                        }
                    }
                    this.mContext.unbindService(serviceState.connection);
                    it.remove();
                }
            }
            this.mCurrentUserId = userId;
            getOrCreateUserStateLocked(userId);
            buildTvInputListLocked(userId, null);
            buildTvContentRatingSystemListLocked(userId);
            this.mWatchLogHandler.obtainMessage(3, getContentResolverForUser(userId)).sendToTarget();
        }
    }

    private void clearSessionAndNotifyClientLocked(SessionState state) {
        if (state.client != null) {
            try {
                state.client.onSessionReleased(state.seq);
            } catch (RemoteException e) {
                Slog.e(TAG, "error in onSessionReleased", e);
            }
        }
        for (SessionState sessionState : getOrCreateUserStateLocked(state.userId).sessionStateMap.values()) {
            if (state.sessionToken == sessionState.hardwareSessionToken) {
                releaseSessionLocked(sessionState.sessionToken, 1000, state.userId);
                try {
                    sessionState.client.onSessionReleased(sessionState.seq);
                } catch (RemoteException e2) {
                    Slog.e(TAG, "error in onSessionReleased", e2);
                }
            }
        }
        removeSessionStateLocked(state.sessionToken, state.userId);
    }

    private void removeUser(int userId) {
        synchronized (this.mLock) {
            UserState userState = (UserState) this.mUserStates.get(userId);
            if (userState == null) {
                return;
            }
            for (SessionState state : userState.sessionStateMap.values()) {
                if (state.session != null) {
                    try {
                        state.session.release();
                    } catch (RemoteException e) {
                        Slog.e(TAG, "error in release", e);
                    }
                }
            }
            userState.sessionStateMap.clear();
            for (ServiceState serviceState : userState.serviceStateMap.values()) {
                if (serviceState.service != null) {
                    if (serviceState.callback != null) {
                        try {
                            serviceState.service.unregisterCallback(serviceState.callback);
                        } catch (RemoteException e2) {
                            Slog.e(TAG, "error in unregisterCallback", e2);
                        }
                    }
                    this.mContext.unbindService(serviceState.connection);
                }
            }
            userState.serviceStateMap.clear();
            userState.inputMap.clear();
            userState.packageSet.clear();
            userState.contentRatingSystemList.clear();
            userState.clientStateMap.clear();
            userState.callbackSet.clear();
            userState.mainSessionToken = null;
            this.mUserStates.remove(userId);
        }
    }

    private ContentResolver getContentResolverForUser(int userId) {
        Context context;
        UserHandle user = new UserHandle(userId);
        try {
            context = this.mContext.createPackageContextAsUser("android", 0, user);
        } catch (NameNotFoundException e) {
            Slog.e(TAG, "failed to create package context as user " + user);
            context = this.mContext;
        }
        return context.getContentResolver();
    }

    private UserState getOrCreateUserStateLocked(int userId) {
        UserState userState = (UserState) this.mUserStates.get(userId);
        if (userState != null) {
            return userState;
        }
        userState = new UserState(this.mContext, userId);
        this.mUserStates.put(userId, userState);
        return userState;
    }

    private ServiceState getServiceStateLocked(ComponentName component, int userId) {
        ServiceState serviceState = (ServiceState) getOrCreateUserStateLocked(userId).serviceStateMap.get(component);
        if (serviceState != null) {
            return serviceState;
        }
        throw new IllegalStateException("Service state not found for " + component + " (userId=" + userId + ")");
    }

    private SessionState getSessionStateLocked(IBinder sessionToken, int callingUid, int userId) {
        SessionState sessionState = (SessionState) getOrCreateUserStateLocked(userId).sessionStateMap.get(sessionToken);
        if (sessionState == null) {
            throw new SessionNotFoundException("Session state not found for token " + sessionToken);
        } else if (callingUid == 1000 || callingUid == sessionState.callingUid) {
            return sessionState;
        } else {
            throw new SecurityException("Illegal access to the session with token " + sessionToken + " from uid " + callingUid);
        }
    }

    private ITvInputSession getSessionLocked(IBinder sessionToken, int callingUid, int userId) {
        return getSessionLocked(getSessionStateLocked(sessionToken, callingUid, userId));
    }

    private ITvInputSession getSessionLocked(SessionState sessionState) {
        ITvInputSession session = sessionState.session;
        if (session != null) {
            return session;
        }
        throw new IllegalStateException("Session not yet created for token " + sessionState.sessionToken);
    }

    private int resolveCallingUserId(int callingPid, int callingUid, int requestedUserId, String methodName) {
        return ActivityManager.handleIncomingUser(callingPid, callingUid, requestedUserId, false, false, methodName, null);
    }

    private void updateServiceConnectionLocked(ComponentName component, int userId) {
        UserState userState = getOrCreateUserStateLocked(userId);
        ServiceState serviceState = (ServiceState) userState.serviceStateMap.get(component);
        if (serviceState != null) {
            if (serviceState.reconnecting) {
                if (serviceState.sessionTokens.isEmpty()) {
                    serviceState.reconnecting = false;
                } else {
                    return;
                }
            }
            int -get5 = userId == this.mCurrentUserId ? serviceState.sessionTokens.isEmpty() ? serviceState.isHardware : 1 : serviceState.sessionTokens.isEmpty() ^ 1;
            if (serviceState.service != null || r2 == 0) {
                if (!(serviceState.service == null || (r2 ^ 1) == 0)) {
                    this.mContext.unbindService(serviceState.connection);
                    userState.serviceStateMap.remove(component);
                }
            } else if (!serviceState.bound) {
                serviceState.bound = this.mContext.bindServiceAsUser(new Intent("android.media.tv.TvInputService").setComponent(component), serviceState.connection, 33554433, new UserHandle(userId));
            }
        }
    }

    private void abortPendingCreateSessionRequestsLocked(ServiceState serviceState, String inputId, int userId) {
        UserState userState = getOrCreateUserStateLocked(userId);
        List<SessionState> sessionsToAbort = new ArrayList();
        for (IBinder sessionToken : serviceState.sessionTokens) {
            SessionState sessionState = (SessionState) userState.sessionStateMap.get(sessionToken);
            if (sessionState.session == null && (inputId == null || sessionState.inputId.equals(inputId))) {
                sessionsToAbort.add(sessionState);
            }
        }
        for (SessionState sessionState2 : sessionsToAbort) {
            removeSessionStateLocked(sessionState2.sessionToken, sessionState2.userId);
            sendSessionTokenToClientLocked(sessionState2.client, sessionState2.inputId, null, null, sessionState2.seq);
        }
        updateServiceConnectionLocked(serviceState.component, userId);
    }

    private void createSessionInternalLocked(ITvInputService service, IBinder sessionToken, int userId) {
        SessionState sessionState = (SessionState) getOrCreateUserStateLocked(userId).sessionStateMap.get(sessionToken);
        InputChannel[] channels = InputChannel.openInputChannelPair(sessionToken.toString());
        ITvInputSessionCallback callback = new SessionCallback(sessionState, channels);
        try {
            if (sessionState.isRecordingSession) {
                service.createRecordingSession(callback, sessionState.inputId);
            } else {
                service.createSession(channels[1], callback, sessionState.inputId);
            }
        } catch (RemoteException e) {
            Slog.e(TAG, "error in createSession", e);
            removeSessionStateLocked(sessionToken, userId);
            sendSessionTokenToClientLocked(sessionState.client, sessionState.inputId, null, null, sessionState.seq);
        }
        channels[1].dispose();
    }

    private void sendSessionTokenToClientLocked(ITvInputClient client, String inputId, IBinder sessionToken, InputChannel channel, int seq) {
        try {
            client.onSessionCreated(inputId, sessionToken, channel, seq);
        } catch (RemoteException e) {
            Slog.e(TAG, "error in onSessionCreated", e);
        }
    }

    private void releaseSessionLocked(IBinder sessionToken, int callingUid, int userId) {
        try {
            SessionState sessionState = getSessionStateLocked(sessionToken, callingUid, userId);
            if (sessionState.session != null) {
                if (sessionToken == getOrCreateUserStateLocked(userId).mainSessionToken) {
                    setMainLocked(sessionToken, false, callingUid, userId);
                }
                sessionState.session.release();
            }
            if (sessionState != null) {
                sessionState.session = null;
            }
        } catch (Exception e) {
            Slog.e(TAG, "error in releaseSession", e);
            if (null != null) {
                null.session = null;
            }
        } catch (Throwable th) {
            if (null != null) {
                null.session = null;
            }
        }
        removeSessionStateLocked(sessionToken, userId);
    }

    private void removeSessionStateLocked(IBinder sessionToken, int userId) {
        UserState userState = getOrCreateUserStateLocked(userId);
        if (sessionToken == userState.mainSessionToken) {
            userState.mainSessionToken = null;
        }
        SessionState sessionState = (SessionState) userState.sessionStateMap.remove(sessionToken);
        if (sessionState != null) {
            ClientState clientState = (ClientState) userState.clientStateMap.get(sessionState.client.asBinder());
            if (clientState != null) {
                clientState.sessionTokens.remove(sessionToken);
                if (clientState.isEmpty()) {
                    userState.clientStateMap.remove(sessionState.client.asBinder());
                }
            }
            ServiceState serviceState = (ServiceState) userState.serviceStateMap.get(sessionState.componentName);
            if (serviceState != null) {
                serviceState.sessionTokens.remove(sessionToken);
            }
            updateServiceConnectionLocked(sessionState.componentName, userId);
            SomeArgs args = SomeArgs.obtain();
            args.arg1 = sessionToken;
            args.arg2 = Long.valueOf(System.currentTimeMillis());
            this.mWatchLogHandler.obtainMessage(2, args).sendToTarget();
        }
    }

    private void setMainLocked(IBinder sessionToken, boolean isMain, int callingUid, int userId) {
        try {
            SessionState sessionState = getSessionStateLocked(sessionToken, callingUid, userId);
            if (sessionState.hardwareSessionToken != null) {
                sessionState = getSessionStateLocked(sessionState.hardwareSessionToken, 1000, userId);
            }
            if (getServiceStateLocked(sessionState.componentName, userId).isHardware) {
                getSessionLocked(sessionState).setMain(isMain);
            }
        } catch (Exception e) {
            Slog.e(TAG, "error in setMain", e);
        }
    }

    private void notifyInputAddedLocked(UserState userState, String inputId) {
        for (ITvInputManagerCallback callback : userState.callbackSet) {
            try {
                callback.onInputAdded(inputId);
            } catch (RemoteException e) {
                Slog.e(TAG, "failed to report added input to callback", e);
            }
        }
    }

    private void notifyInputRemovedLocked(UserState userState, String inputId) {
        for (ITvInputManagerCallback callback : userState.callbackSet) {
            try {
                callback.onInputRemoved(inputId);
            } catch (RemoteException e) {
                Slog.e(TAG, "failed to report removed input to callback", e);
            }
        }
    }

    private void notifyInputUpdatedLocked(UserState userState, String inputId) {
        for (ITvInputManagerCallback callback : userState.callbackSet) {
            try {
                callback.onInputUpdated(inputId);
            } catch (RemoteException e) {
                Slog.e(TAG, "failed to report updated input to callback", e);
            }
        }
    }

    private void notifyInputStateChangedLocked(UserState userState, String inputId, int state, ITvInputManagerCallback targetCallback) {
        if (targetCallback == null) {
            for (ITvInputManagerCallback callback : userState.callbackSet) {
                try {
                    callback.onInputStateChanged(inputId, state);
                } catch (RemoteException e) {
                    Slog.e(TAG, "failed to report state change to callback", e);
                }
            }
            return;
        }
        try {
            targetCallback.onInputStateChanged(inputId, state);
        } catch (RemoteException e2) {
            Slog.e(TAG, "failed to report state change to callback", e2);
        }
    }

    private void updateTvInputInfoLocked(UserState userState, TvInputInfo inputInfo) {
        String inputId = inputInfo.getId();
        TvInputState inputState = (TvInputState) userState.inputMap.get(inputId);
        if (inputState == null) {
            Slog.e(TAG, "failed to set input info - unknown input id " + inputId);
            return;
        }
        inputState.info = inputInfo;
        for (ITvInputManagerCallback callback : userState.callbackSet) {
            try {
                callback.onTvInputInfoUpdated(inputInfo);
            } catch (RemoteException e) {
                Slog.e(TAG, "failed to report updated input info to callback", e);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void setStateLocked(java.lang.String r8, int r9, int r10) {
        /*
        r7 = this;
        r6 = 0;
        r3 = r7.getOrCreateUserStateLocked(r10);
        r4 = r3.inputMap;
        r0 = r4.get(r8);
        r0 = (com.android.server.tv.TvInputManagerService.TvInputState) r0;
        r4 = r3.serviceStateMap;
        r5 = r0.info;
        r5 = r5.getComponent();
        r2 = r4.get(r5);
        r2 = (com.android.server.tv.TvInputManagerService.ServiceState) r2;
        r1 = r0.state;
        r0.state = r9;
        if (r2 == 0) goto L_0x0041;
    L_0x002a:
        r4 = r2.service;
        if (r4 != 0) goto L_0x0041;
    L_0x0030:
        r4 = r2.sessionTokens;
        r4 = r4.isEmpty();
        if (r4 == 0) goto L_0x0040;
    L_0x003a:
        r4 = r2.isHardware;
        if (r4 == 0) goto L_0x0041;
    L_0x0040:
        return;
    L_0x0041:
        if (r1 == r9) goto L_0x0046;
    L_0x0043:
        r7.notifyInputStateChangedLocked(r3, r8, r9, r6);
    L_0x0046:
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.tv.TvInputManagerService.setStateLocked(java.lang.String, int, int):void");
    }
}
