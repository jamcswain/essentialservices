package com.android.server.policy;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings.Global;
import android.util.Slog;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.view.WindowManagerPolicy.WindowState;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class EssentialScreenPolicy {
    private static final boolean DEBUG = true;
    private static final String IKEY_DEFAULT_IMMERSIVE = "ESSENTIAL_DEFAULT_IMMERSIVE";
    private static final int IMODE_APP = 0;
    private static final int IMODE_DEFAULT = 0;
    private static final int IMODE_FIRST = 0;
    private static final int IMODE_FULL_IMMERSIVE = 3;
    private static final int IMODE_LAST = 3;
    private static final int IMODE_NO_NAVBAR = 2;
    private static final int IMODE_NO_STATUSBAR = 1;
    private static final String LKEY_DEFAULT_LETTERBOX = "ESSENTIAL_DEFAULT_LETTERBOX";
    private static final String LKEY_LAYOUT_FULLSCREEN_BL = "ESSENTIAL_LAYOUT_FULLSCREEN_BL";
    private static final String LKEY_PORTRAIT_BYPASS_WL = "ESSENTIAL_PORTRAIT_BYPASS_WL";
    private static final String LKEY_TOTAL_BYPASS_WL = "ESSENTIAL_TOTAL_BYPASS_WL";
    private static final int LMODE_BYPASS_ALL = 2;
    private static final int LMODE_BYPASS_PORTRAIT = 1;
    private static final int LMODE_DEFAULT = 0;
    private static final int LMODE_FIRST = 0;
    private static final int LMODE_LAST = 2;
    private static final int LMODE_NONE = 0;
    private static final String[] NULL = new String[0];
    private static final String RESOUCES_PKG = "com.essential.resources";
    private static final String TAG = EssentialScreenPolicy.class.getSimpleName();
    private static final String WILDCARD_ALL = "*";
    private static int sCameraNotchInset;
    private static Context sContext;
    private static int sDefaultImmersive = 0;
    private static int sDefaultLetterbox = 0;
    private static Set<String> sLayoutFullscreenBlSet;
    private static String sLayoutFullscreenBlString;
    private static Set<String> sPortraitBypassWlSet;
    private static String sPortraitBypassWlString;
    private static int sStatusBarHeight;
    private static Set<String> sTotalBypassWlSet;
    private static String sTotalBypassWlString;

    public static void init(Context context, Handler handler, final WindowManagerFuncs wm) {
        if (sContext == null) {
            sContext = context;
            sStatusBarHeight = context.getResources().getDimensionPixelSize(17105261);
            sCameraNotchInset = context.getResources().getDimensionPixelSize(17105027);
            reloadFromSetting(context);
            ContentResolver resolver = context.getContentResolver();
            resolver.registerContentObserver(Global.getUriFor(LKEY_TOTAL_BYPASS_WL), false, new ContentObserver(handler) {
                public void onChange(boolean selfChange) {
                    synchronized (wm.getWindowManagerLock()) {
                        EssentialScreenPolicy.reloadTotalBypassWl(EssentialScreenPolicy.sContext);
                    }
                }
            }, -1);
            resolver.registerContentObserver(Global.getUriFor(LKEY_PORTRAIT_BYPASS_WL), false, new ContentObserver(handler) {
                public void onChange(boolean selfChange) {
                    synchronized (wm.getWindowManagerLock()) {
                        EssentialScreenPolicy.reloadPortraitBypassWl(EssentialScreenPolicy.sContext);
                    }
                }
            }, -1);
            resolver.registerContentObserver(Global.getUriFor(LKEY_LAYOUT_FULLSCREEN_BL), false, new ContentObserver(handler) {
                public void onChange(boolean selfChange) {
                    synchronized (wm.getWindowManagerLock()) {
                        EssentialScreenPolicy.reloadLayoutFullscreenBl(EssentialScreenPolicy.sContext);
                    }
                }
            }, -1);
            resolver.registerContentObserver(Global.getUriFor(LKEY_DEFAULT_LETTERBOX), false, new ContentObserver(handler) {
                public void onChange(boolean selfChange) {
                    synchronized (wm.getWindowManagerLock()) {
                        EssentialScreenPolicy.reloadDefaultLetterboxMode(EssentialScreenPolicy.sContext);
                    }
                }
            }, -1);
            resolver.registerContentObserver(Global.getUriFor(IKEY_DEFAULT_IMMERSIVE), false, new ContentObserver(handler) {
                public void onChange(boolean selfChange) {
                    synchronized (wm.getWindowManagerLock()) {
                        EssentialScreenPolicy.reloadDefaultImmersiveMode(EssentialScreenPolicy.sContext);
                    }
                }
            }, -1);
            BroadcastReceiver resourceObserver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    Uri data = intent.getData();
                    if (data != null && EssentialScreenPolicy.RESOUCES_PKG.equals(data.getSchemeSpecificPart())) {
                        synchronized (wm.getWindowManagerLock()) {
                            EssentialScreenPolicy.reloadFromSetting(EssentialScreenPolicy.sContext);
                        }
                    }
                }
            };
            IntentFilter filter = new IntentFilter("android.intent.action.PACKAGE_REPLACED");
            filter.addDataScheme("package");
            context.registerReceiver(resourceObserver, filter);
        }
    }

    public static Rect getDisplayFrameInsets(LayoutParams attrs, int sysUiFl, int fl, int rotation) {
        Rect insetRect = new Rect();
        if (!bypass(attrs, rotation)) {
            switch (rotation) {
                case 0:
                    if (insetTop(attrs, sysUiFl, fl)) {
                        insetRect.top = sStatusBarHeight;
                        break;
                    }
                    break;
                case 1:
                    insetRect.left = sCameraNotchInset;
                    break;
                case 2:
                    if (insetBottom(attrs, sysUiFl, fl)) {
                        insetRect.bottom = sCameraNotchInset;
                        break;
                    }
                    break;
                case 3:
                    insetRect.right = sCameraNotchInset;
                    break;
            }
        }
        return insetRect;
    }

    public static Rect getPipInsets(Rect out, Point edgeInsets, int rotation) {
        out.left = edgeInsets.x;
        out.top = edgeInsets.y;
        out.right = edgeInsets.x;
        out.bottom = edgeInsets.y;
        switch (rotation) {
            case 1:
                out.left += sCameraNotchInset;
                break;
            case 3:
                out.right += sCameraNotchInset;
                break;
        }
        return out;
    }

    private static boolean bypass(LayoutParams attrs, int rotation) {
        if (sDefaultLetterbox == 2 || contains(sTotalBypassWlSet, attrs)) {
            return true;
        }
        if (rotation != 0 && rotation != 2) {
            return false;
        }
        if (sDefaultLetterbox != 1) {
            return contains(sPortraitBypassWlSet, attrs);
        }
        return true;
    }

    private static boolean insetTop(LayoutParams attrs, int sysUiFl, int fl) {
        if ((sysUiFl & 4) != 0 || (fl & 1024) != 0) {
            return true;
        }
        if ((sysUiFl & 1024) != 0) {
            return contains(sLayoutFullscreenBlSet, attrs);
        }
        return false;
    }

    private static boolean insetBottom(LayoutParams attrs, int sysUiFl, int fl) {
        if ((sysUiFl & 2) != 0) {
            return true;
        }
        if ((sysUiFl & 512) != 0) {
            return contains(sLayoutFullscreenBlSet, attrs);
        }
        return false;
    }

    public static int getSystemUiVisibility(WindowState win, LayoutParams attrs) {
        if (attrs == null) {
            attrs = win.getAttrs();
        }
        int vis = win != null ? win.getSystemUiVisibility() : attrs.systemUiVisibility;
        if (hideStatusbar()) {
            vis = (vis | 5124) & -1073742081;
        }
        if (hideNavbar()) {
            return (vis | 4610) & 2147483391;
        }
        return vis;
    }

    public static int getWindowFlags(WindowState win, LayoutParams attrs) {
        if (attrs == null) {
            attrs = win.getAttrs();
        }
        int flags = attrs.flags;
        if (hideStatusbar()) {
            flags = (flags | 1024) & -67110913;
        }
        if (hideNavbar()) {
            return flags & -134217729;
        }
        return flags;
    }

    public static int adjustClearableFlags(WindowState win, int clearableFlags) {
        if (win != null) {
            LayoutParams attrs = win.getAttrs();
        }
        if (hideStatusbar()) {
            return clearableFlags & -5;
        }
        return clearableFlags;
    }

    private static boolean hideStatusbar() {
        if (sDefaultImmersive == 1 || sDefaultImmersive == 3) {
            return true;
        }
        return false;
    }

    private static boolean hideNavbar() {
        if (sDefaultImmersive == 2 || sDefaultImmersive == 3) {
            return true;
        }
        return false;
    }

    private static boolean contains(Set<String> filterSet, LayoutParams attrs) {
        if (filterSet == null) {
            return false;
        }
        if (filterSet.contains(WILDCARD_ALL)) {
            return true;
        }
        if (attrs == null || attrs.packageName == null) {
            return false;
        }
        return filterSet.contains(attrs.packageName);
    }

    private static void reloadFromSetting(Context context) {
        Slog.d(TAG, "reloadFromSetting(context)");
        reloadTotalBypassWl(context);
        reloadPortraitBypassWl(context);
        reloadLayoutFullscreenBl(context);
        reloadDefaultImmersiveMode(context);
    }

    private static void reloadTotalBypassWl(Context context) {
        Slog.d(TAG, "reloadTotalBypassWl(context)");
        String value = null;
        try {
            value = Global.getString(context.getContentResolver(), LKEY_TOTAL_BYPASS_WL);
        } catch (Throwable t) {
            Slog.w(TAG, "Error loading total bypass whitelist, value=" + value, t);
        }
        if (value == null) {
            String[] array = getStrings(context, "essential_bypass_whitelist");
            sTotalBypassWlSet = new HashSet(Arrays.asList(array));
            sTotalBypassWlString = null;
            Slog.d(TAG, "loading total bypass from res: " + Arrays.toString(array));
        } else if (!value.equals(sTotalBypassWlString)) {
            sTotalBypassWlString = value;
            sTotalBypassWlSet = parse(value);
        }
    }

    private static void reloadPortraitBypassWl(Context context) {
        Slog.d(TAG, "reloadPortraitBypassWl(context)");
        String value = null;
        try {
            value = Global.getString(context.getContentResolver(), LKEY_PORTRAIT_BYPASS_WL);
        } catch (Throwable t) {
            Slog.w(TAG, "Error loading bypass portrait whitelist, value=" + value, t);
        }
        if (value == null) {
            String[] array = getStrings(context, "essential_bypass_portrait_whitelist");
            sPortraitBypassWlSet = new HashSet(Arrays.asList(array));
            sPortraitBypassWlString = null;
            Slog.d(TAG, "loading portriat bypass from res: " + Arrays.toString(array));
        } else if (!value.equals(sPortraitBypassWlString)) {
            sPortraitBypassWlString = value;
            sPortraitBypassWlSet = parse(value);
        }
    }

    private static void reloadLayoutFullscreenBl(Context context) {
        Slog.d(TAG, "reloadLayoutFullscreenBl(context)");
        String value = null;
        try {
            value = Global.getString(context.getContentResolver(), LKEY_LAYOUT_FULLSCREEN_BL);
        } catch (Throwable t) {
            Slog.w(TAG, "Error loading layout fullscreen blacklist, value=" + value, t);
        }
        if (value == null) {
            String[] array = getStrings(context, "essential_layout_blacklist");
            sLayoutFullscreenBlSet = new HashSet(Arrays.asList(array));
            sLayoutFullscreenBlString = null;
            Slog.d(TAG, "loading LAYOUT_FULLSCREEN blacklist from res: " + Arrays.toString(array));
        } else if (!value.equals(sLayoutFullscreenBlString)) {
            sLayoutFullscreenBlString = value;
            sLayoutFullscreenBlSet = parse(value);
        }
    }

    private static void reloadDefaultLetterboxMode(Context context) {
        try {
            String value = Global.getString(context.getContentResolver(), LKEY_DEFAULT_LETTERBOX);
            int mode = value != null ? Integer.parseInt(value) : 0;
            if (mode > 2 || mode < 0) {
                Slog.w(TAG, "Error loading default letterbox mode, invalid value =" + mode + "; so setting to " + 0);
                mode = 0;
            }
            sDefaultLetterbox = mode;
        } catch (Throwable t) {
            Slog.w(TAG, "Error loading default letterbox mode, value=" + null, t);
        }
    }

    private static void reloadDefaultImmersiveMode(Context context) {
        try {
            String value = Global.getString(context.getContentResolver(), IKEY_DEFAULT_IMMERSIVE);
            int mode = value != null ? Integer.parseInt(value) : 0;
            if (mode > 3 || mode < 0) {
                Slog.w(TAG, "Error loading default immersive mode, invalid value =" + mode + "; so setting to " + 0);
                mode = 0;
            }
            sDefaultImmersive = mode;
        } catch (Throwable t) {
            Slog.w(TAG, "Error loading default immersive mode, value=" + null, t);
        }
    }

    private static String[] getStrings(Context context, String resName) {
        try {
            Resources res = context.getPackageManager().getResourcesForApplication(RESOUCES_PKG);
            int id = res.getIdentifier(resName, "array", RESOUCES_PKG);
            return id == 0 ? NULL : res.getStringArray(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new String[0];
        }
    }

    private static Set<String> parse(String commaSeparatedValues) {
        if (commaSeparatedValues != null) {
            if (commaSeparatedValues.equals(WILDCARD_ALL)) {
                return Collections.singleton(WILDCARD_ALL);
            }
            String[] pkgs = commaSeparatedValues.split(",");
            if (pkgs.length > 0) {
                Set<String> parsedSet = new HashSet(pkgs.length);
                for (String pkg : pkgs) {
                    String pkg2 = pkg2.trim();
                    if (!pkg2.isEmpty()) {
                        parsedSet.add(pkg2);
                    }
                }
                return parsedSet;
            }
        }
        return Collections.emptySet();
    }
}
