package com.android.server.policy;

import android.app.ActivityManager;
import android.app.ActivityManager.StackId;
import android.app.ActivityManagerInternal;
import android.app.ActivityManagerInternal.SleepToken;
import android.app.ActivityThread;
import android.app.AppOpsManager;
import android.app.IUiModeManager;
import android.app.IUiModeManager.Stub;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.UiModeManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.database.ContentObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.display.DisplayManager;
import android.hardware.hdmi.HdmiControlManager;
import android.hardware.hdmi.HdmiPlaybackClient;
import android.hardware.hdmi.HdmiPlaybackClient.OneTouchPlayCallback;
import android.hardware.input.InputManager;
import android.hardware.input.InputManagerInternal;
import android.media.AudioAttributes;
import android.media.AudioAttributes.Builder;
import android.media.AudioSystem;
import android.media.IAudioService;
import android.media.session.MediaSessionLegacyHelper;
import android.net.dhcp.DhcpPacket;
import android.os.Binder;
import android.os.Bundle;
import android.os.FactoryTest;
import android.os.Handler;
import android.os.IBinder;
import android.os.IDeviceIdleController;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.PowerManagerInternal;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.UEventObserver;
import android.os.UEventObserver.UEvent;
import android.os.UserHandle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.service.dreams.DreamManagerInternal;
import android.service.dreams.IDreamManager;
import android.service.vr.IPersistentVrStateCallbacks;
import android.telecom.TelecomManager;
import android.util.EventLog;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.MutableBoolean;
import android.util.Slog;
import android.util.SparseArray;
import android.view.Display;
import android.view.IApplicationToken;
import android.view.IWindowManager;
import android.view.InputChannel;
import android.view.InputEventReceiver;
import android.view.KeyCharacterMap;
import android.view.KeyCharacterMap.FallbackAction;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.WindowManagerInternal;
import android.view.WindowManagerInternal.AppTransitionListener;
import android.view.WindowManagerPolicy;
import android.view.WindowManagerPolicy.InputConsumer;
import android.view.WindowManagerPolicy.OnKeyguardExitResult;
import android.view.WindowManagerPolicy.ScreenOffListener;
import android.view.WindowManagerPolicy.ScreenOnListener;
import android.view.WindowManagerPolicy.WindowManagerFuncs;
import android.view.WindowManagerPolicy.WindowState;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.autofill.AutofillManagerInternal;
import android.view.inputmethod.InputMethodManagerInternal;
import com.android.internal.R;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.logging.MetricsLogger;
import com.android.internal.policy.IKeyguardDismissCallback;
import com.android.internal.policy.IShortcutService;
import com.android.internal.policy.PhoneWindow;
import com.android.internal.statusbar.IStatusBarService;
import com.android.internal.util.ScreenShapeHelper;
import com.android.internal.widget.PointerLocationView;
import com.android.server.GestureLauncherService;
import com.android.server.LocalServices;
import com.android.server.SystemServiceManager;
import com.android.server.audio.AudioService;
import com.android.server.policy.keyguard.KeyguardServiceDelegate;
import com.android.server.policy.keyguard.KeyguardServiceDelegate.DrawnListener;
import com.android.server.policy.keyguard.KeyguardStateMonitor.StateCallback;
import com.android.server.statusbar.StatusBarManagerInternal;
import com.android.server.usb.descriptors.UsbTerminalTypes;
import com.android.server.vr.VrManagerInternal;
import com.android.server.wm.AppTransition;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class PhoneWindowManager implements WindowManagerPolicy {
    static final boolean ALTERNATE_CAR_MODE_NAV_SIZE = false;
    private static final int BRIGHTNESS_STEPS = 10;
    private static final long BUGREPORT_TV_GESTURE_TIMEOUT_MILLIS = 1000;
    static final boolean DEBUG = false;
    static final boolean DEBUG_INPUT = false;
    static final boolean DEBUG_KEYGUARD = false;
    static final boolean DEBUG_LAYOUT = false;
    static final boolean DEBUG_SPLASH_SCREEN = false;
    static final boolean DEBUG_WAKEUP = false;
    static final int DOUBLE_TAP_HOME_NOTHING = 0;
    static final int DOUBLE_TAP_HOME_RECENT_SYSTEM_UI = 1;
    static final boolean ENABLE_DESK_DOCK_HOME_CAPTURE = false;
    static final boolean ENABLE_VR_HEADSET_HOME_CAPTURE = true;
    private static final float KEYGUARD_SCREENSHOT_CHORD_DELAY_MULTIPLIER = 2.5f;
    static final int LAST_LONG_PRESS_HOME_BEHAVIOR = 2;
    static final int LONG_PRESS_BACK_GO_TO_VOICE_ASSIST = 1;
    static final int LONG_PRESS_BACK_NOTHING = 0;
    static final int LONG_PRESS_HOME_ALL_APPS = 1;
    static final int LONG_PRESS_HOME_ASSIST = 2;
    static final int LONG_PRESS_HOME_NOTHING = 0;
    static final int LONG_PRESS_POWER_GLOBAL_ACTIONS = 1;
    static final int LONG_PRESS_POWER_NOTHING = 0;
    static final int LONG_PRESS_POWER_SHUT_OFF = 2;
    static final int LONG_PRESS_POWER_SHUT_OFF_NO_CONFIRM = 3;
    private static final int MSG_ACCESSIBILITY_SHORTCUT = 21;
    private static final int MSG_ACCESSIBILITY_TV = 23;
    private static final int MSG_BACK_DELAYED_PRESS = 20;
    private static final int MSG_BACK_LONG_PRESS = 18;
    private static final int MSG_BUGREPORT_TV = 22;
    private static final int MSG_DISABLE_POINTER_LOCATION = 2;
    private static final int MSG_DISPATCH_BACK_KEY_TO_AUTOFILL = 24;
    private static final int MSG_DISPATCH_MEDIA_KEY_REPEAT_WITH_WAKE_LOCK = 4;
    private static final int MSG_DISPATCH_MEDIA_KEY_WITH_WAKE_LOCK = 3;
    private static final int MSG_DISPATCH_SHOW_GLOBAL_ACTIONS = 10;
    private static final int MSG_DISPATCH_SHOW_RECENTS = 9;
    private static final int MSG_DISPOSE_INPUT_CONSUMER = 19;
    private static final int MSG_ENABLE_POINTER_LOCATION = 1;
    private static final int MSG_HANDLE_ALL_APPS = 26;
    private static final int MSG_HIDE_BOOT_MESSAGE = 11;
    private static final int MSG_KEYGUARD_DRAWN_COMPLETE = 5;
    private static final int MSG_KEYGUARD_DRAWN_TIMEOUT = 6;
    private static final int MSG_LAUNCH_VOICE_ASSIST_WITH_WAKE_LOCK = 12;
    private static final int MSG_POWER_DELAYED_PRESS = 13;
    private static final int MSG_POWER_LONG_PRESS = 14;
    private static final int MSG_REQUEST_TRANSIENT_BARS = 16;
    private static final int MSG_REQUEST_TRANSIENT_BARS_ARG_NAVIGATION = 1;
    private static final int MSG_REQUEST_TRANSIENT_BARS_ARG_STATUS = 0;
    private static final int MSG_SHOW_PICTURE_IN_PICTURE_MENU = 17;
    private static final int MSG_SYSTEM_KEY_PRESS = 25;
    private static final int MSG_UPDATE_DREAMING_SLEEP_TOKEN = 15;
    private static final int MSG_WINDOW_MANAGER_DRAWN_COMPLETE = 7;
    static final int MULTI_PRESS_POWER_BRIGHTNESS_BOOST = 2;
    static final int MULTI_PRESS_POWER_NOTHING = 0;
    static final int MULTI_PRESS_POWER_THEATER_MODE = 1;
    static final int NAV_BAR_OPAQUE_WHEN_FREEFORM_OR_DOCKED = 0;
    static final int NAV_BAR_TRANSLUCENT_WHEN_FREEFORM_OPAQUE_OTHERWISE = 1;
    private static final long PANIC_GESTURE_EXPIRATION = 30000;
    static final int PANIC_PRESS_BACK_COUNT = 4;
    static final int PANIC_PRESS_BACK_HOME = 1;
    static final int PANIC_PRESS_BACK_NOTHING = 0;
    static final int PENDING_KEY_NULL = -1;
    static final boolean PRINT_ANIM = false;
    private static final long SCREENSHOT_CHORD_DEBOUNCE_DELAY_MILLIS = 150;
    static final int SHORT_PRESS_POWER_CLOSE_IME_OR_GO_HOME = 5;
    static final int SHORT_PRESS_POWER_GO_HOME = 4;
    static final int SHORT_PRESS_POWER_GO_TO_SLEEP = 1;
    static final int SHORT_PRESS_POWER_NOTHING = 0;
    static final int SHORT_PRESS_POWER_REALLY_GO_TO_SLEEP = 2;
    static final int SHORT_PRESS_POWER_REALLY_GO_TO_SLEEP_AND_GO_HOME = 3;
    static final int SHORT_PRESS_SLEEP_GO_TO_SLEEP = 0;
    static final int SHORT_PRESS_SLEEP_GO_TO_SLEEP_AND_GO_HOME = 1;
    static final int SHORT_PRESS_WINDOW_NOTHING = 0;
    static final int SHORT_PRESS_WINDOW_PICTURE_IN_PICTURE = 1;
    static final boolean SHOW_SPLASH_SCREENS = true;
    public static final String SYSTEM_DIALOG_REASON_ASSIST = "assist";
    public static final String SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions";
    public static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";
    public static final String SYSTEM_DIALOG_REASON_KEY = "reason";
    public static final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
    public static final String SYSTEM_DIALOG_REASON_SCREENSHOT = "screenshot";
    static final int SYSTEM_UI_CHANGING_LAYOUT = -1073709042;
    private static final String SYSUI_PACKAGE = "com.android.systemui";
    private static final String SYSUI_SCREENSHOT_ERROR_RECEIVER = "com.android.systemui.screenshot.ScreenshotServiceErrorReceiver";
    private static final String SYSUI_SCREENSHOT_SERVICE = "com.android.systemui.screenshot.TakeScreenshotService";
    static final String TAG = "WindowManager";
    public static final int TOAST_WINDOW_TIMEOUT = 3500;
    private static final AudioAttributes VIBRATION_ATTRIBUTES = new Builder().setContentType(4).setUsage(13).build();
    static final int WAITING_FOR_DRAWN_TIMEOUT = 1000;
    private static final int[] WINDOW_TYPES_WHERE_HOME_DOESNT_WORK = new int[]{2003, 2010};
    static final boolean localLOGV = false;
    static final Rect mTmpContentFrame = new Rect();
    static final Rect mTmpDecorFrame = new Rect();
    static final Rect mTmpDisplayFrame = new Rect();
    static final Rect mTmpNavigationFrame = new Rect();
    static final Rect mTmpOutsetFrame = new Rect();
    static final Rect mTmpOverscanFrame = new Rect();
    static final Rect mTmpParentFrame = new Rect();
    private static final Rect mTmpRect = new Rect();
    static final Rect mTmpStableFrame = new Rect();
    static final Rect mTmpVisibleFrame = new Rect();
    static SparseArray<String> sApplicationLaunchKeyCategories = new SparseArray();
    private boolean mA11yShortcutChordVolumeUpKeyConsumed;
    private long mA11yShortcutChordVolumeUpKeyTime;
    private boolean mA11yShortcutChordVolumeUpKeyTriggered;
    boolean mAccelerometerDefault;
    AccessibilityManager mAccessibilityManager;
    private AccessibilityShortcutController mAccessibilityShortcutController;
    private boolean mAccessibilityTvKey1Pressed;
    private boolean mAccessibilityTvKey2Pressed;
    private boolean mAccessibilityTvScheduled;
    private final Runnable mAcquireSleepTokenRunnable = new -$Lambda$pV_TcBBXJOcgD8CpVRVZuDc_ff8((byte) 1, this);
    ActivityManagerInternal mActivityManagerInternal;
    int mAllowAllRotations = -1;
    boolean mAllowLockscreenWhenOn;
    private boolean mAllowTheaterModeWakeFromCameraLens;
    private boolean mAllowTheaterModeWakeFromKey;
    private boolean mAllowTheaterModeWakeFromLidSwitch;
    private boolean mAllowTheaterModeWakeFromMotion;
    private boolean mAllowTheaterModeWakeFromMotionWhenNotDreaming;
    private boolean mAllowTheaterModeWakeFromPowerKey;
    private boolean mAllowTheaterModeWakeFromWakeGesture;
    AppOpsManager mAppOpsManager;
    boolean mAssistKeyLongPressed;
    AutofillManagerInternal mAutofillManagerInternal;
    volatile boolean mAwake;
    volatile boolean mBackKeyHandled;
    volatile int mBackKeyPressCounter;
    volatile boolean mBeganFromNonInteractive;
    boolean mBootMessageNeedsHiding;
    ProgressDialog mBootMsgDialog = null;
    WakeLock mBroadcastWakeLock;
    private boolean mBugreportTvKey1Pressed;
    private boolean mBugreportTvKey2Pressed;
    private boolean mBugreportTvScheduled;
    BurnInProtectionHelper mBurnInProtectionHelper;
    long[] mCalendarDateVibePattern;
    volatile boolean mCameraGestureTriggeredDuringGoingToSleep;
    int mCameraLensCoverState = -1;
    int mCameraNotchInset;
    boolean mCarDockEnablesAccelerometer;
    Intent mCarDockIntent;
    int mCarDockRotation;
    private final Runnable mClearHideNavigationFlag = new Runnable() {
        public void run() {
            synchronized (PhoneWindowManager.this.mWindowManagerFuncs.getWindowManagerLock()) {
                PhoneWindowManager phoneWindowManager = PhoneWindowManager.this;
                phoneWindowManager.mForceClearedSystemUiFlags &= -3;
            }
            PhoneWindowManager.this.mWindowManagerFuncs.reevaluateStatusBarVisibility();
        }
    };
    boolean mConsumeSearchKeyUp;
    int mContentBottom;
    int mContentLeft;
    int mContentRight;
    int mContentTop;
    Context mContext;
    int mCurBottom;
    int mCurLeft;
    int mCurRight;
    int mCurTop;
    int mCurrentAppOrientation = -1;
    private int mCurrentUserId;
    int mDemoHdmiRotation;
    boolean mDemoHdmiRotationLock;
    int mDemoRotation;
    boolean mDemoRotationLock;
    boolean mDeskDockEnablesAccelerometer;
    Intent mDeskDockIntent;
    int mDeskDockRotation;
    private volatile boolean mDismissImeOnBackKeyPressed;
    Display mDisplay;
    private int mDisplayRotation;
    int mDockBottom;
    int mDockLayer;
    int mDockLeft;
    int mDockMode = 0;
    BroadcastReceiver mDockReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.DOCK_EVENT".equals(intent.getAction())) {
                PhoneWindowManager.this.mDockMode = intent.getIntExtra("android.intent.extra.DOCK_STATE", 0);
            } else {
                try {
                    IUiModeManager uiModeService = Stub.asInterface(ServiceManager.getService("uimode"));
                    PhoneWindowManager.this.mUiMode = uiModeService.getCurrentModeType();
                } catch (RemoteException e) {
                }
            }
            PhoneWindowManager.this.updateRotation(true);
            synchronized (PhoneWindowManager.this.mLock) {
                PhoneWindowManager.this.updateOrientationListenerLp();
            }
        }
    };
    int mDockRight;
    int mDockTop;
    final Rect mDockedStackBounds = new Rect();
    int mDoublePressOnPowerBehavior;
    private int mDoubleTapOnHomeBehavior;
    DreamManagerInternal mDreamManagerInternal;
    BroadcastReceiver mDreamReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.DREAMING_STARTED".equals(intent.getAction())) {
                if (PhoneWindowManager.this.mKeyguardDelegate != null) {
                    PhoneWindowManager.this.mKeyguardDelegate.onDreamingStarted();
                }
            } else if ("android.intent.action.DREAMING_STOPPED".equals(intent.getAction()) && PhoneWindowManager.this.mKeyguardDelegate != null) {
                PhoneWindowManager.this.mKeyguardDelegate.onDreamingStopped();
            }
        }
    };
    boolean mDreamingLockscreen;
    SleepToken mDreamingSleepToken;
    boolean mDreamingSleepTokenNeeded;
    final Rect mEmptyRect = new Rect();
    private boolean mEnableCarDockHomeCapture = true;
    boolean mEnableShiftMenuBugReports = false;
    volatile boolean mEndCallKeyHandled;
    private final Runnable mEndCallLongPress = new Runnable() {
        public void run() {
            PhoneWindowManager.this.mEndCallKeyHandled = true;
            PhoneWindowManager.this.performHapticFeedbackLw(null, 0, false);
            PhoneWindowManager.this.showGlobalActionsInternal();
        }
    };
    int mEndcallBehavior;
    private final SparseArray<FallbackAction> mFallbackActions = new SparseArray();
    IApplicationToken mFocusedApp;
    WindowState mFocusedWindow;
    int mForceClearedSystemUiFlags = 0;
    private boolean mForceDefaultOrientation = false;
    boolean mForceShowSystemBars;
    boolean mForceStatusBar;
    boolean mForceStatusBarFromKeyguard;
    private boolean mForceStatusBarTransparent;
    boolean mForcingShowNavBar;
    int mForcingShowNavBarLayer;
    GlobalActions mGlobalActions;
    private GlobalKeyManager mGlobalKeyManager;
    private boolean mGoToSleepOnButtonPressTheaterMode;
    volatile boolean mGoingToSleep;
    private UEventObserver mHDMIObserver = new UEventObserver() {
        public void onUEvent(UEvent event) {
            PhoneWindowManager.this.setHdmiPlugged("1".equals(event.get("SWITCH_STATE")));
        }
    };
    private boolean mHandleVolumeKeysInWM;
    Handler mHandler;
    private boolean mHasFeatureLeanback;
    private boolean mHasFeatureWatch;
    boolean mHasNavigationBar = false;
    boolean mHasSoftInput = false;
    boolean mHaveBuiltInKeyboard;
    boolean mHavePendingMediaKeyRepeatWithWakeLock;
    HdmiControl mHdmiControl;
    boolean mHdmiPlugged;
    private final Runnable mHiddenNavPanic = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r4 = this;
            r0 = com.android.server.policy.PhoneWindowManager.this;
            r0 = r0.mWindowManagerFuncs;
            r1 = r0.getWindowManagerLock();
            monitor-enter(r1);
            r0 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x0031 }
            r0 = r0.isUserSetupComplete();	 Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x0013;
        L_0x0011:
            monitor-exit(r1);
            return;
        L_0x0013:
            r0 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x0031 }
            r2 = android.os.SystemClock.uptimeMillis();	 Catch:{ all -> 0x0031 }
            r0.mPendingPanicGestureUptime = r2;	 Catch:{ all -> 0x0031 }
            r0 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x0031 }
            r0 = r0.mLastSystemUiFlags;	 Catch:{ all -> 0x0031 }
            r0 = com.android.server.policy.PhoneWindowManager.isNavBarEmpty(r0);	 Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x002f;
        L_0x0026:
            r0 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x0031 }
            r0 = r0.mNavigationBarController;	 Catch:{ all -> 0x0031 }
            r0.showTransient();	 Catch:{ all -> 0x0031 }
        L_0x002f:
            monitor-exit(r1);
            return;
        L_0x0031:
            r0 = move-exception;
            monitor-exit(r1);
            throw r0;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.13.run():void");
        }
    };
    boolean mHomeConsumed;
    boolean mHomeDoubleTapPending;
    private final Runnable mHomeDoubleTapTimeoutRunnable = new Runnable() {
        public void run() {
            if (PhoneWindowManager.this.mHomeDoubleTapPending) {
                PhoneWindowManager.this.mHomeDoubleTapPending = false;
                PhoneWindowManager.this.handleShortPressOnHome();
            }
        }
    };
    Intent mHomeIntent;
    boolean mHomePressed;
    private ImmersiveModeConfirmation mImmersiveModeConfirmation;
    int mIncallBackBehavior;
    int mIncallPowerBehavior;
    int mInitialMetaState;
    InputConsumer mInputConsumer = null;
    InputManagerInternal mInputManagerInternal;
    InputMethodManagerInternal mInputMethodManagerInternal;
    private boolean mKeyguardBound;
    KeyguardServiceDelegate mKeyguardDelegate;
    boolean mKeyguardDrawComplete;
    final DrawnListener mKeyguardDrawnCallback = new DrawnListener() {
        public void onDrawn() {
            PhoneWindowManager.this.mHandler.sendEmptyMessage(5);
        }
    };
    private boolean mKeyguardDrawnOnce;
    volatile boolean mKeyguardOccluded;
    private boolean mKeyguardOccludedChanged;
    int mLandscapeRotation = 0;
    boolean mLanguageSwitchKeyPressed;
    final Rect mLastDockedStackBounds = new Rect();
    int mLastDockedStackSysUiFlags;
    boolean mLastFocusNeedsMenu = false;
    int mLastFullscreenStackSysUiFlags;
    WindowState mLastInputMethodTargetWindow = null;
    WindowState mLastInputMethodWindow = null;
    final Rect mLastNonDockedStackBounds = new Rect();
    private boolean mLastShowingDream;
    int mLastSystemUiFlags;
    private boolean mLastWindowSleepTokenNeeded;
    WindowState mLetterBoxBar = null;
    boolean mLidControlsScreenLock;
    boolean mLidControlsSleep;
    int mLidKeyboardAccessibility;
    int mLidNavigationAccessibility;
    int mLidOpenRotation;
    int mLidState = -1;
    private final Object mLock = new Object();
    int mLockScreenTimeout;
    boolean mLockScreenTimerActive;
    private final LogDecelerateInterpolator mLogDecelerateInterpolator = new LogDecelerateInterpolator(100, 0);
    int mLongPressOnBackBehavior;
    private int mLongPressOnHomeBehavior;
    int mLongPressOnPowerBehavior;
    long[] mLongPressVibePattern;
    int mMetaState;
    BroadcastReceiver mMultiuserReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.USER_SWITCHED".equals(intent.getAction())) {
                PhoneWindowManager.this.mSettingsObserver.onChange(false);
                synchronized (PhoneWindowManager.this.mWindowManagerFuncs.getWindowManagerLock()) {
                    PhoneWindowManager.this.mLastSystemUiFlags = 0;
                    PhoneWindowManager.this.updateSystemUiVisibilityLw();
                }
            }
        }
    };
    int mNavBarOpacityMode = 0;
    private final OnBarVisibilityChangedListener mNavBarVisibilityListener = new OnBarVisibilityChangedListener() {
        public void onBarVisibilityChanged(boolean visible) {
            PhoneWindowManager.this.mAccessibilityManager.notifyAccessibilityButtonVisibilityChanged(visible);
        }
    };
    WindowState mNavigationBar = null;
    boolean mNavigationBarCanMove = false;
    private final BarController mNavigationBarController = new BarController("NavigationBar", 134217728, 536870912, Integer.MIN_VALUE, 2, 134217728, 32768);
    int[] mNavigationBarHeightForRotationDefault = new int[4];
    int[] mNavigationBarHeightForRotationInCarMode = new int[4];
    int mNavigationBarPosition = 4;
    int[] mNavigationBarWidthForRotationDefault = new int[4];
    int[] mNavigationBarWidthForRotationInCarMode = new int[4];
    final Rect mNonDockedStackBounds = new Rect();
    MyOrientationListener mOrientationListener;
    boolean mOrientationSensorEnabled = false;
    int mOverscanBottom = 0;
    int mOverscanLeft = 0;
    int mOverscanRight = 0;
    int mOverscanScreenHeight;
    int mOverscanScreenLeft;
    int mOverscanScreenTop;
    int mOverscanScreenWidth;
    int mOverscanTop = 0;
    int mPanicPressOnBackBehavior;
    boolean mPendingCapsLockToggle;
    private boolean mPendingKeyguardOccluded;
    boolean mPendingMetaAction;
    private long mPendingPanicGestureUptime;
    volatile int mPendingWakeKey = -1;
    private volatile boolean mPersistentVrModeEnabled;
    final IPersistentVrStateCallbacks mPersistentVrModeListener = new IPersistentVrStateCallbacks.Stub() {
        public void onPersistentVrStateChanged(boolean enabled) {
            PhoneWindowManager.this.mPersistentVrModeEnabled = enabled;
        }
    };
    volatile boolean mPictureInPictureVisible;
    int mPointerLocationMode = 0;
    PointerLocationView mPointerLocationView;
    int mPortraitRotation = 0;
    volatile boolean mPowerKeyHandled;
    volatile int mPowerKeyPressCounter;
    WakeLock mPowerKeyWakeLock;
    PowerManager mPowerManager;
    PowerManagerInternal mPowerManagerInternal;
    boolean mPreloadedRecentApps;
    int mRecentAppsHeldModifiers;
    volatile boolean mRecentsVisible;
    private final Runnable mReleaseSleepTokenRunnable = new -$Lambda$pV_TcBBXJOcgD8CpVRVZuDc_ff8((byte) 2, this);
    volatile boolean mRequestedOrGoingToSleep;
    int mResettingSystemUiFlags = 0;
    int mRestrictedOverscanScreenHeight;
    int mRestrictedOverscanScreenLeft;
    int mRestrictedOverscanScreenTop;
    int mRestrictedOverscanScreenWidth;
    int mRestrictedScreenHeight;
    int mRestrictedScreenLeft;
    int mRestrictedScreenTop;
    int mRestrictedScreenWidth;
    boolean mSafeMode;
    long[] mSafeModeEnabledVibePattern;
    ScreenLockTimeout mScreenLockTimeout = new ScreenLockTimeout();
    SleepToken mScreenOffSleepToken;
    boolean mScreenOnEarly;
    boolean mScreenOnFully;
    ScreenOnListener mScreenOnListener;
    private boolean mScreenshotChordEnabled;
    private long mScreenshotChordPowerKeyTime;
    private boolean mScreenshotChordPowerKeyTriggered;
    private boolean mScreenshotChordVolumeDownKeyConsumed;
    private long mScreenshotChordVolumeDownKeyTime;
    private boolean mScreenshotChordVolumeDownKeyTriggered;
    ServiceConnection mScreenshotConnection = null;
    final Object mScreenshotLock = new Object();
    private final ScreenshotRunnable mScreenshotRunnable = new ScreenshotRunnable();
    final Runnable mScreenshotTimeout = new Runnable() {
        public void run() {
            synchronized (PhoneWindowManager.this.mScreenshotLock) {
                if (PhoneWindowManager.this.mScreenshotConnection != null) {
                    PhoneWindowManager.this.mContext.unbindService(PhoneWindowManager.this.mScreenshotConnection);
                    PhoneWindowManager.this.mScreenshotConnection = null;
                    PhoneWindowManager.this.notifyScreenshotError();
                }
            }
        }
    };
    boolean mSearchKeyShortcutPending;
    SearchManager mSearchManager;
    int mSeascapeRotation = 0;
    final Object mServiceAquireLock = new Object();
    SettingsObserver mSettingsObserver;
    int mShortPressOnPowerBehavior;
    int mShortPressOnSleepBehavior;
    int mShortPressWindowBehavior;
    private LongSparseArray<IShortcutService> mShortcutKeyServices = new LongSparseArray();
    ShortcutManager mShortcutManager;
    boolean mShowingDream;
    int mStableBottom;
    int mStableFullscreenBottom;
    int mStableFullscreenLeft;
    int mStableFullscreenRight;
    int mStableFullscreenTop;
    int mStableLeft;
    int mStableRight;
    int mStableTop;
    WindowState mStatusBar = null;
    private final StatusBarController mStatusBarController = new StatusBarController();
    int mStatusBarHeight;
    int mStatusBarHeightLandscape;
    int mStatusBarHeightPortrait;
    int mStatusBarLayer;
    StatusBarManagerInternal mStatusBarManagerInternal;
    IStatusBarService mStatusBarService;
    boolean mSupportAutoRotation;
    private boolean mSupportLongPressPowerWhenNonInteractive;
    boolean mSystemBooted;
    int mSystemBottom;
    private SystemGesturesPointerEventListener mSystemGestures;
    int mSystemLeft;
    boolean mSystemReady;
    int mSystemRight;
    int mSystemTop;
    private final MutableBoolean mTmpBoolean = new MutableBoolean(false);
    WindowState mTopDockedOpaqueOrDimmingWindowState;
    WindowState mTopDockedOpaqueWindowState;
    WindowState mTopFullscreenOpaqueOrDimmingWindowState;
    WindowState mTopFullscreenOpaqueWindowState;
    boolean mTopIsFullscreen;
    boolean mTranslucentDecorEnabled = true;
    int mTriplePressOnPowerBehavior;
    int mUiMode;
    IUiModeManager mUiModeManager;
    int mUndockedHdmiRotation;
    int mUnrestrictedScreenHeight;
    int mUnrestrictedScreenLeft;
    int mUnrestrictedScreenTop;
    int mUnrestrictedScreenWidth;
    int mUpsideDownRotation = 0;
    boolean mUseTvRouting;
    int mUserRotation = 0;
    int mUserRotationMode = 0;
    Vibrator mVibrator;
    int mVoiceContentBottom;
    int mVoiceContentLeft;
    int mVoiceContentRight;
    int mVoiceContentTop;
    Intent mVrHeadsetHomeIntent;
    volatile VrManagerInternal mVrManagerInternal;
    boolean mWakeGestureEnabledSetting;
    MyWakeGestureListener mWakeGestureListener;
    IWindowManager mWindowManager;
    final Runnable mWindowManagerDrawCallback = new Runnable() {
        public void run() {
            PhoneWindowManager.this.mHandler.sendEmptyMessage(7);
        }
    };
    boolean mWindowManagerDrawComplete;
    WindowManagerFuncs mWindowManagerFuncs;
    WindowManagerInternal mWindowManagerInternal;
    @GuardedBy("mHandler")
    private SleepToken mWindowSleepToken;
    private boolean mWindowSleepTokenNeeded;

    private static class HdmiControl {
        private final HdmiPlaybackClient mClient;

        private HdmiControl(HdmiPlaybackClient client) {
            this.mClient = client;
        }

        public void turnOnTv() {
            if (this.mClient != null) {
                this.mClient.oneTouchPlay(new OneTouchPlayCallback() {
                    public void onComplete(int result) {
                        if (result != 0) {
                            Log.w(PhoneWindowManager.TAG, "One touch play failed: " + result);
                        }
                    }
                });
            }
        }
    }

    final class HideNavInputEventReceiver extends InputEventReceiver {
        public HideNavInputEventReceiver(InputChannel inputChannel, Looper looper) {
            super(inputChannel, looper);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onInputEvent(android.view.InputEvent r13, int r14) {
            /*
            r12 = this;
            r3 = 0;
            r6 = r13 instanceof android.view.MotionEvent;	 Catch:{ all -> 0x0072 }
            if (r6 == 0) goto L_0x006b;
        L_0x0005:
            r6 = r13.getSource();	 Catch:{ all -> 0x0072 }
            r6 = r6 & 2;
            if (r6 == 0) goto L_0x006b;
        L_0x000d:
            r0 = r13;
            r0 = (android.view.MotionEvent) r0;	 Catch:{ all -> 0x0072 }
            r4 = r0;
            r6 = r4.getAction();	 Catch:{ all -> 0x0072 }
            if (r6 != 0) goto L_0x006b;
        L_0x0017:
            r2 = 0;
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x0072 }
            r6 = r6.mWindowManagerFuncs;	 Catch:{ all -> 0x0072 }
            r7 = r6.getWindowManagerLock();	 Catch:{ all -> 0x0072 }
            monitor-enter(r7);	 Catch:{ all -> 0x0072 }
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r6 = r6.mInputConsumer;	 Catch:{ all -> 0x006f }
            if (r6 != 0) goto L_0x002c;
        L_0x0027:
            monitor-exit(r7);	 Catch:{ all -> 0x0072 }
            r12.finishInputEvent(r13, r3);
            return;
        L_0x002c:
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r6 = r6.mResettingSystemUiFlags;	 Catch:{ all -> 0x006f }
            r6 = r6 | 2;
            r6 = r6 | 1;
            r5 = r6 | 4;
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r6 = r6.mResettingSystemUiFlags;	 Catch:{ all -> 0x006f }
            if (r6 == r5) goto L_0x0041;
        L_0x003c:
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r6.mResettingSystemUiFlags = r5;	 Catch:{ all -> 0x006f }
            r2 = 1;
        L_0x0041:
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r6 = r6.mForceClearedSystemUiFlags;	 Catch:{ all -> 0x006f }
            r5 = r6 | 2;
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r6 = r6.mForceClearedSystemUiFlags;	 Catch:{ all -> 0x006f }
            if (r6 == r5) goto L_0x0061;
        L_0x004d:
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r6.mForceClearedSystemUiFlags = r5;	 Catch:{ all -> 0x006f }
            r2 = 1;
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r6 = r6.mHandler;	 Catch:{ all -> 0x006f }
            r8 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x006f }
            r8 = r8.mClearHideNavigationFlag;	 Catch:{ all -> 0x006f }
            r10 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
            r6.postDelayed(r8, r10);	 Catch:{ all -> 0x006f }
        L_0x0061:
            monitor-exit(r7);	 Catch:{ all -> 0x0072 }
            if (r2 == 0) goto L_0x006b;
        L_0x0064:
            r6 = com.android.server.policy.PhoneWindowManager.this;	 Catch:{ all -> 0x0072 }
            r6 = r6.mWindowManagerFuncs;	 Catch:{ all -> 0x0072 }
            r6.reevaluateStatusBarVisibility();	 Catch:{ all -> 0x0072 }
        L_0x006b:
            r12.finishInputEvent(r13, r3);
            return;
        L_0x006f:
            r6 = move-exception;
            monitor-exit(r7);	 Catch:{ all -> 0x0072 }
            throw r6;	 Catch:{ all -> 0x0072 }
        L_0x0072:
            r6 = move-exception;
            r12.finishInputEvent(r13, r3);
            throw r6;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.HideNavInputEventReceiver.onInputEvent(android.view.InputEvent, int):void");
        }
    }

    class MyOrientationListener extends WindowOrientationListener {
        private final Runnable mUpdateRotationRunnable = new Runnable() {
            public void run() {
                PhoneWindowManager.this.mPowerManagerInternal.powerHint(2, 0);
                PhoneWindowManager.this.updateRotation(false);
            }
        };

        MyOrientationListener(Context context, Handler handler) {
            super(context, handler);
        }

        public void onProposedRotationChanged(int rotation) {
            PhoneWindowManager.this.mHandler.post(this.mUpdateRotationRunnable);
        }
    }

    class MyWakeGestureListener extends WakeGestureListener {
        MyWakeGestureListener(Context context, Handler handler) {
            super(context, handler);
        }

        public void onWakeUp() {
            synchronized (PhoneWindowManager.this.mLock) {
                if (PhoneWindowManager.this.shouldEnableWakeGestureLp()) {
                    PhoneWindowManager.this.performHapticFeedbackLw(null, 1, false);
                    PhoneWindowManager.this.wakeUp(SystemClock.uptimeMillis(), PhoneWindowManager.this.mAllowTheaterModeWakeFromWakeGesture, "android.policy:GESTURE");
                }
            }
        }
    }

    private class PolicyHandler extends Handler {
        private PolicyHandler() {
        }

        public void handleMessage(Message msg) {
            boolean z = true;
            PhoneWindowManager phoneWindowManager;
            switch (msg.what) {
                case 1:
                    PhoneWindowManager.this.enablePointerLocation();
                    return;
                case 2:
                    PhoneWindowManager.this.disablePointerLocation();
                    return;
                case 3:
                    PhoneWindowManager.this.dispatchMediaKeyWithWakeLock((KeyEvent) msg.obj);
                    return;
                case 4:
                    PhoneWindowManager.this.dispatchMediaKeyRepeatWithWakeLock((KeyEvent) msg.obj);
                    return;
                case 5:
                    PhoneWindowManager.this.finishKeyguardDrawn();
                    return;
                case 6:
                    Slog.w(PhoneWindowManager.TAG, "Keyguard drawn timeout. Setting mKeyguardDrawComplete");
                    PhoneWindowManager.this.finishKeyguardDrawn();
                    return;
                case 7:
                    PhoneWindowManager.this.finishWindowsDrawn();
                    return;
                case 9:
                    PhoneWindowManager.this.showRecentApps(false, msg.arg1 != 0);
                    return;
                case 10:
                    PhoneWindowManager.this.showGlobalActionsInternal();
                    return;
                case 11:
                    PhoneWindowManager.this.handleHideBootMessage();
                    return;
                case 12:
                    phoneWindowManager = PhoneWindowManager.this;
                    if (msg.arg1 == 0) {
                        z = false;
                    }
                    phoneWindowManager.launchVoiceAssistWithWakeLock(z);
                    return;
                case 13:
                    PhoneWindowManager phoneWindowManager2 = PhoneWindowManager.this;
                    long longValue = ((Long) msg.obj).longValue();
                    if (msg.arg1 == 0) {
                        z = false;
                    }
                    phoneWindowManager2.powerPress(longValue, z, msg.arg2);
                    PhoneWindowManager.this.finishPowerKeyPress();
                    return;
                case 14:
                    PhoneWindowManager.this.powerLongPress();
                    return;
                case 15:
                    phoneWindowManager = PhoneWindowManager.this;
                    if (msg.arg1 == 0) {
                        z = false;
                    }
                    phoneWindowManager.updateDreamingSleepToken(z);
                    return;
                case 16:
                    WindowState targetBar = msg.arg1 == 0 ? PhoneWindowManager.this.mStatusBar : PhoneWindowManager.this.mNavigationBar;
                    if (targetBar != null) {
                        PhoneWindowManager.this.requestTransientBars(targetBar);
                        return;
                    }
                    return;
                case 17:
                    PhoneWindowManager.this.showPictureInPictureMenuInternal();
                    return;
                case 18:
                    PhoneWindowManager.this.backLongPress();
                    PhoneWindowManager.this.finishBackKeyPress();
                    return;
                case 19:
                    PhoneWindowManager.this.disposeInputConsumer((InputConsumer) msg.obj);
                    return;
                case 20:
                    PhoneWindowManager.this.backMultiPressAction(((Long) msg.obj).longValue(), msg.arg1);
                    PhoneWindowManager.this.finishBackKeyPress();
                    return;
                case 21:
                    PhoneWindowManager.this.accessibilityShortcutActivated();
                    return;
                case 22:
                    PhoneWindowManager.this.takeBugreport();
                    return;
                case 23:
                    if (PhoneWindowManager.this.mAccessibilityShortcutController.isAccessibilityShortcutAvailable(false)) {
                        PhoneWindowManager.this.accessibilityShortcutActivated();
                        return;
                    }
                    return;
                case 24:
                    PhoneWindowManager.this.mAutofillManagerInternal.onBackKeyPressed();
                    return;
                case 25:
                    PhoneWindowManager.this.sendSystemKeyToStatusBar(msg.arg1);
                    return;
                case 26:
                    PhoneWindowManager.this.launchAllAppsAction();
                    return;
                default:
                    return;
            }
        }
    }

    class ScreenLockTimeout implements Runnable {
        Bundle options;

        ScreenLockTimeout() {
        }

        public void run() {
            synchronized (this) {
                if (PhoneWindowManager.this.mKeyguardDelegate != null) {
                    PhoneWindowManager.this.mKeyguardDelegate.doKeyguardTimeout(this.options);
                }
                PhoneWindowManager.this.mLockScreenTimerActive = false;
                this.options = null;
            }
        }

        public void setLockOptions(Bundle options) {
            this.options = options;
        }
    }

    private class ScreenshotRunnable implements Runnable {
        private int mScreenshotType;

        private ScreenshotRunnable() {
            this.mScreenshotType = 1;
        }

        public void setScreenshotType(int screenshotType) {
            this.mScreenshotType = screenshotType;
        }

        public void run() {
            PhoneWindowManager.this.takeScreenshot(this.mScreenshotType);
        }
    }

    class SettingsObserver extends ContentObserver {
        SettingsObserver(Handler handler) {
            super(handler);
        }

        void observe() {
            ContentResolver resolver = PhoneWindowManager.this.mContext.getContentResolver();
            resolver.registerContentObserver(System.getUriFor("end_button_behavior"), false, this, -1);
            resolver.registerContentObserver(Secure.getUriFor("incall_power_button_behavior"), false, this, -1);
            resolver.registerContentObserver(Secure.getUriFor("incall_back_button_behavior"), false, this, -1);
            resolver.registerContentObserver(Secure.getUriFor("wake_gesture_enabled"), false, this, -1);
            resolver.registerContentObserver(System.getUriFor("accelerometer_rotation"), false, this, -1);
            resolver.registerContentObserver(System.getUriFor("user_rotation"), false, this, -1);
            resolver.registerContentObserver(System.getUriFor("screen_off_timeout"), false, this, -1);
            resolver.registerContentObserver(System.getUriFor("pointer_location"), false, this, -1);
            resolver.registerContentObserver(Secure.getUriFor("default_input_method"), false, this, -1);
            resolver.registerContentObserver(Secure.getUriFor("immersive_mode_confirmations"), false, this, -1);
            PhoneWindowManager.this.updateSettings();
        }

        public void onChange(boolean selfChange) {
            PhoneWindowManager.this.updateSettings();
            PhoneWindowManager.this.updateRotation(false);
        }
    }

    static {
        sApplicationLaunchKeyCategories.append(64, "android.intent.category.APP_BROWSER");
        sApplicationLaunchKeyCategories.append(65, "android.intent.category.APP_EMAIL");
        sApplicationLaunchKeyCategories.append(207, "android.intent.category.APP_CONTACTS");
        sApplicationLaunchKeyCategories.append(208, "android.intent.category.APP_CALENDAR");
        sApplicationLaunchKeyCategories.append(209, "android.intent.category.APP_MUSIC");
        sApplicationLaunchKeyCategories.append(210, "android.intent.category.APP_CALCULATOR");
    }

    /* synthetic */ void lambda$-com_android_server_policy_PhoneWindowManager_48891() {
        if (this.mWindowSleepToken == null) {
            this.mWindowSleepToken = this.mActivityManagerInternal.acquireSleepToken("WindowSleepToken", 0);
        }
    }

    /* synthetic */ void lambda$-com_android_server_policy_PhoneWindowManager_49159() {
        if (this.mWindowSleepToken != null) {
            this.mWindowSleepToken.release();
            this.mWindowSleepToken = null;
        }
    }

    IStatusBarService getStatusBarService() {
        IStatusBarService iStatusBarService;
        synchronized (this.mServiceAquireLock) {
            if (this.mStatusBarService == null) {
                this.mStatusBarService = IStatusBarService.Stub.asInterface(ServiceManager.getService("statusbar"));
            }
            iStatusBarService = this.mStatusBarService;
        }
        return iStatusBarService;
    }

    StatusBarManagerInternal getStatusBarManagerInternal() {
        StatusBarManagerInternal statusBarManagerInternal;
        synchronized (this.mServiceAquireLock) {
            if (this.mStatusBarManagerInternal == null) {
                this.mStatusBarManagerInternal = (StatusBarManagerInternal) LocalServices.getService(StatusBarManagerInternal.class);
            }
            statusBarManagerInternal = this.mStatusBarManagerInternal;
        }
        return statusBarManagerInternal;
    }

    boolean needSensorRunningLp() {
        if (this.mSupportAutoRotation && (this.mCurrentAppOrientation == 4 || this.mCurrentAppOrientation == 10 || this.mCurrentAppOrientation == 7 || this.mCurrentAppOrientation == 6)) {
            return true;
        }
        if ((this.mCarDockEnablesAccelerometer && this.mDockMode == 2) || (this.mDeskDockEnablesAccelerometer && (this.mDockMode == 1 || this.mDockMode == 3 || this.mDockMode == 4))) {
            return true;
        }
        if (this.mUserRotationMode == 1) {
            return false;
        }
        return this.mSupportAutoRotation;
    }

    void updateOrientationListenerLp() {
        if (this.mOrientationListener.canDetectOrientation()) {
            boolean disable = true;
            if (this.mScreenOnEarly && this.mAwake && this.mKeyguardDrawComplete && this.mWindowManagerDrawComplete && needSensorRunningLp()) {
                disable = false;
                if (!this.mOrientationSensorEnabled) {
                    this.mOrientationListener.enable(true);
                    this.mOrientationSensorEnabled = true;
                }
            }
            if (disable && this.mOrientationSensorEnabled) {
                this.mOrientationListener.disable();
                this.mOrientationSensorEnabled = false;
            }
        }
    }

    private void interceptBackKeyDown() {
        MetricsLogger.count(this.mContext, "key_back_down", 1);
        this.mBackKeyHandled = false;
        if (hasPanicPressOnBackBehavior() && this.mBackKeyPressCounter != 0 && this.mBackKeyPressCounter < 4) {
            this.mHandler.removeMessages(20);
        }
        if (hasLongPressOnBackBehavior()) {
            Message msg = this.mHandler.obtainMessage(18);
            msg.setAsynchronous(true);
            this.mHandler.sendMessageDelayed(msg, ViewConfiguration.get(this.mContext).getDeviceGlobalActionKeyTimeout());
        }
    }

    private boolean interceptBackKeyUp(KeyEvent event) {
        boolean handled = this.mBackKeyHandled;
        if (hasPanicPressOnBackBehavior()) {
            this.mBackKeyPressCounter++;
            long eventTime = event.getDownTime();
            if (this.mBackKeyPressCounter <= 4) {
                Message msg = this.mHandler.obtainMessage(20, this.mBackKeyPressCounter, 0, Long.valueOf(eventTime));
                msg.setAsynchronous(true);
                this.mHandler.sendMessageDelayed(msg, (long) ViewConfiguration.getMultiPressTimeout());
            }
        }
        cancelPendingBackKeyAction();
        if (this.mHasFeatureWatch) {
            TelecomManager telecomManager = getTelecommService();
            if (telecomManager != null) {
                if (telecomManager.isRinging()) {
                    telecomManager.silenceRinger();
                    return false;
                } else if ((this.mIncallBackBehavior & 1) != 0 && telecomManager.isInCall()) {
                    return telecomManager.endCall();
                }
            }
        }
        if (this.mAutofillManagerInternal != null && event.getKeyCode() == 4) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(24));
        }
        return handled;
    }

    private void interceptPowerKeyDown(KeyEvent event, boolean interactive) {
        if (!this.mPowerKeyWakeLock.isHeld()) {
            this.mPowerKeyWakeLock.acquire();
        }
        if (this.mPowerKeyPressCounter != 0) {
            this.mHandler.removeMessages(13);
        }
        if (this.mImmersiveModeConfirmation.onPowerKeyDown(interactive, SystemClock.elapsedRealtime(), isImmersiveMode(this.mLastSystemUiFlags), isNavBarEmpty(this.mLastSystemUiFlags))) {
            this.mHandler.post(this.mHiddenNavPanic);
        }
        if (interactive && (this.mScreenshotChordPowerKeyTriggered ^ 1) != 0 && (event.getFlags() & 1024) == 0) {
            this.mScreenshotChordPowerKeyTriggered = true;
            this.mScreenshotChordPowerKeyTime = event.getDownTime();
            interceptScreenshotChord();
        }
        TelecomManager telecomManager = getTelecommService();
        boolean hungUp = false;
        if (telecomManager != null) {
            if (telecomManager.isRinging()) {
                telecomManager.silenceRinger();
            } else if ((this.mIncallPowerBehavior & 2) != 0 && telecomManager.isInCall() && interactive) {
                hungUp = telecomManager.endCall();
            }
        }
        GestureLauncherService gestureService = (GestureLauncherService) LocalServices.getService(GestureLauncherService.class);
        boolean z = false;
        if (gestureService != null) {
            z = gestureService.interceptPowerKeyDown(event, interactive, this.mTmpBoolean);
            if (this.mTmpBoolean.value && this.mRequestedOrGoingToSleep) {
                this.mCameraGestureTriggeredDuringGoingToSleep = true;
            }
        }
        sendSystemKeyToStatusBarAsync(event.getKeyCode());
        if (hungUp || this.mScreenshotChordVolumeDownKeyTriggered || this.mA11yShortcutChordVolumeUpKeyTriggered) {
            z = true;
        }
        this.mPowerKeyHandled = z;
        if (!this.mPowerKeyHandled) {
            Message msg;
            if (!interactive) {
                wakeUpFromPowerKey(event.getDownTime());
                if (this.mSupportLongPressPowerWhenNonInteractive && hasLongPressOnPowerBehavior()) {
                    msg = this.mHandler.obtainMessage(14);
                    msg.setAsynchronous(true);
                    this.mHandler.sendMessageDelayed(msg, ViewConfiguration.get(this.mContext).getDeviceGlobalActionKeyTimeout());
                    this.mBeganFromNonInteractive = true;
                } else if (getMaxMultiPressPowerCount() <= 1) {
                    this.mPowerKeyHandled = true;
                } else {
                    this.mBeganFromNonInteractive = true;
                }
            } else if (hasLongPressOnPowerBehavior()) {
                msg = this.mHandler.obtainMessage(14);
                msg.setAsynchronous(true);
                this.mHandler.sendMessageDelayed(msg, ViewConfiguration.get(this.mContext).getDeviceGlobalActionKeyTimeout());
            }
        }
    }

    private void interceptPowerKeyUp(KeyEvent event, boolean interactive, boolean canceled) {
        int i = 0;
        boolean z = !canceled ? this.mPowerKeyHandled : true;
        this.mScreenshotChordPowerKeyTriggered = false;
        cancelPendingScreenshotChordAction();
        cancelPendingPowerKeyAction();
        if (!z) {
            this.mPowerKeyPressCounter++;
            int maxCount = getMaxMultiPressPowerCount();
            long eventTime = event.getDownTime();
            if (this.mPowerKeyPressCounter < maxCount) {
                Handler handler = this.mHandler;
                if (interactive) {
                    i = 1;
                }
                Message msg = handler.obtainMessage(13, i, this.mPowerKeyPressCounter, Long.valueOf(eventTime));
                msg.setAsynchronous(true);
                this.mHandler.sendMessageDelayed(msg, (long) ViewConfiguration.getMultiPressTimeout());
                return;
            }
            powerPress(eventTime, interactive, this.mPowerKeyPressCounter);
        }
        finishPowerKeyPress();
    }

    private void finishPowerKeyPress() {
        this.mBeganFromNonInteractive = false;
        this.mPowerKeyPressCounter = 0;
        if (this.mPowerKeyWakeLock.isHeld()) {
            this.mPowerKeyWakeLock.release();
        }
    }

    private void finishBackKeyPress() {
        this.mBackKeyPressCounter = 0;
    }

    private void cancelPendingPowerKeyAction() {
        if (!this.mPowerKeyHandled) {
            this.mPowerKeyHandled = true;
            this.mHandler.removeMessages(14);
        }
    }

    private void cancelPendingBackKeyAction() {
        if (!this.mBackKeyHandled) {
            this.mBackKeyHandled = true;
            this.mHandler.removeMessages(18);
        }
    }

    private void backMultiPressAction(long eventTime, int count) {
        if (count >= 4) {
            switch (this.mPanicPressOnBackBehavior) {
                case 1:
                    launchHomeFromHotKey();
                    return;
                default:
                    return;
            }
        }
    }

    private void powerPress(long eventTime, boolean interactive, int count) {
        if (!this.mScreenOnEarly || (this.mScreenOnFully ^ 1) == 0) {
            if (count != 2) {
                if (count != 3) {
                    if (interactive && (this.mBeganFromNonInteractive ^ 1) != 0) {
                        switch (this.mShortPressOnPowerBehavior) {
                            case 0:
                                break;
                            case 1:
                                goToSleep(eventTime, 4, 0);
                                break;
                            case 2:
                                goToSleep(eventTime, 4, 1);
                                break;
                            case 3:
                                goToSleep(eventTime, 4, 1);
                                launchHomeFromHotKey();
                                break;
                            case 4:
                                shortPressPowerGoHome();
                                break;
                            case 5:
                                if (!this.mDismissImeOnBackKeyPressed) {
                                    shortPressPowerGoHome();
                                    break;
                                }
                                if (this.mInputMethodManagerInternal == null) {
                                    this.mInputMethodManagerInternal = (InputMethodManagerInternal) LocalServices.getService(InputMethodManagerInternal.class);
                                }
                                if (this.mInputMethodManagerInternal != null) {
                                    this.mInputMethodManagerInternal.hideCurrentInputMethod();
                                    break;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
                powerMultiPressAction(eventTime, interactive, this.mTriplePressOnPowerBehavior);
            } else {
                powerMultiPressAction(eventTime, interactive, this.mDoublePressOnPowerBehavior);
            }
            return;
        }
        Slog.i(TAG, "Suppressed redundant power key press while already in the process of turning the screen on.");
    }

    private void goToSleep(long eventTime, int reason, int flags) {
        this.mRequestedOrGoingToSleep = true;
        this.mPowerManager.goToSleep(eventTime, reason, flags);
    }

    private void shortPressPowerGoHome() {
        launchHomeFromHotKey(true, false);
        if (isKeyguardShowingAndNotOccluded()) {
            this.mKeyguardDelegate.onShortPowerPressedGoHome();
        }
    }

    private void powerMultiPressAction(long eventTime, boolean interactive, int behavior) {
        switch (behavior) {
            case 1:
                if (!isUserSetupComplete()) {
                    Slog.i(TAG, "Ignoring toggling theater mode - device not setup.");
                    return;
                } else if (isTheaterModeEnabled()) {
                    Slog.i(TAG, "Toggling theater mode off.");
                    Global.putInt(this.mContext.getContentResolver(), "theater_mode_on", 0);
                    if (!interactive) {
                        wakeUpFromPowerKey(eventTime);
                        return;
                    }
                    return;
                } else {
                    Slog.i(TAG, "Toggling theater mode on.");
                    Global.putInt(this.mContext.getContentResolver(), "theater_mode_on", 1);
                    if (this.mGoToSleepOnButtonPressTheaterMode && interactive) {
                        goToSleep(eventTime, 4, 0);
                        return;
                    }
                    return;
                }
            case 2:
                Slog.i(TAG, "Starting brightness boost.");
                if (!interactive) {
                    wakeUpFromPowerKey(eventTime);
                }
                this.mPowerManager.boostScreenBrightness(eventTime);
                return;
            default:
                return;
        }
    }

    private int getMaxMultiPressPowerCount() {
        if (this.mTriplePressOnPowerBehavior != 0) {
            return 3;
        }
        if (this.mDoublePressOnPowerBehavior != 0) {
            return 2;
        }
        return 1;
    }

    private void powerLongPress() {
        boolean z = true;
        int behavior = getResolvedLongPressOnPowerBehavior();
        switch (behavior) {
            case 1:
                this.mPowerKeyHandled = true;
                performHapticFeedbackLw(null, 0, false);
                showGlobalActionsInternal();
                return;
            case 2:
            case 3:
                this.mPowerKeyHandled = true;
                performHapticFeedbackLw(null, 0, false);
                sendCloseSystemWindows(SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS);
                WindowManagerFuncs windowManagerFuncs = this.mWindowManagerFuncs;
                if (behavior != 2) {
                    z = false;
                }
                windowManagerFuncs.shutdown(z);
                return;
            default:
                return;
        }
    }

    private void backLongPress() {
        this.mBackKeyHandled = true;
        switch (this.mLongPressOnBackBehavior) {
            case 1:
                boolean z;
                if (this.mKeyguardDelegate == null) {
                    z = false;
                } else {
                    z = this.mKeyguardDelegate.isShowing();
                }
                if (!z) {
                    startActivityAsUser(new Intent("android.intent.action.VOICE_ASSIST"), UserHandle.CURRENT_OR_SELF);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void accessibilityShortcutActivated() {
        this.mAccessibilityShortcutController.performAccessibilityShortcut();
    }

    private void disposeInputConsumer(InputConsumer inputConsumer) {
        if (inputConsumer != null) {
            inputConsumer.dismiss();
        }
    }

    private void sleepPress(long eventTime) {
        if (this.mShortPressOnSleepBehavior == 1) {
            launchHomeFromHotKey(false, true);
        }
    }

    private void sleepRelease(long eventTime) {
        switch (this.mShortPressOnSleepBehavior) {
            case 0:
            case 1:
                Slog.i(TAG, "sleepRelease() calling goToSleep(GO_TO_SLEEP_REASON_SLEEP_BUTTON)");
                goToSleep(eventTime, 6, 0);
                return;
            default:
                return;
        }
    }

    private int getResolvedLongPressOnPowerBehavior() {
        if (FactoryTest.isLongPressOnPowerOffEnabled()) {
            return 3;
        }
        return this.mLongPressOnPowerBehavior;
    }

    private boolean hasLongPressOnPowerBehavior() {
        return getResolvedLongPressOnPowerBehavior() != 0;
    }

    private boolean hasLongPressOnBackBehavior() {
        return this.mLongPressOnBackBehavior != 0;
    }

    private boolean hasPanicPressOnBackBehavior() {
        return this.mPanicPressOnBackBehavior != 0;
    }

    private void interceptScreenshotChord() {
        if (this.mScreenshotChordEnabled && this.mScreenshotChordVolumeDownKeyTriggered && this.mScreenshotChordPowerKeyTriggered && (this.mA11yShortcutChordVolumeUpKeyTriggered ^ 1) != 0) {
            long now = SystemClock.uptimeMillis();
            if (now <= this.mScreenshotChordVolumeDownKeyTime + SCREENSHOT_CHORD_DEBOUNCE_DELAY_MILLIS && now <= this.mScreenshotChordPowerKeyTime + SCREENSHOT_CHORD_DEBOUNCE_DELAY_MILLIS) {
                this.mScreenshotChordVolumeDownKeyConsumed = true;
                cancelPendingPowerKeyAction();
                this.mScreenshotRunnable.setScreenshotType(1);
                this.mHandler.postDelayed(this.mScreenshotRunnable, getScreenshotChordLongPressDelay());
            }
        }
    }

    private void interceptAccessibilityShortcutChord() {
        if (this.mAccessibilityShortcutController.isAccessibilityShortcutAvailable(isKeyguardLocked()) && this.mScreenshotChordVolumeDownKeyTriggered && this.mA11yShortcutChordVolumeUpKeyTriggered && (this.mScreenshotChordPowerKeyTriggered ^ 1) != 0) {
            long now = SystemClock.uptimeMillis();
            if (now <= this.mScreenshotChordVolumeDownKeyTime + SCREENSHOT_CHORD_DEBOUNCE_DELAY_MILLIS && now <= this.mA11yShortcutChordVolumeUpKeyTime + SCREENSHOT_CHORD_DEBOUNCE_DELAY_MILLIS) {
                this.mScreenshotChordVolumeDownKeyConsumed = true;
                this.mA11yShortcutChordVolumeUpKeyConsumed = true;
                this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(21), ViewConfiguration.get(this.mContext).getAccessibilityShortcutKeyTimeout());
            }
        }
    }

    private long getScreenshotChordLongPressDelay() {
        if (this.mKeyguardDelegate.isShowing()) {
            return (long) (((float) ViewConfiguration.get(this.mContext).getDeviceGlobalActionKeyTimeout()) * KEYGUARD_SCREENSHOT_CHORD_DELAY_MULTIPLIER);
        }
        return ViewConfiguration.get(this.mContext).getDeviceGlobalActionKeyTimeout();
    }

    private void cancelPendingScreenshotChordAction() {
        this.mHandler.removeCallbacks(this.mScreenshotRunnable);
    }

    private void cancelPendingAccessibilityShortcutAction() {
        this.mHandler.removeMessages(21);
    }

    public void showGlobalActions() {
        this.mHandler.removeMessages(10);
        this.mHandler.sendEmptyMessage(10);
    }

    void showGlobalActionsInternal() {
        sendCloseSystemWindows(SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS);
        if (this.mGlobalActions == null) {
            this.mGlobalActions = new GlobalActions(this.mContext, this.mWindowManagerFuncs);
        }
        boolean keyguardShowing = isKeyguardShowingAndNotOccluded();
        this.mGlobalActions.showDialog(keyguardShowing, isDeviceProvisioned());
        if (keyguardShowing) {
            this.mPowerManager.userActivity(SystemClock.uptimeMillis(), false);
        }
    }

    boolean isDeviceProvisioned() {
        if (Global.getInt(this.mContext.getContentResolver(), "device_provisioned", 0) != 0) {
            return true;
        }
        return false;
    }

    boolean isUserSetupComplete() {
        boolean isSetupComplete = Secure.getIntForUser(this.mContext.getContentResolver(), "user_setup_complete", 0, -2) != 0;
        if (this.mHasFeatureLeanback) {
            return isSetupComplete & isTvUserSetupComplete();
        }
        return isSetupComplete;
    }

    private boolean isTvUserSetupComplete() {
        return Secure.getIntForUser(this.mContext.getContentResolver(), "tv_user_setup_complete", 0, -2) != 0;
    }

    private void handleShortPressOnHome() {
        HdmiControl hdmiControl = getHdmiControl();
        if (hdmiControl != null) {
            hdmiControl.turnOnTv();
        }
        if (this.mDreamManagerInternal == null || !this.mDreamManagerInternal.isDreaming()) {
            launchHomeFromHotKey();
        } else {
            this.mDreamManagerInternal.stopDream(false);
        }
    }

    private HdmiControl getHdmiControl() {
        if (this.mHdmiControl == null) {
            if (!this.mContext.getPackageManager().hasSystemFeature("android.hardware.hdmi.cec")) {
                return null;
            }
            HdmiControlManager manager = (HdmiControlManager) this.mContext.getSystemService("hdmi_control");
            HdmiPlaybackClient client = null;
            if (manager != null) {
                client = manager.getPlaybackClient();
            }
            this.mHdmiControl = new HdmiControl(client);
        }
        return this.mHdmiControl;
    }

    private void handleLongPressOnHome(int deviceId) {
        if (this.mLongPressOnHomeBehavior != 0) {
            this.mHomeConsumed = true;
            performHapticFeedbackLw(null, 0, false);
            switch (this.mLongPressOnHomeBehavior) {
                case 1:
                    launchAllAppsAction();
                    break;
                case 2:
                    launchAssistAction(null, deviceId);
                    break;
                default:
                    Log.w(TAG, "Undefined home long press behavior: " + this.mLongPressOnHomeBehavior);
                    break;
            }
        }
    }

    private void launchAllAppsAction() {
        Intent intent = new Intent("android.intent.action.ALL_APPS");
        if (this.mHasFeatureLeanback) {
            PackageManager pm = this.mContext.getPackageManager();
            Intent intentLauncher = new Intent("android.intent.action.MAIN");
            intentLauncher.addCategory("android.intent.category.HOME");
            ResolveInfo resolveInfo = pm.resolveActivityAsUser(intentLauncher, DumpState.DUMP_DEXOPT, this.mCurrentUserId);
            if (resolveInfo != null) {
                intent.setPackage(resolveInfo.activityInfo.packageName);
            }
        }
        startActivityAsUser(intent, UserHandle.CURRENT);
    }

    private void handleDoubleTapOnHome() {
        if (this.mDoubleTapOnHomeBehavior == 1) {
            this.mHomeConsumed = true;
            toggleRecentApps();
        }
    }

    private void showPictureInPictureMenu(KeyEvent event) {
        this.mHandler.removeMessages(17);
        Message msg = this.mHandler.obtainMessage(17);
        msg.setAsynchronous(true);
        msg.sendToTarget();
    }

    private void showPictureInPictureMenuInternal() {
        StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
        if (statusbar != null) {
            statusbar.showPictureInPictureMenu();
        }
    }

    private boolean isRoundWindow() {
        return this.mContext.getResources().getConfiguration().isScreenRound();
    }

    public void init(Context context, IWindowManager windowManager, WindowManagerFuncs windowManagerFuncs) {
        boolean z;
        this.mContext = context;
        this.mWindowManager = windowManager;
        this.mWindowManagerFuncs = windowManagerFuncs;
        this.mWindowManagerInternal = (WindowManagerInternal) LocalServices.getService(WindowManagerInternal.class);
        this.mActivityManagerInternal = (ActivityManagerInternal) LocalServices.getService(ActivityManagerInternal.class);
        this.mInputManagerInternal = (InputManagerInternal) LocalServices.getService(InputManagerInternal.class);
        this.mDreamManagerInternal = (DreamManagerInternal) LocalServices.getService(DreamManagerInternal.class);
        this.mPowerManagerInternal = (PowerManagerInternal) LocalServices.getService(PowerManagerInternal.class);
        this.mAppOpsManager = (AppOpsManager) this.mContext.getSystemService("appops");
        this.mHasFeatureWatch = this.mContext.getPackageManager().hasSystemFeature("android.hardware.type.watch");
        this.mCameraNotchInset = context.getResources().getDimensionPixelSize(17105027);
        this.mHasFeatureLeanback = this.mContext.getPackageManager().hasSystemFeature("android.software.leanback");
        this.mAccessibilityShortcutController = new AccessibilityShortcutController(this.mContext, new Handler(), this.mCurrentUserId);
        boolean burnInProtectionEnabled = context.getResources().getBoolean(17956945);
        boolean burnInProtectionDevMode = SystemProperties.getBoolean("persist.debug.force_burn_in", false);
        if (burnInProtectionEnabled || burnInProtectionDevMode) {
            int minHorizontal;
            int maxHorizontal;
            int minVertical;
            int maxVertical;
            int maxRadius;
            if (burnInProtectionDevMode) {
                minHorizontal = -8;
                maxHorizontal = 8;
                minVertical = -8;
                maxVertical = -4;
                if (isRoundWindow()) {
                    maxRadius = 6;
                } else {
                    maxRadius = -1;
                }
            } else {
                Resources resources = context.getResources();
                minHorizontal = resources.getInteger(17694749);
                maxHorizontal = resources.getInteger(17694746);
                minVertical = resources.getInteger(17694750);
                maxVertical = resources.getInteger(17694748);
                maxRadius = resources.getInteger(17694747);
            }
            this.mBurnInProtectionHelper = new BurnInProtectionHelper(context, minHorizontal, maxHorizontal, minVertical, maxVertical, maxRadius);
        }
        PhoneWindowManager phoneWindowManager = this;
        this.mHandler = new PolicyHandler();
        this.mWakeGestureListener = new MyWakeGestureListener(this.mContext, this.mHandler);
        this.mOrientationListener = new MyOrientationListener(this.mContext, this.mHandler);
        try {
            this.mOrientationListener.setCurrentRotation(windowManager.getDefaultDisplayRotation());
        } catch (RemoteException e) {
        }
        this.mSettingsObserver = new SettingsObserver(this.mHandler);
        this.mSettingsObserver.observe();
        this.mShortcutManager = new ShortcutManager(context);
        this.mUiMode = context.getResources().getInteger(17694770);
        this.mHomeIntent = new Intent("android.intent.action.MAIN", null);
        this.mHomeIntent.addCategory("android.intent.category.HOME");
        this.mHomeIntent.addFlags(270532608);
        this.mEnableCarDockHomeCapture = context.getResources().getBoolean(17956946);
        this.mCarDockIntent = new Intent("android.intent.action.MAIN", null);
        this.mCarDockIntent.addCategory("android.intent.category.CAR_DOCK");
        this.mCarDockIntent.addFlags(270532608);
        this.mDeskDockIntent = new Intent("android.intent.action.MAIN", null);
        this.mDeskDockIntent.addCategory("android.intent.category.DESK_DOCK");
        this.mDeskDockIntent.addFlags(270532608);
        this.mVrHeadsetHomeIntent = new Intent("android.intent.action.MAIN", null);
        this.mVrHeadsetHomeIntent.addCategory("android.intent.category.VR_HOME");
        this.mVrHeadsetHomeIntent.addFlags(270532608);
        this.mPowerManager = (PowerManager) context.getSystemService("power");
        this.mBroadcastWakeLock = this.mPowerManager.newWakeLock(1, "PhoneWindowManager.mBroadcastWakeLock");
        this.mPowerKeyWakeLock = this.mPowerManager.newWakeLock(1, "PhoneWindowManager.mPowerKeyWakeLock");
        this.mEnableShiftMenuBugReports = "1".equals(SystemProperties.get("ro.debuggable"));
        this.mSupportAutoRotation = this.mContext.getResources().getBoolean(17957023);
        this.mLidOpenRotation = readRotation(17694794);
        this.mCarDockRotation = readRotation(17694754);
        this.mDeskDockRotation = readRotation(17694773);
        this.mUndockedHdmiRotation = readRotation(17694862);
        this.mCarDockEnablesAccelerometer = this.mContext.getResources().getBoolean(17956908);
        this.mDeskDockEnablesAccelerometer = this.mContext.getResources().getBoolean(17956920);
        this.mLidKeyboardAccessibility = this.mContext.getResources().getInteger(17694792);
        this.mLidNavigationAccessibility = this.mContext.getResources().getInteger(17694793);
        this.mLidControlsScreenLock = this.mContext.getResources().getBoolean(17956978);
        this.mLidControlsSleep = this.mContext.getResources().getBoolean(17956979);
        this.mTranslucentDecorEnabled = this.mContext.getResources().getBoolean(17956960);
        this.mAllowTheaterModeWakeFromKey = this.mContext.getResources().getBoolean(17956879);
        if (this.mAllowTheaterModeWakeFromKey) {
            z = true;
        } else {
            z = this.mContext.getResources().getBoolean(17956883);
        }
        this.mAllowTheaterModeWakeFromPowerKey = z;
        this.mAllowTheaterModeWakeFromMotion = this.mContext.getResources().getBoolean(17956881);
        this.mAllowTheaterModeWakeFromMotionWhenNotDreaming = this.mContext.getResources().getBoolean(17956882);
        this.mAllowTheaterModeWakeFromCameraLens = this.mContext.getResources().getBoolean(17956876);
        this.mAllowTheaterModeWakeFromLidSwitch = this.mContext.getResources().getBoolean(17956880);
        this.mAllowTheaterModeWakeFromWakeGesture = this.mContext.getResources().getBoolean(17956878);
        this.mGoToSleepOnButtonPressTheaterMode = this.mContext.getResources().getBoolean(17956970);
        this.mSupportLongPressPowerWhenNonInteractive = this.mContext.getResources().getBoolean(17957025);
        this.mLongPressOnBackBehavior = this.mContext.getResources().getInteger(17694797);
        this.mPanicPressOnBackBehavior = this.mContext.getResources().getInteger(17694737);
        this.mShortPressOnPowerBehavior = this.mContext.getResources().getInteger(17694850);
        this.mLongPressOnPowerBehavior = this.mContext.getResources().getInteger(17694799);
        this.mDoublePressOnPowerBehavior = this.mContext.getResources().getInteger(17694775);
        this.mTriplePressOnPowerBehavior = this.mContext.getResources().getInteger(17694861);
        this.mShortPressOnSleepBehavior = this.mContext.getResources().getInteger(17694851);
        this.mUseTvRouting = AudioSystem.getPlatformType(this.mContext) == 2;
        this.mHandleVolumeKeysInWM = this.mContext.getResources().getBoolean(17956972);
        readConfigurationDependentBehaviors();
        this.mAccessibilityManager = (AccessibilityManager) context.getSystemService("accessibility");
        IntentFilter filter = new IntentFilter();
        filter.addAction(UiModeManager.ACTION_ENTER_CAR_MODE);
        filter.addAction(UiModeManager.ACTION_EXIT_CAR_MODE);
        filter.addAction(UiModeManager.ACTION_ENTER_DESK_MODE);
        filter.addAction(UiModeManager.ACTION_EXIT_DESK_MODE);
        filter.addAction("android.intent.action.DOCK_EVENT");
        Intent intent = context.registerReceiver(this.mDockReceiver, filter);
        if (intent != null) {
            this.mDockMode = intent.getIntExtra("android.intent.extra.DOCK_STATE", 0);
        }
        filter = new IntentFilter();
        filter.addAction("android.intent.action.DREAMING_STARTED");
        filter.addAction("android.intent.action.DREAMING_STOPPED");
        context.registerReceiver(this.mDreamReceiver, filter);
        context.registerReceiver(this.mMultiuserReceiver, new IntentFilter("android.intent.action.USER_SWITCHED"));
        this.mSystemGestures = new SystemGesturesPointerEventListener(context, new Callbacks() {
            public void onSwipeFromTop() {
                if (PhoneWindowManager.this.mStatusBar != null) {
                    PhoneWindowManager.this.requestTransientBars(PhoneWindowManager.this.mStatusBar);
                }
            }

            public void onSwipeFromBottom() {
                if (PhoneWindowManager.this.mNavigationBar != null && PhoneWindowManager.this.mNavigationBarPosition == 4) {
                    PhoneWindowManager.this.requestTransientBars(PhoneWindowManager.this.mNavigationBar);
                }
            }

            public void onSwipeFromRight() {
                if (PhoneWindowManager.this.mNavigationBar != null && PhoneWindowManager.this.mNavigationBarPosition == 2) {
                    PhoneWindowManager.this.requestTransientBars(PhoneWindowManager.this.mNavigationBar);
                }
            }

            public void onSwipeFromLeft() {
                if (PhoneWindowManager.this.mNavigationBar != null && PhoneWindowManager.this.mNavigationBarPosition == 1) {
                    PhoneWindowManager.this.requestTransientBars(PhoneWindowManager.this.mNavigationBar);
                }
            }

            public void onFling(int duration) {
                if (PhoneWindowManager.this.mPowerManagerInternal != null) {
                    PhoneWindowManager.this.mPowerManagerInternal.powerHint(2, duration);
                }
            }

            public void onDebug() {
            }

            public void onDown() {
                PhoneWindowManager.this.mOrientationListener.onTouchStart();
            }

            public void onUpOrCancel() {
                PhoneWindowManager.this.mOrientationListener.onTouchEnd();
            }

            public void onMouseHoverAtTop() {
                PhoneWindowManager.this.mHandler.removeMessages(16);
                Message msg = PhoneWindowManager.this.mHandler.obtainMessage(16);
                msg.arg1 = 0;
                PhoneWindowManager.this.mHandler.sendMessageDelayed(msg, 500);
            }

            public void onMouseHoverAtBottom() {
                PhoneWindowManager.this.mHandler.removeMessages(16);
                Message msg = PhoneWindowManager.this.mHandler.obtainMessage(16);
                msg.arg1 = 1;
                PhoneWindowManager.this.mHandler.sendMessageDelayed(msg, 500);
            }

            public void onMouseLeaveFromEdge() {
                PhoneWindowManager.this.mHandler.removeMessages(16);
            }
        });
        this.mImmersiveModeConfirmation = new ImmersiveModeConfirmation(this.mContext);
        this.mWindowManagerFuncs.registerPointerEventListener(this.mSystemGestures);
        this.mVibrator = (Vibrator) context.getSystemService("vibrator");
        this.mLongPressVibePattern = getLongIntArray(this.mContext.getResources(), 17236011);
        this.mCalendarDateVibePattern = getLongIntArray(this.mContext.getResources(), 17235986);
        this.mSafeModeEnabledVibePattern = getLongIntArray(this.mContext.getResources(), 17236023);
        this.mScreenshotChordEnabled = this.mContext.getResources().getBoolean(17956959);
        this.mGlobalKeyManager = new GlobalKeyManager(this.mContext);
        initializeHdmiState();
        if (!this.mPowerManager.isInteractive()) {
            startedGoingToSleep(2);
            finishedGoingToSleep(2);
        }
        this.mWindowManagerInternal.registerAppTransitionListener(this.mStatusBarController.getAppTransitionListener());
        this.mWindowManagerInternal.registerAppTransitionListener(new AppTransitionListener() {
            public int onAppTransitionStartingLocked(int transit, IBinder openToken, IBinder closeToken, Animation openAnimation, Animation closeAnimation) {
                return PhoneWindowManager.this.handleStartTransitionForKeyguardLw(transit, openAnimation);
            }

            public void onAppTransitionCancelledLocked(int transit) {
                PhoneWindowManager.this.handleStartTransitionForKeyguardLw(transit, null);
            }
        });
        this.mKeyguardDelegate = new KeyguardServiceDelegate(this.mContext, new StateCallback() {
            public void onTrustedChanged() {
                PhoneWindowManager.this.mWindowManagerFuncs.notifyKeyguardTrustedChanged();
            }
        });
        EssentialScreenPolicy.init(this.mContext, this.mHandler, windowManagerFuncs);
    }

    private void readConfigurationDependentBehaviors() {
        Resources res = this.mContext.getResources();
        this.mLongPressOnHomeBehavior = res.getInteger(17694798);
        if (this.mLongPressOnHomeBehavior < 0 || this.mLongPressOnHomeBehavior > 2) {
            this.mLongPressOnHomeBehavior = 0;
        }
        this.mDoubleTapOnHomeBehavior = res.getInteger(17694776);
        if (this.mDoubleTapOnHomeBehavior < 0 || this.mDoubleTapOnHomeBehavior > 1) {
            this.mDoubleTapOnHomeBehavior = 0;
        }
        this.mShortPressWindowBehavior = 0;
        if (this.mContext.getPackageManager().hasSystemFeature("android.software.picture_in_picture")) {
            this.mShortPressWindowBehavior = 1;
        }
        this.mNavBarOpacityMode = res.getInteger(17694813);
    }

    public void setInitialDisplaySize(Display display, int width, int height, int density) {
        if (this.mContext != null && display.getDisplayId() == 0) {
            int shortSize;
            int longSize;
            this.mDisplay = display;
            Resources res = this.mContext.getResources();
            if (width > height) {
                shortSize = height;
                longSize = width;
                this.mLandscapeRotation = 0;
                this.mSeascapeRotation = 2;
                if (res.getBoolean(17957001)) {
                    this.mPortraitRotation = 1;
                    this.mUpsideDownRotation = 3;
                } else {
                    this.mPortraitRotation = 3;
                    this.mUpsideDownRotation = 1;
                }
            } else {
                shortSize = width;
                longSize = height;
                this.mPortraitRotation = 0;
                this.mUpsideDownRotation = 2;
                if (res.getBoolean(17957001)) {
                    this.mLandscapeRotation = 3;
                    this.mSeascapeRotation = 1;
                } else {
                    this.mLandscapeRotation = 1;
                    this.mSeascapeRotation = 3;
                }
            }
            int shortSizeDp = (shortSize * 160) / density;
            int longSizeDp = (longSize * 160) / density;
            boolean z = width != height && shortSizeDp < 600;
            this.mNavigationBarCanMove = z;
            this.mHasNavigationBar = res.getBoolean(17957010);
            String navBarOverride = SystemProperties.get("qemu.hw.mainkeys");
            if ("1".equals(navBarOverride)) {
                this.mHasNavigationBar = false;
            } else if ("0".equals(navBarOverride)) {
                this.mHasNavigationBar = true;
            }
            if ("portrait".equals(SystemProperties.get("persist.demo.hdmirotation"))) {
                this.mDemoHdmiRotation = this.mPortraitRotation;
            } else {
                this.mDemoHdmiRotation = this.mLandscapeRotation;
            }
            this.mDemoHdmiRotationLock = SystemProperties.getBoolean("persist.demo.hdmirotationlock", false);
            if ("portrait".equals(SystemProperties.get("persist.demo.remoterotation"))) {
                this.mDemoRotation = this.mPortraitRotation;
            } else {
                this.mDemoRotation = this.mLandscapeRotation;
            }
            this.mDemoRotationLock = SystemProperties.getBoolean("persist.demo.rotationlock", false);
            z = (longSizeDp < 960 || shortSizeDp < 720 || !res.getBoolean(17956967)) ? false : "true".equals(SystemProperties.get("config.override_forced_orient")) ^ 1;
            this.mForceDefaultOrientation = z;
        }
    }

    private boolean canHideNavigationBar() {
        return this.mHasNavigationBar;
    }

    public boolean isDefaultOrientationForced() {
        return this.mForceDefaultOrientation;
    }

    public void setDisplayOverscan(Display display, int left, int top, int right, int bottom) {
        if (display.getDisplayId() == 0) {
            this.mOverscanLeft = left;
            this.mOverscanTop = top;
            this.mOverscanRight = right;
            this.mOverscanBottom = bottom;
        }
    }

    public void updateSettings() {
        int i = 2;
        ContentResolver resolver = this.mContext.getContentResolver();
        boolean updateRotation = false;
        synchronized (this.mLock) {
            int userRotationMode;
            this.mEndcallBehavior = System.getIntForUser(resolver, "end_button_behavior", 2, -2);
            this.mIncallPowerBehavior = Secure.getIntForUser(resolver, "incall_power_button_behavior", 1, -2);
            this.mIncallBackBehavior = Secure.getIntForUser(resolver, "incall_back_button_behavior", 0, -2);
            boolean wakeGestureEnabledSetting = Secure.getIntForUser(resolver, "wake_gesture_enabled", 0, -2) != 0;
            if (this.mWakeGestureEnabledSetting != wakeGestureEnabledSetting) {
                this.mWakeGestureEnabledSetting = wakeGestureEnabledSetting;
                updateWakeGestureListenerLp();
            }
            int userRotation = System.getIntForUser(resolver, "user_rotation", 0, -2);
            if (this.mUserRotation != userRotation) {
                this.mUserRotation = userRotation;
                updateRotation = true;
            }
            if (System.getIntForUser(resolver, "accelerometer_rotation", 0, -2) != 0) {
                userRotationMode = 0;
            } else {
                userRotationMode = 1;
            }
            if (this.mUserRotationMode != userRotationMode) {
                this.mUserRotationMode = userRotationMode;
                updateRotation = true;
                updateOrientationListenerLp();
            }
            if (this.mSystemReady) {
                int pointerLocation = System.getIntForUser(resolver, "pointer_location", 0, -2);
                if (this.mPointerLocationMode != pointerLocation) {
                    this.mPointerLocationMode = pointerLocation;
                    Handler handler = this.mHandler;
                    if (pointerLocation != 0) {
                        i = 1;
                    }
                    handler.sendEmptyMessage(i);
                }
            }
            this.mLockScreenTimeout = System.getIntForUser(resolver, "screen_off_timeout", 0, -2);
            String imId = Secure.getStringForUser(resolver, "default_input_method", -2);
            boolean hasSoftInput = imId != null && imId.length() > 0;
            if (this.mHasSoftInput != hasSoftInput) {
                this.mHasSoftInput = hasSoftInput;
                updateRotation = true;
            }
            if (this.mImmersiveModeConfirmation != null) {
                this.mImmersiveModeConfirmation.loadSetting(this.mCurrentUserId);
            }
        }
        if (updateRotation) {
            updateRotation(true);
        }
    }

    private void updateWakeGestureListenerLp() {
        if (shouldEnableWakeGestureLp()) {
            this.mWakeGestureListener.requestWakeUpTrigger();
        } else {
            this.mWakeGestureListener.cancelWakeUpTrigger();
        }
    }

    private boolean shouldEnableWakeGestureLp() {
        if (!this.mWakeGestureEnabledSetting || (this.mAwake ^ 1) == 0) {
            return false;
        }
        if (this.mLidControlsSleep && this.mLidState == 0) {
            return false;
        }
        return this.mWakeGestureListener.isSupported();
    }

    private void enablePointerLocation() {
        if (this.mPointerLocationView == null) {
            this.mPointerLocationView = new PointerLocationView(this.mContext);
            this.mPointerLocationView.setPrintCoords(false);
            LayoutParams lp = new LayoutParams(-1, -1);
            lp.type = 2015;
            lp.flags = 1304;
            if (ActivityManager.isHighEndGfx()) {
                lp.flags |= 16777216;
                lp.privateFlags |= 2;
            }
            lp.format = -3;
            lp.setTitle("PointerLocation");
            WindowManager wm = (WindowManager) this.mContext.getSystemService("window");
            lp.inputFeatures |= 2;
            wm.addView(this.mPointerLocationView, lp);
            this.mWindowManagerFuncs.registerPointerEventListener(this.mPointerLocationView);
        }
    }

    private void disablePointerLocation() {
        if (this.mPointerLocationView != null) {
            this.mWindowManagerFuncs.unregisterPointerEventListener(this.mPointerLocationView);
            ((WindowManager) this.mContext.getSystemService("window")).removeView(this.mPointerLocationView);
            this.mPointerLocationView = null;
        }
    }

    private int readRotation(int resID) {
        try {
            switch (this.mContext.getResources().getInteger(resID)) {
                case 0:
                    return 0;
                case 90:
                    return 1;
                case 180:
                    return 2;
                case 270:
                    return 3;
            }
        } catch (NotFoundException e) {
        }
        return -1;
    }

    public int checkAddPermission(LayoutParams attrs, int[] outAppOp) {
        int i = 0;
        int type = attrs.type;
        outAppOp[0] = -1;
        if ((type < 1 || type > 99) && ((type < 1000 || type > 1999) && (type < 2000 || type > 2999))) {
            return -10;
        }
        if (type < 2000 || type > 2999) {
            return 0;
        }
        if (LayoutParams.isSystemAlertWindowType(type)) {
            outAppOp[0] = 24;
            int callingUid = Binder.getCallingUid();
            if (UserHandle.getAppId(callingUid) == 1000) {
                return 0;
            }
            ApplicationInfo applicationInfoAsUser;
            try {
                applicationInfoAsUser = this.mContext.getPackageManager().getApplicationInfoAsUser(attrs.packageName, 0, UserHandle.getUserId(callingUid));
            } catch (NameNotFoundException e) {
                applicationInfoAsUser = null;
            }
            if (applicationInfoAsUser == null || (type != 2038 && applicationInfoAsUser.targetSdkVersion >= 26)) {
                if (this.mContext.checkCallingOrSelfPermission("android.permission.INTERNAL_SYSTEM_WINDOW") != 0) {
                    i = -8;
                }
                return i;
            }
            switch (this.mAppOpsManager.checkOpNoThrow(outAppOp[0], callingUid, attrs.packageName)) {
                case 0:
                case 1:
                    return 0;
                case 2:
                    return applicationInfoAsUser.targetSdkVersion < 23 ? 0 : -8;
                default:
                    if (this.mContext.checkCallingOrSelfPermission("android.permission.SYSTEM_ALERT_WINDOW") != 0) {
                        i = -8;
                    }
                    return i;
            }
        }
        switch (type) {
            case 2005:
                outAppOp[0] = 45;
                return 0;
            case 2011:
            case 2013:
            case 2023:
            case 2030:
            case 2031:
            case 2032:
            case 2035:
            case 2037:
                return 0;
            default:
                if (this.mContext.checkCallingOrSelfPermission("android.permission.INTERNAL_SYSTEM_WINDOW") != 0) {
                    i = -8;
                }
                return i;
        }
    }

    public boolean checkShowToOwnerOnly(LayoutParams attrs) {
        boolean z = true;
        switch (attrs.type) {
            case 3:
            case 2000:
            case 2001:
            case 2002:
            case 2007:
            case 2008:
            case 2009:
            case 2014:
            case 2017:
            case 2018:
            case 2019:
            case 2020:
            case 2021:
            case 2022:
            case 2024:
            case 2026:
            case 2027:
            case 2030:
            case 2034:
            case 2037:
                break;
            default:
                if ((attrs.privateFlags & 16) == 0) {
                    return true;
                }
                break;
        }
        if (this.mContext.checkCallingOrSelfPermission("android.permission.INTERNAL_SYSTEM_WINDOW") == 0) {
            z = false;
        }
        return z;
    }

    public void adjustWindowParamsLw(LayoutParams attrs) {
        switch (attrs.type) {
            case 2000:
                if (this.mKeyguardOccluded) {
                    attrs.flags &= -1048577;
                    attrs.privateFlags &= -1025;
                    break;
                }
                break;
            case 2005:
                if (attrs.hideTimeoutMilliseconds < 0 || attrs.hideTimeoutMilliseconds > 3500) {
                    attrs.hideTimeoutMilliseconds = 3500;
                }
                attrs.windowAnimations = 16973828;
                break;
            case 2006:
            case 2015:
                attrs.flags |= 24;
                attrs.flags &= -262145;
                break;
            case 2036:
                attrs.flags |= 8;
                break;
        }
        if (attrs.type != 2000) {
            attrs.privateFlags &= -1025;
        }
        if ((attrs.flags & Integer.MIN_VALUE) != 0) {
            attrs.subtreeSystemUiVisibility |= 512;
        }
        boolean forceWindowDrawsStatusBarBackground = (attrs.privateFlags & DumpState.DUMP_INTENT_FILTER_VERIFIERS) != 0;
        if ((attrs.flags & Integer.MIN_VALUE) != 0 || (forceWindowDrawsStatusBarBackground && attrs.height == -1 && attrs.width == -1)) {
            attrs.subtreeSystemUiVisibility |= 1024;
        }
    }

    void readLidState() {
        this.mLidState = this.mWindowManagerFuncs.getLidState();
    }

    private void readCameraLensCoverState() {
        this.mCameraLensCoverState = this.mWindowManagerFuncs.getCameraLensCoverState();
    }

    private boolean isHidden(int accessibilityMode) {
        boolean z = true;
        switch (accessibilityMode) {
            case 1:
                if (this.mLidState != 0) {
                    z = false;
                }
                return z;
            case 2:
                if (this.mLidState != 1) {
                    z = false;
                }
                return z;
            default:
                return false;
        }
    }

    public void adjustConfigurationLw(Configuration config, int keyboardPresence, int navigationPresence) {
        boolean z = false;
        if ((keyboardPresence & 1) != 0) {
            z = true;
        }
        this.mHaveBuiltInKeyboard = z;
        readConfigurationDependentBehaviors();
        readLidState();
        if (config.keyboard == 1 || (keyboardPresence == 1 && isHidden(this.mLidKeyboardAccessibility))) {
            config.hardKeyboardHidden = 2;
            if (!this.mHasSoftInput) {
                config.keyboardHidden = 2;
            }
        }
        if (config.navigation == 1 || (navigationPresence == 1 && isHidden(this.mLidNavigationAccessibility))) {
            config.navigationHidden = 2;
        }
    }

    public void onConfigurationChanged() {
        Resources res = ActivityThread.currentActivityThread().getSystemUiContext().getResources();
        updateStatusbarHeight(-1);
        int[] iArr = this.mNavigationBarHeightForRotationDefault;
        int i = this.mPortraitRotation;
        int dimensionPixelSize = res.getDimensionPixelSize(17105142);
        this.mNavigationBarHeightForRotationDefault[this.mUpsideDownRotation] = dimensionPixelSize;
        iArr[i] = dimensionPixelSize;
        iArr = this.mNavigationBarHeightForRotationDefault;
        i = this.mLandscapeRotation;
        dimensionPixelSize = res.getDimensionPixelSize(17105144);
        this.mNavigationBarHeightForRotationDefault[this.mSeascapeRotation] = dimensionPixelSize;
        iArr[i] = dimensionPixelSize;
        iArr = this.mNavigationBarWidthForRotationDefault;
        i = this.mPortraitRotation;
        dimensionPixelSize = res.getDimensionPixelSize(17105147);
        this.mNavigationBarWidthForRotationDefault[this.mSeascapeRotation] = dimensionPixelSize;
        this.mNavigationBarWidthForRotationDefault[this.mLandscapeRotation] = dimensionPixelSize;
        this.mNavigationBarWidthForRotationDefault[this.mUpsideDownRotation] = dimensionPixelSize;
        iArr[i] = dimensionPixelSize;
    }

    public int getMaxWallpaperLayer() {
        return getWindowLayerFromTypeLw(2000);
    }

    private int getNavigationBarWidth(int rotation, int uiMode) {
        return this.mNavigationBarWidthForRotationDefault[rotation];
    }

    public void updateStatusbarHeight(int displayRotation) {
        Resources res = ActivityThread.currentActivityThread().getSystemUiContext().getResources();
        this.mStatusBarHeightPortrait = res.getDimensionPixelSize(17105261);
        this.mStatusBarHeightLandscape = res.getDimensionPixelSize(17105260);
        switch (displayRotation) {
            case 0:
            case 2:
                this.mStatusBarHeight = this.mStatusBarHeightPortrait;
                return;
            case 1:
            case 3:
                this.mStatusBarHeight = this.mStatusBarHeightLandscape;
                return;
            default:
                this.mStatusBarHeight = res.getDimensionPixelSize(17105259);
                return;
        }
    }

    public int getNonDecorDisplayWidth(int fullWidth, int fullHeight, int rotation, int uiMode, int displayId) {
        if (displayId == 0 && this.mHasNavigationBar && this.mNavigationBarCanMove && fullWidth > fullHeight) {
            return fullWidth - getNavigationBarWidth(rotation, uiMode);
        }
        return fullWidth;
    }

    private int getNavigationBarHeight(int rotation, int uiMode) {
        return this.mNavigationBarHeightForRotationDefault[rotation];
    }

    public int getNonDecorDisplayHeight(int fullWidth, int fullHeight, int rotation, int uiMode, int displayId) {
        if (displayId == 0 && this.mHasNavigationBar && (!this.mNavigationBarCanMove || fullWidth < fullHeight)) {
            return fullHeight - getNavigationBarHeight(rotation, uiMode);
        }
        return fullHeight;
    }

    public int getConfigDisplayWidth(int fullWidth, int fullHeight, int rotation, int uiMode, int displayId) {
        return getNonDecorDisplayWidth(fullWidth, fullHeight, rotation, uiMode, displayId);
    }

    public int getConfigDisplayHeight(int fullWidth, int fullHeight, int rotation, int uiMode, int displayId) {
        if (displayId == 0) {
            return getNonDecorDisplayHeight(fullWidth, fullHeight, rotation, uiMode, displayId) - this.mStatusBarHeight;
        }
        return fullHeight;
    }

    public boolean isKeyguardHostWindow(LayoutParams attrs) {
        return attrs.type == 2000;
    }

    public boolean canBeHiddenByKeyguardLw(WindowState win) {
        boolean z = false;
        switch (win.getAttrs().type) {
            case 2000:
            case 2013:
            case 2019:
            case 2023:
                return false;
            default:
                if (win == this.mLetterBoxBar) {
                    return false;
                }
                if (getWindowLayerLw(win) < getWindowLayerFromTypeLw(2000)) {
                    z = true;
                }
                return z;
        }
    }

    private boolean shouldBeHiddenByKeyguard(WindowState win, WindowState imeTarget) {
        boolean z = true;
        int i = 0;
        if (win.getAppToken() != null) {
            return false;
        }
        int showImeOverKeyguard;
        int i2;
        boolean hideDockDivider;
        LayoutParams attrs = win.getAttrs();
        if (imeTarget == null || !imeTarget.isVisibleLw()) {
            showImeOverKeyguard = 0;
        } else if ((imeTarget.getAttrs().flags & DumpState.DUMP_FROZEN) == 0) {
            showImeOverKeyguard = canBeHiddenByKeyguardLw(imeTarget) ^ 1;
        } else {
            showImeOverKeyguard = 1;
        }
        if (win.isInputMethodWindow() || imeTarget == this) {
            i2 = showImeOverKeyguard;
        } else {
            i2 = 0;
        }
        if (isKeyguardLocked() && isKeyguardOccluded()) {
            if ((attrs.flags & DumpState.DUMP_FROZEN) != 0) {
                i = 1;
            } else if ((attrs.privateFlags & 256) != 0) {
                i = 1;
            }
            i2 |= i;
        }
        boolean keyguardLocked = isKeyguardLocked();
        if (attrs.type == 2034) {
            hideDockDivider = this.mWindowManagerInternal.isStackVisible(3) ^ 1;
        } else {
            hideDockDivider = false;
        }
        if (!(keyguardLocked && (r0 ^ 1) != 0 && win.getDisplayId() == 0)) {
            z = hideDockDivider;
        }
        return z;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.WindowManagerPolicy.StartingSurface addSplashScreen(android.os.IBinder r19, java.lang.String r20, int r21, android.content.res.CompatibilityInfo r22, java.lang.CharSequence r23, int r24, int r25, int r26, int r27, android.content.res.Configuration r28, int r29) {
        /*
        r18 = this;
        if (r20 != 0) goto L_0x0004;
    L_0x0002:
        r15 = 0;
        return r15;
    L_0x0004:
        r14 = 0;
        r12 = 0;
        r0 = r18;
        r2 = r0.mContext;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r18;
        r1 = r29;
        r3 = r0.getDisplayContext(r2, r1);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        if (r3 != 0) goto L_0x0016;
    L_0x0014:
        r15 = 0;
        return r15;
    L_0x0016:
        r2 = r3;
        r15 = r3.getThemeResId();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r21;
        if (r0 != r15) goto L_0x0021;
    L_0x001f:
        if (r24 == 0) goto L_0x002d;
    L_0x0021:
        r15 = 4;
        r0 = r20;
        r2 = r3.createPackageContext(r0, r15);	 Catch:{ NameNotFoundException -> 0x01da }
        r0 = r21;
        r2.setTheme(r0);	 Catch:{ NameNotFoundException -> 0x01da }
    L_0x002d:
        if (r28 == 0) goto L_0x0061;
    L_0x002f:
        r15 = android.content.res.Configuration.EMPTY;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r28;
        r15 = r0.equals(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r15 ^ 1;
        if (r15 == 0) goto L_0x0061;
    L_0x003b:
        r0 = r28;
        r8 = r2.createConfigurationContext(r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r21;
        r8.setTheme(r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = com.android.internal.R.styleable.Window;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r11 = r8.obtainStyledAttributes(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = 1;
        r16 = 0;
        r0 = r16;
        r10 = r11.getResourceId(r15, r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        if (r10 == 0) goto L_0x005e;
    L_0x0057:
        r15 = r8.getDrawable(r10);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        if (r15 == 0) goto L_0x005e;
    L_0x005d:
        r2 = r8;
    L_0x005e:
        r11.recycle();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
    L_0x0061:
        r13 = new com.android.internal.policy.PhoneWindow;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r13.<init>(r2);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = 1;
        r13.setIsStartingWindow(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r2.getResources();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r16 = 0;
        r0 = r24;
        r1 = r16;
        r7 = r15.getText(r0, r1);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        if (r7 == 0) goto L_0x0143;
    L_0x007a:
        r15 = 1;
        r13.setTitle(r7, r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
    L_0x007e:
        r15 = 3;
        r13.setType(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r18;
        r15 = r0.mWindowManagerFuncs;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r15.getWindowManagerLock();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        monitor-enter(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r18;
        r0 = r0.mKeyguardOccluded;	 Catch:{ all -> 0x0188 }
        r16 = r0;
        if (r16 == 0) goto L_0x0097;
    L_0x0093:
        r16 = 524288; // 0x80000 float:7.34684E-40 double:2.590327E-318;
        r27 = r27 | r16;
    L_0x0097:
        monitor-exit(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r27 | 16;
        r15 = r15 | 8;
        r16 = 131072; // 0x20000 float:1.83671E-40 double:6.47582E-319;
        r15 = r15 | r16;
        r16 = r27 | 16;
        r16 = r16 | 8;
        r17 = 131072; // 0x20000 float:1.83671E-40 double:6.47582E-319;
        r16 = r16 | r17;
        r0 = r16;
        r13.setFlags(r15, r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r25;
        r13.setDefaultIcon(r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r26;
        r13.setDefaultLogo(r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = -1;
        r16 = -1;
        r0 = r16;
        r13.setLayout(r15, r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r9 = r13.getAttributes();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r19;
        r9.token = r0;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r20;
        r9.packageName = r0;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r13.getWindowStyle();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r16 = 8;
        r17 = 0;
        r15 = r15.getResourceId(r16, r17);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r9.windowAnimations = r15;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r9.privateFlags;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r15 | 1;
        r9.privateFlags = r15;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r9.privateFlags;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r15 | 16;
        r9.privateFlags = r15;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r22.supportsScreen();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        if (r15 != 0) goto L_0x00f1;
    L_0x00eb:
        r15 = r9.privateFlags;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r15 | 128;
        r9.privateFlags = r15;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
    L_0x00f1:
        r15 = new java.lang.StringBuilder;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15.<init>();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r16 = "Splash Screen ";
        r15 = r15.append(r16);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r20;
        r15 = r15.append(r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r15.toString();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r9.setTitle(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r18;
        r0.addSplashscreenContent(r13, r2);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = "window";
        r15 = r2.getSystemService(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r15;
        r0 = (android.view.WindowManager) r0;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r14 = r0;
        r12 = r13.getDecorView();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r14.addView(r12, r9);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r15 = r12.getParent();	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        if (r15 == 0) goto L_0x01c1;
    L_0x0127:
        r15 = new com.android.server.policy.SplashScreenSurface;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        r0 = r19;
        r15.<init>(r12, r0);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
    L_0x012e:
        if (r12 == 0) goto L_0x0142;
    L_0x0130:
        r16 = r12.getParent();
        if (r16 != 0) goto L_0x0142;
    L_0x0136:
        r16 = "WindowManager";
        r17 = "view not successfully added to wm, removing view";
        android.util.Log.w(r16, r17);
        r14.removeViewImmediate(r12);
    L_0x0142:
        return r15;
    L_0x0143:
        r15 = 0;
        r0 = r23;
        r13.setTitle(r0, r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        goto L_0x007e;
    L_0x014b:
        r5 = move-exception;
        r15 = "WindowManager";
        r16 = new java.lang.StringBuilder;	 Catch:{ all -> 0x01c4 }
        r16.<init>();	 Catch:{ all -> 0x01c4 }
        r0 = r16;
        r1 = r19;
        r16 = r0.append(r1);	 Catch:{ all -> 0x01c4 }
        r17 = " already running, starting window not displayed. ";
        r16 = r16.append(r17);	 Catch:{ all -> 0x01c4 }
        r17 = r5.getMessage();	 Catch:{ all -> 0x01c4 }
        r16 = r16.append(r17);	 Catch:{ all -> 0x01c4 }
        r16 = r16.toString();	 Catch:{ all -> 0x01c4 }
        android.util.Log.w(r15, r16);	 Catch:{ all -> 0x01c4 }
        if (r12 == 0) goto L_0x0186;
    L_0x0174:
        r15 = r12.getParent();
        if (r15 != 0) goto L_0x0186;
    L_0x017a:
        r15 = "WindowManager";
        r16 = "view not successfully added to wm, removing view";
        android.util.Log.w(r15, r16);
        r14.removeViewImmediate(r12);
    L_0x0186:
        r15 = 0;
        return r15;
    L_0x0188:
        r16 = move-exception;
        monitor-exit(r15);	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
        throw r16;	 Catch:{ BadTokenException -> 0x014b, RuntimeException -> 0x018b }
    L_0x018b:
        r6 = move-exception;
        r15 = "WindowManager";
        r16 = new java.lang.StringBuilder;	 Catch:{ all -> 0x01c4 }
        r16.<init>();	 Catch:{ all -> 0x01c4 }
        r0 = r16;
        r1 = r19;
        r16 = r0.append(r1);	 Catch:{ all -> 0x01c4 }
        r17 = " failed creating starting window";
        r16 = r16.append(r17);	 Catch:{ all -> 0x01c4 }
        r16 = r16.toString();	 Catch:{ all -> 0x01c4 }
        r0 = r16;
        android.util.Log.w(r15, r0, r6);	 Catch:{ all -> 0x01c4 }
        if (r12 == 0) goto L_0x0186;
    L_0x01ae:
        r15 = r12.getParent();
        if (r15 != 0) goto L_0x0186;
    L_0x01b4:
        r15 = "WindowManager";
        r16 = "view not successfully added to wm, removing view";
        android.util.Log.w(r15, r16);
        r14.removeViewImmediate(r12);
        goto L_0x0186;
    L_0x01c1:
        r15 = 0;
        goto L_0x012e;
    L_0x01c4:
        r15 = move-exception;
        if (r12 == 0) goto L_0x01d9;
    L_0x01c7:
        r16 = r12.getParent();
        if (r16 != 0) goto L_0x01d9;
    L_0x01cd:
        r16 = "WindowManager";
        r17 = "view not successfully added to wm, removing view";
        android.util.Log.w(r16, r17);
        r14.removeViewImmediate(r12);
    L_0x01d9:
        throw r15;
    L_0x01da:
        r4 = move-exception;
        goto L_0x002d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.addSplashScreen(android.os.IBinder, java.lang.String, int, android.content.res.CompatibilityInfo, java.lang.CharSequence, int, int, int, int, android.content.res.Configuration, int):android.view.WindowManagerPolicy$StartingSurface");
    }

    private void addSplashscreenContent(PhoneWindow win, Context ctx) {
        TypedArray a = ctx.obtainStyledAttributes(R.styleable.Window);
        int resId = a.getResourceId(48, 0);
        a.recycle();
        if (resId != 0) {
            Drawable drawable = ctx.getDrawable(resId);
            if (drawable != null) {
                View v = new View(ctx);
                v.setBackground(drawable);
                win.setContentView(v);
            }
        }
    }

    private Context getDisplayContext(Context context, int displayId) {
        if (displayId == 0) {
            return context;
        }
        Display targetDisplay = ((DisplayManager) context.getSystemService("display")).getDisplay(displayId);
        if (targetDisplay == null) {
            return null;
        }
        return context.createDisplayContext(targetDisplay);
    }

    public int prepareAddWindowLw(WindowState win, LayoutParams attrs) {
        switch (attrs.type) {
            case 2000:
                this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "PhoneWindowManager");
                if (this.mStatusBar == null || !this.mStatusBar.isAlive()) {
                    this.mStatusBar = win;
                    this.mStatusBarController.setWindow(win);
                    setKeyguardOccludedLw(this.mKeyguardOccluded, true);
                    break;
                }
                return -7;
            case 2014:
            case 2017:
            case 2024:
            case 2033:
                this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "PhoneWindowManager");
                if (attrs != null && attrs.getTitle().equals(this.mContext.getString(17040077))) {
                    if (this.mLetterBoxBar == null || !this.mLetterBoxBar.isAlive()) {
                        this.mLetterBoxBar = win;
                        break;
                    }
                    return -7;
                }
                break;
            case 2019:
                this.mContext.enforceCallingOrSelfPermission("android.permission.STATUS_BAR_SERVICE", "PhoneWindowManager");
                if (this.mNavigationBar == null || !this.mNavigationBar.isAlive()) {
                    this.mNavigationBar = win;
                    this.mNavigationBarController.setWindow(win);
                    this.mNavigationBarController.setOnBarVisibilityChangedListener(this.mNavBarVisibilityListener, true);
                    break;
                }
                return -7;
        }
        return 0;
    }

    public void removeWindowLw(WindowState win) {
        if (this.mStatusBar == win) {
            this.mStatusBar = null;
            this.mStatusBarController.setWindow(null);
        } else if (this.mNavigationBar == win) {
            this.mNavigationBar = null;
            this.mNavigationBarController.setWindow(null);
        }
    }

    public int selectAnimationLw(WindowState win, int transit) {
        if (win == this.mStatusBar) {
            boolean isKeyguard = (win.getAttrs().privateFlags & 1024) != 0;
            boolean expanded = win.getAttrs().height == -1 ? win.getAttrs().width == -1 : false;
            if (isKeyguard || expanded) {
                return -1;
            }
            if (transit == 2 || transit == 4) {
                return 17432619;
            }
            if (transit == 1 || transit == 3) {
                return 17432618;
            }
        } else if (win == this.mNavigationBar) {
            if (win.getAttrs().windowAnimations != 0) {
                return 0;
            }
            if (this.mNavigationBarPosition == 4) {
                if (transit == 2 || transit == 4) {
                    if (isKeyguardShowingAndNotOccluded()) {
                        return 17432613;
                    }
                    return 17432612;
                } else if (transit == 1 || transit == 3) {
                    return 17432611;
                }
            } else if (this.mNavigationBarPosition == 2) {
                if (transit == 2 || transit == 4) {
                    return 17432617;
                }
                if (transit == 1 || transit == 3) {
                    return 17432616;
                }
            } else if (this.mNavigationBarPosition == 1) {
                if (transit == 2 || transit == 4) {
                    return 17432615;
                }
                if (transit == 1 || transit == 3) {
                    return 17432614;
                }
            }
        } else if (win.getAttrs().type == 2034) {
            return selectDockedDividerAnimationLw(win, transit);
        }
        if (transit != 5) {
            return (win.getAttrs().type == 2023 && this.mDreamingLockscreen && transit == 1) ? -1 : 0;
        } else {
            if (win.hasAppShownWindows()) {
                return 17432593;
            }
        }
    }

    private int selectDockedDividerAnimationLw(WindowState win, int transit) {
        int insets = this.mWindowManagerFuncs.getDockedDividerInsetsLw();
        Rect frame = win.getFrameLw();
        boolean behindNavBar = this.mNavigationBar != null ? ((this.mNavigationBarPosition != 4 || frame.top + insets < this.mNavigationBar.getFrameLw().top) && (this.mNavigationBarPosition != 2 || frame.left + insets < this.mNavigationBar.getFrameLw().left)) ? this.mNavigationBarPosition == 1 ? frame.right - insets <= this.mNavigationBar.getFrameLw().right : false : true : false;
        boolean landscape = frame.height() > frame.width();
        boolean offscreenLandscape = landscape ? frame.right - insets > 0 ? frame.left + insets >= win.getDisplayFrameLw().right : true : false;
        boolean offscreenPortrait = !landscape ? frame.top - insets > 0 ? frame.bottom + insets >= win.getDisplayFrameLw().bottom : true : false;
        boolean z = !offscreenLandscape ? offscreenPortrait : true;
        if (behindNavBar || z) {
            return 0;
        }
        if (transit == 1 || transit == 3) {
            return 17432576;
        }
        if (transit == 2) {
            return 17432577;
        }
        return 0;
    }

    public void selectRotationAnimationLw(int[] anim) {
        if ((this.mScreenOnFully ? okToAnimate() ^ 1 : 1) != 0) {
            anim[0] = 17432685;
            anim[1] = 17432684;
            return;
        }
        if (this.mTopFullscreenOpaqueWindowState != null) {
            int animationHint = this.mTopFullscreenOpaqueWindowState.getRotationAnimationHint();
            if (animationHint < 0 && this.mTopIsFullscreen) {
                animationHint = this.mTopFullscreenOpaqueWindowState.getAttrs().rotationAnimation;
            }
            switch (animationHint) {
                case 1:
                case 3:
                    anim[0] = 17432686;
                    anim[1] = 17432684;
                    break;
                case 2:
                    anim[0] = 17432685;
                    anim[1] = 17432684;
                    break;
                default:
                    anim[1] = 0;
                    anim[0] = 0;
                    break;
            }
        }
        anim[1] = 0;
        anim[0] = 0;
    }

    public boolean validateRotationAnimationLw(int exitAnimId, int enterAnimId, boolean forceDefault) {
        boolean z = true;
        switch (exitAnimId) {
            case 17432685:
            case 17432686:
                if (forceDefault) {
                    return false;
                }
                int[] anim = new int[2];
                selectRotationAnimationLw(anim);
                if (!(exitAnimId == anim[0] && enterAnimId == anim[1])) {
                    z = false;
                }
                return z;
            default:
                return true;
        }
    }

    public Animation createHiddenByKeyguardExit(boolean onWallpaper, boolean goingToNotificationShade) {
        if (goingToNotificationShade) {
            return AnimationUtils.loadAnimation(this.mContext, 17432661);
        }
        int i;
        Context context = this.mContext;
        if (onWallpaper) {
            i = 17432662;
        } else {
            i = 17432660;
        }
        AnimationSet set = (AnimationSet) AnimationUtils.loadAnimation(context, i);
        List<Animation> animations = set.getAnimations();
        for (int i2 = animations.size() - 1; i2 >= 0; i2--) {
            ((Animation) animations.get(i2)).setInterpolator(this.mLogDecelerateInterpolator);
        }
        return set;
    }

    public Animation createKeyguardWallpaperExit(boolean goingToNotificationShade) {
        if (goingToNotificationShade) {
            return null;
        }
        return AnimationUtils.loadAnimation(this.mContext, 17432665);
    }

    private static void awakenDreams() {
        IDreamManager dreamManager = getDreamManager();
        if (dreamManager != null) {
            try {
                dreamManager.awaken();
            } catch (RemoteException e) {
            }
        }
    }

    static IDreamManager getDreamManager() {
        return IDreamManager.Stub.asInterface(ServiceManager.checkService("dreams"));
    }

    TelecomManager getTelecommService() {
        return (TelecomManager) this.mContext.getSystemService("telecom");
    }

    static IAudioService getAudioService() {
        IAudioService audioService = IAudioService.Stub.asInterface(ServiceManager.checkService("audio"));
        if (audioService == null) {
            Log.w(TAG, "Unable to find IAudioService interface.");
        }
        return audioService;
    }

    boolean keyguardOn() {
        return !isKeyguardShowingAndNotOccluded() ? inKeyguardRestrictedKeyInputMode() : true;
    }

    public long interceptKeyBeforeDispatching(android.view.WindowManagerPolicy.WindowState r53, android.view.KeyEvent r54, int r55) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r51_0 'voiceIntent' android.content.Intent) in PHI: PHI: (r51_1 'voiceIntent' android.content.Intent) = (r51_0 'voiceIntent' android.content.Intent), (r51_2 'voiceIntent' android.content.Intent) binds: {(r51_0 'voiceIntent' android.content.Intent)=B:209:0x031e, (r51_2 'voiceIntent' android.content.Intent)=B:215:0x0348}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:79)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:60)
	at jadx.core.ProcessClass.process(ProcessClass.java:39)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r52 = this;
        r31 = r52.keyguardOn();
        r30 = r54.getKeyCode();
        r37 = r54.getRepeatCount();
        r34 = r54.getMetaState();
        r26 = r54.getFlags();
        r4 = r54.getAction();
        if (r4 != 0) goto L_0x004d;
    L_0x001a:
        r23 = 1;
    L_0x001c:
        r18 = r54.isCanceled();
        r0 = r52;
        r4 = r0.mScreenshotChordEnabled;
        if (r4 == 0) goto L_0x0066;
    L_0x0026:
        r0 = r26;
        r4 = r0 & 1024;
        if (r4 != 0) goto L_0x0066;
    L_0x002c:
        r0 = r52;
        r4 = r0.mScreenshotChordVolumeDownKeyTriggered;
        if (r4 == 0) goto L_0x0050;
    L_0x0032:
        r0 = r52;
        r4 = r0.mScreenshotChordPowerKeyTriggered;
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x0050;
    L_0x003a:
        r38 = android.os.SystemClock.uptimeMillis();
        r0 = r52;
        r6 = r0.mScreenshotChordVolumeDownKeyTime;
        r8 = 150; // 0x96 float:2.1E-43 double:7.4E-322;
        r48 = r6 + r8;
        r4 = (r38 > r48 ? 1 : (r38 == r48 ? 0 : -1));
        if (r4 >= 0) goto L_0x0050;
    L_0x004a:
        r6 = r48 - r38;
        return r6;
    L_0x004d:
        r23 = 0;
        goto L_0x001c;
    L_0x0050:
        r4 = 25;
        r0 = r30;
        if (r0 != r4) goto L_0x0066;
    L_0x0056:
        r0 = r52;
        r4 = r0.mScreenshotChordVolumeDownKeyConsumed;
        if (r4 == 0) goto L_0x0066;
    L_0x005c:
        if (r23 != 0) goto L_0x0063;
    L_0x005e:
        r4 = 0;
        r0 = r52;
        r0.mScreenshotChordVolumeDownKeyConsumed = r4;
    L_0x0063:
        r6 = -1;
        return r6;
    L_0x0066:
        r0 = r52;
        r4 = r0.mAccessibilityShortcutController;
        r6 = 0;
        r4 = r4.isAccessibilityShortcutAvailable(r6);
        if (r4 == 0) goto L_0x00cc;
    L_0x0071:
        r0 = r26;
        r4 = r0 & 1024;
        if (r4 != 0) goto L_0x00cc;
    L_0x0077:
        r0 = r52;
        r4 = r0.mScreenshotChordVolumeDownKeyTriggered;
        r0 = r52;
        r6 = r0.mA11yShortcutChordVolumeUpKeyTriggered;
        r4 = r4 ^ r6;
        if (r4 == 0) goto L_0x00a0;
    L_0x0082:
        r38 = android.os.SystemClock.uptimeMillis();
        r0 = r52;
        r4 = r0.mScreenshotChordVolumeDownKeyTriggered;
        if (r4 == 0) goto L_0x009b;
    L_0x008c:
        r0 = r52;
        r6 = r0.mScreenshotChordVolumeDownKeyTime;
    L_0x0090:
        r8 = 150; // 0x96 float:2.1E-43 double:7.4E-322;
        r48 = r6 + r8;
        r4 = (r38 > r48 ? 1 : (r38 == r48 ? 0 : -1));
        if (r4 >= 0) goto L_0x00a0;
    L_0x0098:
        r6 = r48 - r38;
        return r6;
    L_0x009b:
        r0 = r52;
        r6 = r0.mA11yShortcutChordVolumeUpKeyTime;
        goto L_0x0090;
    L_0x00a0:
        r4 = 25;
        r0 = r30;
        if (r0 != r4) goto L_0x00b6;
    L_0x00a6:
        r0 = r52;
        r4 = r0.mScreenshotChordVolumeDownKeyConsumed;
        if (r4 == 0) goto L_0x00b6;
    L_0x00ac:
        if (r23 != 0) goto L_0x00b3;
    L_0x00ae:
        r4 = 0;
        r0 = r52;
        r0.mScreenshotChordVolumeDownKeyConsumed = r4;
    L_0x00b3:
        r6 = -1;
        return r6;
    L_0x00b6:
        r4 = 24;
        r0 = r30;
        if (r0 != r4) goto L_0x00cc;
    L_0x00bc:
        r0 = r52;
        r4 = r0.mA11yShortcutChordVolumeUpKeyConsumed;
        if (r4 == 0) goto L_0x00cc;
    L_0x00c2:
        if (r23 != 0) goto L_0x00c9;
    L_0x00c4:
        r4 = 0;
        r0 = r52;
        r0.mA11yShortcutChordVolumeUpKeyConsumed = r4;
    L_0x00c9:
        r6 = -1;
        return r6;
    L_0x00cc:
        r0 = r52;
        r4 = r0.mPendingMetaAction;
        if (r4 == 0) goto L_0x00df;
    L_0x00d2:
        r4 = android.view.KeyEvent.isMetaKey(r30);
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x00df;
    L_0x00da:
        r4 = 0;
        r0 = r52;
        r0.mPendingMetaAction = r4;
    L_0x00df:
        r0 = r52;
        r4 = r0.mPendingCapsLockToggle;
        if (r4 == 0) goto L_0x00fa;
    L_0x00e5:
        r4 = android.view.KeyEvent.isMetaKey(r30);
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x00fa;
    L_0x00ed:
        r4 = android.view.KeyEvent.isAltKey(r30);
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x00fa;
    L_0x00f5:
        r4 = 0;
        r0 = r52;
        r0.mPendingCapsLockToggle = r4;
    L_0x00fa:
        r4 = 3;
        r0 = r30;
        if (r0 != r4) goto L_0x01ce;
    L_0x00ff:
        if (r23 != 0) goto L_0x0154;
    L_0x0101:
        r52.cancelPreloadRecentApps();
        r4 = 0;
        r0 = r52;
        r0.mHomePressed = r4;
        r0 = r52;
        r4 = r0.mHomeConsumed;
        if (r4 == 0) goto L_0x0117;
    L_0x010f:
        r4 = 0;
        r0 = r52;
        r0.mHomeConsumed = r4;
        r6 = -1;
        return r6;
    L_0x0117:
        if (r18 == 0) goto L_0x0125;
    L_0x0119:
        r4 = "WindowManager";
        r6 = "Ignoring HOME; event canceled.";
        android.util.Log.i(r4, r6);
        r6 = -1;
        return r6;
    L_0x0125:
        r0 = r52;
        r4 = r0.mDoubleTapOnHomeBehavior;
        if (r4 == 0) goto L_0x014e;
    L_0x012b:
        r0 = r52;
        r4 = r0.mHandler;
        r0 = r52;
        r6 = r0.mHomeDoubleTapTimeoutRunnable;
        r4.removeCallbacks(r6);
        r4 = 1;
        r0 = r52;
        r0.mHomeDoubleTapPending = r4;
        r0 = r52;
        r4 = r0.mHandler;
        r0 = r52;
        r6 = r0.mHomeDoubleTapTimeoutRunnable;
        r7 = android.view.ViewConfiguration.getDoubleTapTimeout();
        r8 = (long) r7;
        r4.postDelayed(r6, r8);
        r6 = -1;
        return r6;
    L_0x014e:
        r52.handleShortPressOnHome();
        r6 = -1;
        return r6;
    L_0x0154:
        if (r53 == 0) goto L_0x016f;
    L_0x0156:
        r15 = r53.getAttrs();
    L_0x015a:
        if (r15 == 0) goto L_0x018c;
    L_0x015c:
        r0 = r15.type;
        r47 = r0;
        r4 = 2009; // 0x7d9 float:2.815E-42 double:9.926E-321;
        r0 = r47;
        if (r0 == r4) goto L_0x016c;
    L_0x0166:
        r4 = r15.privateFlags;
        r4 = r4 & 1024;
        if (r4 == 0) goto L_0x0171;
    L_0x016c:
        r6 = 0;
        return r6;
    L_0x016f:
        r15 = 0;
        goto L_0x015a;
    L_0x0171:
        r4 = WINDOW_TYPES_WHERE_HOME_DOESNT_WORK;
        r0 = r4.length;
        r50 = r0;
        r28 = 0;
    L_0x0178:
        r0 = r28;
        r1 = r50;
        if (r0 >= r1) goto L_0x018c;
    L_0x017e:
        r4 = WINDOW_TYPES_WHERE_HOME_DOESNT_WORK;
        r4 = r4[r28];
        r0 = r47;
        if (r0 != r4) goto L_0x0189;
    L_0x0186:
        r6 = -1;
        return r6;
    L_0x0189:
        r28 = r28 + 1;
        goto L_0x0178;
    L_0x018c:
        if (r37 != 0) goto L_0x01ba;
    L_0x018e:
        r4 = 1;
        r0 = r52;
        r0.mHomePressed = r4;
        r0 = r52;
        r4 = r0.mHomeDoubleTapPending;
        if (r4 == 0) goto L_0x01af;
    L_0x0199:
        r4 = 0;
        r0 = r52;
        r0.mHomeDoubleTapPending = r4;
        r0 = r52;
        r4 = r0.mHandler;
        r0 = r52;
        r6 = r0.mHomeDoubleTapTimeoutRunnable;
        r4.removeCallbacks(r6);
        r52.handleDoubleTapOnHome();
    L_0x01ac:
        r6 = -1;
        return r6;
    L_0x01af:
        r0 = r52;
        r4 = r0.mDoubleTapOnHomeBehavior;
        r6 = 1;
        if (r4 != r6) goto L_0x01ac;
    L_0x01b6:
        r52.preloadRecentApps();
        goto L_0x01ac;
    L_0x01ba:
        r4 = r54.getFlags();
        r4 = r4 & 128;
        if (r4 == 0) goto L_0x01ac;
    L_0x01c2:
        if (r31 != 0) goto L_0x01ac;
    L_0x01c4:
        r4 = r54.getDeviceId();
        r0 = r52;
        r0.handleLongPressOnHome(r4);
        goto L_0x01ac;
    L_0x01ce:
        r4 = 82;
        r0 = r30;
        if (r0 != r4) goto L_0x01ff;
    L_0x01d4:
        r20 = 1;
        if (r23 == 0) goto L_0x0258;
    L_0x01d8:
        if (r37 != 0) goto L_0x0258;
    L_0x01da:
        r0 = r52;
        r4 = r0.mEnableShiftMenuBugReports;
        if (r4 == 0) goto L_0x0258;
    L_0x01e0:
        r4 = r34 & 1;
        r6 = 1;
        if (r4 != r6) goto L_0x0258;
    L_0x01e5:
        r5 = new android.content.Intent;
        r4 = "android.intent.action.BUG_REPORT";
        r5.<init>(r4);
        r0 = r52;
        r4 = r0.mContext;
        r6 = android.os.UserHandle.CURRENT;
        r7 = 0;
        r8 = 0;
        r9 = 0;
        r10 = 0;
        r11 = 0;
        r12 = 0;
        r4.sendOrderedBroadcastAsUser(r5, r6, r7, r8, r9, r10, r11, r12);
        r6 = -1;
        return r6;
    L_0x01ff:
        r4 = 84;
        r0 = r30;
        if (r0 != r4) goto L_0x0229;
    L_0x0205:
        if (r23 == 0) goto L_0x0216;
    L_0x0207:
        if (r37 != 0) goto L_0x0213;
    L_0x0209:
        r4 = 1;
        r0 = r52;
        r0.mSearchKeyShortcutPending = r4;
        r4 = 0;
        r0 = r52;
        r0.mConsumeSearchKeyUp = r4;
    L_0x0213:
        r6 = 0;
        return r6;
    L_0x0216:
        r4 = 0;
        r0 = r52;
        r0.mSearchKeyShortcutPending = r4;
        r0 = r52;
        r4 = r0.mConsumeSearchKeyUp;
        if (r4 == 0) goto L_0x0213;
    L_0x0221:
        r4 = 0;
        r0 = r52;
        r0.mConsumeSearchKeyUp = r4;
        r6 = -1;
        return r6;
    L_0x0229:
        r4 = 187; // 0xbb float:2.62E-43 double:9.24E-322;
        r0 = r30;
        if (r0 != r4) goto L_0x0241;
    L_0x022f:
        if (r31 != 0) goto L_0x0238;
    L_0x0231:
        if (r23 == 0) goto L_0x023b;
    L_0x0233:
        if (r37 != 0) goto L_0x023b;
    L_0x0235:
        r52.preloadRecentApps();
    L_0x0238:
        r6 = -1;
        return r6;
    L_0x023b:
        if (r23 != 0) goto L_0x0238;
    L_0x023d:
        r52.toggleRecentApps();
        goto L_0x0238;
    L_0x0241:
        r4 = 42;
        r0 = r30;
        if (r0 != r4) goto L_0x027f;
    L_0x0247:
        r4 = r54.isMetaPressed();
        if (r4 == 0) goto L_0x027f;
    L_0x024d:
        if (r23 == 0) goto L_0x0258;
    L_0x024f:
        r40 = r52.getStatusBarService();
        if (r40 == 0) goto L_0x0258;
    L_0x0255:
        r40.expandNotificationsPanel();	 Catch:{ RemoteException -> 0x027d }
    L_0x0258:
        r13 = 0;
        r4 = android.view.KeyEvent.isModifierKey(r30);
        if (r4 == 0) goto L_0x0272;
    L_0x025f:
        r0 = r52;
        r4 = r0.mPendingCapsLockToggle;
        if (r4 != 0) goto L_0x04b1;
    L_0x0265:
        r0 = r52;
        r4 = r0.mMetaState;
        r0 = r52;
        r0.mInitialMetaState = r4;
        r4 = 1;
        r0 = r52;
        r0.mPendingCapsLockToggle = r4;
    L_0x0272:
        r0 = r34;
        r1 = r52;
        r1.mMetaState = r0;
        if (r13 == 0) goto L_0x04ea;
    L_0x027a:
        r6 = -1;
        return r6;
    L_0x027d:
        r24 = move-exception;
        goto L_0x0258;
    L_0x027f:
        r4 = 47;
        r0 = r30;
        if (r0 != r4) goto L_0x02b7;
    L_0x0285:
        r4 = r54.isMetaPressed();
        if (r4 == 0) goto L_0x02b7;
    L_0x028b:
        r4 = r54.isCtrlPressed();
        if (r4 == 0) goto L_0x02b7;
    L_0x0291:
        if (r23 == 0) goto L_0x0258;
    L_0x0293:
        if (r37 != 0) goto L_0x0258;
    L_0x0295:
        r4 = r54.isShiftPressed();
        if (r4 == 0) goto L_0x02b4;
    L_0x029b:
        r47 = 2;
    L_0x029d:
        r0 = r52;
        r4 = r0.mScreenshotRunnable;
        r0 = r47;
        r4.setScreenshotType(r0);
        r0 = r52;
        r4 = r0.mHandler;
        r0 = r52;
        r6 = r0.mScreenshotRunnable;
        r4.post(r6);
        r6 = -1;
        return r6;
    L_0x02b4:
        r47 = 1;
        goto L_0x029d;
    L_0x02b7:
        r4 = 76;
        r0 = r30;
        if (r0 != r4) goto L_0x02d9;
    L_0x02bd:
        r4 = r54.isMetaPressed();
        if (r4 == 0) goto L_0x02d9;
    L_0x02c3:
        if (r23 == 0) goto L_0x0258;
    L_0x02c5:
        if (r37 != 0) goto L_0x0258;
    L_0x02c7:
        r4 = r52.isKeyguardLocked();
        r4 = r4 ^ 1;
        if (r4 == 0) goto L_0x0258;
    L_0x02cf:
        r4 = r54.getDeviceId();
        r0 = r52;
        r0.toggleKeyboardShortcutsMenu(r4);
        goto L_0x0258;
    L_0x02d9:
        r4 = 219; // 0xdb float:3.07E-43 double:1.08E-321;
        r0 = r30;
        if (r0 != r4) goto L_0x0314;
    L_0x02df:
        if (r23 == 0) goto L_0x02fb;
    L_0x02e1:
        if (r37 != 0) goto L_0x02eb;
    L_0x02e3:
        r4 = 0;
        r0 = r52;
        r0.mAssistKeyLongPressed = r4;
    L_0x02e8:
        r6 = -1;
        return r6;
    L_0x02eb:
        r4 = 1;
        r0 = r37;
        if (r0 != r4) goto L_0x02e8;
    L_0x02f0:
        r4 = 1;
        r0 = r52;
        r0.mAssistKeyLongPressed = r4;
        if (r31 != 0) goto L_0x02e8;
    L_0x02f7:
        r52.launchAssistLongPressAction();
        goto L_0x02e8;
    L_0x02fb:
        r0 = r52;
        r4 = r0.mAssistKeyLongPressed;
        if (r4 == 0) goto L_0x0307;
    L_0x0301:
        r4 = 0;
        r0 = r52;
        r0.mAssistKeyLongPressed = r4;
        goto L_0x02e8;
    L_0x0307:
        if (r31 != 0) goto L_0x02e8;
    L_0x0309:
        r4 = r54.getDeviceId();
        r6 = 0;
        r0 = r52;
        r0.launchAssistAction(r6, r4);
        goto L_0x02e8;
    L_0x0314:
        r4 = 231; // 0xe7 float:3.24E-43 double:1.14E-321;
        r0 = r30;
        if (r0 != r4) goto L_0x035c;
    L_0x031a:
        if (r23 != 0) goto L_0x0258;
    L_0x031c:
        if (r31 != 0) goto L_0x0333;
    L_0x031e:
        r51 = new android.content.Intent;
        r4 = "android.speech.action.WEB_SEARCH";
        r0 = r51;
        r0.<init>(r4);
    L_0x0328:
        r4 = android.os.UserHandle.CURRENT_OR_SELF;
        r0 = r52;
        r1 = r51;
        r0.startActivityAsUser(r1, r4);
        goto L_0x0258;
    L_0x0333:
        r4 = "deviceidle";
        r4 = android.os.ServiceManager.getService(r4);
        r21 = android.os.IDeviceIdleController.Stub.asInterface(r4);
        if (r21 == 0) goto L_0x0348;
    L_0x0340:
        r4 = "voice-search";	 Catch:{ RemoteException -> 0x077f }
        r0 = r21;	 Catch:{ RemoteException -> 0x077f }
        r0.exitIdle(r4);	 Catch:{ RemoteException -> 0x077f }
    L_0x0348:
        r51 = new android.content.Intent;
        r4 = "android.speech.action.VOICE_SEARCH_HANDS_FREE";
        r0 = r51;
        r0.<init>(r4);
        r4 = "android.speech.extras.EXTRA_SECURE";
        r6 = 1;
        r0 = r51;
        r0.putExtra(r4, r6);
        goto L_0x0328;
    L_0x035c:
        r4 = 120; // 0x78 float:1.68E-43 double:5.93E-322;
        r0 = r30;
        if (r0 != r4) goto L_0x037c;
    L_0x0362:
        if (r23 == 0) goto L_0x0379;
    L_0x0364:
        if (r37 != 0) goto L_0x0379;
    L_0x0366:
        r0 = r52;
        r4 = r0.mScreenshotRunnable;
        r6 = 1;
        r4.setScreenshotType(r6);
        r0 = r52;
        r4 = r0.mHandler;
        r0 = r52;
        r6 = r0.mScreenshotRunnable;
        r4.post(r6);
    L_0x0379:
        r6 = -1;
        return r6;
    L_0x037c:
        r4 = 221; // 0xdd float:3.1E-43 double:1.09E-321;
        r0 = r30;
        if (r0 == r4) goto L_0x0388;
    L_0x0382:
        r4 = 220; // 0xdc float:3.08E-43 double:1.087E-321;
        r0 = r30;
        if (r0 != r4) goto L_0x0420;
    L_0x0388:
        if (r23 == 0) goto L_0x0419;
    L_0x038a:
        r4 = 221; // 0xdd float:3.1E-43 double:1.09E-321;
        r0 = r30;
        if (r0 != r4) goto L_0x041c;
    L_0x0390:
        r22 = 1;
    L_0x0392:
        r0 = r52;
        r4 = r0.mContext;
        r4 = r4.getContentResolver();
        r6 = "screen_brightness_mode";
        r7 = 0;
        r8 = -3;
        r16 = android.provider.Settings.System.getIntForUser(r4, r6, r7, r8);
        if (r16 == 0) goto L_0x03b5;
    L_0x03a5:
        r0 = r52;
        r4 = r0.mContext;
        r4 = r4.getContentResolver();
        r6 = "screen_brightness_mode";
        r7 = 0;
        r8 = -3;
        android.provider.Settings.System.putIntForUser(r4, r6, r7, r8);
    L_0x03b5:
        r0 = r52;
        r4 = r0.mPowerManager;
        r35 = r4.getMinimumScreenBrightnessSetting();
        r0 = r52;
        r4 = r0.mPowerManager;
        r32 = r4.getMaximumScreenBrightnessSetting();
        r4 = r32 - r35;
        r4 = r4 + 10;
        r4 = r4 + -1;
        r4 = r4 / 10;
        r46 = r4 * r22;
        r0 = r52;
        r4 = r0.mContext;
        r4 = r4.getContentResolver();
        r6 = "screen_brightness";
        r0 = r52;
        r7 = r0.mPowerManager;
        r7 = r7.getDefaultScreenBrightnessSetting();
        r8 = -3;
        r17 = android.provider.Settings.System.getIntForUser(r4, r6, r7, r8);
        r17 = r17 + r46;
        r0 = r32;
        r1 = r17;
        r17 = java.lang.Math.min(r0, r1);
        r0 = r35;
        r1 = r17;
        r17 = java.lang.Math.max(r0, r1);
        r0 = r52;
        r4 = r0.mContext;
        r4 = r4.getContentResolver();
        r6 = "screen_brightness";
        r7 = -3;
        r0 = r17;
        android.provider.Settings.System.putIntForUser(r4, r6, r0, r7);
        r4 = new android.content.Intent;
        r6 = "com.android.intent.action.SHOW_BRIGHTNESS_DIALOG";
        r4.<init>(r6);
        r6 = android.os.UserHandle.CURRENT_OR_SELF;
        r0 = r52;
        r0.startActivityAsUser(r4, r6);
    L_0x0419:
        r6 = -1;
        return r6;
    L_0x041c:
        r22 = -1;
        goto L_0x0392;
    L_0x0420:
        r4 = 24;
        r0 = r30;
        if (r0 == r4) goto L_0x042c;
    L_0x0426:
        r4 = 25;
        r0 = r30;
        if (r0 != r4) goto L_0x0442;
    L_0x042c:
        r0 = r52;
        r4 = r0.mUseTvRouting;
        if (r4 != 0) goto L_0x0438;
    L_0x0432:
        r0 = r52;
        r4 = r0.mHandleVolumeKeysInWM;
        if (r4 == 0) goto L_0x0457;
    L_0x0438:
        r0 = r52;
        r1 = r54;
        r0.dispatchDirectAudioEvent(r1);
        r6 = -1;
        return r6;
    L_0x0442:
        r4 = 164; // 0xa4 float:2.3E-43 double:8.1E-322;
        r0 = r30;
        if (r0 == r4) goto L_0x042c;
    L_0x0448:
        r4 = 61;
        r0 = r30;
        if (r0 != r4) goto L_0x0460;
    L_0x044e:
        r4 = r54.isMetaPressed();
        if (r4 == 0) goto L_0x0460;
    L_0x0454:
        r6 = 0;
        return r6;
    L_0x0457:
        r0 = r52;
        r4 = r0.mPersistentVrModeEnabled;
        if (r4 == 0) goto L_0x0258;
    L_0x045d:
        r6 = -1;
        return r6;
    L_0x0460:
        r0 = r52;
        r4 = r0.mHasFeatureLeanback;
        if (r4 == 0) goto L_0x0475;
    L_0x0466:
        r0 = r52;
        r1 = r30;
        r2 = r23;
        r4 = r0.interceptBugreportGestureTv(r1, r2);
        if (r4 == 0) goto L_0x0475;
    L_0x0472:
        r6 = -1;
        return r6;
    L_0x0475:
        r0 = r52;
        r4 = r0.mHasFeatureLeanback;
        if (r4 == 0) goto L_0x048a;
    L_0x047b:
        r0 = r52;
        r1 = r30;
        r2 = r23;
        r4 = r0.interceptAccessibilityGestureTv(r1, r2);
        if (r4 == 0) goto L_0x048a;
    L_0x0487:
        r6 = -1;
        return r6;
    L_0x048a:
        r4 = 284; // 0x11c float:3.98E-43 double:1.403E-321;
        r0 = r30;
        if (r0 != r4) goto L_0x0258;
    L_0x0490:
        if (r23 != 0) goto L_0x04ae;
    L_0x0492:
        r0 = r52;
        r4 = r0.mHandler;
        r6 = 26;
        r4.removeMessages(r6);
        r0 = r52;
        r4 = r0.mHandler;
        r6 = 26;
        r36 = r4.obtainMessage(r6);
        r4 = 1;
        r0 = r36;
        r0.setAsynchronous(r4);
        r36.sendToTarget();
    L_0x04ae:
        r6 = -1;
        return r6;
    L_0x04b1:
        r4 = r54.getAction();
        r6 = 1;
        if (r4 != r6) goto L_0x0272;
    L_0x04b8:
        r0 = r52;
        r4 = r0.mMetaState;
        r14 = r4 & 50;
        r0 = r52;
        r4 = r0.mMetaState;
        r6 = 458752; // 0x70000 float:6.42848E-40 double:2.266536E-318;
        r33 = r4 & r6;
        if (r33 == 0) goto L_0x04e3;
    L_0x04c8:
        if (r14 == 0) goto L_0x04e3;
    L_0x04ca:
        r0 = r52;
        r4 = r0.mInitialMetaState;
        r0 = r52;
        r6 = r0.mMetaState;
        r7 = r14 | r33;
        r6 = r6 ^ r7;
        if (r4 != r6) goto L_0x04e3;
    L_0x04d7:
        r0 = r52;
        r4 = r0.mInputManagerInternal;
        r6 = r54.getDeviceId();
        r4.toggleCapsLock(r6);
        r13 = 1;
    L_0x04e3:
        r4 = 0;
        r0 = r52;
        r0.mPendingCapsLockToggle = r4;
        goto L_0x0272;
    L_0x04ea:
        r4 = android.view.KeyEvent.isMetaKey(r30);
        if (r4 == 0) goto L_0x050d;
    L_0x04f0:
        if (r23 == 0) goto L_0x04fa;
    L_0x04f2:
        r4 = 1;
        r0 = r52;
        r0.mPendingMetaAction = r4;
    L_0x04f7:
        r6 = -1;
        return r6;
    L_0x04fa:
        r0 = r52;
        r4 = r0.mPendingMetaAction;
        if (r4 == 0) goto L_0x04f7;
    L_0x0500:
        r4 = "android.intent.extra.ASSIST_INPUT_HINT_KEYBOARD";
        r6 = r54.getDeviceId();
        r0 = r52;
        r0.launchAssistAction(r4, r6);
        goto L_0x04f7;
    L_0x050d:
        r0 = r52;
        r4 = r0.mSearchKeyShortcutPending;
        if (r4 == 0) goto L_0x0596;
    L_0x0513:
        r29 = r54.getKeyCharacterMap();
        r4 = r29.isPrintingKey(r30);
        if (r4 == 0) goto L_0x0596;
    L_0x051d:
        r4 = 1;
        r0 = r52;
        r0.mConsumeSearchKeyUp = r4;
        r4 = 0;
        r0 = r52;
        r0.mSearchKeyShortcutPending = r4;
        if (r23 == 0) goto L_0x0552;
    L_0x0529:
        if (r37 != 0) goto L_0x0552;
    L_0x052b:
        r4 = r31 ^ 1;
        if (r4 == 0) goto L_0x0552;
    L_0x052f:
        r0 = r52;
        r4 = r0.mShortcutManager;
        r0 = r29;
        r1 = r30;
        r2 = r34;
        r44 = r4.getIntent(r0, r1, r2);
        if (r44 == 0) goto L_0x0577;
    L_0x053f:
        r4 = 268435456; // 0x10000000 float:2.5243549E-29 double:1.32624737E-315;
        r0 = r44;
        r0.addFlags(r4);
        r4 = android.os.UserHandle.CURRENT;	 Catch:{ ActivityNotFoundException -> 0x0555 }
        r0 = r52;	 Catch:{ ActivityNotFoundException -> 0x0555 }
        r1 = r44;	 Catch:{ ActivityNotFoundException -> 0x0555 }
        r0.startActivityAsUser(r1, r4);	 Catch:{ ActivityNotFoundException -> 0x0555 }
        r52.dismissKeyboardShortcutsMenu();	 Catch:{ ActivityNotFoundException -> 0x0555 }
    L_0x0552:
        r6 = -1;
        return r6;
    L_0x0555:
        r25 = move-exception;
        r4 = "WindowManager";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Dropping shortcut key combination because the activity to which it is registered was not found: SEARCH+";
        r6 = r6.append(r7);
        r7 = android.view.KeyEvent.keyCodeToString(r30);
        r6 = r6.append(r7);
        r6 = r6.toString();
        r0 = r25;
        android.util.Slog.w(r4, r6, r0);
        goto L_0x0552;
    L_0x0577:
        r4 = "WindowManager";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Dropping unregistered shortcut key combination: SEARCH+";
        r6 = r6.append(r7);
        r7 = android.view.KeyEvent.keyCodeToString(r30);
        r6 = r6.append(r7);
        r6 = r6.toString();
        android.util.Slog.i(r4, r6);
        goto L_0x0552;
    L_0x0596:
        if (r23 == 0) goto L_0x05f9;
    L_0x0598:
        if (r37 != 0) goto L_0x05f9;
    L_0x059a:
        r4 = r31 ^ 1;
        if (r4 == 0) goto L_0x05f9;
    L_0x059e:
        r4 = 65536; // 0x10000 float:9.18355E-41 double:3.2379E-319;
        r4 = r4 & r34;
        if (r4 == 0) goto L_0x05f9;
    L_0x05a4:
        r29 = r54.getKeyCharacterMap();
        r4 = r29.isPrintingKey(r30);
        if (r4 == 0) goto L_0x05f9;
    L_0x05ae:
        r0 = r52;
        r4 = r0.mShortcutManager;
        r6 = -458753; // 0xfffffffffff8ffff float:NaN double:NaN;
        r6 = r6 & r34;
        r0 = r29;
        r1 = r30;
        r44 = r4.getIntent(r0, r1, r6);
        if (r44 == 0) goto L_0x05f9;
    L_0x05c1:
        r4 = 268435456; // 0x10000000 float:2.5243549E-29 double:1.32624737E-315;
        r0 = r44;
        r0.addFlags(r4);
        r4 = android.os.UserHandle.CURRENT;	 Catch:{ ActivityNotFoundException -> 0x05d7 }
        r0 = r52;	 Catch:{ ActivityNotFoundException -> 0x05d7 }
        r1 = r44;	 Catch:{ ActivityNotFoundException -> 0x05d7 }
        r0.startActivityAsUser(r1, r4);	 Catch:{ ActivityNotFoundException -> 0x05d7 }
        r52.dismissKeyboardShortcutsMenu();	 Catch:{ ActivityNotFoundException -> 0x05d7 }
    L_0x05d4:
        r6 = -1;
        return r6;
    L_0x05d7:
        r25 = move-exception;
        r4 = "WindowManager";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Dropping shortcut key combination because the activity to which it is registered was not found: META+";
        r6 = r6.append(r7);
        r7 = android.view.KeyEvent.keyCodeToString(r30);
        r6 = r6.append(r7);
        r6 = r6.toString();
        r0 = r25;
        android.util.Slog.w(r4, r6, r0);
        goto L_0x05d4;
    L_0x05f9:
        if (r23 == 0) goto L_0x0655;
    L_0x05fb:
        if (r37 != 0) goto L_0x0655;
    L_0x05fd:
        r4 = r31 ^ 1;
        if (r4 == 0) goto L_0x0655;
    L_0x0601:
        r4 = sApplicationLaunchKeyCategories;
        r0 = r30;
        r19 = r4.get(r0);
        r19 = (java.lang.String) r19;
        if (r19 == 0) goto L_0x0655;
    L_0x060d:
        r4 = "android.intent.action.MAIN";
        r0 = r19;
        r5 = android.content.Intent.makeMainSelectorActivity(r4, r0);
        r4 = 268435456; // 0x10000000 float:2.5243549E-29 double:1.32624737E-315;
        r5.setFlags(r4);
        r4 = android.os.UserHandle.CURRENT;	 Catch:{ ActivityNotFoundException -> 0x0628 }
        r0 = r52;	 Catch:{ ActivityNotFoundException -> 0x0628 }
        r0.startActivityAsUser(r5, r4);	 Catch:{ ActivityNotFoundException -> 0x0628 }
        r52.dismissKeyboardShortcutsMenu();	 Catch:{ ActivityNotFoundException -> 0x0628 }
    L_0x0625:
        r6 = -1;
        return r6;
    L_0x0628:
        r25 = move-exception;
        r4 = "WindowManager";
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Dropping application launch key because the activity to which it is registered was not found: keyCode=";
        r6 = r6.append(r7);
        r0 = r30;
        r6 = r6.append(r0);
        r7 = ", category=";
        r6 = r6.append(r7);
        r0 = r19;
        r6 = r6.append(r0);
        r6 = r6.toString();
        r0 = r25;
        android.util.Slog.w(r4, r6, r0);
        goto L_0x0625;
    L_0x0655:
        if (r23 == 0) goto L_0x0690;
    L_0x0657:
        if (r37 != 0) goto L_0x0690;
    L_0x0659:
        r4 = 61;
        r0 = r30;
        if (r0 != r4) goto L_0x0690;
    L_0x065f:
        r0 = r52;
        r4 = r0.mRecentAppsHeldModifiers;
        if (r4 != 0) goto L_0x06ac;
    L_0x0665:
        r4 = r31 ^ 1;
        if (r4 == 0) goto L_0x06ac;
    L_0x0669:
        r4 = r52.isUserSetupComplete();
        if (r4 == 0) goto L_0x06ac;
    L_0x066f:
        r4 = r54.getModifiers();
        r0 = r4 & -194;
        r41 = r0;
        r4 = 2;
        r0 = r41;
        r4 = android.view.KeyEvent.metaStateHasModifiers(r0, r4);
        if (r4 == 0) goto L_0x06ac;
    L_0x0680:
        r0 = r41;
        r1 = r52;
        r1.mRecentAppsHeldModifiers = r0;
        r4 = 1;
        r6 = 0;
        r0 = r52;
        r0.showRecentApps(r4, r6);
        r6 = -1;
        return r6;
    L_0x0690:
        if (r23 != 0) goto L_0x06ac;
    L_0x0692:
        r0 = r52;
        r4 = r0.mRecentAppsHeldModifiers;
        if (r4 == 0) goto L_0x06ac;
    L_0x0698:
        r0 = r52;
        r4 = r0.mRecentAppsHeldModifiers;
        r4 = r4 & r34;
        if (r4 != 0) goto L_0x06ac;
    L_0x06a0:
        r4 = 0;
        r0 = r52;
        r0.mRecentAppsHeldModifiers = r4;
        r4 = 1;
        r6 = 0;
        r0 = r52;
        r0.hideRecentApps(r4, r6);
    L_0x06ac:
        if (r23 == 0) goto L_0x06d9;
    L_0x06ae:
        if (r37 != 0) goto L_0x06d9;
    L_0x06b0:
        r4 = 204; // 0xcc float:2.86E-43 double:1.01E-321;
        r0 = r30;
        if (r0 == r4) goto L_0x06c2;
    L_0x06b6:
        r4 = 62;
        r0 = r30;
        if (r0 != r4) goto L_0x06d9;
    L_0x06bc:
        r4 = 458752; // 0x70000 float:6.42848E-40 double:2.266536E-318;
        r4 = r4 & r34;
        if (r4 == 0) goto L_0x06d9;
    L_0x06c2:
        r0 = r34;
        r4 = r0 & 193;
        if (r4 != 0) goto L_0x06d6;
    L_0x06c8:
        r27 = 1;
    L_0x06ca:
        r0 = r52;
        r4 = r0.mWindowManagerFuncs;
        r0 = r27;
        r4.switchInputMethod(r0);
        r6 = -1;
        return r6;
    L_0x06d6:
        r27 = 0;
        goto L_0x06ca;
    L_0x06d9:
        r0 = r52;
        r4 = r0.mLanguageSwitchKeyPressed;
        if (r4 == 0) goto L_0x06f7;
    L_0x06df:
        r4 = r23 ^ 1;
        if (r4 == 0) goto L_0x06f7;
    L_0x06e3:
        r4 = 204; // 0xcc float:2.86E-43 double:1.01E-321;
        r0 = r30;
        if (r0 == r4) goto L_0x06ef;
    L_0x06e9:
        r4 = 62;
        r0 = r30;
        if (r0 != r4) goto L_0x06f7;
    L_0x06ef:
        r4 = 0;
        r0 = r52;
        r0.mLanguageSwitchKeyPressed = r4;
        r6 = -1;
        return r6;
    L_0x06f7:
        r4 = isValidGlobalKey(r30);
        if (r4 == 0) goto L_0x0712;
    L_0x06fd:
        r0 = r52;
        r4 = r0.mGlobalKeyManager;
        r0 = r52;
        r6 = r0.mContext;
        r0 = r30;
        r1 = r54;
        r4 = r4.handleGlobalKey(r6, r0, r1);
        if (r4 == 0) goto L_0x0712;
    L_0x070f:
        r6 = -1;
        return r6;
    L_0x0712:
        if (r23 == 0) goto L_0x0773;
    L_0x0714:
        r0 = r30;
        r0 = (long) r0;
        r42 = r0;
        r4 = r54.isCtrlPressed();
        if (r4 == 0) goto L_0x0726;
    L_0x071f:
        r6 = 17592186044416; // 0x100000000000 float:0.0 double:8.6916947597938E-311;
        r42 = r42 | r6;
    L_0x0726:
        r4 = r54.isAltPressed();
        if (r4 == 0) goto L_0x0733;
    L_0x072c:
        r6 = 8589934592; // 0x200000000 float:0.0 double:4.243991582E-314;
        r42 = r42 | r6;
    L_0x0733:
        r4 = r54.isShiftPressed();
        if (r4 == 0) goto L_0x0740;
    L_0x0739:
        r6 = 4294967296; // 0x100000000 float:0.0 double:2.121995791E-314;
        r42 = r42 | r6;
    L_0x0740:
        r4 = r54.isMetaPressed();
        if (r4 == 0) goto L_0x074a;
    L_0x0746:
        r6 = 281474976710656; // 0x1000000000000 float:0.0 double:1.390671161567E-309;
        r42 = r42 | r6;
    L_0x074a:
        r0 = r52;
        r4 = r0.mShortcutKeyServices;
        r0 = r42;
        r45 = r4.get(r0);
        r45 = (com.android.internal.policy.IShortcutService) r45;
        if (r45 == 0) goto L_0x0773;
    L_0x0758:
        r4 = r52.isUserSetupComplete();	 Catch:{ RemoteException -> 0x0768 }
        if (r4 == 0) goto L_0x0765;	 Catch:{ RemoteException -> 0x0768 }
    L_0x075e:
        r0 = r45;	 Catch:{ RemoteException -> 0x0768 }
        r1 = r42;	 Catch:{ RemoteException -> 0x0768 }
        r0.notifyShortcutKeyPressed(r1);	 Catch:{ RemoteException -> 0x0768 }
    L_0x0765:
        r6 = -1;
        return r6;
    L_0x0768:
        r24 = move-exception;
        r0 = r52;
        r4 = r0.mShortcutKeyServices;
        r0 = r42;
        r4.delete(r0);
        goto L_0x0765;
    L_0x0773:
        r4 = 65536; // 0x10000 float:9.18355E-41 double:3.2379E-319;
        r4 = r4 & r34;
        if (r4 == 0) goto L_0x077c;
    L_0x0779:
        r6 = -1;
        return r6;
    L_0x077c:
        r6 = 0;
        return r6;
    L_0x077f:
        r24 = move-exception;
        goto L_0x0348;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.interceptKeyBeforeDispatching(android.view.WindowManagerPolicy$WindowState, android.view.KeyEvent, int):long");
    }

    private boolean interceptBugreportGestureTv(int keyCode, boolean down) {
        if (keyCode == 23) {
            this.mBugreportTvKey1Pressed = down;
        } else if (keyCode == 4) {
            this.mBugreportTvKey2Pressed = down;
        }
        if (this.mBugreportTvKey1Pressed && this.mBugreportTvKey2Pressed) {
            if (!this.mBugreportTvScheduled) {
                this.mBugreportTvScheduled = true;
                Message msg = Message.obtain(this.mHandler, 22);
                msg.setAsynchronous(true);
                this.mHandler.sendMessageDelayed(msg, 1000);
            }
        } else if (this.mBugreportTvScheduled) {
            this.mHandler.removeMessages(22);
            this.mBugreportTvScheduled = false;
        }
        return this.mBugreportTvScheduled;
    }

    private boolean interceptAccessibilityGestureTv(int keyCode, boolean down) {
        if (keyCode == 4) {
            this.mAccessibilityTvKey1Pressed = down;
        } else if (keyCode == 20) {
            this.mAccessibilityTvKey2Pressed = down;
        }
        if (this.mAccessibilityTvKey1Pressed && this.mAccessibilityTvKey2Pressed) {
            if (!this.mAccessibilityTvScheduled) {
                this.mAccessibilityTvScheduled = true;
                Message msg = Message.obtain(this.mHandler, 23);
                msg.setAsynchronous(true);
                this.mHandler.sendMessageDelayed(msg, ViewConfiguration.get(this.mContext).getAccessibilityShortcutKeyTimeout());
            }
        } else if (this.mAccessibilityTvScheduled) {
            this.mHandler.removeMessages(23);
            this.mAccessibilityTvScheduled = false;
        }
        return this.mAccessibilityTvScheduled;
    }

    private void takeBugreport() {
        if ("1".equals(SystemProperties.get("ro.debuggable")) || Global.getInt(this.mContext.getContentResolver(), "development_settings_enabled", 0) == 1) {
            try {
                ActivityManager.getService().requestBugReport(1);
            } catch (RemoteException e) {
                Slog.e(TAG, "Error taking bugreport", e);
            }
        }
    }

    public KeyEvent dispatchUnhandledKey(WindowState win, KeyEvent event, int policyFlags) {
        KeyEvent fallbackEvent = null;
        if ((event.getFlags() & 1024) == 0) {
            FallbackAction fallbackAction;
            KeyCharacterMap kcm = event.getKeyCharacterMap();
            int keyCode = event.getKeyCode();
            int metaState = event.getMetaState();
            boolean initialDown = event.getAction() == 0 ? event.getRepeatCount() == 0 : false;
            if (initialDown) {
                fallbackAction = kcm.getFallbackAction(keyCode, metaState);
            } else {
                fallbackAction = (FallbackAction) this.mFallbackActions.get(keyCode);
            }
            if (fallbackAction != null) {
                fallbackEvent = KeyEvent.obtain(event.getDownTime(), event.getEventTime(), event.getAction(), fallbackAction.keyCode, event.getRepeatCount(), fallbackAction.metaState, event.getDeviceId(), event.getScanCode(), event.getFlags() | 1024, event.getSource(), null);
                if (!interceptFallback(win, fallbackEvent, policyFlags)) {
                    fallbackEvent.recycle();
                    fallbackEvent = null;
                }
                if (initialDown) {
                    this.mFallbackActions.put(keyCode, fallbackAction);
                } else if (event.getAction() == 1) {
                    this.mFallbackActions.remove(keyCode);
                    fallbackAction.recycle();
                }
            }
        }
        return fallbackEvent;
    }

    private boolean interceptFallback(WindowState win, KeyEvent fallbackEvent, int policyFlags) {
        if ((interceptKeyBeforeQueueing(fallbackEvent, policyFlags) & 1) == 0 || interceptKeyBeforeDispatching(win, fallbackEvent, policyFlags) != 0) {
            return false;
        }
        return true;
    }

    public void registerShortcutKey(long shortcutCode, IShortcutService shortcutService) throws RemoteException {
        synchronized (this.mLock) {
            IShortcutService service = (IShortcutService) this.mShortcutKeyServices.get(shortcutCode);
            if (service == null || !service.asBinder().pingBinder()) {
                this.mShortcutKeyServices.put(shortcutCode, shortcutService);
            } else {
                throw new RemoteException("Key already exists.");
            }
        }
    }

    public void onKeyguardOccludedChangedLw(boolean occluded) {
        if (this.mKeyguardDelegate == null || !this.mKeyguardDelegate.isShowing()) {
            setKeyguardOccludedLw(occluded, false);
            return;
        }
        this.mPendingKeyguardOccluded = occluded;
        this.mKeyguardOccludedChanged = true;
    }

    private int handleStartTransitionForKeyguardLw(int transit, Animation anim) {
        if (this.mKeyguardOccludedChanged) {
            this.mKeyguardOccludedChanged = false;
            if (setKeyguardOccludedLw(this.mPendingKeyguardOccluded, false)) {
                return 5;
            }
        }
        if (AppTransition.isKeyguardGoingAwayTransit(transit)) {
            long startTime;
            long duration;
            if (anim != null) {
                startTime = SystemClock.uptimeMillis() + anim.getStartOffset();
            } else {
                startTime = SystemClock.uptimeMillis();
            }
            if (anim != null) {
                duration = anim.getDuration();
            } else {
                duration = 0;
            }
            startKeyguardExitAnimation(startTime, duration);
        }
        return 0;
    }

    private void launchAssistLongPressAction() {
        performHapticFeedbackLw(null, 0, false);
        sendCloseSystemWindows(SYSTEM_DIALOG_REASON_ASSIST);
        Intent intent = new Intent("android.intent.action.SEARCH_LONG_PRESS");
        intent.setFlags(268435456);
        try {
            SearchManager searchManager = getSearchManager();
            if (searchManager != null) {
                searchManager.stopSearch();
            }
            startActivityAsUser(intent, UserHandle.CURRENT);
        } catch (ActivityNotFoundException e) {
            Slog.w(TAG, "No activity to handle assist long press action.", e);
        }
    }

    private void launchAssistAction(String hint, int deviceId) {
        sendCloseSystemWindows(SYSTEM_DIALOG_REASON_ASSIST);
        if (isUserSetupComplete()) {
            Bundle bundle = null;
            if (deviceId > Integer.MIN_VALUE) {
                bundle = new Bundle();
                bundle.putInt("android.intent.extra.ASSIST_INPUT_DEVICE_ID", deviceId);
            }
            if ((this.mContext.getResources().getConfiguration().uiMode & 15) == 4) {
                ((SearchManager) this.mContext.getSystemService("search")).launchLegacyAssist(hint, UserHandle.myUserId(), bundle);
            } else {
                if (hint != null) {
                    if (bundle == null) {
                        bundle = new Bundle();
                    }
                    bundle.putBoolean(hint, true);
                }
                StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
                if (statusbar != null) {
                    statusbar.startAssist(bundle);
                }
            }
        }
    }

    private void startActivityAsUser(Intent intent, UserHandle handle) {
        if (isUserSetupComplete()) {
            this.mContext.startActivityAsUser(intent, handle);
        } else {
            Slog.i(TAG, "Not starting activity because user setup is in progress: " + intent);
        }
    }

    private SearchManager getSearchManager() {
        if (this.mSearchManager == null) {
            this.mSearchManager = (SearchManager) this.mContext.getSystemService("search");
        }
        return this.mSearchManager;
    }

    private void preloadRecentApps() {
        this.mPreloadedRecentApps = true;
        StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
        if (statusbar != null) {
            statusbar.preloadRecentApps();
        }
    }

    private void cancelPreloadRecentApps() {
        if (this.mPreloadedRecentApps) {
            this.mPreloadedRecentApps = false;
            StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
            if (statusbar != null) {
                statusbar.cancelPreloadRecentApps();
            }
        }
    }

    private void toggleRecentApps() {
        this.mPreloadedRecentApps = false;
        StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
        if (statusbar != null) {
            statusbar.toggleRecentApps();
        }
    }

    public void showRecentApps(boolean fromHome) {
        int i;
        this.mHandler.removeMessages(9);
        Handler handler = this.mHandler;
        if (fromHome) {
            i = 1;
        } else {
            i = 0;
        }
        handler.obtainMessage(9, i, 0).sendToTarget();
    }

    private void showRecentApps(boolean triggeredFromAltTab, boolean fromHome) {
        this.mPreloadedRecentApps = false;
        StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
        if (statusbar != null) {
            statusbar.showRecentApps(triggeredFromAltTab, fromHome);
        }
    }

    private void toggleKeyboardShortcutsMenu(int deviceId) {
        StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
        if (statusbar != null) {
            statusbar.toggleKeyboardShortcutsMenu(deviceId);
        }
    }

    private void dismissKeyboardShortcutsMenu() {
        StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
        if (statusbar != null) {
            statusbar.dismissKeyboardShortcutsMenu();
        }
    }

    private void hideRecentApps(boolean triggeredFromAltTab, boolean triggeredFromHome) {
        this.mPreloadedRecentApps = false;
        StatusBarManagerInternal statusbar = getStatusBarManagerInternal();
        if (statusbar != null) {
            statusbar.hideRecentApps(triggeredFromAltTab, triggeredFromHome);
        }
    }

    void launchHomeFromHotKey() {
        launchHomeFromHotKey(true, true);
    }

    void launchHomeFromHotKey(final boolean awakenFromDreams, boolean respectKeyguard) {
        if (respectKeyguard) {
            if (!isKeyguardShowingAndNotOccluded()) {
                if (!this.mKeyguardOccluded && this.mKeyguardDelegate.isInputRestricted()) {
                    this.mKeyguardDelegate.verifyUnlock(new OnKeyguardExitResult() {
                        public void onKeyguardExitResult(boolean success) {
                            if (success) {
                                try {
                                    ActivityManager.getService().stopAppSwitches();
                                } catch (RemoteException e) {
                                }
                                PhoneWindowManager.this.sendCloseSystemWindows(PhoneWindowManager.SYSTEM_DIALOG_REASON_HOME_KEY);
                                PhoneWindowManager.this.startDockOrHome(true, awakenFromDreams);
                            }
                        }
                    });
                    return;
                }
            }
            return;
        }
        try {
            ActivityManager.getService().stopAppSwitches();
        } catch (RemoteException e) {
        }
        if (this.mRecentsVisible) {
            if (awakenFromDreams) {
                awakenDreams();
            }
            hideRecentApps(false, true);
        } else {
            sendCloseSystemWindows(SYSTEM_DIALOG_REASON_HOME_KEY);
            startDockOrHome(true, awakenFromDreams);
        }
    }

    public void setRecentsVisibilityLw(boolean visible) {
        this.mRecentsVisible = visible;
    }

    public void setPipVisibilityLw(boolean visible) {
        this.mPictureInPictureVisible = visible;
    }

    public int adjustSystemUiVisibilityLw(int visibility) {
        this.mStatusBarController.adjustSystemUiVisibilityLw(this.mLastSystemUiFlags, visibility);
        this.mNavigationBarController.adjustSystemUiVisibilityLw(this.mLastSystemUiFlags, visibility);
        this.mResettingSystemUiFlags &= visibility;
        return ((~this.mResettingSystemUiFlags) & visibility) & (~this.mForceClearedSystemUiFlags);
    }

    public boolean getInsetHintLw(LayoutParams attrs, Rect taskBounds, int displayRotation, int displayWidth, int displayHeight, Rect outContentInsets, Rect outStableInsets, Rect outOutsets) {
        int fl = EssentialScreenPolicy.getWindowFlags(null, attrs);
        int systemUiVisibility = EssentialScreenPolicy.getSystemUiVisibility(null, attrs) | attrs.subtreeSystemUiVisibility;
        if (outOutsets != null ? shouldUseOutsets(attrs, fl) : false) {
            int outset = ScreenShapeHelper.getWindowOutsetBottomPx(this.mContext.getResources());
            if (outset > 0) {
                if (displayRotation == 0) {
                    outOutsets.bottom += outset;
                } else if (displayRotation == 1) {
                    outOutsets.right += outset;
                } else if (displayRotation == 2) {
                    outOutsets.top += outset;
                } else if (displayRotation == 3) {
                    outOutsets.left += outset;
                }
            }
        }
        if ((65792 & fl) == 65792) {
            int availRight;
            int availBottom;
            if (!canHideNavigationBar() || (systemUiVisibility & 512) == 0) {
                availRight = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                availBottom = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
            } else {
                availRight = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                availBottom = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
            }
            if ((systemUiVisibility & 256) != 0) {
                if ((fl & 1024) != 0) {
                    outContentInsets.set(this.mStableFullscreenLeft, this.mStableFullscreenTop, availRight - this.mStableFullscreenRight, availBottom - this.mStableFullscreenBottom);
                } else {
                    outContentInsets.set(this.mStableLeft, this.mStableTop, availRight - this.mStableRight, availBottom - this.mStableBottom);
                }
            } else if ((fl & 1024) != 0 || (33554432 & fl) != 0) {
                outContentInsets.setEmpty();
            } else if ((systemUiVisibility & UsbTerminalTypes.TERMINAL_BIDIR_SKRPHONE_SUPRESS) == 0) {
                outContentInsets.set(this.mCurLeft, this.mCurTop, availRight - this.mCurRight, availBottom - this.mCurBottom);
            } else {
                outContentInsets.set(this.mCurLeft, this.mCurTop, availRight - this.mCurRight, availBottom - this.mCurBottom);
            }
            outStableInsets.set(this.mStableLeft, this.mStableTop, availRight - this.mStableRight, availBottom - this.mStableBottom);
            if (taskBounds != null) {
                calculateRelevantTaskInsets(taskBounds, outContentInsets, displayWidth, displayHeight);
                calculateRelevantTaskInsets(taskBounds, outStableInsets, displayWidth, displayHeight);
            }
            return this.mForceShowSystemBars;
        }
        outContentInsets.setEmpty();
        outStableInsets.setEmpty();
        return this.mForceShowSystemBars;
    }

    private void calculateRelevantTaskInsets(Rect taskBounds, Rect inOutInsets, int displayWidth, int displayHeight) {
        mTmpRect.set(0, 0, displayWidth, displayHeight);
        mTmpRect.inset(inOutInsets);
        mTmpRect.intersect(taskBounds);
        inOutInsets.set(mTmpRect.left - taskBounds.left, mTmpRect.top - taskBounds.top, taskBounds.right - mTmpRect.right, taskBounds.bottom - mTmpRect.bottom);
    }

    private boolean shouldUseOutsets(LayoutParams attrs, int fl) {
        return attrs.type == 2013 || (33555456 & fl) != 0;
    }

    public void beginLayoutLw(boolean isDefaultDisplay, int displayWidth, int displayHeight, int displayRotation, int uiMode) {
        int overscanLeft;
        int overscanTop;
        int overscanRight;
        int overscanBottom;
        this.mDisplayRotation = displayRotation;
        if (isDefaultDisplay) {
            switch (displayRotation) {
                case 1:
                    overscanLeft = this.mOverscanTop;
                    overscanTop = this.mOverscanRight;
                    overscanRight = this.mOverscanBottom;
                    overscanBottom = this.mOverscanLeft;
                    break;
                case 2:
                    overscanLeft = this.mOverscanRight;
                    overscanTop = this.mOverscanBottom;
                    overscanRight = this.mOverscanLeft;
                    overscanBottom = this.mOverscanTop;
                    break;
                case 3:
                    overscanLeft = this.mOverscanBottom;
                    overscanTop = this.mOverscanLeft;
                    overscanRight = this.mOverscanTop;
                    overscanBottom = this.mOverscanRight;
                    break;
                default:
                    overscanLeft = this.mOverscanLeft;
                    overscanTop = this.mOverscanTop;
                    overscanRight = this.mOverscanRight;
                    overscanBottom = this.mOverscanBottom;
                    break;
            }
        }
        overscanLeft = 0;
        overscanTop = 0;
        overscanRight = 0;
        overscanBottom = 0;
        this.mRestrictedOverscanScreenLeft = 0;
        this.mOverscanScreenLeft = 0;
        this.mRestrictedOverscanScreenTop = 0;
        this.mOverscanScreenTop = 0;
        this.mRestrictedOverscanScreenWidth = displayWidth;
        this.mOverscanScreenWidth = displayWidth;
        this.mRestrictedOverscanScreenHeight = displayHeight;
        this.mOverscanScreenHeight = displayHeight;
        this.mSystemLeft = 0;
        this.mSystemTop = 0;
        this.mSystemRight = displayWidth;
        this.mSystemBottom = displayHeight;
        this.mUnrestrictedScreenLeft = overscanLeft;
        this.mUnrestrictedScreenTop = overscanTop;
        this.mUnrestrictedScreenWidth = (displayWidth - overscanLeft) - overscanRight;
        this.mUnrestrictedScreenHeight = (displayHeight - overscanTop) - overscanBottom;
        this.mRestrictedScreenLeft = this.mUnrestrictedScreenLeft;
        this.mRestrictedScreenTop = this.mUnrestrictedScreenTop;
        int i = this.mUnrestrictedScreenWidth;
        this.mSystemGestures.screenWidth = i;
        this.mRestrictedScreenWidth = i;
        i = this.mUnrestrictedScreenHeight;
        this.mSystemGestures.screenHeight = i;
        this.mRestrictedScreenHeight = i;
        i = this.mUnrestrictedScreenLeft;
        this.mCurLeft = i;
        this.mStableFullscreenLeft = i;
        this.mStableLeft = i;
        this.mVoiceContentLeft = i;
        this.mContentLeft = i;
        this.mDockLeft = i;
        i = this.mUnrestrictedScreenTop;
        this.mCurTop = i;
        this.mStableFullscreenTop = i;
        this.mStableTop = i;
        this.mVoiceContentTop = i;
        this.mContentTop = i;
        this.mDockTop = i;
        i = displayWidth - overscanRight;
        this.mCurRight = i;
        this.mStableFullscreenRight = i;
        this.mStableRight = i;
        this.mVoiceContentRight = i;
        this.mContentRight = i;
        this.mDockRight = i;
        i = displayHeight - overscanBottom;
        this.mCurBottom = i;
        this.mStableFullscreenBottom = i;
        this.mStableBottom = i;
        this.mVoiceContentBottom = i;
        this.mContentBottom = i;
        this.mDockBottom = i;
        this.mDockLayer = 268435456;
        this.mStatusBarLayer = -1;
        Rect pf = mTmpParentFrame;
        Rect df = mTmpDisplayFrame;
        Rect of = mTmpOverscanFrame;
        Rect vf = mTmpVisibleFrame;
        Rect dcf = mTmpDecorFrame;
        i = this.mDockLeft;
        vf.left = i;
        of.left = i;
        df.left = i;
        pf.left = i;
        i = this.mDockTop;
        vf.top = i;
        of.top = i;
        df.top = i;
        pf.top = i;
        i = this.mDockRight;
        vf.right = i;
        of.right = i;
        df.right = i;
        pf.right = i;
        i = this.mDockBottom;
        vf.bottom = i;
        of.bottom = i;
        df.bottom = i;
        pf.bottom = i;
        dcf.setEmpty();
        if (isDefaultDisplay) {
            int sysui = this.mLastSystemUiFlags;
            boolean navVisible = (sysui & 2) == 0;
            boolean navTranslucent = (-2147450880 & sysui) != 0;
            boolean immersive = (sysui & 2048) != 0;
            boolean immersiveSticky = (sysui & 4096) != 0;
            boolean z = !immersive ? immersiveSticky : true;
            navTranslucent &= immersiveSticky ^ 1;
            boolean z2 = isStatusBarKeyguard() ? this.mKeyguardOccluded ^ 1 : false;
            if (!z2) {
                navTranslucent &= areTranslucentBarsAllowed();
            }
            boolean statusBarExpandedNotKeyguard = (z2 || this.mStatusBar == null || this.mStatusBar.getAttrs().height != -1) ? false : this.mStatusBar.getAttrs().width == -1;
            if (navVisible || z) {
                if (this.mInputConsumer != null) {
                    this.mHandler.sendMessage(this.mHandler.obtainMessage(19, this.mInputConsumer));
                    this.mInputConsumer = null;
                }
            } else if (this.mInputConsumer == null) {
                this.mInputConsumer = this.mWindowManagerFuncs.createInputConsumer(this.mHandler.getLooper(), "nav_input_consumer", new -$Lambda$Nd7e3Murb8x7RqelLk3bI3c3rfY(this));
                InputManager.getInstance().setPointerIconType(0);
            }
            if (layoutNavigationBar(displayWidth, displayHeight, displayRotation, uiMode, overscanLeft, overscanRight, overscanBottom, dcf, navVisible | (canHideNavigationBar() ^ 1), navTranslucent, z, statusBarExpandedNotKeyguard) | layoutStatusBar(pf, df, of, vf, dcf, sysui, z2, displayRotation)) {
                updateSystemUiVisibilityLw();
            }
        }
    }

    /* synthetic */ InputEventReceiver lambda$-com_android_server_policy_PhoneWindowManager_204839(InputChannel channel, Looper looper) {
        return new HideNavInputEventReceiver(channel, looper);
    }

    private boolean layoutStatusBar(Rect pf, Rect df, Rect of, Rect vf, Rect dcf, int sysui, boolean isKeyguardShowing, int displayRotation) {
        if (this.mStatusBar != null) {
            int i = this.mUnrestrictedScreenLeft;
            of.left = i;
            df.left = i;
            pf.left = i;
            i = this.mUnrestrictedScreenTop;
            of.top = i;
            df.top = i;
            pf.top = i;
            i = this.mUnrestrictedScreenWidth + this.mUnrestrictedScreenLeft;
            of.right = i;
            df.right = i;
            pf.right = i;
            i = this.mUnrestrictedScreenHeight + this.mUnrestrictedScreenTop;
            of.bottom = i;
            df.bottom = i;
            pf.bottom = i;
            vf.left = this.mStableLeft;
            vf.top = this.mStableTop;
            vf.right = this.mStableRight;
            vf.bottom = this.mStableBottom;
            switch (displayRotation) {
                case 1:
                    if (this.mNavigationBarPosition == 2) {
                        vf.left += this.mCameraNotchInset;
                        break;
                    }
                    break;
                case 3:
                    if (this.mNavigationBarPosition == 1) {
                        vf.right -= this.mCameraNotchInset;
                        break;
                    }
                    break;
            }
            this.mStatusBarLayer = this.mStatusBar.getSurfaceLayer();
            this.mStatusBar.computeFrameLw(pf, df, vf, vf, vf, dcf, vf, vf);
            this.mStableTop = this.mUnrestrictedScreenTop + this.mStatusBarHeight;
            boolean statusBarTransient = (67108864 & sysui) != 0;
            int statusBarTranslucent = (1073741832 & sysui) != 0 ? 1 : 0;
            if (!isKeyguardShowing) {
                statusBarTranslucent &= areTranslucentBarsAllowed();
            }
            if (this.mStatusBar.isVisibleLw() && (statusBarTransient ^ 1) != 0) {
                this.mDockTop = this.mUnrestrictedScreenTop + this.mStatusBarHeight;
                i = this.mDockTop;
                this.mCurTop = i;
                this.mVoiceContentTop = i;
                this.mContentTop = i;
                i = this.mDockBottom;
                this.mCurBottom = i;
                this.mVoiceContentBottom = i;
                this.mContentBottom = i;
                i = this.mDockLeft;
                this.mCurLeft = i;
                this.mVoiceContentLeft = i;
                this.mContentLeft = i;
                i = this.mDockRight;
                this.mCurRight = i;
                this.mVoiceContentRight = i;
                this.mContentRight = i;
            }
            if (!(!this.mStatusBar.isVisibleLw() || (this.mStatusBar.isAnimatingLw() ^ 1) == 0 || (statusBarTransient ^ 1) == 0 || (r10 ^ 1) == 0 || (this.mStatusBarController.wasRecentlyTranslucent() ^ 1) == 0)) {
                this.mSystemTop = this.mUnrestrictedScreenTop + this.mStatusBarHeight;
            }
            if (this.mStatusBarController.checkHiddenLw()) {
                return true;
            }
        }
        return false;
    }

    private boolean layoutNavigationBar(int displayWidth, int displayHeight, int displayRotation, int uiMode, int overscanLeft, int overscanRight, int overscanBottom, Rect dcf, boolean navVisible, boolean navTranslucent, boolean navAllowedHidden, boolean statusBarExpandedNotKeyguard) {
        if (this.mNavigationBar != null) {
            int i;
            boolean transientNavBarShowing = this.mNavigationBarController.isTransientShowing();
            this.mNavigationBarPosition = navigationBarPosition(displayWidth, displayHeight, displayRotation);
            if (this.mNavigationBarPosition == 4) {
                mTmpNavigationFrame.set(0, (displayHeight - overscanBottom) - getNavigationBarHeight(displayRotation, uiMode), displayWidth, displayHeight - overscanBottom);
                i = mTmpNavigationFrame.top;
                this.mStableFullscreenBottom = i;
                this.mStableBottom = i;
                if (transientNavBarShowing) {
                    this.mNavigationBarController.setBarShowingLw(true);
                } else if (navVisible) {
                    this.mNavigationBarController.setBarShowingLw(true);
                    this.mDockBottom = mTmpNavigationFrame.top;
                    this.mRestrictedScreenHeight = this.mDockBottom - this.mRestrictedScreenTop;
                    this.mRestrictedOverscanScreenHeight = this.mDockBottom - this.mRestrictedOverscanScreenTop;
                } else {
                    this.mNavigationBarController.setBarShowingLw(statusBarExpandedNotKeyguard);
                }
                if (!(!navVisible || (navTranslucent ^ 1) == 0 || (navAllowedHidden ^ 1) == 0 || (this.mNavigationBar.isAnimatingLw() ^ 1) == 0 || (this.mNavigationBarController.wasRecentlyTranslucent() ^ 1) == 0)) {
                    this.mSystemBottom = mTmpNavigationFrame.top;
                }
            } else if (this.mNavigationBarPosition == 2) {
                mTmpNavigationFrame.set((displayWidth - overscanRight) - getNavigationBarWidth(displayRotation, uiMode), 0, displayWidth - overscanRight, displayHeight);
                i = mTmpNavigationFrame.left;
                this.mStableFullscreenRight = i;
                this.mStableRight = i;
                if (transientNavBarShowing) {
                    this.mNavigationBarController.setBarShowingLw(true);
                } else if (navVisible) {
                    this.mNavigationBarController.setBarShowingLw(true);
                    this.mDockRight = mTmpNavigationFrame.left;
                    this.mRestrictedScreenWidth = this.mDockRight - this.mRestrictedScreenLeft;
                    this.mRestrictedOverscanScreenWidth = this.mDockRight - this.mRestrictedOverscanScreenLeft;
                } else {
                    this.mNavigationBarController.setBarShowingLw(statusBarExpandedNotKeyguard);
                }
                if (!(!navVisible || (navTranslucent ^ 1) == 0 || (navAllowedHidden ^ 1) == 0 || (this.mNavigationBar.isAnimatingLw() ^ 1) == 0 || (this.mNavigationBarController.wasRecentlyTranslucent() ^ 1) == 0)) {
                    this.mSystemRight = mTmpNavigationFrame.left;
                }
            } else if (this.mNavigationBarPosition == 1) {
                mTmpNavigationFrame.set(overscanLeft, 0, overscanLeft + getNavigationBarWidth(displayRotation, uiMode), displayHeight);
                i = mTmpNavigationFrame.right;
                this.mStableFullscreenLeft = i;
                this.mStableLeft = i;
                if (transientNavBarShowing) {
                    this.mNavigationBarController.setBarShowingLw(true);
                } else if (navVisible) {
                    this.mNavigationBarController.setBarShowingLw(true);
                    this.mDockLeft = mTmpNavigationFrame.right;
                    i = this.mDockLeft;
                    this.mRestrictedOverscanScreenLeft = i;
                    this.mRestrictedScreenLeft = i;
                    this.mRestrictedScreenWidth = this.mDockRight - this.mRestrictedScreenLeft;
                    this.mRestrictedOverscanScreenWidth = this.mDockRight - this.mRestrictedOverscanScreenLeft;
                } else {
                    this.mNavigationBarController.setBarShowingLw(statusBarExpandedNotKeyguard);
                }
                if (!(!navVisible || (navTranslucent ^ 1) == 0 || (navAllowedHidden ^ 1) == 0 || (this.mNavigationBar.isAnimatingLw() ^ 1) == 0 || (this.mNavigationBarController.wasRecentlyTranslucent() ^ 1) == 0)) {
                    this.mSystemLeft = mTmpNavigationFrame.right;
                }
            }
            i = this.mDockTop;
            this.mCurTop = i;
            this.mVoiceContentTop = i;
            this.mContentTop = i;
            i = this.mDockBottom;
            this.mCurBottom = i;
            this.mVoiceContentBottom = i;
            this.mContentBottom = i;
            i = this.mDockLeft;
            this.mCurLeft = i;
            this.mVoiceContentLeft = i;
            this.mContentLeft = i;
            i = this.mDockRight;
            this.mCurRight = i;
            this.mVoiceContentRight = i;
            this.mContentRight = i;
            this.mStatusBarLayer = this.mNavigationBar.getSurfaceLayer();
            this.mNavigationBar.computeFrameLw(mTmpNavigationFrame, mTmpNavigationFrame, mTmpNavigationFrame, mTmpNavigationFrame, mTmpNavigationFrame, dcf, mTmpNavigationFrame, mTmpNavigationFrame);
            if (this.mNavigationBarController.checkHiddenLw()) {
                return true;
            }
        }
        return false;
    }

    private int navigationBarPosition(int displayWidth, int displayHeight, int displayRotation) {
        if (!this.mNavigationBarCanMove || displayWidth <= displayHeight) {
            return 4;
        }
        if (displayRotation == 3) {
            return 1;
        }
        return 2;
    }

    public int getSystemDecorLayerLw() {
        if (this.mStatusBar != null && this.mStatusBar.isVisibleLw()) {
            return this.mStatusBar.getSurfaceLayer();
        }
        if (this.mNavigationBar == null || !this.mNavigationBar.isVisibleLw()) {
            return 0;
        }
        return this.mNavigationBar.getSurfaceLayer();
    }

    public void getContentRectLw(Rect r) {
        r.set(this.mContentLeft, this.mContentTop, this.mContentRight, this.mContentBottom);
    }

    void setAttachedWindowFrames(WindowState win, int fl, int adjust, WindowState attached, boolean insetDecors, Rect pf, Rect df, Rect of, Rect cf, Rect vf) {
        if (win.getSurfaceLayer() <= this.mDockLayer || attached.getSurfaceLayer() >= this.mDockLayer) {
            Rect displayFrameLw;
            if (adjust != 16) {
                cf.set((1073741824 & fl) != 0 ? attached.getContentFrameLw() : attached.getOverscanFrameLw());
            } else {
                cf.set(attached.getContentFrameLw());
                if (attached.isVoiceInteraction()) {
                    if (cf.left < this.mVoiceContentLeft) {
                        cf.left = this.mVoiceContentLeft;
                    }
                    if (cf.top < this.mVoiceContentTop) {
                        cf.top = this.mVoiceContentTop;
                    }
                    if (cf.right > this.mVoiceContentRight) {
                        cf.right = this.mVoiceContentRight;
                    }
                    if (cf.bottom > this.mVoiceContentBottom) {
                        cf.bottom = this.mVoiceContentBottom;
                    }
                } else if (attached.getSurfaceLayer() < this.mDockLayer) {
                    if (cf.left < this.mContentLeft) {
                        cf.left = this.mContentLeft;
                    }
                    if (cf.top < this.mContentTop) {
                        cf.top = this.mContentTop;
                    }
                    if (cf.right > this.mContentRight) {
                        cf.right = this.mContentRight;
                    }
                    if (cf.bottom > this.mContentBottom) {
                        cf.bottom = this.mContentBottom;
                    }
                }
            }
            if (insetDecors) {
                displayFrameLw = attached.getDisplayFrameLw();
            } else {
                displayFrameLw = cf;
            }
            df.set(displayFrameLw);
            if (insetDecors) {
                cf = attached.getOverscanFrameLw();
            }
            of.set(cf);
            vf.set(attached.getVisibleFrameLw());
        } else {
            int i = this.mDockLeft;
            vf.left = i;
            cf.left = i;
            of.left = i;
            df.left = i;
            i = this.mDockTop;
            vf.top = i;
            cf.top = i;
            of.top = i;
            df.top = i;
            i = this.mDockRight;
            vf.right = i;
            cf.right = i;
            of.right = i;
            df.right = i;
            i = this.mDockBottom;
            vf.bottom = i;
            cf.bottom = i;
            of.bottom = i;
            df.bottom = i;
        }
        if ((fl & 256) == 0) {
            df = attached.getFrameLw();
        }
        pf.set(df);
    }

    private void applyStableConstraints(int sysui, int fl, Rect r) {
        if ((sysui & 256) == 0) {
            return;
        }
        if ((fl & 1024) != 0) {
            if (r.left < this.mStableFullscreenLeft) {
                r.left = this.mStableFullscreenLeft;
            }
            if (r.top < this.mStableFullscreenTop) {
                r.top = this.mStableFullscreenTop;
            }
            if (r.right > this.mStableFullscreenRight) {
                r.right = this.mStableFullscreenRight;
            }
            if (r.bottom > this.mStableFullscreenBottom) {
                r.bottom = this.mStableFullscreenBottom;
                return;
            }
            return;
        }
        if (r.left < this.mStableLeft) {
            r.left = this.mStableLeft;
        }
        if (r.top < this.mStableTop) {
            r.top = this.mStableTop;
        }
        if (r.right > this.mStableRight) {
            r.right = this.mStableRight;
        }
        if (r.bottom > this.mStableBottom) {
            r.bottom = this.mStableBottom;
        }
    }

    private boolean canReceiveInput(WindowState win) {
        return (((win.getAttrs().flags & 8) != 0) ^ ((win.getAttrs().flags & DumpState.DUMP_INTENT_FILTER_VERIFIERS) != 0)) ^ 1;
    }

    public void layoutWindowLw(WindowState win, WindowState attached) {
        if ((win != this.mStatusBar || (canReceiveInput(win) ^ 1) == 0) && win != this.mNavigationBar) {
            boolean isVisibleLw;
            Rect letterboxInsets;
            LayoutParams attrs = win.getAttrs();
            boolean isDefaultDisplay = win.isDefaultDisplay();
            boolean needsToOffsetInputMethodTarget = isDefaultDisplay ? win == this.mLastInputMethodTargetWindow && this.mLastInputMethodWindow != null : false;
            if (needsToOffsetInputMethodTarget) {
                offsetInputMethodWindowLw(this.mLastInputMethodWindow);
            }
            int fl = EssentialScreenPolicy.getWindowFlags(win, attrs);
            int pfl = attrs.privateFlags;
            int sim = attrs.softInputMode;
            int sysUiFl = EssentialScreenPolicy.getSystemUiVisibility(win, null);
            Rect pf = mTmpParentFrame;
            Rect df = mTmpDisplayFrame;
            Rect of = mTmpOverscanFrame;
            Rect cf = mTmpContentFrame;
            Rect vf = mTmpVisibleFrame;
            Rect dcf = mTmpDecorFrame;
            Rect sf = mTmpStableFrame;
            Rect osf = null;
            dcf.setEmpty();
            if (isDefaultDisplay && this.mHasNavigationBar && this.mNavigationBar != null) {
                isVisibleLw = this.mNavigationBar.isVisibleLw();
            } else {
                isVisibleLw = false;
            }
            int adjust = sim & 240;
            if (isDefaultDisplay) {
                sf.set(this.mStableLeft, this.mStableTop, this.mStableRight, this.mStableBottom);
            } else {
                sf.set(this.mOverscanLeft, this.mOverscanTop, this.mOverscanRight, this.mOverscanBottom);
            }
            int i;
            if (isDefaultDisplay) {
                if (attrs.type == 2011) {
                    i = this.mDockLeft;
                    vf.left = i;
                    cf.left = i;
                    of.left = i;
                    df.left = i;
                    pf.left = i;
                    i = this.mDockTop;
                    vf.top = i;
                    cf.top = i;
                    of.top = i;
                    df.top = i;
                    pf.top = i;
                    i = this.mDockRight;
                    vf.right = i;
                    cf.right = i;
                    of.right = i;
                    df.right = i;
                    pf.right = i;
                    i = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                    of.bottom = i;
                    df.bottom = i;
                    pf.bottom = i;
                    i = this.mStableBottom;
                    vf.bottom = i;
                    cf.bottom = i;
                    if (this.mStatusBar != null && this.mFocusedWindow == this.mStatusBar) {
                        if (canReceiveInput(this.mStatusBar)) {
                            if (this.mNavigationBarPosition == 2) {
                                i = this.mStableRight;
                                vf.right = i;
                                cf.right = i;
                                of.right = i;
                                df.right = i;
                                pf.right = i;
                            } else if (this.mNavigationBarPosition == 1) {
                                i = this.mStableLeft;
                                vf.left = i;
                                cf.left = i;
                                of.left = i;
                                df.left = i;
                                pf.left = i;
                            }
                        }
                    }
                    attrs.gravity = 80;
                    this.mDockLayer = win.getSurfaceLayer();
                } else if (attrs.type == 2031) {
                    i = this.mUnrestrictedScreenLeft;
                    of.left = i;
                    df.left = i;
                    pf.left = i;
                    i = this.mUnrestrictedScreenTop;
                    of.top = i;
                    df.top = i;
                    pf.top = i;
                    i = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                    of.right = i;
                    df.right = i;
                    pf.right = i;
                    i = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                    of.bottom = i;
                    df.bottom = i;
                    pf.bottom = i;
                    if (adjust != 16) {
                        cf.left = this.mDockLeft;
                        cf.top = this.mDockTop;
                        cf.right = this.mDockRight;
                        cf.bottom = this.mDockBottom;
                    } else {
                        cf.left = this.mContentLeft;
                        cf.top = this.mContentTop;
                        cf.right = this.mContentRight;
                        cf.bottom = this.mContentBottom;
                    }
                    if (adjust != 48) {
                        vf.left = this.mCurLeft;
                        vf.top = this.mCurTop;
                        vf.right = this.mCurRight;
                        vf.bottom = this.mCurBottom;
                    } else {
                        vf.set(cf);
                    }
                } else if (attrs.type == 2013) {
                    layoutWallpaper(win, pf, df, of, cf);
                } else if (win == this.mStatusBar) {
                    i = this.mUnrestrictedScreenLeft;
                    of.left = i;
                    df.left = i;
                    pf.left = i;
                    i = this.mUnrestrictedScreenTop;
                    of.top = i;
                    df.top = i;
                    pf.top = i;
                    i = this.mUnrestrictedScreenWidth + this.mUnrestrictedScreenLeft;
                    of.right = i;
                    df.right = i;
                    pf.right = i;
                    i = this.mUnrestrictedScreenHeight + this.mUnrestrictedScreenTop;
                    of.bottom = i;
                    df.bottom = i;
                    pf.bottom = i;
                    i = this.mStableLeft;
                    vf.left = i;
                    cf.left = i;
                    i = this.mStableTop;
                    vf.top = i;
                    cf.top = i;
                    i = this.mStableRight;
                    vf.right = i;
                    cf.right = i;
                    vf.bottom = this.mStableBottom;
                    if (adjust == 16) {
                        cf.bottom = this.mContentBottom;
                    } else {
                        cf.bottom = this.mDockBottom;
                        vf.bottom = this.mContentBottom;
                    }
                } else {
                    dcf.left = this.mSystemLeft;
                    dcf.top = this.mSystemTop;
                    dcf.right = this.mSystemRight;
                    dcf.bottom = this.mSystemBottom;
                    boolean inheritTranslucentDecor = (attrs.privateFlags & 512) != 0;
                    boolean isAppWindow = attrs.type >= 1 ? attrs.type <= 99 : false;
                    int isAnimatingLw = win == this.mTopFullscreenOpaqueWindowState ? win.isAnimatingLw() ^ 1 : 0;
                    if (!(!isAppWindow || (inheritTranslucentDecor ^ 1) == 0 || (isAnimatingLw ^ 1) == 0)) {
                        if ((sysUiFl & 4) == 0 && (fl & 1024) == 0 && (67108864 & fl) == 0 && (Integer.MIN_VALUE & fl) == 0 && (DumpState.DUMP_INTENT_FILTER_VERIFIERS & pfl) == 0) {
                            dcf.top = this.mStableTop;
                        }
                        if ((134217728 & fl) == 0 && (sysUiFl & 2) == 0 && (Integer.MIN_VALUE & fl) == 0) {
                            dcf.bottom = this.mStableBottom;
                            dcf.right = this.mStableRight;
                        }
                    }
                    if ((65792 & fl) == 65792) {
                        if (attached != null) {
                            setAttachedWindowFrames(win, fl, adjust, attached, true, pf, df, of, cf, vf);
                        } else {
                            if (attrs.type == 2014 || attrs.type == 2017) {
                                i = isVisibleLw ? this.mDockLeft : this.mUnrestrictedScreenLeft;
                                of.left = i;
                                df.left = i;
                                pf.left = i;
                                i = this.mUnrestrictedScreenTop;
                                of.top = i;
                                df.top = i;
                                pf.top = i;
                                if (isVisibleLw) {
                                    i = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                                } else {
                                    i = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                                }
                                of.right = i;
                                df.right = i;
                                pf.right = i;
                                if (!isVisibleLw || win == this.mLetterBoxBar) {
                                    i = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                                } else {
                                    i = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                                }
                                of.bottom = i;
                                df.bottom = i;
                                pf.bottom = i;
                            } else if ((33554432 & fl) != 0 && attrs.type >= 1 && attrs.type <= 1999) {
                                i = this.mOverscanScreenLeft;
                                of.left = i;
                                df.left = i;
                                pf.left = i;
                                i = this.mOverscanScreenTop;
                                of.top = i;
                                df.top = i;
                                pf.top = i;
                                i = this.mOverscanScreenLeft + this.mOverscanScreenWidth;
                                of.right = i;
                                df.right = i;
                                pf.right = i;
                                i = this.mOverscanScreenTop + this.mOverscanScreenHeight;
                                of.bottom = i;
                                df.bottom = i;
                                pf.bottom = i;
                            } else if (!canHideNavigationBar() || (sysUiFl & 512) == 0 || attrs.type < 1 || attrs.type > 1999) {
                                i = this.mRestrictedOverscanScreenLeft;
                                df.left = i;
                                pf.left = i;
                                i = this.mRestrictedOverscanScreenTop;
                                df.top = i;
                                pf.top = i;
                                i = this.mRestrictedOverscanScreenLeft + this.mRestrictedOverscanScreenWidth;
                                df.right = i;
                                pf.right = i;
                                i = this.mRestrictedOverscanScreenTop + this.mRestrictedOverscanScreenHeight;
                                df.bottom = i;
                                pf.bottom = i;
                                of.left = this.mUnrestrictedScreenLeft;
                                of.top = this.mUnrestrictedScreenTop;
                                of.right = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                                of.bottom = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                            } else {
                                i = this.mOverscanScreenLeft;
                                df.left = i;
                                pf.left = i;
                                i = this.mOverscanScreenTop;
                                df.top = i;
                                pf.top = i;
                                i = this.mOverscanScreenLeft + this.mOverscanScreenWidth;
                                df.right = i;
                                pf.right = i;
                                i = this.mOverscanScreenTop + this.mOverscanScreenHeight;
                                df.bottom = i;
                                pf.bottom = i;
                                of.left = this.mUnrestrictedScreenLeft;
                                of.top = this.mUnrestrictedScreenTop;
                                of.right = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                                of.bottom = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                            }
                            if ((fl & 1024) != 0) {
                                cf.left = this.mRestrictedScreenLeft;
                                cf.top = this.mRestrictedScreenTop;
                                cf.right = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                                cf.bottom = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                            } else if (win.isVoiceInteraction()) {
                                cf.left = this.mVoiceContentLeft;
                                cf.top = this.mVoiceContentTop;
                                cf.right = this.mVoiceContentRight;
                                cf.bottom = this.mVoiceContentBottom;
                            } else if (adjust != 16) {
                                cf.left = this.mDockLeft;
                                cf.top = this.mDockTop;
                                cf.right = this.mDockRight;
                                cf.bottom = this.mDockBottom;
                            } else {
                                cf.left = this.mContentLeft;
                                cf.top = this.mContentTop;
                                cf.right = this.mContentRight;
                                cf.bottom = this.mContentBottom;
                            }
                            applyStableConstraints(sysUiFl, fl, cf);
                            if (adjust != 48) {
                                vf.left = this.mCurLeft;
                                vf.top = this.mCurTop;
                                vf.right = this.mCurRight;
                                vf.bottom = this.mCurBottom;
                            } else {
                                vf.set(cf);
                            }
                        }
                    } else if ((fl & 256) != 0 || (sysUiFl & UsbTerminalTypes.TERMINAL_EXTERN_UNDEFINED) != 0) {
                        if (attrs.type == 2014 || attrs.type == 2017) {
                            i = isVisibleLw ? this.mDockLeft : this.mUnrestrictedScreenLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mUnrestrictedScreenTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            if (isVisibleLw) {
                                i = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                            } else {
                                i = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                            }
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            if (!isVisibleLw || win == this.mLetterBoxBar) {
                                i = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                            } else {
                                i = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                            }
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                        } else if (attrs.type == 2020) {
                            i = this.mUnrestrictedScreenLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mUnrestrictedScreenTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            i = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            i = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                        } else if (attrs.type == 2019 || attrs.type == 2024) {
                            i = this.mUnrestrictedScreenLeft;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mUnrestrictedScreenTop;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            i = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            i = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                        } else if ((attrs.type == 2015 || attrs.type == 2021 || attrs.type == 2036) && (fl & 1024) != 0) {
                            i = this.mOverscanScreenLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mOverscanScreenTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            i = this.mOverscanScreenLeft + this.mOverscanScreenWidth;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            i = this.mOverscanScreenTop + this.mOverscanScreenHeight;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                        } else if (attrs.type == 2021) {
                            i = this.mOverscanScreenLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mOverscanScreenTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            i = this.mOverscanScreenLeft + this.mOverscanScreenWidth;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            i = this.mOverscanScreenTop + this.mOverscanScreenHeight;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                        } else if ((33554432 & fl) != 0 && attrs.type >= 1 && attrs.type <= 1999) {
                            i = this.mOverscanScreenLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mOverscanScreenTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            i = this.mOverscanScreenLeft + this.mOverscanScreenWidth;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            i = this.mOverscanScreenTop + this.mOverscanScreenHeight;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                        } else if (canHideNavigationBar() && (sysUiFl & 512) != 0 && (attrs.type == 2000 || attrs.type == 2005 || attrs.type == 2034 || attrs.type == 2033 || (attrs.type >= 1 && attrs.type <= 1999))) {
                            i = this.mUnrestrictedScreenLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mUnrestrictedScreenTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            i = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            i = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                        } else if ((sysUiFl & 1024) != 0) {
                            i = this.mRestrictedScreenLeft;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mRestrictedScreenTop;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            i = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            i = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                            if (adjust != 16) {
                                cf.left = this.mDockLeft;
                                cf.top = this.mDockTop;
                                cf.right = this.mDockRight;
                                cf.bottom = this.mDockBottom;
                            } else {
                                cf.left = this.mContentLeft;
                                cf.top = this.mContentTop;
                                cf.right = this.mContentRight;
                                cf.bottom = this.mContentBottom;
                            }
                        } else {
                            i = this.mRestrictedScreenLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            pf.left = i;
                            i = this.mRestrictedScreenTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            pf.top = i;
                            i = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            pf.right = i;
                            i = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                            pf.bottom = i;
                        }
                        applyStableConstraints(sysUiFl, fl, cf);
                        if (adjust != 48) {
                            vf.left = this.mCurLeft;
                            vf.top = this.mCurTop;
                            vf.right = this.mCurRight;
                            vf.bottom = this.mCurBottom;
                        } else {
                            vf.set(cf);
                        }
                    } else if (attached != null) {
                        setAttachedWindowFrames(win, fl, adjust, attached, false, pf, df, of, cf, vf);
                    } else if (attrs.type == 2014 || attrs.type == 2020) {
                        i = this.mRestrictedScreenLeft;
                        cf.left = i;
                        of.left = i;
                        df.left = i;
                        pf.left = i;
                        i = this.mRestrictedScreenTop;
                        cf.top = i;
                        of.top = i;
                        df.top = i;
                        pf.top = i;
                        i = this.mRestrictedScreenLeft + this.mRestrictedScreenWidth;
                        cf.right = i;
                        of.right = i;
                        df.right = i;
                        pf.right = i;
                        i = this.mRestrictedScreenTop + this.mRestrictedScreenHeight;
                        cf.bottom = i;
                        of.bottom = i;
                        df.bottom = i;
                        pf.bottom = i;
                    } else if (attrs.type == 2005 || attrs.type == 2003) {
                        i = this.mStableLeft;
                        cf.left = i;
                        of.left = i;
                        df.left = i;
                        pf.left = i;
                        i = this.mStableTop;
                        cf.top = i;
                        of.top = i;
                        df.top = i;
                        pf.top = i;
                        i = this.mStableRight;
                        cf.right = i;
                        of.right = i;
                        df.right = i;
                        pf.right = i;
                        i = this.mStableBottom;
                        cf.bottom = i;
                        of.bottom = i;
                        df.bottom = i;
                        pf.bottom = i;
                    } else {
                        pf.left = this.mContentLeft;
                        pf.top = this.mContentTop;
                        pf.right = this.mContentRight;
                        pf.bottom = this.mContentBottom;
                        if (win.isVoiceInteraction()) {
                            i = this.mVoiceContentLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            i = this.mVoiceContentTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            i = this.mVoiceContentRight;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            i = this.mVoiceContentBottom;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                        } else if (adjust != 16) {
                            i = this.mDockLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            i = this.mDockTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            i = this.mDockRight;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            i = this.mDockBottom;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                        } else {
                            i = this.mContentLeft;
                            cf.left = i;
                            of.left = i;
                            df.left = i;
                            i = this.mContentTop;
                            cf.top = i;
                            of.top = i;
                            df.top = i;
                            i = this.mContentRight;
                            cf.right = i;
                            of.right = i;
                            df.right = i;
                            i = this.mContentBottom;
                            cf.bottom = i;
                            of.bottom = i;
                            df.bottom = i;
                        }
                        if (adjust != 48) {
                            vf.left = this.mCurLeft;
                            vf.top = this.mCurTop;
                            vf.right = this.mCurRight;
                            vf.bottom = this.mCurBottom;
                        } else {
                            vf.set(cf);
                        }
                    }
                }
            } else if (attached != null) {
                setAttachedWindowFrames(win, fl, adjust, attached, true, pf, df, of, cf, vf);
            } else {
                i = this.mOverscanScreenLeft;
                cf.left = i;
                of.left = i;
                df.left = i;
                pf.left = i;
                i = this.mOverscanScreenTop;
                cf.top = i;
                of.top = i;
                df.top = i;
                pf.top = i;
                i = this.mOverscanScreenLeft + this.mOverscanScreenWidth;
                cf.right = i;
                of.right = i;
                df.right = i;
                pf.right = i;
                i = this.mOverscanScreenTop + this.mOverscanScreenHeight;
                cf.bottom = i;
                of.bottom = i;
                df.bottom = i;
                pf.bottom = i;
            }
            if (!((fl & 512) == 0 || attrs.type == 2010 || (win.isInMultiWindowMode() ^ 1) == 0)) {
                df.top = -10000;
                df.left = -10000;
                df.bottom = 10000;
                df.right = 10000;
                if (attrs.type != 2013) {
                    vf.top = -10000;
                    vf.left = -10000;
                    cf.top = -10000;
                    cf.left = -10000;
                    of.top = -10000;
                    of.left = -10000;
                    vf.bottom = 10000;
                    vf.right = 10000;
                    cf.bottom = 10000;
                    cf.right = 10000;
                    of.bottom = 10000;
                    of.right = 10000;
                }
            }
            boolean useOutsets = shouldUseOutsets(attrs, fl);
            if (isDefaultDisplay && useOutsets) {
                osf = mTmpOutsetFrame;
                osf.set(cf.left, cf.top, cf.right, cf.bottom);
                int outset = ScreenShapeHelper.getWindowOutsetBottomPx(this.mContext.getResources());
                if (outset > 0) {
                    int rotation = this.mDisplayRotation;
                    if (rotation == 0) {
                        osf.bottom += outset;
                    } else if (rotation == 1) {
                        osf.right += outset;
                    } else if (rotation == 2) {
                        osf.top -= outset;
                    } else if (rotation == 3) {
                        osf.left -= outset;
                    }
                }
            }
            if (!isDefaultDisplay || (win.isInMultiWindowMode() ^ 1) == 0 || attrs.type == 2034) {
                letterboxInsets = this.mEmptyRect;
            } else {
                if (attrs.type == 2011) {
                    letterboxInsets = getLetterboxForRotation(this.mDisplayRotation);
                } else {
                    letterboxInsets = EssentialScreenPolicy.getDisplayFrameInsets(attrs, sysUiFl, fl, this.mDisplayRotation);
                }
                if (attached == null) {
                    if ("com.felicanetworks.mfs".equals(attrs.packageName)) {
                        df.inset(letterboxInsets);
                    } else {
                        pf.inset(letterboxInsets);
                    }
                }
            }
            win.setLetterboxInsets(letterboxInsets);
            win.computeFrameLw(pf, df, of, cf, vf, dcf, sf, osf);
            if (attrs.type == 2011 && win.isVisibleLw() && (win.getGivenInsetsPendingLw() ^ 1) != 0) {
                setLastInputMethodWindowLw(null, null);
                offsetInputMethodWindowLw(win);
            }
            if (attrs.type == 2031 && win.isVisibleLw() && (win.getGivenInsetsPendingLw() ^ 1) != 0) {
                offsetVoiceInputWindowLw(win);
            }
        }
    }

    private void layoutWallpaper(WindowState win, Rect pf, Rect df, Rect of, Rect cf) {
        int i = this.mOverscanScreenLeft;
        df.left = i;
        pf.left = i;
        i = this.mOverscanScreenTop;
        df.top = i;
        pf.top = i;
        i = this.mOverscanScreenLeft + this.mOverscanScreenWidth;
        df.right = i;
        pf.right = i;
        i = this.mOverscanScreenTop + this.mOverscanScreenHeight;
        df.bottom = i;
        pf.bottom = i;
        i = this.mUnrestrictedScreenLeft;
        cf.left = i;
        of.left = i;
        i = this.mUnrestrictedScreenTop;
        cf.top = i;
        of.top = i;
        i = this.mUnrestrictedScreenLeft + this.mUnrestrictedScreenWidth;
        cf.right = i;
        of.right = i;
        i = this.mUnrestrictedScreenTop + this.mUnrestrictedScreenHeight;
        cf.bottom = i;
        of.bottom = i;
    }

    private void offsetInputMethodWindowLw(WindowState win) {
        int top = Math.max(win.getDisplayFrameLw().top, win.getContentFrameLw().top) + win.getGivenContentInsetsLw().top;
        if (this.mContentBottom > top) {
            this.mContentBottom = top;
        }
        if (this.mVoiceContentBottom > top) {
            this.mVoiceContentBottom = top;
        }
        top = win.getVisibleFrameLw().top + win.getGivenVisibleInsetsLw().top;
        if (this.mCurBottom > top) {
            this.mCurBottom = top;
        }
    }

    private void offsetVoiceInputWindowLw(WindowState win) {
        int top = Math.max(win.getDisplayFrameLw().top, win.getContentFrameLw().top) + win.getGivenContentInsetsLw().top;
        if (this.mVoiceContentBottom > top) {
            this.mVoiceContentBottom = top;
        }
    }

    public void finishLayoutLw() {
    }

    public void beginPostLayoutPolicyLw(int displayWidth, int displayHeight) {
        this.mTopFullscreenOpaqueWindowState = null;
        this.mTopFullscreenOpaqueOrDimmingWindowState = null;
        this.mTopDockedOpaqueWindowState = null;
        this.mTopDockedOpaqueOrDimmingWindowState = null;
        this.mForceStatusBar = false;
        this.mForceStatusBarFromKeyguard = false;
        this.mForceStatusBarTransparent = false;
        this.mForcingShowNavBar = false;
        this.mForcingShowNavBarLayer = -1;
        this.mAllowLockscreenWhenOn = false;
        this.mShowingDream = false;
        this.mWindowSleepTokenNeeded = false;
    }

    public void applyPostLayoutPolicyLw(WindowState win, LayoutParams attrs, WindowState attached, WindowState imeTarget) {
        boolean affectsSystemUi = win.canAffectSystemUiFlags();
        applyKeyguardPolicyLw(win, imeTarget);
        int fl = EssentialScreenPolicy.getWindowFlags(win, attrs);
        if (this.mTopFullscreenOpaqueWindowState == null && affectsSystemUi && attrs.type == 2011) {
            this.mForcingShowNavBar = true;
            this.mForcingShowNavBarLayer = win.getSurfaceLayer();
        }
        if (attrs.type == 2000) {
            if ((attrs.privateFlags & 1024) != 0) {
                this.mForceStatusBarFromKeyguard = true;
            }
            if ((attrs.privateFlags & 4096) != 0) {
                this.mForceStatusBarTransparent = true;
            }
        }
        boolean appWindow = attrs.type >= 1 ? attrs.type < 2000 : false;
        int stackId = win.getStackId();
        if (this.mTopFullscreenOpaqueWindowState == null && affectsSystemUi) {
            if ((fl & 2048) != 0) {
                this.mForceStatusBar = true;
            }
            if (attrs.type == 2023 && (!this.mDreamingLockscreen || (win.isVisibleLw() && win.hasDrawnLw()))) {
                this.mShowingDream = true;
                appWindow = true;
            }
            if (appWindow && attached == null && attrs.isFullscreen() && StackId.normallyFullscreenWindows(stackId)) {
                this.mTopFullscreenOpaqueWindowState = win;
                if (this.mTopFullscreenOpaqueOrDimmingWindowState == null) {
                    this.mTopFullscreenOpaqueOrDimmingWindowState = win;
                }
                if ((fl & 1) != 0) {
                    this.mAllowLockscreenWhenOn = true;
                }
            }
        }
        if (affectsSystemUi && win.getAttrs().type == 2031) {
            if (this.mTopFullscreenOpaqueWindowState == null) {
                this.mTopFullscreenOpaqueWindowState = win;
                if (this.mTopFullscreenOpaqueOrDimmingWindowState == null) {
                    this.mTopFullscreenOpaqueOrDimmingWindowState = win;
                }
            }
            if (this.mTopDockedOpaqueWindowState == null) {
                this.mTopDockedOpaqueWindowState = win;
                if (this.mTopDockedOpaqueOrDimmingWindowState == null) {
                    this.mTopDockedOpaqueOrDimmingWindowState = win;
                }
            }
        }
        if (this.mTopFullscreenOpaqueOrDimmingWindowState == null && affectsSystemUi && win.isDimming() && StackId.normallyFullscreenWindows(stackId)) {
            this.mTopFullscreenOpaqueOrDimmingWindowState = win;
        }
        if (this.mTopDockedOpaqueWindowState == null && affectsSystemUi && appWindow && attached == null && attrs.isFullscreen() && stackId == 3) {
            this.mTopDockedOpaqueWindowState = win;
            if (this.mTopDockedOpaqueOrDimmingWindowState == null) {
                this.mTopDockedOpaqueOrDimmingWindowState = win;
            }
        }
        if (this.mTopDockedOpaqueOrDimmingWindowState == null && affectsSystemUi && win.isDimming() && stackId == 3) {
            this.mTopDockedOpaqueOrDimmingWindowState = win;
        }
        if (win.isVisibleLw() && (attrs.privateFlags & DumpState.DUMP_COMPILER_STATS) != 0 && win.canAcquireSleepToken()) {
            this.mWindowSleepTokenNeeded = true;
        }
    }

    private void applyKeyguardPolicyLw(WindowState win, WindowState imeTarget) {
        if (!canBeHiddenByKeyguardLw(win)) {
            return;
        }
        if (shouldBeHiddenByKeyguard(win, imeTarget)) {
            win.hideLw(false);
        } else {
            win.showLw(false);
        }
    }

    public int finishPostLayoutPolicyLw() {
        int changes = 0;
        boolean z = false;
        if (this.mTopFullscreenOpaqueWindowState != null) {
            LayoutParams attrs = this.mTopFullscreenOpaqueWindowState.getAttrs();
        }
        if (!this.mShowingDream) {
            this.mDreamingLockscreen = isKeyguardShowingAndNotOccluded();
            if (this.mDreamingSleepTokenNeeded) {
                this.mDreamingSleepTokenNeeded = false;
                this.mHandler.obtainMessage(15, 0, 1).sendToTarget();
            }
        } else if (!this.mDreamingSleepTokenNeeded) {
            this.mDreamingSleepTokenNeeded = true;
            this.mHandler.obtainMessage(15, 1, 1).sendToTarget();
        }
        if (this.mStatusBar != null) {
            int shouldBeTransparent;
            if (!this.mForceStatusBarTransparent || (this.mForceStatusBar ^ 1) == 0) {
                shouldBeTransparent = 0;
            } else {
                shouldBeTransparent = this.mForceStatusBarFromKeyguard ^ 1;
            }
            if (shouldBeTransparent == 0) {
                this.mStatusBarController.setShowTransparent(false);
            } else if (!this.mStatusBar.isVisibleLw()) {
                this.mStatusBarController.setShowTransparent(true);
            }
            LayoutParams statusBarAttrs = this.mStatusBar.getAttrs();
            boolean statusBarExpanded = statusBarAttrs.height == -1 ? statusBarAttrs.width == -1 : false;
            boolean topAppHidesStatusBar = topAppHidesStatusBar();
            if (this.mForceStatusBar || this.mForceStatusBarFromKeyguard || this.mForceStatusBarTransparent || statusBarExpanded) {
                if (this.mStatusBarController.setBarShowingLw(true)) {
                    changes = 1;
                }
                z = this.mTopIsFullscreen ? this.mStatusBar.isAnimatingLw() : false;
                if ((this.mForceStatusBarFromKeyguard || statusBarExpanded) && this.mStatusBarController.isTransientShowing()) {
                    this.mStatusBarController.updateVisibilityLw(false, this.mLastSystemUiFlags, this.mLastSystemUiFlags);
                }
                if (statusBarExpanded && this.mNavigationBar != null && this.mNavigationBarController.setBarShowingLw(true)) {
                    changes |= 1;
                }
            } else if (this.mTopFullscreenOpaqueWindowState != null) {
                z = topAppHidesStatusBar;
                if (this.mStatusBarController.isTransientShowing()) {
                    if (this.mStatusBarController.setBarShowingLw(true)) {
                        changes = 1;
                    }
                } else if (!topAppHidesStatusBar || (this.mWindowManagerInternal.isStackVisible(2) ^ 1) == 0 || (this.mWindowManagerInternal.isStackVisible(3) ^ 1) == 0) {
                    if (this.mStatusBarController.setBarShowingLw(true)) {
                        changes = 1;
                    }
                    topAppHidesStatusBar = false;
                } else if (this.mStatusBarController.setBarShowingLw(false)) {
                    changes = 1;
                }
            }
            this.mStatusBarController.setTopAppHidesStatusBar(topAppHidesStatusBar);
        }
        if (this.mTopIsFullscreen != z) {
            if (!z) {
                changes |= 1;
            }
            this.mTopIsFullscreen = z;
        }
        if ((updateSystemUiVisibilityLw() & SYSTEM_UI_CHANGING_LAYOUT) != 0) {
            changes |= 1;
        }
        if (this.mShowingDream != this.mLastShowingDream) {
            this.mLastShowingDream = this.mShowingDream;
            this.mWindowManagerFuncs.notifyShowingDreamChanged();
        }
        if (this.mLetterBoxBar != null) {
            Rect insets = null;
            if (keyguardOn() && this.mTopFullscreenOpaqueWindowState != null) {
                insets = this.mTopFullscreenOpaqueWindowState.getLetterboxInsets();
            } else if (!keyguardOn()) {
                if (this.mTopFullscreenOpaqueWindowState == null) {
                    insets = EssentialScreenPolicy.getDisplayFrameInsets(null, 0, 0, this.mDisplayRotation);
                } else if (!this.mTopFullscreenOpaqueWindowState.isInMultiWindowMode()) {
                    insets = this.mTopFullscreenOpaqueWindowState.getLetterboxInsets();
                }
            }
            if (insets == null || (insets.left == 0 && insets.top == 0 && insets.right == 0 && insets.bottom == 0)) {
                if (this.mLetterBoxBar.isVisibleLw()) {
                    this.mLetterBoxBar.hideLw(false);
                }
            } else if (!this.mLetterBoxBar.isVisibleLw()) {
                this.mLetterBoxBar.showLw(false);
            }
            if (insets == null) {
                insets = new Rect();
            }
            this.mWindowManagerFuncs.setLetterBoxInsets(insets, this.mDisplayRotation);
        }
        updateWindowSleepToken();
        updateLockScreenTimeout();
        return changes;
    }

    public Rect getLetterboxForRotation(int rotation) {
        WindowState top = null;
        if (keyguardOn() && this.mTopFullscreenOpaqueWindowState != null) {
            top = this.mTopFullscreenOpaqueWindowState;
        } else if (!(keyguardOn() || this.mTopFullscreenOpaqueWindowState == null || (this.mTopFullscreenOpaqueWindowState.isInMultiWindowMode() ^ 1) == 0)) {
            top = this.mTopFullscreenOpaqueWindowState;
        }
        if (top == null) {
            return EssentialScreenPolicy.getDisplayFrameInsets(null, 0, 0, rotation);
        }
        LayoutParams attrs = top.getAttrs();
        return EssentialScreenPolicy.getDisplayFrameInsets(attrs, EssentialScreenPolicy.getSystemUiVisibility(top, null), EssentialScreenPolicy.getWindowFlags(top, attrs), rotation);
    }

    private void updateWindowSleepToken() {
        if (this.mWindowSleepTokenNeeded && (this.mLastWindowSleepTokenNeeded ^ 1) != 0) {
            this.mHandler.removeCallbacks(this.mReleaseSleepTokenRunnable);
            this.mHandler.post(this.mAcquireSleepTokenRunnable);
        } else if (!this.mWindowSleepTokenNeeded && this.mLastWindowSleepTokenNeeded) {
            this.mHandler.removeCallbacks(this.mAcquireSleepTokenRunnable);
            this.mHandler.post(this.mReleaseSleepTokenRunnable);
        }
        this.mLastWindowSleepTokenNeeded = this.mWindowSleepTokenNeeded;
    }

    private boolean topAppHidesStatusBar() {
        boolean z = true;
        if (this.mTopFullscreenOpaqueWindowState == null) {
            return false;
        }
        if ((EssentialScreenPolicy.getWindowFlags(null, this.mTopFullscreenOpaqueWindowState.getAttrs()) & 1024) == 0 && (this.mLastSystemUiFlags & 4) == 0) {
            z = false;
        }
        return z;
    }

    private boolean setKeyguardOccludedLw(boolean isOccluded, boolean force) {
        boolean wasOccluded = this.mKeyguardOccluded;
        boolean showing = this.mKeyguardDelegate.isShowing();
        boolean z = wasOccluded == isOccluded ? force : true;
        LayoutParams attrs;
        if (!isOccluded && z && showing) {
            this.mKeyguardOccluded = false;
            this.mKeyguardDelegate.setOccluded(false, true);
            if (this.mStatusBar != null) {
                attrs = this.mStatusBar.getAttrs();
                attrs.privateFlags |= 1024;
                if (!this.mKeyguardDelegate.hasLockscreenWallpaper()) {
                    attrs = this.mStatusBar.getAttrs();
                    attrs.flags |= DumpState.DUMP_DEXOPT;
                }
            }
            return true;
        } else if (isOccluded && z && showing) {
            this.mKeyguardOccluded = true;
            this.mKeyguardDelegate.setOccluded(true, false);
            if (this.mStatusBar != null) {
                attrs = this.mStatusBar.getAttrs();
                attrs.privateFlags &= -1025;
                attrs = this.mStatusBar.getAttrs();
                attrs.flags &= -1048577;
            }
            return true;
        } else if (!z) {
            return false;
        } else {
            this.mKeyguardOccluded = isOccluded;
            this.mKeyguardDelegate.setOccluded(isOccluded, false);
            return false;
        }
    }

    private boolean isStatusBarKeyguard() {
        if (this.mStatusBar == null || (this.mStatusBar.getAttrs().privateFlags & 1024) == 0) {
            return false;
        }
        return true;
    }

    public boolean allowAppAnimationsLw() {
        if (this.mShowingDream) {
            return false;
        }
        return true;
    }

    public int focusChangedLw(WindowState lastFocus, WindowState newFocus) {
        this.mFocusedWindow = newFocus;
        if ((updateSystemUiVisibilityLw() & SYSTEM_UI_CHANGING_LAYOUT) != 0) {
            return 1;
        }
        return 0;
    }

    public void notifyLidSwitchChanged(long whenNanos, boolean lidOpen) {
        int newLidState = lidOpen ? 1 : 0;
        if (newLidState != this.mLidState) {
            this.mLidState = newLidState;
            applyLidSwitchState();
            updateRotation(true);
            if (lidOpen) {
                wakeUp(SystemClock.uptimeMillis(), this.mAllowTheaterModeWakeFromLidSwitch, "android.policy:LID");
            } else if (!this.mLidControlsSleep) {
                this.mPowerManager.userActivity(SystemClock.uptimeMillis(), false);
            }
        }
    }

    public void notifyCameraLensCoverSwitchChanged(long whenNanos, boolean lensCovered) {
        int lensCoverState = lensCovered ? 1 : 0;
        if (this.mCameraLensCoverState != lensCoverState) {
            if (this.mCameraLensCoverState == 1 && lensCoverState == 0) {
                boolean z;
                Intent intent;
                if (this.mKeyguardDelegate == null) {
                    z = false;
                } else {
                    z = this.mKeyguardDelegate.isShowing();
                }
                if (z) {
                    intent = new Intent("android.media.action.STILL_IMAGE_CAMERA_SECURE");
                } else {
                    intent = new Intent("android.media.action.STILL_IMAGE_CAMERA");
                }
                wakeUp(whenNanos / 1000000, this.mAllowTheaterModeWakeFromCameraLens, "android.policy:CAMERA_COVER");
                startActivityAsUser(intent, UserHandle.CURRENT_OR_SELF);
            }
            this.mCameraLensCoverState = lensCoverState;
        }
    }

    void setHdmiPlugged(boolean plugged) {
        if (this.mHdmiPlugged != plugged) {
            this.mHdmiPlugged = plugged;
            updateRotation(true, true);
            Intent intent = new Intent("android.intent.action.HDMI_PLUGGED");
            intent.addFlags(67108864);
            intent.putExtra(AudioService.CONNECT_INTENT_KEY_STATE, plugged);
            this.mContext.sendStickyBroadcastAsUser(intent, UserHandle.ALL);
        }
    }

    void initializeHdmiState() {
        IOException ex;
        NumberFormatException ex2;
        Throwable th;
        boolean plugged = false;
        if (new File("/sys/devices/virtual/switch/hdmi/state").exists()) {
            this.mHDMIObserver.startObserving("DEVPATH=/devices/virtual/switch/hdmi");
            String filename = "/sys/class/switch/hdmi/state";
            FileReader fileReader = null;
            try {
                FileReader reader = new FileReader("/sys/class/switch/hdmi/state");
                try {
                    char[] buf = new char[15];
                    int n = reader.read(buf);
                    if (n > 1) {
                        plugged = Integer.parseInt(new String(buf, 0, n + -1)) != 0;
                    }
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IOException e2) {
                    ex = e2;
                    fileReader = reader;
                    Slog.w(TAG, "Couldn't read hdmi state from /sys/class/switch/hdmi/state: " + ex);
                    if (fileReader != null) {
                        try {
                            fileReader.close();
                        } catch (IOException e3) {
                        }
                    }
                    this.mHdmiPlugged = plugged ^ 1;
                    setHdmiPlugged(this.mHdmiPlugged ^ 1);
                } catch (NumberFormatException e4) {
                    ex2 = e4;
                    fileReader = reader;
                    try {
                        Slog.w(TAG, "Couldn't read hdmi state from /sys/class/switch/hdmi/state: " + ex2);
                        if (fileReader != null) {
                            try {
                                fileReader.close();
                            } catch (IOException e5) {
                            }
                        }
                        this.mHdmiPlugged = plugged ^ 1;
                        setHdmiPlugged(this.mHdmiPlugged ^ 1);
                    } catch (Throwable th2) {
                        th = th2;
                        if (fileReader != null) {
                            try {
                                fileReader.close();
                            } catch (IOException e6) {
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    fileReader = reader;
                    if (fileReader != null) {
                        fileReader.close();
                    }
                    throw th;
                }
            } catch (IOException e7) {
                ex = e7;
                Slog.w(TAG, "Couldn't read hdmi state from /sys/class/switch/hdmi/state: " + ex);
                if (fileReader != null) {
                    fileReader.close();
                }
                this.mHdmiPlugged = plugged ^ 1;
                setHdmiPlugged(this.mHdmiPlugged ^ 1);
            } catch (NumberFormatException e8) {
                ex2 = e8;
                Slog.w(TAG, "Couldn't read hdmi state from /sys/class/switch/hdmi/state: " + ex2);
                if (fileReader != null) {
                    fileReader.close();
                }
                this.mHdmiPlugged = plugged ^ 1;
                setHdmiPlugged(this.mHdmiPlugged ^ 1);
            }
        }
        this.mHdmiPlugged = plugged ^ 1;
        setHdmiPlugged(this.mHdmiPlugged ^ 1);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void takeScreenshot(final int r9) {
        /*
        r8 = this;
        r4 = r8.mScreenshotLock;
        monitor-enter(r4);
        r3 = r8.mScreenshotConnection;	 Catch:{ all -> 0x003b }
        if (r3 == 0) goto L_0x0009;
    L_0x0007:
        monitor-exit(r4);
        return;
    L_0x0009:
        r1 = new android.content.ComponentName;	 Catch:{ all -> 0x003b }
        r3 = "com.android.systemui";
        r5 = "com.android.systemui.screenshot.TakeScreenshotService";
        r1.<init>(r3, r5);	 Catch:{ all -> 0x003b }
        r2 = new android.content.Intent;	 Catch:{ all -> 0x003b }
        r2.<init>();	 Catch:{ all -> 0x003b }
        r2.setComponent(r1);	 Catch:{ all -> 0x003b }
        r0 = new com.android.server.policy.PhoneWindowManager$18;	 Catch:{ all -> 0x003b }
        r0.<init>(r9);	 Catch:{ all -> 0x003b }
        r3 = r8.mContext;	 Catch:{ all -> 0x003b }
        r5 = android.os.UserHandle.CURRENT;	 Catch:{ all -> 0x003b }
        r6 = 33554433; // 0x2000001 float:9.403956E-38 double:1.65780926E-316;
        r3 = r3.bindServiceAsUser(r2, r0, r6, r5);	 Catch:{ all -> 0x003b }
        if (r3 == 0) goto L_0x0039;
    L_0x002e:
        r8.mScreenshotConnection = r0;	 Catch:{ all -> 0x003b }
        r3 = r8.mHandler;	 Catch:{ all -> 0x003b }
        r5 = r8.mScreenshotTimeout;	 Catch:{ all -> 0x003b }
        r6 = 10000; // 0x2710 float:1.4013E-41 double:4.9407E-320;
        r3.postDelayed(r5, r6);	 Catch:{ all -> 0x003b }
    L_0x0039:
        monitor-exit(r4);
        return;
    L_0x003b:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.takeScreenshot(int):void");
    }

    private void notifyScreenshotError() {
        ComponentName errorComponent = new ComponentName(SYSUI_PACKAGE, SYSUI_SCREENSHOT_ERROR_RECEIVER);
        Intent errorIntent = new Intent("android.intent.action.USER_PRESENT");
        errorIntent.setComponent(errorComponent);
        errorIntent.addFlags(335544320);
        this.mContext.sendBroadcastAsUser(errorIntent, UserHandle.CURRENT);
    }

    public int interceptKeyBeforeQueueing(KeyEvent event, int policyFlags) {
        if (!this.mSystemBooted) {
            return 0;
        }
        boolean z;
        int isWakeKey;
        int result;
        boolean interactive = (536870912 & policyFlags) != 0;
        boolean down = event.getAction() == 0;
        boolean canceled = event.isCanceled();
        int keyCode = event.getKeyCode();
        boolean isInjected = (16777216 & policyFlags) != 0;
        if (this.mKeyguardDelegate == null) {
            z = false;
        } else if (interactive) {
            z = isKeyguardShowingAndNotOccluded();
        } else {
            z = this.mKeyguardDelegate.isShowing();
        }
        if ((policyFlags & 1) == 0) {
            isWakeKey = event.isWakeKey();
        } else {
            isWakeKey = 1;
        }
        if (interactive || (isInjected && (isWakeKey ^ 1) != 0)) {
            result = 1;
            isWakeKey = 0;
            if (interactive) {
                if (keyCode == this.mPendingWakeKey && (down ^ 1) != 0) {
                    result = 0;
                }
                this.mPendingWakeKey = -1;
            }
        } else if (interactive || !shouldDispatchInputWhenNonInteractive(event)) {
            result = 0;
            if (!(isWakeKey == 0 || (down && (isWakeKeyWhenScreenOff(keyCode) ^ 1) == 0))) {
                isWakeKey = 0;
            }
            if (isWakeKey != 0 && down) {
                this.mPendingWakeKey = keyCode;
            }
        } else {
            result = 1;
            this.mPendingWakeKey = -1;
        }
        if (isValidGlobalKey(keyCode) && this.mGlobalKeyManager.shouldHandleGlobalKey(keyCode, event)) {
            if (isWakeKey != 0) {
                wakeUp(event.getEventTime(), this.mAllowTheaterModeWakeFromKey, "android.policy:KEY");
            }
            return result;
        }
        boolean useHapticFeedback = (!down || (policyFlags & 2) == 0) ? false : event.getRepeatCount() == 0;
        TelecomManager telecomManager;
        Message msg;
        switch (keyCode) {
            case 4:
                if (!down) {
                    if (interceptBackKeyUp(event)) {
                        result &= -2;
                        break;
                    }
                }
                interceptBackKeyDown();
                break;
                break;
            case 5:
                if (down) {
                    telecomManager = getTelecommService();
                    if (telecomManager != null && telecomManager.isRinging()) {
                        Log.i(TAG, "interceptKeyBeforeQueueing: CALL key-down while ringing: Answer the call!");
                        telecomManager.acceptRingingCall();
                        result &= -2;
                        break;
                    }
                }
                break;
            case 6:
                result &= -2;
                if (!down) {
                    if (!this.mEndCallKeyHandled) {
                        this.mHandler.removeCallbacks(this.mEndCallLongPress);
                        if (!canceled && (((this.mEndcallBehavior & 1) == 0 || !goHome()) && (this.mEndcallBehavior & 2) != 0)) {
                            goToSleep(event.getEventTime(), 4, 0);
                            isWakeKey = 0;
                            break;
                        }
                    }
                }
                telecomManager = getTelecommService();
                int hungUp = 0;
                if (telecomManager != null) {
                    hungUp = telecomManager.endCall();
                }
                if (interactive && (r11 ^ 1) != 0) {
                    this.mEndCallKeyHandled = false;
                    this.mHandler.postDelayed(this.mEndCallLongPress, ViewConfiguration.get(this.mContext).getDeviceGlobalActionKeyTimeout());
                    break;
                }
                this.mEndCallKeyHandled = true;
                break;
                break;
            case 24:
            case 25:
            case 164:
                if (keyCode == 25) {
                    if (!down) {
                        this.mScreenshotChordVolumeDownKeyTriggered = false;
                        cancelPendingScreenshotChordAction();
                        cancelPendingAccessibilityShortcutAction();
                    } else if (interactive && (this.mScreenshotChordVolumeDownKeyTriggered ^ 1) != 0 && (event.getFlags() & 1024) == 0) {
                        this.mScreenshotChordVolumeDownKeyTriggered = true;
                        this.mScreenshotChordVolumeDownKeyTime = event.getDownTime();
                        this.mScreenshotChordVolumeDownKeyConsumed = false;
                        cancelPendingPowerKeyAction();
                        interceptScreenshotChord();
                        interceptAccessibilityShortcutChord();
                    }
                } else if (keyCode == 24) {
                    if (!down) {
                        this.mA11yShortcutChordVolumeUpKeyTriggered = false;
                        cancelPendingScreenshotChordAction();
                        cancelPendingAccessibilityShortcutAction();
                    } else if (interactive && (this.mA11yShortcutChordVolumeUpKeyTriggered ^ 1) != 0 && (event.getFlags() & 1024) == 0) {
                        this.mA11yShortcutChordVolumeUpKeyTriggered = true;
                        this.mA11yShortcutChordVolumeUpKeyTime = event.getDownTime();
                        this.mA11yShortcutChordVolumeUpKeyConsumed = false;
                        cancelPendingPowerKeyAction();
                        cancelPendingScreenshotChordAction();
                        interceptAccessibilityShortcutChord();
                    }
                }
                if (down) {
                    sendSystemKeyToStatusBarAsync(event.getKeyCode());
                    telecomManager = getTelecommService();
                    if (telecomManager != null && telecomManager.isRinging()) {
                        Log.i(TAG, "interceptKeyBeforeQueueing: VOLUME key-down while ringing: Silence ringer!");
                        telecomManager.silenceRinger();
                        result &= -2;
                        break;
                    }
                    int audioMode = 0;
                    try {
                        audioMode = getAudioService().getMode();
                    } catch (Exception e) {
                        Log.e(TAG, "Error getting AudioService in interceptKeyBeforeQueueing.", e);
                    }
                    boolean isInCall = (telecomManager == null || !telecomManager.isInCall()) ? audioMode == 3 : true;
                    if (isInCall && (result & 1) == 0) {
                        MediaSessionLegacyHelper.getHelper(this.mContext).sendVolumeKeyEvent(event, Integer.MIN_VALUE, false);
                        break;
                    }
                }
                if (!this.mUseTvRouting && !this.mHandleVolumeKeysInWM) {
                    if ((result & 1) == 0) {
                        MediaSessionLegacyHelper.getHelper(this.mContext).sendVolumeKeyEvent(event, Integer.MIN_VALUE, true);
                        break;
                    }
                }
                result |= 1;
                break;
                break;
            case 26:
                cancelPendingAccessibilityShortcutAction();
                result &= -2;
                isWakeKey = 0;
                if (!down) {
                    interceptPowerKeyUp(event, interactive, canceled);
                    break;
                }
                interceptPowerKeyDown(event, interactive);
                break;
            case HdmiCecKeycode.CEC_KEYCODE_RESERVED /*79*/:
            case HdmiCecKeycode.CEC_KEYCODE_INITIAL_CONFIGURATION /*85*/:
            case HdmiCecKeycode.CEC_KEYCODE_SELECT_BROADCAST_TYPE /*86*/:
            case HdmiCecKeycode.CEC_KEYCODE_SELECT_SOUND_PRESENTATION /*87*/:
            case 88:
            case 89:
            case 90:
            case 91:
            case 126:
            case 127:
            case 130:
            case NetdResponseCode.DnsProxyQueryResult /*222*/:
                if (MediaSessionLegacyHelper.getHelper(this.mContext).isGlobalPriorityActive()) {
                    result &= -2;
                }
                if ((result & 1) == 0) {
                    this.mBroadcastWakeLock.acquire();
                    msg = this.mHandler.obtainMessage(3, new KeyEvent(event));
                    msg.setAsynchronous(true);
                    msg.sendToTarget();
                    break;
                }
                break;
            case 171:
                if (this.mShortPressWindowBehavior == 1 && this.mPictureInPictureVisible) {
                    if (!down) {
                        showPictureInPictureMenu(event);
                    }
                    result &= -2;
                    break;
                }
            case NetdResponseCode.ClatdStatusResult /*223*/:
                result &= -2;
                isWakeKey = 0;
                if (!this.mPowerManager.isInteractive()) {
                    useHapticFeedback = false;
                }
                if (!down) {
                    sleepRelease(event.getEventTime());
                    break;
                }
                sleepPress(event.getEventTime());
                break;
            case 224:
                result &= -2;
                isWakeKey = 1;
                break;
            case 231:
                if ((result & 1) == 0 && (down ^ 1) != 0) {
                    this.mBroadcastWakeLock.acquire();
                    msg = this.mHandler.obtainMessage(12, z ? 1 : 0, 0);
                    msg.setAsynchronous(true);
                    msg.sendToTarget();
                    break;
                }
            case 276:
                result &= -2;
                isWakeKey = 0;
                if (!down) {
                    this.mPowerManagerInternal.setUserInactiveOverrideFromWindowManager();
                    break;
                }
                break;
            case 280:
            case 281:
            case 282:
            case 283:
                result &= -2;
                interceptSystemNavigationKey(event);
                break;
        }
        if (useHapticFeedback) {
            performHapticFeedbackLw(null, 1, false);
        }
        if (isWakeKey != 0) {
            wakeUp(event.getEventTime(), this.mAllowTheaterModeWakeFromKey, "android.policy:KEY");
        }
        return result;
    }

    private void interceptSystemNavigationKey(KeyEvent event) {
        if (event.getAction() != 1) {
            return;
        }
        if (!(this.mAccessibilityManager.isEnabled() && (this.mAccessibilityManager.sendFingerprintGesture(event.getKeyCode()) ^ 1) == 0) && areSystemNavigationKeysEnabled()) {
            sendSystemKeyToStatusBarAsync(event.getKeyCode());
        }
    }

    private void sendSystemKeyToStatusBar(int keyCode) {
        IStatusBarService statusBar = getStatusBarService();
        if (statusBar != null) {
            try {
                statusBar.handleSystemKey(keyCode);
            } catch (RemoteException e) {
            }
        }
    }

    private void sendSystemKeyToStatusBarAsync(int keyCode) {
        Message message = this.mHandler.obtainMessage(25, keyCode, 0);
        message.setAsynchronous(true);
        this.mHandler.sendMessage(message);
    }

    private static boolean isValidGlobalKey(int keyCode) {
        switch (keyCode) {
            case 26:
            case NetdResponseCode.ClatdStatusResult /*223*/:
            case 224:
                return false;
            default:
                return true;
        }
    }

    private boolean isWakeKeyWhenScreenOff(int keyCode) {
        boolean z = true;
        switch (keyCode) {
            case 24:
            case 25:
            case 164:
                if (this.mDockMode == 0) {
                    z = false;
                }
                return z;
            case 27:
            case HdmiCecKeycode.CEC_KEYCODE_RESERVED /*79*/:
            case HdmiCecKeycode.CEC_KEYCODE_INITIAL_CONFIGURATION /*85*/:
            case HdmiCecKeycode.CEC_KEYCODE_SELECT_BROADCAST_TYPE /*86*/:
            case HdmiCecKeycode.CEC_KEYCODE_SELECT_SOUND_PRESENTATION /*87*/:
            case 88:
            case 89:
            case 90:
            case 91:
            case 126:
            case 127:
            case 130:
            case NetdResponseCode.DnsProxyQueryResult /*222*/:
                return false;
            default:
                return true;
        }
    }

    public int interceptMotionBeforeQueueingNonInteractive(long whenNanos, int policyFlags) {
        if ((policyFlags & 1) != 0 && wakeUp(whenNanos / 1000000, this.mAllowTheaterModeWakeFromMotion, "android.policy:MOTION")) {
            return 0;
        }
        if (shouldDispatchInputWhenNonInteractive(null)) {
            return 1;
        }
        if (isTheaterModeEnabled() && (policyFlags & 1) != 0) {
            wakeUp(whenNanos / 1000000, this.mAllowTheaterModeWakeFromMotionWhenNotDreaming, "android.policy:MOTION");
        }
        return 0;
    }

    private boolean shouldDispatchInputWhenNonInteractive(KeyEvent event) {
        boolean displayOff = this.mDisplay == null || this.mDisplay.getState() == 1;
        if (displayOff && (this.mHasFeatureWatch ^ 1) != 0) {
            return false;
        }
        if (isKeyguardShowingAndNotOccluded() && (displayOff ^ 1) != 0) {
            return true;
        }
        if (this.mHasFeatureWatch && event != null && (event.getKeyCode() == 4 || event.getKeyCode() == DhcpPacket.MIN_PACKET_LENGTH_L3)) {
            return false;
        }
        IDreamManager dreamManager = getDreamManager();
        if (dreamManager != null) {
            try {
                if (dreamManager.isDreaming()) {
                    return true;
                }
            } catch (RemoteException e) {
                Slog.e(TAG, "RemoteException when checking if dreaming", e);
            }
        }
        return false;
    }

    private void dispatchDirectAudioEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            int keyCode = event.getKeyCode();
            String pkgName = this.mContext.getOpPackageName();
            switch (keyCode) {
                case 24:
                    try {
                        getAudioService().adjustSuggestedStreamVolume(1, Integer.MIN_VALUE, 4101, pkgName, TAG);
                        break;
                    } catch (Exception e) {
                        Log.e(TAG, "Error dispatching volume up in dispatchTvAudioEvent.", e);
                        break;
                    }
                case 25:
                    try {
                        getAudioService().adjustSuggestedStreamVolume(-1, Integer.MIN_VALUE, 4101, pkgName, TAG);
                        break;
                    } catch (Exception e2) {
                        Log.e(TAG, "Error dispatching volume down in dispatchTvAudioEvent.", e2);
                        break;
                    }
                case 164:
                    try {
                        if (event.getRepeatCount() == 0) {
                            getAudioService().adjustSuggestedStreamVolume(101, Integer.MIN_VALUE, 4101, pkgName, TAG);
                            break;
                        }
                    } catch (Exception e22) {
                        Log.e(TAG, "Error dispatching mute in dispatchTvAudioEvent.", e22);
                        break;
                    }
                    break;
            }
        }
    }

    void dispatchMediaKeyWithWakeLock(KeyEvent event) {
        if (this.mHavePendingMediaKeyRepeatWithWakeLock) {
            this.mHandler.removeMessages(4);
            this.mHavePendingMediaKeyRepeatWithWakeLock = false;
            this.mBroadcastWakeLock.release();
        }
        dispatchMediaKeyWithWakeLockToAudioService(event);
        if (event.getAction() == 0 && event.getRepeatCount() == 0) {
            this.mHavePendingMediaKeyRepeatWithWakeLock = true;
            Message msg = this.mHandler.obtainMessage(4, event);
            msg.setAsynchronous(true);
            this.mHandler.sendMessageDelayed(msg, (long) ViewConfiguration.getKeyRepeatTimeout());
            return;
        }
        this.mBroadcastWakeLock.release();
    }

    void dispatchMediaKeyRepeatWithWakeLock(KeyEvent event) {
        this.mHavePendingMediaKeyRepeatWithWakeLock = false;
        dispatchMediaKeyWithWakeLockToAudioService(KeyEvent.changeTimeRepeat(event, SystemClock.uptimeMillis(), 1, event.getFlags() | 128));
        this.mBroadcastWakeLock.release();
    }

    void dispatchMediaKeyWithWakeLockToAudioService(KeyEvent event) {
        if (this.mActivityManagerInternal.isSystemReady()) {
            MediaSessionLegacyHelper.getHelper(this.mContext).sendMediaButtonEvent(event, true);
        }
    }

    void launchVoiceAssistWithWakeLock(boolean keyguardActive) {
        IDeviceIdleController dic = IDeviceIdleController.Stub.asInterface(ServiceManager.getService("deviceidle"));
        if (dic != null) {
            try {
                dic.exitIdle("voice-search");
            } catch (RemoteException e) {
            }
        }
        Intent voiceIntent = new Intent("android.speech.action.VOICE_SEARCH_HANDS_FREE");
        voiceIntent.putExtra("android.speech.extras.EXTRA_SECURE", keyguardActive);
        startActivityAsUser(voiceIntent, UserHandle.CURRENT_OR_SELF);
        this.mBroadcastWakeLock.release();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void requestTransientBars(android.view.WindowManagerPolicy.WindowState r5) {
        /*
        r4 = this;
        r2 = r4.mWindowManagerFuncs;
        r3 = r2.getWindowManagerLock();
        monitor-enter(r3);
        r2 = r4.isUserSetupComplete();	 Catch:{ all -> 0x004b }
        if (r2 != 0) goto L_0x000f;
    L_0x000d:
        monitor-exit(r3);
        return;
    L_0x000f:
        r2 = r4.mStatusBarController;	 Catch:{ all -> 0x004b }
        r1 = r2.checkShowTransientBarLw();	 Catch:{ all -> 0x004b }
        r2 = r4.mNavigationBarController;	 Catch:{ all -> 0x004b }
        r2 = r2.checkShowTransientBarLw();	 Catch:{ all -> 0x004b }
        if (r2 == 0) goto L_0x0031;
    L_0x001d:
        r2 = r4.mLastSystemUiFlags;	 Catch:{ all -> 0x004b }
        r2 = isNavBarEmpty(r2);	 Catch:{ all -> 0x004b }
        r0 = r2 ^ 1;
    L_0x0025:
        if (r1 != 0) goto L_0x0029;
    L_0x0027:
        if (r0 == 0) goto L_0x0049;
    L_0x0029:
        if (r0 != 0) goto L_0x0033;
    L_0x002b:
        r2 = r4.mNavigationBar;	 Catch:{ all -> 0x004b }
        if (r5 != r2) goto L_0x0033;
    L_0x002f:
        monitor-exit(r3);
        return;
    L_0x0031:
        r0 = 0;
        goto L_0x0025;
    L_0x0033:
        if (r1 == 0) goto L_0x003a;
    L_0x0035:
        r2 = r4.mStatusBarController;	 Catch:{ all -> 0x004b }
        r2.showTransient();	 Catch:{ all -> 0x004b }
    L_0x003a:
        if (r0 == 0) goto L_0x0041;
    L_0x003c:
        r2 = r4.mNavigationBarController;	 Catch:{ all -> 0x004b }
        r2.showTransient();	 Catch:{ all -> 0x004b }
    L_0x0041:
        r2 = r4.mImmersiveModeConfirmation;	 Catch:{ all -> 0x004b }
        r2.confirmCurrentPrompt();	 Catch:{ all -> 0x004b }
        r4.updateSystemUiVisibilityLw();	 Catch:{ all -> 0x004b }
    L_0x0049:
        monitor-exit(r3);
        return;
    L_0x004b:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.requestTransientBars(android.view.WindowManagerPolicy$WindowState):void");
    }

    public void startedGoingToSleep(int why) {
        this.mGoingToSleep = true;
        this.mRequestedOrGoingToSleep = true;
        if (this.mKeyguardDelegate != null) {
            this.mKeyguardDelegate.onStartedGoingToSleep(why);
        }
    }

    public void finishedGoingToSleep(int why) {
        EventLog.writeEvent(70000, 0);
        MetricsLogger.histogram(this.mContext, "screen_timeout", this.mLockScreenTimeout / 1000);
        this.mGoingToSleep = false;
        this.mRequestedOrGoingToSleep = false;
        synchronized (this.mLock) {
            this.mAwake = false;
            updateWakeGestureListenerLp();
            updateOrientationListenerLp();
            updateLockScreenTimeout();
        }
        if (this.mKeyguardDelegate != null) {
            this.mKeyguardDelegate.onFinishedGoingToSleep(why, this.mCameraGestureTriggeredDuringGoingToSleep);
        }
        this.mCameraGestureTriggeredDuringGoingToSleep = false;
    }

    public void startedWakingUp() {
        EventLog.writeEvent(70000, 1);
        synchronized (this.mLock) {
            this.mAwake = true;
            updateWakeGestureListenerLp();
            updateOrientationListenerLp();
            updateLockScreenTimeout();
        }
        if (this.mKeyguardDelegate != null) {
            this.mKeyguardDelegate.onStartedWakingUp();
        }
    }

    public void finishedWakingUp() {
        if (this.mKeyguardDelegate != null) {
            this.mKeyguardDelegate.onFinishedWakingUp();
        }
    }

    private void wakeUpFromPowerKey(long eventTime) {
        wakeUp(eventTime, this.mAllowTheaterModeWakeFromPowerKey, "android.policy:POWER");
    }

    private boolean wakeUp(long wakeTime, boolean wakeInTheaterMode, String reason) {
        boolean theaterModeEnabled = isTheaterModeEnabled();
        if (!wakeInTheaterMode && theaterModeEnabled) {
            return false;
        }
        if (theaterModeEnabled) {
            Global.putInt(this.mContext.getContentResolver(), "theater_mode_on", 0);
        }
        this.mPowerManager.wakeUp(wakeTime, reason);
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void finishKeyguardDrawn() {
        /*
        r4 = this;
        r1 = r4.mLock;
        monitor-enter(r1);
        r0 = r4.mScreenOnEarly;	 Catch:{ all -> 0x0028 }
        if (r0 == 0) goto L_0x000b;
    L_0x0007:
        r0 = r4.mKeyguardDrawComplete;	 Catch:{ all -> 0x0028 }
        if (r0 == 0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r1);
        return;
    L_0x000d:
        r0 = 1;
        r4.mKeyguardDrawComplete = r0;	 Catch:{ all -> 0x0028 }
        r0 = r4.mKeyguardDelegate;	 Catch:{ all -> 0x0028 }
        if (r0 == 0) goto L_0x001a;
    L_0x0014:
        r0 = r4.mHandler;	 Catch:{ all -> 0x0028 }
        r2 = 6;
        r0.removeMessages(r2);	 Catch:{ all -> 0x0028 }
    L_0x001a:
        r0 = 0;
        r4.mWindowManagerDrawComplete = r0;	 Catch:{ all -> 0x0028 }
        monitor-exit(r1);
        r0 = r4.mWindowManagerInternal;
        r1 = r4.mWindowManagerDrawCallback;
        r2 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r0.waitForAllWindowsDrawn(r1, r2);
        return;
    L_0x0028:
        r0 = move-exception;
        monitor-exit(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.finishKeyguardDrawn():void");
    }

    public void screenTurnedOff() {
        updateScreenOffSleepToken(true);
        synchronized (this.mLock) {
            this.mScreenOnEarly = false;
            this.mScreenOnFully = false;
            this.mKeyguardDrawComplete = false;
            this.mWindowManagerDrawComplete = false;
            this.mScreenOnListener = null;
            updateOrientationListenerLp();
            if (this.mKeyguardDelegate != null) {
                this.mKeyguardDelegate.onScreenTurnedOff();
            }
        }
        reportScreenStateToVrManager(false);
    }

    private long getKeyguardDrawnTimeout() {
        return (long) (((SystemServiceManager) LocalServices.getService(SystemServiceManager.class)).isBootCompleted() ? 1000 : 5000);
    }

    public void screenTurningOn(ScreenOnListener screenOnListener) {
        updateScreenOffSleepToken(false);
        synchronized (this.mLock) {
            this.mScreenOnEarly = true;
            this.mScreenOnFully = false;
            this.mKeyguardDrawComplete = false;
            this.mWindowManagerDrawComplete = false;
            this.mScreenOnListener = screenOnListener;
            if (this.mKeyguardDelegate != null) {
                this.mHandler.removeMessages(6);
                this.mHandler.sendEmptyMessageDelayed(6, getKeyguardDrawnTimeout());
                this.mKeyguardDelegate.onScreenTurningOn(this.mKeyguardDrawnCallback);
            } else {
                finishKeyguardDrawn();
            }
        }
    }

    public void screenTurnedOn() {
        synchronized (this.mLock) {
            if (this.mKeyguardDelegate != null) {
                this.mKeyguardDelegate.onScreenTurnedOn();
            }
        }
        reportScreenStateToVrManager(true);
    }

    public void screenTurningOff(ScreenOffListener screenOffListener) {
        this.mWindowManagerFuncs.screenTurningOff(screenOffListener);
        synchronized (this.mLock) {
            if (this.mKeyguardDelegate != null) {
                this.mKeyguardDelegate.onScreenTurningOff();
            }
        }
    }

    private void reportScreenStateToVrManager(boolean isScreenOn) {
        if (this.mVrManagerInternal != null) {
            this.mVrManagerInternal.onScreenStateChanged(isScreenOn);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void finishWindowsDrawn() {
        /*
        r2 = this;
        r1 = r2.mLock;
        monitor-enter(r1);
        r0 = r2.mScreenOnEarly;	 Catch:{ all -> 0x0015 }
        if (r0 == 0) goto L_0x000b;
    L_0x0007:
        r0 = r2.mWindowManagerDrawComplete;	 Catch:{ all -> 0x0015 }
        if (r0 == 0) goto L_0x000d;
    L_0x000b:
        monitor-exit(r1);
        return;
    L_0x000d:
        r0 = 1;
        r2.mWindowManagerDrawComplete = r0;	 Catch:{ all -> 0x0015 }
        monitor-exit(r1);
        r2.finishScreenTurningOn();
        return;
    L_0x0015:
        r0 = move-exception;
        monitor-exit(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.finishWindowsDrawn():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void finishScreenTurningOn() {
        /*
        r5 = this;
        r3 = r5.mLock;
        monitor-enter(r3);
        r5.updateOrientationListenerLp();	 Catch:{ all -> 0x0026 }
        monitor-exit(r3);
        r4 = r5.mLock;
        monitor-enter(r4);
        r3 = r5.mScreenOnFully;	 Catch:{ all -> 0x0057 }
        if (r3 != 0) goto L_0x0024;
    L_0x000e:
        r3 = r5.mScreenOnEarly;	 Catch:{ all -> 0x0057 }
        r3 = r3 ^ 1;
        if (r3 != 0) goto L_0x0024;
    L_0x0014:
        r3 = r5.mWindowManagerDrawComplete;	 Catch:{ all -> 0x0057 }
        r3 = r3 ^ 1;
        if (r3 != 0) goto L_0x0024;
    L_0x001a:
        r3 = r5.mAwake;	 Catch:{ all -> 0x0057 }
        if (r3 == 0) goto L_0x0029;
    L_0x001e:
        r3 = r5.mKeyguardDrawComplete;	 Catch:{ all -> 0x0057 }
        r3 = r3 ^ 1;
        if (r3 == 0) goto L_0x0029;
    L_0x0024:
        monitor-exit(r4);
        return;
    L_0x0026:
        r4 = move-exception;
        monitor-exit(r3);
        throw r4;
    L_0x0029:
        r1 = r5.mScreenOnListener;	 Catch:{ all -> 0x0057 }
        r3 = 0;
        r5.mScreenOnListener = r3;	 Catch:{ all -> 0x0057 }
        r3 = 1;
        r5.mScreenOnFully = r3;	 Catch:{ all -> 0x0057 }
        r3 = r5.mKeyguardDrawnOnce;	 Catch:{ all -> 0x0057 }
        if (r3 != 0) goto L_0x0055;
    L_0x0035:
        r3 = r5.mAwake;	 Catch:{ all -> 0x0057 }
        if (r3 == 0) goto L_0x0055;
    L_0x0039:
        r3 = 1;
        r5.mKeyguardDrawnOnce = r3;	 Catch:{ all -> 0x0057 }
        r0 = 1;
        r3 = r5.mBootMessageNeedsHiding;	 Catch:{ all -> 0x0057 }
        if (r3 == 0) goto L_0x0047;
    L_0x0041:
        r3 = 0;
        r5.mBootMessageNeedsHiding = r3;	 Catch:{ all -> 0x0057 }
        r5.hideBootMessages();	 Catch:{ all -> 0x0057 }
    L_0x0047:
        monitor-exit(r4);
        if (r1 == 0) goto L_0x004d;
    L_0x004a:
        r1.onScreenOn();
    L_0x004d:
        if (r0 == 0) goto L_0x0054;
    L_0x004f:
        r3 = r5.mWindowManager;	 Catch:{ RemoteException -> 0x005a }
        r3.enableScreenIfNeeded();	 Catch:{ RemoteException -> 0x005a }
    L_0x0054:
        return;
    L_0x0055:
        r0 = 0;
        goto L_0x0047;
    L_0x0057:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
    L_0x005a:
        r2 = move-exception;
        goto L_0x0054;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.finishScreenTurningOn():void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handleHideBootMessage() {
        /*
        r3 = this;
        r2 = 0;
        r1 = r3.mLock;
        monitor-enter(r1);
        r0 = r3.mKeyguardDrawnOnce;	 Catch:{ all -> 0x001a }
        if (r0 != 0) goto L_0x000d;
    L_0x0008:
        r0 = 1;
        r3.mBootMessageNeedsHiding = r0;	 Catch:{ all -> 0x001a }
        monitor-exit(r1);
        return;
    L_0x000d:
        monitor-exit(r1);
        r0 = r3.mBootMsgDialog;
        if (r0 == 0) goto L_0x0019;
    L_0x0012:
        r0 = r3.mBootMsgDialog;
        r0.dismiss();
        r3.mBootMsgDialog = r2;
    L_0x0019:
        return;
    L_0x001a:
        r0 = move-exception;
        monitor-exit(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.handleHideBootMessage():void");
    }

    public boolean isScreenOn() {
        boolean z;
        synchronized (this.mLock) {
            z = this.mScreenOnEarly;
        }
        return z;
    }

    public boolean okToAnimate() {
        return this.mAwake ? this.mGoingToSleep ^ 1 : false;
    }

    public void enableKeyguard(boolean enabled) {
        if (this.mKeyguardDelegate != null) {
            this.mKeyguardDelegate.setKeyguardEnabled(enabled);
        }
    }

    public void exitKeyguardSecurely(OnKeyguardExitResult callback) {
        if (this.mKeyguardDelegate != null) {
            this.mKeyguardDelegate.verifyUnlock(callback);
        }
    }

    public boolean isKeyguardShowingAndNotOccluded() {
        boolean z = false;
        if (this.mKeyguardDelegate == null) {
            return false;
        }
        if (this.mKeyguardDelegate.isShowing()) {
            z = this.mKeyguardOccluded ^ 1;
        }
        return z;
    }

    public boolean isKeyguardTrustedLw() {
        if (this.mKeyguardDelegate == null) {
            return false;
        }
        return this.mKeyguardDelegate.isTrusted();
    }

    public boolean isKeyguardLocked() {
        return keyguardOn();
    }

    public boolean isKeyguardSecure(int userId) {
        if (this.mKeyguardDelegate == null) {
            return false;
        }
        return this.mKeyguardDelegate.isSecure(userId);
    }

    public boolean isKeyguardOccluded() {
        if (this.mKeyguardDelegate == null) {
            return false;
        }
        return this.mKeyguardOccluded;
    }

    public boolean inKeyguardRestrictedKeyInputMode() {
        if (this.mKeyguardDelegate == null) {
            return false;
        }
        return this.mKeyguardDelegate.isInputRestricted();
    }

    public void dismissKeyguardLw(IKeyguardDismissCallback callback) {
        if (this.mKeyguardDelegate != null && this.mKeyguardDelegate.isShowing()) {
            this.mKeyguardDelegate.dismiss(callback);
        } else if (callback != null) {
            try {
                callback.onDismissError();
            } catch (RemoteException e) {
                Slog.w(TAG, "Failed to call callback", e);
            }
        }
    }

    public boolean isKeyguardDrawnLw() {
        boolean z;
        synchronized (this.mLock) {
            z = this.mKeyguardDrawnOnce;
        }
        return z;
    }

    public boolean isShowingDreamLw() {
        return this.mShowingDream;
    }

    public void startKeyguardExitAnimation(long startTime, long fadeoutDuration) {
        if (this.mKeyguardDelegate != null) {
            this.mKeyguardDelegate.startKeyguardExitAnimation(startTime, fadeoutDuration);
        }
    }

    public void getStableInsetsLw(int displayRotation, int displayWidth, int displayHeight, Rect outInsets) {
        outInsets.setEmpty();
        getNonDecorInsetsLw(displayRotation, displayWidth, displayHeight, outInsets);
        outInsets.top = this.mStatusBarHeight;
    }

    public void getNonDecorInsetsLw(int displayRotation, int displayWidth, int displayHeight, Rect outInsets) {
        outInsets.setEmpty();
        if (this.mHasNavigationBar) {
            int position = navigationBarPosition(displayWidth, displayHeight, displayRotation);
            if (position == 4) {
                outInsets.bottom = getNavigationBarHeight(displayRotation, this.mUiMode);
            } else if (position == 2) {
                outInsets.right = getNavigationBarWidth(displayRotation, this.mUiMode);
            } else if (position == 1) {
                outInsets.left = getNavigationBarWidth(displayRotation, this.mUiMode);
            }
        }
    }

    public boolean isNavBarForcedShownLw(WindowState windowState) {
        return this.mForceShowSystemBars;
    }

    public int getNavBarPosition() {
        return this.mNavigationBarPosition;
    }

    public boolean isDockSideAllowed(int dockSide) {
        boolean z = true;
        if (this.mNavigationBarCanMove) {
            if (!(dockSide == 2 || dockSide == 1)) {
                z = false;
            }
            return z;
        }
        if (!(dockSide == 2 || dockSide == 1 || dockSide == 3)) {
            z = false;
        }
        return z;
    }

    void sendCloseSystemWindows() {
        PhoneWindow.sendCloseSystemWindows(this.mContext, null);
    }

    void sendCloseSystemWindows(String reason) {
        PhoneWindow.sendCloseSystemWindows(this.mContext, reason);
    }

    public int rotationForOrientationLw(int orientation, int lastRotation) {
        if (this.mForceDefaultOrientation) {
            return 0;
        }
        synchronized (this.mLock) {
            int preferredRotation;
            int i;
            int sensorRotation = this.mOrientationListener.getProposedRotation();
            if (sensorRotation < 0) {
                sensorRotation = lastRotation;
            }
            if (this.mLidState == 1 && this.mLidOpenRotation >= 0) {
                preferredRotation = this.mLidOpenRotation;
            } else if (this.mDockMode == 2 && (this.mCarDockEnablesAccelerometer || this.mCarDockRotation >= 0)) {
                preferredRotation = this.mCarDockEnablesAccelerometer ? sensorRotation : this.mCarDockRotation;
            } else if ((this.mDockMode == 1 || this.mDockMode == 3 || this.mDockMode == 4) && (this.mDeskDockEnablesAccelerometer || this.mDeskDockRotation >= 0)) {
                preferredRotation = this.mDeskDockEnablesAccelerometer ? sensorRotation : this.mDeskDockRotation;
            } else if (this.mHdmiPlugged && this.mDemoHdmiRotationLock) {
                preferredRotation = this.mDemoHdmiRotation;
            } else if (this.mHdmiPlugged && this.mDockMode == 0 && this.mUndockedHdmiRotation >= 0) {
                preferredRotation = this.mUndockedHdmiRotation;
            } else if (this.mDemoRotationLock) {
                preferredRotation = this.mDemoRotation;
            } else if (this.mPersistentVrModeEnabled) {
                preferredRotation = this.mPortraitRotation;
            } else if (orientation == 14) {
                preferredRotation = lastRotation;
            } else if (!this.mSupportAutoRotation) {
                preferredRotation = -1;
            } else if ((this.mUserRotationMode == 0 && (orientation == 2 || orientation == -1 || orientation == 11 || orientation == 12 || orientation == 13)) || orientation == 4 || orientation == 10 || orientation == 6 || orientation == 7) {
                if (this.mAllowAllRotations < 0) {
                    if (this.mContext.getResources().getBoolean(17956870)) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    this.mAllowAllRotations = i;
                }
                preferredRotation = (sensorRotation != 2 || this.mAllowAllRotations == 1 || orientation == 10 || orientation == 13) ? sensorRotation : lastRotation;
            } else {
                preferredRotation = (this.mUserRotationMode != 1 || orientation == 5) ? -1 : this.mUserRotation;
            }
            switch (orientation) {
                case 0:
                    if (isLandscapeOrSeascape(preferredRotation)) {
                        return preferredRotation;
                    }
                    i = this.mLandscapeRotation;
                    return i;
                case 1:
                    if (isAnyPortrait(preferredRotation)) {
                        return preferredRotation;
                    }
                    i = this.mPortraitRotation;
                    return i;
                case 6:
                case 11:
                    if (isLandscapeOrSeascape(preferredRotation)) {
                        return preferredRotation;
                    } else if (isLandscapeOrSeascape(lastRotation)) {
                        return lastRotation;
                    } else {
                        i = this.mLandscapeRotation;
                        return i;
                    }
                case 7:
                case 12:
                    if (isAnyPortrait(preferredRotation)) {
                        return preferredRotation;
                    } else if (isAnyPortrait(lastRotation)) {
                        return lastRotation;
                    } else {
                        i = this.mPortraitRotation;
                        return i;
                    }
                case 8:
                    if (isLandscapeOrSeascape(preferredRotation)) {
                        return preferredRotation;
                    }
                    i = this.mSeascapeRotation;
                    return i;
                case 9:
                    if (isAnyPortrait(preferredRotation)) {
                        return preferredRotation;
                    }
                    i = this.mUpsideDownRotation;
                    return i;
                default:
                    if (preferredRotation >= 0) {
                        return preferredRotation;
                    }
                    return 0;
            }
        }
    }

    public boolean rotationHasCompatibleMetricsLw(int orientation, int rotation) {
        switch (orientation) {
            case 0:
            case 6:
            case 8:
                return isLandscapeOrSeascape(rotation);
            case 1:
            case 7:
            case 9:
                return isAnyPortrait(rotation);
            default:
                return true;
        }
    }

    public void setRotationLw(int rotation) {
        this.mOrientationListener.setCurrentRotation(rotation);
    }

    private boolean isLandscapeOrSeascape(int rotation) {
        return rotation == this.mLandscapeRotation || rotation == this.mSeascapeRotation;
    }

    private boolean isAnyPortrait(int rotation) {
        return rotation == this.mPortraitRotation || rotation == this.mUpsideDownRotation;
    }

    public int getUserRotationMode() {
        if (System.getIntForUser(this.mContext.getContentResolver(), "accelerometer_rotation", 0, -2) != 0) {
            return 0;
        }
        return 1;
    }

    public void setUserRotationMode(int mode, int rot) {
        ContentResolver res = this.mContext.getContentResolver();
        if (mode == 1) {
            System.putIntForUser(res, "user_rotation", rot, -2);
            System.putIntForUser(res, "accelerometer_rotation", 0, -2);
            return;
        }
        System.putIntForUser(res, "accelerometer_rotation", 1, -2);
    }

    public void setSafeMode(boolean safeMode) {
        this.mSafeMode = safeMode;
        if (safeMode) {
            performHapticFeedbackLw(null, 10001, true);
        }
    }

    static long[] getLongIntArray(Resources r, int resid) {
        int[] ar = r.getIntArray(resid);
        if (ar == null) {
            return null;
        }
        long[] out = new long[ar.length];
        for (int i = 0; i < ar.length; i++) {
            out[i] = (long) ar[i];
        }
        return out;
    }

    private void bindKeyguard() {
        synchronized (this.mLock) {
            if (this.mKeyguardBound) {
                return;
            }
            this.mKeyguardBound = true;
            this.mKeyguardDelegate.bindService(this.mContext);
        }
    }

    public void onSystemUiStarted() {
        bindKeyguard();
    }

    public void systemReady() {
        this.mKeyguardDelegate.onSystemReady();
        this.mVrManagerInternal = (VrManagerInternal) LocalServices.getService(VrManagerInternal.class);
        if (this.mVrManagerInternal != null) {
            this.mVrManagerInternal.addPersistentVrModeStateListener(this.mPersistentVrModeListener);
        }
        readCameraLensCoverState();
        updateUiMode();
        synchronized (this.mLock) {
            updateOrientationListenerLp();
            this.mSystemReady = true;
            this.mHandler.post(new Runnable() {
                public void run() {
                    PhoneWindowManager.this.updateSettings();
                }
            });
            if (this.mSystemBooted) {
                this.mKeyguardDelegate.onBootCompleted();
            }
        }
        this.mSystemGestures.systemReady();
        this.mImmersiveModeConfirmation.systemReady();
        this.mAutofillManagerInternal = (AutofillManagerInternal) LocalServices.getService(AutofillManagerInternal.class);
    }

    public void systemBooted() {
        bindKeyguard();
        synchronized (this.mLock) {
            this.mSystemBooted = true;
            if (this.mSystemReady) {
                this.mKeyguardDelegate.onBootCompleted();
            }
        }
        startedWakingUp();
        screenTurningOn(null);
        screenTurnedOn();
    }

    public boolean canDismissBootAnimation() {
        boolean z;
        synchronized (this.mLock) {
            z = this.mKeyguardDrawComplete;
        }
        return z;
    }

    public void showBootMessage(final CharSequence msg, boolean always) {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (PhoneWindowManager.this.mBootMsgDialog == null) {
                    int theme;
                    if (PhoneWindowManager.this.mContext.getPackageManager().hasSystemFeature("android.software.leanback")) {
                        theme = 16974820;
                    } else {
                        theme = 0;
                    }
                    PhoneWindowManager.this.mBootMsgDialog = new ProgressDialog(PhoneWindowManager.this.mContext, theme) {
                        public boolean dispatchKeyEvent(KeyEvent event) {
                            return true;
                        }

                        public boolean dispatchKeyShortcutEvent(KeyEvent event) {
                            return true;
                        }

                        public boolean dispatchTouchEvent(MotionEvent ev) {
                            return true;
                        }

                        public boolean dispatchTrackballEvent(MotionEvent ev) {
                            return true;
                        }

                        public boolean dispatchGenericMotionEvent(MotionEvent ev) {
                            return true;
                        }

                        public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event) {
                            return true;
                        }
                    };
                    if (PhoneWindowManager.this.mContext.getPackageManager().isUpgrade()) {
                        PhoneWindowManager.this.mBootMsgDialog.setTitle(17039475);
                    } else {
                        PhoneWindowManager.this.mBootMsgDialog.setTitle(17039467);
                    }
                    PhoneWindowManager.this.mBootMsgDialog.setProgressStyle(0);
                    PhoneWindowManager.this.mBootMsgDialog.setIndeterminate(true);
                    PhoneWindowManager.this.mBootMsgDialog.getWindow().setType(2021);
                    PhoneWindowManager.this.mBootMsgDialog.getWindow().addFlags(258);
                    PhoneWindowManager.this.mBootMsgDialog.getWindow().setDimAmount(1.0f);
                    LayoutParams lp = PhoneWindowManager.this.mBootMsgDialog.getWindow().getAttributes();
                    lp.screenOrientation = 5;
                    PhoneWindowManager.this.mBootMsgDialog.getWindow().setAttributes(lp);
                    PhoneWindowManager.this.mBootMsgDialog.setCancelable(false);
                    PhoneWindowManager.this.mBootMsgDialog.show();
                }
                PhoneWindowManager.this.mBootMsgDialog.setMessage(msg);
            }
        });
    }

    public void hideBootMessages() {
        this.mHandler.sendEmptyMessage(11);
    }

    public void userActivity() {
        synchronized (this.mScreenLockTimeout) {
            if (this.mLockScreenTimerActive) {
                this.mHandler.removeCallbacks(this.mScreenLockTimeout);
                this.mHandler.postDelayed(this.mScreenLockTimeout, (long) this.mLockScreenTimeout);
            }
        }
    }

    public void lockNow(Bundle options) {
        this.mContext.enforceCallingOrSelfPermission("android.permission.DEVICE_POWER", null);
        this.mHandler.removeCallbacks(this.mScreenLockTimeout);
        if (options != null) {
            this.mScreenLockTimeout.setLockOptions(options);
        }
        this.mHandler.post(this.mScreenLockTimeout);
    }

    private void updateLockScreenTimeout() {
        synchronized (this.mScreenLockTimeout) {
            boolean isSecure;
            if (this.mAllowLockscreenWhenOn && this.mAwake && this.mKeyguardDelegate != null) {
                isSecure = this.mKeyguardDelegate.isSecure(this.mCurrentUserId);
            } else {
                isSecure = false;
            }
            if (this.mLockScreenTimerActive != isSecure) {
                if (isSecure) {
                    this.mHandler.removeCallbacks(this.mScreenLockTimeout);
                    this.mHandler.postDelayed(this.mScreenLockTimeout, (long) this.mLockScreenTimeout);
                } else {
                    this.mHandler.removeCallbacks(this.mScreenLockTimeout);
                }
                this.mLockScreenTimerActive = isSecure;
            }
        }
    }

    private void updateDreamingSleepToken(boolean acquire) {
        if (acquire) {
            if (this.mDreamingSleepToken == null) {
                this.mDreamingSleepToken = this.mActivityManagerInternal.acquireSleepToken("Dream", 0);
            }
        } else if (this.mDreamingSleepToken != null) {
            this.mDreamingSleepToken.release();
            this.mDreamingSleepToken = null;
        }
    }

    private void updateScreenOffSleepToken(boolean acquire) {
        if (acquire) {
            if (this.mScreenOffSleepToken == null) {
                this.mScreenOffSleepToken = this.mActivityManagerInternal.acquireSleepToken("ScreenOff", 0);
            }
        } else if (this.mScreenOffSleepToken != null) {
            this.mScreenOffSleepToken.release();
            this.mScreenOffSleepToken = null;
        }
    }

    public void enableScreenAfterBoot() {
        readLidState();
        applyLidSwitchState();
        updateRotation(true);
    }

    private void applyLidSwitchState() {
        if (this.mLidState == 0 && this.mLidControlsSleep) {
            goToSleep(SystemClock.uptimeMillis(), 3, 1);
        } else if (this.mLidState == 0 && this.mLidControlsScreenLock) {
            this.mWindowManagerFuncs.lockDeviceNow();
        }
        synchronized (this.mLock) {
            updateWakeGestureListenerLp();
        }
    }

    void updateUiMode() {
        if (this.mUiModeManager == null) {
            this.mUiModeManager = Stub.asInterface(ServiceManager.getService("uimode"));
        }
        try {
            this.mUiMode = this.mUiModeManager.getCurrentModeType();
        } catch (RemoteException e) {
        }
    }

    void updateRotation(boolean alwaysSendConfiguration) {
        try {
            this.mWindowManager.updateRotation(alwaysSendConfiguration, false);
        } catch (RemoteException e) {
        }
    }

    void updateRotation(boolean alwaysSendConfiguration, boolean forceRelayout) {
        try {
            this.mWindowManager.updateRotation(alwaysSendConfiguration, forceRelayout);
        } catch (RemoteException e) {
        }
    }

    Intent createHomeDockIntent() {
        Intent intent;
        if (this.mUiMode == 3) {
            if (this.mEnableCarDockHomeCapture) {
                intent = this.mCarDockIntent;
            }
            intent = null;
        } else {
            if (this.mUiMode != 2) {
                if (this.mUiMode == 6 && (this.mDockMode == 1 || this.mDockMode == 4 || this.mDockMode == 3)) {
                    intent = this.mDeskDockIntent;
                } else if (this.mUiMode == 7) {
                    intent = this.mVrHeadsetHomeIntent;
                }
            }
            intent = null;
        }
        if (intent == null) {
            return null;
        }
        ActivityInfo ai = null;
        ResolveInfo info = this.mContext.getPackageManager().resolveActivityAsUser(intent, 65664, this.mCurrentUserId);
        if (info != null) {
            ai = info.activityInfo;
        }
        if (ai == null || ai.metaData == null || !ai.metaData.getBoolean("android.dock_home")) {
            return null;
        }
        Intent intent2 = new Intent(intent);
        intent2.setClassName(ai.packageName, ai.name);
        return intent2;
    }

    void startDockOrHome(boolean fromHomeKey, boolean awakenFromDreams) {
        Intent intent;
        if (awakenFromDreams) {
            awakenDreams();
        }
        Intent dock = createHomeDockIntent();
        if (dock != null) {
            if (fromHomeKey) {
                try {
                    dock.putExtra("android.intent.extra.FROM_HOME_KEY", fromHomeKey);
                } catch (ActivityNotFoundException e) {
                }
            }
            startActivityAsUser(dock, UserHandle.CURRENT);
            return;
        }
        if (fromHomeKey) {
            intent = new Intent(this.mHomeIntent);
            intent.putExtra("android.intent.extra.FROM_HOME_KEY", fromHomeKey);
        } else {
            intent = this.mHomeIntent;
        }
        startActivityAsUser(intent, UserHandle.CURRENT);
    }

    boolean goHome() {
        if (isUserSetupComplete()) {
            try {
                if (SystemProperties.getInt("persist.sys.uts-test-mode", 0) == 1) {
                    Log.d(TAG, "UTS-TEST-MODE");
                } else {
                    ActivityManager.getService().stopAppSwitches();
                    sendCloseSystemWindows();
                    Intent dock = createHomeDockIntent();
                    if (dock != null && ActivityManager.getService().startActivityAsUser(null, null, dock, dock.resolveTypeIfNeeded(this.mContext.getContentResolver()), null, null, 0, 1, null, null, -2) == 1) {
                        return false;
                    }
                }
                if (ActivityManager.getService().startActivityAsUser(null, null, this.mHomeIntent, this.mHomeIntent.resolveTypeIfNeeded(this.mContext.getContentResolver()), null, null, 0, 1, null, null, -2) == 1) {
                    return false;
                }
            } catch (RemoteException e) {
            }
            return true;
        }
        Slog.i(TAG, "Not going home because user setup is in progress.");
        return false;
    }

    public void setCurrentOrientationLw(int newOrientation) {
        synchronized (this.mLock) {
            if (newOrientation != this.mCurrentAppOrientation) {
                this.mCurrentAppOrientation = newOrientation;
                updateOrientationListenerLp();
            }
        }
    }

    private boolean isTheaterModeEnabled() {
        return Global.getInt(this.mContext.getContentResolver(), "theater_mode_on", 0) == 1;
    }

    private boolean areSystemNavigationKeysEnabled() {
        return Secure.getIntForUser(this.mContext.getContentResolver(), "system_navigation_keys_enabled", 0, -2) == 1;
    }

    public boolean performHapticFeedbackLw(WindowState win, int effectId, boolean always) {
        if (!this.mVibrator.hasVibrator()) {
            return false;
        }
        if ((System.getIntForUser(this.mContext.getContentResolver(), "haptic_feedback_enabled", 0, -2) == 0) && (always ^ 1) != 0) {
            return false;
        }
        VibrationEffect effect = getVibrationEffect(effectId);
        if (effect == null) {
            return false;
        }
        int owningUid;
        String owningPackage;
        if (win != null) {
            owningUid = win.getOwningUid();
            owningPackage = win.getOwningPackage();
        } else {
            owningUid = Process.myUid();
            owningPackage = this.mContext.getOpPackageName();
        }
        this.mVibrator.vibrate(owningUid, owningPackage, effect, VIBRATION_ATTRIBUTES);
        return true;
    }

    private VibrationEffect getVibrationEffect(int effectId) {
        long[] pattern;
        switch (effectId) {
            case 0:
                pattern = this.mLongPressVibePattern;
                break;
            case 1:
                return VibrationEffect.get(0);
            case 3:
                return VibrationEffect.get(0);
            case 4:
                return VibrationEffect.get(2);
            case 5:
                pattern = this.mCalendarDateVibePattern;
                break;
            case 6:
                return VibrationEffect.get(2);
            case 7:
                return VibrationEffect.get(2, false);
            case 8:
                return VibrationEffect.get(2, false);
            case 9:
                return VibrationEffect.get(2, false);
            case 10001:
                pattern = this.mSafeModeEnabledVibePattern;
                break;
            default:
                return null;
        }
        if (pattern.length == 0) {
            return null;
        }
        if (pattern.length == 1) {
            return VibrationEffect.createOneShot(pattern[0], -1);
        }
        return VibrationEffect.createWaveform(pattern, -1);
    }

    public void keepScreenOnStartedLw() {
    }

    public void keepScreenOnStoppedLw() {
        if (isKeyguardShowingAndNotOccluded()) {
            this.mPowerManager.userActivity(SystemClock.uptimeMillis(), false);
        }
    }

    private int updateSystemUiVisibilityLw() {
        WindowState winCandidate;
        if (this.mFocusedWindow != null) {
            winCandidate = this.mFocusedWindow;
        } else {
            winCandidate = this.mTopFullscreenOpaqueWindowState;
        }
        if (winCandidate == null) {
            return 0;
        }
        if (winCandidate.getAttrs().token == this.mImmersiveModeConfirmation.getWindowToken()) {
            winCandidate = isStatusBarKeyguard() ? this.mStatusBar : this.mTopFullscreenOpaqueWindowState;
            if (winCandidate == null) {
                return 0;
            }
        }
        final WindowState win = winCandidate;
        if ((winCandidate.getAttrs().privateFlags & 1024) != 0 && this.mKeyguardOccluded) {
            return 0;
        }
        int tmpVisibility = (EssentialScreenPolicy.getSystemUiVisibility(win, null) & (~this.mResettingSystemUiFlags)) & (~this.mForceClearedSystemUiFlags);
        if (this.mForcingShowNavBar && win.getSurfaceLayer() < this.mForcingShowNavBarLayer) {
            tmpVisibility &= ~EssentialScreenPolicy.adjustClearableFlags(win, 7);
        }
        final int fullscreenVisibility = updateLightStatusBarLw(0, this.mTopFullscreenOpaqueWindowState, this.mTopFullscreenOpaqueOrDimmingWindowState);
        final int dockedVisibility = updateLightStatusBarLw(0, this.mTopDockedOpaqueWindowState, this.mTopDockedOpaqueOrDimmingWindowState);
        this.mWindowManagerFuncs.getStackBounds(0, this.mNonDockedStackBounds);
        this.mWindowManagerFuncs.getStackBounds(3, this.mDockedStackBounds);
        final int visibility = updateSystemBarsLw(win, this.mLastSystemUiFlags, tmpVisibility);
        int diff = visibility ^ this.mLastSystemUiFlags;
        int fullscreenDiff = fullscreenVisibility ^ this.mLastFullscreenStackSysUiFlags;
        int dockedDiff = dockedVisibility ^ this.mLastDockedStackSysUiFlags;
        final boolean needsMenu = win.getNeedsMenuLw(this.mTopFullscreenOpaqueWindowState);
        if (diff == 0 && fullscreenDiff == 0 && dockedDiff == 0 && this.mLastFocusNeedsMenu == needsMenu && this.mFocusedApp == win.getAppToken() && this.mLastNonDockedStackBounds.equals(this.mNonDockedStackBounds) && this.mLastDockedStackBounds.equals(this.mDockedStackBounds)) {
            return 0;
        }
        this.mLastSystemUiFlags = visibility;
        this.mLastFullscreenStackSysUiFlags = fullscreenVisibility;
        this.mLastDockedStackSysUiFlags = dockedVisibility;
        this.mLastFocusNeedsMenu = needsMenu;
        this.mFocusedApp = win.getAppToken();
        final Rect fullscreenStackBounds = new Rect(this.mNonDockedStackBounds);
        final Rect dockedStackBounds = new Rect(this.mDockedStackBounds);
        this.mHandler.post(new Runnable() {
            public void run() {
                StatusBarManagerInternal statusbar = PhoneWindowManager.this.getStatusBarManagerInternal();
                if (statusbar != null) {
                    statusbar.setSystemUiVisibility(visibility, fullscreenVisibility, dockedVisibility, -1, fullscreenStackBounds, dockedStackBounds, win.toString());
                    statusbar.topAppWindowChanged(needsMenu);
                }
            }
        });
        return diff;
    }

    private int updateLightStatusBarLw(int vis, WindowState opaque, WindowState opaqueOrDimming) {
        int i = isStatusBarKeyguard() ? this.mKeyguardOccluded ^ 1 : 0;
        WindowState statusColorWin = i != 0 ? this.mStatusBar : opaqueOrDimming;
        if (statusColorWin != null && (statusColorWin == opaque || i != 0)) {
            vis = (vis & -8193) | (EssentialScreenPolicy.getSystemUiVisibility(statusColorWin, null) & 8192);
        } else if (statusColorWin != null && statusColorWin.isDimming()) {
            vis &= -8193;
        }
        if (opaque == null || (vis & 8192) == 0 || opaque.getLetterboxInsets().top == 0) {
            return vis;
        }
        return vis & -8193;
    }

    private int updateLightNavigationBarLw(int vis, WindowState opaque, WindowState opaqueOrDimming) {
        WindowState navColorWin;
        WindowState imeWin = this.mWindowManagerFuncs.getInputMethodWindowLw();
        if (imeWin != null && imeWin.isVisibleLw() && this.mNavigationBarPosition == 4) {
            navColorWin = imeWin;
        } else {
            navColorWin = opaqueOrDimming;
        }
        if (navColorWin != null) {
            if (navColorWin == opaque) {
                vis = (vis & -17) | (EssentialScreenPolicy.getSystemUiVisibility(navColorWin, null) & 16);
            } else if (navColorWin.isDimming() || navColorWin == imeWin) {
                vis &= -17;
            }
        }
        if (opaque == null || (vis & 16) == 0 || opaque.getLetterboxInsets().bottom == 0) {
            return vis;
        }
        return vis & -17;
    }

    private boolean drawsSystemBarBackground(WindowState win) {
        return win == null || (win.getAttrs().flags & Integer.MIN_VALUE) != 0;
    }

    private boolean forcesDrawStatusBarBackground(WindowState win) {
        return win == null || (win.getAttrs().privateFlags & DumpState.DUMP_INTENT_FILTER_VERIFIERS) != 0;
    }

    private int updateSystemBarsLw(WindowState win, int oldVis, int vis) {
        WindowState fullscreenTransWin;
        boolean z;
        boolean transientNavBarAllowed;
        boolean denyTransientStatus;
        boolean dockedStackVisible = this.mWindowManagerInternal.isStackVisible(3);
        boolean freeformStackVisible = this.mWindowManagerInternal.isStackVisible(2);
        boolean resizing = this.mWindowManagerInternal.isDockedDividerResizing();
        boolean z2 = (dockedStackVisible || freeformStackVisible) ? true : resizing;
        this.mForceShowSystemBars = z2;
        int i = this.mForceShowSystemBars ? this.mForceStatusBarFromKeyguard ^ 1 : 0;
        if (!isStatusBarKeyguard() || (this.mKeyguardOccluded ^ 1) == 0) {
            fullscreenTransWin = this.mTopFullscreenOpaqueWindowState;
        } else {
            fullscreenTransWin = this.mStatusBar;
        }
        vis = this.mNavigationBarController.applyTranslucentFlagLw(fullscreenTransWin, this.mStatusBarController.applyTranslucentFlagLw(fullscreenTransWin, vis, oldVis), oldVis);
        int dockedVis = this.mStatusBarController.applyTranslucentFlagLw(this.mTopDockedOpaqueWindowState, 0, 0);
        if (drawsSystemBarBackground(this.mTopFullscreenOpaqueWindowState) && (1073741824 & vis) == 0) {
            z = true;
        } else {
            z = forcesDrawStatusBarBackground(this.mTopFullscreenOpaqueWindowState);
        }
        boolean z3;
        if (drawsSystemBarBackground(this.mTopDockedOpaqueWindowState) && (1073741824 & dockedVis) == 0) {
            z3 = true;
        } else {
            z3 = forcesDrawStatusBarBackground(this.mTopDockedOpaqueWindowState);
        }
        boolean statusBarHasFocus = win.getAttrs().type == 2000;
        if (statusBarHasFocus && (isStatusBarKeyguard() ^ 1) != 0) {
            int flags = 14342;
            if (this.mKeyguardOccluded) {
                flags = -1073727482;
            }
            vis = ((~flags) & vis) | (oldVis & flags);
        }
        if (z && r8) {
            vis = (vis | 8) & -1073741825;
        } else {
            if (areTranslucentBarsAllowed() || fullscreenTransWin == this.mStatusBar) {
                if (i != 0) {
                }
            }
            vis &= -1073741833;
        }
        vis = configureNavBarOpacity(vis, dockedStackVisible, freeformStackVisible, resizing);
        boolean immersiveSticky = (vis & 4096) != 0;
        boolean hideStatusBarWM = this.mTopFullscreenOpaqueWindowState != null ? (EssentialScreenPolicy.getWindowFlags(this.mTopFullscreenOpaqueWindowState, null) & 1024) != 0 : false;
        boolean hideStatusBarSysui = (vis & 4) != 0;
        boolean hideNavBarSysui = (vis & 2) != 0;
        boolean z4 = this.mStatusBar != null ? !statusBarHasFocus ? !this.mForceShowSystemBars ? !hideStatusBarWM ? hideStatusBarSysui ? immersiveSticky : false : true : false : true : false;
        if (this.mNavigationBar == null || (this.mForceShowSystemBars ^ 1) == 0 || !hideNavBarSysui) {
            transientNavBarAllowed = false;
        } else {
            transientNavBarAllowed = immersiveSticky;
        }
        boolean pendingPanic = this.mPendingPanicGestureUptime != 0 ? SystemClock.uptimeMillis() - this.mPendingPanicGestureUptime <= 30000 : false;
        if (pendingPanic && hideNavBarSysui && (isStatusBarKeyguard() ^ 1) != 0 && this.mKeyguardDrawComplete) {
            this.mPendingPanicGestureUptime = 0;
            this.mStatusBarController.showTransient();
            if (!isNavBarEmpty(vis)) {
                this.mNavigationBarController.showTransient();
            }
        }
        if (!this.mStatusBarController.isTransientShowRequested() || (z4 ^ 1) == 0) {
            denyTransientStatus = false;
        } else {
            denyTransientStatus = hideStatusBarSysui;
        }
        int i2;
        if (this.mNavigationBarController.isTransientShowRequested()) {
            i2 = transientNavBarAllowed ^ 1;
        } else {
            i2 = 0;
        }
        if (denyTransientStatus || r6 != 0 || this.mForceShowSystemBars) {
            clearClearableFlagsLw();
            vis &= -8;
        }
        int i3 = !((vis & 2048) != 0) ? (vis & 4096) != 0 : 1;
        if (hideNavBarSysui && (i3 ^ 1) != 0 && getWindowLayerLw(win) > getWindowLayerFromTypeLw(2022)) {
            vis &= -3;
        }
        vis = this.mStatusBarController.updateVisibilityLw(z4, oldVis, vis);
        boolean oldImmersiveMode = isImmersiveMode(oldVis);
        boolean newImmersiveMode = isImmersiveMode(vis);
        if (!(win == null || oldImmersiveMode == newImmersiveMode)) {
            this.mImmersiveModeConfirmation.immersiveModeChangedLw(win.getOwningPackage(), newImmersiveMode, isUserSetupComplete(), isNavBarEmpty(win.getSystemUiVisibility()));
        }
        return updateLightNavigationBarLw(this.mNavigationBarController.updateVisibilityLw(transientNavBarAllowed, oldVis, vis), this.mTopFullscreenOpaqueWindowState, this.mTopFullscreenOpaqueOrDimmingWindowState);
    }

    private int configureNavBarOpacity(int visibility, boolean dockedStackVisible, boolean freeformStackVisible, boolean isDockedDividerResizing) {
        if (this.mNavBarOpacityMode == 0) {
            if (dockedStackVisible || freeformStackVisible || isDockedDividerResizing) {
                visibility = setNavBarOpaqueFlag(visibility);
            }
        } else if (this.mNavBarOpacityMode == 1) {
            if (isDockedDividerResizing) {
                visibility = setNavBarOpaqueFlag(visibility);
            } else if (freeformStackVisible) {
                visibility = setNavBarTranslucentFlag(visibility);
            } else {
                visibility = setNavBarOpaqueFlag(visibility);
            }
        }
        if (areTranslucentBarsAllowed()) {
            return visibility;
        }
        return visibility & Integer.MAX_VALUE;
    }

    private int setNavBarOpaqueFlag(int visibility) {
        return visibility & 2147450879;
    }

    private int setNavBarTranslucentFlag(int visibility) {
        return (visibility & -32769) | Integer.MIN_VALUE;
    }

    private void clearClearableFlagsLw() {
        int newVal = this.mResettingSystemUiFlags | 7;
        if (newVal != this.mResettingSystemUiFlags) {
            this.mResettingSystemUiFlags = newVal;
            this.mWindowManagerFuncs.reevaluateStatusBarVisibility();
        }
    }

    private boolean isImmersiveMode(int vis) {
        if (this.mNavigationBar == null || (vis & 2) == 0 || (vis & 6144) == 0) {
            return false;
        }
        return canHideNavigationBar();
    }

    private static boolean isNavBarEmpty(int systemUiFlags) {
        return (systemUiFlags & 23068672) == 23068672;
    }

    private boolean areTranslucentBarsAllowed() {
        return this.mTranslucentDecorEnabled;
    }

    public boolean hasNavigationBar() {
        return this.mHasNavigationBar;
    }

    public void setLastInputMethodWindowLw(WindowState ime, WindowState target) {
        this.mLastInputMethodWindow = ime;
        this.mLastInputMethodTargetWindow = target;
    }

    public void setDismissImeOnBackKeyPressed(boolean newValue) {
        this.mDismissImeOnBackKeyPressed = newValue;
    }

    public int getInputMethodWindowVisibleHeightLw() {
        return this.mDockBottom - this.mCurBottom;
    }

    public void setCurrentUserLw(int newUserId) {
        this.mCurrentUserId = newUserId;
        if (this.mKeyguardDelegate != null) {
            this.mKeyguardDelegate.setCurrentUser(newUserId);
        }
        if (this.mAccessibilityShortcutController != null) {
            this.mAccessibilityShortcutController.setCurrentUser(newUserId);
        }
        StatusBarManagerInternal statusBar = getStatusBarManagerInternal();
        if (statusBar != null) {
            statusBar.setCurrentUser(newUserId);
        }
        setLastInputMethodWindowLw(null, null);
    }

    public void setSwitchingUser(boolean switching) {
        this.mKeyguardDelegate.setSwitchingUser(switching);
    }

    public boolean canMagnifyWindow(int windowType) {
        switch (windowType) {
            case 2011:
            case 2012:
            case 2019:
            case 2027:
                return false;
            default:
                return true;
        }
    }

    public boolean isTopLevelWindow(int windowType) {
        boolean z = true;
        if (windowType < 1000 || windowType > 1999) {
            return true;
        }
        if (windowType != 1003) {
            z = false;
        }
        return z;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean shouldRotateSeamlessly(int r6, int r7) {
        /*
        r5 = this;
        r3 = 2;
        r4 = 0;
        r2 = r5.mUpsideDownRotation;
        if (r6 == r2) goto L_0x000a;
    L_0x0006:
        r2 = r5.mUpsideDownRotation;
        if (r7 != r2) goto L_0x000b;
    L_0x000a:
        return r4;
    L_0x000b:
        r2 = r5.mNavigationBarCanMove;
        if (r2 != 0) goto L_0x0010;
    L_0x000f:
        return r4;
    L_0x0010:
        r0 = r7 - r6;
        if (r0 >= 0) goto L_0x0016;
    L_0x0014:
        r0 = r0 + 4;
    L_0x0016:
        if (r0 != r3) goto L_0x0019;
    L_0x0018:
        return r4;
    L_0x0019:
        r1 = r5.mTopFullscreenOpaqueWindowState;
        r2 = r5.mFocusedWindow;
        if (r1 == r2) goto L_0x0020;
    L_0x001f:
        return r4;
    L_0x0020:
        if (r1 == 0) goto L_0x003d;
    L_0x0022:
        r2 = r1.isAnimatingLw();
        r2 = r2 ^ 1;
        if (r2 == 0) goto L_0x003d;
    L_0x002a:
        r2 = r1.getAttrs();
        r2 = r2.rotationAnimation;
        if (r2 == r3) goto L_0x003b;
    L_0x0032:
        r2 = r1.getAttrs();
        r2 = r2.rotationAnimation;
        r3 = 3;
        if (r2 != r3) goto L_0x003d;
    L_0x003b:
        r2 = 1;
        return r2;
    L_0x003d:
        return r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.policy.PhoneWindowManager.shouldRotateSeamlessly(int, int):boolean");
    }

    public void dump(String prefix, PrintWriter pw, String[] args) {
        pw.print(prefix);
        pw.print("mSafeMode=");
        pw.print(this.mSafeMode);
        pw.print(" mSystemReady=");
        pw.print(this.mSystemReady);
        pw.print(" mSystemBooted=");
        pw.println(this.mSystemBooted);
        pw.print(prefix);
        pw.print("mLidState=");
        pw.print(this.mLidState);
        pw.print(" mLidOpenRotation=");
        pw.print(this.mLidOpenRotation);
        pw.print(" mCameraLensCoverState=");
        pw.print(this.mCameraLensCoverState);
        pw.print(" mHdmiPlugged=");
        pw.println(this.mHdmiPlugged);
        if (this.mLastSystemUiFlags == 0 && this.mResettingSystemUiFlags == 0) {
            if (this.mForceClearedSystemUiFlags != 0) {
            }
            if (this.mLastFocusNeedsMenu) {
                pw.print(prefix);
                pw.print("mLastFocusNeedsMenu=");
                pw.println(this.mLastFocusNeedsMenu);
            }
            pw.print(prefix);
            pw.print("mWakeGestureEnabledSetting=");
            pw.println(this.mWakeGestureEnabledSetting);
            pw.print(prefix);
            pw.print("mSupportAutoRotation=");
            pw.println(this.mSupportAutoRotation);
            pw.print(prefix);
            pw.print("mUiMode=");
            pw.print(this.mUiMode);
            pw.print(" mDockMode=");
            pw.print(this.mDockMode);
            pw.print(" mEnableCarDockHomeCapture=");
            pw.print(this.mEnableCarDockHomeCapture);
            pw.print(" mCarDockRotation=");
            pw.print(this.mCarDockRotation);
            pw.print(" mDeskDockRotation=");
            pw.println(this.mDeskDockRotation);
            pw.print(prefix);
            pw.print("mUserRotationMode=");
            pw.print(this.mUserRotationMode);
            pw.print(" mUserRotation=");
            pw.print(this.mUserRotation);
            pw.print(" mAllowAllRotations=");
            pw.println(this.mAllowAllRotations);
            pw.print(prefix);
            pw.print("mCurrentAppOrientation=");
            pw.println(this.mCurrentAppOrientation);
            pw.print(prefix);
            pw.print("mCarDockEnablesAccelerometer=");
            pw.print(this.mCarDockEnablesAccelerometer);
            pw.print(" mDeskDockEnablesAccelerometer=");
            pw.println(this.mDeskDockEnablesAccelerometer);
            pw.print(prefix);
            pw.print("mLidKeyboardAccessibility=");
            pw.print(this.mLidKeyboardAccessibility);
            pw.print(" mLidNavigationAccessibility=");
            pw.print(this.mLidNavigationAccessibility);
            pw.print(" mLidControlsScreenLock=");
            pw.println(this.mLidControlsScreenLock);
            pw.print(" mLidControlsSleep=");
            pw.println(this.mLidControlsSleep);
            pw.print(prefix);
            pw.print(" mLongPressOnBackBehavior=");
            pw.println(this.mLongPressOnBackBehavior);
            pw.print(prefix);
            pw.print("mShortPressOnPowerBehavior=");
            pw.print(this.mShortPressOnPowerBehavior);
            pw.print(" mLongPressOnPowerBehavior=");
            pw.println(this.mLongPressOnPowerBehavior);
            pw.print(prefix);
            pw.print("mDoublePressOnPowerBehavior=");
            pw.print(this.mDoublePressOnPowerBehavior);
            pw.print(" mTriplePressOnPowerBehavior=");
            pw.println(this.mTriplePressOnPowerBehavior);
            pw.print(prefix);
            pw.print("mHasSoftInput=");
            pw.println(this.mHasSoftInput);
            pw.print(prefix);
            pw.print("mAwake=");
            pw.println(this.mAwake);
            pw.print(prefix);
            pw.print("mScreenOnEarly=");
            pw.print(this.mScreenOnEarly);
            pw.print(" mScreenOnFully=");
            pw.println(this.mScreenOnFully);
            pw.print(prefix);
            pw.print("mKeyguardDrawComplete=");
            pw.print(this.mKeyguardDrawComplete);
            pw.print(" mWindowManagerDrawComplete=");
            pw.println(this.mWindowManagerDrawComplete);
            pw.print(prefix);
            pw.print("mOrientationSensorEnabled=");
            pw.println(this.mOrientationSensorEnabled);
            pw.print(prefix);
            pw.print("mOverscanScreen=(");
            pw.print(this.mOverscanScreenLeft);
            pw.print(",");
            pw.print(this.mOverscanScreenTop);
            pw.print(") ");
            pw.print(this.mOverscanScreenWidth);
            pw.print("x");
            pw.println(this.mOverscanScreenHeight);
            if (this.mOverscanLeft == 0 && this.mOverscanTop == 0 && this.mOverscanRight == 0) {
                if (this.mOverscanBottom != 0) {
                }
                pw.print(prefix);
                pw.print("mRestrictedOverscanScreen=(");
                pw.print(this.mRestrictedOverscanScreenLeft);
                pw.print(",");
                pw.print(this.mRestrictedOverscanScreenTop);
                pw.print(") ");
                pw.print(this.mRestrictedOverscanScreenWidth);
                pw.print("x");
                pw.println(this.mRestrictedOverscanScreenHeight);
                pw.print(prefix);
                pw.print("mUnrestrictedScreen=(");
                pw.print(this.mUnrestrictedScreenLeft);
                pw.print(",");
                pw.print(this.mUnrestrictedScreenTop);
                pw.print(") ");
                pw.print(this.mUnrestrictedScreenWidth);
                pw.print("x");
                pw.println(this.mUnrestrictedScreenHeight);
                pw.print(prefix);
                pw.print("mRestrictedScreen=(");
                pw.print(this.mRestrictedScreenLeft);
                pw.print(",");
                pw.print(this.mRestrictedScreenTop);
                pw.print(") ");
                pw.print(this.mRestrictedScreenWidth);
                pw.print("x");
                pw.println(this.mRestrictedScreenHeight);
                pw.print(prefix);
                pw.print("mStableFullscreen=(");
                pw.print(this.mStableFullscreenLeft);
                pw.print(",");
                pw.print(this.mStableFullscreenTop);
                pw.print(")-(");
                pw.print(this.mStableFullscreenRight);
                pw.print(",");
                pw.print(this.mStableFullscreenBottom);
                pw.println(")");
                pw.print(prefix);
                pw.print("mStable=(");
                pw.print(this.mStableLeft);
                pw.print(",");
                pw.print(this.mStableTop);
                pw.print(")-(");
                pw.print(this.mStableRight);
                pw.print(",");
                pw.print(this.mStableBottom);
                pw.println(")");
                pw.print(prefix);
                pw.print("mSystem=(");
                pw.print(this.mSystemLeft);
                pw.print(",");
                pw.print(this.mSystemTop);
                pw.print(")-(");
                pw.print(this.mSystemRight);
                pw.print(",");
                pw.print(this.mSystemBottom);
                pw.println(")");
                pw.print(prefix);
                pw.print("mCur=(");
                pw.print(this.mCurLeft);
                pw.print(",");
                pw.print(this.mCurTop);
                pw.print(")-(");
                pw.print(this.mCurRight);
                pw.print(",");
                pw.print(this.mCurBottom);
                pw.println(")");
                pw.print(prefix);
                pw.print("mContent=(");
                pw.print(this.mContentLeft);
                pw.print(",");
                pw.print(this.mContentTop);
                pw.print(")-(");
                pw.print(this.mContentRight);
                pw.print(",");
                pw.print(this.mContentBottom);
                pw.println(")");
                pw.print(prefix);
                pw.print("mVoiceContent=(");
                pw.print(this.mVoiceContentLeft);
                pw.print(",");
                pw.print(this.mVoiceContentTop);
                pw.print(")-(");
                pw.print(this.mVoiceContentRight);
                pw.print(",");
                pw.print(this.mVoiceContentBottom);
                pw.println(")");
                pw.print(prefix);
                pw.print("mDock=(");
                pw.print(this.mDockLeft);
                pw.print(",");
                pw.print(this.mDockTop);
                pw.print(")-(");
                pw.print(this.mDockRight);
                pw.print(",");
                pw.print(this.mDockBottom);
                pw.println(")");
                pw.print(prefix);
                pw.print("mDockLayer=");
                pw.print(this.mDockLayer);
                pw.print(" mStatusBarLayer=");
                pw.println(this.mStatusBarLayer);
                pw.print(prefix);
                pw.print("mShowingDream=");
                pw.print(this.mShowingDream);
                pw.print(" mDreamingLockscreen=");
                pw.print(this.mDreamingLockscreen);
                pw.print(" mDreamingSleepToken=");
                pw.println(this.mDreamingSleepToken);
                if (this.mLastInputMethodWindow != null) {
                    pw.print(prefix);
                    pw.print("mLastInputMethodWindow=");
                    pw.println(this.mLastInputMethodWindow);
                }
                if (this.mLastInputMethodTargetWindow != null) {
                    pw.print(prefix);
                    pw.print("mLastInputMethodTargetWindow=");
                    pw.println(this.mLastInputMethodTargetWindow);
                }
                pw.print(prefix);
                pw.print("mDismissImeOnBackKeyPressed=");
                pw.println(this.mDismissImeOnBackKeyPressed);
                if (this.mStatusBar != null) {
                    pw.print(prefix);
                    pw.print("mStatusBar=");
                    pw.print(this.mStatusBar);
                    pw.print(" isStatusBarKeyguard=");
                    pw.println(isStatusBarKeyguard());
                }
                if (this.mNavigationBar != null) {
                    pw.print(prefix);
                    pw.print("mNavigationBar=");
                    pw.println(this.mNavigationBar);
                }
                if (this.mFocusedWindow != null) {
                    pw.print(prefix);
                    pw.print("mFocusedWindow=");
                    pw.println(this.mFocusedWindow);
                }
                if (this.mFocusedApp != null) {
                    pw.print(prefix);
                    pw.print("mFocusedApp=");
                    pw.println(this.mFocusedApp);
                }
                if (this.mTopFullscreenOpaqueWindowState != null) {
                    pw.print(prefix);
                    pw.print("mTopFullscreenOpaqueWindowState=");
                    pw.println(this.mTopFullscreenOpaqueWindowState);
                }
                if (this.mTopFullscreenOpaqueOrDimmingWindowState != null) {
                    pw.print(prefix);
                    pw.print("mTopFullscreenOpaqueOrDimmingWindowState=");
                    pw.println(this.mTopFullscreenOpaqueOrDimmingWindowState);
                }
                if (this.mForcingShowNavBar) {
                    pw.print(prefix);
                    pw.print("mForcingShowNavBar=");
                    pw.println(this.mForcingShowNavBar);
                    pw.print("mForcingShowNavBarLayer=");
                    pw.println(this.mForcingShowNavBarLayer);
                }
                pw.print(prefix);
                pw.print("mTopIsFullscreen=");
                pw.print(this.mTopIsFullscreen);
                pw.print(" mKeyguardOccluded=");
                pw.println(this.mKeyguardOccluded);
                pw.print(" mKeyguardOccludedChanged=");
                pw.println(this.mKeyguardOccludedChanged);
                pw.print(" mPendingKeyguardOccluded=");
                pw.println(this.mPendingKeyguardOccluded);
                pw.print(prefix);
                pw.print("mForceStatusBar=");
                pw.print(this.mForceStatusBar);
                pw.print(" mForceStatusBarFromKeyguard=");
                pw.println(this.mForceStatusBarFromKeyguard);
                pw.print(prefix);
                pw.print("mHomePressed=");
                pw.println(this.mHomePressed);
                pw.print(prefix);
                pw.print("mAllowLockscreenWhenOn=");
                pw.print(this.mAllowLockscreenWhenOn);
                pw.print(" mLockScreenTimeout=");
                pw.print(this.mLockScreenTimeout);
                pw.print(" mLockScreenTimerActive=");
                pw.println(this.mLockScreenTimerActive);
                pw.print(prefix);
                pw.print("mEndcallBehavior=");
                pw.print(this.mEndcallBehavior);
                pw.print(" mIncallPowerBehavior=");
                pw.print(this.mIncallPowerBehavior);
                pw.print(" mIncallBackBehavior=");
                pw.print(this.mIncallBackBehavior);
                pw.print(" mLongPressOnHomeBehavior=");
                pw.println(this.mLongPressOnHomeBehavior);
                pw.print(prefix);
                pw.print("mLandscapeRotation=");
                pw.print(this.mLandscapeRotation);
                pw.print(" mSeascapeRotation=");
                pw.println(this.mSeascapeRotation);
                pw.print(prefix);
                pw.print("mPortraitRotation=");
                pw.print(this.mPortraitRotation);
                pw.print(" mUpsideDownRotation=");
                pw.println(this.mUpsideDownRotation);
                pw.print(prefix);
                pw.print("mDemoHdmiRotation=");
                pw.print(this.mDemoHdmiRotation);
                pw.print(" mDemoHdmiRotationLock=");
                pw.println(this.mDemoHdmiRotationLock);
                pw.print(prefix);
                pw.print("mUndockedHdmiRotation=");
                pw.println(this.mUndockedHdmiRotation);
                if (this.mHasFeatureLeanback) {
                    pw.print(prefix);
                    pw.print("mAccessibilityTvKey1Pressed=");
                    pw.println(this.mAccessibilityTvKey1Pressed);
                    pw.print(prefix);
                    pw.print("mAccessibilityTvKey2Pressed=");
                    pw.println(this.mAccessibilityTvKey2Pressed);
                    pw.print(prefix);
                    pw.print("mAccessibilityTvScheduled=");
                    pw.println(this.mAccessibilityTvScheduled);
                }
                this.mGlobalKeyManager.dump(prefix, pw);
                this.mStatusBarController.dump(pw, prefix);
                this.mNavigationBarController.dump(pw, prefix);
                if (this.mWakeGestureListener != null) {
                    this.mWakeGestureListener.dump(pw, prefix);
                }
                if (this.mOrientationListener != null) {
                    this.mOrientationListener.dump(pw, prefix);
                }
                if (this.mBurnInProtectionHelper != null) {
                    this.mBurnInProtectionHelper.dump(prefix, pw);
                }
                if (this.mKeyguardDelegate == null) {
                    this.mKeyguardDelegate.dump(prefix, pw);
                }
            }
            pw.print(prefix);
            pw.print("mOverscan left=");
            pw.print(this.mOverscanLeft);
            pw.print(" top=");
            pw.print(this.mOverscanTop);
            pw.print(" right=");
            pw.print(this.mOverscanRight);
            pw.print(" bottom=");
            pw.println(this.mOverscanBottom);
            pw.print(prefix);
            pw.print("mRestrictedOverscanScreen=(");
            pw.print(this.mRestrictedOverscanScreenLeft);
            pw.print(",");
            pw.print(this.mRestrictedOverscanScreenTop);
            pw.print(") ");
            pw.print(this.mRestrictedOverscanScreenWidth);
            pw.print("x");
            pw.println(this.mRestrictedOverscanScreenHeight);
            pw.print(prefix);
            pw.print("mUnrestrictedScreen=(");
            pw.print(this.mUnrestrictedScreenLeft);
            pw.print(",");
            pw.print(this.mUnrestrictedScreenTop);
            pw.print(") ");
            pw.print(this.mUnrestrictedScreenWidth);
            pw.print("x");
            pw.println(this.mUnrestrictedScreenHeight);
            pw.print(prefix);
            pw.print("mRestrictedScreen=(");
            pw.print(this.mRestrictedScreenLeft);
            pw.print(",");
            pw.print(this.mRestrictedScreenTop);
            pw.print(") ");
            pw.print(this.mRestrictedScreenWidth);
            pw.print("x");
            pw.println(this.mRestrictedScreenHeight);
            pw.print(prefix);
            pw.print("mStableFullscreen=(");
            pw.print(this.mStableFullscreenLeft);
            pw.print(",");
            pw.print(this.mStableFullscreenTop);
            pw.print(")-(");
            pw.print(this.mStableFullscreenRight);
            pw.print(",");
            pw.print(this.mStableFullscreenBottom);
            pw.println(")");
            pw.print(prefix);
            pw.print("mStable=(");
            pw.print(this.mStableLeft);
            pw.print(",");
            pw.print(this.mStableTop);
            pw.print(")-(");
            pw.print(this.mStableRight);
            pw.print(",");
            pw.print(this.mStableBottom);
            pw.println(")");
            pw.print(prefix);
            pw.print("mSystem=(");
            pw.print(this.mSystemLeft);
            pw.print(",");
            pw.print(this.mSystemTop);
            pw.print(")-(");
            pw.print(this.mSystemRight);
            pw.print(",");
            pw.print(this.mSystemBottom);
            pw.println(")");
            pw.print(prefix);
            pw.print("mCur=(");
            pw.print(this.mCurLeft);
            pw.print(",");
            pw.print(this.mCurTop);
            pw.print(")-(");
            pw.print(this.mCurRight);
            pw.print(",");
            pw.print(this.mCurBottom);
            pw.println(")");
            pw.print(prefix);
            pw.print("mContent=(");
            pw.print(this.mContentLeft);
            pw.print(",");
            pw.print(this.mContentTop);
            pw.print(")-(");
            pw.print(this.mContentRight);
            pw.print(",");
            pw.print(this.mContentBottom);
            pw.println(")");
            pw.print(prefix);
            pw.print("mVoiceContent=(");
            pw.print(this.mVoiceContentLeft);
            pw.print(",");
            pw.print(this.mVoiceContentTop);
            pw.print(")-(");
            pw.print(this.mVoiceContentRight);
            pw.print(",");
            pw.print(this.mVoiceContentBottom);
            pw.println(")");
            pw.print(prefix);
            pw.print("mDock=(");
            pw.print(this.mDockLeft);
            pw.print(",");
            pw.print(this.mDockTop);
            pw.print(")-(");
            pw.print(this.mDockRight);
            pw.print(",");
            pw.print(this.mDockBottom);
            pw.println(")");
            pw.print(prefix);
            pw.print("mDockLayer=");
            pw.print(this.mDockLayer);
            pw.print(" mStatusBarLayer=");
            pw.println(this.mStatusBarLayer);
            pw.print(prefix);
            pw.print("mShowingDream=");
            pw.print(this.mShowingDream);
            pw.print(" mDreamingLockscreen=");
            pw.print(this.mDreamingLockscreen);
            pw.print(" mDreamingSleepToken=");
            pw.println(this.mDreamingSleepToken);
            if (this.mLastInputMethodWindow != null) {
                pw.print(prefix);
                pw.print("mLastInputMethodWindow=");
                pw.println(this.mLastInputMethodWindow);
            }
            if (this.mLastInputMethodTargetWindow != null) {
                pw.print(prefix);
                pw.print("mLastInputMethodTargetWindow=");
                pw.println(this.mLastInputMethodTargetWindow);
            }
            pw.print(prefix);
            pw.print("mDismissImeOnBackKeyPressed=");
            pw.println(this.mDismissImeOnBackKeyPressed);
            if (this.mStatusBar != null) {
                pw.print(prefix);
                pw.print("mStatusBar=");
                pw.print(this.mStatusBar);
                pw.print(" isStatusBarKeyguard=");
                pw.println(isStatusBarKeyguard());
            }
            if (this.mNavigationBar != null) {
                pw.print(prefix);
                pw.print("mNavigationBar=");
                pw.println(this.mNavigationBar);
            }
            if (this.mFocusedWindow != null) {
                pw.print(prefix);
                pw.print("mFocusedWindow=");
                pw.println(this.mFocusedWindow);
            }
            if (this.mFocusedApp != null) {
                pw.print(prefix);
                pw.print("mFocusedApp=");
                pw.println(this.mFocusedApp);
            }
            if (this.mTopFullscreenOpaqueWindowState != null) {
                pw.print(prefix);
                pw.print("mTopFullscreenOpaqueWindowState=");
                pw.println(this.mTopFullscreenOpaqueWindowState);
            }
            if (this.mTopFullscreenOpaqueOrDimmingWindowState != null) {
                pw.print(prefix);
                pw.print("mTopFullscreenOpaqueOrDimmingWindowState=");
                pw.println(this.mTopFullscreenOpaqueOrDimmingWindowState);
            }
            if (this.mForcingShowNavBar) {
                pw.print(prefix);
                pw.print("mForcingShowNavBar=");
                pw.println(this.mForcingShowNavBar);
                pw.print("mForcingShowNavBarLayer=");
                pw.println(this.mForcingShowNavBarLayer);
            }
            pw.print(prefix);
            pw.print("mTopIsFullscreen=");
            pw.print(this.mTopIsFullscreen);
            pw.print(" mKeyguardOccluded=");
            pw.println(this.mKeyguardOccluded);
            pw.print(" mKeyguardOccludedChanged=");
            pw.println(this.mKeyguardOccludedChanged);
            pw.print(" mPendingKeyguardOccluded=");
            pw.println(this.mPendingKeyguardOccluded);
            pw.print(prefix);
            pw.print("mForceStatusBar=");
            pw.print(this.mForceStatusBar);
            pw.print(" mForceStatusBarFromKeyguard=");
            pw.println(this.mForceStatusBarFromKeyguard);
            pw.print(prefix);
            pw.print("mHomePressed=");
            pw.println(this.mHomePressed);
            pw.print(prefix);
            pw.print("mAllowLockscreenWhenOn=");
            pw.print(this.mAllowLockscreenWhenOn);
            pw.print(" mLockScreenTimeout=");
            pw.print(this.mLockScreenTimeout);
            pw.print(" mLockScreenTimerActive=");
            pw.println(this.mLockScreenTimerActive);
            pw.print(prefix);
            pw.print("mEndcallBehavior=");
            pw.print(this.mEndcallBehavior);
            pw.print(" mIncallPowerBehavior=");
            pw.print(this.mIncallPowerBehavior);
            pw.print(" mIncallBackBehavior=");
            pw.print(this.mIncallBackBehavior);
            pw.print(" mLongPressOnHomeBehavior=");
            pw.println(this.mLongPressOnHomeBehavior);
            pw.print(prefix);
            pw.print("mLandscapeRotation=");
            pw.print(this.mLandscapeRotation);
            pw.print(" mSeascapeRotation=");
            pw.println(this.mSeascapeRotation);
            pw.print(prefix);
            pw.print("mPortraitRotation=");
            pw.print(this.mPortraitRotation);
            pw.print(" mUpsideDownRotation=");
            pw.println(this.mUpsideDownRotation);
            pw.print(prefix);
            pw.print("mDemoHdmiRotation=");
            pw.print(this.mDemoHdmiRotation);
            pw.print(" mDemoHdmiRotationLock=");
            pw.println(this.mDemoHdmiRotationLock);
            pw.print(prefix);
            pw.print("mUndockedHdmiRotation=");
            pw.println(this.mUndockedHdmiRotation);
            if (this.mHasFeatureLeanback) {
                pw.print(prefix);
                pw.print("mAccessibilityTvKey1Pressed=");
                pw.println(this.mAccessibilityTvKey1Pressed);
                pw.print(prefix);
                pw.print("mAccessibilityTvKey2Pressed=");
                pw.println(this.mAccessibilityTvKey2Pressed);
                pw.print(prefix);
                pw.print("mAccessibilityTvScheduled=");
                pw.println(this.mAccessibilityTvScheduled);
            }
            this.mGlobalKeyManager.dump(prefix, pw);
            this.mStatusBarController.dump(pw, prefix);
            this.mNavigationBarController.dump(pw, prefix);
            if (this.mWakeGestureListener != null) {
                this.mWakeGestureListener.dump(pw, prefix);
            }
            if (this.mOrientationListener != null) {
                this.mOrientationListener.dump(pw, prefix);
            }
            if (this.mBurnInProtectionHelper != null) {
                this.mBurnInProtectionHelper.dump(prefix, pw);
            }
            if (this.mKeyguardDelegate == null) {
                this.mKeyguardDelegate.dump(prefix, pw);
            }
        }
        pw.print(prefix);
        pw.print("mLastSystemUiFlags=0x");
        pw.print(Integer.toHexString(this.mLastSystemUiFlags));
        pw.print(" mResettingSystemUiFlags=0x");
        pw.print(Integer.toHexString(this.mResettingSystemUiFlags));
        pw.print(" mForceClearedSystemUiFlags=0x");
        pw.println(Integer.toHexString(this.mForceClearedSystemUiFlags));
        if (this.mLastFocusNeedsMenu) {
            pw.print(prefix);
            pw.print("mLastFocusNeedsMenu=");
            pw.println(this.mLastFocusNeedsMenu);
        }
        pw.print(prefix);
        pw.print("mWakeGestureEnabledSetting=");
        pw.println(this.mWakeGestureEnabledSetting);
        pw.print(prefix);
        pw.print("mSupportAutoRotation=");
        pw.println(this.mSupportAutoRotation);
        pw.print(prefix);
        pw.print("mUiMode=");
        pw.print(this.mUiMode);
        pw.print(" mDockMode=");
        pw.print(this.mDockMode);
        pw.print(" mEnableCarDockHomeCapture=");
        pw.print(this.mEnableCarDockHomeCapture);
        pw.print(" mCarDockRotation=");
        pw.print(this.mCarDockRotation);
        pw.print(" mDeskDockRotation=");
        pw.println(this.mDeskDockRotation);
        pw.print(prefix);
        pw.print("mUserRotationMode=");
        pw.print(this.mUserRotationMode);
        pw.print(" mUserRotation=");
        pw.print(this.mUserRotation);
        pw.print(" mAllowAllRotations=");
        pw.println(this.mAllowAllRotations);
        pw.print(prefix);
        pw.print("mCurrentAppOrientation=");
        pw.println(this.mCurrentAppOrientation);
        pw.print(prefix);
        pw.print("mCarDockEnablesAccelerometer=");
        pw.print(this.mCarDockEnablesAccelerometer);
        pw.print(" mDeskDockEnablesAccelerometer=");
        pw.println(this.mDeskDockEnablesAccelerometer);
        pw.print(prefix);
        pw.print("mLidKeyboardAccessibility=");
        pw.print(this.mLidKeyboardAccessibility);
        pw.print(" mLidNavigationAccessibility=");
        pw.print(this.mLidNavigationAccessibility);
        pw.print(" mLidControlsScreenLock=");
        pw.println(this.mLidControlsScreenLock);
        pw.print(" mLidControlsSleep=");
        pw.println(this.mLidControlsSleep);
        pw.print(prefix);
        pw.print(" mLongPressOnBackBehavior=");
        pw.println(this.mLongPressOnBackBehavior);
        pw.print(prefix);
        pw.print("mShortPressOnPowerBehavior=");
        pw.print(this.mShortPressOnPowerBehavior);
        pw.print(" mLongPressOnPowerBehavior=");
        pw.println(this.mLongPressOnPowerBehavior);
        pw.print(prefix);
        pw.print("mDoublePressOnPowerBehavior=");
        pw.print(this.mDoublePressOnPowerBehavior);
        pw.print(" mTriplePressOnPowerBehavior=");
        pw.println(this.mTriplePressOnPowerBehavior);
        pw.print(prefix);
        pw.print("mHasSoftInput=");
        pw.println(this.mHasSoftInput);
        pw.print(prefix);
        pw.print("mAwake=");
        pw.println(this.mAwake);
        pw.print(prefix);
        pw.print("mScreenOnEarly=");
        pw.print(this.mScreenOnEarly);
        pw.print(" mScreenOnFully=");
        pw.println(this.mScreenOnFully);
        pw.print(prefix);
        pw.print("mKeyguardDrawComplete=");
        pw.print(this.mKeyguardDrawComplete);
        pw.print(" mWindowManagerDrawComplete=");
        pw.println(this.mWindowManagerDrawComplete);
        pw.print(prefix);
        pw.print("mOrientationSensorEnabled=");
        pw.println(this.mOrientationSensorEnabled);
        pw.print(prefix);
        pw.print("mOverscanScreen=(");
        pw.print(this.mOverscanScreenLeft);
        pw.print(",");
        pw.print(this.mOverscanScreenTop);
        pw.print(") ");
        pw.print(this.mOverscanScreenWidth);
        pw.print("x");
        pw.println(this.mOverscanScreenHeight);
        if (this.mOverscanBottom != 0) {
            pw.print(prefix);
            pw.print("mOverscan left=");
            pw.print(this.mOverscanLeft);
            pw.print(" top=");
            pw.print(this.mOverscanTop);
            pw.print(" right=");
            pw.print(this.mOverscanRight);
            pw.print(" bottom=");
            pw.println(this.mOverscanBottom);
        }
        pw.print(prefix);
        pw.print("mRestrictedOverscanScreen=(");
        pw.print(this.mRestrictedOverscanScreenLeft);
        pw.print(",");
        pw.print(this.mRestrictedOverscanScreenTop);
        pw.print(") ");
        pw.print(this.mRestrictedOverscanScreenWidth);
        pw.print("x");
        pw.println(this.mRestrictedOverscanScreenHeight);
        pw.print(prefix);
        pw.print("mUnrestrictedScreen=(");
        pw.print(this.mUnrestrictedScreenLeft);
        pw.print(",");
        pw.print(this.mUnrestrictedScreenTop);
        pw.print(") ");
        pw.print(this.mUnrestrictedScreenWidth);
        pw.print("x");
        pw.println(this.mUnrestrictedScreenHeight);
        pw.print(prefix);
        pw.print("mRestrictedScreen=(");
        pw.print(this.mRestrictedScreenLeft);
        pw.print(",");
        pw.print(this.mRestrictedScreenTop);
        pw.print(") ");
        pw.print(this.mRestrictedScreenWidth);
        pw.print("x");
        pw.println(this.mRestrictedScreenHeight);
        pw.print(prefix);
        pw.print("mStableFullscreen=(");
        pw.print(this.mStableFullscreenLeft);
        pw.print(",");
        pw.print(this.mStableFullscreenTop);
        pw.print(")-(");
        pw.print(this.mStableFullscreenRight);
        pw.print(",");
        pw.print(this.mStableFullscreenBottom);
        pw.println(")");
        pw.print(prefix);
        pw.print("mStable=(");
        pw.print(this.mStableLeft);
        pw.print(",");
        pw.print(this.mStableTop);
        pw.print(")-(");
        pw.print(this.mStableRight);
        pw.print(",");
        pw.print(this.mStableBottom);
        pw.println(")");
        pw.print(prefix);
        pw.print("mSystem=(");
        pw.print(this.mSystemLeft);
        pw.print(",");
        pw.print(this.mSystemTop);
        pw.print(")-(");
        pw.print(this.mSystemRight);
        pw.print(",");
        pw.print(this.mSystemBottom);
        pw.println(")");
        pw.print(prefix);
        pw.print("mCur=(");
        pw.print(this.mCurLeft);
        pw.print(",");
        pw.print(this.mCurTop);
        pw.print(")-(");
        pw.print(this.mCurRight);
        pw.print(",");
        pw.print(this.mCurBottom);
        pw.println(")");
        pw.print(prefix);
        pw.print("mContent=(");
        pw.print(this.mContentLeft);
        pw.print(",");
        pw.print(this.mContentTop);
        pw.print(")-(");
        pw.print(this.mContentRight);
        pw.print(",");
        pw.print(this.mContentBottom);
        pw.println(")");
        pw.print(prefix);
        pw.print("mVoiceContent=(");
        pw.print(this.mVoiceContentLeft);
        pw.print(",");
        pw.print(this.mVoiceContentTop);
        pw.print(")-(");
        pw.print(this.mVoiceContentRight);
        pw.print(",");
        pw.print(this.mVoiceContentBottom);
        pw.println(")");
        pw.print(prefix);
        pw.print("mDock=(");
        pw.print(this.mDockLeft);
        pw.print(",");
        pw.print(this.mDockTop);
        pw.print(")-(");
        pw.print(this.mDockRight);
        pw.print(",");
        pw.print(this.mDockBottom);
        pw.println(")");
        pw.print(prefix);
        pw.print("mDockLayer=");
        pw.print(this.mDockLayer);
        pw.print(" mStatusBarLayer=");
        pw.println(this.mStatusBarLayer);
        pw.print(prefix);
        pw.print("mShowingDream=");
        pw.print(this.mShowingDream);
        pw.print(" mDreamingLockscreen=");
        pw.print(this.mDreamingLockscreen);
        pw.print(" mDreamingSleepToken=");
        pw.println(this.mDreamingSleepToken);
        if (this.mLastInputMethodWindow != null) {
            pw.print(prefix);
            pw.print("mLastInputMethodWindow=");
            pw.println(this.mLastInputMethodWindow);
        }
        if (this.mLastInputMethodTargetWindow != null) {
            pw.print(prefix);
            pw.print("mLastInputMethodTargetWindow=");
            pw.println(this.mLastInputMethodTargetWindow);
        }
        pw.print(prefix);
        pw.print("mDismissImeOnBackKeyPressed=");
        pw.println(this.mDismissImeOnBackKeyPressed);
        if (this.mStatusBar != null) {
            pw.print(prefix);
            pw.print("mStatusBar=");
            pw.print(this.mStatusBar);
            pw.print(" isStatusBarKeyguard=");
            pw.println(isStatusBarKeyguard());
        }
        if (this.mNavigationBar != null) {
            pw.print(prefix);
            pw.print("mNavigationBar=");
            pw.println(this.mNavigationBar);
        }
        if (this.mFocusedWindow != null) {
            pw.print(prefix);
            pw.print("mFocusedWindow=");
            pw.println(this.mFocusedWindow);
        }
        if (this.mFocusedApp != null) {
            pw.print(prefix);
            pw.print("mFocusedApp=");
            pw.println(this.mFocusedApp);
        }
        if (this.mTopFullscreenOpaqueWindowState != null) {
            pw.print(prefix);
            pw.print("mTopFullscreenOpaqueWindowState=");
            pw.println(this.mTopFullscreenOpaqueWindowState);
        }
        if (this.mTopFullscreenOpaqueOrDimmingWindowState != null) {
            pw.print(prefix);
            pw.print("mTopFullscreenOpaqueOrDimmingWindowState=");
            pw.println(this.mTopFullscreenOpaqueOrDimmingWindowState);
        }
        if (this.mForcingShowNavBar) {
            pw.print(prefix);
            pw.print("mForcingShowNavBar=");
            pw.println(this.mForcingShowNavBar);
            pw.print("mForcingShowNavBarLayer=");
            pw.println(this.mForcingShowNavBarLayer);
        }
        pw.print(prefix);
        pw.print("mTopIsFullscreen=");
        pw.print(this.mTopIsFullscreen);
        pw.print(" mKeyguardOccluded=");
        pw.println(this.mKeyguardOccluded);
        pw.print(" mKeyguardOccludedChanged=");
        pw.println(this.mKeyguardOccludedChanged);
        pw.print(" mPendingKeyguardOccluded=");
        pw.println(this.mPendingKeyguardOccluded);
        pw.print(prefix);
        pw.print("mForceStatusBar=");
        pw.print(this.mForceStatusBar);
        pw.print(" mForceStatusBarFromKeyguard=");
        pw.println(this.mForceStatusBarFromKeyguard);
        pw.print(prefix);
        pw.print("mHomePressed=");
        pw.println(this.mHomePressed);
        pw.print(prefix);
        pw.print("mAllowLockscreenWhenOn=");
        pw.print(this.mAllowLockscreenWhenOn);
        pw.print(" mLockScreenTimeout=");
        pw.print(this.mLockScreenTimeout);
        pw.print(" mLockScreenTimerActive=");
        pw.println(this.mLockScreenTimerActive);
        pw.print(prefix);
        pw.print("mEndcallBehavior=");
        pw.print(this.mEndcallBehavior);
        pw.print(" mIncallPowerBehavior=");
        pw.print(this.mIncallPowerBehavior);
        pw.print(" mIncallBackBehavior=");
        pw.print(this.mIncallBackBehavior);
        pw.print(" mLongPressOnHomeBehavior=");
        pw.println(this.mLongPressOnHomeBehavior);
        pw.print(prefix);
        pw.print("mLandscapeRotation=");
        pw.print(this.mLandscapeRotation);
        pw.print(" mSeascapeRotation=");
        pw.println(this.mSeascapeRotation);
        pw.print(prefix);
        pw.print("mPortraitRotation=");
        pw.print(this.mPortraitRotation);
        pw.print(" mUpsideDownRotation=");
        pw.println(this.mUpsideDownRotation);
        pw.print(prefix);
        pw.print("mDemoHdmiRotation=");
        pw.print(this.mDemoHdmiRotation);
        pw.print(" mDemoHdmiRotationLock=");
        pw.println(this.mDemoHdmiRotationLock);
        pw.print(prefix);
        pw.print("mUndockedHdmiRotation=");
        pw.println(this.mUndockedHdmiRotation);
        if (this.mHasFeatureLeanback) {
            pw.print(prefix);
            pw.print("mAccessibilityTvKey1Pressed=");
            pw.println(this.mAccessibilityTvKey1Pressed);
            pw.print(prefix);
            pw.print("mAccessibilityTvKey2Pressed=");
            pw.println(this.mAccessibilityTvKey2Pressed);
            pw.print(prefix);
            pw.print("mAccessibilityTvScheduled=");
            pw.println(this.mAccessibilityTvScheduled);
        }
        this.mGlobalKeyManager.dump(prefix, pw);
        this.mStatusBarController.dump(pw, prefix);
        this.mNavigationBarController.dump(pw, prefix);
        if (this.mWakeGestureListener != null) {
            this.mWakeGestureListener.dump(pw, prefix);
        }
        if (this.mOrientationListener != null) {
            this.mOrientationListener.dump(pw, prefix);
        }
        if (this.mBurnInProtectionHelper != null) {
            this.mBurnInProtectionHelper.dump(prefix, pw);
        }
        if (this.mKeyguardDelegate == null) {
            this.mKeyguardDelegate.dump(prefix, pw);
        }
    }
}
