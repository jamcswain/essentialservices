package com.android.server.display;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.display.DisplayManagerInternal.DisplayPowerCallbacks;
import android.hardware.display.DisplayManagerInternal.DisplayPowerRequest;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.Trace;
import android.util.MathUtils;
import android.util.Slog;
import android.util.Spline;
import android.util.TimeUtils;
import android.view.Display;
import android.view.WindowManagerPolicy;
import android.view.WindowManagerPolicy.ScreenOffListener;
import android.view.WindowManagerPolicy.ScreenOnListener;
import com.android.internal.app.IBatteryStats;
import com.android.server.LocalServices;
import com.android.server.am.BatteryStatsService;
import com.android.server.display.RampAnimator.Listener;
import java.io.PrintWriter;

final class DisplayPowerController implements Callbacks {
    static final /* synthetic */ boolean -assertionsDisabled = (DisplayPowerController.class.desiredAssertionStatus() ^ 1);
    private static final int COLOR_FADE_OFF_ANIMATION_DURATION_MILLIS = 400;
    private static final int COLOR_FADE_ON_ANIMATION_DURATION_MILLIS = 250;
    private static final boolean DEBUG = false;
    private static final boolean DEBUG_PRETEND_PROXIMITY_SENSOR_ABSENT = false;
    private static final int MSG_PROXIMITY_SENSOR_DEBOUNCED = 2;
    private static final int MSG_SCREEN_OFF_UNBLOCKED = 4;
    private static final int MSG_SCREEN_ON_UNBLOCKED = 3;
    private static final int MSG_UPDATE_POWER_STATE = 1;
    private static final int PROXIMITY_NEGATIVE = 0;
    private static final int PROXIMITY_POSITIVE = 1;
    private static final int PROXIMITY_SENSOR_NEGATIVE_DEBOUNCE_DELAY = 250;
    private static final int PROXIMITY_SENSOR_POSITIVE_DEBOUNCE_DELAY = 0;
    private static final int PROXIMITY_UNKNOWN = -1;
    private static final int RAMP_STATE_SKIP_AUTOBRIGHT = 2;
    private static final int RAMP_STATE_SKIP_INITIAL = 1;
    private static final int RAMP_STATE_SKIP_NONE = 0;
    private static final int REPORTED_TO_POLICY_SCREEN_OFF = 0;
    private static final int REPORTED_TO_POLICY_SCREEN_ON = 2;
    private static final int REPORTED_TO_POLICY_SCREEN_TURNING_OFF = 3;
    private static final int REPORTED_TO_POLICY_SCREEN_TURNING_ON = 1;
    private static final int SCREEN_DIM_MINIMUM_REDUCTION = 10;
    private static final String SCREEN_OFF_BLOCKED_TRACE_NAME = "Screen off blocked";
    private static final String SCREEN_ON_BLOCKED_TRACE_NAME = "Screen on blocked";
    private static final String TAG = "DisplayPowerController";
    private static final float TYPICAL_PROXIMITY_THRESHOLD = 5.0f;
    private static final boolean USE_COLOR_FADE_ON_ANIMATION = false;
    private final boolean mAllowAutoBrightnessWhileDozingConfig;
    private final AnimatorListener mAnimatorListener = new AnimatorListener() {
        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            DisplayPowerController.this.sendUpdatePowerState();
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationCancel(Animator animation) {
        }
    };
    private boolean mAppliedAutoBrightness;
    private boolean mAppliedDimming;
    private boolean mAppliedLowPower;
    private AutomaticBrightnessController mAutomaticBrightnessController;
    private final IBatteryStats mBatteryStats;
    private final DisplayBlanker mBlanker;
    private boolean mBrightnessBucketsInDozeConfig;
    private final int mBrightnessRampRateFast;
    private final int mBrightnessRampRateSlow;
    private final DisplayPowerCallbacks mCallbacks;
    private final Runnable mCleanListener = new Runnable() {
        public void run() {
            DisplayPowerController.this.sendUpdatePowerState();
        }
    };
    private final boolean mColorFadeEnabled;
    private boolean mColorFadeFadesConfig;
    private ObjectAnimator mColorFadeOffAnimator;
    private ObjectAnimator mColorFadeOnAnimator;
    private final Context mContext;
    private boolean mDisplayBlanksAfterDozeConfig;
    private boolean mDisplayReadyLocked;
    private boolean mDozing;
    private final DisplayControllerHandler mHandler;
    private int mInitialAutoBrightness;
    private final Object mLock = new Object();
    private final Runnable mOnProximityNegativeRunnable = new Runnable() {
        public void run() {
            DisplayPowerController.this.mCallbacks.onProximityNegative();
            DisplayPowerController.this.mCallbacks.releaseSuspendBlocker();
        }
    };
    private final Runnable mOnProximityPositiveRunnable = new Runnable() {
        public void run() {
            DisplayPowerController.this.mCallbacks.onProximityPositive();
            DisplayPowerController.this.mCallbacks.releaseSuspendBlocker();
        }
    };
    private final Runnable mOnStateChangedRunnable = new Runnable() {
        public void run() {
            DisplayPowerController.this.mCallbacks.onStateChanged();
            DisplayPowerController.this.mCallbacks.releaseSuspendBlocker();
        }
    };
    private int mPendingProximity = -1;
    private long mPendingProximityDebounceTime = -1;
    private boolean mPendingRequestChangedLocked;
    private DisplayPowerRequest mPendingRequestLocked;
    private boolean mPendingScreenOff;
    private ScreenOffUnblocker mPendingScreenOffUnblocker;
    private ScreenOnUnblocker mPendingScreenOnUnblocker;
    private boolean mPendingUpdatePowerStateLocked;
    private boolean mPendingWaitForNegativeProximityLocked;
    private DisplayPowerRequest mPowerRequest;
    private DisplayPowerState mPowerState;
    private int mProximity = -1;
    private Sensor mProximitySensor;
    private boolean mProximitySensorEnabled;
    private final SensorEventListener mProximitySensorListener = new SensorEventListener() {
        public void onSensorChanged(SensorEvent event) {
            if (DisplayPowerController.this.mProximitySensorEnabled) {
                long time = SystemClock.uptimeMillis();
                float distance = event.values[0];
                boolean positive = distance >= 0.0f && distance < DisplayPowerController.this.mProximityThreshold;
                DisplayPowerController.this.handleProximitySensorEvent(time, positive);
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    private float mProximityThreshold;
    private final Listener mRampAnimatorListener = new Listener() {
        public void onAnimationEnd() {
            DisplayPowerController.this.sendUpdatePowerState();
        }
    };
    private int mReportedScreenStateToPolicy;
    private final int mScreenBrightnessDarkConfig;
    private final int mScreenBrightnessDimConfig;
    private final int mScreenBrightnessDozeConfig;
    private RampAnimator<DisplayPowerState> mScreenBrightnessRampAnimator;
    private final int mScreenBrightnessRangeMaximum;
    private final int mScreenBrightnessRangeMinimum;
    private boolean mScreenOffBecauseOfProximity;
    private long mScreenOffBlockStartRealTime;
    private long mScreenOnBlockStartRealTime;
    private final SensorManager mSensorManager;
    private int mSkipRampState = 0;
    private final boolean mSkipScreenOnBrightnessRamp;
    private boolean mUnfinishedBusiness;
    private boolean mUseSoftwareAutoBrightnessConfig;
    private boolean mWaitingForNegativeProximity;
    private final WindowManagerPolicy mWindowManagerPolicy;

    private final class DisplayControllerHandler extends Handler {
        public DisplayControllerHandler(Looper looper) {
            super(looper, null, true);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    DisplayPowerController.this.updatePowerState();
                    return;
                case 2:
                    DisplayPowerController.this.debounceProximitySensor();
                    return;
                case 3:
                    if (DisplayPowerController.this.mPendingScreenOnUnblocker == msg.obj) {
                        DisplayPowerController.this.unblockScreenOn();
                        DisplayPowerController.this.updatePowerState();
                        return;
                    }
                    return;
                case 4:
                    if (DisplayPowerController.this.mPendingScreenOffUnblocker == msg.obj) {
                        DisplayPowerController.this.unblockScreenOff();
                        DisplayPowerController.this.updatePowerState();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private final class ScreenOffUnblocker implements ScreenOffListener {
        private ScreenOffUnblocker() {
        }

        public void onScreenOff() {
            Message msg = DisplayPowerController.this.mHandler.obtainMessage(4, this);
            msg.setAsynchronous(true);
            DisplayPowerController.this.mHandler.sendMessage(msg);
        }
    }

    private final class ScreenOnUnblocker implements ScreenOnListener {
        private ScreenOnUnblocker() {
        }

        public void onScreenOn() {
            Message msg = DisplayPowerController.this.mHandler.obtainMessage(3, this);
            msg.setAsynchronous(true);
            DisplayPowerController.this.mHandler.sendMessage(msg);
        }
    }

    public DisplayPowerController(Context context, DisplayPowerCallbacks callbacks, Handler handler, SensorManager sensorManager, DisplayBlanker blanker) {
        this.mHandler = new DisplayControllerHandler(handler.getLooper());
        this.mCallbacks = callbacks;
        this.mBatteryStats = BatteryStatsService.getService();
        this.mSensorManager = sensorManager;
        this.mWindowManagerPolicy = (WindowManagerPolicy) LocalServices.getService(WindowManagerPolicy.class);
        this.mBlanker = blanker;
        this.mContext = context;
        Resources resources = context.getResources();
        int screenBrightnessSettingMinimum = clampAbsoluteBrightness(resources.getInteger(17694848));
        this.mScreenBrightnessDozeConfig = clampAbsoluteBrightness(resources.getInteger(17694842));
        this.mScreenBrightnessDimConfig = clampAbsoluteBrightness(resources.getInteger(17694841));
        this.mScreenBrightnessDarkConfig = clampAbsoluteBrightness(resources.getInteger(17694840));
        if (this.mScreenBrightnessDarkConfig > this.mScreenBrightnessDimConfig) {
            Slog.w(TAG, "Expected config_screenBrightnessDark (" + this.mScreenBrightnessDarkConfig + ") to be less than or equal to " + "config_screenBrightnessDim (" + this.mScreenBrightnessDimConfig + ").");
        }
        if (this.mScreenBrightnessDarkConfig > screenBrightnessSettingMinimum) {
            Slog.w(TAG, "Expected config_screenBrightnessDark (" + this.mScreenBrightnessDarkConfig + ") to be less than or equal to " + "config_screenBrightnessSettingMinimum (" + screenBrightnessSettingMinimum + ").");
        }
        int screenBrightnessRangeMinimum = Math.min(Math.min(screenBrightnessSettingMinimum, this.mScreenBrightnessDimConfig), this.mScreenBrightnessDarkConfig);
        this.mScreenBrightnessRangeMaximum = 255;
        this.mUseSoftwareAutoBrightnessConfig = resources.getBoolean(17956894);
        this.mAllowAutoBrightnessWhileDozingConfig = resources.getBoolean(17956872);
        this.mBrightnessRampRateFast = resources.getInteger(17694744);
        this.mBrightnessRampRateSlow = resources.getInteger(17694745);
        this.mSkipScreenOnBrightnessRamp = resources.getBoolean(17957014);
        int lightSensorRate = resources.getInteger(17694734);
        int initialLightSensorRate = resources.getInteger(17694733);
        if (initialLightSensorRate == -1) {
            initialLightSensorRate = lightSensorRate;
        } else if (initialLightSensorRate > lightSensorRate) {
            Slog.w(TAG, "Expected config_autoBrightnessInitialLightSensorRate (" + initialLightSensorRate + ") to be less than or equal to " + "config_autoBrightnessLightSensorRate (" + lightSensorRate + ").");
        }
        long brighteningLightDebounce = (long) resources.getInteger(17694731);
        long darkeningLightDebounce = (long) resources.getInteger(17694732);
        boolean autoBrightnessResetAmbientLuxAfterWarmUp = resources.getBoolean(17956890);
        int ambientLightHorizon = resources.getInteger(17694730);
        float autoBrightnessAdjustmentMaxGamma = resources.getFraction(18022400, 1, 1);
        HysteresisLevels hysteresisLevels = new HysteresisLevels(resources.getIntArray(17236000), resources.getIntArray(17236001), resources.getIntArray(17236002));
        if (this.mUseSoftwareAutoBrightnessConfig) {
            int[] lux = resources.getIntArray(17235982);
            int[] screenBrightness = resources.getIntArray(17235981);
            int lightSensorWarmUpTimeConfig = resources.getInteger(17694795);
            float dozeScaleFactor = resources.getFraction(18022403, 1, 1);
            Spline screenAutoBrightnessSpline = createAutoBrightnessSpline(lux, screenBrightness);
            if (screenAutoBrightnessSpline == null) {
                Slog.e(TAG, "Error in config.xml.  config_autoBrightnessLcdBacklightValues (size " + screenBrightness.length + ") " + "must be monotic and have exactly one more entry than " + "config_autoBrightnessLevels (size " + lux.length + ") " + "which must be strictly increasing.  " + "Auto-brightness will be disabled.");
                this.mUseSoftwareAutoBrightnessConfig = false;
            } else {
                int bottom = clampAbsoluteBrightness(screenBrightness[0]);
                if (this.mScreenBrightnessDarkConfig > bottom) {
                    Slog.w(TAG, "config_screenBrightnessDark (" + this.mScreenBrightnessDarkConfig + ") should be less than or equal to the first value of " + "config_autoBrightnessLcdBacklightValues (" + bottom + ").");
                }
                if (bottom < screenBrightnessRangeMinimum) {
                    screenBrightnessRangeMinimum = bottom;
                }
                this.mAutomaticBrightnessController = new AutomaticBrightnessController(this, handler.getLooper(), sensorManager, screenAutoBrightnessSpline, lightSensorWarmUpTimeConfig, screenBrightnessRangeMinimum, this.mScreenBrightnessRangeMaximum, dozeScaleFactor, lightSensorRate, initialLightSensorRate, brighteningLightDebounce, darkeningLightDebounce, autoBrightnessResetAmbientLuxAfterWarmUp, ambientLightHorizon, autoBrightnessAdjustmentMaxGamma, hysteresisLevels);
            }
        }
        this.mScreenBrightnessRangeMinimum = screenBrightnessRangeMinimum;
        this.mColorFadeEnabled = ActivityManager.isLowRamDeviceStatic() ^ 1;
        this.mColorFadeFadesConfig = resources.getBoolean(17956887);
        this.mDisplayBlanksAfterDozeConfig = resources.getBoolean(17956928);
        this.mBrightnessBucketsInDozeConfig = resources.getBoolean(17956929);
        this.mProximitySensor = this.mSensorManager.getDefaultSensor(8);
        if (this.mProximitySensor != null) {
            this.mProximityThreshold = Math.min(this.mProximitySensor.getMaximumRange(), 5.0f);
        }
    }

    public boolean isProximitySensorAvailable() {
        return this.mProximitySensor != null;
    }

    public boolean requestPowerState(DisplayPowerRequest request, boolean waitForNegativeProximity) {
        boolean z;
        synchronized (this.mLock) {
            boolean changed = false;
            if (waitForNegativeProximity) {
                if ((this.mPendingWaitForNegativeProximityLocked ^ 1) != 0) {
                    this.mPendingWaitForNegativeProximityLocked = true;
                    changed = true;
                }
            }
            if (this.mPendingRequestLocked == null) {
                this.mPendingRequestLocked = new DisplayPowerRequest(request);
                changed = true;
            } else if (!this.mPendingRequestLocked.equals(request)) {
                this.mPendingRequestLocked.copyFrom(request);
                changed = true;
            }
            if (changed) {
                this.mDisplayReadyLocked = false;
            }
            if (changed && (this.mPendingRequestChangedLocked ^ 1) != 0) {
                this.mPendingRequestChangedLocked = true;
                sendUpdatePowerStateLocked();
            }
            z = this.mDisplayReadyLocked;
        }
        return z;
    }

    private void sendUpdatePowerState() {
        synchronized (this.mLock) {
            sendUpdatePowerStateLocked();
        }
    }

    private void sendUpdatePowerStateLocked() {
        if (!this.mPendingUpdatePowerStateLocked) {
            this.mPendingUpdatePowerStateLocked = true;
            Message msg = this.mHandler.obtainMessage(1);
            msg.setAsynchronous(true);
            this.mHandler.sendMessage(msg);
        }
    }

    private void initialize() {
        this.mPowerState = new DisplayPowerState(this.mBlanker, this.mColorFadeEnabled ? new ColorFade(0) : null);
        if (this.mColorFadeEnabled) {
            this.mColorFadeOnAnimator = ObjectAnimator.ofFloat(this.mPowerState, DisplayPowerState.COLOR_FADE_LEVEL, new float[]{0.0f, 1.0f});
            this.mColorFadeOnAnimator.setDuration(250);
            this.mColorFadeOnAnimator.addListener(this.mAnimatorListener);
            this.mColorFadeOffAnimator = ObjectAnimator.ofFloat(this.mPowerState, DisplayPowerState.COLOR_FADE_LEVEL, new float[]{1.0f, 0.0f});
            this.mColorFadeOffAnimator.setDuration(400);
            this.mColorFadeOffAnimator.addListener(this.mAnimatorListener);
        }
        this.mScreenBrightnessRampAnimator = new RampAnimator(this.mPowerState, DisplayPowerState.SCREEN_BRIGHTNESS);
        this.mScreenBrightnessRampAnimator.setListener(this.mRampAnimatorListener);
        try {
            this.mBatteryStats.noteScreenState(this.mPowerState.getScreenState());
            this.mBatteryStats.noteScreenBrightness(this.mPowerState.getScreenBrightness());
        } catch (RemoteException e) {
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void updatePowerState() {
        /*
        r23 = this;
        r11 = 0;
        r4 = 0;
        r0 = r23;
        r0 = r0.mLock;
        r21 = r0;
        monitor-enter(r21);
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mPendingUpdatePowerStateLocked = r0;	 Catch:{ all -> 0x00e8 }
        r0 = r23;
        r0 = r0.mPendingRequestLocked;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        if (r20 != 0) goto L_0x001b;
    L_0x0019:
        monitor-exit(r21);
        return;
    L_0x001b:
        r0 = r23;
        r0 = r0.mPowerRequest;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        if (r20 != 0) goto L_0x0082;
    L_0x0023:
        r20 = new android.hardware.display.DisplayManagerInternal$DisplayPowerRequest;	 Catch:{ all -> 0x00e8 }
        r0 = r23;
        r0 = r0.mPendingRequestLocked;	 Catch:{ all -> 0x00e8 }
        r22 = r0;
        r0 = r20;
        r1 = r22;
        r0.<init>(r1);	 Catch:{ all -> 0x00e8 }
        r0 = r20;
        r1 = r23;
        r1.mPowerRequest = r0;	 Catch:{ all -> 0x00e8 }
        r0 = r23;
        r0 = r0.mPendingWaitForNegativeProximityLocked;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        r0 = r20;
        r1 = r23;
        r1.mWaitingForNegativeProximity = r0;	 Catch:{ all -> 0x00e8 }
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mPendingWaitForNegativeProximityLocked = r0;	 Catch:{ all -> 0x00e8 }
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mPendingRequestChangedLocked = r0;	 Catch:{ all -> 0x00e8 }
        r11 = 1;
    L_0x0055:
        r0 = r23;
        r0 = r0.mDisplayReadyLocked;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        r12 = r20 ^ 1;
        monitor-exit(r21);
        if (r11 == 0) goto L_0x0063;
    L_0x0060:
        r23.initialize();
    L_0x0063:
        r7 = -1;
        r14 = 0;
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.policy;
        r20 = r0;
        switch(r20) {
            case 0: goto L_0x00ed;
            case 1: goto L_0x00f1;
            case 2: goto L_0x0074;
            case 3: goto L_0x0074;
            case 4: goto L_0x0122;
            default: goto L_0x0074;
        };
    L_0x0074:
        r17 = 2;
    L_0x0076:
        r20 = -assertionsDisabled;
        if (r20 != 0) goto L_0x0126;
    L_0x007a:
        if (r17 != 0) goto L_0x0126;
    L_0x007c:
        r20 = new java.lang.AssertionError;
        r20.<init>();
        throw r20;
    L_0x0082:
        r0 = r23;
        r0 = r0.mPendingRequestChangedLocked;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        if (r20 == 0) goto L_0x0055;
    L_0x008a:
        r0 = r23;
        r0 = r0.mPowerRequest;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        r0 = r20;
        r0 = r0.screenAutoBrightnessAdjustment;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        r0 = r23;
        r0 = r0.mPendingRequestLocked;	 Catch:{ all -> 0x00e8 }
        r22 = r0;
        r0 = r22;
        r0 = r0.screenAutoBrightnessAdjustment;	 Catch:{ all -> 0x00e8 }
        r22 = r0;
        r20 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1));
        if (r20 == 0) goto L_0x00eb;
    L_0x00a6:
        r4 = 1;
    L_0x00a7:
        r0 = r23;
        r0 = r0.mPowerRequest;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        r0 = r23;
        r0 = r0.mPendingRequestLocked;	 Catch:{ all -> 0x00e8 }
        r22 = r0;
        r0 = r20;
        r1 = r22;
        r0.copyFrom(r1);	 Catch:{ all -> 0x00e8 }
        r0 = r23;
        r0 = r0.mWaitingForNegativeProximity;	 Catch:{ all -> 0x00e8 }
        r20 = r0;
        r0 = r23;
        r0 = r0.mPendingWaitForNegativeProximityLocked;	 Catch:{ all -> 0x00e8 }
        r22 = r0;
        r20 = r20 | r22;
        r0 = r20;
        r1 = r23;
        r1.mWaitingForNegativeProximity = r0;	 Catch:{ all -> 0x00e8 }
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mPendingWaitForNegativeProximityLocked = r0;	 Catch:{ all -> 0x00e8 }
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mPendingRequestChangedLocked = r0;	 Catch:{ all -> 0x00e8 }
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mDisplayReadyLocked = r0;	 Catch:{ all -> 0x00e8 }
        goto L_0x0055;
    L_0x00e8:
        r20 = move-exception;
        monitor-exit(r21);
        throw r20;
    L_0x00eb:
        r4 = 0;
        goto L_0x00a7;
    L_0x00ed:
        r17 = 1;
        r14 = 1;
        goto L_0x0076;
    L_0x00f1:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.dozeScreenState;
        r20 = r0;
        if (r20 == 0) goto L_0x011f;
    L_0x00ff:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.dozeScreenState;
        r17 = r0;
    L_0x010b:
        r0 = r23;
        r0 = r0.mAllowAutoBrightnessWhileDozingConfig;
        r20 = r0;
        if (r20 != 0) goto L_0x0076;
    L_0x0113:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r7 = r0.dozeScreenBrightness;
        goto L_0x0076;
    L_0x011f:
        r17 = 3;
        goto L_0x010b;
    L_0x0122:
        r17 = 5;
        goto L_0x0076;
    L_0x0126:
        r0 = r23;
        r0 = r0.mProximitySensor;
        r20 = r0;
        if (r20 == 0) goto L_0x04b6;
    L_0x012e:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.useProximitySensor;
        r20 = r0;
        if (r20 == 0) goto L_0x0472;
    L_0x013c:
        r20 = 1;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x0472;
    L_0x0144:
        r20 = 1;
        r0 = r23;
        r1 = r20;
        r0.setProximitySensorEnabled(r1);
        r0 = r23;
        r0 = r0.mScreenOffBecauseOfProximity;
        r20 = r0;
        if (r20 != 0) goto L_0x016e;
    L_0x0155:
        r0 = r23;
        r0 = r0.mProximity;
        r20 = r0;
        r21 = 1;
        r0 = r20;
        r1 = r21;
        if (r0 != r1) goto L_0x016e;
    L_0x0163:
        r20 = 1;
        r0 = r20;
        r1 = r23;
        r1.mScreenOffBecauseOfProximity = r0;
        r23.sendOnProximityPositiveWithWakelock();
    L_0x016e:
        r0 = r23;
        r0 = r0.mScreenOffBecauseOfProximity;
        r20 = r0;
        if (r20 == 0) goto L_0x018f;
    L_0x0176:
        r0 = r23;
        r0 = r0.mProximity;
        r20 = r0;
        r21 = 1;
        r0 = r20;
        r1 = r21;
        if (r0 == r1) goto L_0x018f;
    L_0x0184:
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mScreenOffBecauseOfProximity = r0;
        r23.sendOnProximityNegativeWithWakelock();
    L_0x018f:
        r0 = r23;
        r0 = r0.mScreenOffBecauseOfProximity;
        r20 = r0;
        if (r20 == 0) goto L_0x0199;
    L_0x0197:
        r17 = 1;
    L_0x0199:
        r0 = r23;
        r0 = r0.mPowerState;
        r20 = r0;
        r13 = r20.getScreenState();
        r0 = r23;
        r1 = r17;
        r0.animateScreenStateChange(r1, r14);
        r0 = r23;
        r0 = r0.mPowerState;
        r20 = r0;
        r17 = r20.getScreenState();
        r20 = 1;
        r0 = r17;
        r1 = r20;
        if (r0 != r1) goto L_0x01bd;
    L_0x01bc:
        r7 = 0;
    L_0x01bd:
        r5 = 0;
        r0 = r23;
        r0 = r0.mAutomaticBrightnessController;
        r20 = r0;
        if (r20 == 0) goto L_0x022f;
    L_0x01c6:
        r0 = r23;
        r0 = r0.mAllowAutoBrightnessWhileDozingConfig;
        r20 = r0;
        if (r20 == 0) goto L_0x04c3;
    L_0x01ce:
        r20 = 3;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x01de;
    L_0x01d6:
        r20 = 4;
        r0 = r17;
        r1 = r20;
        if (r0 != r1) goto L_0x04c0;
    L_0x01de:
        r6 = 1;
    L_0x01df:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.useAutoBrightness;
        r20 = r0;
        if (r20 == 0) goto L_0x04c9;
    L_0x01ed:
        r20 = 2;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x01f7;
    L_0x01f5:
        if (r6 == 0) goto L_0x04c9;
    L_0x01f7:
        if (r7 >= 0) goto L_0x04c6;
    L_0x01f9:
        r5 = 1;
    L_0x01fa:
        if (r4 == 0) goto L_0x04cc;
    L_0x01fc:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.brightnessSetByUser;
        r18 = r0;
    L_0x0208:
        r0 = r23;
        r0 = r0.mAutomaticBrightnessController;
        r21 = r0;
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.screenAutoBrightnessAdjustment;
        r22 = r0;
        r20 = 2;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x04d0;
    L_0x0222:
        r20 = 1;
    L_0x0224:
        r0 = r21;
        r1 = r22;
        r2 = r20;
        r3 = r18;
        r0.configure(r5, r1, r2, r3);
    L_0x022f:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.boostScreenBrightness;
        r20 = r0;
        if (r20 == 0) goto L_0x0241;
    L_0x023d:
        if (r7 == 0) goto L_0x0241;
    L_0x023f:
        r7 = 255; // 0xff float:3.57E-43 double:1.26E-321;
    L_0x0241:
        r16 = 0;
        if (r7 >= 0) goto L_0x04de;
    L_0x0245:
        if (r5 == 0) goto L_0x0251;
    L_0x0247:
        r0 = r23;
        r0 = r0.mAutomaticBrightnessController;
        r20 = r0;
        r7 = r20.getAutomaticScreenBrightness();
    L_0x0251:
        if (r7 < 0) goto L_0x04d4;
    L_0x0253:
        r0 = r23;
        r7 = r0.clampScreenBrightness(r7);
        r0 = r23;
        r0 = r0.mAppliedAutoBrightness;
        r20 = r0;
        if (r20 == 0) goto L_0x0267;
    L_0x0261:
        r20 = r4 ^ 1;
        if (r20 == 0) goto L_0x0267;
    L_0x0265:
        r16 = 1;
    L_0x0267:
        r20 = 1;
        r0 = r20;
        r1 = r23;
        r1.mAppliedAutoBrightness = r0;
    L_0x026f:
        if (r7 >= 0) goto L_0x0285;
    L_0x0271:
        r20 = 3;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x0281;
    L_0x0279:
        r20 = 4;
        r0 = r17;
        r1 = r20;
        if (r0 != r1) goto L_0x0285;
    L_0x0281:
        r0 = r23;
        r7 = r0.mScreenBrightnessDozeConfig;
    L_0x0285:
        if (r7 >= 0) goto L_0x029b;
    L_0x0287:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.screenBrightness;
        r20 = r0;
        r0 = r23;
        r1 = r20;
        r7 = r0.clampScreenBrightness(r1);
    L_0x029b:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.policy;
        r20 = r0;
        r21 = 2;
        r0 = r20;
        r1 = r21;
        if (r0 != r1) goto L_0x04e8;
    L_0x02af:
        r0 = r23;
        r0 = r0.mScreenBrightnessRangeMinimum;
        r20 = r0;
        r0 = r20;
        if (r7 <= r0) goto L_0x02cf;
    L_0x02b9:
        r20 = r7 + -10;
        r0 = r23;
        r0 = r0.mScreenBrightnessDimConfig;
        r21 = r0;
        r20 = java.lang.Math.min(r20, r21);
        r0 = r23;
        r0 = r0.mScreenBrightnessRangeMinimum;
        r21 = r0;
        r7 = java.lang.Math.max(r20, r21);
    L_0x02cf:
        r0 = r23;
        r0 = r0.mAppliedDimming;
        r20 = r0;
        if (r20 != 0) goto L_0x02d9;
    L_0x02d7:
        r16 = 0;
    L_0x02d9:
        r20 = 1;
        r0 = r20;
        r1 = r23;
        r1.mAppliedDimming = r0;
    L_0x02e1:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.lowPowerMode;
        r20 = r0;
        if (r20 == 0) goto L_0x04fc;
    L_0x02ef:
        r0 = r23;
        r0 = r0.mScreenBrightnessRangeMinimum;
        r20 = r0;
        r0 = r20;
        if (r7 <= r0) goto L_0x031f;
    L_0x02f9:
        r0 = r23;
        r0 = r0.mPowerRequest;
        r20 = r0;
        r0 = r20;
        r0 = r0.screenLowPowerBrightnessFactor;
        r20 = r0;
        r21 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r8 = java.lang.Math.min(r20, r21);
        r0 = (float) r7;
        r20 = r0;
        r20 = r20 * r8;
        r0 = r20;
        r10 = (int) r0;
        r0 = r23;
        r0 = r0.mScreenBrightnessRangeMinimum;
        r20 = r0;
        r0 = r20;
        r7 = java.lang.Math.max(r10, r0);
    L_0x031f:
        r0 = r23;
        r0 = r0.mAppliedLowPower;
        r20 = r0;
        if (r20 != 0) goto L_0x0329;
    L_0x0327:
        r16 = 0;
    L_0x0329:
        r20 = 1;
        r0 = r20;
        r1 = r23;
        r1.mAppliedLowPower = r0;
    L_0x0331:
        r0 = r23;
        r0 = r0.mPendingScreenOff;
        r20 = r0;
        if (r20 != 0) goto L_0x0398;
    L_0x0339:
        r0 = r23;
        r0 = r0.mSkipScreenOnBrightnessRamp;
        r20 = r0;
        if (r20 == 0) goto L_0x0365;
    L_0x0341:
        r20 = 2;
        r0 = r17;
        r1 = r20;
        if (r0 != r1) goto L_0x0552;
    L_0x0349:
        r0 = r23;
        r0 = r0.mSkipRampState;
        r20 = r0;
        if (r20 != 0) goto L_0x0510;
    L_0x0351:
        r0 = r23;
        r0 = r0.mDozing;
        r20 = r0;
        if (r20 == 0) goto L_0x0510;
    L_0x0359:
        r0 = r23;
        r0.mInitialAutoBrightness = r7;
        r20 = 1;
        r0 = r20;
        r1 = r23;
        r1.mSkipRampState = r0;
    L_0x0365:
        r20 = 5;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x0373;
    L_0x036d:
        r20 = 5;
        r0 = r20;
        if (r13 != r0) goto L_0x055c;
    L_0x0373:
        r19 = 1;
    L_0x0375:
        r20 = 2;
        r0 = r17;
        r1 = r20;
        if (r0 != r1) goto L_0x0560;
    L_0x037d:
        r0 = r23;
        r0 = r0.mSkipRampState;
        r20 = r0;
        if (r20 != 0) goto L_0x0560;
    L_0x0385:
        r20 = r19 ^ 1;
        if (r20 == 0) goto L_0x0572;
    L_0x0389:
        if (r16 == 0) goto L_0x057d;
    L_0x038b:
        r0 = r23;
        r0 = r0.mBrightnessRampRateSlow;
        r20 = r0;
    L_0x0391:
        r0 = r23;
        r1 = r20;
        r0.animateScreenBrightness(r7, r1);
    L_0x0398:
        r0 = r23;
        r0 = r0.mPendingScreenOnUnblocker;
        r20 = r0;
        if (r20 != 0) goto L_0x0585;
    L_0x03a0:
        r0 = r23;
        r0 = r0.mColorFadeEnabled;
        r20 = r0;
        if (r20 == 0) goto L_0x03c2;
    L_0x03a8:
        r0 = r23;
        r0 = r0.mColorFadeOnAnimator;
        r20 = r0;
        r20 = r20.isStarted();
        if (r20 != 0) goto L_0x0585;
    L_0x03b4:
        r0 = r23;
        r0 = r0.mColorFadeOffAnimator;
        r20 = r0;
        r20 = r20.isStarted();
        r20 = r20 ^ 1;
        if (r20 == 0) goto L_0x0585;
    L_0x03c2:
        r0 = r23;
        r0 = r0.mPowerState;
        r20 = r0;
        r0 = r23;
        r0 = r0.mCleanListener;
        r21 = r0;
        r15 = r20.waitUntilClean(r21);
    L_0x03d2:
        if (r15 == 0) goto L_0x0588;
    L_0x03d4:
        r0 = r23;
        r0 = r0.mScreenBrightnessRampAnimator;
        r20 = r0;
        r20 = r20.isAnimating();
        r9 = r20 ^ 1;
    L_0x03e0:
        if (r15 == 0) goto L_0x040a;
    L_0x03e2:
        r20 = 1;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x040a;
    L_0x03ea:
        r0 = r23;
        r0 = r0.mReportedScreenStateToPolicy;
        r20 = r0;
        r21 = 1;
        r0 = r20;
        r1 = r21;
        if (r0 != r1) goto L_0x040a;
    L_0x03f8:
        r20 = 2;
        r0 = r23;
        r1 = r20;
        r0.setReportedScreenState(r1);
        r0 = r23;
        r0 = r0.mWindowManagerPolicy;
        r20 = r0;
        r20.screenTurnedOn();
    L_0x040a:
        if (r9 != 0) goto L_0x0427;
    L_0x040c:
        r0 = r23;
        r0 = r0.mUnfinishedBusiness;
        r20 = r0;
        r20 = r20 ^ 1;
        if (r20 == 0) goto L_0x0427;
    L_0x0416:
        r0 = r23;
        r0 = r0.mCallbacks;
        r20 = r0;
        r20.acquireSuspendBlocker();
        r20 = 1;
        r0 = r20;
        r1 = r23;
        r1.mUnfinishedBusiness = r0;
    L_0x0427:
        if (r15 == 0) goto L_0x0446;
    L_0x0429:
        if (r12 == 0) goto L_0x0446;
    L_0x042b:
        r0 = r23;
        r0 = r0.mLock;
        r21 = r0;
        monitor-enter(r21);
        r0 = r23;
        r0 = r0.mPendingRequestChangedLocked;	 Catch:{ all -> 0x058b }
        r20 = r0;
        if (r20 != 0) goto L_0x0442;
    L_0x043a:
        r20 = 1;
        r0 = r20;
        r1 = r23;
        r1.mDisplayReadyLocked = r0;	 Catch:{ all -> 0x058b }
    L_0x0442:
        monitor-exit(r21);
        r23.sendOnStateChangedWithWakelock();
    L_0x0446:
        if (r9 == 0) goto L_0x0461;
    L_0x0448:
        r0 = r23;
        r0 = r0.mUnfinishedBusiness;
        r20 = r0;
        if (r20 == 0) goto L_0x0461;
    L_0x0450:
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mUnfinishedBusiness = r0;
        r0 = r23;
        r0 = r0.mCallbacks;
        r20 = r0;
        r20.releaseSuspendBlocker();
    L_0x0461:
        r20 = 2;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x058e;
    L_0x0469:
        r20 = 1;
    L_0x046b:
        r0 = r20;
        r1 = r23;
        r1.mDozing = r0;
        return;
    L_0x0472:
        r0 = r23;
        r0 = r0.mWaitingForNegativeProximity;
        r20 = r0;
        if (r20 == 0) goto L_0x04a3;
    L_0x047a:
        r0 = r23;
        r0 = r0.mScreenOffBecauseOfProximity;
        r20 = r0;
        if (r20 == 0) goto L_0x04a3;
    L_0x0482:
        r0 = r23;
        r0 = r0.mProximity;
        r20 = r0;
        r21 = 1;
        r0 = r20;
        r1 = r21;
        if (r0 != r1) goto L_0x04a3;
    L_0x0490:
        r20 = 1;
        r0 = r17;
        r1 = r20;
        if (r0 == r1) goto L_0x04a3;
    L_0x0498:
        r20 = 1;
        r0 = r23;
        r1 = r20;
        r0.setProximitySensorEnabled(r1);
        goto L_0x016e;
    L_0x04a3:
        r20 = 0;
        r0 = r23;
        r1 = r20;
        r0.setProximitySensorEnabled(r1);
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mWaitingForNegativeProximity = r0;
        goto L_0x016e;
    L_0x04b6:
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mWaitingForNegativeProximity = r0;
        goto L_0x018f;
    L_0x04c0:
        r6 = 0;
        goto L_0x01df;
    L_0x04c3:
        r6 = 0;
        goto L_0x01df;
    L_0x04c6:
        r5 = 0;
        goto L_0x01fa;
    L_0x04c9:
        r5 = 0;
        goto L_0x01fa;
    L_0x04cc:
        r18 = 0;
        goto L_0x0208;
    L_0x04d0:
        r20 = 0;
        goto L_0x0224;
    L_0x04d4:
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mAppliedAutoBrightness = r0;
        goto L_0x026f;
    L_0x04de:
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mAppliedAutoBrightness = r0;
        goto L_0x026f;
    L_0x04e8:
        r0 = r23;
        r0 = r0.mAppliedDimming;
        r20 = r0;
        if (r20 == 0) goto L_0x02e1;
    L_0x04f0:
        r16 = 0;
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mAppliedDimming = r0;
        goto L_0x02e1;
    L_0x04fc:
        r0 = r23;
        r0 = r0.mAppliedLowPower;
        r20 = r0;
        if (r20 == 0) goto L_0x0331;
    L_0x0504:
        r16 = 0;
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mAppliedLowPower = r0;
        goto L_0x0331;
    L_0x0510:
        r0 = r23;
        r0 = r0.mSkipRampState;
        r20 = r0;
        r21 = 1;
        r0 = r20;
        r1 = r21;
        if (r0 != r1) goto L_0x053a;
    L_0x051e:
        r0 = r23;
        r0 = r0.mUseSoftwareAutoBrightnessConfig;
        r20 = r0;
        if (r20 == 0) goto L_0x053a;
    L_0x0526:
        r0 = r23;
        r0 = r0.mInitialAutoBrightness;
        r20 = r0;
        r0 = r20;
        if (r7 == r0) goto L_0x053a;
    L_0x0530:
        r20 = 2;
        r0 = r20;
        r1 = r23;
        r1.mSkipRampState = r0;
        goto L_0x0365;
    L_0x053a:
        r0 = r23;
        r0 = r0.mSkipRampState;
        r20 = r0;
        r21 = 2;
        r0 = r20;
        r1 = r21;
        if (r0 != r1) goto L_0x0365;
    L_0x0548:
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mSkipRampState = r0;
        goto L_0x0365;
    L_0x0552:
        r20 = 0;
        r0 = r20;
        r1 = r23;
        r1.mSkipRampState = r0;
        goto L_0x0365;
    L_0x055c:
        r19 = 0;
        goto L_0x0375;
    L_0x0560:
        r20 = 3;
        r0 = r17;
        r1 = r20;
        if (r0 != r1) goto L_0x0572;
    L_0x0568:
        r0 = r23;
        r0 = r0.mBrightnessBucketsInDozeConfig;
        r20 = r0;
        r20 = r20 ^ 1;
        if (r20 != 0) goto L_0x0385;
    L_0x0572:
        r20 = 0;
        r0 = r23;
        r1 = r20;
        r0.animateScreenBrightness(r7, r1);
        goto L_0x0398;
    L_0x057d:
        r0 = r23;
        r0 = r0.mBrightnessRampRateFast;
        r20 = r0;
        goto L_0x0391;
    L_0x0585:
        r15 = 0;
        goto L_0x03d2;
    L_0x0588:
        r9 = 0;
        goto L_0x03e0;
    L_0x058b:
        r20 = move-exception;
        monitor-exit(r21);
        throw r20;
    L_0x058e:
        r20 = 0;
        goto L_0x046b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.display.DisplayPowerController.updatePowerState():void");
    }

    public void updateBrightness() {
        sendUpdatePowerState();
    }

    private void blockScreenOn() {
        if (this.mPendingScreenOnUnblocker == null) {
            Trace.asyncTraceBegin(131072, SCREEN_ON_BLOCKED_TRACE_NAME, 0);
            this.mPendingScreenOnUnblocker = new ScreenOnUnblocker();
            this.mScreenOnBlockStartRealTime = SystemClock.elapsedRealtime();
            Slog.i(TAG, "Blocking screen on until initial contents have been drawn.");
        }
    }

    private void unblockScreenOn() {
        if (this.mPendingScreenOnUnblocker != null) {
            this.mPendingScreenOnUnblocker = null;
            Slog.i(TAG, "Unblocked screen on after " + (SystemClock.elapsedRealtime() - this.mScreenOnBlockStartRealTime) + " ms");
            Trace.asyncTraceEnd(131072, SCREEN_ON_BLOCKED_TRACE_NAME, 0);
        }
    }

    private void blockScreenOff() {
        if (this.mPendingScreenOffUnblocker == null) {
            Trace.asyncTraceBegin(131072, SCREEN_OFF_BLOCKED_TRACE_NAME, 0);
            this.mPendingScreenOffUnblocker = new ScreenOffUnblocker();
            this.mScreenOffBlockStartRealTime = SystemClock.elapsedRealtime();
            Slog.i(TAG, "Blocking screen off");
        }
    }

    private void unblockScreenOff() {
        if (this.mPendingScreenOffUnblocker != null) {
            this.mPendingScreenOffUnblocker = null;
            Slog.i(TAG, "Unblocked screen off after " + (SystemClock.elapsedRealtime() - this.mScreenOffBlockStartRealTime) + " ms");
            Trace.asyncTraceEnd(131072, SCREEN_OFF_BLOCKED_TRACE_NAME, 0);
        }
    }

    private boolean setScreenState(int state) {
        return setScreenState(state, false);
    }

    private boolean setScreenState(int state, boolean reportOnly) {
        boolean z = true;
        boolean isOff = state == 1;
        if (this.mPowerState.getScreenState() != state) {
            if (isOff && (this.mScreenOffBecauseOfProximity ^ 1) != 0) {
                if (this.mReportedScreenStateToPolicy == 2) {
                    setReportedScreenState(3);
                    blockScreenOff();
                    this.mWindowManagerPolicy.screenTurningOff(this.mPendingScreenOffUnblocker);
                    unblockScreenOff();
                } else if (this.mPendingScreenOffUnblocker != null) {
                    return false;
                }
            }
            if (!reportOnly) {
                this.mPowerState.setScreenState(state);
                try {
                    this.mBatteryStats.noteScreenState(state);
                } catch (RemoteException e) {
                }
            }
        }
        if (isOff && this.mReportedScreenStateToPolicy != 0 && (this.mScreenOffBecauseOfProximity ^ 1) != 0) {
            setReportedScreenState(0);
            unblockScreenOn();
            this.mWindowManagerPolicy.screenTurnedOff();
        } else if (!isOff && this.mReportedScreenStateToPolicy == 3) {
            unblockScreenOff();
            this.mWindowManagerPolicy.screenTurnedOff();
            setReportedScreenState(0);
        }
        if (!isOff && this.mReportedScreenStateToPolicy == 0) {
            setReportedScreenState(1);
            if (this.mPowerState.getColorFadeLevel() == 0.0f) {
                blockScreenOn();
            } else {
                unblockScreenOn();
            }
            this.mWindowManagerPolicy.screenTurningOn(this.mPendingScreenOnUnblocker);
        }
        if (this.mPendingScreenOnUnblocker != null) {
            z = false;
        }
        return z;
    }

    private void setReportedScreenState(int state) {
        Trace.traceCounter(131072, "ReportedScreenStateToPolicy", state);
        this.mReportedScreenStateToPolicy = state;
    }

    private int clampScreenBrightness(int value) {
        return MathUtils.constrain(value, this.mScreenBrightnessRangeMinimum, this.mScreenBrightnessRangeMaximum);
    }

    private void animateScreenBrightness(int target, int rate) {
        if (this.mScreenBrightnessRampAnimator.animateTo(target, rate)) {
            Trace.traceCounter(131072, "TargetScreenBrightness", target);
            try {
                this.mBatteryStats.noteScreenBrightness(target);
            } catch (RemoteException e) {
            }
        }
    }

    private void animateScreenStateChange(int target, boolean performScreenOffTransition) {
        int i = 2;
        if (this.mColorFadeEnabled && (this.mColorFadeOnAnimator.isStarted() || this.mColorFadeOffAnimator.isStarted())) {
            if (target == 2) {
                this.mPendingScreenOff = false;
            } else {
                return;
            }
        }
        if (this.mDisplayBlanksAfterDozeConfig && Display.isDozeState(this.mPowerState.getScreenState()) && (Display.isDozeState(target) ^ 1) != 0) {
            boolean z;
            this.mPowerState.prepareColorFade(this.mContext, this.mColorFadeFadesConfig ? 2 : 0);
            if (this.mColorFadeOffAnimator != null) {
                this.mColorFadeOffAnimator.end();
            }
            if (target != 1) {
                z = true;
            } else {
                z = false;
            }
            setScreenState(1, z);
        }
        if (this.mPendingScreenOff && target != 1) {
            setScreenState(1);
            this.mPendingScreenOff = false;
            this.mPowerState.dismissColorFadeResources();
        }
        if (target == 2) {
            if (setScreenState(2)) {
                this.mPowerState.setColorFadeLevel(1.0f);
                this.mPowerState.dismissColorFade();
            }
        } else if (target == 5) {
            if (!(this.mScreenBrightnessRampAnimator.isAnimating() && this.mPowerState.getScreenState() == 2) && setScreenState(5)) {
                this.mPowerState.setColorFadeLevel(1.0f);
                this.mPowerState.dismissColorFade();
            }
        } else if (target == 3) {
            if (!(this.mScreenBrightnessRampAnimator.isAnimating() && this.mPowerState.getScreenState() == 2) && setScreenState(3)) {
                this.mPowerState.setColorFadeLevel(1.0f);
                this.mPowerState.dismissColorFade();
            }
        } else if (target != 4) {
            this.mPendingScreenOff = true;
            if (!this.mColorFadeEnabled) {
                this.mPowerState.setColorFadeLevel(0.0f);
            }
            if (this.mPowerState.getColorFadeLevel() == 0.0f) {
                setScreenState(1);
                this.mPendingScreenOff = false;
                this.mPowerState.dismissColorFadeResources();
            } else {
                if (performScreenOffTransition) {
                    DisplayPowerState displayPowerState = this.mPowerState;
                    Context context = this.mContext;
                    if (!this.mColorFadeFadesConfig) {
                        i = 1;
                    }
                    if (displayPowerState.prepareColorFade(context, i) && this.mPowerState.getScreenState() != 1) {
                        this.mColorFadeOffAnimator.start();
                    }
                }
                this.mColorFadeOffAnimator.end();
            }
        } else if (!this.mScreenBrightnessRampAnimator.isAnimating() || this.mPowerState.getScreenState() == 4) {
            if (this.mPowerState.getScreenState() != 4) {
                if (setScreenState(3)) {
                    setScreenState(4);
                } else {
                    return;
                }
            }
            this.mPowerState.setColorFadeLevel(1.0f);
            this.mPowerState.dismissColorFade();
        }
    }

    private void setProximitySensorEnabled(boolean enable) {
        if (enable) {
            if (!this.mProximitySensorEnabled) {
                this.mProximitySensorEnabled = true;
                this.mSensorManager.registerListener(this.mProximitySensorListener, this.mProximitySensor, 3, this.mHandler);
            }
        } else if (this.mProximitySensorEnabled) {
            this.mProximitySensorEnabled = false;
            this.mProximity = -1;
            this.mPendingProximity = -1;
            this.mHandler.removeMessages(2);
            this.mSensorManager.unregisterListener(this.mProximitySensorListener);
            clearPendingProximityDebounceTime();
        }
    }

    private void handleProximitySensorEvent(long time, boolean positive) {
        if (this.mProximitySensorEnabled && (this.mPendingProximity != 0 || (positive ^ 1) == 0)) {
            if (this.mPendingProximity != 1 || !positive) {
                this.mHandler.removeMessages(2);
                if (positive) {
                    this.mPendingProximity = 1;
                    setPendingProximityDebounceTime(0 + time);
                } else {
                    this.mPendingProximity = 0;
                    setPendingProximityDebounceTime(250 + time);
                }
                debounceProximitySensor();
            }
        }
    }

    private void debounceProximitySensor() {
        if (this.mProximitySensorEnabled && this.mPendingProximity != -1 && this.mPendingProximityDebounceTime >= 0) {
            if (this.mPendingProximityDebounceTime <= SystemClock.uptimeMillis()) {
                this.mProximity = this.mPendingProximity;
                updatePowerState();
                clearPendingProximityDebounceTime();
                return;
            }
            Message msg = this.mHandler.obtainMessage(2);
            msg.setAsynchronous(true);
            this.mHandler.sendMessageAtTime(msg, this.mPendingProximityDebounceTime);
        }
    }

    private void clearPendingProximityDebounceTime() {
        if (this.mPendingProximityDebounceTime >= 0) {
            this.mPendingProximityDebounceTime = -1;
            this.mCallbacks.releaseSuspendBlocker();
        }
    }

    private void setPendingProximityDebounceTime(long debounceTime) {
        if (this.mPendingProximityDebounceTime < 0) {
            this.mCallbacks.acquireSuspendBlocker();
        }
        this.mPendingProximityDebounceTime = debounceTime;
    }

    private void sendOnStateChangedWithWakelock() {
        this.mCallbacks.acquireSuspendBlocker();
        this.mHandler.post(this.mOnStateChangedRunnable);
    }

    private void sendOnProximityPositiveWithWakelock() {
        this.mCallbacks.acquireSuspendBlocker();
        this.mHandler.post(this.mOnProximityPositiveRunnable);
    }

    private void sendOnProximityNegativeWithWakelock() {
        this.mCallbacks.acquireSuspendBlocker();
        this.mHandler.post(this.mOnProximityNegativeRunnable);
    }

    public void dump(final PrintWriter pw) {
        synchronized (this.mLock) {
            pw.println();
            pw.println("Display Power Controller Locked State:");
            pw.println("  mDisplayReadyLocked=" + this.mDisplayReadyLocked);
            pw.println("  mPendingRequestLocked=" + this.mPendingRequestLocked);
            pw.println("  mPendingRequestChangedLocked=" + this.mPendingRequestChangedLocked);
            pw.println("  mPendingWaitForNegativeProximityLocked=" + this.mPendingWaitForNegativeProximityLocked);
            pw.println("  mPendingUpdatePowerStateLocked=" + this.mPendingUpdatePowerStateLocked);
        }
        pw.println();
        pw.println("Display Power Controller Configuration:");
        pw.println("  mScreenBrightnessDozeConfig=" + this.mScreenBrightnessDozeConfig);
        pw.println("  mScreenBrightnessDimConfig=" + this.mScreenBrightnessDimConfig);
        pw.println("  mScreenBrightnessDarkConfig=" + this.mScreenBrightnessDarkConfig);
        pw.println("  mScreenBrightnessRangeMinimum=" + this.mScreenBrightnessRangeMinimum);
        pw.println("  mScreenBrightnessRangeMaximum=" + this.mScreenBrightnessRangeMaximum);
        pw.println("  mUseSoftwareAutoBrightnessConfig=" + this.mUseSoftwareAutoBrightnessConfig);
        pw.println("  mAllowAutoBrightnessWhileDozingConfig=" + this.mAllowAutoBrightnessWhileDozingConfig);
        pw.println("  mColorFadeFadesConfig=" + this.mColorFadeFadesConfig);
        this.mHandler.runWithScissors(new Runnable() {
            public void run() {
                DisplayPowerController.this.dumpLocal(pw);
            }
        }, 1000);
    }

    private void dumpLocal(PrintWriter pw) {
        pw.println();
        pw.println("Display Power Controller Thread State:");
        pw.println("  mPowerRequest=" + this.mPowerRequest);
        pw.println("  mWaitingForNegativeProximity=" + this.mWaitingForNegativeProximity);
        pw.println("  mProximitySensor=" + this.mProximitySensor);
        pw.println("  mProximitySensorEnabled=" + this.mProximitySensorEnabled);
        pw.println("  mProximityThreshold=" + this.mProximityThreshold);
        pw.println("  mProximity=" + proximityToString(this.mProximity));
        pw.println("  mPendingProximity=" + proximityToString(this.mPendingProximity));
        pw.println("  mPendingProximityDebounceTime=" + TimeUtils.formatUptime(this.mPendingProximityDebounceTime));
        pw.println("  mScreenOffBecauseOfProximity=" + this.mScreenOffBecauseOfProximity);
        pw.println("  mAppliedAutoBrightness=" + this.mAppliedAutoBrightness);
        pw.println("  mAppliedDimming=" + this.mAppliedDimming);
        pw.println("  mAppliedLowPower=" + this.mAppliedLowPower);
        pw.println("  mPendingScreenOnUnblocker=" + this.mPendingScreenOnUnblocker);
        pw.println("  mPendingScreenOff=" + this.mPendingScreenOff);
        pw.println("  mReportedToPolicy=" + reportedToPolicyToString(this.mReportedScreenStateToPolicy));
        pw.println("  mScreenBrightnessRampAnimator.isAnimating()=" + this.mScreenBrightnessRampAnimator.isAnimating());
        if (this.mColorFadeOnAnimator != null) {
            pw.println("  mColorFadeOnAnimator.isStarted()=" + this.mColorFadeOnAnimator.isStarted());
        }
        if (this.mColorFadeOffAnimator != null) {
            pw.println("  mColorFadeOffAnimator.isStarted()=" + this.mColorFadeOffAnimator.isStarted());
        }
        if (this.mPowerState != null) {
            this.mPowerState.dump(pw);
        }
        if (this.mAutomaticBrightnessController != null) {
            this.mAutomaticBrightnessController.dump(pw);
        }
    }

    private static String proximityToString(int state) {
        switch (state) {
            case -1:
                return "Unknown";
            case 0:
                return "Negative";
            case 1:
                return "Positive";
            default:
                return Integer.toString(state);
        }
    }

    private static String reportedToPolicyToString(int state) {
        switch (state) {
            case 0:
                return "REPORTED_TO_POLICY_SCREEN_OFF";
            case 1:
                return "REPORTED_TO_POLICY_SCREEN_TURNING_ON";
            case 2:
                return "REPORTED_TO_POLICY_SCREEN_ON";
            default:
                return Integer.toString(state);
        }
    }

    private static Spline createAutoBrightnessSpline(int[] lux, int[] brightness) {
        if (lux == null || lux.length == 0 || brightness == null || brightness.length == 0) {
            Slog.e(TAG, "Could not create auto-brightness spline.");
            return null;
        }
        try {
            int n = brightness.length;
            float[] x = new float[n];
            float[] y = new float[n];
            y[0] = normalizeAbsoluteBrightness(brightness[0]);
            for (int i = 1; i < n; i++) {
                x[i] = (float) lux[i - 1];
                y[i] = normalizeAbsoluteBrightness(brightness[i]);
            }
            return Spline.createSpline(x, y);
        } catch (IllegalArgumentException ex) {
            Slog.e(TAG, "Could not create auto-brightness spline.", ex);
            return null;
        }
    }

    private static float normalizeAbsoluteBrightness(int value) {
        return ((float) clampAbsoluteBrightness(value)) / 255.0f;
    }

    private static int clampAbsoluteBrightness(int value) {
        return MathUtils.constrain(value, 0, 255);
    }
}
