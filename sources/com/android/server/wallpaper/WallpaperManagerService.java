package com.android.server.wallpaper;

import android.app.ActivityManager;
import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.app.IWallpaperManager.Stub;
import android.app.IWallpaperManagerCallback;
import android.app.UserSwitchObserver;
import android.app.WallpaperColors;
import android.app.WallpaperInfo;
import android.app.WallpaperManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.content.pm.UserInfo;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.IRemoteCallback;
import android.os.ParcelFileDescriptor;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.SELinux;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.Trace;
import android.os.UserHandle;
import android.os.UserManager;
import android.service.wallpaper.IWallpaperConnection;
import android.service.wallpaper.IWallpaperEngine;
import android.service.wallpaper.IWallpaperService;
import android.system.ErrnoException;
import android.system.Os;
import android.util.EventLog;
import android.util.Slog;
import android.util.SparseArray;
import android.view.IWindowManager;
import android.view.WindowManager;
import com.android.internal.content.PackageMonitor;
import com.android.internal.os.BackgroundThread;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.JournaledFile;
import com.android.server.EventLogTags;
import com.android.server.FgThread;
import com.android.server.SystemService;
import com.android.server.usb.UsbAudioDevice;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class WallpaperManagerService extends Stub {
    static final boolean DEBUG = false;
    static final boolean DEBUG_LIVE = true;
    static final int MAX_WALLPAPER_COMPONENT_LOG_LENGTH = 128;
    static final long MIN_WALLPAPER_CRASH_TIME = 10000;
    static final String TAG = "WallpaperManagerService";
    static final String WALLPAPER = "wallpaper_orig";
    static final String WALLPAPER_CROP = "wallpaper";
    static final String WALLPAPER_INFO = "wallpaper_info.xml";
    static final String WALLPAPER_LOCK_CROP = "wallpaper_lock";
    static final String WALLPAPER_LOCK_ORIG = "wallpaper_lock_orig";
    static final String[] sPerUserFiles = new String[]{WALLPAPER, WALLPAPER_CROP, WALLPAPER_LOCK_ORIG, WALLPAPER_LOCK_CROP, WALLPAPER_INFO};
    final AppOpsManager mAppOpsManager;
    final SparseArray<RemoteCallbackList<IWallpaperManagerCallback>> mColorsChangedListeners;
    final Context mContext;
    int mCurrentUserId;
    final ComponentName mDefaultWallpaperComponent;
    final IPackageManager mIPackageManager;
    final IWindowManager mIWindowManager;
    final ComponentName mImageWallpaper;
    IWallpaperManagerCallback mKeyguardListener;
    WallpaperData mLastWallpaper;
    final Object mLock = new Object();
    final SparseArray<WallpaperData> mLockWallpaperMap = new SparseArray();
    final MyPackageMonitor mMonitor;
    boolean mShuttingDown;
    final SparseArray<Boolean> mUserRestorecon = new SparseArray();
    boolean mWaitingForUnlock;
    int mWallpaperId;
    final SparseArray<WallpaperData> mWallpaperMap = new SparseArray();

    public static class Lifecycle extends SystemService {
        private WallpaperManagerService mService;

        public Lifecycle(Context context) {
            super(context);
        }

        public void onStart() {
            this.mService = new WallpaperManagerService(getContext());
            publishBinderService(WallpaperManagerService.WALLPAPER_CROP, this.mService);
        }

        public void onBootPhase(int phase) {
            if (phase == SystemService.PHASE_ACTIVITY_MANAGER_READY) {
                this.mService.systemReady();
            } else if (phase == 600) {
                this.mService.switchUser(0, null);
            }
        }

        public void onUnlockUser(int userHandle) {
            this.mService.onUnlockUser(userHandle);
        }
    }

    class MyPackageMonitor extends PackageMonitor {
        MyPackageMonitor() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onPackageUpdateFinished(java.lang.String r9, int r10) {
            /*
            r8 = this;
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;
            r6 = r0.mLock;
            monitor-enter(r6);
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0090 }
            r0 = r0.mCurrentUserId;	 Catch:{ all -> 0x0090 }
            r2 = r8.getChangingUserId();	 Catch:{ all -> 0x0090 }
            if (r0 == r2) goto L_0x0011;
        L_0x000f:
            monitor-exit(r6);
            return;
        L_0x0011:
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0090 }
            r0 = r0.mWallpaperMap;	 Catch:{ all -> 0x0090 }
            r2 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0090 }
            r2 = r2.mCurrentUserId;	 Catch:{ all -> 0x0090 }
            r4 = r0.get(r2);	 Catch:{ all -> 0x0090 }
            r4 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r4;	 Catch:{ all -> 0x0090 }
            if (r4 == 0) goto L_0x008e;
        L_0x0021:
            r1 = r4.wallpaperComponent;	 Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x008e;
        L_0x0025:
            r0 = r1.getPackageName();	 Catch:{ all -> 0x0090 }
            r0 = r0.equals(r9);	 Catch:{ all -> 0x0090 }
            if (r0 == 0) goto L_0x008e;
        L_0x002f:
            r0 = "WallpaperManagerService";
            r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0090 }
            r2.<init>();	 Catch:{ all -> 0x0090 }
            r3 = "Wallpaper ";
            r2 = r2.append(r3);	 Catch:{ all -> 0x0090 }
            r2 = r2.append(r1);	 Catch:{ all -> 0x0090 }
            r3 = " update has finished";
            r2 = r2.append(r3);	 Catch:{ all -> 0x0090 }
            r2 = r2.toString();	 Catch:{ all -> 0x0090 }
            android.util.Slog.i(r0, r2);	 Catch:{ all -> 0x0090 }
            r0 = 0;
            r4.wallpaperUpdating = r0;	 Catch:{ all -> 0x0090 }
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0090 }
            r0.clearWallpaperComponentLocked(r4);	 Catch:{ all -> 0x0090 }
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0090 }
            r2 = 0;
            r3 = 0;
            r5 = 0;
            r0 = r0.bindWallpaperComponentLocked(r1, r2, r3, r4, r5);	 Catch:{ all -> 0x0090 }
            if (r0 != 0) goto L_0x008e;
        L_0x0063:
            r0 = "WallpaperManagerService";
            r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0090 }
            r2.<init>();	 Catch:{ all -> 0x0090 }
            r3 = "Wallpaper ";
            r2 = r2.append(r3);	 Catch:{ all -> 0x0090 }
            r2 = r2.append(r1);	 Catch:{ all -> 0x0090 }
            r3 = " no longer available; reverting to default";
            r2 = r2.append(r3);	 Catch:{ all -> 0x0090 }
            r2 = r2.toString();	 Catch:{ all -> 0x0090 }
            android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x0090 }
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0090 }
            r2 = r4.userId;	 Catch:{ all -> 0x0090 }
            r3 = 0;
            r5 = 1;
            r7 = 0;
            r0.clearWallpaperLocked(r3, r5, r2, r7);	 Catch:{ all -> 0x0090 }
        L_0x008e:
            monitor-exit(r6);
            return;
        L_0x0090:
            r0 = move-exception;
            monitor-exit(r6);
            throw r0;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.MyPackageMonitor.onPackageUpdateFinished(java.lang.String, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onPackageModified(java.lang.String r5) {
            /*
            r4 = this;
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x003b }
            r1 = r1.mCurrentUserId;	 Catch:{ all -> 0x003b }
            r3 = r4.getChangingUserId();	 Catch:{ all -> 0x003b }
            if (r1 == r3) goto L_0x0011;
        L_0x000f:
            monitor-exit(r2);
            return;
        L_0x0011:
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x003b }
            r1 = r1.mWallpaperMap;	 Catch:{ all -> 0x003b }
            r3 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x003b }
            r3 = r3.mCurrentUserId;	 Catch:{ all -> 0x003b }
            r0 = r1.get(r3);	 Catch:{ all -> 0x003b }
            r0 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r0;	 Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x0039;
        L_0x0021:
            r1 = r0.wallpaperComponent;	 Catch:{ all -> 0x003b }
            if (r1 == 0) goto L_0x0033;
        L_0x0025:
            r1 = r0.wallpaperComponent;	 Catch:{ all -> 0x003b }
            r1 = r1.getPackageName();	 Catch:{ all -> 0x003b }
            r1 = r1.equals(r5);	 Catch:{ all -> 0x003b }
            r1 = r1 ^ 1;
            if (r1 == 0) goto L_0x0035;
        L_0x0033:
            monitor-exit(r2);
            return;
        L_0x0035:
            r1 = 1;
            r4.doPackagesChangedLocked(r1, r0);	 Catch:{ all -> 0x003b }
        L_0x0039:
            monitor-exit(r2);
            return;
        L_0x003b:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.MyPackageMonitor.onPackageModified(java.lang.String):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onPackageUpdateStarted(java.lang.String r6, int r7) {
            /*
            r5 = this;
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x006a }
            r1 = r1.mCurrentUserId;	 Catch:{ all -> 0x006a }
            r3 = r5.getChangingUserId();	 Catch:{ all -> 0x006a }
            if (r1 == r3) goto L_0x0011;
        L_0x000f:
            monitor-exit(r2);
            return;
        L_0x0011:
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x006a }
            r1 = r1.mWallpaperMap;	 Catch:{ all -> 0x006a }
            r3 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x006a }
            r3 = r3.mCurrentUserId;	 Catch:{ all -> 0x006a }
            r0 = r1.get(r3);	 Catch:{ all -> 0x006a }
            r0 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r0;	 Catch:{ all -> 0x006a }
            if (r0 == 0) goto L_0x0068;
        L_0x0021:
            r1 = r0.wallpaperComponent;	 Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0068;
        L_0x0025:
            r1 = r0.wallpaperComponent;	 Catch:{ all -> 0x006a }
            r1 = r1.getPackageName();	 Catch:{ all -> 0x006a }
            r1 = r1.equals(r6);	 Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0068;
        L_0x0031:
            r1 = "WallpaperManagerService";
            r3 = new java.lang.StringBuilder;	 Catch:{ all -> 0x006a }
            r3.<init>();	 Catch:{ all -> 0x006a }
            r4 = "Wallpaper service ";
            r3 = r3.append(r4);	 Catch:{ all -> 0x006a }
            r4 = r0.wallpaperComponent;	 Catch:{ all -> 0x006a }
            r3 = r3.append(r4);	 Catch:{ all -> 0x006a }
            r4 = " is updating";
            r3 = r3.append(r4);	 Catch:{ all -> 0x006a }
            r3 = r3.toString();	 Catch:{ all -> 0x006a }
            android.util.Slog.i(r1, r3);	 Catch:{ all -> 0x006a }
            r1 = 1;
            r0.wallpaperUpdating = r1;	 Catch:{ all -> 0x006a }
            r1 = r0.connection;	 Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x0068;
        L_0x005b:
            r1 = com.android.server.FgThread.getHandler();	 Catch:{ all -> 0x006a }
            r3 = r0.connection;	 Catch:{ all -> 0x006a }
            r3 = r3.mResetRunnable;	 Catch:{ all -> 0x006a }
            r1.removeCallbacks(r3);	 Catch:{ all -> 0x006a }
        L_0x0068:
            monitor-exit(r2);
            return;
        L_0x006a:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.MyPackageMonitor.onPackageUpdateStarted(java.lang.String, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onHandleForceStop(android.content.Intent r7, java.lang.String[] r8, int r9, boolean r10) {
            /*
            r6 = this;
            r3 = com.android.server.wallpaper.WallpaperManagerService.this;
            r4 = r3.mLock;
            monitor-enter(r4);
            r0 = 0;
            r3 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x002a }
            r3 = r3.mCurrentUserId;	 Catch:{ all -> 0x002a }
            r5 = r6.getChangingUserId();	 Catch:{ all -> 0x002a }
            if (r3 == r5) goto L_0x0013;
        L_0x0010:
            r3 = 0;
            monitor-exit(r4);
            return r3;
        L_0x0013:
            r3 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x002a }
            r3 = r3.mWallpaperMap;	 Catch:{ all -> 0x002a }
            r5 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x002a }
            r5 = r5.mCurrentUserId;	 Catch:{ all -> 0x002a }
            r2 = r3.get(r5);	 Catch:{ all -> 0x002a }
            r2 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r2;	 Catch:{ all -> 0x002a }
            if (r2 == 0) goto L_0x0028;
        L_0x0023:
            r1 = r6.doPackagesChangedLocked(r10, r2);	 Catch:{ all -> 0x002a }
            r0 = r1;
        L_0x0028:
            monitor-exit(r4);
            return r0;
        L_0x002a:
            r3 = move-exception;
            monitor-exit(r4);
            throw r3;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.MyPackageMonitor.onHandleForceStop(android.content.Intent, java.lang.String[], int, boolean):boolean");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onSomePackagesChanged() {
            /*
            r4 = this;
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;
            r2 = r1.mLock;
            monitor-enter(r2);
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0027 }
            r1 = r1.mCurrentUserId;	 Catch:{ all -> 0x0027 }
            r3 = r4.getChangingUserId();	 Catch:{ all -> 0x0027 }
            if (r1 == r3) goto L_0x0011;
        L_0x000f:
            monitor-exit(r2);
            return;
        L_0x0011:
            r1 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0027 }
            r1 = r1.mWallpaperMap;	 Catch:{ all -> 0x0027 }
            r3 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0027 }
            r3 = r3.mCurrentUserId;	 Catch:{ all -> 0x0027 }
            r0 = r1.get(r3);	 Catch:{ all -> 0x0027 }
            r0 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r0;	 Catch:{ all -> 0x0027 }
            if (r0 == 0) goto L_0x0025;
        L_0x0021:
            r1 = 1;
            r4.doPackagesChangedLocked(r1, r0);	 Catch:{ all -> 0x0027 }
        L_0x0025:
            monitor-exit(r2);
            return;
        L_0x0027:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.MyPackageMonitor.onSomePackagesChanged():void");
        }

        boolean doPackagesChangedLocked(boolean doit, WallpaperData wallpaper) {
            int change;
            boolean changed = false;
            if (wallpaper.wallpaperComponent != null) {
                change = isPackageDisappearing(wallpaper.wallpaperComponent.getPackageName());
                if (change == 3 || change == 2) {
                    changed = true;
                    if (doit) {
                        Slog.w(WallpaperManagerService.TAG, "Wallpaper uninstalled, removing: " + wallpaper.wallpaperComponent);
                        WallpaperManagerService.this.clearWallpaperLocked(false, 1, wallpaper.userId, null);
                    }
                }
            }
            if (wallpaper.nextWallpaperComponent != null) {
                change = isPackageDisappearing(wallpaper.nextWallpaperComponent.getPackageName());
                if (change == 3 || change == 2) {
                    wallpaper.nextWallpaperComponent = null;
                }
            }
            if (wallpaper.wallpaperComponent != null && isPackageModified(wallpaper.wallpaperComponent.getPackageName())) {
                try {
                    WallpaperManagerService.this.mContext.getPackageManager().getServiceInfo(wallpaper.wallpaperComponent, 786432);
                } catch (NameNotFoundException e) {
                    Slog.w(WallpaperManagerService.TAG, "Wallpaper component gone, removing: " + wallpaper.wallpaperComponent);
                    WallpaperManagerService.this.clearWallpaperLocked(false, 1, wallpaper.userId, null);
                }
            }
            if (wallpaper.nextWallpaperComponent != null && isPackageModified(wallpaper.nextWallpaperComponent.getPackageName())) {
                try {
                    WallpaperManagerService.this.mContext.getPackageManager().getServiceInfo(wallpaper.nextWallpaperComponent, 786432);
                } catch (NameNotFoundException e2) {
                    wallpaper.nextWallpaperComponent = null;
                }
            }
            return changed;
        }
    }

    class WallpaperConnection extends IWallpaperConnection.Stub implements ServiceConnection {
        private static final long WALLPAPER_RECONNECT_TIMEOUT_MS = 10000;
        boolean mDimensionsChanged = false;
        IWallpaperEngine mEngine;
        final WallpaperInfo mInfo;
        boolean mPaddingChanged = false;
        IRemoteCallback mReply;
        private Runnable mResetRunnable = new -$Lambda$ZWcNEw3ZwVVSi_pP2mGGLvztkS0((byte) 0, this);
        IWallpaperService mService;
        final Binder mToken = new Binder();
        WallpaperData mWallpaper;

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        /* synthetic */ void lambda$-com_android_server_wallpaper_WallpaperManagerService$WallpaperConnection_32460() {
            /*
            r6 = this;
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;
            r1 = r0.mLock;
            monitor-enter(r1);
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0059 }
            r0 = r0.mShuttingDown;	 Catch:{ all -> 0x0059 }
            if (r0 == 0) goto L_0x0016;
        L_0x000b:
            r0 = "WallpaperManagerService";
            r2 = "Ignoring relaunch timeout during shutdown";
            android.util.Slog.i(r0, r2);	 Catch:{ all -> 0x0059 }
            monitor-exit(r1);
            return;
        L_0x0016:
            r0 = r6.mWallpaper;	 Catch:{ all -> 0x0059 }
            r0 = r0.wallpaperUpdating;	 Catch:{ all -> 0x0059 }
            if (r0 != 0) goto L_0x0057;
        L_0x001c:
            r0 = r6.mWallpaper;	 Catch:{ all -> 0x0059 }
            r0 = r0.userId;	 Catch:{ all -> 0x0059 }
            r2 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0059 }
            r2 = r2.mCurrentUserId;	 Catch:{ all -> 0x0059 }
            if (r0 != r2) goto L_0x0057;
        L_0x0026:
            r0 = "WallpaperManagerService";
            r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0059 }
            r2.<init>();	 Catch:{ all -> 0x0059 }
            r3 = "Wallpaper reconnect timed out for ";
            r2 = r2.append(r3);	 Catch:{ all -> 0x0059 }
            r3 = r6.mWallpaper;	 Catch:{ all -> 0x0059 }
            r3 = r3.wallpaperComponent;	 Catch:{ all -> 0x0059 }
            r2 = r2.append(r3);	 Catch:{ all -> 0x0059 }
            r3 = ", reverting to built-in wallpaper!";
            r2 = r2.append(r3);	 Catch:{ all -> 0x0059 }
            r2 = r2.toString();	 Catch:{ all -> 0x0059 }
            android.util.Slog.w(r0, r2);	 Catch:{ all -> 0x0059 }
            r0 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0059 }
            r2 = r6.mWallpaper;	 Catch:{ all -> 0x0059 }
            r2 = r2.userId;	 Catch:{ all -> 0x0059 }
            r3 = 1;
            r4 = 1;
            r5 = 0;
            r0.clearWallpaperLocked(r3, r4, r2, r5);	 Catch:{ all -> 0x0059 }
        L_0x0057:
            monitor-exit(r1);
            return;
        L_0x0059:
            r0 = move-exception;
            monitor-exit(r1);
            throw r0;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.WallpaperConnection.lambda$-com_android_server_wallpaper_WallpaperManagerService$WallpaperConnection_32460():void");
        }

        public WallpaperConnection(WallpaperInfo info, WallpaperData wallpaper) {
            this.mInfo = info;
            this.mWallpaper = wallpaper;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            synchronized (WallpaperManagerService.this.mLock) {
                if (this.mWallpaper.connection == this) {
                    this.mService = IWallpaperService.Stub.asInterface(service);
                    WallpaperManagerService.this.attachServiceLocked(this, this.mWallpaper);
                    WallpaperManagerService.this.saveSettingsLocked(this.mWallpaper.userId);
                    FgThread.getHandler().removeCallbacks(this.mResetRunnable);
                }
            }
        }

        public void onServiceDisconnected(ComponentName name) {
            synchronized (WallpaperManagerService.this.mLock) {
                Slog.w(WallpaperManagerService.TAG, "Wallpaper service gone: " + name);
                if (!Objects.equals(name, this.mWallpaper.wallpaperComponent)) {
                    Slog.e(WallpaperManagerService.TAG, "Does not match expected wallpaper component " + this.mWallpaper.wallpaperComponent);
                }
                this.mService = null;
                this.mEngine = null;
                if (this.mWallpaper.connection == this && !this.mWallpaper.wallpaperUpdating) {
                    WallpaperManagerService.this.mContext.getMainThreadHandler().postDelayed(new -$Lambda$ZWcNEw3ZwVVSi_pP2mGGLvztkS0((byte) 1, this), 1000);
                }
            }
        }

        /* synthetic */ void lambda$-com_android_server_wallpaper_WallpaperManagerService$WallpaperConnection_35472() {
            processDisconnect(this);
        }

        private void processDisconnect(ServiceConnection connection) {
            synchronized (WallpaperManagerService.this.mLock) {
                if (connection == this.mWallpaper.connection) {
                    ComponentName wpService = this.mWallpaper.wallpaperComponent;
                    if (!(this.mWallpaper.wallpaperUpdating || this.mWallpaper.userId != WallpaperManagerService.this.mCurrentUserId || (Objects.equals(WallpaperManagerService.this.mDefaultWallpaperComponent, wpService) ^ 1) == 0 || (Objects.equals(WallpaperManagerService.this.mImageWallpaper, wpService) ^ 1) == 0)) {
                        if (this.mWallpaper.lastDiedTime == 0 || this.mWallpaper.lastDiedTime + 10000 <= SystemClock.uptimeMillis()) {
                            this.mWallpaper.lastDiedTime = SystemClock.uptimeMillis();
                            Handler fgHandler = FgThread.getHandler();
                            fgHandler.removeCallbacks(this.mResetRunnable);
                            fgHandler.postDelayed(this.mResetRunnable, 10000);
                            Slog.i(WallpaperManagerService.TAG, "Started wallpaper reconnect timeout for " + wpService);
                        } else {
                            Slog.w(WallpaperManagerService.TAG, "Reverting to built-in wallpaper!");
                            WallpaperManagerService.this.clearWallpaperLocked(true, 1, this.mWallpaper.userId, null);
                        }
                        String flattened = wpService.flattenToString();
                        EventLog.writeEvent(EventLogTags.WP_WALLPAPER_CRASHED, flattened.substring(0, Math.min(flattened.length(), 128)));
                    }
                } else {
                    Slog.i(WallpaperManagerService.TAG, "Wallpaper changed during disconnect tracking; ignoring");
                }
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onWallpaperColorsChanged(android.app.WallpaperColors r6) {
            /*
            r5 = this;
            r2 = com.android.server.wallpaper.WallpaperManagerService.this;
            r3 = r2.mLock;
            monitor-enter(r3);
            r2 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0036 }
            r2 = r2.mImageWallpaper;	 Catch:{ all -> 0x0036 }
            r4 = r5.mWallpaper;	 Catch:{ all -> 0x0036 }
            r4 = r4.wallpaperComponent;	 Catch:{ all -> 0x0036 }
            r2 = r2.equals(r4);	 Catch:{ all -> 0x0036 }
            if (r2 == 0) goto L_0x0015;
        L_0x0013:
            monitor-exit(r3);
            return;
        L_0x0015:
            r2 = r5.mWallpaper;	 Catch:{ all -> 0x0036 }
            r2.primaryColors = r6;	 Catch:{ all -> 0x0036 }
            r1 = 1;
            r2 = com.android.server.wallpaper.WallpaperManagerService.this;	 Catch:{ all -> 0x0036 }
            r2 = r2.mLockWallpaperMap;	 Catch:{ all -> 0x0036 }
            r4 = r5.mWallpaper;	 Catch:{ all -> 0x0036 }
            r4 = r4.userId;	 Catch:{ all -> 0x0036 }
            r0 = r2.get(r4);	 Catch:{ all -> 0x0036 }
            r0 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r0;	 Catch:{ all -> 0x0036 }
            if (r0 != 0) goto L_0x002b;
        L_0x002a:
            r1 = 3;
        L_0x002b:
            monitor-exit(r3);
            if (r1 == 0) goto L_0x0035;
        L_0x002e:
            r2 = com.android.server.wallpaper.WallpaperManagerService.this;
            r3 = r5.mWallpaper;
            r2.notifyWallpaperColorsChanged(r3, r1);
        L_0x0035:
            return;
        L_0x0036:
            r2 = move-exception;
            monitor-exit(r3);
            throw r2;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.WallpaperConnection.onWallpaperColorsChanged(android.app.WallpaperColors):void");
        }

        public void attachEngine(IWallpaperEngine engine) {
            synchronized (WallpaperManagerService.this.mLock) {
                this.mEngine = engine;
                if (this.mDimensionsChanged) {
                    try {
                        this.mEngine.setDesiredSize(this.mWallpaper.width, this.mWallpaper.height);
                    } catch (RemoteException e) {
                        Slog.w(WallpaperManagerService.TAG, "Failed to set wallpaper dimensions", e);
                    }
                    this.mDimensionsChanged = false;
                }
                if (this.mPaddingChanged) {
                    try {
                        this.mEngine.setDisplayPadding(this.mWallpaper.padding);
                    } catch (RemoteException e2) {
                        Slog.w(WallpaperManagerService.TAG, "Failed to set wallpaper padding", e2);
                    }
                    this.mPaddingChanged = false;
                }
                try {
                    this.mEngine.requestWallpaperColors();
                } catch (RemoteException e22) {
                    Slog.w(WallpaperManagerService.TAG, "Failed to request wallpaper colors", e22);
                }
            }
        }

        public void engineShown(IWallpaperEngine engine) {
            synchronized (WallpaperManagerService.this.mLock) {
                if (this.mReply != null) {
                    long ident = Binder.clearCallingIdentity();
                    try {
                        this.mReply.sendResult(null);
                    } catch (RemoteException e) {
                        Binder.restoreCallingIdentity(ident);
                    }
                    this.mReply = null;
                }
            }
        }

        public ParcelFileDescriptor setWallpaper(String name) {
            synchronized (WallpaperManagerService.this.mLock) {
                if (this.mWallpaper.connection == this) {
                    ParcelFileDescriptor updateWallpaperBitmapLocked = WallpaperManagerService.this.updateWallpaperBitmapLocked(name, this.mWallpaper, null);
                    return updateWallpaperBitmapLocked;
                }
                return null;
            }
        }
    }

    static class WallpaperData {
        boolean allowBackup;
        private RemoteCallbackList<IWallpaperManagerCallback> callbacks = new RemoteCallbackList();
        WallpaperConnection connection;
        final File cropFile;
        final Rect cropHint = new Rect(0, 0, 0, 0);
        int height = -1;
        boolean imageWallpaperPending;
        long lastDiedTime;
        String name = "";
        ComponentName nextWallpaperComponent;
        final Rect padding = new Rect(0, 0, 0, 0);
        WallpaperColors primaryColors;
        IWallpaperManagerCallback setComplete;
        int userId;
        ComponentName wallpaperComponent;
        final File wallpaperFile;
        int wallpaperId;
        WallpaperObserver wallpaperObserver;
        boolean wallpaperUpdating;
        int whichPending;
        int width = -1;

        WallpaperData(int userId, String inputFileName, String cropFileName) {
            this.userId = userId;
            File wallpaperDir = WallpaperManagerService.getWallpaperDir(userId);
            this.wallpaperFile = new File(wallpaperDir, inputFileName);
            this.cropFile = new File(wallpaperDir, cropFileName);
        }

        boolean cropExists() {
            return this.cropFile.exists();
        }

        boolean sourceExists() {
            return this.wallpaperFile.exists();
        }
    }

    private class WallpaperObserver extends FileObserver {
        final int mUserId;
        final WallpaperData mWallpaper;
        final File mWallpaperDir;
        final File mWallpaperFile = new File(this.mWallpaperDir, WallpaperManagerService.WALLPAPER);
        final File mWallpaperLockFile = new File(this.mWallpaperDir, WallpaperManagerService.WALLPAPER_LOCK_ORIG);

        public WallpaperObserver(WallpaperData wallpaper) {
            super(WallpaperManagerService.getWallpaperDir(wallpaper.userId).getAbsolutePath(), 1672);
            this.mUserId = wallpaper.userId;
            this.mWallpaperDir = WallpaperManagerService.getWallpaperDir(wallpaper.userId);
            this.mWallpaper = wallpaper;
        }

        private WallpaperData dataForEvent(boolean sysChanged, boolean lockChanged) {
            WallpaperData wallpaper = null;
            synchronized (WallpaperManagerService.this.mLock) {
                if (lockChanged) {
                    wallpaper = (WallpaperData) WallpaperManagerService.this.mLockWallpaperMap.get(this.mUserId);
                }
                if (wallpaper == null) {
                    wallpaper = (WallpaperData) WallpaperManagerService.this.mWallpaperMap.get(this.mUserId);
                }
            }
            if (wallpaper != null) {
                return wallpaper;
            }
            return this.mWallpaper;
        }

        public void onEvent(int event, String path) {
            if (path != null) {
                boolean moved = event == 128;
                boolean z = event != 8 ? moved : true;
                File changedFile = new File(this.mWallpaperDir, path);
                boolean sysWallpaperChanged = this.mWallpaperFile.equals(changedFile);
                boolean lockWallpaperChanged = this.mWallpaperLockFile.equals(changedFile);
                int notifyColorsWhich = 0;
                WallpaperData wallpaper = dataForEvent(sysWallpaperChanged, lockWallpaperChanged);
                if (moved && lockWallpaperChanged) {
                    SELinux.restorecon(changedFile);
                    WallpaperManagerService.this.notifyLockWallpaperChanged();
                    WallpaperManagerService.this.notifyWallpaperColorsChanged(wallpaper, 2);
                    return;
                }
                synchronized (WallpaperManagerService.this.mLock) {
                    if (sysWallpaperChanged || lockWallpaperChanged) {
                        WallpaperManagerService.this.notifyCallbacksLocked(wallpaper);
                        if (wallpaper.wallpaperComponent != null && event == 8) {
                            if (wallpaper.imageWallpaperPending) {
                            }
                        }
                        if (z) {
                            SELinux.restorecon(changedFile);
                            if (moved) {
                                WallpaperManagerService.this.loadSettingsLocked(wallpaper.userId, true);
                            }
                            WallpaperManagerService.this.generateCrop(wallpaper);
                            wallpaper.imageWallpaperPending = false;
                            if (sysWallpaperChanged) {
                                WallpaperManagerService.this.bindWallpaperComponentLocked(WallpaperManagerService.this.mImageWallpaper, true, false, wallpaper, null);
                                notifyColorsWhich = 1;
                            }
                            if (lockWallpaperChanged || (wallpaper.whichPending & 2) != 0) {
                                if (!lockWallpaperChanged) {
                                    WallpaperManagerService.this.mLockWallpaperMap.remove(wallpaper.userId);
                                }
                                WallpaperManagerService.this.notifyLockWallpaperChanged();
                                notifyColorsWhich |= 2;
                            }
                            WallpaperManagerService.this.saveSettingsLocked(wallpaper.userId);
                            if (wallpaper.setComplete != null) {
                                try {
                                    wallpaper.setComplete.onWallpaperChanged();
                                } catch (RemoteException e) {
                                }
                            }
                        }
                    }
                }
                if (notifyColorsWhich != 0) {
                    WallpaperManagerService.this.notifyWallpaperColorsChanged(wallpaper, notifyColorsWhich);
                }
            }
        }
    }

    void notifyLockWallpaperChanged() {
        IWallpaperManagerCallback cb = this.mKeyguardListener;
        if (cb != null) {
            try {
                cb.onWallpaperChanged();
            } catch (RemoteException e) {
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void notifyWallpaperColorsChanged(com.android.server.wallpaper.WallpaperManagerService.WallpaperData r7, int r8) {
        /*
        r6 = this;
        r4 = r6.mLock;
        monitor-enter(r4);
        r3 = r6.mColorsChangedListeners;	 Catch:{ all -> 0x0041 }
        r5 = r7.userId;	 Catch:{ all -> 0x0041 }
        r0 = r3.get(r5);	 Catch:{ all -> 0x0041 }
        r0 = (android.os.RemoteCallbackList) r0;	 Catch:{ all -> 0x0041 }
        r3 = r6.mColorsChangedListeners;	 Catch:{ all -> 0x0041 }
        r5 = -1;
        r2 = r3.get(r5);	 Catch:{ all -> 0x0041 }
        r2 = (android.os.RemoteCallbackList) r2;	 Catch:{ all -> 0x0041 }
        r3 = emptyCallbackList(r0);	 Catch:{ all -> 0x0041 }
        if (r3 == 0) goto L_0x0024;
    L_0x001c:
        r3 = emptyCallbackList(r2);	 Catch:{ all -> 0x0041 }
        if (r3 == 0) goto L_0x0024;
    L_0x0022:
        monitor-exit(r4);
        return;
    L_0x0024:
        r3 = r7.primaryColors;	 Catch:{ all -> 0x0041 }
        if (r3 != 0) goto L_0x003f;
    L_0x0028:
        r1 = 1;
    L_0x0029:
        monitor-exit(r4);
        r3 = r7.primaryColors;
        r4 = r7.userId;
        r6.notifyColorListeners(r3, r8, r4);
        if (r1 == 0) goto L_0x004c;
    L_0x0033:
        r6.extractColors(r7);
        r3 = r6.mLock;
        monitor-enter(r3);
        r4 = r7.primaryColors;	 Catch:{ all -> 0x004d }
        if (r4 != 0) goto L_0x0044;
    L_0x003d:
        monitor-exit(r3);
        return;
    L_0x003f:
        r1 = 0;
        goto L_0x0029;
    L_0x0041:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
    L_0x0044:
        monitor-exit(r3);
        r3 = r7.primaryColors;
        r4 = r7.userId;
        r6.notifyColorListeners(r3, r8, r4);
    L_0x004c:
        return;
    L_0x004d:
        r4 = move-exception;
        monitor-exit(r3);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.notifyWallpaperColorsChanged(com.android.server.wallpaper.WallpaperManagerService$WallpaperData, int):void");
    }

    private static <T extends IInterface> boolean emptyCallbackList(RemoteCallbackList<T> list) {
        return list == null || list.getRegisteredCallbackCount() == 0;
    }

    private void notifyColorListeners(WallpaperColors wallpaperColors, int which, int userId) {
        int count;
        ArrayList<IWallpaperManagerCallback> colorListeners = new ArrayList();
        synchronized (this.mLock) {
            int i;
            RemoteCallbackList<IWallpaperManagerCallback> currentUserColorListeners = (RemoteCallbackList) this.mColorsChangedListeners.get(userId);
            RemoteCallbackList<IWallpaperManagerCallback> userAllColorListeners = (RemoteCallbackList) this.mColorsChangedListeners.get(-1);
            IWallpaperManagerCallback keyguardListener = this.mKeyguardListener;
            if (currentUserColorListeners != null) {
                count = currentUserColorListeners.beginBroadcast();
                for (i = 0; i < count; i++) {
                    colorListeners.add((IWallpaperManagerCallback) currentUserColorListeners.getBroadcastItem(i));
                }
                currentUserColorListeners.finishBroadcast();
            }
            if (userAllColorListeners != null) {
                count = userAllColorListeners.beginBroadcast();
                for (i = 0; i < count; i++) {
                    colorListeners.add((IWallpaperManagerCallback) userAllColorListeners.getBroadcastItem(i));
                }
                userAllColorListeners.finishBroadcast();
            }
        }
        count = colorListeners.size();
        for (i = 0; i < count; i++) {
            try {
                ((IWallpaperManagerCallback) colorListeners.get(i)).onWallpaperColorsChanged(wallpaperColors, which, userId);
            } catch (RemoteException e) {
            }
        }
        if (keyguardListener != null) {
            try {
                keyguardListener.onWallpaperColorsChanged(wallpaperColors, which, userId);
            } catch (RemoteException e2) {
            }
        }
    }

    private void extractColors(WallpaperData wallpaper) {
        String cropFile = null;
        synchronized (this.mLock) {
            boolean imageWallpaper = !this.mImageWallpaper.equals(wallpaper.wallpaperComponent) ? wallpaper.wallpaperComponent == null : true;
            if (imageWallpaper && wallpaper.cropFile != null && wallpaper.cropFile.exists()) {
                cropFile = wallpaper.cropFile.getAbsolutePath();
            }
            int wallpaperId = wallpaper.wallpaperId;
        }
        WallpaperColors wallpaperColors = null;
        if (cropFile != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(cropFile);
            if (bitmap != null) {
                wallpaperColors = WallpaperColors.fromBitmap(bitmap);
                bitmap.recycle();
            }
        } else if (imageWallpaper) {
            wallpaperColors = new WallpaperColors(Color.valueOf(UsbAudioDevice.kAudioDeviceMetaMask), Color.valueOf(-5307626), Color.valueOf(-15466496), 6);
        }
        if (wallpaperColors == null) {
            Slog.w(TAG, "Cannot extract colors because wallpaper could not be read.");
            return;
        }
        synchronized (this.mLock) {
            if (wallpaper.wallpaperId == wallpaperId) {
                wallpaper.primaryColors = wallpaperColors;
                saveSettingsLocked(wallpaper.userId);
            } else {
                Slog.w(TAG, "Not setting primary colors since wallpaper changed");
            }
        }
    }

    private void generateCrop(WallpaperData wallpaper) {
        Object obj;
        boolean restorecon;
        Throwable th;
        boolean success = false;
        Rect cropHint = new Rect(wallpaper.cropHint);
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(wallpaper.wallpaperFile.getAbsolutePath(), options);
        if (options.outWidth <= 0 || options.outHeight <= 0) {
            Slog.w(TAG, "Invalid wallpaper data");
            success = false;
        } else {
            boolean needCrop = false;
            if (cropHint.isEmpty()) {
                cropHint.top = 0;
                cropHint.left = 0;
                cropHint.right = options.outWidth;
                cropHint.bottom = options.outHeight;
            } else {
                cropHint.offset(cropHint.right > options.outWidth ? options.outWidth - cropHint.right : 0, cropHint.bottom > options.outHeight ? options.outHeight - cropHint.bottom : 0);
                if (cropHint.left < 0) {
                    cropHint.left = 0;
                }
                if (cropHint.top < 0) {
                    cropHint.top = 0;
                }
                if (options.outHeight <= cropHint.height()) {
                    if (options.outWidth > cropHint.width()) {
                        needCrop = true;
                    } else {
                        needCrop = false;
                    }
                } else {
                    needCrop = true;
                }
            }
            boolean needScale = wallpaper.height != cropHint.height();
            if (needCrop || (needScale ^ 1) == 0) {
                AutoCloseable autoCloseable = null;
                BufferedOutputStream bos = null;
                try {
                    Options scaler;
                    BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(wallpaper.wallpaperFile.getAbsolutePath(), false);
                    int scale = 1;
                    while (scale * 2 < cropHint.height() / wallpaper.height) {
                        scale *= 2;
                    }
                    if (scale > 1) {
                        scaler = new Options();
                        scaler.inSampleSize = scale;
                    } else {
                        scaler = null;
                    }
                    Bitmap cropped = decoder.decodeRegion(cropHint, scaler);
                    decoder.recycle();
                    if (cropped == null) {
                        Slog.e(TAG, "Could not decode new wallpaper");
                    } else {
                        BufferedOutputStream bos2;
                        cropHint.offsetTo(0, 0);
                        cropHint.right /= scale;
                        cropHint.bottom /= scale;
                        Bitmap finalCrop = Bitmap.createScaledBitmap(cropped, (int) (((float) cropHint.width()) * (((float) wallpaper.height) / ((float) cropHint.height()))), wallpaper.height, true);
                        FileOutputStream f = new FileOutputStream(wallpaper.cropFile);
                        try {
                            bos2 = new BufferedOutputStream(f, 32768);
                        } catch (Exception e) {
                            obj = f;
                            IoUtils.closeQuietly(bos);
                            IoUtils.closeQuietly(autoCloseable);
                            if (!success) {
                                Slog.e(TAG, "Unable to apply new wallpaper");
                                wallpaper.cropFile.delete();
                            }
                            if (!wallpaper.cropFile.exists()) {
                                restorecon = SELinux.restorecon(wallpaper.cropFile.getAbsoluteFile());
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            obj = f;
                            IoUtils.closeQuietly(bos);
                            IoUtils.closeQuietly(autoCloseable);
                            throw th;
                        }
                        try {
                            finalCrop.compress(CompressFormat.JPEG, 100, bos2);
                            bos2.flush();
                            success = true;
                            bos = bos2;
                            obj = f;
                        } catch (Exception e2) {
                            bos = bos2;
                            obj = f;
                            IoUtils.closeQuietly(bos);
                            IoUtils.closeQuietly(autoCloseable);
                            if (success) {
                                Slog.e(TAG, "Unable to apply new wallpaper");
                                wallpaper.cropFile.delete();
                            }
                            if (!wallpaper.cropFile.exists()) {
                                restorecon = SELinux.restorecon(wallpaper.cropFile.getAbsoluteFile());
                            }
                        } catch (Throwable th3) {
                            th = th3;
                            bos = bos2;
                            obj = f;
                            IoUtils.closeQuietly(bos);
                            IoUtils.closeQuietly(autoCloseable);
                            throw th;
                        }
                    }
                    IoUtils.closeQuietly(bos);
                    IoUtils.closeQuietly(autoCloseable);
                } catch (Exception e3) {
                    IoUtils.closeQuietly(bos);
                    IoUtils.closeQuietly(autoCloseable);
                    if (success) {
                        Slog.e(TAG, "Unable to apply new wallpaper");
                        wallpaper.cropFile.delete();
                    }
                    if (!wallpaper.cropFile.exists()) {
                        restorecon = SELinux.restorecon(wallpaper.cropFile.getAbsoluteFile());
                    }
                } catch (Throwable th4) {
                    th = th4;
                    IoUtils.closeQuietly(bos);
                    IoUtils.closeQuietly(autoCloseable);
                    throw th;
                }
            }
            success = FileUtils.copyFile(wallpaper.wallpaperFile, wallpaper.cropFile);
            if (!success) {
                wallpaper.cropFile.delete();
            }
        }
        if (success) {
            Slog.e(TAG, "Unable to apply new wallpaper");
            wallpaper.cropFile.delete();
        }
        if (!wallpaper.cropFile.exists()) {
            restorecon = SELinux.restorecon(wallpaper.cropFile.getAbsoluteFile());
        }
    }

    int makeWallpaperIdLocked() {
        do {
            this.mWallpaperId++;
        } while (this.mWallpaperId == 0);
        return this.mWallpaperId;
    }

    public WallpaperManagerService(Context context) {
        this.mContext = context;
        this.mShuttingDown = false;
        this.mImageWallpaper = ComponentName.unflattenFromString(context.getResources().getString(17039969));
        this.mDefaultWallpaperComponent = WallpaperManager.getDefaultWallpaperComponent(context);
        this.mIWindowManager = IWindowManager.Stub.asInterface(ServiceManager.getService("window"));
        this.mIPackageManager = AppGlobals.getPackageManager();
        this.mAppOpsManager = (AppOpsManager) this.mContext.getSystemService("appops");
        this.mMonitor = new MyPackageMonitor();
        this.mMonitor.register(context, null, UserHandle.ALL, true);
        getWallpaperDir(0).mkdirs();
        loadSettingsLocked(0, false);
        getWallpaperSafeLocked(0, 1);
        this.mColorsChangedListeners = new SparseArray();
    }

    private static File getWallpaperDir(int userId) {
        return Environment.getUserSystemDirectory(userId);
    }

    protected void finalize() throws Throwable {
        super.finalize();
        for (int i = 0; i < this.mWallpaperMap.size(); i++) {
            ((WallpaperData) this.mWallpaperMap.valueAt(i)).wallpaperObserver.stopWatching();
        }
    }

    void systemReady() {
        WallpaperData wallpaper = (WallpaperData) this.mWallpaperMap.get(0);
        if (this.mImageWallpaper.equals(wallpaper.nextWallpaperComponent)) {
            if (!wallpaper.cropExists()) {
                generateCrop(wallpaper);
            }
            if (!wallpaper.cropExists()) {
                clearWallpaperLocked(false, 1, 0, null);
            }
        }
        IntentFilter userFilter = new IntentFilter();
        userFilter.addAction("android.intent.action.USER_REMOVED");
        this.mContext.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if ("android.intent.action.USER_REMOVED".equals(intent.getAction())) {
                    WallpaperManagerService.this.onRemoveUser(intent.getIntExtra("android.intent.extra.user_handle", -10000));
                }
            }
        }, userFilter);
        this.mContext.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if ("android.intent.action.ACTION_SHUTDOWN".equals(intent.getAction())) {
                    synchronized (WallpaperManagerService.this.mLock) {
                        WallpaperManagerService.this.mShuttingDown = true;
                    }
                }
            }
        }, new IntentFilter("android.intent.action.ACTION_SHUTDOWN"));
        try {
            ActivityManager.getService().registerUserSwitchObserver(new UserSwitchObserver() {
                public void onUserSwitching(int newUserId, IRemoteCallback reply) {
                    WallpaperManagerService.this.switchUser(newUserId, reply);
                }
            }, TAG);
        } catch (RemoteException e) {
            e.rethrowAsRuntimeException();
        }
    }

    public String getName() {
        if (Binder.getCallingUid() != 1000) {
            throw new RuntimeException("getName() can only be called from the system process");
        }
        String str;
        synchronized (this.mLock) {
            str = ((WallpaperData) this.mWallpaperMap.get(0)).name;
        }
        return str;
    }

    void stopObserver(WallpaperData wallpaper) {
        if (wallpaper != null && wallpaper.wallpaperObserver != null) {
            wallpaper.wallpaperObserver.stopWatching();
            wallpaper.wallpaperObserver = null;
        }
    }

    void stopObserversLocked(int userId) {
        stopObserver((WallpaperData) this.mWallpaperMap.get(userId));
        stopObserver((WallpaperData) this.mLockWallpaperMap.get(userId));
        this.mWallpaperMap.remove(userId);
        this.mLockWallpaperMap.remove(userId);
    }

    void onUnlockUser(final int userId) {
        synchronized (this.mLock) {
            if (this.mCurrentUserId == userId) {
                if (this.mWaitingForUnlock) {
                    switchUser(userId, null);
                }
                if (this.mUserRestorecon.get(userId) != Boolean.TRUE) {
                    this.mUserRestorecon.put(userId, Boolean.TRUE);
                    BackgroundThread.getHandler().post(new Runnable() {
                        public void run() {
                            File wallpaperDir = WallpaperManagerService.getWallpaperDir(userId);
                            for (String filename : WallpaperManagerService.sPerUserFiles) {
                                File f = new File(wallpaperDir, filename);
                                if (f.exists()) {
                                    SELinux.restorecon(f);
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    void onRemoveUser(int userId) {
        if (userId >= 1) {
            File wallpaperDir = getWallpaperDir(userId);
            synchronized (this.mLock) {
                stopObserversLocked(userId);
                for (String filename : sPerUserFiles) {
                    new File(wallpaperDir, filename).delete();
                }
                this.mUserRestorecon.remove(userId);
            }
        }
    }

    void switchUser(int userId, IRemoteCallback reply) {
        WallpaperData systemWallpaper;
        WallpaperData lockWallpaper;
        synchronized (this.mLock) {
            this.mCurrentUserId = userId;
            systemWallpaper = getWallpaperSafeLocked(userId, 1);
            WallpaperData tmpLockWallpaper = (WallpaperData) this.mLockWallpaperMap.get(userId);
            lockWallpaper = tmpLockWallpaper == null ? systemWallpaper : tmpLockWallpaper;
            if (systemWallpaper.wallpaperObserver == null) {
                systemWallpaper.wallpaperObserver = new WallpaperObserver(systemWallpaper);
                systemWallpaper.wallpaperObserver.startWatching();
            }
            switchWallpaper(systemWallpaper, reply);
        }
        FgThread.getHandler().post(new com.android.server.wallpaper.-$Lambda$ZWcNEw3ZwVVSi_pP2mGGLvztkS0.AnonymousClass1(this, systemWallpaper, lockWallpaper));
    }

    /* synthetic */ void lambda$-com_android_server_wallpaper_WallpaperManagerService_56569(WallpaperData systemWallpaper, WallpaperData lockWallpaper) {
        notifyWallpaperColorsChanged(systemWallpaper, 1);
        notifyWallpaperColorsChanged(lockWallpaper, 2);
    }

    void switchWallpaper(WallpaperData wallpaper, IRemoteCallback reply) {
        synchronized (this.mLock) {
            this.mWaitingForUnlock = false;
            ComponentName cname = wallpaper.wallpaperComponent != null ? wallpaper.wallpaperComponent : wallpaper.nextWallpaperComponent;
            if (!bindWallpaperComponentLocked(cname, true, false, wallpaper, reply)) {
                ServiceInfo si = null;
                try {
                    si = this.mIPackageManager.getServiceInfo(cname, DumpState.DUMP_DOMAIN_PREFERRED, wallpaper.userId);
                } catch (RemoteException e) {
                }
                if (si == null) {
                    Slog.w(TAG, "Failure starting previous wallpaper; clearing");
                    clearWallpaperLocked(false, 1, wallpaper.userId, reply);
                } else {
                    Slog.w(TAG, "Wallpaper isn't direct boot aware; using fallback until unlocked");
                    wallpaper.wallpaperComponent = wallpaper.nextWallpaperComponent;
                    WallpaperData fallback = new WallpaperData(wallpaper.userId, WALLPAPER_LOCK_ORIG, WALLPAPER_LOCK_CROP);
                    ensureSaneWallpaperData(fallback);
                    bindWallpaperComponentLocked(this.mImageWallpaper, true, false, fallback, reply);
                    this.mWaitingForUnlock = true;
                }
            }
        }
    }

    public void clearWallpaper(String callingPackage, int which, int userId) {
        checkPermission("android.permission.SET_WALLPAPER");
        if (isWallpaperSupported(callingPackage) && (isSetWallpaperAllowed(callingPackage) ^ 1) == 0) {
            userId = ActivityManager.handleIncomingUser(Binder.getCallingPid(), Binder.getCallingUid(), userId, false, true, "clearWallpaper", null);
            WallpaperData data = null;
            synchronized (this.mLock) {
                clearWallpaperLocked(false, which, userId, null);
                if (which == 2) {
                    data = (WallpaperData) this.mLockWallpaperMap.get(userId);
                }
                if (which == 1 || r8 == null) {
                    data = (WallpaperData) this.mWallpaperMap.get(userId);
                }
            }
            if (data != null) {
                notifyWallpaperColorsChanged(data, which);
            }
        }
    }

    void clearWallpaperLocked(boolean defaultFailed, int which, int userId, IRemoteCallback reply) {
        if (which == 1 || which == 2) {
            WallpaperData wallpaper;
            if (which == 2) {
                wallpaper = (WallpaperData) this.mLockWallpaperMap.get(userId);
                if (wallpaper == null) {
                    return;
                }
            }
            wallpaper = (WallpaperData) this.mWallpaperMap.get(userId);
            if (wallpaper == null) {
                loadSettingsLocked(userId, false);
                wallpaper = (WallpaperData) this.mWallpaperMap.get(userId);
            }
            if (wallpaper != null) {
                long ident = Binder.clearCallingIdentity();
                try {
                    if (wallpaper.wallpaperFile.exists()) {
                        wallpaper.wallpaperFile.delete();
                        wallpaper.cropFile.delete();
                        if (which == 2) {
                            this.mLockWallpaperMap.remove(userId);
                            IWallpaperManagerCallback cb = this.mKeyguardListener;
                            if (cb != null) {
                                try {
                                    cb.onWallpaperChanged();
                                } catch (RemoteException e) {
                                }
                            }
                            saveSettingsLocked(userId);
                            return;
                        }
                    }
                    Throwable e2 = null;
                    try {
                        wallpaper.primaryColors = null;
                        wallpaper.imageWallpaperPending = false;
                        if (userId != this.mCurrentUserId) {
                            Binder.restoreCallingIdentity(ident);
                            return;
                        }
                        ComponentName componentName;
                        if (defaultFailed) {
                            componentName = this.mImageWallpaper;
                        } else {
                            componentName = null;
                        }
                        if (bindWallpaperComponentLocked(componentName, true, false, wallpaper, reply)) {
                            Binder.restoreCallingIdentity(ident);
                            return;
                        }
                        Slog.e(TAG, "Default wallpaper component not found!", e2);
                        clearWallpaperComponentLocked(wallpaper);
                        if (reply != null) {
                            try {
                                reply.sendResult(null);
                            } catch (RemoteException e3) {
                            }
                        }
                        Binder.restoreCallingIdentity(ident);
                        return;
                    } catch (Throwable e1) {
                        e2 = e1;
                    }
                } finally {
                    Binder.restoreCallingIdentity(ident);
                }
            } else {
                return;
            }
        }
        throw new IllegalArgumentException("Must specify exactly one kind of wallpaper to read");
    }

    public boolean hasNamedWallpaper(String name) {
        synchronized (this.mLock) {
            long ident = Binder.clearCallingIdentity();
            try {
                List<UserInfo> users = ((UserManager) this.mContext.getSystemService("user")).getUsers();
                Binder.restoreCallingIdentity(ident);
                for (UserInfo user : users) {
                    if (!user.isManagedProfile()) {
                        WallpaperData wd = (WallpaperData) this.mWallpaperMap.get(user.id);
                        if (wd == null) {
                            loadSettingsLocked(user.id, false);
                            wd = (WallpaperData) this.mWallpaperMap.get(user.id);
                        }
                        if (wd != null && name.equals(wd.name)) {
                            return true;
                        }
                    }
                }
                return false;
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(ident);
            }
        }
    }

    private Point getDefaultDisplaySize() {
        Point p = new Point();
        ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getRealSize(p);
        return p;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setDimensionHints(int r8, int r9, java.lang.String r10) throws android.os.RemoteException {
        /*
        r7 = this;
        r4 = "android.permission.SET_WALLPAPER_HINTS";
        r7.checkPermission(r4);
        r4 = r7.isWallpaperSupported(r10);
        if (r4 != 0) goto L_0x000d;
    L_0x000c:
        return;
    L_0x000d:
        r5 = r7.mLock;
        monitor-enter(r5);
        r2 = android.os.UserHandle.getCallingUserId();	 Catch:{ all -> 0x0026 }
        r4 = 1;
        r3 = r7.getWallpaperSafeLocked(r2, r4);	 Catch:{ all -> 0x0026 }
        if (r8 <= 0) goto L_0x001d;
    L_0x001b:
        if (r9 > 0) goto L_0x0029;
    L_0x001d:
        r4 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x0026 }
        r6 = "width and height must be > 0";
        r4.<init>(r6);	 Catch:{ all -> 0x0026 }
        throw r4;	 Catch:{ all -> 0x0026 }
    L_0x0026:
        r4 = move-exception;
        monitor-exit(r5);
        throw r4;
    L_0x0029:
        r0 = r7.getDefaultDisplaySize();	 Catch:{ all -> 0x0026 }
        r4 = r0.x;	 Catch:{ all -> 0x0026 }
        r8 = java.lang.Math.max(r8, r4);	 Catch:{ all -> 0x0026 }
        r4 = r0.y;	 Catch:{ all -> 0x0026 }
        r9 = java.lang.Math.max(r9, r4);	 Catch:{ all -> 0x0026 }
        r4 = r3.width;	 Catch:{ all -> 0x0026 }
        if (r8 != r4) goto L_0x0041;
    L_0x003d:
        r4 = r3.height;	 Catch:{ all -> 0x0026 }
        if (r9 == r4) goto L_0x0062;
    L_0x0041:
        r3.width = r8;	 Catch:{ all -> 0x0026 }
        r3.height = r9;	 Catch:{ all -> 0x0026 }
        r7.saveSettingsLocked(r2);	 Catch:{ all -> 0x0026 }
        r4 = r7.mCurrentUserId;	 Catch:{ all -> 0x0026 }
        if (r4 == r2) goto L_0x004e;
    L_0x004c:
        monitor-exit(r5);
        return;
    L_0x004e:
        r4 = r3.connection;	 Catch:{ all -> 0x0026 }
        if (r4 == 0) goto L_0x0062;
    L_0x0052:
        r4 = r3.connection;	 Catch:{ all -> 0x0026 }
        r4 = r4.mEngine;	 Catch:{ all -> 0x0026 }
        if (r4 == 0) goto L_0x0064;
    L_0x0058:
        r4 = r3.connection;	 Catch:{ RemoteException -> 0x0070 }
        r4 = r4.mEngine;	 Catch:{ RemoteException -> 0x0070 }
        r4.setDesiredSize(r8, r9);	 Catch:{ RemoteException -> 0x0070 }
    L_0x005f:
        r7.notifyCallbacksLocked(r3);	 Catch:{ all -> 0x0026 }
    L_0x0062:
        monitor-exit(r5);
        return;
    L_0x0064:
        r4 = r3.connection;	 Catch:{ all -> 0x0026 }
        r4 = r4.mService;	 Catch:{ all -> 0x0026 }
        if (r4 == 0) goto L_0x0062;
    L_0x006a:
        r4 = r3.connection;	 Catch:{ all -> 0x0026 }
        r6 = 1;
        r4.mDimensionsChanged = r6;	 Catch:{ all -> 0x0026 }
        goto L_0x0062;
    L_0x0070:
        r1 = move-exception;
        goto L_0x005f;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.setDimensionHints(int, int, java.lang.String):void");
    }

    public int getWidthHint() throws RemoteException {
        synchronized (this.mLock) {
            WallpaperData wallpaper = (WallpaperData) this.mWallpaperMap.get(UserHandle.getCallingUserId());
            if (wallpaper != null) {
                int i = wallpaper.width;
                return i;
            }
            return 0;
        }
    }

    public int getHeightHint() throws RemoteException {
        synchronized (this.mLock) {
            WallpaperData wallpaper = (WallpaperData) this.mWallpaperMap.get(UserHandle.getCallingUserId());
            if (wallpaper != null) {
                int i = wallpaper.height;
                return i;
            }
            return 0;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setDisplayPadding(android.graphics.Rect r8, java.lang.String r9) {
        /*
        r7 = this;
        r3 = "android.permission.SET_WALLPAPER_HINTS";
        r7.checkPermission(r3);
        r3 = r7.isWallpaperSupported(r9);
        if (r3 != 0) goto L_0x000d;
    L_0x000c:
        return;
    L_0x000d:
        r4 = r7.mLock;
        monitor-enter(r4);
        r1 = android.os.UserHandle.getCallingUserId();	 Catch:{ all -> 0x003b }
        r3 = 1;
        r2 = r7.getWallpaperSafeLocked(r1, r3);	 Catch:{ all -> 0x003b }
        r3 = r8.left;	 Catch:{ all -> 0x003b }
        if (r3 < 0) goto L_0x0021;
    L_0x001d:
        r3 = r8.top;	 Catch:{ all -> 0x003b }
        if (r3 >= 0) goto L_0x003e;
    L_0x0021:
        r3 = new java.lang.IllegalArgumentException;	 Catch:{ all -> 0x003b }
        r5 = new java.lang.StringBuilder;	 Catch:{ all -> 0x003b }
        r5.<init>();	 Catch:{ all -> 0x003b }
        r6 = "padding must be positive: ";
        r5 = r5.append(r6);	 Catch:{ all -> 0x003b }
        r5 = r5.append(r8);	 Catch:{ all -> 0x003b }
        r5 = r5.toString();	 Catch:{ all -> 0x003b }
        r3.<init>(r5);	 Catch:{ all -> 0x003b }
        throw r3;	 Catch:{ all -> 0x003b }
    L_0x003b:
        r3 = move-exception;
        monitor-exit(r4);
        throw r3;
    L_0x003e:
        r3 = r8.right;	 Catch:{ all -> 0x003b }
        if (r3 < 0) goto L_0x0021;
    L_0x0042:
        r3 = r8.bottom;	 Catch:{ all -> 0x003b }
        if (r3 < 0) goto L_0x0021;
    L_0x0046:
        r3 = r2.padding;	 Catch:{ all -> 0x003b }
        r3 = r8.equals(r3);	 Catch:{ all -> 0x003b }
        if (r3 != 0) goto L_0x0070;
    L_0x004e:
        r3 = r2.padding;	 Catch:{ all -> 0x003b }
        r3.set(r8);	 Catch:{ all -> 0x003b }
        r7.saveSettingsLocked(r1);	 Catch:{ all -> 0x003b }
        r3 = r7.mCurrentUserId;	 Catch:{ all -> 0x003b }
        if (r3 == r1) goto L_0x005c;
    L_0x005a:
        monitor-exit(r4);
        return;
    L_0x005c:
        r3 = r2.connection;	 Catch:{ all -> 0x003b }
        if (r3 == 0) goto L_0x0070;
    L_0x0060:
        r3 = r2.connection;	 Catch:{ all -> 0x003b }
        r3 = r3.mEngine;	 Catch:{ all -> 0x003b }
        if (r3 == 0) goto L_0x0072;
    L_0x0066:
        r3 = r2.connection;	 Catch:{ RemoteException -> 0x007e }
        r3 = r3.mEngine;	 Catch:{ RemoteException -> 0x007e }
        r3.setDisplayPadding(r8);	 Catch:{ RemoteException -> 0x007e }
    L_0x006d:
        r7.notifyCallbacksLocked(r2);	 Catch:{ all -> 0x003b }
    L_0x0070:
        monitor-exit(r4);
        return;
    L_0x0072:
        r3 = r2.connection;	 Catch:{ all -> 0x003b }
        r3 = r3.mService;	 Catch:{ all -> 0x003b }
        if (r3 == 0) goto L_0x0070;
    L_0x0078:
        r3 = r2.connection;	 Catch:{ all -> 0x003b }
        r5 = 1;
        r3.mPaddingChanged = r5;	 Catch:{ all -> 0x003b }
        goto L_0x0070;
    L_0x007e:
        r0 = move-exception;
        goto L_0x006d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.setDisplayPadding(android.graphics.Rect, java.lang.String):void");
    }

    private void enforceCallingOrSelfPermissionAndAppOp(String permission, String callingPkg, int callingUid, String message) {
        this.mContext.enforceCallingOrSelfPermission(permission, message);
        String opName = AppOpsManager.permissionToOp(permission);
        if (opName != null && this.mAppOpsManager.noteOp(opName, callingUid, callingPkg) != 0) {
            throw new SecurityException(message + ": " + callingPkg + " is not allowed to " + permission);
        }
    }

    public ParcelFileDescriptor getWallpaper(String callingPkg, IWallpaperManagerCallback cb, int which, Bundle outParams, int wallpaperUserId) {
        if (this.mContext.checkCallingOrSelfPermission("android.permission.READ_WALLPAPER_INTERNAL") != 0) {
            enforceCallingOrSelfPermissionAndAppOp("android.permission.READ_EXTERNAL_STORAGE", callingPkg, Binder.getCallingUid(), "read wallpaper");
        }
        wallpaperUserId = ActivityManager.handleIncomingUser(Binder.getCallingPid(), Binder.getCallingUid(), wallpaperUserId, false, true, "getWallpaper", null);
        if (which == 1 || which == 2) {
            synchronized (this.mLock) {
                WallpaperData wallpaper = (WallpaperData) (which == 2 ? this.mLockWallpaperMap : this.mWallpaperMap).get(wallpaperUserId);
                if (wallpaper == null) {
                    return null;
                }
                if (outParams != null) {
                    try {
                        outParams.putInt("width", wallpaper.width);
                        outParams.putInt("height", wallpaper.height);
                    } catch (FileNotFoundException e) {
                        Slog.w(TAG, "Error getting wallpaper", e);
                        return null;
                    }
                }
                if (cb != null) {
                    wallpaper.callbacks.register(cb);
                }
                if (wallpaper.cropFile.exists()) {
                    ParcelFileDescriptor open = ParcelFileDescriptor.open(wallpaper.cropFile, 268435456);
                    return open;
                }
                return null;
            }
        }
        throw new IllegalArgumentException("Must specify exactly one kind of wallpaper to read");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.WallpaperInfo getWallpaperInfo(int r9) {
        /*
        r8 = this;
        r6 = 0;
        r0 = android.os.Binder.getCallingPid();
        r1 = android.os.Binder.getCallingUid();
        r5 = "getWallpaperInfo";
        r3 = 0;
        r4 = 1;
        r2 = r9;
        r9 = android.app.ActivityManager.handleIncomingUser(r0, r1, r2, r3, r4, r5, r6);
        r1 = r8.mLock;
        monitor-enter(r1);
        r0 = r8.mWallpaperMap;	 Catch:{ all -> 0x002c }
        r7 = r0.get(r9);	 Catch:{ all -> 0x002c }
        r7 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r7;	 Catch:{ all -> 0x002c }
        if (r7 == 0) goto L_0x002a;
    L_0x0020:
        r0 = r7.connection;	 Catch:{ all -> 0x002c }
        if (r0 == 0) goto L_0x002a;
    L_0x0024:
        r0 = r7.connection;	 Catch:{ all -> 0x002c }
        r0 = r0.mInfo;	 Catch:{ all -> 0x002c }
        monitor-exit(r1);
        return r0;
    L_0x002a:
        monitor-exit(r1);
        return r6;
    L_0x002c:
        r0 = move-exception;
        monitor-exit(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.getWallpaperInfo(int):android.app.WallpaperInfo");
    }

    public int getWallpaperIdForUser(int which, int userId) {
        userId = ActivityManager.handleIncomingUser(Binder.getCallingPid(), Binder.getCallingUid(), userId, false, true, "getWallpaperIdForUser", null);
        if (which == 1 || which == 2) {
            SparseArray<WallpaperData> map = which == 2 ? this.mLockWallpaperMap : this.mWallpaperMap;
            synchronized (this.mLock) {
                WallpaperData wallpaper = (WallpaperData) map.get(userId);
                if (wallpaper != null) {
                    int i = wallpaper.wallpaperId;
                    return i;
                }
                return -1;
            }
        }
        throw new IllegalArgumentException("Must specify exactly one kind of wallpaper");
    }

    public void registerWallpaperColorsCallback(IWallpaperManagerCallback cb, int userId) {
        userId = ActivityManager.handleIncomingUser(Binder.getCallingPid(), Binder.getCallingUid(), userId, true, true, "registerWallpaperColorsCallback", null);
        synchronized (this.mLock) {
            RemoteCallbackList<IWallpaperManagerCallback> userColorsChangedListeners = (RemoteCallbackList) this.mColorsChangedListeners.get(userId);
            if (userColorsChangedListeners == null) {
                userColorsChangedListeners = new RemoteCallbackList();
                this.mColorsChangedListeners.put(userId, userColorsChangedListeners);
            }
            userColorsChangedListeners.register(cb);
        }
    }

    public void unregisterWallpaperColorsCallback(IWallpaperManagerCallback cb, int userId) {
        userId = ActivityManager.handleIncomingUser(Binder.getCallingPid(), Binder.getCallingUid(), userId, true, true, "unregisterWallpaperColorsCallback", null);
        synchronized (this.mLock) {
            RemoteCallbackList<IWallpaperManagerCallback> userColorsChangedListeners = (RemoteCallbackList) this.mColorsChangedListeners.get(userId);
            if (userColorsChangedListeners != null) {
                userColorsChangedListeners.unregister(cb);
            }
        }
    }

    public boolean setLockWallpaperCallback(IWallpaperManagerCallback cb) {
        checkPermission("android.permission.INTERNAL_SYSTEM_WINDOW");
        synchronized (this.mLock) {
            this.mKeyguardListener = cb;
        }
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.app.WallpaperColors getWallpaperColors(int r12, int r13) throws android.os.RemoteException {
        /*
        r11 = this;
        r10 = 2;
        r5 = 1;
        r7 = 0;
        if (r12 == r10) goto L_0x0010;
    L_0x0005:
        if (r12 == r5) goto L_0x0010;
    L_0x0007:
        r1 = new java.lang.IllegalArgumentException;
        r2 = "which should be either FLAG_LOCK or FLAG_SYSTEM";
        r1.<init>(r2);
        throw r1;
    L_0x0010:
        r1 = android.os.Binder.getCallingPid();
        r2 = android.os.Binder.getCallingUid();
        r6 = "getWallpaperColors";
        r4 = 0;
        r3 = r13;
        r13 = android.app.ActivityManager.handleIncomingUser(r1, r2, r3, r4, r5, r6, r7);
        r9 = 0;
        r2 = r11.mLock;
        monitor-enter(r2);
        if (r12 != r10) goto L_0x0031;
    L_0x0027:
        r1 = r11.mLockWallpaperMap;	 Catch:{ all -> 0x0055 }
        r1 = r1.get(r13);	 Catch:{ all -> 0x0055 }
        r0 = r1;
        r0 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r0;	 Catch:{ all -> 0x0055 }
        r9 = r0;
    L_0x0031:
        if (r9 != 0) goto L_0x003d;
    L_0x0033:
        r1 = r11.mWallpaperMap;	 Catch:{ all -> 0x0055 }
        r1 = r1.get(r13);	 Catch:{ all -> 0x0055 }
        r0 = r1;
        r0 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r0;	 Catch:{ all -> 0x0055 }
        r9 = r0;
    L_0x003d:
        if (r9 != 0) goto L_0x0041;
    L_0x003f:
        monitor-exit(r2);
        return r7;
    L_0x0041:
        r1 = r9.primaryColors;	 Catch:{ all -> 0x0055 }
        if (r1 != 0) goto L_0x0053;
    L_0x0045:
        r8 = 1;
    L_0x0046:
        monitor-exit(r2);
        if (r8 == 0) goto L_0x004c;
    L_0x0049:
        r11.extractColors(r9);
    L_0x004c:
        r1 = r11.mLock;
        monitor-enter(r1);
        r2 = r9.primaryColors;	 Catch:{ all -> 0x0058 }
        monitor-exit(r1);
        return r2;
    L_0x0053:
        r8 = 0;
        goto L_0x0046;
    L_0x0055:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
    L_0x0058:
        r2 = move-exception;
        monitor-exit(r1);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.getWallpaperColors(int, int):android.app.WallpaperColors");
    }

    public android.os.ParcelFileDescriptor setWallpaper(java.lang.String r15, java.lang.String r16, android.graphics.Rect r17, boolean r18, android.os.Bundle r19, int r20, android.app.IWallpaperManagerCallback r21, int r22) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r17_1 'cropHint' android.graphics.Rect) in PHI: PHI: (r17_2 'cropHint' android.graphics.Rect) = (r17_1 'cropHint' android.graphics.Rect), (r17_0 'cropHint' android.graphics.Rect) binds: {(r17_1 'cropHint' android.graphics.Rect)=B:11:0x0049, (r17_0 'cropHint' android.graphics.Rect)=B:37:0x00c6}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:79)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r14 = this;
        r2 = getCallingPid();
        r3 = getCallingUid();
        r7 = "changing wallpaper";
        r5 = 0;
        r6 = 1;
        r8 = 0;
        r4 = r22;
        r22 = android.app.ActivityManager.handleIncomingUser(r2, r3, r4, r5, r6, r7, r8);
        r2 = "android.permission.SET_WALLPAPER";
        r14.checkPermission(r2);
        r2 = r20 & 3;
        if (r2 != 0) goto L_0x0033;
    L_0x001e:
        r9 = "Must specify a valid wallpaper category to set";
        r2 = "WallpaperManagerService";
        r3 = "Must specify a valid wallpaper category to set";
        android.util.Slog.e(r2, r3);
        r2 = new java.lang.IllegalArgumentException;
        r3 = "Must specify a valid wallpaper category to set";
        r2.<init>(r3);
        throw r2;
    L_0x0033:
        r0 = r16;
        r2 = r14.isWallpaperSupported(r0);
        if (r2 == 0) goto L_0x0045;
    L_0x003b:
        r0 = r16;
        r2 = r14.isSetWallpaperAllowed(r0);
        r2 = r2 ^ 1;
        if (r2 == 0) goto L_0x0047;
    L_0x0045:
        r2 = 0;
        return r2;
    L_0x0047:
        if (r17 != 0) goto L_0x009a;
    L_0x0049:
        r17 = new android.graphics.Rect;
        r2 = 0;
        r3 = 0;
        r4 = 0;
        r5 = 0;
        r0 = r17;
        r0.<init>(r2, r3, r4, r5);
    L_0x0054:
        r3 = r14.mLock;
        monitor-enter(r3);
        r2 = 1;
        r0 = r20;
        if (r0 != r2) goto L_0x006b;
    L_0x005c:
        r2 = r14.mLockWallpaperMap;	 Catch:{ all -> 0x00ce }
        r0 = r22;	 Catch:{ all -> 0x00ce }
        r2 = r2.get(r0);	 Catch:{ all -> 0x00ce }
        if (r2 != 0) goto L_0x006b;	 Catch:{ all -> 0x00ce }
    L_0x0066:
        r0 = r22;	 Catch:{ all -> 0x00ce }
        r14.migrateSystemToLockWallpaperLocked(r0);	 Catch:{ all -> 0x00ce }
    L_0x006b:
        r0 = r22;	 Catch:{ all -> 0x00ce }
        r1 = r20;	 Catch:{ all -> 0x00ce }
        r13 = r14.getWallpaperSafeLocked(r0, r1);	 Catch:{ all -> 0x00ce }
        r10 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x00ce }
        r0 = r19;	 Catch:{ all -> 0x00c9 }
        r12 = r14.updateWallpaperBitmapLocked(r15, r13, r0);	 Catch:{ all -> 0x00c9 }
        if (r12 == 0) goto L_0x0095;	 Catch:{ all -> 0x00c9 }
    L_0x007f:
        r2 = 1;	 Catch:{ all -> 0x00c9 }
        r13.imageWallpaperPending = r2;	 Catch:{ all -> 0x00c9 }
        r0 = r20;	 Catch:{ all -> 0x00c9 }
        r13.whichPending = r0;	 Catch:{ all -> 0x00c9 }
        r0 = r21;	 Catch:{ all -> 0x00c9 }
        r13.setComplete = r0;	 Catch:{ all -> 0x00c9 }
        r2 = r13.cropHint;	 Catch:{ all -> 0x00c9 }
        r0 = r17;	 Catch:{ all -> 0x00c9 }
        r2.set(r0);	 Catch:{ all -> 0x00c9 }
        r0 = r18;	 Catch:{ all -> 0x00c9 }
        r13.allowBackup = r0;	 Catch:{ all -> 0x00c9 }
    L_0x0095:
        android.os.Binder.restoreCallingIdentity(r10);	 Catch:{ all -> 0x00ce }
        monitor-exit(r3);
        return r12;
    L_0x009a:
        r2 = r17.isEmpty();
        if (r2 != 0) goto L_0x00a6;
    L_0x00a0:
        r0 = r17;
        r2 = r0.left;
        if (r2 >= 0) goto L_0x00c2;
    L_0x00a6:
        r2 = new java.lang.IllegalArgumentException;
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "Invalid crop rect supplied: ";
        r3 = r3.append(r4);
        r0 = r17;
        r3 = r3.append(r0);
        r3 = r3.toString();
        r2.<init>(r3);
        throw r2;
    L_0x00c2:
        r0 = r17;
        r2 = r0.top;
        if (r2 >= 0) goto L_0x0054;
    L_0x00c8:
        goto L_0x00a6;
    L_0x00c9:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r10);	 Catch:{ all -> 0x00ce }
        throw r2;	 Catch:{ all -> 0x00ce }
    L_0x00ce:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.setWallpaper(java.lang.String, java.lang.String, android.graphics.Rect, boolean, android.os.Bundle, int, android.app.IWallpaperManagerCallback, int):android.os.ParcelFileDescriptor");
    }

    private void migrateSystemToLockWallpaperLocked(int userId) {
        WallpaperData sysWP = (WallpaperData) this.mWallpaperMap.get(userId);
        if (sysWP != null) {
            WallpaperData lockWP = new WallpaperData(userId, WALLPAPER_LOCK_ORIG, WALLPAPER_LOCK_CROP);
            lockWP.wallpaperId = sysWP.wallpaperId;
            lockWP.cropHint.set(sysWP.cropHint);
            lockWP.width = sysWP.width;
            lockWP.height = sysWP.height;
            lockWP.allowBackup = sysWP.allowBackup;
            lockWP.primaryColors = sysWP.primaryColors;
            try {
                Os.rename(sysWP.wallpaperFile.getAbsolutePath(), lockWP.wallpaperFile.getAbsolutePath());
                Os.rename(sysWP.cropFile.getAbsolutePath(), lockWP.cropFile.getAbsolutePath());
                this.mLockWallpaperMap.put(userId, lockWP);
            } catch (ErrnoException e) {
                Slog.e(TAG, "Can't migrate system wallpaper: " + e.getMessage());
                lockWP.wallpaperFile.delete();
                lockWP.cropFile.delete();
            }
        }
    }

    ParcelFileDescriptor updateWallpaperBitmapLocked(String name, WallpaperData wallpaper, Bundle extras) {
        if (name == null) {
            name = "";
        }
        try {
            File dir = getWallpaperDir(wallpaper.userId);
            if (!dir.exists()) {
                dir.mkdir();
                FileUtils.setPermissions(dir.getPath(), 505, -1, -1);
            }
            ParcelFileDescriptor fd = ParcelFileDescriptor.open(wallpaper.wallpaperFile, 1006632960);
            if (!SELinux.restorecon(wallpaper.wallpaperFile)) {
                return null;
            }
            wallpaper.name = name;
            wallpaper.wallpaperId = makeWallpaperIdLocked();
            if (extras != null) {
                extras.putInt("android.service.wallpaper.extra.ID", wallpaper.wallpaperId);
            }
            wallpaper.primaryColors = null;
            return fd;
        } catch (FileNotFoundException e) {
            Slog.w(TAG, "Error setting wallpaper", e);
            return null;
        }
    }

    public void setWallpaperComponentChecked(ComponentName name, String callingPackage, int userId) {
        if (isWallpaperSupported(callingPackage) && isSetWallpaperAllowed(callingPackage)) {
            setWallpaperComponent(name, userId);
        }
    }

    public void setWallpaperComponent(ComponentName name) {
        setWallpaperComponent(name, UserHandle.getCallingUserId());
    }

    private void setWallpaperComponent(ComponentName name, int userId) {
        userId = ActivityManager.handleIncomingUser(getCallingPid(), getCallingUid(), userId, false, true, "changing live wallpaper", null);
        checkPermission("android.permission.SET_WALLPAPER_COMPONENT");
        int which = 1;
        boolean shouldNotifyColors = false;
        synchronized (this.mLock) {
            WallpaperData wallpaper = (WallpaperData) this.mWallpaperMap.get(userId);
            if (wallpaper == null) {
                throw new IllegalStateException("Wallpaper not yet initialized for user " + userId);
            }
            long ident = Binder.clearCallingIdentity();
            if (this.mImageWallpaper.equals(wallpaper.wallpaperComponent) && this.mLockWallpaperMap.get(userId) == null) {
                migrateSystemToLockWallpaperLocked(userId);
            }
            if (this.mLockWallpaperMap.get(userId) == null) {
                which = 3;
            }
            try {
                wallpaper.imageWallpaperPending = false;
                boolean same = changingToSame(name, wallpaper);
                if (bindWallpaperComponentLocked(name, false, true, wallpaper, null)) {
                    if (!same) {
                        wallpaper.primaryColors = null;
                    }
                    wallpaper.wallpaperId = makeWallpaperIdLocked();
                    notifyCallbacksLocked(wallpaper);
                    shouldNotifyColors = true;
                }
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }
        if (shouldNotifyColors) {
            notifyWallpaperColorsChanged(wallpaper, which);
        }
    }

    private boolean changingToSame(ComponentName componentName, WallpaperData wallpaper) {
        if (wallpaper.connection != null) {
            if (wallpaper.wallpaperComponent == null) {
                if (componentName == null) {
                    return true;
                }
            } else if (wallpaper.wallpaperComponent.equals(componentName)) {
                return true;
            }
        }
        return false;
    }

    boolean bindWallpaperComponentLocked(android.content.ComponentName r23, boolean r24, boolean r25, com.android.server.wallpaper.WallpaperManagerService.WallpaperData r26, android.os.IRemoteCallback r27) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r20_1 android.app.WallpaperInfo) in PHI: PHI: (r20_2 android.app.WallpaperInfo) = (r20_1 android.app.WallpaperInfo), (r20_0 android.app.WallpaperInfo) binds: {(r20_1 android.app.WallpaperInfo)=B:42:?, (r20_0 android.app.WallpaperInfo)=B:88:0x0155}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:79)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r22 = this;
        r3 = "WallpaperManagerService";
        r4 = new java.lang.StringBuilder;
        r4.<init>();
        r5 = "bindWallpaperComponentLocked: componentName=";
        r4 = r4.append(r5);
        r0 = r23;
        r4 = r4.append(r0);
        r4 = r4.toString();
        android.util.Slog.v(r3, r4);
        if (r24 != 0) goto L_0x002c;
    L_0x001e:
        r0 = r22;
        r1 = r23;
        r2 = r26;
        r3 = r0.changingToSame(r1, r2);
        if (r3 == 0) goto L_0x002c;
    L_0x002a:
        r3 = 1;
        return r3;
    L_0x002c:
        if (r23 != 0) goto L_0x0045;
    L_0x002e:
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r0.mDefaultWallpaperComponent;	 Catch:{ RemoteException -> 0x00ab }
        r23 = r0;	 Catch:{ RemoteException -> 0x00ab }
        if (r23 != 0) goto L_0x0045;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0036:
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r0.mImageWallpaper;	 Catch:{ RemoteException -> 0x00ab }
        r23 = r0;	 Catch:{ RemoteException -> 0x00ab }
        r3 = "WallpaperManagerService";	 Catch:{ RemoteException -> 0x00ab }
        r4 = "No default component; using image wallpaper";	 Catch:{ RemoteException -> 0x00ab }
        android.util.Slog.v(r3, r4);	 Catch:{ RemoteException -> 0x00ab }
    L_0x0045:
        r0 = r26;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r0.userId;	 Catch:{ RemoteException -> 0x00ab }
        r18 = r0;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.mIPackageManager;	 Catch:{ RemoteException -> 0x00ab }
        r4 = 4224; // 0x1080 float:5.919E-42 double:2.087E-320;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r23;	 Catch:{ RemoteException -> 0x00ab }
        r1 = r18;	 Catch:{ RemoteException -> 0x00ab }
        r19 = r3.getServiceInfo(r0, r4, r1);	 Catch:{ RemoteException -> 0x00ab }
        if (r19 != 0) goto L_0x0080;	 Catch:{ RemoteException -> 0x00ab }
    L_0x005b:
        r3 = "WallpaperManagerService";	 Catch:{ RemoteException -> 0x00ab }
        r4 = new java.lang.StringBuilder;	 Catch:{ RemoteException -> 0x00ab }
        r4.<init>();	 Catch:{ RemoteException -> 0x00ab }
        r5 = "Attempted wallpaper ";	 Catch:{ RemoteException -> 0x00ab }
        r4 = r4.append(r5);	 Catch:{ RemoteException -> 0x00ab }
        r0 = r23;	 Catch:{ RemoteException -> 0x00ab }
        r4 = r4.append(r0);	 Catch:{ RemoteException -> 0x00ab }
        r5 = " is unavailable";	 Catch:{ RemoteException -> 0x00ab }
        r4 = r4.append(r5);	 Catch:{ RemoteException -> 0x00ab }
        r4 = r4.toString();	 Catch:{ RemoteException -> 0x00ab }
        android.util.Slog.w(r3, r4);	 Catch:{ RemoteException -> 0x00ab }
        r3 = 0;	 Catch:{ RemoteException -> 0x00ab }
        return r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0080:
        r3 = "android.permission.BIND_WALLPAPER";	 Catch:{ RemoteException -> 0x00ab }
        r0 = r19;	 Catch:{ RemoteException -> 0x00ab }
        r4 = r0.permission;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.equals(r4);	 Catch:{ RemoteException -> 0x00ab }
        if (r3 != 0) goto L_0x00dd;	 Catch:{ RemoteException -> 0x00ab }
    L_0x008d:
        r3 = new java.lang.StringBuilder;	 Catch:{ RemoteException -> 0x00ab }
        r3.<init>();	 Catch:{ RemoteException -> 0x00ab }
        r4 = "Selected service does not require android.permission.BIND_WALLPAPER: ";	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.append(r4);	 Catch:{ RemoteException -> 0x00ab }
        r0 = r23;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.append(r0);	 Catch:{ RemoteException -> 0x00ab }
        r14 = r3.toString();	 Catch:{ RemoteException -> 0x00ab }
        if (r25 == 0) goto L_0x00d5;	 Catch:{ RemoteException -> 0x00ab }
    L_0x00a5:
        r3 = new java.lang.SecurityException;	 Catch:{ RemoteException -> 0x00ab }
        r3.<init>(r14);	 Catch:{ RemoteException -> 0x00ab }
        throw r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x00ab:
        r9 = move-exception;
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "Remote exception for ";
        r3 = r3.append(r4);
        r0 = r23;
        r3 = r3.append(r0);
        r4 = "\n";
        r3 = r3.append(r4);
        r3 = r3.append(r9);
        r14 = r3.toString();
        if (r25 == 0) goto L_0x026c;
    L_0x00cf:
        r3 = new java.lang.IllegalArgumentException;
        r3.<init>(r14);
        throw r3;
    L_0x00d5:
        r3 = "WallpaperManagerService";	 Catch:{ RemoteException -> 0x00ab }
        android.util.Slog.w(r3, r14);	 Catch:{ RemoteException -> 0x00ab }
        r3 = 0;	 Catch:{ RemoteException -> 0x00ab }
        return r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x00dd:
        r20 = 0;	 Catch:{ RemoteException -> 0x00ab }
        r13 = new android.content.Intent;	 Catch:{ RemoteException -> 0x00ab }
        r3 = "android.service.wallpaper.WallpaperService";	 Catch:{ RemoteException -> 0x00ab }
        r13.<init>(r3);	 Catch:{ RemoteException -> 0x00ab }
        if (r23 == 0) goto L_0x01a3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x00e9:
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.mImageWallpaper;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r23;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.equals(r3);	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3 ^ 1;	 Catch:{ RemoteException -> 0x00ab }
        if (r3 == 0) goto L_0x01a3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x00f7:
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.mIPackageManager;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r4 = r0.mContext;	 Catch:{ RemoteException -> 0x00ab }
        r4 = r4.getContentResolver();	 Catch:{ RemoteException -> 0x00ab }
        r4 = r13.resolveTypeIfNeeded(r4);	 Catch:{ RemoteException -> 0x00ab }
        r5 = 128; // 0x80 float:1.794E-43 double:6.32E-322;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r18;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.queryIntentServices(r13, r4, r5, r0);	 Catch:{ RemoteException -> 0x00ab }
        r16 = r3.getList();	 Catch:{ RemoteException -> 0x00ab }
        r12 = 0;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0114:
        r3 = r16.size();	 Catch:{ RemoteException -> 0x00ab }
        if (r12 >= r3) goto L_0x0155;	 Catch:{ RemoteException -> 0x00ab }
    L_0x011a:
        r0 = r16;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.get(r12);	 Catch:{ RemoteException -> 0x00ab }
        r3 = (android.content.pm.ResolveInfo) r3;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r3.serviceInfo;	 Catch:{ RemoteException -> 0x00ab }
        r17 = r0;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r17;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.name;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r19;	 Catch:{ RemoteException -> 0x00ab }
        r4 = r0.name;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.equals(r4);	 Catch:{ RemoteException -> 0x00ab }
        if (r3 == 0) goto L_0x0197;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0134:
        r0 = r17;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.packageName;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r19;	 Catch:{ RemoteException -> 0x00ab }
        r4 = r0.packageName;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.equals(r4);	 Catch:{ RemoteException -> 0x00ab }
        if (r3 == 0) goto L_0x0197;
    L_0x0142:
        r20 = new android.app.WallpaperInfo;	 Catch:{ XmlPullParserException -> 0x0186, IOException -> 0x0175 }
        r0 = r22;	 Catch:{ XmlPullParserException -> 0x0186, IOException -> 0x0175 }
        r4 = r0.mContext;	 Catch:{ XmlPullParserException -> 0x0186, IOException -> 0x0175 }
        r0 = r16;	 Catch:{ XmlPullParserException -> 0x0186, IOException -> 0x0175 }
        r3 = r0.get(r12);	 Catch:{ XmlPullParserException -> 0x0186, IOException -> 0x0175 }
        r3 = (android.content.pm.ResolveInfo) r3;	 Catch:{ XmlPullParserException -> 0x0186, IOException -> 0x0175 }
        r0 = r20;	 Catch:{ XmlPullParserException -> 0x0186, IOException -> 0x0175 }
        r0.<init>(r4, r3);	 Catch:{ XmlPullParserException -> 0x0186, IOException -> 0x0175 }
    L_0x0155:
        if (r20 != 0) goto L_0x01a3;
    L_0x0157:
        r3 = new java.lang.StringBuilder;	 Catch:{ RemoteException -> 0x00ab }
        r3.<init>();	 Catch:{ RemoteException -> 0x00ab }
        r4 = "Selected service is not a wallpaper: ";	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.append(r4);	 Catch:{ RemoteException -> 0x00ab }
        r0 = r23;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.append(r0);	 Catch:{ RemoteException -> 0x00ab }
        r14 = r3.toString();	 Catch:{ RemoteException -> 0x00ab }
        if (r25 == 0) goto L_0x019b;	 Catch:{ RemoteException -> 0x00ab }
    L_0x016f:
        r3 = new java.lang.SecurityException;	 Catch:{ RemoteException -> 0x00ab }
        r3.<init>(r14);	 Catch:{ RemoteException -> 0x00ab }
        throw r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0175:
        r10 = move-exception;	 Catch:{ RemoteException -> 0x00ab }
        if (r25 == 0) goto L_0x017e;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0178:
        r3 = new java.lang.IllegalArgumentException;	 Catch:{ RemoteException -> 0x00ab }
        r3.<init>(r10);	 Catch:{ RemoteException -> 0x00ab }
        throw r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x017e:
        r3 = "WallpaperManagerService";	 Catch:{ RemoteException -> 0x00ab }
        android.util.Slog.w(r3, r10);	 Catch:{ RemoteException -> 0x00ab }
        r3 = 0;	 Catch:{ RemoteException -> 0x00ab }
        return r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0186:
        r11 = move-exception;	 Catch:{ RemoteException -> 0x00ab }
        if (r25 == 0) goto L_0x018f;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0189:
        r3 = new java.lang.IllegalArgumentException;	 Catch:{ RemoteException -> 0x00ab }
        r3.<init>(r11);	 Catch:{ RemoteException -> 0x00ab }
        throw r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x018f:
        r3 = "WallpaperManagerService";	 Catch:{ RemoteException -> 0x00ab }
        android.util.Slog.w(r3, r11);	 Catch:{ RemoteException -> 0x00ab }
        r3 = 0;	 Catch:{ RemoteException -> 0x00ab }
        return r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0197:
        r12 = r12 + 1;	 Catch:{ RemoteException -> 0x00ab }
        goto L_0x0114;	 Catch:{ RemoteException -> 0x00ab }
    L_0x019b:
        r3 = "WallpaperManagerService";	 Catch:{ RemoteException -> 0x00ab }
        android.util.Slog.w(r3, r14);	 Catch:{ RemoteException -> 0x00ab }
        r3 = 0;	 Catch:{ RemoteException -> 0x00ab }
        return r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x01a3:
        r15 = new com.android.server.wallpaper.WallpaperManagerService$WallpaperConnection;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r1 = r20;	 Catch:{ RemoteException -> 0x00ab }
        r2 = r26;	 Catch:{ RemoteException -> 0x00ab }
        r15.<init>(r1, r2);	 Catch:{ RemoteException -> 0x00ab }
        r0 = r23;	 Catch:{ RemoteException -> 0x00ab }
        r13.setComponent(r0);	 Catch:{ RemoteException -> 0x00ab }
        r3 = "android.intent.extra.client_label";	 Catch:{ RemoteException -> 0x00ab }
        r4 = 17040957; // 0x104063d float:2.4249047E-38 double:8.4193514E-317;	 Catch:{ RemoteException -> 0x00ab }
        r13.putExtra(r3, r4);	 Catch:{ RemoteException -> 0x00ab }
        r21 = "android.intent.extra.client_intent";	 Catch:{ RemoteException -> 0x00ab }
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.mContext;	 Catch:{ RemoteException -> 0x00ab }
        r4 = new android.content.Intent;	 Catch:{ RemoteException -> 0x00ab }
        r5 = "android.intent.action.SET_WALLPAPER";	 Catch:{ RemoteException -> 0x00ab }
        r4.<init>(r5);	 Catch:{ RemoteException -> 0x00ab }
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r5 = r0.mContext;	 Catch:{ RemoteException -> 0x00ab }
        r6 = 17039612; // 0x10400fc float:2.4245277E-38 double:8.418687E-317;	 Catch:{ RemoteException -> 0x00ab }
        r5 = r5.getText(r6);	 Catch:{ RemoteException -> 0x00ab }
        r5 = android.content.Intent.createChooser(r4, r5);	 Catch:{ RemoteException -> 0x00ab }
        r8 = new android.os.UserHandle;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r18;	 Catch:{ RemoteException -> 0x00ab }
        r8.<init>(r0);	 Catch:{ RemoteException -> 0x00ab }
        r4 = 0;	 Catch:{ RemoteException -> 0x00ab }
        r6 = 0;	 Catch:{ RemoteException -> 0x00ab }
        r7 = 0;	 Catch:{ RemoteException -> 0x00ab }
        r3 = android.app.PendingIntent.getActivityAsUser(r3, r4, r5, r6, r7, r8);	 Catch:{ RemoteException -> 0x00ab }
        r0 = r21;	 Catch:{ RemoteException -> 0x00ab }
        r13.putExtra(r0, r3);	 Catch:{ RemoteException -> 0x00ab }
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.mContext;	 Catch:{ RemoteException -> 0x00ab }
        r4 = new android.os.UserHandle;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r18;	 Catch:{ RemoteException -> 0x00ab }
        r4.<init>(r0);	 Catch:{ RemoteException -> 0x00ab }
        r5 = 570425345; // 0x22000001 float:1.7347237E-18 double:2.818275665E-315;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.bindServiceAsUser(r13, r15, r5, r4);	 Catch:{ RemoteException -> 0x00ab }
        if (r3 != 0) goto L_0x0227;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0201:
        r3 = new java.lang.StringBuilder;	 Catch:{ RemoteException -> 0x00ab }
        r3.<init>();	 Catch:{ RemoteException -> 0x00ab }
        r4 = "Unable to bind service: ";	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.append(r4);	 Catch:{ RemoteException -> 0x00ab }
        r0 = r23;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r3.append(r0);	 Catch:{ RemoteException -> 0x00ab }
        r14 = r3.toString();	 Catch:{ RemoteException -> 0x00ab }
        if (r25 == 0) goto L_0x021f;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0219:
        r3 = new java.lang.IllegalArgumentException;	 Catch:{ RemoteException -> 0x00ab }
        r3.<init>(r14);	 Catch:{ RemoteException -> 0x00ab }
        throw r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x021f:
        r3 = "WallpaperManagerService";	 Catch:{ RemoteException -> 0x00ab }
        android.util.Slog.w(r3, r14);	 Catch:{ RemoteException -> 0x00ab }
        r3 = 0;	 Catch:{ RemoteException -> 0x00ab }
        return r3;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0227:
        r0 = r26;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.userId;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r4 = r0.mCurrentUserId;	 Catch:{ RemoteException -> 0x00ab }
        if (r3 != r4) goto L_0x0240;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0231:
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.mLastWallpaper;	 Catch:{ RemoteException -> 0x00ab }
        if (r3 == 0) goto L_0x0240;	 Catch:{ RemoteException -> 0x00ab }
    L_0x0237:
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r3 = r0.mLastWallpaper;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r22;	 Catch:{ RemoteException -> 0x00ab }
        r0.detachWallpaperLocked(r3);	 Catch:{ RemoteException -> 0x00ab }
    L_0x0240:
        r0 = r23;	 Catch:{ RemoteException -> 0x00ab }
        r1 = r26;	 Catch:{ RemoteException -> 0x00ab }
        r1.wallpaperComponent = r0;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r26;	 Catch:{ RemoteException -> 0x00ab }
        r0.connection = r15;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r27;	 Catch:{ RemoteException -> 0x00ab }
        r15.mReply = r0;	 Catch:{ RemoteException -> 0x00ab }
        r0 = r26;	 Catch:{ RemoteException -> 0x0274 }
        r3 = r0.userId;	 Catch:{ RemoteException -> 0x0274 }
        r0 = r22;	 Catch:{ RemoteException -> 0x0274 }
        r4 = r0.mCurrentUserId;	 Catch:{ RemoteException -> 0x0274 }
        if (r3 != r4) goto L_0x026a;	 Catch:{ RemoteException -> 0x0274 }
    L_0x0258:
        r0 = r22;	 Catch:{ RemoteException -> 0x0274 }
        r3 = r0.mIWindowManager;	 Catch:{ RemoteException -> 0x0274 }
        r4 = r15.mToken;	 Catch:{ RemoteException -> 0x0274 }
        r5 = 2013; // 0x7dd float:2.821E-42 double:9.946E-321;	 Catch:{ RemoteException -> 0x0274 }
        r6 = 0;	 Catch:{ RemoteException -> 0x0274 }
        r3.addWindowToken(r4, r5, r6);	 Catch:{ RemoteException -> 0x0274 }
        r0 = r26;	 Catch:{ RemoteException -> 0x0274 }
        r1 = r22;	 Catch:{ RemoteException -> 0x0274 }
        r1.mLastWallpaper = r0;	 Catch:{ RemoteException -> 0x0274 }
    L_0x026a:
        r3 = 1;
        return r3;
    L_0x026c:
        r3 = "WallpaperManagerService";
        android.util.Slog.w(r3, r14);
        r3 = 0;
        return r3;
    L_0x0274:
        r9 = move-exception;
        goto L_0x026a;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.bindWallpaperComponentLocked(android.content.ComponentName, boolean, boolean, com.android.server.wallpaper.WallpaperManagerService$WallpaperData, android.os.IRemoteCallback):boolean");
    }

    void detachWallpaperLocked(WallpaperData wallpaper) {
        if (wallpaper.connection != null) {
            if (wallpaper.connection.mReply != null) {
                try {
                    wallpaper.connection.mReply.sendResult(null);
                } catch (RemoteException e) {
                }
                wallpaper.connection.mReply = null;
            }
            if (wallpaper.connection.mEngine != null) {
                try {
                    wallpaper.connection.mEngine.destroy();
                } catch (RemoteException e2) {
                }
            }
            this.mContext.unbindService(wallpaper.connection);
            try {
                this.mIWindowManager.removeWindowToken(wallpaper.connection.mToken, 0);
            } catch (RemoteException e3) {
            }
            wallpaper.connection.mService = null;
            wallpaper.connection.mEngine = null;
            wallpaper.connection = null;
        }
    }

    void clearWallpaperComponentLocked(WallpaperData wallpaper) {
        wallpaper.wallpaperComponent = null;
        detachWallpaperLocked(wallpaper);
    }

    void attachServiceLocked(WallpaperConnection conn, WallpaperData wallpaper) {
        try {
            conn.mService.attach(conn, conn.mToken, 2013, false, wallpaper.width, wallpaper.height, wallpaper.padding);
        } catch (RemoteException e) {
            Slog.w(TAG, "Failed attaching wallpaper; clearing", e);
            if (!wallpaper.wallpaperUpdating) {
                bindWallpaperComponentLocked(null, false, false, wallpaper, null);
            }
        }
    }

    private void notifyCallbacksLocked(WallpaperData wallpaper) {
        int n = wallpaper.callbacks.beginBroadcast();
        for (int i = 0; i < n; i++) {
            try {
                ((IWallpaperManagerCallback) wallpaper.callbacks.getBroadcastItem(i)).onWallpaperChanged();
            } catch (RemoteException e) {
            }
        }
        wallpaper.callbacks.finishBroadcast();
        this.mContext.sendBroadcastAsUser(new Intent("android.intent.action.WALLPAPER_CHANGED"), new UserHandle(this.mCurrentUserId));
    }

    private void checkPermission(String permission) {
        if (this.mContext.checkCallingOrSelfPermission(permission) != 0) {
            throw new SecurityException("Access denied to process: " + Binder.getCallingPid() + ", must have permission " + permission);
        }
    }

    public boolean isWallpaperSupported(String callingPackage) {
        return this.mAppOpsManager.checkOpNoThrow(48, Binder.getCallingUid(), callingPackage) == 0;
    }

    public boolean isSetWallpaperAllowed(String callingPackage) {
        if (!Arrays.asList(this.mContext.getPackageManager().getPackagesForUid(Binder.getCallingUid())).contains(callingPackage)) {
            return false;
        }
        DevicePolicyManager dpm = (DevicePolicyManager) this.mContext.getSystemService(DevicePolicyManager.class);
        if (dpm.isDeviceOwnerApp(callingPackage) || dpm.isProfileOwnerApp(callingPackage)) {
            return true;
        }
        return ((UserManager) this.mContext.getSystemService("user")).hasUserRestriction("no_set_wallpaper") ^ 1;
    }

    public boolean isWallpaperBackupEligible(int which, int userId) {
        if (Binder.getCallingUid() != 1000) {
            throw new SecurityException("Only the system may call isWallpaperBackupEligible");
        }
        WallpaperData wallpaper;
        if (which == 2) {
            wallpaper = (WallpaperData) this.mLockWallpaperMap.get(userId);
        } else {
            wallpaper = (WallpaperData) this.mWallpaperMap.get(userId);
        }
        return wallpaper != null ? wallpaper.allowBackup : false;
    }

    private static JournaledFile makeJournaledFile(int userId) {
        String base = new File(getWallpaperDir(userId), WALLPAPER_INFO).getAbsolutePath();
        return new JournaledFile(new File(base), new File(base + ".tmp"));
    }

    private void saveSettingsLocked(int userId) {
        JournaledFile journal = makeJournaledFile(userId);
        BufferedOutputStream stream = null;
        try {
            XmlSerializer out = new FastXmlSerializer();
            FileOutputStream fstream = new FileOutputStream(journal.chooseForWrite(), false);
            try {
                BufferedOutputStream stream2 = new BufferedOutputStream(fstream);
                FileOutputStream fileOutputStream;
                try {
                    out.setOutput(stream2, StandardCharsets.UTF_8.name());
                    out.startDocument(null, Boolean.valueOf(true));
                    WallpaperData wallpaper = (WallpaperData) this.mWallpaperMap.get(userId);
                    if (wallpaper != null) {
                        writeWallpaperAttributes(out, "wp", wallpaper);
                    }
                    wallpaper = (WallpaperData) this.mLockWallpaperMap.get(userId);
                    if (wallpaper != null) {
                        writeWallpaperAttributes(out, "kwp", wallpaper);
                    }
                    out.endDocument();
                    stream2.flush();
                    Trace.traceBegin(16777216, "WallpaperManager::fsync1");
                    FileUtils.sync(fstream);
                    Trace.traceEnd(16777216);
                    stream2.close();
                    journal.commit();
                    fileOutputStream = fstream;
                } catch (IOException e) {
                    stream = stream2;
                    fileOutputStream = fstream;
                    IoUtils.closeQuietly(stream);
                    journal.rollback();
                }
            } catch (IOException e2) {
                IoUtils.closeQuietly(stream);
                journal.rollback();
            }
        } catch (IOException e3) {
            IoUtils.closeQuietly(stream);
            journal.rollback();
        }
    }

    private void writeWallpaperAttributes(XmlSerializer out, String tag, WallpaperData wallpaper) throws IllegalArgumentException, IllegalStateException, IOException {
        out.startTag(null, tag);
        out.attribute(null, "id", Integer.toString(wallpaper.wallpaperId));
        out.attribute(null, "width", Integer.toString(wallpaper.width));
        out.attribute(null, "height", Integer.toString(wallpaper.height));
        out.attribute(null, "cropLeft", Integer.toString(wallpaper.cropHint.left));
        out.attribute(null, "cropTop", Integer.toString(wallpaper.cropHint.top));
        out.attribute(null, "cropRight", Integer.toString(wallpaper.cropHint.right));
        out.attribute(null, "cropBottom", Integer.toString(wallpaper.cropHint.bottom));
        if (wallpaper.padding.left != 0) {
            out.attribute(null, "paddingLeft", Integer.toString(wallpaper.padding.left));
        }
        if (wallpaper.padding.top != 0) {
            out.attribute(null, "paddingTop", Integer.toString(wallpaper.padding.top));
        }
        if (wallpaper.padding.right != 0) {
            out.attribute(null, "paddingRight", Integer.toString(wallpaper.padding.right));
        }
        if (wallpaper.padding.bottom != 0) {
            out.attribute(null, "paddingBottom", Integer.toString(wallpaper.padding.bottom));
        }
        if (wallpaper.primaryColors != null) {
            int colorsCount = wallpaper.primaryColors.getMainColors().size();
            out.attribute(null, "colorsCount", Integer.toString(colorsCount));
            if (colorsCount > 0) {
                for (int i = 0; i < colorsCount; i++) {
                    out.attribute(null, "colorValue" + i, Integer.toString(((Color) wallpaper.primaryColors.getMainColors().get(i)).toArgb()));
                }
            }
            out.attribute(null, "colorHints", Integer.toString(wallpaper.primaryColors.getColorHints()));
        }
        out.attribute(null, "name", wallpaper.name);
        if (!(wallpaper.wallpaperComponent == null || (wallpaper.wallpaperComponent.equals(this.mImageWallpaper) ^ 1) == 0)) {
            out.attribute(null, "component", wallpaper.wallpaperComponent.flattenToShortString());
        }
        if (wallpaper.allowBackup) {
            out.attribute(null, "backup", "true");
        }
        out.endTag(null, tag);
    }

    private void migrateFromOld() {
        File preNWallpaper = new File(getWallpaperDir(0), WALLPAPER_CROP);
        File originalWallpaper = new File("/data/data/com.android.settings/files/wallpaper");
        File newWallpaper = new File(getWallpaperDir(0), WALLPAPER);
        if (preNWallpaper.exists()) {
            if (!newWallpaper.exists()) {
                FileUtils.copyFile(preNWallpaper, newWallpaper);
            }
        } else if (originalWallpaper.exists()) {
            File oldInfo = new File("/data/system/wallpaper_info.xml");
            if (oldInfo.exists()) {
                oldInfo.renameTo(new File(getWallpaperDir(0), WALLPAPER_INFO));
            }
            FileUtils.copyFile(originalWallpaper, preNWallpaper);
            originalWallpaper.renameTo(newWallpaper);
        }
    }

    private int getAttributeInt(XmlPullParser parser, String name, int defValue) {
        String value = parser.getAttributeValue(null, name);
        if (value == null) {
            return defValue;
        }
        return Integer.parseInt(value);
    }

    private WallpaperData getWallpaperSafeLocked(int userId, int which) {
        SparseArray<WallpaperData> whichSet = which == 2 ? this.mLockWallpaperMap : this.mWallpaperMap;
        WallpaperData wallpaper = (WallpaperData) whichSet.get(userId);
        if (wallpaper != null) {
            return wallpaper;
        }
        loadSettingsLocked(userId, false);
        wallpaper = (WallpaperData) whichSet.get(userId);
        if (wallpaper != null) {
            return wallpaper;
        }
        if (which == 2) {
            wallpaper = new WallpaperData(userId, WALLPAPER_LOCK_ORIG, WALLPAPER_LOCK_CROP);
            this.mLockWallpaperMap.put(userId, wallpaper);
            ensureSaneWallpaperData(wallpaper);
            return wallpaper;
        }
        Slog.wtf(TAG, "Didn't find wallpaper in non-lock case!");
        wallpaper = new WallpaperData(userId, WALLPAPER, WALLPAPER_CROP);
        this.mWallpaperMap.put(userId, wallpaper);
        ensureSaneWallpaperData(wallpaper);
        return wallpaper;
    }

    private void loadSettingsLocked(int r27, boolean r28) {
        /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r20_2 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData) in PHI: PHI: (r20_3 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData) = (r20_1 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData), (r20_2 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData), (r20_2 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData), (r20_2 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData) binds: {(r20_1 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData)=B:1:0x0019, (r20_2 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData)=B:3:0x004c, (r20_2 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData)=B:6:0x0054, (r20_2 'wallpaper' com.android.server.wallpaper.WallpaperManagerService$WallpaperData)=B:33:0x0153}
	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:79)
	at jadx.core.dex.visitors.ModVisitor.processInvoke(ModVisitor.java:222)
	at jadx.core.dex.visitors.ModVisitor.replaceStep(ModVisitor.java:83)
	at jadx.core.dex.visitors.ModVisitor.visit(ModVisitor.java:68)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
        /*
        r26 = this;
        r12 = makeJournaledFile(r27);
        r15 = 0;
        r11 = r12.chooseForRead();
        r0 = r26;
        r0 = r0.mWallpaperMap;
        r21 = r0;
        r0 = r21;
        r1 = r27;
        r20 = r0.get(r1);
        r20 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r20;
        if (r20 != 0) goto L_0x005b;
    L_0x001b:
        r26.migrateFromOld();
        r20 = new com.android.server.wallpaper.WallpaperManagerService$WallpaperData;
        r21 = "wallpaper_orig";
        r22 = "wallpaper";
        r0 = r20;
        r1 = r27;
        r2 = r21;
        r3 = r22;
        r0.<init>(r1, r2, r3);
        r21 = 1;
        r0 = r21;
        r1 = r20;
        r1.allowBackup = r0;
        r0 = r26;
        r0 = r0.mWallpaperMap;
        r21 = r0;
        r0 = r21;
        r1 = r27;
        r2 = r20;
        r0.put(r1, r2);
        r21 = r20.cropExists();
        if (r21 != 0) goto L_0x005b;
    L_0x004e:
        r21 = r20.sourceExists();
        if (r21 == 0) goto L_0x0153;
    L_0x0054:
        r0 = r26;
        r1 = r20;
        r0.generateCrop(r1);
    L_0x005b:
        r17 = 0;
        r16 = new java.io.FileInputStream;	 Catch:{ FileNotFoundException -> 0x02a8, NullPointerException -> 0x0268, NumberFormatException -> 0x023c, XmlPullParserException -> 0x0210, IOException -> 0x01e4, IndexOutOfBoundsException -> 0x01b8 }
        r0 = r16;	 Catch:{ FileNotFoundException -> 0x02a8, NullPointerException -> 0x0268, NumberFormatException -> 0x023c, XmlPullParserException -> 0x0210, IOException -> 0x01e4, IndexOutOfBoundsException -> 0x01b8 }
        r0.<init>(r11);	 Catch:{ FileNotFoundException -> 0x02a8, NullPointerException -> 0x0268, NumberFormatException -> 0x023c, XmlPullParserException -> 0x0210, IOException -> 0x01e4, IndexOutOfBoundsException -> 0x01b8 }
        r14 = android.util.Xml.newPullParser();	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = java.nio.charset.StandardCharsets.UTF_8;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = r21.name();	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r16;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r14.setInput(r0, r1);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x0075:
        r19 = r14.next();	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = 2;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r19;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        if (r0 != r1) goto L_0x00db;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x0081:
        r18 = r14.getName();	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = "wp";	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r18;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = r0.equals(r1);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        if (r21 == 0) goto L_0x0162;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x0092:
        r0 = r26;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r20;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r2 = r28;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0.parseWallpaperAttributes(r14, r1, r2);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = "component";	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r22 = 0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r22;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r4 = r14.getAttributeValue(r0, r1);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        if (r4 == 0) goto L_0x015e;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x00aa:
        r21 = android.content.ComponentName.unflattenFromString(r4);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x00ae:
        r0 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r20;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1.nextWallpaperComponent = r0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r20;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r0.nextWallpaperComponent;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = r0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        if (r21 == 0) goto L_0x00cf;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x00bc:
        r21 = "android";	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r20;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r0.nextWallpaperComponent;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r22 = r0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r22 = r22.getPackageName();	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = r21.equals(r22);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        if (r21 == 0) goto L_0x00db;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x00cf:
        r0 = r26;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r0.mImageWallpaper;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = r0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r20;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1.nextWallpaperComponent = r0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x00db:
        r21 = 1;
        r0 = r19;
        r1 = r21;
        if (r0 != r1) goto L_0x0075;
    L_0x00e3:
        r17 = 1;
        r15 = r16;
    L_0x00e7:
        libcore.io.IoUtils.closeQuietly(r15);
        if (r17 != 0) goto L_0x0294;
    L_0x00ec:
        r21 = -1;
        r0 = r21;
        r1 = r20;
        r1.width = r0;
        r21 = -1;
        r0 = r21;
        r1 = r20;
        r1.height = r0;
        r0 = r20;
        r0 = r0.cropHint;
        r21 = r0;
        r22 = 0;
        r23 = 0;
        r24 = 0;
        r25 = 0;
        r21.set(r22, r23, r24, r25);
        r0 = r20;
        r0 = r0.padding;
        r21 = r0;
        r22 = 0;
        r23 = 0;
        r24 = 0;
        r25 = 0;
        r21.set(r22, r23, r24, r25);
        r21 = "";
        r0 = r21;
        r1 = r20;
        r1.name = r0;
        r0 = r26;
        r0 = r0.mLockWallpaperMap;
        r21 = r0;
        r0 = r21;
        r1 = r27;
        r0.remove(r1);
    L_0x0134:
        r0 = r26;
        r1 = r20;
        r0.ensureSaneWallpaperData(r1);
        r0 = r26;
        r0 = r0.mLockWallpaperMap;
        r21 = r0;
        r0 = r21;
        r1 = r27;
        r13 = r0.get(r1);
        r13 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r13;
        if (r13 == 0) goto L_0x0152;
    L_0x014d:
        r0 = r26;
        r0.ensureSaneWallpaperData(r13);
    L_0x0152:
        return;
    L_0x0153:
        r21 = "WallpaperManagerService";
        r22 = "No static wallpaper imagery; defaults will be shown";
        android.util.Slog.i(r21, r22);
        goto L_0x005b;
    L_0x015e:
        r21 = 0;
        goto L_0x00ae;
    L_0x0162:
        r21 = "kwp";	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r18;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = r0.equals(r1);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        if (r21 == 0) goto L_0x00db;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x016f:
        r0 = r26;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r0.mLockWallpaperMap;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = r0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r27;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r13 = r0.get(r1);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r13 = (com.android.server.wallpaper.WallpaperManagerService.WallpaperData) r13;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        if (r13 != 0) goto L_0x019f;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x0181:
        r13 = new com.android.server.wallpaper.WallpaperManagerService$WallpaperData;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = "wallpaper_lock_orig";	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r22 = "wallpaper_lock";	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r27;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r2 = r22;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r13.<init>(r0, r1, r2);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r26;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r0.mLockWallpaperMap;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r21 = r0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r27;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0.put(r1, r13);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
    L_0x019f:
        r21 = 0;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0 = r26;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r1 = r21;	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        r0.parseWallpaperAttributes(r14, r13, r1);	 Catch:{ FileNotFoundException -> 0x01aa, NullPointerException -> 0x02ab, NumberFormatException -> 0x02af, XmlPullParserException -> 0x02b3, IOException -> 0x02b8, IndexOutOfBoundsException -> 0x02bd }
        goto L_0x00db;
    L_0x01aa:
        r5 = move-exception;
        r15 = r16;
    L_0x01ad:
        r21 = "WallpaperManagerService";
        r22 = "no current wallpaper -- first boot?";
        android.util.Slog.w(r21, r22);
        goto L_0x00e7;
    L_0x01b8:
        r7 = move-exception;
    L_0x01b9:
        r21 = "WallpaperManagerService";
        r22 = new java.lang.StringBuilder;
        r22.<init>();
        r23 = "failed parsing ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r11);
        r23 = " ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r7);
        r22 = r22.toString();
        android.util.Slog.w(r21, r22);
        goto L_0x00e7;
    L_0x01e4:
        r6 = move-exception;
    L_0x01e5:
        r21 = "WallpaperManagerService";
        r22 = new java.lang.StringBuilder;
        r22.<init>();
        r23 = "failed parsing ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r11);
        r23 = " ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r6);
        r22 = r22.toString();
        android.util.Slog.w(r21, r22);
        goto L_0x00e7;
    L_0x0210:
        r10 = move-exception;
    L_0x0211:
        r21 = "WallpaperManagerService";
        r22 = new java.lang.StringBuilder;
        r22.<init>();
        r23 = "failed parsing ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r11);
        r23 = " ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r10);
        r22 = r22.toString();
        android.util.Slog.w(r21, r22);
        goto L_0x00e7;
    L_0x023c:
        r9 = move-exception;
    L_0x023d:
        r21 = "WallpaperManagerService";
        r22 = new java.lang.StringBuilder;
        r22.<init>();
        r23 = "failed parsing ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r11);
        r23 = " ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r9);
        r22 = r22.toString();
        android.util.Slog.w(r21, r22);
        goto L_0x00e7;
    L_0x0268:
        r8 = move-exception;
    L_0x0269:
        r21 = "WallpaperManagerService";
        r22 = new java.lang.StringBuilder;
        r22.<init>();
        r23 = "failed parsing ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r11);
        r23 = " ";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r8);
        r22 = r22.toString();
        android.util.Slog.w(r21, r22);
        goto L_0x00e7;
    L_0x0294:
        r0 = r20;
        r0 = r0.wallpaperId;
        r21 = r0;
        if (r21 > 0) goto L_0x0134;
    L_0x029c:
        r21 = r26.makeWallpaperIdLocked();
        r0 = r21;
        r1 = r20;
        r1.wallpaperId = r0;
        goto L_0x0134;
    L_0x02a8:
        r5 = move-exception;
        goto L_0x01ad;
    L_0x02ab:
        r8 = move-exception;
        r15 = r16;
        goto L_0x0269;
    L_0x02af:
        r9 = move-exception;
        r15 = r16;
        goto L_0x023d;
    L_0x02b3:
        r10 = move-exception;
        r15 = r16;
        goto L_0x0211;
    L_0x02b8:
        r6 = move-exception;
        r15 = r16;
        goto L_0x01e5;
    L_0x02bd:
        r7 = move-exception;
        r15 = r16;
        goto L_0x01b9;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.wallpaper.WallpaperManagerService.loadSettingsLocked(int, boolean):void");
    }

    private void ensureSaneWallpaperData(WallpaperData wallpaper) {
        int baseSize = getMaximumSizeDimension();
        if (wallpaper.width < baseSize) {
            wallpaper.width = baseSize;
        }
        if (wallpaper.height < baseSize) {
            wallpaper.height = baseSize;
        }
        if (wallpaper.cropHint.width() <= 0 || wallpaper.cropHint.height() <= 0) {
            wallpaper.cropHint.set(0, 0, wallpaper.width, wallpaper.height);
        }
    }

    private void parseWallpaperAttributes(XmlPullParser parser, WallpaperData wallpaper, boolean keepDimensionHints) {
        String idString = parser.getAttributeValue(null, "id");
        if (idString != null) {
            int id = Integer.parseInt(idString);
            wallpaper.wallpaperId = id;
            if (id > this.mWallpaperId) {
                this.mWallpaperId = id;
            }
        } else {
            wallpaper.wallpaperId = makeWallpaperIdLocked();
        }
        if (!keepDimensionHints) {
            wallpaper.width = Integer.parseInt(parser.getAttributeValue(null, "width"));
            wallpaper.height = Integer.parseInt(parser.getAttributeValue(null, "height"));
        }
        wallpaper.cropHint.left = getAttributeInt(parser, "cropLeft", 0);
        wallpaper.cropHint.top = getAttributeInt(parser, "cropTop", 0);
        wallpaper.cropHint.right = getAttributeInt(parser, "cropRight", 0);
        wallpaper.cropHint.bottom = getAttributeInt(parser, "cropBottom", 0);
        wallpaper.padding.left = getAttributeInt(parser, "paddingLeft", 0);
        wallpaper.padding.top = getAttributeInt(parser, "paddingTop", 0);
        wallpaper.padding.right = getAttributeInt(parser, "paddingRight", 0);
        wallpaper.padding.bottom = getAttributeInt(parser, "paddingBottom", 0);
        int colorsCount = getAttributeInt(parser, "colorsCount", 0);
        if (colorsCount > 0) {
            Color primary = null;
            Color secondary = null;
            Color tertiary = null;
            for (int i = 0; i < colorsCount; i++) {
                Color color = Color.valueOf(getAttributeInt(parser, "colorValue" + i, 0));
                if (i != 0) {
                    if (i != 1) {
                        if (i != 2) {
                            break;
                        }
                        tertiary = color;
                    } else {
                        secondary = color;
                    }
                } else {
                    primary = color;
                }
            }
            wallpaper.primaryColors = new WallpaperColors(primary, secondary, tertiary, getAttributeInt(parser, "colorHints", 0));
        }
        wallpaper.name = parser.getAttributeValue(null, "name");
        wallpaper.allowBackup = "true".equals(parser.getAttributeValue(null, "backup"));
    }

    private int getMaximumSizeDimension() {
        return ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getMaximumSizeDimension();
    }

    public void settingsRestored() {
        if (Binder.getCallingUid() != 1000) {
            throw new RuntimeException("settingsRestored() can only be called from the system process");
        }
        synchronized (this.mLock) {
            boolean success;
            loadSettingsLocked(0, false);
            WallpaperData wallpaper = (WallpaperData) this.mWallpaperMap.get(0);
            wallpaper.wallpaperId = makeWallpaperIdLocked();
            wallpaper.allowBackup = true;
            if (wallpaper.nextWallpaperComponent == null || (wallpaper.nextWallpaperComponent.equals(this.mImageWallpaper) ^ 1) == 0) {
                if ("".equals(wallpaper.name)) {
                    success = true;
                } else {
                    success = restoreNamedResourceLocked(wallpaper);
                }
                if (success) {
                    generateCrop(wallpaper);
                    bindWallpaperComponentLocked(wallpaper.nextWallpaperComponent, true, false, wallpaper, null);
                }
            } else {
                if (!bindWallpaperComponentLocked(wallpaper.nextWallpaperComponent, false, false, wallpaper, null)) {
                    bindWallpaperComponentLocked(null, false, false, wallpaper, null);
                }
                success = true;
            }
        }
        if (!success) {
            Slog.e(TAG, "Failed to restore wallpaper: '" + wallpaper.name + "'");
            wallpaper.name = "";
            getWallpaperDir(0).delete();
        }
        synchronized (this.mLock) {
            saveSettingsLocked(0);
        }
    }

    boolean restoreNamedResourceLocked(WallpaperData wallpaper) {
        Throwable th;
        Object cos;
        Object obj;
        IOException e;
        if (wallpaper.name.length() > 4 && "res:".equals(wallpaper.name.substring(0, 4))) {
            String resName = wallpaper.name.substring(4);
            String pkg = null;
            int colon = resName.indexOf(58);
            if (colon > 0) {
                pkg = resName.substring(0, colon);
            }
            String ident = null;
            int slash = resName.lastIndexOf(47);
            if (slash > 0) {
                ident = resName.substring(slash + 1);
            }
            String type = null;
            if (colon > 0 && slash > 0 && slash - colon > 1) {
                type = resName.substring(colon + 1, slash);
            }
            if (!(pkg == null || ident == null || type == null)) {
                int i = -1;
                AutoCloseable autoCloseable = null;
                AutoCloseable autoCloseable2 = null;
                AutoCloseable autoCloseable3 = null;
                try {
                    Resources r = this.mContext.createPackageContext(pkg, 4).getResources();
                    i = r.getIdentifier(resName, null, null);
                    if (i == 0) {
                        Slog.e(TAG, "couldn't resolve identifier pkg=" + pkg + " type=" + type + " ident=" + ident);
                        IoUtils.closeQuietly(null);
                        IoUtils.closeQuietly(null);
                        IoUtils.closeQuietly(null);
                        return false;
                    }
                    autoCloseable = r.openRawResource(i);
                    if (wallpaper.wallpaperFile.exists()) {
                        wallpaper.wallpaperFile.delete();
                        wallpaper.cropFile.delete();
                    }
                    FileOutputStream fos = new FileOutputStream(wallpaper.wallpaperFile);
                    try {
                        FileOutputStream cos2 = new FileOutputStream(wallpaper.cropFile);
                        try {
                            byte[] buffer = new byte[32768];
                            while (true) {
                                int amt = autoCloseable.read(buffer);
                                if (amt <= 0) {
                                    break;
                                }
                                fos.write(buffer, 0, amt);
                                cos2.write(buffer, 0, amt);
                            }
                            Slog.v(TAG, "Restored wallpaper: " + resName);
                            IoUtils.closeQuietly(autoCloseable);
                            if (fos != null) {
                                Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                                FileUtils.sync(fos);
                                Trace.traceEnd(16777216);
                            }
                            if (cos2 != null) {
                                Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                                FileUtils.sync(cos2);
                                Trace.traceEnd(16777216);
                            }
                            IoUtils.closeQuietly(fos);
                            IoUtils.closeQuietly(cos2);
                            return true;
                        } catch (NameNotFoundException e2) {
                            autoCloseable3 = cos2;
                            autoCloseable2 = fos;
                            try {
                                Slog.e(TAG, "Package name " + pkg + " not found");
                                IoUtils.closeQuietly(autoCloseable);
                                if (autoCloseable2 != null) {
                                    Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                                    FileUtils.sync(autoCloseable2);
                                    Trace.traceEnd(16777216);
                                }
                                if (autoCloseable3 != null) {
                                    Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                                    FileUtils.sync(autoCloseable3);
                                    Trace.traceEnd(16777216);
                                }
                                IoUtils.closeQuietly(autoCloseable2);
                                IoUtils.closeQuietly(autoCloseable3);
                                return false;
                            } catch (Throwable th2) {
                                th = th2;
                                IoUtils.closeQuietly(autoCloseable);
                                if (autoCloseable2 != null) {
                                    Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                                    FileUtils.sync(autoCloseable2);
                                    Trace.traceEnd(16777216);
                                }
                                if (autoCloseable3 != null) {
                                    Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                                    FileUtils.sync(autoCloseable3);
                                    Trace.traceEnd(16777216);
                                }
                                IoUtils.closeQuietly(autoCloseable2);
                                IoUtils.closeQuietly(autoCloseable3);
                                throw th;
                            }
                        } catch (NotFoundException e3) {
                            cos = cos2;
                            obj = fos;
                            Slog.e(TAG, "Resource not found: " + i);
                            IoUtils.closeQuietly(autoCloseable);
                            if (autoCloseable2 != null) {
                                Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                                FileUtils.sync(autoCloseable2);
                                Trace.traceEnd(16777216);
                            }
                            if (autoCloseable3 != null) {
                                Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                                FileUtils.sync(autoCloseable3);
                                Trace.traceEnd(16777216);
                            }
                            IoUtils.closeQuietly(autoCloseable2);
                            IoUtils.closeQuietly(autoCloseable3);
                            return false;
                        } catch (IOException e4) {
                            e = e4;
                            cos = cos2;
                            obj = fos;
                            Slog.e(TAG, "IOException while restoring wallpaper ", e);
                            IoUtils.closeQuietly(autoCloseable);
                            if (autoCloseable2 != null) {
                                Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                                FileUtils.sync(autoCloseable2);
                                Trace.traceEnd(16777216);
                            }
                            if (autoCloseable3 != null) {
                                Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                                FileUtils.sync(autoCloseable3);
                                Trace.traceEnd(16777216);
                            }
                            IoUtils.closeQuietly(autoCloseable2);
                            IoUtils.closeQuietly(autoCloseable3);
                            return false;
                        } catch (Throwable th3) {
                            th = th3;
                            cos = cos2;
                            obj = fos;
                            IoUtils.closeQuietly(autoCloseable);
                            if (autoCloseable2 != null) {
                                Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                                FileUtils.sync(autoCloseable2);
                                Trace.traceEnd(16777216);
                            }
                            if (autoCloseable3 != null) {
                                Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                                FileUtils.sync(autoCloseable3);
                                Trace.traceEnd(16777216);
                            }
                            IoUtils.closeQuietly(autoCloseable2);
                            IoUtils.closeQuietly(autoCloseable3);
                            throw th;
                        }
                    } catch (NameNotFoundException e5) {
                        obj = fos;
                        Slog.e(TAG, "Package name " + pkg + " not found");
                        IoUtils.closeQuietly(autoCloseable);
                        if (autoCloseable2 != null) {
                            Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                            FileUtils.sync(autoCloseable2);
                            Trace.traceEnd(16777216);
                        }
                        if (autoCloseable3 != null) {
                            Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                            FileUtils.sync(autoCloseable3);
                            Trace.traceEnd(16777216);
                        }
                        IoUtils.closeQuietly(autoCloseable2);
                        IoUtils.closeQuietly(autoCloseable3);
                        return false;
                    } catch (NotFoundException e6) {
                        obj = fos;
                        Slog.e(TAG, "Resource not found: " + i);
                        IoUtils.closeQuietly(autoCloseable);
                        if (autoCloseable2 != null) {
                            Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                            FileUtils.sync(autoCloseable2);
                            Trace.traceEnd(16777216);
                        }
                        if (autoCloseable3 != null) {
                            Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                            FileUtils.sync(autoCloseable3);
                            Trace.traceEnd(16777216);
                        }
                        IoUtils.closeQuietly(autoCloseable2);
                        IoUtils.closeQuietly(autoCloseable3);
                        return false;
                    } catch (IOException e7) {
                        e = e7;
                        obj = fos;
                        Slog.e(TAG, "IOException while restoring wallpaper ", e);
                        IoUtils.closeQuietly(autoCloseable);
                        if (autoCloseable2 != null) {
                            Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                            FileUtils.sync(autoCloseable2);
                            Trace.traceEnd(16777216);
                        }
                        if (autoCloseable3 != null) {
                            Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                            FileUtils.sync(autoCloseable3);
                            Trace.traceEnd(16777216);
                        }
                        IoUtils.closeQuietly(autoCloseable2);
                        IoUtils.closeQuietly(autoCloseable3);
                        return false;
                    } catch (Throwable th4) {
                        th = th4;
                        obj = fos;
                        IoUtils.closeQuietly(autoCloseable);
                        if (autoCloseable2 != null) {
                            Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                            FileUtils.sync(autoCloseable2);
                            Trace.traceEnd(16777216);
                        }
                        if (autoCloseable3 != null) {
                            Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                            FileUtils.sync(autoCloseable3);
                            Trace.traceEnd(16777216);
                        }
                        IoUtils.closeQuietly(autoCloseable2);
                        IoUtils.closeQuietly(autoCloseable3);
                        throw th;
                    }
                } catch (NameNotFoundException e8) {
                    Slog.e(TAG, "Package name " + pkg + " not found");
                    IoUtils.closeQuietly(autoCloseable);
                    if (autoCloseable2 != null) {
                        Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                        FileUtils.sync(autoCloseable2);
                        Trace.traceEnd(16777216);
                    }
                    if (autoCloseable3 != null) {
                        Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                        FileUtils.sync(autoCloseable3);
                        Trace.traceEnd(16777216);
                    }
                    IoUtils.closeQuietly(autoCloseable2);
                    IoUtils.closeQuietly(autoCloseable3);
                    return false;
                } catch (NotFoundException e9) {
                    Slog.e(TAG, "Resource not found: " + i);
                    IoUtils.closeQuietly(autoCloseable);
                    if (autoCloseable2 != null) {
                        Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                        FileUtils.sync(autoCloseable2);
                        Trace.traceEnd(16777216);
                    }
                    if (autoCloseable3 != null) {
                        Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                        FileUtils.sync(autoCloseable3);
                        Trace.traceEnd(16777216);
                    }
                    IoUtils.closeQuietly(autoCloseable2);
                    IoUtils.closeQuietly(autoCloseable3);
                    return false;
                } catch (IOException e10) {
                    e = e10;
                    Slog.e(TAG, "IOException while restoring wallpaper ", e);
                    IoUtils.closeQuietly(autoCloseable);
                    if (autoCloseable2 != null) {
                        Trace.traceBegin(16777216, "WallpaperManager::fsync2");
                        FileUtils.sync(autoCloseable2);
                        Trace.traceEnd(16777216);
                    }
                    if (autoCloseable3 != null) {
                        Trace.traceBegin(16777216, "WallpaperManager::fsync3");
                        FileUtils.sync(autoCloseable3);
                        Trace.traceEnd(16777216);
                    }
                    IoUtils.closeQuietly(autoCloseable2);
                    IoUtils.closeQuietly(autoCloseable3);
                    return false;
                }
            }
        }
        return false;
    }

    protected void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        if (DumpUtils.checkDumpPermission(this.mContext, TAG, pw)) {
            synchronized (this.mLock) {
                int i;
                WallpaperData wallpaper;
                pw.println("System wallpaper state:");
                for (i = 0; i < this.mWallpaperMap.size(); i++) {
                    wallpaper = (WallpaperData) this.mWallpaperMap.valueAt(i);
                    pw.print(" User ");
                    pw.print(wallpaper.userId);
                    pw.print(": id=");
                    pw.println(wallpaper.wallpaperId);
                    pw.print("  mWidth=");
                    pw.print(wallpaper.width);
                    pw.print(" mHeight=");
                    pw.println(wallpaper.height);
                    pw.print("  mCropHint=");
                    pw.println(wallpaper.cropHint);
                    pw.print("  mPadding=");
                    pw.println(wallpaper.padding);
                    pw.print("  mName=");
                    pw.println(wallpaper.name);
                    pw.print("  mAllowBackup=");
                    pw.println(wallpaper.allowBackup);
                    pw.print("  mWallpaperComponent=");
                    pw.println(wallpaper.wallpaperComponent);
                    if (wallpaper.connection != null) {
                        WallpaperConnection conn = wallpaper.connection;
                        pw.print("  Wallpaper connection ");
                        pw.print(conn);
                        pw.println(":");
                        if (conn.mInfo != null) {
                            pw.print("    mInfo.component=");
                            pw.println(conn.mInfo.getComponent());
                        }
                        pw.print("    mToken=");
                        pw.println(conn.mToken);
                        pw.print("    mService=");
                        pw.println(conn.mService);
                        pw.print("    mEngine=");
                        pw.println(conn.mEngine);
                        pw.print("    mLastDiedTime=");
                        pw.println(wallpaper.lastDiedTime - SystemClock.uptimeMillis());
                    }
                }
                pw.println("Lock wallpaper state:");
                for (i = 0; i < this.mLockWallpaperMap.size(); i++) {
                    wallpaper = (WallpaperData) this.mLockWallpaperMap.valueAt(i);
                    pw.print(" User ");
                    pw.print(wallpaper.userId);
                    pw.print(": id=");
                    pw.println(wallpaper.wallpaperId);
                    pw.print("  mWidth=");
                    pw.print(wallpaper.width);
                    pw.print(" mHeight=");
                    pw.println(wallpaper.height);
                    pw.print("  mCropHint=");
                    pw.println(wallpaper.cropHint);
                    pw.print("  mPadding=");
                    pw.println(wallpaper.padding);
                    pw.print("  mName=");
                    pw.println(wallpaper.name);
                    pw.print("  mAllowBackup=");
                    pw.println(wallpaper.allowBackup);
                }
            }
        }
    }
}
