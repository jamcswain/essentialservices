package com.android.server.print;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.UserInfo;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Binder;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import android.print.IPrintManager.Stub;
import android.printservice.PrintServiceInfo;
import android.provider.Settings.Secure;
import android.util.SparseArray;
import com.android.internal.content.PackageMonitor;
import com.android.internal.os.BackgroundThread;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.Preconditions;
import com.android.server.SystemService;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

public final class PrintManagerService extends SystemService {
    private static final String LOG_TAG = "PrintManagerService";
    private final PrintManagerImpl mPrintManagerImpl;

    class PrintManagerImpl extends Stub {
        private static final int BACKGROUND_USER_ID = -10;
        private final Context mContext;
        private final Object mLock = new Object();
        private final UserManager mUserManager;
        private final SparseArray<UserState> mUserStates = new SparseArray();

        PrintManagerImpl(Context context) {
            this.mContext = context;
            this.mUserManager = (UserManager) context.getSystemService("user");
            registerContentObservers();
            registerBroadcastReceivers();
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.os.Bundle print(java.lang.String r10, android.print.IPrintDocumentAdapter r11, android.print.PrintAttributes r12, java.lang.String r13, int r14, int r15) {
            /*
            r9 = this;
            r10 = com.android.internal.util.Preconditions.checkStringNotEmpty(r10);
            r10 = (java.lang.String) r10;
            r11 = com.android.internal.util.Preconditions.checkNotNull(r11);
            r11 = (android.print.IPrintDocumentAdapter) r11;
            r13 = com.android.internal.util.Preconditions.checkStringNotEmpty(r13);
            r13 = (java.lang.String) r13;
            r8 = r9.resolveCallingUserEnforcingPermissions(r15);
            r2 = r9.mLock;
            monitor-enter(r2);
            r1 = r9.resolveCallingProfileParentLocked(r8);	 Catch:{ all -> 0x0043 }
            r3 = r9.getCurrentUserId();	 Catch:{ all -> 0x0043 }
            if (r1 == r3) goto L_0x0026;
        L_0x0023:
            r1 = 0;
            monitor-exit(r2);
            return r1;
        L_0x0026:
            r5 = r9.resolveCallingAppEnforcingPermissions(r14);	 Catch:{ all -> 0x0043 }
            r4 = r9.resolveCallingPackageNameEnforcingSecurity(r13);	 Catch:{ all -> 0x0043 }
            r1 = 0;
            r0 = r9.getOrCreateUserStateLocked(r8, r1);	 Catch:{ all -> 0x0043 }
            monitor-exit(r2);
            r6 = android.os.Binder.clearCallingIdentity();
            r1 = r10;
            r2 = r11;
            r3 = r12;
            r1 = r0.print(r1, r2, r3, r4, r5);	 Catch:{ all -> 0x0046 }
            android.os.Binder.restoreCallingIdentity(r6);
            return r1;
        L_0x0043:
            r1 = move-exception;
            monitor-exit(r2);
            throw r1;
        L_0x0046:
            r1 = move-exception;
            android.os.Binder.restoreCallingIdentity(r6);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.print(java.lang.String, android.print.IPrintDocumentAdapter, android.print.PrintAttributes, java.lang.String, int, int):android.os.Bundle");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.util.List<android.print.PrintJobInfo> getPrintJobInfos(int r9, int r10) {
            /*
            r8 = this;
            r3 = r8.resolveCallingUserEnforcingPermissions(r10);
            r6 = r8.mLock;
            monitor-enter(r6);
            r5 = r8.resolveCallingProfileParentLocked(r3);	 Catch:{ all -> 0x002a }
            r7 = r8.getCurrentUserId();	 Catch:{ all -> 0x002a }
            if (r5 == r7) goto L_0x0014;
        L_0x0011:
            r5 = 0;
            monitor-exit(r6);
            return r5;
        L_0x0014:
            r2 = r8.resolveCallingAppEnforcingPermissions(r9);	 Catch:{ all -> 0x002a }
            r5 = 0;
            r4 = r8.getOrCreateUserStateLocked(r3, r5);	 Catch:{ all -> 0x002a }
            monitor-exit(r6);
            r0 = android.os.Binder.clearCallingIdentity();
            r5 = r4.getPrintJobInfos(r2);	 Catch:{ all -> 0x002d }
            android.os.Binder.restoreCallingIdentity(r0);
            return r5;
        L_0x002a:
            r5 = move-exception;
            monitor-exit(r6);
            throw r5;
        L_0x002d:
            r5 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r5;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.getPrintJobInfos(int, int):java.util.List<android.print.PrintJobInfo>");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.print.PrintJobInfo getPrintJobInfo(android.print.PrintJobId r10, int r11, int r12) {
            /*
            r9 = this;
            r8 = 0;
            if (r10 != 0) goto L_0x0004;
        L_0x0003:
            return r8;
        L_0x0004:
            r3 = r9.resolveCallingUserEnforcingPermissions(r12);
            r6 = r9.mLock;
            monitor-enter(r6);
            r5 = r9.resolveCallingProfileParentLocked(r3);	 Catch:{ all -> 0x002d }
            r7 = r9.getCurrentUserId();	 Catch:{ all -> 0x002d }
            if (r5 == r7) goto L_0x0017;
        L_0x0015:
            monitor-exit(r6);
            return r8;
        L_0x0017:
            r2 = r9.resolveCallingAppEnforcingPermissions(r11);	 Catch:{ all -> 0x002d }
            r5 = 0;
            r4 = r9.getOrCreateUserStateLocked(r3, r5);	 Catch:{ all -> 0x002d }
            monitor-exit(r6);
            r0 = android.os.Binder.clearCallingIdentity();
            r5 = r4.getPrintJobInfo(r10, r2);	 Catch:{ all -> 0x0030 }
            android.os.Binder.restoreCallingIdentity(r0);
            return r5;
        L_0x002d:
            r5 = move-exception;
            monitor-exit(r6);
            throw r5;
        L_0x0030:
            r5 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r5;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.getPrintJobInfo(android.print.PrintJobId, int, int):android.print.PrintJobInfo");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.graphics.drawable.Icon getCustomPrinterIcon(android.print.PrinterId r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.PrinterId) r8;
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002c }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x002c }
            if (r4 == r6) goto L_0x001a;
        L_0x0017:
            r4 = 0;
            monitor-exit(r5);
            return r4;
        L_0x001a:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002c }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r4 = r3.getCustomPrinterIcon(r8);	 Catch:{ all -> 0x002f }
            android.os.Binder.restoreCallingIdentity(r0);
            return r4;
        L_0x002c:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x002f:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.getCustomPrinterIcon(android.print.PrinterId, int):android.graphics.drawable.Icon");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void cancelPrintJob(android.print.PrintJobId r9, int r10, int r11) {
            /*
            r8 = this;
            if (r9 != 0) goto L_0x0003;
        L_0x0002:
            return;
        L_0x0003:
            r3 = r8.resolveCallingUserEnforcingPermissions(r11);
            r6 = r8.mLock;
            monitor-enter(r6);
            r5 = r8.resolveCallingProfileParentLocked(r3);	 Catch:{ all -> 0x002b }
            r7 = r8.getCurrentUserId();	 Catch:{ all -> 0x002b }
            if (r5 == r7) goto L_0x0016;
        L_0x0014:
            monitor-exit(r6);
            return;
        L_0x0016:
            r2 = r8.resolveCallingAppEnforcingPermissions(r10);	 Catch:{ all -> 0x002b }
            r5 = 0;
            r4 = r8.getOrCreateUserStateLocked(r3, r5);	 Catch:{ all -> 0x002b }
            monitor-exit(r6);
            r0 = android.os.Binder.clearCallingIdentity();
            r4.cancelPrintJob(r9, r2);	 Catch:{ all -> 0x002e }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002b:
            r5 = move-exception;
            monitor-exit(r6);
            throw r5;
        L_0x002e:
            r5 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r5;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.cancelPrintJob(android.print.PrintJobId, int, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void restartPrintJob(android.print.PrintJobId r9, int r10, int r11) {
            /*
            r8 = this;
            if (r9 != 0) goto L_0x0003;
        L_0x0002:
            return;
        L_0x0003:
            r3 = r8.resolveCallingUserEnforcingPermissions(r11);
            r6 = r8.mLock;
            monitor-enter(r6);
            r5 = r8.resolveCallingProfileParentLocked(r3);	 Catch:{ all -> 0x002b }
            r7 = r8.getCurrentUserId();	 Catch:{ all -> 0x002b }
            if (r5 == r7) goto L_0x0016;
        L_0x0014:
            monitor-exit(r6);
            return;
        L_0x0016:
            r2 = r8.resolveCallingAppEnforcingPermissions(r10);	 Catch:{ all -> 0x002b }
            r5 = 0;
            r4 = r8.getOrCreateUserStateLocked(r3, r5);	 Catch:{ all -> 0x002b }
            monitor-exit(r6);
            r0 = android.os.Binder.clearCallingIdentity();
            r4.restartPrintJob(r9, r2);	 Catch:{ all -> 0x002e }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002b:
            r5 = move-exception;
            monitor-exit(r6);
            throw r5;
        L_0x002e:
            r5 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r5;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.restartPrintJob(android.print.PrintJobId, int, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.util.List<android.printservice.PrintServiceInfo> getPrintServices(int r9, int r10) {
            /*
            r8 = this;
            r7 = 0;
            r4 = 3;
            com.android.internal.util.Preconditions.checkFlagsArgument(r9, r4);
            r4 = r8.mContext;
            r5 = "android.permission.READ_PRINT_SERVICES";
            r4.enforceCallingOrSelfPermission(r5, r7);
            r2 = r8.resolveCallingUserEnforcingPermissions(r10);
            r5 = r8.mLock;
            monitor-enter(r5);
            r4 = r8.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x0032 }
            r6 = r8.getCurrentUserId();	 Catch:{ all -> 0x0032 }
            if (r4 == r6) goto L_0x0020;
        L_0x001e:
            monitor-exit(r5);
            return r7;
        L_0x0020:
            r4 = 0;
            r3 = r8.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x0032 }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r4 = r3.getPrintServices(r9);	 Catch:{ all -> 0x0035 }
            android.os.Binder.restoreCallingIdentity(r0);
            return r4;
        L_0x0032:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x0035:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.getPrintServices(int, int):java.util.List<android.printservice.PrintServiceInfo>");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void setPrintServiceEnabled(android.content.ComponentName r10, boolean r11, int r12) {
            /*
            r9 = this;
            r4 = r9.resolveCallingUserEnforcingPermissions(r12);
            r6 = android.os.Binder.getCallingUid();
            r0 = android.os.UserHandle.getAppId(r6);
            r6 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
            if (r0 == r6) goto L_0x0037;
        L_0x0010:
            r6 = r9.mContext;	 Catch:{ NameNotFoundException -> 0x002c }
            r6 = r6.getPackageManager();	 Catch:{ NameNotFoundException -> 0x002c }
            r7 = "com.android.printspooler";
            r6 = r6.getPackageUidAsUser(r7, r4);	 Catch:{ NameNotFoundException -> 0x002c }
            r6 = android.os.UserHandle.getAppId(r6);	 Catch:{ NameNotFoundException -> 0x002c }
            if (r0 == r6) goto L_0x0037;
        L_0x0023:
            r6 = new java.lang.SecurityException;	 Catch:{ NameNotFoundException -> 0x002c }
            r7 = "Only system and print spooler can call this";
            r6.<init>(r7);	 Catch:{ NameNotFoundException -> 0x002c }
            throw r6;	 Catch:{ NameNotFoundException -> 0x002c }
        L_0x002c:
            r1 = move-exception;
            r6 = "PrintManagerService";
            r7 = "Could not verify caller";
            android.util.Log.e(r6, r7, r1);
            return;
        L_0x0037:
            r10 = com.android.internal.util.Preconditions.checkNotNull(r10);
            r10 = (android.content.ComponentName) r10;
            r7 = r9.mLock;
            monitor-enter(r7);
            r6 = r9.resolveCallingProfileParentLocked(r4);	 Catch:{ all -> 0x005d }
            r8 = r9.getCurrentUserId();	 Catch:{ all -> 0x005d }
            if (r6 == r8) goto L_0x004c;
        L_0x004a:
            monitor-exit(r7);
            return;
        L_0x004c:
            r6 = 0;
            r5 = r9.getOrCreateUserStateLocked(r4, r6);	 Catch:{ all -> 0x005d }
            monitor-exit(r7);
            r2 = android.os.Binder.clearCallingIdentity();
            r5.setPrintServiceEnabled(r10, r11);	 Catch:{ all -> 0x0060 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x005d:
            r6 = move-exception;
            monitor-exit(r7);
            throw r6;
        L_0x0060:
            r6 = move-exception;
            android.os.Binder.restoreCallingIdentity(r2);
            throw r6;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.setPrintServiceEnabled(android.content.ComponentName, boolean, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.util.List<android.printservice.recommendation.RecommendationInfo> getPrintServiceRecommendations(int r9) {
            /*
            r8 = this;
            r7 = 0;
            r4 = r8.mContext;
            r5 = "android.permission.READ_PRINT_SERVICE_RECOMMENDATIONS";
            r4.enforceCallingOrSelfPermission(r5, r7);
            r2 = r8.resolveCallingUserEnforcingPermissions(r9);
            r5 = r8.mLock;
            monitor-enter(r5);
            r4 = r8.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002e }
            r6 = r8.getCurrentUserId();	 Catch:{ all -> 0x002e }
            if (r4 == r6) goto L_0x001c;
        L_0x001a:
            monitor-exit(r5);
            return r7;
        L_0x001c:
            r4 = 0;
            r3 = r8.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002e }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r4 = r3.getPrintServiceRecommendations();	 Catch:{ all -> 0x0031 }
            android.os.Binder.restoreCallingIdentity(r0);
            return r4;
        L_0x002e:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x0031:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.getPrintServiceRecommendations(int):java.util.List<android.printservice.recommendation.RecommendationInfo>");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void createPrinterDiscoverySession(android.print.IPrinterDiscoveryObserver r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.IPrinterDiscoveryObserver) r8;
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002a }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x002a }
            if (r4 == r6) goto L_0x0019;
        L_0x0017:
            monitor-exit(r5);
            return;
        L_0x0019:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002a }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.createPrinterDiscoverySession(r8);	 Catch:{ all -> 0x002d }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002a:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x002d:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.createPrinterDiscoverySession(android.print.IPrinterDiscoveryObserver, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void destroyPrinterDiscoverySession(android.print.IPrinterDiscoveryObserver r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.IPrinterDiscoveryObserver) r8;
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002a }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x002a }
            if (r4 == r6) goto L_0x0019;
        L_0x0017:
            monitor-exit(r5);
            return;
        L_0x0019:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002a }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.destroyPrinterDiscoverySession(r8);	 Catch:{ all -> 0x002d }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002a:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x002d:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.destroyPrinterDiscoverySession(android.print.IPrinterDiscoveryObserver, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void startPrinterDiscovery(android.print.IPrinterDiscoveryObserver r8, java.util.List<android.print.PrinterId> r9, int r10) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.IPrinterDiscoveryObserver) r8;
            if (r9 == 0) goto L_0x0011;
        L_0x0008:
            r4 = "PrinterId";
            r9 = com.android.internal.util.Preconditions.checkCollectionElementsNotNull(r9, r4);
            r9 = (java.util.List) r9;
        L_0x0011:
            r2 = r7.resolveCallingUserEnforcingPermissions(r10);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x0035 }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x0035 }
            if (r4 == r6) goto L_0x0024;
        L_0x0022:
            monitor-exit(r5);
            return;
        L_0x0024:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x0035 }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.startPrinterDiscovery(r8, r9);	 Catch:{ all -> 0x0038 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x0035:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x0038:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.startPrinterDiscovery(android.print.IPrinterDiscoveryObserver, java.util.List, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void stopPrinterDiscovery(android.print.IPrinterDiscoveryObserver r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.IPrinterDiscoveryObserver) r8;
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002a }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x002a }
            if (r4 == r6) goto L_0x0019;
        L_0x0017:
            monitor-exit(r5);
            return;
        L_0x0019:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002a }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.stopPrinterDiscovery(r8);	 Catch:{ all -> 0x002d }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002a:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x002d:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.stopPrinterDiscovery(android.print.IPrinterDiscoveryObserver, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void validatePrinters(java.util.List<android.print.PrinterId> r8, int r9) {
            /*
            r7 = this;
            r4 = "PrinterId";
            r8 = com.android.internal.util.Preconditions.checkCollectionElementsNotNull(r8, r4);
            r8 = (java.util.List) r8;
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002d }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x002d }
            if (r4 == r6) goto L_0x001c;
        L_0x001a:
            monitor-exit(r5);
            return;
        L_0x001c:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002d }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.validatePrinters(r8);	 Catch:{ all -> 0x0030 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002d:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x0030:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.validatePrinters(java.util.List, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void startPrinterStateTracking(android.print.PrinterId r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.PrinterId) r8;
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002a }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x002a }
            if (r4 == r6) goto L_0x0019;
        L_0x0017:
            monitor-exit(r5);
            return;
        L_0x0019:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002a }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.startPrinterStateTracking(r8);	 Catch:{ all -> 0x002d }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002a:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x002d:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.startPrinterStateTracking(android.print.PrinterId, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void stopPrinterStateTracking(android.print.PrinterId r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.PrinterId) r8;
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002a }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x002a }
            if (r4 == r6) goto L_0x0019;
        L_0x0017:
            monitor-exit(r5);
            return;
        L_0x0019:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002a }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.stopPrinterStateTracking(r8);	 Catch:{ all -> 0x002d }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002a:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x002d:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.stopPrinterStateTracking(android.print.PrinterId, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void addPrintJobStateChangeListener(android.print.IPrintJobStateChangeListener r9, int r10, int r11) throws android.os.RemoteException {
            /*
            r8 = this;
            r9 = com.android.internal.util.Preconditions.checkNotNull(r9);
            r9 = (android.print.IPrintJobStateChangeListener) r9;
            r3 = r8.resolveCallingUserEnforcingPermissions(r11);
            r6 = r8.mLock;
            monitor-enter(r6);
            r5 = r8.resolveCallingProfileParentLocked(r3);	 Catch:{ all -> 0x002e }
            r7 = r8.getCurrentUserId();	 Catch:{ all -> 0x002e }
            if (r5 == r7) goto L_0x0019;
        L_0x0017:
            monitor-exit(r6);
            return;
        L_0x0019:
            r2 = r8.resolveCallingAppEnforcingPermissions(r10);	 Catch:{ all -> 0x002e }
            r5 = 0;
            r4 = r8.getOrCreateUserStateLocked(r3, r5);	 Catch:{ all -> 0x002e }
            monitor-exit(r6);
            r0 = android.os.Binder.clearCallingIdentity();
            r4.addPrintJobStateChangeListener(r9, r2);	 Catch:{ all -> 0x0031 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002e:
            r5 = move-exception;
            monitor-exit(r6);
            throw r5;
        L_0x0031:
            r5 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r5;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.addPrintJobStateChangeListener(android.print.IPrintJobStateChangeListener, int, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void removePrintJobStateChangeListener(android.print.IPrintJobStateChangeListener r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.IPrintJobStateChangeListener) r8;
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x002a }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x002a }
            if (r4 == r6) goto L_0x0019;
        L_0x0017:
            monitor-exit(r5);
            return;
        L_0x0019:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x002a }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.removePrintJobStateChangeListener(r8);	 Catch:{ all -> 0x002d }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x002a:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x002d:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.removePrintJobStateChangeListener(android.print.IPrintJobStateChangeListener, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void addPrintServicesChangeListener(android.print.IPrintServicesChangeListener r8, int r9) throws android.os.RemoteException {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.IPrintServicesChangeListener) r8;
            r4 = r7.mContext;
            r5 = "android.permission.READ_PRINT_SERVICES";
            r6 = 0;
            r4.enforceCallingOrSelfPermission(r5, r6);
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x0033 }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x0033 }
            if (r4 == r6) goto L_0x0022;
        L_0x0020:
            monitor-exit(r5);
            return;
        L_0x0022:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x0033 }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.addPrintServicesChangeListener(r8);	 Catch:{ all -> 0x0036 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x0033:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x0036:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.addPrintServicesChangeListener(android.print.IPrintServicesChangeListener, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void removePrintServicesChangeListener(android.print.IPrintServicesChangeListener r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.print.IPrintServicesChangeListener) r8;
            r4 = r7.mContext;
            r5 = "android.permission.READ_PRINT_SERVICES";
            r6 = 0;
            r4.enforceCallingOrSelfPermission(r5, r6);
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x0033 }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x0033 }
            if (r4 == r6) goto L_0x0022;
        L_0x0020:
            monitor-exit(r5);
            return;
        L_0x0022:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x0033 }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.removePrintServicesChangeListener(r8);	 Catch:{ all -> 0x0036 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x0033:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x0036:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.removePrintServicesChangeListener(android.print.IPrintServicesChangeListener, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void addPrintServiceRecommendationsChangeListener(android.printservice.recommendation.IRecommendationsChangeListener r8, int r9) throws android.os.RemoteException {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.printservice.recommendation.IRecommendationsChangeListener) r8;
            r4 = r7.mContext;
            r5 = "android.permission.READ_PRINT_SERVICE_RECOMMENDATIONS";
            r6 = 0;
            r4.enforceCallingOrSelfPermission(r5, r6);
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x0033 }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x0033 }
            if (r4 == r6) goto L_0x0022;
        L_0x0020:
            monitor-exit(r5);
            return;
        L_0x0022:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x0033 }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.addPrintServiceRecommendationsChangeListener(r8);	 Catch:{ all -> 0x0036 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x0033:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x0036:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.addPrintServiceRecommendationsChangeListener(android.printservice.recommendation.IRecommendationsChangeListener, int):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void removePrintServiceRecommendationsChangeListener(android.printservice.recommendation.IRecommendationsChangeListener r8, int r9) {
            /*
            r7 = this;
            r8 = com.android.internal.util.Preconditions.checkNotNull(r8);
            r8 = (android.printservice.recommendation.IRecommendationsChangeListener) r8;
            r4 = r7.mContext;
            r5 = "android.permission.READ_PRINT_SERVICE_RECOMMENDATIONS";
            r6 = 0;
            r4.enforceCallingOrSelfPermission(r5, r6);
            r2 = r7.resolveCallingUserEnforcingPermissions(r9);
            r5 = r7.mLock;
            monitor-enter(r5);
            r4 = r7.resolveCallingProfileParentLocked(r2);	 Catch:{ all -> 0x0033 }
            r6 = r7.getCurrentUserId();	 Catch:{ all -> 0x0033 }
            if (r4 == r6) goto L_0x0022;
        L_0x0020:
            monitor-exit(r5);
            return;
        L_0x0022:
            r4 = 0;
            r3 = r7.getOrCreateUserStateLocked(r2, r4);	 Catch:{ all -> 0x0033 }
            monitor-exit(r5);
            r0 = android.os.Binder.clearCallingIdentity();
            r3.removePrintServiceRecommendationsChangeListener(r8);	 Catch:{ all -> 0x0036 }
            android.os.Binder.restoreCallingIdentity(r0);
            return;
        L_0x0033:
            r4 = move-exception;
            monitor-exit(r5);
            throw r4;
        L_0x0036:
            r4 = move-exception;
            android.os.Binder.restoreCallingIdentity(r0);
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.removePrintServiceRecommendationsChangeListener(android.printservice.recommendation.IRecommendationsChangeListener, int):void");
        }

        public void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
            fd = (FileDescriptor) Preconditions.checkNotNull(fd);
            pw = (PrintWriter) Preconditions.checkNotNull(pw);
            if (DumpUtils.checkDumpPermission(this.mContext, PrintManagerService.LOG_TAG, pw)) {
                synchronized (this.mLock) {
                    long identity = Binder.clearCallingIdentity();
                    try {
                        pw.println("PRINT MANAGER STATE (dumpsys print)");
                        int userStateCount = this.mUserStates.size();
                        for (int i = 0; i < userStateCount; i++) {
                            ((UserState) this.mUserStates.valueAt(i)).dump(fd, pw, "");
                            pw.println();
                        }
                        Binder.restoreCallingIdentity(identity);
                    } catch (Throwable th) {
                        Binder.restoreCallingIdentity(identity);
                    }
                }
            }
        }

        private void registerContentObservers() {
            final Uri enabledPrintServicesUri = Secure.getUriFor("disabled_print_services");
            this.mContext.getContentResolver().registerContentObserver(enabledPrintServicesUri, false, new ContentObserver(BackgroundThread.getHandler()) {
                public void onChange(boolean selfChange, Uri uri, int userId) {
                    if (enabledPrintServicesUri.equals(uri)) {
                        synchronized (PrintManagerImpl.this.mLock) {
                            int userCount = PrintManagerImpl.this.mUserStates.size();
                            int i = 0;
                            while (i < userCount) {
                                if (userId == -1 || userId == PrintManagerImpl.this.mUserStates.keyAt(i)) {
                                    ((UserState) PrintManagerImpl.this.mUserStates.valueAt(i)).updateIfNeededLocked();
                                }
                                i++;
                            }
                        }
                    }
                }
            }, -1);
        }

        private void registerBroadcastReceivers() {
            new PackageMonitor() {
                private boolean hasPrintService(String packageName) {
                    Intent intent = new Intent("android.printservice.PrintService");
                    intent.setPackage(packageName);
                    List<ResolveInfo> installedServices = PrintManagerImpl.this.mContext.getPackageManager().queryIntentServicesAsUser(intent, 268435460, getChangingUserId());
                    return installedServices != null ? installedServices.isEmpty() ^ 1 : false;
                }

                private boolean hadPrintService(UserState userState, String packageName) {
                    List<PrintServiceInfo> installedServices = userState.getPrintServices(3);
                    if (installedServices == null) {
                        return false;
                    }
                    int numInstalledServices = installedServices.size();
                    for (int i = 0; i < numInstalledServices; i++) {
                        if (((PrintServiceInfo) installedServices.get(i)).getResolveInfo().serviceInfo.packageName.equals(packageName)) {
                            return true;
                        }
                    }
                    return false;
                }

                public void onPackageModified(String packageName) {
                    if (PrintManagerImpl.this.mUserManager.isUserUnlockingOrUnlocked(getChangingUserId())) {
                        UserState userState = PrintManagerImpl.this.getOrCreateUserStateLocked(getChangingUserId(), false, false);
                        boolean prunePrintServices = false;
                        synchronized (PrintManagerImpl.this.mLock) {
                            if (hadPrintService(userState, packageName) || hasPrintService(packageName)) {
                                userState.updateIfNeededLocked();
                                prunePrintServices = true;
                            }
                        }
                        if (prunePrintServices) {
                            userState.prunePrintServices();
                        }
                    }
                }

                public void onPackageRemoved(String packageName, int uid) {
                    if (PrintManagerImpl.this.mUserManager.isUserUnlockingOrUnlocked(getChangingUserId())) {
                        UserState userState = PrintManagerImpl.this.getOrCreateUserStateLocked(getChangingUserId(), false, false);
                        boolean prunePrintServices = false;
                        synchronized (PrintManagerImpl.this.mLock) {
                            if (hadPrintService(userState, packageName)) {
                                userState.updateIfNeededLocked();
                                prunePrintServices = true;
                            }
                        }
                        if (prunePrintServices) {
                            userState.prunePrintServices();
                        }
                    }
                }

                public boolean onHandleForceStop(Intent intent, String[] stoppedPackages, int uid, boolean doit) {
                    if (!PrintManagerImpl.this.mUserManager.isUserUnlockingOrUnlocked(getChangingUserId())) {
                        return false;
                    }
                    synchronized (PrintManagerImpl.this.mLock) {
                        UserState userState = PrintManagerImpl.this.getOrCreateUserStateLocked(getChangingUserId(), false, false);
                        boolean stoppedSomePackages = false;
                        List<PrintServiceInfo> enabledServices = userState.getPrintServices(1);
                        if (enabledServices == null) {
                            return false;
                        }
                        for (PrintServiceInfo componentName : enabledServices) {
                            String componentPackage = componentName.getComponentName().getPackageName();
                            int i = 0;
                            int length = stoppedPackages.length;
                            while (i < length) {
                                if (!componentPackage.equals(stoppedPackages[i])) {
                                    i++;
                                } else if (doit) {
                                    stoppedSomePackages = true;
                                } else {
                                    return true;
                                }
                            }
                        }
                        if (stoppedSomePackages) {
                            userState.updateIfNeededLocked();
                        }
                        return false;
                    }
                }

                public void onPackageAdded(String packageName, int uid) {
                    if (PrintManagerImpl.this.mUserManager.isUserUnlockingOrUnlocked(getChangingUserId())) {
                        synchronized (PrintManagerImpl.this.mLock) {
                            if (hasPrintService(packageName)) {
                                PrintManagerImpl.this.getOrCreateUserStateLocked(getChangingUserId(), false, false).updateIfNeededLocked();
                            }
                        }
                    }
                }
            }.register(this.mContext, BackgroundThread.getHandler().getLooper(), UserHandle.ALL, true);
        }

        private UserState getOrCreateUserStateLocked(int userId, boolean lowPriority) {
            return getOrCreateUserStateLocked(userId, lowPriority, true);
        }

        private UserState getOrCreateUserStateLocked(int userId, boolean lowPriority, boolean enforceUserUnlockingOrUnlocked) {
            if (!enforceUserUnlockingOrUnlocked || (this.mUserManager.isUserUnlockingOrUnlocked(userId) ^ 1) == 0) {
                UserState userState = (UserState) this.mUserStates.get(userId);
                if (userState == null) {
                    userState = new UserState(this.mContext, userId, this.mLock, lowPriority);
                    this.mUserStates.put(userId, userState);
                }
                if (!lowPriority) {
                    userState.increasePriority();
                }
                return userState;
            }
            throw new IllegalStateException("User " + userId + " must be unlocked for printing to be available");
        }

        private void handleUserUnlocked(final int userId) {
            BackgroundThread.getHandler().post(new Runnable() {
                public void run() {
                    if (PrintManagerImpl.this.mUserManager.isUserUnlockingOrUnlocked(userId)) {
                        UserState userState;
                        synchronized (PrintManagerImpl.this.mLock) {
                            userState = PrintManagerImpl.this.getOrCreateUserStateLocked(userId, true, false);
                            userState.updateIfNeededLocked();
                        }
                        userState.removeObsoletePrintJobs();
                    }
                }
            });
        }

        private void handleUserStopped(final int userId) {
            BackgroundThread.getHandler().post(new Runnable() {
                public void run() {
                    synchronized (PrintManagerImpl.this.mLock) {
                        UserState userState = (UserState) PrintManagerImpl.this.mUserStates.get(userId);
                        if (userState != null) {
                            userState.destroyLocked();
                            PrintManagerImpl.this.mUserStates.remove(userId);
                        }
                    }
                }
            });
        }

        private int resolveCallingProfileParentLocked(int userId) {
            if (userId == getCurrentUserId()) {
                return userId;
            }
            long identity = Binder.clearCallingIdentity();
            try {
                UserInfo parent = this.mUserManager.getProfileParent(userId);
                if (parent != null) {
                    int identifier = parent.getUserHandle().getIdentifier();
                    return identifier;
                }
                Binder.restoreCallingIdentity(identity);
                return -10;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private int resolveCallingAppEnforcingPermissions(int r6) {
            /*
            r5 = this;
            r1 = android.os.Binder.getCallingUid();
            if (r1 != 0) goto L_0x0007;
        L_0x0006:
            return r6;
        L_0x0007:
            r0 = android.os.UserHandle.getAppId(r1);
            if (r6 == r0) goto L_0x0011;
        L_0x000d:
            r2 = 2000; // 0x7d0 float:2.803E-42 double:9.88E-321;
            if (r0 != r2) goto L_0x0012;
        L_0x0011:
            return r6;
        L_0x0012:
            r2 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
            if (r0 == r2) goto L_0x0011;
        L_0x0016:
            r2 = r5.mContext;
            r3 = "com.android.printspooler.permission.ACCESS_ALL_PRINT_JOBS";
            r2 = r2.checkCallingPermission(r3);
            if (r2 == 0) goto L_0x0054;
        L_0x0021:
            r2 = new java.lang.SecurityException;
            r3 = new java.lang.StringBuilder;
            r3.<init>();
            r4 = "Call from app ";
            r3 = r3.append(r4);
            r3 = r3.append(r0);
            r4 = " as app ";
            r3 = r3.append(r4);
            r3 = r3.append(r6);
            r4 = " without com.android.printspooler.permission";
            r3 = r3.append(r4);
            r4 = ".ACCESS_ALL_PRINT_JOBS";
            r3 = r3.append(r4);
            r3 = r3.toString();
            r2.<init>(r3);
            throw r2;
        L_0x0054:
            return r6;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.print.PrintManagerService.PrintManagerImpl.resolveCallingAppEnforcingPermissions(int):int");
        }

        private int resolveCallingUserEnforcingPermissions(int userId) {
            try {
                return ActivityManager.getService().handleIncomingUser(Binder.getCallingPid(), Binder.getCallingUid(), userId, true, true, "", null);
            } catch (RemoteException e) {
                return userId;
            }
        }

        private String resolveCallingPackageNameEnforcingSecurity(String packageName) {
            for (Object equals : this.mContext.getPackageManager().getPackagesForUid(Binder.getCallingUid())) {
                if (packageName.equals(equals)) {
                    return packageName;
                }
            }
            throw new IllegalArgumentException("packageName has to belong to the caller");
        }

        private int getCurrentUserId() {
            long identity = Binder.clearCallingIdentity();
            try {
                int currentUser = ActivityManager.getCurrentUser();
                return currentUser;
            } finally {
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    public PrintManagerService(Context context) {
        super(context);
        this.mPrintManagerImpl = new PrintManagerImpl(context);
    }

    public void onStart() {
        publishBinderService("print", this.mPrintManagerImpl);
    }

    public void onUnlockUser(int userHandle) {
        this.mPrintManagerImpl.handleUserUnlocked(userHandle);
    }

    public void onStopUser(int userHandle) {
        this.mPrintManagerImpl.handleUserStopped(userHandle);
    }
}
