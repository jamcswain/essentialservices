package com.android.server.accounts;

import android.accounts.AccountManagerInternal;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import android.util.Log;
import android.util.PackageUtils;
import android.util.Pair;
import android.util.Slog;
import android.util.Xml;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.content.PackageMonitor;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.XmlUtils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public final class AccountManagerBackupHelper {
    private static final String ATTR_ACCOUNT_SHA_256 = "account-sha-256";
    private static final String ATTR_DIGEST = "digest";
    private static final String ATTR_PACKAGE = "package";
    private static final long PENDING_RESTORE_TIMEOUT_MILLIS = 3600000;
    private static final String TAG = "AccountManagerBackupHelper";
    private static final String TAG_PERMISSION = "permission";
    private static final String TAG_PERMISSIONS = "permissions";
    private final AccountManagerInternal mAccountManagerInternal;
    private final AccountManagerService mAccountManagerService;
    private final Object mLock = new Object();
    @GuardedBy("mLock")
    private Runnable mRestoreCancelCommand;
    @GuardedBy("mLock")
    private RestorePackageMonitor mRestorePackageMonitor;
    @GuardedBy("mLock")
    private List<PendingAppPermission> mRestorePendingAppPermissions;

    private final class CancelRestoreCommand implements Runnable {
        private CancelRestoreCommand() {
        }

        public void run() {
            synchronized (AccountManagerBackupHelper.this.mLock) {
                AccountManagerBackupHelper.this.mRestorePendingAppPermissions = null;
                if (AccountManagerBackupHelper.this.mRestorePackageMonitor != null) {
                    AccountManagerBackupHelper.this.mRestorePackageMonitor.unregister();
                    AccountManagerBackupHelper.this.mRestorePackageMonitor = null;
                }
            }
        }
    }

    private final class PendingAppPermission {
        private final String accountDigest;
        private final String certDigest;
        private final String packageName;
        private final int userId;

        public PendingAppPermission(String accountDigest, String packageName, String certDigest, int userId) {
            this.accountDigest = accountDigest;
            this.packageName = packageName;
            this.certDigest = certDigest;
            this.userId = userId;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean apply(android.content.pm.PackageManager r18) {
            /*
            r17 = this;
            r1 = 0;
            r0 = r17;
            r11 = com.android.server.accounts.AccountManagerBackupHelper.this;
            r11 = r11.mAccountManagerService;
            r0 = r17;
            r12 = r0.userId;
            r3 = r11.getUserAccounts(r12);
            r12 = r3.dbLock;
            monitor-enter(r12);
            r13 = r3.cacheLock;	 Catch:{ all -> 0x0058 }
            monitor-enter(r13);	 Catch:{ all -> 0x0058 }
            r11 = r3.accountCache;	 Catch:{ all -> 0x0055 }
            r11 = r11.values();	 Catch:{ all -> 0x0055 }
            r5 = r11.iterator();	 Catch:{ all -> 0x0055 }
        L_0x0021:
            r11 = r5.hasNext();	 Catch:{ all -> 0x0055 }
            if (r11 == 0) goto L_0x004c;
        L_0x0027:
            r4 = r5.next();	 Catch:{ all -> 0x0055 }
            r4 = (android.accounts.Account[]) r4;	 Catch:{ all -> 0x0055 }
            r11 = 0;
            r14 = r4.length;	 Catch:{ all -> 0x0055 }
        L_0x002f:
            if (r11 >= r14) goto L_0x004a;
        L_0x0031:
            r2 = r4[r11];	 Catch:{ all -> 0x0055 }
            r0 = r17;
            r15 = r0.accountDigest;	 Catch:{ all -> 0x0055 }
            r0 = r2.name;	 Catch:{ all -> 0x0055 }
            r16 = r0;
            r16 = r16.getBytes();	 Catch:{ all -> 0x0055 }
            r16 = android.util.PackageUtils.computeSha256Digest(r16);	 Catch:{ all -> 0x0055 }
            r15 = r15.equals(r16);	 Catch:{ all -> 0x0055 }
            if (r15 == 0) goto L_0x0052;
        L_0x0049:
            r1 = r2;
        L_0x004a:
            if (r1 == 0) goto L_0x0021;
        L_0x004c:
            monitor-exit(r13);	 Catch:{ all -> 0x0058 }
            monitor-exit(r12);
            if (r1 != 0) goto L_0x005b;
        L_0x0050:
            r11 = 0;
            return r11;
        L_0x0052:
            r11 = r11 + 1;
            goto L_0x002f;
        L_0x0055:
            r11 = move-exception;
            monitor-exit(r13);	 Catch:{ all -> 0x0058 }
            throw r11;	 Catch:{ all -> 0x0058 }
        L_0x0058:
            r11 = move-exception;
            monitor-exit(r12);
            throw r11;
        L_0x005b:
            r0 = r17;
            r11 = r0.packageName;	 Catch:{ NameNotFoundException -> 0x0096 }
            r0 = r17;
            r12 = r0.userId;	 Catch:{ NameNotFoundException -> 0x0096 }
            r13 = 64;
            r0 = r18;
            r7 = r0.getPackageInfoAsUser(r11, r13, r12);	 Catch:{ NameNotFoundException -> 0x0096 }
            r11 = r7.signatures;
            r9 = android.util.PackageUtils.computeSignaturesSha256Digests(r11);
            r8 = android.util.PackageUtils.computeSignaturesSha256Digest(r9);
            r0 = r17;
            r11 = r0.certDigest;
            r11 = r11.equals(r8);
            if (r11 != 0) goto L_0x0099;
        L_0x007f:
            r11 = r7.signatures;
            r11 = r11.length;
            r12 = 1;
            if (r11 <= r12) goto L_0x0094;
        L_0x0085:
            r0 = r17;
            r11 = r0.certDigest;
            r12 = 0;
            r12 = r9[r12];
            r11 = r11.equals(r12);
            r11 = r11 ^ 1;
            if (r11 == 0) goto L_0x0099;
        L_0x0094:
            r11 = 0;
            return r11;
        L_0x0096:
            r6 = move-exception;
            r11 = 0;
            return r11;
        L_0x0099:
            r11 = r7.applicationInfo;
            r10 = r11.uid;
            r0 = r17;
            r11 = com.android.server.accounts.AccountManagerBackupHelper.this;
            r11 = r11.mAccountManagerInternal;
            r11 = r11.hasAccountAccess(r1, r10);
            if (r11 != 0) goto L_0x00b9;
        L_0x00ab:
            r0 = r17;
            r11 = com.android.server.accounts.AccountManagerBackupHelper.this;
            r11 = r11.mAccountManagerService;
            r12 = "com.android.AccountManager.ACCOUNT_ACCESS_TOKEN_TYPE";
            r11.grantAppPermission(r1, r12, r10);
        L_0x00b9:
            r11 = 1;
            return r11;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.accounts.AccountManagerBackupHelper.PendingAppPermission.apply(android.content.pm.PackageManager):boolean");
        }
    }

    private final class RestorePackageMonitor extends PackageMonitor {
        private RestorePackageMonitor() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onPackageAdded(java.lang.String r7, int r8) {
            /*
            r6 = this;
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;
            r4 = r3.mLock;
            monitor-enter(r4);
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mRestorePendingAppPermissions;	 Catch:{ all -> 0x005c }
            if (r3 != 0) goto L_0x0011;
        L_0x000f:
            monitor-exit(r4);
            return;
        L_0x0011:
            r3 = android.os.UserHandle.getUserId(r8);	 Catch:{ all -> 0x005c }
            if (r3 == 0) goto L_0x0019;
        L_0x0017:
            monitor-exit(r4);
            return;
        L_0x0019:
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mRestorePendingAppPermissions;	 Catch:{ all -> 0x005c }
            r0 = r3.size();	 Catch:{ all -> 0x005c }
            r1 = r0 + -1;
        L_0x0025:
            if (r1 < 0) goto L_0x005f;
        L_0x0027:
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mRestorePendingAppPermissions;	 Catch:{ all -> 0x005c }
            r2 = r3.get(r1);	 Catch:{ all -> 0x005c }
            r2 = (com.android.server.accounts.AccountManagerBackupHelper.PendingAppPermission) r2;	 Catch:{ all -> 0x005c }
            r3 = r2.packageName;	 Catch:{ all -> 0x005c }
            r3 = r3.equals(r7);	 Catch:{ all -> 0x005c }
            if (r3 != 0) goto L_0x0040;
        L_0x003d:
            r1 = r1 + -1;
            goto L_0x0025;
        L_0x0040:
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mAccountManagerService;	 Catch:{ all -> 0x005c }
            r3 = r3.mContext;	 Catch:{ all -> 0x005c }
            r3 = r3.getPackageManager();	 Catch:{ all -> 0x005c }
            r3 = r2.apply(r3);	 Catch:{ all -> 0x005c }
            if (r3 == 0) goto L_0x003d;
        L_0x0052:
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mRestorePendingAppPermissions;	 Catch:{ all -> 0x005c }
            r3.remove(r1);	 Catch:{ all -> 0x005c }
            goto L_0x003d;
        L_0x005c:
            r3 = move-exception;
            monitor-exit(r4);
            throw r3;
        L_0x005f:
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mRestorePendingAppPermissions;	 Catch:{ all -> 0x005c }
            r3 = r3.isEmpty();	 Catch:{ all -> 0x005c }
            if (r3 == 0) goto L_0x0093;
        L_0x006b:
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mRestoreCancelCommand;	 Catch:{ all -> 0x005c }
            if (r3 == 0) goto L_0x0093;
        L_0x0073:
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mAccountManagerService;	 Catch:{ all -> 0x005c }
            r3 = r3.mHandler;	 Catch:{ all -> 0x005c }
            r5 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r5 = r5.mRestoreCancelCommand;	 Catch:{ all -> 0x005c }
            r3.removeCallbacks(r5);	 Catch:{ all -> 0x005c }
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r3 = r3.mRestoreCancelCommand;	 Catch:{ all -> 0x005c }
            r3.run();	 Catch:{ all -> 0x005c }
            r3 = com.android.server.accounts.AccountManagerBackupHelper.this;	 Catch:{ all -> 0x005c }
            r5 = 0;
            r3.mRestoreCancelCommand = r5;	 Catch:{ all -> 0x005c }
        L_0x0093:
            monitor-exit(r4);
            return;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.accounts.AccountManagerBackupHelper.RestorePackageMonitor.onPackageAdded(java.lang.String, int):void");
        }
    }

    public AccountManagerBackupHelper(AccountManagerService accountManagerService, AccountManagerInternal accountManagerInternal) {
        this.mAccountManagerService = accountManagerService;
        this.mAccountManagerInternal = accountManagerInternal;
    }

    public byte[] backupAccountAccessPermissions(int userId) {
        UserAccounts accounts = this.mAccountManagerService.getUserAccounts(userId);
        synchronized (accounts.dbLock) {
            synchronized (accounts.cacheLock) {
                List<Pair<String, Integer>> allAccountGrants = accounts.accountsDb.findAllAccountGrants();
                if (allAccountGrants.isEmpty()) {
                    return null;
                }
                ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                XmlSerializer serializer = new FastXmlSerializer();
                serializer.setOutput(dataStream, StandardCharsets.UTF_8.name());
                serializer.startDocument(null, Boolean.valueOf(true));
                serializer.startTag(null, TAG_PERMISSIONS);
                PackageManager packageManager = this.mAccountManagerService.mContext.getPackageManager();
                for (Pair<String, Integer> grant : allAccountGrants) {
                    String accountName = grant.first;
                    String[] packageNames = packageManager.getPackagesForUid(((Integer) grant.second).intValue());
                    if (packageNames != null) {
                        for (String packageName : packageNames) {
                            try {
                                try {
                                    String digest = PackageUtils.computeSignaturesSha256Digest(packageManager.getPackageInfoAsUser(packageName, 64, userId).signatures);
                                    if (digest != null) {
                                        serializer.startTag(null, TAG_PERMISSION);
                                        serializer.attribute(null, ATTR_ACCOUNT_SHA_256, PackageUtils.computeSha256Digest(accountName.getBytes()));
                                        serializer.attribute(null, ATTR_PACKAGE, packageName);
                                        serializer.attribute(null, ATTR_DIGEST, digest);
                                        serializer.endTag(null, TAG_PERMISSION);
                                    }
                                } catch (IOException e) {
                                    Log.e(TAG, "Error backing up account access grants", e);
                                    return null;
                                }
                            } catch (NameNotFoundException e2) {
                                Slog.i(TAG, "Skipping backup of account access grant for non-existing package: " + packageName);
                            }
                        }
                        continue;
                    }
                }
                serializer.endTag(null, TAG_PERMISSIONS);
                serializer.endDocument();
                serializer.flush();
                return dataStream.toByteArray();
            }
        }
    }

    public void restoreAccountAccessPermissions(byte[] data, int userId) {
        try {
            AccountManagerBackupHelper accountManagerBackupHelper;
            ByteArrayInputStream dataStream = new ByteArrayInputStream(data);
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(dataStream, StandardCharsets.UTF_8.name());
            PackageManager packageManager = this.mAccountManagerService.mContext.getPackageManager();
            int permissionsOuterDepth = parser.getDepth();
            while (XmlUtils.nextElementWithin(parser, permissionsOuterDepth)) {
                if (TAG_PERMISSIONS.equals(parser.getName())) {
                    int permissionOuterDepth = parser.getDepth();
                    while (XmlUtils.nextElementWithin(parser, permissionOuterDepth)) {
                        if (TAG_PERMISSION.equals(parser.getName())) {
                            String accountDigest = parser.getAttributeValue(null, ATTR_ACCOUNT_SHA_256);
                            if (TextUtils.isEmpty(accountDigest)) {
                                XmlUtils.skipCurrentTag(parser);
                            }
                            String packageName = parser.getAttributeValue(null, ATTR_PACKAGE);
                            if (TextUtils.isEmpty(packageName)) {
                                XmlUtils.skipCurrentTag(parser);
                            }
                            String digest = parser.getAttributeValue(null, ATTR_DIGEST);
                            if (TextUtils.isEmpty(digest)) {
                                XmlUtils.skipCurrentTag(parser);
                            }
                            PendingAppPermission pendingAppPermission = new PendingAppPermission(accountDigest, packageName, digest, userId);
                            if (pendingAppPermission.apply(packageManager)) {
                                continue;
                            } else {
                                synchronized (this.mLock) {
                                    if (this.mRestorePackageMonitor == null) {
                                        accountManagerBackupHelper = this;
                                        this.mRestorePackageMonitor = new RestorePackageMonitor();
                                        this.mRestorePackageMonitor.register(this.mAccountManagerService.mContext, this.mAccountManagerService.mHandler.getLooper(), true);
                                    }
                                    if (this.mRestorePendingAppPermissions == null) {
                                        this.mRestorePendingAppPermissions = new ArrayList();
                                    }
                                    this.mRestorePendingAppPermissions.add(pendingAppPermission);
                                }
                            }
                        }
                    }
                    continue;
                }
            }
            accountManagerBackupHelper = this;
            this.mRestoreCancelCommand = new CancelRestoreCommand();
            this.mAccountManagerService.mHandler.postDelayed(this.mRestoreCancelCommand, PENDING_RESTORE_TIMEOUT_MILLIS);
        } catch (Exception e) {
            Log.e(TAG, "Error restoring app permissions", e);
        }
    }
}
