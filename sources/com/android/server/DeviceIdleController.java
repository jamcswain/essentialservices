package com.android.server;

import android.app.ActivityManager;
import android.app.ActivityManagerInternal;
import android.app.AlarmManager;
import android.app.AlarmManager.OnAlarmListener;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.ContentObserver;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationRequest;
import android.net.INetworkPolicyManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IDeviceIdleController.Stub;
import android.os.IMaintenanceActivityListener;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.PowerManagerInternal;
import android.os.Process;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.os.ShellCallback;
import android.os.ShellCommand;
import android.os.SystemClock;
import android.os.Trace;
import android.os.UserHandle;
import android.provider.Settings.Global;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.KeyValueListParser;
import android.util.MutableLong;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.util.TimeUtils;
import android.util.Xml;
import com.android.internal.app.IBatteryStats;
import com.android.internal.os.AtomicFile;
import com.android.internal.os.BackgroundThread;
import com.android.internal.util.DumpUtils;
import com.android.internal.util.FastXmlSerializer;
import com.android.internal.util.XmlUtils;
import com.android.server.am.BatteryStatsService;
import com.android.server.backup.RefactoredBackupManagerService;
import com.android.server.location.LocationFudger;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class DeviceIdleController extends SystemService implements DeviceIdleCallback {
    private static final boolean COMPRESS_TIME = false;
    private static final boolean DEBUG = false;
    private static final int EVENT_BUFFER_SIZE = 100;
    private static final int EVENT_DEEP_IDLE = 4;
    private static final int EVENT_DEEP_MAINTENANCE = 5;
    private static final int EVENT_LIGHT_IDLE = 2;
    private static final int EVENT_LIGHT_MAINTENANCE = 3;
    private static final int EVENT_NORMAL = 1;
    private static final int EVENT_NULL = 0;
    private static final int LIGHT_STATE_ACTIVE = 0;
    private static final int LIGHT_STATE_IDLE = 4;
    private static final int LIGHT_STATE_IDLE_MAINTENANCE = 6;
    private static final int LIGHT_STATE_INACTIVE = 1;
    private static final int LIGHT_STATE_OVERRIDE = 7;
    private static final int LIGHT_STATE_PRE_IDLE = 3;
    private static final int LIGHT_STATE_WAITING_FOR_NETWORK = 5;
    private static final int MSG_FINISH_IDLE_OP = 8;
    private static final int MSG_REPORT_ACTIVE = 5;
    private static final int MSG_REPORT_IDLE_OFF = 4;
    private static final int MSG_REPORT_IDLE_ON = 2;
    private static final int MSG_REPORT_IDLE_ON_LIGHT = 3;
    private static final int MSG_REPORT_MAINTENANCE_ACTIVITY = 7;
    private static final int MSG_TEMP_APP_WHITELIST_TIMEOUT = 6;
    private static final int MSG_WRITE_CONFIG = 1;
    private static final int STATE_ACTIVE = 0;
    private static final int STATE_IDLE = 5;
    private static final int STATE_IDLE_MAINTENANCE = 6;
    private static final int STATE_IDLE_PENDING = 2;
    private static final int STATE_INACTIVE = 1;
    private static final int STATE_LOCATING = 4;
    private static final int STATE_SENSING = 3;
    private static final String TAG = "DeviceIdleController";
    private int mActiveIdleOpCount;
    private WakeLock mActiveIdleWakeLock;
    private AlarmManager mAlarmManager;
    private boolean mAlarmsActive;
    private AnyMotionDetector mAnyMotionDetector;
    private IBatteryStats mBatteryStats;
    BinderService mBinderService;
    private boolean mCharging;
    public final AtomicFile mConfigFile = new AtomicFile(new File(getSystemDir(), "deviceidle.xml"));
    private ConnectivityService mConnectivityService;
    private Constants mConstants;
    private long mCurIdleBudget;
    private final OnAlarmListener mDeepAlarmListener = new OnAlarmListener() {
        public void onAlarm() {
            synchronized (DeviceIdleController.this) {
                DeviceIdleController.this.stepIdleStateLocked("s:alarm");
            }
        }
    };
    private boolean mDeepEnabled;
    private final int[] mEventCmds = new int[100];
    private final long[] mEventTimes = new long[100];
    private boolean mForceIdle;
    private final LocationListener mGenericLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            synchronized (DeviceIdleController.this) {
                DeviceIdleController.this.receivedGenericLocationLocked(location);
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private WakeLock mGoingIdleWakeLock;
    private final LocationListener mGpsLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            synchronized (DeviceIdleController.this) {
                DeviceIdleController.this.receivedGpsLocationLocked(location);
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    final MyHandler mHandler = new MyHandler(BackgroundThread.getHandler().getLooper());
    private boolean mHasGps;
    private boolean mHasNetworkLocation;
    private Intent mIdleIntent;
    private final BroadcastReceiver mIdleStartedDoneReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.os.action.DEVICE_IDLE_MODE_CHANGED".equals(intent.getAction())) {
                DeviceIdleController.this.mHandler.sendEmptyMessageDelayed(8, DeviceIdleController.this.mConstants.MIN_DEEP_MAINTENANCE_TIME);
            } else {
                DeviceIdleController.this.mHandler.sendEmptyMessageDelayed(8, DeviceIdleController.this.mConstants.MIN_LIGHT_MAINTENANCE_TIME);
            }
        }
    };
    private long mInactiveTimeout;
    private final BroadcastReceiver mInteractivityReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            synchronized (DeviceIdleController.this) {
                DeviceIdleController.this.updateInteractivityLocked();
            }
        }
    };
    private boolean mJobsActive;
    private Location mLastGenericLocation;
    private Location mLastGpsLocation;
    private final OnAlarmListener mLightAlarmListener = new OnAlarmListener() {
        public void onAlarm() {
            synchronized (DeviceIdleController.this) {
                DeviceIdleController.this.stepLightIdleStateLocked("s:alarm");
            }
        }
    };
    private boolean mLightEnabled;
    private Intent mLightIdleIntent;
    private int mLightState;
    private ActivityManagerInternal mLocalActivityManager;
    private com.android.server.AlarmManagerService.LocalService mLocalAlarmManager;
    private PowerManagerInternal mLocalPowerManager;
    private boolean mLocated;
    private boolean mLocating;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;
    private final RemoteCallbackList<IMaintenanceActivityListener> mMaintenanceActivityListeners = new RemoteCallbackList();
    private long mMaintenanceStartTime;
    private final MotionListener mMotionListener = new MotionListener();
    private Sensor mMotionSensor;
    private boolean mNetworkConnected;
    private INetworkPolicyManager mNetworkPolicyManager;
    Runnable mNetworkPolicyTempWhitelistCallback;
    private long mNextAlarmTime;
    private long mNextIdleDelay;
    private long mNextIdlePendingDelay;
    private long mNextLightAlarmTime;
    private long mNextLightIdleDelay;
    private long mNextSensingTimeoutAlarmTime;
    private boolean mNotMoving;
    private PowerManager mPowerManager;
    private int[] mPowerSaveWhitelistAllAppIdArray = new int[0];
    private final SparseBooleanArray mPowerSaveWhitelistAllAppIds = new SparseBooleanArray();
    private final ArrayMap<String, Integer> mPowerSaveWhitelistApps = new ArrayMap();
    private final ArrayMap<String, Integer> mPowerSaveWhitelistAppsExceptIdle = new ArrayMap();
    private int[] mPowerSaveWhitelistExceptIdleAppIdArray = new int[0];
    private final SparseBooleanArray mPowerSaveWhitelistExceptIdleAppIds = new SparseBooleanArray();
    private final SparseBooleanArray mPowerSaveWhitelistSystemAppIds = new SparseBooleanArray();
    private final SparseBooleanArray mPowerSaveWhitelistSystemAppIdsExceptIdle = new SparseBooleanArray();
    private int[] mPowerSaveWhitelistUserAppIdArray = new int[0];
    private final SparseBooleanArray mPowerSaveWhitelistUserAppIds = new SparseBooleanArray();
    private final ArrayMap<String, Integer> mPowerSaveWhitelistUserApps = new ArrayMap();
    private final ArraySet<String> mPowerSaveWhitelistUserAppsExceptIdle = new ArraySet();
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            boolean z = false;
            String action = intent.getAction();
            if (action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                DeviceIdleController.this.updateConnectivityState(intent);
            } else if (action.equals("android.intent.action.BATTERY_CHANGED")) {
                synchronized (DeviceIdleController.this) {
                    int plugged = intent.getIntExtra("plugged", 0);
                    DeviceIdleController deviceIdleController = DeviceIdleController.this;
                    if (plugged != 0) {
                        z = true;
                    }
                    deviceIdleController.updateChargingLocked(z);
                }
            } else if (action.equals("android.intent.action.PACKAGE_REMOVED") && !intent.getBooleanExtra("android.intent.extra.REPLACING", false)) {
                Uri data = intent.getData();
                if (data != null) {
                    String ssp = data.getSchemeSpecificPart();
                    if (ssp != null) {
                        DeviceIdleController.this.removePowerSaveWhitelistAppInternal(ssp);
                    }
                }
            }
        }
    };
    private boolean mReportedMaintenanceActivity;
    private boolean mScreenOn;
    private final OnAlarmListener mSensingTimeoutAlarmListener = new OnAlarmListener() {
        public void onAlarm() {
            if (DeviceIdleController.this.mState == 3) {
                synchronized (DeviceIdleController.this) {
                    DeviceIdleController.this.becomeInactiveIfAppropriateLocked();
                }
            }
        }
    };
    private SensorManager mSensorManager;
    private int mState;
    private int[] mTempWhitelistAppIdArray = new int[0];
    private final SparseArray<Pair<MutableLong, String>> mTempWhitelistAppIdEndTimes = new SparseArray();

    private final class BinderService extends Stub {
        private BinderService() {
        }

        public void addPowerSaveWhitelistApp(String name) {
            DeviceIdleController.this.getContext().enforceCallingOrSelfPermission("android.permission.DEVICE_POWER", null);
            long ident = Binder.clearCallingIdentity();
            try {
                DeviceIdleController.this.addPowerSaveWhitelistAppInternal(name);
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }

        public void removePowerSaveWhitelistApp(String name) {
            DeviceIdleController.this.getContext().enforceCallingOrSelfPermission("android.permission.DEVICE_POWER", null);
            long ident = Binder.clearCallingIdentity();
            try {
                DeviceIdleController.this.removePowerSaveWhitelistAppInternal(name);
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }

        public String[] getSystemPowerWhitelistExceptIdle() {
            return DeviceIdleController.this.getSystemPowerWhitelistExceptIdleInternal();
        }

        public String[] getSystemPowerWhitelist() {
            return DeviceIdleController.this.getSystemPowerWhitelistInternal();
        }

        public String[] getUserPowerWhitelist() {
            return DeviceIdleController.this.getUserPowerWhitelistInternal();
        }

        public String[] getFullPowerWhitelistExceptIdle() {
            return DeviceIdleController.this.getFullPowerWhitelistExceptIdleInternal();
        }

        public String[] getFullPowerWhitelist() {
            return DeviceIdleController.this.getFullPowerWhitelistInternal();
        }

        public int[] getAppIdWhitelistExceptIdle() {
            return DeviceIdleController.this.getAppIdWhitelistExceptIdleInternal();
        }

        public int[] getAppIdWhitelist() {
            return DeviceIdleController.this.getAppIdWhitelistInternal();
        }

        public int[] getAppIdUserWhitelist() {
            return DeviceIdleController.this.getAppIdUserWhitelistInternal();
        }

        public int[] getAppIdTempWhitelist() {
            return DeviceIdleController.this.getAppIdTempWhitelistInternal();
        }

        public boolean isPowerSaveWhitelistExceptIdleApp(String name) {
            return DeviceIdleController.this.isPowerSaveWhitelistExceptIdleAppInternal(name);
        }

        public boolean isPowerSaveWhitelistApp(String name) {
            return DeviceIdleController.this.isPowerSaveWhitelistAppInternal(name);
        }

        public void addPowerSaveTempWhitelistApp(String packageName, long duration, int userId, String reason) throws RemoteException {
            DeviceIdleController.this.addPowerSaveTempWhitelistAppChecked(packageName, duration, userId, reason);
        }

        public long addPowerSaveTempWhitelistAppForMms(String packageName, int userId, String reason) throws RemoteException {
            long duration = DeviceIdleController.this.mConstants.MMS_TEMP_APP_WHITELIST_DURATION;
            DeviceIdleController.this.addPowerSaveTempWhitelistAppChecked(packageName, duration, userId, reason);
            return duration;
        }

        public long addPowerSaveTempWhitelistAppForSms(String packageName, int userId, String reason) throws RemoteException {
            long duration = DeviceIdleController.this.mConstants.SMS_TEMP_APP_WHITELIST_DURATION;
            DeviceIdleController.this.addPowerSaveTempWhitelistAppChecked(packageName, duration, userId, reason);
            return duration;
        }

        public void exitIdle(String reason) {
            DeviceIdleController.this.getContext().enforceCallingOrSelfPermission("android.permission.DEVICE_POWER", null);
            long ident = Binder.clearCallingIdentity();
            try {
                DeviceIdleController.this.exitIdleInternal(reason);
            } finally {
                Binder.restoreCallingIdentity(ident);
            }
        }

        public boolean registerMaintenanceActivityListener(IMaintenanceActivityListener listener) {
            return DeviceIdleController.this.registerMaintenanceActivityListener(listener);
        }

        public void unregisterMaintenanceActivityListener(IMaintenanceActivityListener listener) {
            DeviceIdleController.this.unregisterMaintenanceActivityListener(listener);
        }

        protected void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
            DeviceIdleController.this.dump(fd, pw, args);
        }

        public void onShellCommand(FileDescriptor in, FileDescriptor out, FileDescriptor err, String[] args, ShellCallback callback, ResultReceiver resultReceiver) {
            new Shell().exec(this, in, out, err, args, callback, resultReceiver);
        }
    }

    private final class Constants extends ContentObserver {
        private static final String KEY_IDLE_AFTER_INACTIVE_TIMEOUT = "idle_after_inactive_to";
        private static final String KEY_IDLE_FACTOR = "idle_factor";
        private static final String KEY_IDLE_PENDING_FACTOR = "idle_pending_factor";
        private static final String KEY_IDLE_PENDING_TIMEOUT = "idle_pending_to";
        private static final String KEY_IDLE_TIMEOUT = "idle_to";
        private static final String KEY_INACTIVE_TIMEOUT = "inactive_to";
        private static final String KEY_LIGHT_IDLE_AFTER_INACTIVE_TIMEOUT = "light_after_inactive_to";
        private static final String KEY_LIGHT_IDLE_FACTOR = "light_idle_factor";
        private static final String KEY_LIGHT_IDLE_MAINTENANCE_MAX_BUDGET = "light_idle_maintenance_max_budget";
        private static final String KEY_LIGHT_IDLE_MAINTENANCE_MIN_BUDGET = "light_idle_maintenance_min_budget";
        private static final String KEY_LIGHT_IDLE_TIMEOUT = "light_idle_to";
        private static final String KEY_LIGHT_MAX_IDLE_TIMEOUT = "light_max_idle_to";
        private static final String KEY_LIGHT_PRE_IDLE_TIMEOUT = "light_pre_idle_to";
        private static final String KEY_LOCATING_TIMEOUT = "locating_to";
        private static final String KEY_LOCATION_ACCURACY = "location_accuracy";
        private static final String KEY_MAX_IDLE_PENDING_TIMEOUT = "max_idle_pending_to";
        private static final String KEY_MAX_IDLE_TIMEOUT = "max_idle_to";
        private static final String KEY_MAX_TEMP_APP_WHITELIST_DURATION = "max_temp_app_whitelist_duration";
        private static final String KEY_MIN_DEEP_MAINTENANCE_TIME = "min_deep_maintenance_time";
        private static final String KEY_MIN_LIGHT_MAINTENANCE_TIME = "min_light_maintenance_time";
        private static final String KEY_MIN_TIME_TO_ALARM = "min_time_to_alarm";
        private static final String KEY_MMS_TEMP_APP_WHITELIST_DURATION = "mms_temp_app_whitelist_duration";
        private static final String KEY_MOTION_INACTIVE_TIMEOUT = "motion_inactive_to";
        private static final String KEY_NOTIFICATION_WHITELIST_DURATION = "notification_whitelist_duration";
        private static final String KEY_SENSING_TIMEOUT = "sensing_to";
        private static final String KEY_SMS_TEMP_APP_WHITELIST_DURATION = "sms_temp_app_whitelist_duration";
        public long IDLE_AFTER_INACTIVE_TIMEOUT;
        public float IDLE_FACTOR;
        public float IDLE_PENDING_FACTOR;
        public long IDLE_PENDING_TIMEOUT;
        public long IDLE_TIMEOUT;
        public long INACTIVE_TIMEOUT;
        public long LIGHT_IDLE_AFTER_INACTIVE_TIMEOUT;
        public float LIGHT_IDLE_FACTOR;
        public long LIGHT_IDLE_MAINTENANCE_MAX_BUDGET;
        public long LIGHT_IDLE_MAINTENANCE_MIN_BUDGET;
        public long LIGHT_IDLE_TIMEOUT;
        public long LIGHT_MAX_IDLE_TIMEOUT;
        public long LIGHT_PRE_IDLE_TIMEOUT;
        public long LOCATING_TIMEOUT;
        public float LOCATION_ACCURACY;
        public long MAX_IDLE_PENDING_TIMEOUT;
        public long MAX_IDLE_TIMEOUT;
        public long MAX_TEMP_APP_WHITELIST_DURATION;
        public long MIN_DEEP_MAINTENANCE_TIME;
        public long MIN_LIGHT_MAINTENANCE_TIME;
        public long MIN_TIME_TO_ALARM;
        public long MMS_TEMP_APP_WHITELIST_DURATION;
        public long MOTION_INACTIVE_TIMEOUT;
        public long NOTIFICATION_WHITELIST_DURATION;
        public long SENSING_TIMEOUT;
        public long SMS_TEMP_APP_WHITELIST_DURATION;
        private final boolean mHasWatch;
        private final KeyValueListParser mParser = new KeyValueListParser(',');
        private final ContentResolver mResolver;

        public Constants(Handler handler, ContentResolver resolver) {
            String str;
            super(handler);
            this.mResolver = resolver;
            this.mHasWatch = DeviceIdleController.this.getContext().getPackageManager().hasSystemFeature("android.hardware.type.watch");
            ContentResolver contentResolver = this.mResolver;
            if (this.mHasWatch) {
                str = "device_idle_constants_watch";
            } else {
                str = "device_idle_constants";
            }
            contentResolver.registerContentObserver(Global.getUriFor(str), false, this);
            updateConstants();
        }

        public void onChange(boolean selfChange, Uri uri) {
            updateConstants();
        }

        private void updateConstants() {
            int i = 15;
            synchronized (DeviceIdleController.this) {
                try {
                    String str;
                    KeyValueListParser keyValueListParser = this.mParser;
                    ContentResolver contentResolver = this.mResolver;
                    if (this.mHasWatch) {
                        str = "device_idle_constants_watch";
                    } else {
                        str = "device_idle_constants";
                    }
                    keyValueListParser.setString(Global.getString(contentResolver, str));
                } catch (IllegalArgumentException e) {
                    Slog.e(DeviceIdleController.TAG, "Bad device idle settings", e);
                }
                this.LIGHT_IDLE_AFTER_INACTIVE_TIMEOUT = this.mParser.getLong(KEY_LIGHT_IDLE_AFTER_INACTIVE_TIMEOUT, RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL);
                this.LIGHT_PRE_IDLE_TIMEOUT = this.mParser.getLong(KEY_LIGHT_PRE_IDLE_TIMEOUT, LocationFudger.FASTEST_INTERVAL_MS);
                this.LIGHT_IDLE_TIMEOUT = this.mParser.getLong(KEY_LIGHT_IDLE_TIMEOUT, RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL);
                this.LIGHT_IDLE_FACTOR = this.mParser.getFloat(KEY_LIGHT_IDLE_FACTOR, 2.0f);
                this.LIGHT_MAX_IDLE_TIMEOUT = this.mParser.getLong(KEY_LIGHT_MAX_IDLE_TIMEOUT, 900000);
                this.LIGHT_IDLE_MAINTENANCE_MIN_BUDGET = this.mParser.getLong(KEY_LIGHT_IDLE_MAINTENANCE_MIN_BUDGET, 60000);
                this.LIGHT_IDLE_MAINTENANCE_MAX_BUDGET = this.mParser.getLong(KEY_LIGHT_IDLE_MAINTENANCE_MAX_BUDGET, RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL);
                this.MIN_LIGHT_MAINTENANCE_TIME = this.mParser.getLong(KEY_MIN_LIGHT_MAINTENANCE_TIME, 5000);
                this.MIN_DEEP_MAINTENANCE_TIME = this.mParser.getLong(KEY_MIN_DEEP_MAINTENANCE_TIME, 30000);
                this.INACTIVE_TIMEOUT = this.mParser.getLong(KEY_INACTIVE_TIMEOUT, ((long) ((this.mHasWatch ? 15 : 30) * 60)) * 1000);
                this.SENSING_TIMEOUT = this.mParser.getLong(KEY_SENSING_TIMEOUT, 240000);
                this.LOCATING_TIMEOUT = this.mParser.getLong(KEY_LOCATING_TIMEOUT, 30000);
                this.LOCATION_ACCURACY = this.mParser.getFloat(KEY_LOCATION_ACCURACY, 20.0f);
                this.MOTION_INACTIVE_TIMEOUT = this.mParser.getLong(KEY_MOTION_INACTIVE_TIMEOUT, LocationFudger.FASTEST_INTERVAL_MS);
                if (!this.mHasWatch) {
                    i = 30;
                }
                this.IDLE_AFTER_INACTIVE_TIMEOUT = this.mParser.getLong(KEY_IDLE_AFTER_INACTIVE_TIMEOUT, ((long) (i * 60)) * 1000);
                this.IDLE_PENDING_TIMEOUT = this.mParser.getLong(KEY_IDLE_PENDING_TIMEOUT, RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL);
                this.MAX_IDLE_PENDING_TIMEOUT = this.mParser.getLong(KEY_MAX_IDLE_PENDING_TIMEOUT, LocationFudger.FASTEST_INTERVAL_MS);
                this.IDLE_PENDING_FACTOR = this.mParser.getFloat(KEY_IDLE_PENDING_FACTOR, 2.0f);
                this.IDLE_TIMEOUT = this.mParser.getLong(KEY_IDLE_TIMEOUT, 3600000);
                this.MAX_IDLE_TIMEOUT = this.mParser.getLong(KEY_MAX_IDLE_TIMEOUT, 21600000);
                this.IDLE_FACTOR = this.mParser.getFloat(KEY_IDLE_FACTOR, 2.0f);
                this.MIN_TIME_TO_ALARM = this.mParser.getLong(KEY_MIN_TIME_TO_ALARM, 3600000);
                this.MAX_TEMP_APP_WHITELIST_DURATION = this.mParser.getLong(KEY_MAX_TEMP_APP_WHITELIST_DURATION, RefactoredBackupManagerService.TIMEOUT_FULL_BACKUP_INTERVAL);
                this.MMS_TEMP_APP_WHITELIST_DURATION = this.mParser.getLong(KEY_MMS_TEMP_APP_WHITELIST_DURATION, 60000);
                this.SMS_TEMP_APP_WHITELIST_DURATION = this.mParser.getLong(KEY_SMS_TEMP_APP_WHITELIST_DURATION, 20000);
                this.NOTIFICATION_WHITELIST_DURATION = this.mParser.getLong(KEY_NOTIFICATION_WHITELIST_DURATION, 30000);
            }
        }

        void dump(PrintWriter pw) {
            pw.println("  Settings:");
            pw.print("    ");
            pw.print(KEY_LIGHT_IDLE_AFTER_INACTIVE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.LIGHT_IDLE_AFTER_INACTIVE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_LIGHT_PRE_IDLE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.LIGHT_PRE_IDLE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_LIGHT_IDLE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.LIGHT_IDLE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_LIGHT_IDLE_FACTOR);
            pw.print("=");
            pw.print(this.LIGHT_IDLE_FACTOR);
            pw.println();
            pw.print("    ");
            pw.print(KEY_LIGHT_MAX_IDLE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.LIGHT_MAX_IDLE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_LIGHT_IDLE_MAINTENANCE_MIN_BUDGET);
            pw.print("=");
            TimeUtils.formatDuration(this.LIGHT_IDLE_MAINTENANCE_MIN_BUDGET, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_LIGHT_IDLE_MAINTENANCE_MAX_BUDGET);
            pw.print("=");
            TimeUtils.formatDuration(this.LIGHT_IDLE_MAINTENANCE_MAX_BUDGET, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_MIN_LIGHT_MAINTENANCE_TIME);
            pw.print("=");
            TimeUtils.formatDuration(this.MIN_LIGHT_MAINTENANCE_TIME, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_MIN_DEEP_MAINTENANCE_TIME);
            pw.print("=");
            TimeUtils.formatDuration(this.MIN_DEEP_MAINTENANCE_TIME, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_INACTIVE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.INACTIVE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_SENSING_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.SENSING_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_LOCATING_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.LOCATING_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_LOCATION_ACCURACY);
            pw.print("=");
            pw.print(this.LOCATION_ACCURACY);
            pw.print("m");
            pw.println();
            pw.print("    ");
            pw.print(KEY_MOTION_INACTIVE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.MOTION_INACTIVE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_IDLE_AFTER_INACTIVE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.IDLE_AFTER_INACTIVE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_IDLE_PENDING_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.IDLE_PENDING_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_MAX_IDLE_PENDING_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.MAX_IDLE_PENDING_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_IDLE_PENDING_FACTOR);
            pw.print("=");
            pw.println(this.IDLE_PENDING_FACTOR);
            pw.print("    ");
            pw.print(KEY_IDLE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.IDLE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_MAX_IDLE_TIMEOUT);
            pw.print("=");
            TimeUtils.formatDuration(this.MAX_IDLE_TIMEOUT, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_IDLE_FACTOR);
            pw.print("=");
            pw.println(this.IDLE_FACTOR);
            pw.print("    ");
            pw.print(KEY_MIN_TIME_TO_ALARM);
            pw.print("=");
            TimeUtils.formatDuration(this.MIN_TIME_TO_ALARM, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_MAX_TEMP_APP_WHITELIST_DURATION);
            pw.print("=");
            TimeUtils.formatDuration(this.MAX_TEMP_APP_WHITELIST_DURATION, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_MMS_TEMP_APP_WHITELIST_DURATION);
            pw.print("=");
            TimeUtils.formatDuration(this.MMS_TEMP_APP_WHITELIST_DURATION, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_SMS_TEMP_APP_WHITELIST_DURATION);
            pw.print("=");
            TimeUtils.formatDuration(this.SMS_TEMP_APP_WHITELIST_DURATION, pw);
            pw.println();
            pw.print("    ");
            pw.print(KEY_NOTIFICATION_WHITELIST_DURATION);
            pw.print("=");
            TimeUtils.formatDuration(this.NOTIFICATION_WHITELIST_DURATION, pw);
            pw.println();
        }
    }

    public class LocalService {
        public void addPowerSaveTempWhitelistApp(int callingUid, String packageName, long duration, int userId, boolean sync, String reason) {
            DeviceIdleController.this.addPowerSaveTempWhitelistAppInternal(callingUid, packageName, duration, userId, sync, reason);
        }

        public void addPowerSaveTempWhitelistAppDirect(int appId, long duration, boolean sync, String reason) {
            DeviceIdleController.this.addPowerSaveTempWhitelistAppDirectInternal(0, appId, duration, sync, reason);
        }

        public long getNotificationWhitelistDuration() {
            return DeviceIdleController.this.mConstants.NOTIFICATION_WHITELIST_DURATION;
        }

        public void setNetworkPolicyTempWhitelistCallback(Runnable callback) {
            DeviceIdleController.this.setNetworkPolicyTempWhitelistCallbackInternal(callback);
        }

        public void setJobsActive(boolean active) {
            DeviceIdleController.this.setJobsActive(active);
        }

        public void setAlarmsActive(boolean active) {
            DeviceIdleController.this.setAlarmsActive(active);
        }

        public boolean isAppOnWhitelist(int appid) {
            return DeviceIdleController.this.isAppOnWhitelistInternal(appid);
        }

        public int[] getPowerSaveWhitelistUserAppIds() {
            return DeviceIdleController.this.getPowerSaveWhitelistUserAppIds();
        }
    }

    private final class MotionListener extends TriggerEventListener implements SensorEventListener {
        boolean active;

        private MotionListener() {
            this.active = false;
        }

        public void onTrigger(TriggerEvent event) {
            synchronized (DeviceIdleController.this) {
                this.active = false;
                DeviceIdleController.this.motionLocked();
            }
        }

        public void onSensorChanged(SensorEvent event) {
            synchronized (DeviceIdleController.this) {
                DeviceIdleController.this.mSensorManager.unregisterListener(this, DeviceIdleController.this.mMotionSensor);
                this.active = false;
                DeviceIdleController.this.motionLocked();
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public boolean registerLocked() {
            boolean success;
            if (DeviceIdleController.this.mMotionSensor.getReportingMode() == 2) {
                success = DeviceIdleController.this.mSensorManager.requestTriggerSensor(DeviceIdleController.this.mMotionListener, DeviceIdleController.this.mMotionSensor);
            } else {
                success = DeviceIdleController.this.mSensorManager.registerListener(DeviceIdleController.this.mMotionListener, DeviceIdleController.this.mMotionSensor, 3);
            }
            if (success) {
                this.active = true;
            } else {
                Slog.e(DeviceIdleController.TAG, "Unable to register for " + DeviceIdleController.this.mMotionSensor);
            }
            return success;
        }

        public void unregisterLocked() {
            if (DeviceIdleController.this.mMotionSensor.getReportingMode() == 2) {
                DeviceIdleController.this.mSensorManager.cancelTriggerSensor(DeviceIdleController.this.mMotionListener, DeviceIdleController.this.mMotionSensor);
            } else {
                DeviceIdleController.this.mSensorManager.unregisterListener(DeviceIdleController.this.mMotionListener);
            }
            this.active = false;
        }
    }

    final class MyHandler extends Handler {
        MyHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            boolean deepChanged;
            boolean lightChanged;
            switch (msg.what) {
                case 1:
                    DeviceIdleController.this.handleWriteConfigFile();
                    return;
                case 2:
                case 3:
                    EventLogTags.writeDeviceIdleOnStart();
                    if (msg.what == 2) {
                        deepChanged = DeviceIdleController.this.mLocalPowerManager.setDeviceIdleMode(true);
                        lightChanged = DeviceIdleController.this.mLocalPowerManager.setLightDeviceIdleMode(false);
                    } else {
                        deepChanged = DeviceIdleController.this.mLocalPowerManager.setDeviceIdleMode(false);
                        lightChanged = DeviceIdleController.this.mLocalPowerManager.setLightDeviceIdleMode(true);
                    }
                    try {
                        int i;
                        DeviceIdleController.this.mNetworkPolicyManager.setDeviceIdleMode(true);
                        IBatteryStats -get0 = DeviceIdleController.this.mBatteryStats;
                        if (msg.what == 2) {
                            i = 2;
                        } else {
                            i = 1;
                        }
                        -get0.noteDeviceIdleMode(i, null, Process.myUid());
                    } catch (RemoteException e) {
                    }
                    if (deepChanged) {
                        DeviceIdleController.this.getContext().sendBroadcastAsUser(DeviceIdleController.this.mIdleIntent, UserHandle.ALL);
                    }
                    if (lightChanged) {
                        DeviceIdleController.this.getContext().sendBroadcastAsUser(DeviceIdleController.this.mLightIdleIntent, UserHandle.ALL);
                    }
                    EventLogTags.writeDeviceIdleOnComplete();
                    DeviceIdleController.this.mGoingIdleWakeLock.release();
                    return;
                case 4:
                    EventLogTags.writeDeviceIdleOffStart(Shell.NIGHT_MODE_STR_UNKNOWN);
                    deepChanged = DeviceIdleController.this.mLocalPowerManager.setDeviceIdleMode(false);
                    lightChanged = DeviceIdleController.this.mLocalPowerManager.setLightDeviceIdleMode(false);
                    try {
                        DeviceIdleController.this.mNetworkPolicyManager.setDeviceIdleMode(false);
                        DeviceIdleController.this.mBatteryStats.noteDeviceIdleMode(0, null, Process.myUid());
                    } catch (RemoteException e2) {
                    }
                    if (deepChanged) {
                        DeviceIdleController.this.incActiveIdleOps();
                        DeviceIdleController.this.getContext().sendOrderedBroadcastAsUser(DeviceIdleController.this.mIdleIntent, UserHandle.ALL, null, DeviceIdleController.this.mIdleStartedDoneReceiver, null, 0, null, null);
                    }
                    if (lightChanged) {
                        DeviceIdleController.this.incActiveIdleOps();
                        DeviceIdleController.this.getContext().sendOrderedBroadcastAsUser(DeviceIdleController.this.mLightIdleIntent, UserHandle.ALL, null, DeviceIdleController.this.mIdleStartedDoneReceiver, null, 0, null, null);
                    }
                    DeviceIdleController.this.decActiveIdleOps();
                    EventLogTags.writeDeviceIdleOffComplete();
                    return;
                case 5:
                    String activeReason = msg.obj;
                    int activeUid = msg.arg1;
                    EventLogTags.writeDeviceIdleOffStart(activeReason != null ? activeReason : Shell.NIGHT_MODE_STR_UNKNOWN);
                    deepChanged = DeviceIdleController.this.mLocalPowerManager.setDeviceIdleMode(false);
                    lightChanged = DeviceIdleController.this.mLocalPowerManager.setLightDeviceIdleMode(false);
                    try {
                        DeviceIdleController.this.mNetworkPolicyManager.setDeviceIdleMode(false);
                        DeviceIdleController.this.mBatteryStats.noteDeviceIdleMode(0, activeReason, activeUid);
                    } catch (RemoteException e3) {
                    }
                    if (deepChanged) {
                        DeviceIdleController.this.getContext().sendBroadcastAsUser(DeviceIdleController.this.mIdleIntent, UserHandle.ALL);
                    }
                    if (lightChanged) {
                        DeviceIdleController.this.getContext().sendBroadcastAsUser(DeviceIdleController.this.mLightIdleIntent, UserHandle.ALL);
                    }
                    EventLogTags.writeDeviceIdleOffComplete();
                    return;
                case 6:
                    DeviceIdleController.this.checkTempAppWhitelistTimeout(msg.arg1);
                    return;
                case 7:
                    boolean active = msg.arg1 == 1;
                    int size = DeviceIdleController.this.mMaintenanceActivityListeners.beginBroadcast();
                    for (int i2 = 0; i2 < size; i2++) {
                        try {
                            ((IMaintenanceActivityListener) DeviceIdleController.this.mMaintenanceActivityListeners.getBroadcastItem(i2)).onMaintenanceActivityChanged(active);
                        } catch (RemoteException e4) {
                        } catch (Throwable th) {
                            DeviceIdleController.this.mMaintenanceActivityListeners.finishBroadcast();
                        }
                    }
                    DeviceIdleController.this.mMaintenanceActivityListeners.finishBroadcast();
                    return;
                case 8:
                    DeviceIdleController.this.decActiveIdleOps();
                    return;
                default:
                    return;
            }
        }
    }

    class Shell extends ShellCommand {
        int userId = 0;

        Shell() {
        }

        public int onCommand(String cmd) {
            return DeviceIdleController.this.onShellCommand(this, cmd);
        }

        public void onHelp() {
            DeviceIdleController.dumpHelp(getOutPrintWriter());
        }
    }

    private static String stateToString(int state) {
        switch (state) {
            case 0:
                return "ACTIVE";
            case 1:
                return "INACTIVE";
            case 2:
                return "IDLE_PENDING";
            case 3:
                return "SENSING";
            case 4:
                return "LOCATING";
            case 5:
                return "IDLE";
            case 6:
                return "IDLE_MAINTENANCE";
            default:
                return Integer.toString(state);
        }
    }

    private static String lightStateToString(int state) {
        switch (state) {
            case 0:
                return "ACTIVE";
            case 1:
                return "INACTIVE";
            case 3:
                return "PRE_IDLE";
            case 4:
                return "IDLE";
            case 5:
                return "WAITING_FOR_NETWORK";
            case 6:
                return "IDLE_MAINTENANCE";
            case 7:
                return "OVERRIDE";
            default:
                return Integer.toString(state);
        }
    }

    private void addEvent(int cmd) {
        if (this.mEventCmds[0] != cmd) {
            System.arraycopy(this.mEventCmds, 0, this.mEventCmds, 1, 99);
            System.arraycopy(this.mEventTimes, 0, this.mEventTimes, 1, 99);
            this.mEventCmds[0] = cmd;
            this.mEventTimes[0] = SystemClock.elapsedRealtime();
        }
    }

    public void onAnyMotionResult(int result) {
        if (result != -1) {
            synchronized (this) {
                cancelSensingTimeoutAlarmLocked();
            }
        }
        if (result == 1 || result == -1) {
            synchronized (this) {
                handleMotionDetectedLocked(this.mConstants.INACTIVE_TIMEOUT, "non_stationary");
            }
        } else if (result != 0) {
            return;
        } else {
            if (this.mState == 3) {
                synchronized (this) {
                    this.mNotMoving = true;
                    stepIdleStateLocked("s:stationary");
                }
            } else if (this.mState == 4) {
                synchronized (this) {
                    this.mNotMoving = true;
                    if (this.mLocated) {
                        stepIdleStateLocked("s:stationary");
                    }
                }
            } else {
                return;
            }
        }
    }

    public DeviceIdleController(Context context) {
        super(context);
    }

    boolean isAppOnWhitelistInternal(int appid) {
        boolean z = false;
        synchronized (this) {
            if (Arrays.binarySearch(this.mPowerSaveWhitelistAllAppIdArray, appid) >= 0) {
                z = true;
            }
        }
        return z;
    }

    int[] getPowerSaveWhitelistUserAppIds() {
        int[] iArr;
        synchronized (this) {
            iArr = this.mPowerSaveWhitelistUserAppIdArray;
        }
        return iArr;
    }

    private static File getSystemDir() {
        return new File(Environment.getDataDirectory(), "system");
    }

    public void onStart() {
        PackageManager pm = getContext().getPackageManager();
        synchronized (this) {
            int i;
            boolean z = getContext().getResources().getBoolean(17956944);
            this.mDeepEnabled = z;
            this.mLightEnabled = z;
            SystemConfig sysConfig = SystemConfig.getInstance();
            ArraySet<String> allowPowerExceptIdle = sysConfig.getAllowInPowerSaveExceptIdle();
            for (i = 0; i < allowPowerExceptIdle.size(); i++) {
                try {
                    ApplicationInfo ai = pm.getApplicationInfo((String) allowPowerExceptIdle.valueAt(i), DumpState.DUMP_DEXOPT);
                    int appid = UserHandle.getAppId(ai.uid);
                    this.mPowerSaveWhitelistAppsExceptIdle.put(ai.packageName, Integer.valueOf(appid));
                    this.mPowerSaveWhitelistSystemAppIdsExceptIdle.put(appid, true);
                } catch (NameNotFoundException e) {
                }
            }
            ArraySet<String> allowPower = sysConfig.getAllowInPowerSave();
            for (i = 0; i < allowPower.size(); i++) {
                try {
                    ai = pm.getApplicationInfo((String) allowPower.valueAt(i), DumpState.DUMP_DEXOPT);
                    appid = UserHandle.getAppId(ai.uid);
                    this.mPowerSaveWhitelistAppsExceptIdle.put(ai.packageName, Integer.valueOf(appid));
                    this.mPowerSaveWhitelistSystemAppIdsExceptIdle.put(appid, true);
                    this.mPowerSaveWhitelistApps.put(ai.packageName, Integer.valueOf(appid));
                    this.mPowerSaveWhitelistSystemAppIds.put(appid, true);
                } catch (NameNotFoundException e2) {
                }
            }
            this.mConstants = new Constants(this.mHandler, getContext().getContentResolver());
            readConfigFileLocked();
            updateWhitelistAppIdsLocked();
            this.mNetworkConnected = true;
            this.mScreenOn = true;
            this.mCharging = true;
            this.mState = 0;
            this.mLightState = 0;
            this.mInactiveTimeout = this.mConstants.INACTIVE_TIMEOUT;
        }
        this.mBinderService = new BinderService();
        publishBinderService("deviceidle", this.mBinderService);
        publishLocalService(LocalService.class, new LocalService());
    }

    public void onBootPhase(int phase) {
        if (phase == SystemService.PHASE_SYSTEM_SERVICES_READY) {
            synchronized (this) {
                this.mAlarmManager = (AlarmManager) getContext().getSystemService("alarm");
                this.mBatteryStats = BatteryStatsService.getService();
                this.mLocalActivityManager = (ActivityManagerInternal) getLocalService(ActivityManagerInternal.class);
                this.mLocalPowerManager = (PowerManagerInternal) getLocalService(PowerManagerInternal.class);
                this.mPowerManager = (PowerManager) getContext().getSystemService(PowerManager.class);
                this.mActiveIdleWakeLock = this.mPowerManager.newWakeLock(1, "deviceidle_maint");
                this.mActiveIdleWakeLock.setReferenceCounted(false);
                this.mGoingIdleWakeLock = this.mPowerManager.newWakeLock(1, "deviceidle_going_idle");
                this.mGoingIdleWakeLock.setReferenceCounted(true);
                this.mConnectivityService = (ConnectivityService) ServiceManager.getService("connectivity");
                this.mLocalAlarmManager = (com.android.server.AlarmManagerService.LocalService) getLocalService(com.android.server.AlarmManagerService.LocalService.class);
                this.mNetworkPolicyManager = INetworkPolicyManager.Stub.asInterface(ServiceManager.getService("netpolicy"));
                this.mSensorManager = (SensorManager) getContext().getSystemService("sensor");
                int sigMotionSensorId = getContext().getResources().getInteger(17694735);
                if (sigMotionSensorId > 0) {
                    this.mMotionSensor = this.mSensorManager.getDefaultSensor(sigMotionSensorId, true);
                }
                if (this.mMotionSensor == null && getContext().getResources().getBoolean(17956891)) {
                    this.mMotionSensor = this.mSensorManager.getDefaultSensor(26, true);
                }
                if (this.mMotionSensor == null) {
                    this.mMotionSensor = this.mSensorManager.getDefaultSensor(17, true);
                }
                if (getContext().getResources().getBoolean(17956892)) {
                    this.mLocationManager = (LocationManager) getContext().getSystemService("location");
                    this.mLocationRequest = new LocationRequest().setQuality(100).setInterval(0).setFastestInterval(0).setNumUpdates(1);
                }
                this.mAnyMotionDetector = new AnyMotionDetector((PowerManager) getContext().getSystemService("power"), this.mHandler, this.mSensorManager, this, ((float) getContext().getResources().getInteger(17694736)) / 100.0f);
                this.mIdleIntent = new Intent("android.os.action.DEVICE_IDLE_MODE_CHANGED");
                this.mIdleIntent.addFlags(1342177280);
                this.mLightIdleIntent = new Intent("android.os.action.LIGHT_DEVICE_IDLE_MODE_CHANGED");
                this.mLightIdleIntent.addFlags(1342177280);
                IntentFilter filter = new IntentFilter();
                filter.addAction("android.intent.action.BATTERY_CHANGED");
                getContext().registerReceiver(this.mReceiver, filter);
                filter = new IntentFilter();
                filter.addAction("android.intent.action.PACKAGE_REMOVED");
                filter.addDataScheme("package");
                getContext().registerReceiver(this.mReceiver, filter);
                filter = new IntentFilter();
                filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
                getContext().registerReceiver(this.mReceiver, filter);
                filter = new IntentFilter();
                filter.addAction("android.intent.action.SCREEN_OFF");
                filter.addAction("android.intent.action.SCREEN_ON");
                getContext().registerReceiver(this.mInteractivityReceiver, filter);
                this.mLocalActivityManager.setDeviceIdleWhitelist(this.mPowerSaveWhitelistAllAppIdArray);
                this.mLocalPowerManager.setDeviceIdleWhitelist(this.mPowerSaveWhitelistAllAppIdArray);
                this.mLocalAlarmManager.setDeviceIdleUserWhitelist(this.mPowerSaveWhitelistUserAppIdArray);
                updateInteractivityLocked();
            }
            updateConnectivityState(null);
        }
    }

    public boolean addPowerSaveWhitelistAppInternal(String name) {
        synchronized (this) {
            try {
                if (this.mPowerSaveWhitelistUserApps.put(name, Integer.valueOf(UserHandle.getAppId(getContext().getPackageManager().getApplicationInfo(name, DumpState.DUMP_CHANGES).uid))) == null) {
                    reportPowerSaveWhitelistChangedLocked();
                    updateWhitelistAppIdsLocked();
                    writeConfigFileLocked();
                }
            } catch (NameNotFoundException e) {
                return false;
            }
        }
        return true;
    }

    public boolean removePowerSaveWhitelistAppInternal(String name) {
        synchronized (this) {
            if (this.mPowerSaveWhitelistUserApps.remove(name) != null) {
                reportPowerSaveWhitelistChangedLocked();
                updateWhitelistAppIdsLocked();
                writeConfigFileLocked();
                return true;
            }
            return false;
        }
    }

    public boolean getPowerSaveWhitelistAppInternal(String name) {
        boolean containsKey;
        synchronized (this) {
            containsKey = this.mPowerSaveWhitelistUserApps.containsKey(name);
        }
        return containsKey;
    }

    public boolean addPowerSaveWhitelistExceptIdleInternal(String name) {
        synchronized (this) {
            try {
                if (this.mPowerSaveWhitelistAppsExceptIdle.put(name, Integer.valueOf(UserHandle.getAppId(getContext().getPackageManager().getApplicationInfo(name, DumpState.DUMP_CHANGES).uid))) == null) {
                    this.mPowerSaveWhitelistUserAppsExceptIdle.add(name);
                    reportPowerSaveWhitelistChangedLocked();
                    this.mPowerSaveWhitelistExceptIdleAppIdArray = buildAppIdArray(this.mPowerSaveWhitelistAppsExceptIdle, this.mPowerSaveWhitelistUserApps, this.mPowerSaveWhitelistExceptIdleAppIds);
                }
            } catch (NameNotFoundException e) {
                return false;
            }
        }
        return true;
    }

    public void resetPowerSaveWhitelistExceptIdleInternal() {
        synchronized (this) {
            if (this.mPowerSaveWhitelistAppsExceptIdle.removeAll(this.mPowerSaveWhitelistUserAppsExceptIdle)) {
                reportPowerSaveWhitelistChangedLocked();
                this.mPowerSaveWhitelistExceptIdleAppIdArray = buildAppIdArray(this.mPowerSaveWhitelistAppsExceptIdle, this.mPowerSaveWhitelistUserApps, this.mPowerSaveWhitelistExceptIdleAppIds);
                this.mPowerSaveWhitelistUserAppsExceptIdle.clear();
            }
        }
    }

    public boolean getPowerSaveWhitelistExceptIdleInternal(String name) {
        boolean containsKey;
        synchronized (this) {
            containsKey = this.mPowerSaveWhitelistAppsExceptIdle.containsKey(name);
        }
        return containsKey;
    }

    public String[] getSystemPowerWhitelistExceptIdleInternal() {
        String[] apps;
        synchronized (this) {
            int size = this.mPowerSaveWhitelistAppsExceptIdle.size();
            apps = new String[size];
            for (int i = 0; i < size; i++) {
                apps[i] = (String) this.mPowerSaveWhitelistAppsExceptIdle.keyAt(i);
            }
        }
        return apps;
    }

    public String[] getSystemPowerWhitelistInternal() {
        String[] apps;
        synchronized (this) {
            int size = this.mPowerSaveWhitelistApps.size();
            apps = new String[size];
            for (int i = 0; i < size; i++) {
                apps[i] = (String) this.mPowerSaveWhitelistApps.keyAt(i);
            }
        }
        return apps;
    }

    public String[] getUserPowerWhitelistInternal() {
        String[] apps;
        synchronized (this) {
            apps = new String[this.mPowerSaveWhitelistUserApps.size()];
            for (int i = 0; i < this.mPowerSaveWhitelistUserApps.size(); i++) {
                apps[i] = (String) this.mPowerSaveWhitelistUserApps.keyAt(i);
            }
        }
        return apps;
    }

    public String[] getFullPowerWhitelistExceptIdleInternal() {
        String[] apps;
        synchronized (this) {
            int i;
            apps = new String[(this.mPowerSaveWhitelistAppsExceptIdle.size() + this.mPowerSaveWhitelistUserApps.size())];
            int cur = 0;
            for (i = 0; i < this.mPowerSaveWhitelistAppsExceptIdle.size(); i++) {
                apps[cur] = (String) this.mPowerSaveWhitelistAppsExceptIdle.keyAt(i);
                cur++;
            }
            for (i = 0; i < this.mPowerSaveWhitelistUserApps.size(); i++) {
                apps[cur] = (String) this.mPowerSaveWhitelistUserApps.keyAt(i);
                cur++;
            }
        }
        return apps;
    }

    public String[] getFullPowerWhitelistInternal() {
        String[] apps;
        synchronized (this) {
            int i;
            apps = new String[(this.mPowerSaveWhitelistApps.size() + this.mPowerSaveWhitelistUserApps.size())];
            int cur = 0;
            for (i = 0; i < this.mPowerSaveWhitelistApps.size(); i++) {
                apps[cur] = (String) this.mPowerSaveWhitelistApps.keyAt(i);
                cur++;
            }
            for (i = 0; i < this.mPowerSaveWhitelistUserApps.size(); i++) {
                apps[cur] = (String) this.mPowerSaveWhitelistUserApps.keyAt(i);
                cur++;
            }
        }
        return apps;
    }

    public boolean isPowerSaveWhitelistExceptIdleAppInternal(String packageName) {
        boolean z;
        synchronized (this) {
            if (this.mPowerSaveWhitelistAppsExceptIdle.containsKey(packageName)) {
                z = true;
            } else {
                z = this.mPowerSaveWhitelistUserApps.containsKey(packageName);
            }
        }
        return z;
    }

    public boolean isPowerSaveWhitelistAppInternal(String packageName) {
        boolean z;
        synchronized (this) {
            if (this.mPowerSaveWhitelistApps.containsKey(packageName)) {
                z = true;
            } else {
                z = this.mPowerSaveWhitelistUserApps.containsKey(packageName);
            }
        }
        return z;
    }

    public int[] getAppIdWhitelistExceptIdleInternal() {
        int[] iArr;
        synchronized (this) {
            iArr = this.mPowerSaveWhitelistExceptIdleAppIdArray;
        }
        return iArr;
    }

    public int[] getAppIdWhitelistInternal() {
        int[] iArr;
        synchronized (this) {
            iArr = this.mPowerSaveWhitelistAllAppIdArray;
        }
        return iArr;
    }

    public int[] getAppIdUserWhitelistInternal() {
        int[] iArr;
        synchronized (this) {
            iArr = this.mPowerSaveWhitelistUserAppIdArray;
        }
        return iArr;
    }

    public int[] getAppIdTempWhitelistInternal() {
        int[] iArr;
        synchronized (this) {
            iArr = this.mTempWhitelistAppIdArray;
        }
        return iArr;
    }

    void addPowerSaveTempWhitelistAppChecked(String packageName, long duration, int userId, String reason) throws RemoteException {
        getContext().enforceCallingPermission("android.permission.CHANGE_DEVICE_IDLE_TEMP_WHITELIST", "No permission to change device idle whitelist");
        int callingUid = Binder.getCallingUid();
        userId = ActivityManager.getService().handleIncomingUser(Binder.getCallingPid(), callingUid, userId, false, false, "addPowerSaveTempWhitelistApp", null);
        long token = Binder.clearCallingIdentity();
        try {
            addPowerSaveTempWhitelistAppInternal(callingUid, packageName, duration, userId, true, reason);
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    void addPowerSaveTempWhitelistAppInternal(int callingUid, String packageName, long duration, int userId, boolean sync, String reason) {
        try {
            addPowerSaveTempWhitelistAppDirectInternal(callingUid, UserHandle.getAppId(getContext().getPackageManager().getPackageUidAsUser(packageName, userId)), duration, sync, reason);
        } catch (NameNotFoundException e) {
        }
    }

    void addPowerSaveTempWhitelistAppDirectInternal(int callingUid, int appId, long duration, boolean sync, String reason) {
        long timeNow = SystemClock.elapsedRealtime();
        Runnable runnable = null;
        synchronized (this) {
            int callingAppId = UserHandle.getAppId(callingUid);
            if (callingAppId < 10000 || this.mPowerSaveWhitelistSystemAppIds.get(callingAppId)) {
                duration = Math.min(duration, this.mConstants.MAX_TEMP_APP_WHITELIST_DURATION);
                Pair<MutableLong, String> entry = (Pair) this.mTempWhitelistAppIdEndTimes.get(appId);
                boolean newEntry = entry == null;
                if (newEntry) {
                    entry = new Pair(new MutableLong(0), reason);
                    this.mTempWhitelistAppIdEndTimes.put(appId, entry);
                }
                ((MutableLong) entry.first).value = timeNow + duration;
                if (newEntry) {
                    try {
                        this.mBatteryStats.noteEvent(32785, reason, appId);
                    } catch (RemoteException e) {
                    }
                    postTempActiveTimeoutMessage(appId, duration);
                    updateTempWhitelistAppIdsLocked(appId, true);
                    if (this.mNetworkPolicyTempWhitelistCallback != null) {
                        if (sync) {
                            runnable = this.mNetworkPolicyTempWhitelistCallback;
                        } else {
                            this.mHandler.post(this.mNetworkPolicyTempWhitelistCallback);
                        }
                    }
                    reportTempWhitelistChangedLocked();
                }
            } else {
                throw new SecurityException("Calling app " + UserHandle.formatUid(callingUid) + " is not on whitelist");
            }
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    public void setNetworkPolicyTempWhitelistCallbackInternal(Runnable callback) {
        synchronized (this) {
            this.mNetworkPolicyTempWhitelistCallback = callback;
        }
    }

    private void postTempActiveTimeoutMessage(int uid, long delay) {
        this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(6, uid, 0), delay);
    }

    void checkTempAppWhitelistTimeout(int uid) {
        long timeNow = SystemClock.elapsedRealtime();
        synchronized (this) {
            Pair<MutableLong, String> entry = (Pair) this.mTempWhitelistAppIdEndTimes.get(uid);
            if (entry == null) {
                return;
            } else if (timeNow >= ((MutableLong) entry.first).value) {
                this.mTempWhitelistAppIdEndTimes.delete(uid);
                updateTempWhitelistAppIdsLocked(uid, false);
                if (this.mNetworkPolicyTempWhitelistCallback != null) {
                    this.mHandler.post(this.mNetworkPolicyTempWhitelistCallback);
                }
                reportTempWhitelistChangedLocked();
                try {
                    this.mBatteryStats.noteEvent(16401, (String) entry.second, uid);
                } catch (RemoteException e) {
                }
            } else {
                postTempActiveTimeoutMessage(uid, ((MutableLong) entry.first).value - timeNow);
            }
        }
    }

    public void exitIdleInternal(String reason) {
        synchronized (this) {
            becomeActiveLocked(reason, Binder.getCallingUid());
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void updateConnectivityState(android.content.Intent r7) {
        /*
        r6 = this;
        monitor-enter(r6);
        r0 = r6.mConnectivityService;	 Catch:{ all -> 0x0007 }
        monitor-exit(r6);
        if (r0 != 0) goto L_0x000a;
    L_0x0006:
        return;
    L_0x0007:
        r4 = move-exception;
        monitor-exit(r6);
        throw r4;
    L_0x000a:
        r3 = r0.getActiveNetworkInfo();
        monitor-enter(r6);
        if (r3 != 0) goto L_0x0027;
    L_0x0011:
        r1 = 0;
    L_0x0012:
        r4 = r6.mNetworkConnected;	 Catch:{ all -> 0x0049 }
        if (r1 == r4) goto L_0x0025;
    L_0x0016:
        r6.mNetworkConnected = r1;	 Catch:{ all -> 0x0049 }
        if (r1 == 0) goto L_0x0025;
    L_0x001a:
        r4 = r6.mLightState;	 Catch:{ all -> 0x0049 }
        r5 = 5;
        if (r4 != r5) goto L_0x0025;
    L_0x001f:
        r4 = "network";
        r6.stepLightIdleStateLocked(r4);	 Catch:{ all -> 0x0049 }
    L_0x0025:
        monitor-exit(r6);
        return;
    L_0x0027:
        if (r7 != 0) goto L_0x002e;
    L_0x0029:
        r1 = r3.isConnected();	 Catch:{ all -> 0x0049 }
        goto L_0x0012;
    L_0x002e:
        r4 = "networkType";
        r5 = -1;
        r2 = r7.getIntExtra(r4, r5);	 Catch:{ all -> 0x0049 }
        r4 = r3.getType();	 Catch:{ all -> 0x0049 }
        if (r4 == r2) goto L_0x003e;
    L_0x003c:
        monitor-exit(r6);
        return;
    L_0x003e:
        r4 = "noConnectivity";
        r5 = 0;
        r4 = r7.getBooleanExtra(r4, r5);	 Catch:{ all -> 0x0049 }
        r1 = r4 ^ 1;
        goto L_0x0012;
    L_0x0049:
        r4 = move-exception;
        monitor-exit(r6);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.DeviceIdleController.updateConnectivityState(android.content.Intent):void");
    }

    void updateInteractivityLocked() {
        boolean screenOn = this.mPowerManager.isInteractive();
        if (!screenOn && this.mScreenOn) {
            this.mScreenOn = false;
            if (!this.mForceIdle) {
                becomeInactiveIfAppropriateLocked();
            }
        } else if (screenOn) {
            this.mScreenOn = true;
            if (!this.mForceIdle) {
                becomeActiveLocked("screen", Process.myUid());
            }
        }
    }

    void updateChargingLocked(boolean charging) {
        if (!charging && this.mCharging) {
            this.mCharging = false;
            if (!this.mForceIdle) {
                becomeInactiveIfAppropriateLocked();
            }
        } else if (charging) {
            this.mCharging = charging;
            if (!this.mForceIdle) {
                becomeActiveLocked("charging", Process.myUid());
            }
        }
    }

    void scheduleReportActiveLocked(String activeReason, int activeUid) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(5, activeUid, 0, activeReason));
    }

    void becomeActiveLocked(String activeReason, int activeUid) {
        if (this.mState != 0 || this.mLightState != 0) {
            EventLogTags.writeDeviceIdle(0, activeReason);
            EventLogTags.writeDeviceIdleLight(0, activeReason);
            scheduleReportActiveLocked(activeReason, activeUid);
            this.mState = 0;
            this.mLightState = 0;
            this.mInactiveTimeout = this.mConstants.INACTIVE_TIMEOUT;
            this.mCurIdleBudget = 0;
            this.mMaintenanceStartTime = 0;
            resetIdleManagementLocked();
            resetLightIdleManagementLocked();
            addEvent(1);
        }
    }

    void becomeInactiveIfAppropriateLocked() {
        if ((!this.mScreenOn && (this.mCharging ^ 1) != 0) || this.mForceIdle) {
            if (this.mState == 0 && this.mDeepEnabled) {
                this.mState = 1;
                resetIdleManagementLocked();
                scheduleAlarmLocked(this.mInactiveTimeout, false);
                EventLogTags.writeDeviceIdle(this.mState, "no activity");
            }
            if (this.mLightState == 0 && this.mLightEnabled) {
                this.mLightState = 1;
                resetLightIdleManagementLocked();
                scheduleLightAlarmLocked(this.mConstants.LIGHT_IDLE_AFTER_INACTIVE_TIMEOUT);
                EventLogTags.writeDeviceIdleLight(this.mLightState, "no activity");
            }
        }
    }

    void resetIdleManagementLocked() {
        this.mNextIdlePendingDelay = 0;
        this.mNextIdleDelay = 0;
        this.mNextLightIdleDelay = 0;
        cancelAlarmLocked();
        cancelSensingTimeoutAlarmLocked();
        cancelLocatingLocked();
        stopMonitoringMotionLocked();
        this.mAnyMotionDetector.stop();
    }

    void resetLightIdleManagementLocked() {
        cancelLightAlarmLocked();
    }

    void exitForceIdleLocked() {
        if (this.mForceIdle) {
            this.mForceIdle = false;
            if (this.mScreenOn || this.mCharging) {
                becomeActiveLocked("exit-force", Process.myUid());
            }
        }
    }

    void stepLightIdleStateLocked(String reason) {
        if (this.mLightState != 7) {
            EventLogTags.writeDeviceIdleLightStep();
            switch (this.mLightState) {
                case 1:
                    this.mCurIdleBudget = this.mConstants.LIGHT_IDLE_MAINTENANCE_MIN_BUDGET;
                    this.mNextLightIdleDelay = this.mConstants.LIGHT_IDLE_TIMEOUT;
                    this.mMaintenanceStartTime = 0;
                    if (!isOpsInactiveLocked()) {
                        this.mLightState = 3;
                        EventLogTags.writeDeviceIdleLight(this.mLightState, reason);
                        scheduleLightAlarmLocked(this.mConstants.LIGHT_PRE_IDLE_TIMEOUT);
                        break;
                    }
                case 3:
                case 6:
                    if (this.mMaintenanceStartTime != 0) {
                        long duration = SystemClock.elapsedRealtime() - this.mMaintenanceStartTime;
                        if (duration < this.mConstants.LIGHT_IDLE_MAINTENANCE_MIN_BUDGET) {
                            this.mCurIdleBudget += this.mConstants.LIGHT_IDLE_MAINTENANCE_MIN_BUDGET - duration;
                        } else {
                            this.mCurIdleBudget -= duration - this.mConstants.LIGHT_IDLE_MAINTENANCE_MIN_BUDGET;
                        }
                    }
                    this.mMaintenanceStartTime = 0;
                    scheduleLightAlarmLocked(this.mNextLightIdleDelay);
                    this.mNextLightIdleDelay = Math.min(this.mConstants.LIGHT_MAX_IDLE_TIMEOUT, (long) (((float) this.mNextLightIdleDelay) * this.mConstants.LIGHT_IDLE_FACTOR));
                    if (this.mNextLightIdleDelay < this.mConstants.LIGHT_IDLE_TIMEOUT) {
                        this.mNextLightIdleDelay = this.mConstants.LIGHT_IDLE_TIMEOUT;
                    }
                    this.mLightState = 4;
                    EventLogTags.writeDeviceIdleLight(this.mLightState, reason);
                    addEvent(2);
                    this.mGoingIdleWakeLock.acquire();
                    this.mHandler.sendEmptyMessage(3);
                    break;
                case 4:
                case 5:
                    if (!this.mNetworkConnected && this.mLightState != 5) {
                        scheduleLightAlarmLocked(this.mNextLightIdleDelay);
                        this.mLightState = 5;
                        EventLogTags.writeDeviceIdleLight(this.mLightState, reason);
                        break;
                    }
                    this.mActiveIdleOpCount = 1;
                    this.mActiveIdleWakeLock.acquire();
                    this.mMaintenanceStartTime = SystemClock.elapsedRealtime();
                    if (this.mCurIdleBudget < this.mConstants.LIGHT_IDLE_MAINTENANCE_MIN_BUDGET) {
                        this.mCurIdleBudget = this.mConstants.LIGHT_IDLE_MAINTENANCE_MIN_BUDGET;
                    } else if (this.mCurIdleBudget > this.mConstants.LIGHT_IDLE_MAINTENANCE_MAX_BUDGET) {
                        this.mCurIdleBudget = this.mConstants.LIGHT_IDLE_MAINTENANCE_MAX_BUDGET;
                    }
                    scheduleLightAlarmLocked(this.mCurIdleBudget);
                    this.mLightState = 6;
                    EventLogTags.writeDeviceIdleLight(this.mLightState, reason);
                    addEvent(3);
                    this.mHandler.sendEmptyMessage(4);
                    break;
                    break;
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    void stepIdleStateLocked(java.lang.String r13) {
        /*
        r12 = this;
        r11 = 2;
        r10 = 4;
        r7 = 1;
        r5 = 0;
        r4 = 0;
        com.android.server.EventLogTags.writeDeviceIdleStep();
        r8 = android.os.SystemClock.elapsedRealtime();
        r0 = r12.mConstants;
        r0 = r0.MIN_TIME_TO_ALARM;
        r0 = r0 + r8;
        r2 = r12.mAlarmManager;
        r2 = r2.getNextWakeFromIdleTime();
        r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1));
        if (r0 <= 0) goto L_0x002d;
    L_0x001b:
        r0 = r12.mState;
        if (r0 == 0) goto L_0x002c;
    L_0x001f:
        r0 = "alarm";
        r1 = android.os.Process.myUid();
        r12.becomeActiveLocked(r0, r1);
        r12.becomeInactiveIfAppropriateLocked();
    L_0x002c:
        return;
    L_0x002d:
        r0 = r12.mState;
        switch(r0) {
            case 1: goto L_0x0033;
            case 2: goto L_0x0051;
            case 3: goto L_0x0071;
            case 4: goto L_0x00cd;
            case 5: goto L_0x012d;
            case 6: goto L_0x00d8;
            default: goto L_0x0032;
        };
    L_0x0032:
        return;
    L_0x0033:
        r12.startMonitoringMotionLocked();
        r0 = r12.mConstants;
        r0 = r0.IDLE_AFTER_INACTIVE_TIMEOUT;
        r12.scheduleAlarmLocked(r0, r4);
        r0 = r12.mConstants;
        r0 = r0.IDLE_PENDING_TIMEOUT;
        r12.mNextIdlePendingDelay = r0;
        r0 = r12.mConstants;
        r0 = r0.IDLE_TIMEOUT;
        r12.mNextIdleDelay = r0;
        r12.mState = r11;
        r0 = r12.mState;
        com.android.server.EventLogTags.writeDeviceIdle(r0, r13);
        goto L_0x0032;
    L_0x0051:
        r0 = 3;
        r12.mState = r0;
        r0 = r12.mState;
        com.android.server.EventLogTags.writeDeviceIdle(r0, r13);
        r0 = r12.mConstants;
        r0 = r0.SENSING_TIMEOUT;
        r12.scheduleSensingTimeoutAlarmLocked(r0);
        r12.cancelLocatingLocked();
        r12.mNotMoving = r4;
        r12.mLocated = r4;
        r12.mLastGenericLocation = r5;
        r12.mLastGpsLocation = r5;
        r0 = r12.mAnyMotionDetector;
        r0.checkForAnyMotion();
        goto L_0x0032;
    L_0x0071:
        r12.cancelSensingTimeoutAlarmLocked();
        r12.mState = r10;
        r0 = r12.mState;
        com.android.server.EventLogTags.writeDeviceIdle(r0, r13);
        r0 = r12.mConstants;
        r0 = r0.LOCATING_TIMEOUT;
        r12.scheduleAlarmLocked(r0, r4);
        r0 = r12.mLocationManager;
        if (r0 == 0) goto L_0x0126;
    L_0x0086:
        r0 = r12.mLocationManager;
        r1 = "network";
        r0 = r0.getProvider(r1);
        if (r0 == 0) goto L_0x0126;
    L_0x0091:
        r0 = r12.mLocationManager;
        r1 = r12.mLocationRequest;
        r2 = r12.mGenericLocationListener;
        r3 = r12.mHandler;
        r3 = r3.getLooper();
        r0.requestLocationUpdates(r1, r2, r3);
        r12.mLocating = r7;
    L_0x00a2:
        r0 = r12.mLocationManager;
        if (r0 == 0) goto L_0x012a;
    L_0x00a6:
        r0 = r12.mLocationManager;
        r1 = "gps";
        r0 = r0.getProvider(r1);
        if (r0 == 0) goto L_0x012a;
    L_0x00b1:
        r12.mHasGps = r7;
        r0 = r12.mLocationManager;
        r1 = "gps";
        r2 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r4 = 1084227584; // 0x40a00000 float:5.0 double:5.356796015E-315;
        r5 = r12.mGpsLocationListener;
        r6 = r12.mHandler;
        r6 = r6.getLooper();
        r0.requestLocationUpdates(r1, r2, r4, r5, r6);
        r12.mLocating = r7;
    L_0x00c9:
        r0 = r12.mLocating;
        if (r0 != 0) goto L_0x0032;
    L_0x00cd:
        r12.cancelAlarmLocked();
        r12.cancelLocatingLocked();
        r0 = r12.mAnyMotionDetector;
        r0.stop();
    L_0x00d8:
        r0 = r12.mNextIdleDelay;
        r12.scheduleAlarmLocked(r0, r7);
        r0 = r12.mNextIdleDelay;
        r0 = (float) r0;
        r1 = r12.mConstants;
        r1 = r1.IDLE_FACTOR;
        r0 = r0 * r1;
        r0 = (long) r0;
        r12.mNextIdleDelay = r0;
        r0 = r12.mNextIdleDelay;
        r2 = r12.mConstants;
        r2 = r2.MAX_IDLE_TIMEOUT;
        r0 = java.lang.Math.min(r0, r2);
        r12.mNextIdleDelay = r0;
        r0 = r12.mNextIdleDelay;
        r2 = r12.mConstants;
        r2 = r2.IDLE_TIMEOUT;
        r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1));
        if (r0 >= 0) goto L_0x0104;
    L_0x00fe:
        r0 = r12.mConstants;
        r0 = r0.IDLE_TIMEOUT;
        r12.mNextIdleDelay = r0;
    L_0x0104:
        r0 = 5;
        r12.mState = r0;
        r0 = r12.mLightState;
        r1 = 7;
        if (r0 == r1) goto L_0x0112;
    L_0x010c:
        r0 = 7;
        r12.mLightState = r0;
        r12.cancelLightAlarmLocked();
    L_0x0112:
        r0 = r12.mState;
        com.android.server.EventLogTags.writeDeviceIdle(r0, r13);
        r12.addEvent(r10);
        r0 = r12.mGoingIdleWakeLock;
        r0.acquire();
        r0 = r12.mHandler;
        r0.sendEmptyMessage(r11);
        goto L_0x0032;
    L_0x0126:
        r12.mHasNetworkLocation = r4;
        goto L_0x00a2;
    L_0x012a:
        r12.mHasGps = r4;
        goto L_0x00c9;
    L_0x012d:
        r12.mActiveIdleOpCount = r7;
        r0 = r12.mActiveIdleWakeLock;
        r0.acquire();
        r0 = r12.mNextIdlePendingDelay;
        r12.scheduleAlarmLocked(r0, r4);
        r0 = android.os.SystemClock.elapsedRealtime();
        r12.mMaintenanceStartTime = r0;
        r0 = r12.mConstants;
        r0 = r0.MAX_IDLE_PENDING_TIMEOUT;
        r2 = r12.mNextIdlePendingDelay;
        r2 = (float) r2;
        r3 = r12.mConstants;
        r3 = r3.IDLE_PENDING_FACTOR;
        r2 = r2 * r3;
        r2 = (long) r2;
        r0 = java.lang.Math.min(r0, r2);
        r12.mNextIdlePendingDelay = r0;
        r0 = r12.mNextIdlePendingDelay;
        r2 = r12.mConstants;
        r2 = r2.IDLE_PENDING_TIMEOUT;
        r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1));
        if (r0 >= 0) goto L_0x0162;
    L_0x015c:
        r0 = r12.mConstants;
        r0 = r0.IDLE_PENDING_TIMEOUT;
        r12.mNextIdlePendingDelay = r0;
    L_0x0162:
        r0 = 6;
        r12.mState = r0;
        r0 = r12.mState;
        com.android.server.EventLogTags.writeDeviceIdle(r0, r13);
        r0 = 5;
        r12.addEvent(r0);
        r0 = r12.mHandler;
        r0.sendEmptyMessage(r10);
        goto L_0x0032;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.DeviceIdleController.stepIdleStateLocked(java.lang.String):void");
    }

    void incActiveIdleOps() {
        synchronized (this) {
            this.mActiveIdleOpCount++;
        }
    }

    void decActiveIdleOps() {
        synchronized (this) {
            this.mActiveIdleOpCount--;
            if (this.mActiveIdleOpCount <= 0) {
                exitMaintenanceEarlyIfNeededLocked();
                this.mActiveIdleWakeLock.release();
            }
        }
    }

    void setJobsActive(boolean active) {
        synchronized (this) {
            this.mJobsActive = active;
            reportMaintenanceActivityIfNeededLocked();
            if (!active) {
                exitMaintenanceEarlyIfNeededLocked();
            }
        }
    }

    void setAlarmsActive(boolean active) {
        synchronized (this) {
            this.mAlarmsActive = active;
            if (!active) {
                exitMaintenanceEarlyIfNeededLocked();
            }
        }
    }

    boolean registerMaintenanceActivityListener(IMaintenanceActivityListener listener) {
        boolean z;
        synchronized (this) {
            this.mMaintenanceActivityListeners.register(listener);
            z = this.mReportedMaintenanceActivity;
        }
        return z;
    }

    void unregisterMaintenanceActivityListener(IMaintenanceActivityListener listener) {
        synchronized (this) {
            this.mMaintenanceActivityListeners.unregister(listener);
        }
    }

    void reportMaintenanceActivityIfNeededLocked() {
        boolean active = this.mJobsActive;
        if (active != this.mReportedMaintenanceActivity) {
            int i;
            this.mReportedMaintenanceActivity = active;
            MyHandler myHandler = this.mHandler;
            if (this.mReportedMaintenanceActivity) {
                i = 1;
            } else {
                i = 0;
            }
            this.mHandler.sendMessage(myHandler.obtainMessage(7, i, 0));
        }
    }

    boolean isOpsInactiveLocked() {
        return (this.mActiveIdleOpCount > 0 || (this.mJobsActive ^ 1) == 0) ? false : this.mAlarmsActive ^ 1;
    }

    void exitMaintenanceEarlyIfNeededLocked() {
        if (!(this.mState == 6 || this.mLightState == 6)) {
            if (this.mLightState != 3) {
                return;
            }
        }
        if (isOpsInactiveLocked()) {
            long now = SystemClock.elapsedRealtime();
            if (this.mState == 6) {
                stepIdleStateLocked("s:early");
            } else if (this.mLightState == 3) {
                stepLightIdleStateLocked("s:predone");
            } else {
                stepLightIdleStateLocked("s:early");
            }
        }
    }

    void motionLocked() {
        handleMotionDetectedLocked(this.mConstants.MOTION_INACTIVE_TIMEOUT, "motion");
    }

    void handleMotionDetectedLocked(long timeout, String type) {
        boolean becomeInactive = false;
        if (this.mState != 0) {
            scheduleReportActiveLocked(type, Process.myUid());
            this.mState = 0;
            this.mInactiveTimeout = timeout;
            this.mCurIdleBudget = 0;
            this.mMaintenanceStartTime = 0;
            EventLogTags.writeDeviceIdle(this.mState, type);
            addEvent(1);
            becomeInactive = true;
        }
        if (this.mLightState == 7) {
            this.mLightState = 0;
            EventLogTags.writeDeviceIdleLight(this.mLightState, type);
            becomeInactive = true;
        }
        if (becomeInactive) {
            becomeInactiveIfAppropriateLocked();
        }
    }

    void receivedGenericLocationLocked(Location location) {
        if (this.mState != 4) {
            cancelLocatingLocked();
            return;
        }
        this.mLastGenericLocation = new Location(location);
        if (location.getAccuracy() <= this.mConstants.LOCATION_ACCURACY || !this.mHasGps) {
            this.mLocated = true;
            if (this.mNotMoving) {
                stepIdleStateLocked("s:location");
            }
        }
    }

    void receivedGpsLocationLocked(Location location) {
        if (this.mState != 4) {
            cancelLocatingLocked();
            return;
        }
        this.mLastGpsLocation = new Location(location);
        if (location.getAccuracy() <= this.mConstants.LOCATION_ACCURACY) {
            this.mLocated = true;
            if (this.mNotMoving) {
                stepIdleStateLocked("s:gps");
            }
        }
    }

    void startMonitoringMotionLocked() {
        if (this.mMotionSensor != null && (this.mMotionListener.active ^ 1) != 0) {
            this.mMotionListener.registerLocked();
        }
    }

    void stopMonitoringMotionLocked() {
        if (this.mMotionSensor != null && this.mMotionListener.active) {
            this.mMotionListener.unregisterLocked();
        }
    }

    void cancelAlarmLocked() {
        if (this.mNextAlarmTime != 0) {
            this.mNextAlarmTime = 0;
            this.mAlarmManager.cancel(this.mDeepAlarmListener);
        }
    }

    void cancelLightAlarmLocked() {
        if (this.mNextLightAlarmTime != 0) {
            this.mNextLightAlarmTime = 0;
            this.mAlarmManager.cancel(this.mLightAlarmListener);
        }
    }

    void cancelLocatingLocked() {
        if (this.mLocating) {
            this.mLocationManager.removeUpdates(this.mGenericLocationListener);
            this.mLocationManager.removeUpdates(this.mGpsLocationListener);
            this.mLocating = false;
        }
    }

    void cancelSensingTimeoutAlarmLocked() {
        if (this.mNextSensingTimeoutAlarmTime != 0) {
            this.mNextSensingTimeoutAlarmTime = 0;
            this.mAlarmManager.cancel(this.mSensingTimeoutAlarmListener);
        }
    }

    void scheduleAlarmLocked(long delay, boolean idleUntil) {
        if (this.mMotionSensor != null) {
            this.mNextAlarmTime = SystemClock.elapsedRealtime() + delay;
            if (idleUntil) {
                this.mAlarmManager.setIdleUntil(2, this.mNextAlarmTime, "DeviceIdleController.deep", this.mDeepAlarmListener, this.mHandler);
            } else {
                this.mAlarmManager.set(2, this.mNextAlarmTime, "DeviceIdleController.deep", this.mDeepAlarmListener, this.mHandler);
            }
        }
    }

    void scheduleLightAlarmLocked(long delay) {
        this.mNextLightAlarmTime = SystemClock.elapsedRealtime() + delay;
        this.mAlarmManager.set(2, this.mNextLightAlarmTime, "DeviceIdleController.light", this.mLightAlarmListener, this.mHandler);
    }

    void scheduleSensingTimeoutAlarmLocked(long delay) {
        this.mNextSensingTimeoutAlarmTime = SystemClock.elapsedRealtime() + delay;
        this.mAlarmManager.set(2, this.mNextSensingTimeoutAlarmTime, "DeviceIdleController.sensing", this.mSensingTimeoutAlarmListener, this.mHandler);
    }

    private static int[] buildAppIdArray(ArrayMap<String, Integer> systemApps, ArrayMap<String, Integer> userApps, SparseBooleanArray outAppIds) {
        int i;
        outAppIds.clear();
        if (systemApps != null) {
            for (i = 0; i < systemApps.size(); i++) {
                outAppIds.put(((Integer) systemApps.valueAt(i)).intValue(), true);
            }
        }
        if (userApps != null) {
            for (i = 0; i < userApps.size(); i++) {
                outAppIds.put(((Integer) userApps.valueAt(i)).intValue(), true);
            }
        }
        int size = outAppIds.size();
        int[] appids = new int[size];
        for (i = 0; i < size; i++) {
            appids[i] = outAppIds.keyAt(i);
        }
        return appids;
    }

    private void updateWhitelistAppIdsLocked() {
        this.mPowerSaveWhitelistExceptIdleAppIdArray = buildAppIdArray(this.mPowerSaveWhitelistAppsExceptIdle, this.mPowerSaveWhitelistUserApps, this.mPowerSaveWhitelistExceptIdleAppIds);
        this.mPowerSaveWhitelistAllAppIdArray = buildAppIdArray(this.mPowerSaveWhitelistApps, this.mPowerSaveWhitelistUserApps, this.mPowerSaveWhitelistAllAppIds);
        this.mPowerSaveWhitelistUserAppIdArray = buildAppIdArray(null, this.mPowerSaveWhitelistUserApps, this.mPowerSaveWhitelistUserAppIds);
        if (this.mLocalActivityManager != null) {
            this.mLocalActivityManager.setDeviceIdleWhitelist(this.mPowerSaveWhitelistAllAppIdArray);
        }
        if (this.mLocalPowerManager != null) {
            this.mLocalPowerManager.setDeviceIdleWhitelist(this.mPowerSaveWhitelistAllAppIdArray);
        }
        if (this.mLocalAlarmManager != null) {
            this.mLocalAlarmManager.setDeviceIdleUserWhitelist(this.mPowerSaveWhitelistUserAppIdArray);
        }
    }

    private void updateTempWhitelistAppIdsLocked(int appId, boolean adding) {
        int size = this.mTempWhitelistAppIdEndTimes.size();
        if (this.mTempWhitelistAppIdArray.length != size) {
            this.mTempWhitelistAppIdArray = new int[size];
        }
        for (int i = 0; i < size; i++) {
            this.mTempWhitelistAppIdArray[i] = this.mTempWhitelistAppIdEndTimes.keyAt(i);
        }
        if (this.mLocalActivityManager != null) {
            this.mLocalActivityManager.updateDeviceIdleTempWhitelist(this.mTempWhitelistAppIdArray, appId, adding);
        }
        if (this.mLocalPowerManager != null) {
            this.mLocalPowerManager.setDeviceIdleTempWhitelist(this.mTempWhitelistAppIdArray);
        }
    }

    private void reportPowerSaveWhitelistChangedLocked() {
        Intent intent = new Intent("android.os.action.POWER_SAVE_WHITELIST_CHANGED");
        intent.addFlags(1073741824);
        getContext().sendBroadcastAsUser(intent, UserHandle.SYSTEM);
    }

    private void reportTempWhitelistChangedLocked() {
        Intent intent = new Intent("android.os.action.POWER_SAVE_TEMP_WHITELIST_CHANGED");
        intent.addFlags(1073741824);
        getContext().sendBroadcastAsUser(intent, UserHandle.SYSTEM);
    }

    void readConfigFileLocked() {
        this.mPowerSaveWhitelistUserApps.clear();
        try {
            FileInputStream stream = this.mConfigFile.openRead();
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(stream, StandardCharsets.UTF_8.name());
                readConfigFileLocked(parser);
                try {
                    stream.close();
                } catch (IOException e) {
                }
            } catch (XmlPullParserException e2) {
                try {
                    stream.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    stream.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        } catch (FileNotFoundException e5) {
        }
    }

    private void readConfigFileLocked(XmlPullParser parser) {
        int type;
        PackageManager pm = getContext().getPackageManager();
        do {
            try {
                type = parser.next();
                if (type == 2) {
                    break;
                }
            } catch (IllegalStateException e) {
                Slog.w(TAG, "Failed parsing config " + e);
                return;
            } catch (NullPointerException e2) {
                Slog.w(TAG, "Failed parsing config " + e2);
                return;
            } catch (NumberFormatException e3) {
                Slog.w(TAG, "Failed parsing config " + e3);
                return;
            } catch (XmlPullParserException e4) {
                Slog.w(TAG, "Failed parsing config " + e4);
                return;
            } catch (IOException e5) {
                Slog.w(TAG, "Failed parsing config " + e5);
                return;
            } catch (IndexOutOfBoundsException e6) {
                Slog.w(TAG, "Failed parsing config " + e6);
                return;
            }
        } while (type != 1);
        if (type != 2) {
            throw new IllegalStateException("no start tag found");
        }
        int outerDepth = parser.getDepth();
        while (true) {
            type = parser.next();
            if (type == 1) {
                return;
            }
            if (type == 3 && parser.getDepth() <= outerDepth) {
                return;
            }
            if (!(type == 3 || type == 4)) {
                if (parser.getName().equals("wl")) {
                    String name = parser.getAttributeValue(null, "n");
                    if (name != null) {
                        try {
                            ApplicationInfo ai = pm.getApplicationInfo(name, DumpState.DUMP_CHANGES);
                            this.mPowerSaveWhitelistUserApps.put(ai.packageName, Integer.valueOf(UserHandle.getAppId(ai.uid)));
                        } catch (NameNotFoundException e7) {
                        }
                    }
                } else {
                    Slog.w(TAG, "Unknown element under <config>: " + parser.getName());
                    XmlUtils.skipCurrentTag(parser);
                }
            }
        }
    }

    void writeConfigFileLocked() {
        this.mHandler.removeMessages(1);
        this.mHandler.sendEmptyMessageDelayed(1, 5000);
    }

    void handleWriteConfigFile() {
        ByteArrayOutputStream memStream = new ByteArrayOutputStream();
        try {
            synchronized (this) {
                XmlSerializer out = new FastXmlSerializer();
                out.setOutput(memStream, StandardCharsets.UTF_8.name());
                writeConfigFileLocked(out);
            }
        } catch (IOException e) {
        }
        synchronized (this.mConfigFile) {
            FileOutputStream fileOutputStream = null;
            try {
                fileOutputStream = this.mConfigFile.startWrite();
                memStream.writeTo(fileOutputStream);
                fileOutputStream.flush();
                Trace.traceBegin(16777216, "DeviceIdleController::fsync");
                FileUtils.sync(fileOutputStream);
                Trace.traceEnd(16777216);
                fileOutputStream.close();
                this.mConfigFile.finishWrite(fileOutputStream);
            } catch (IOException e2) {
                Slog.w(TAG, "Error writing config file", e2);
                this.mConfigFile.failWrite(fileOutputStream);
            }
        }
    }

    void writeConfigFileLocked(XmlSerializer out) throws IOException {
        out.startDocument(null, Boolean.valueOf(true));
        out.startTag(null, "config");
        for (int i = 0; i < this.mPowerSaveWhitelistUserApps.size(); i++) {
            String name = (String) this.mPowerSaveWhitelistUserApps.keyAt(i);
            out.startTag(null, "wl");
            out.attribute(null, "n", name);
            out.endTag(null, "wl");
        }
        out.endTag(null, "config");
        out.endDocument();
    }

    static void dumpHelp(PrintWriter pw) {
        pw.println("Device idle controller (deviceidle) commands:");
        pw.println("  help");
        pw.println("    Print this help text.");
        pw.println("  step [light|deep]");
        pw.println("    Immediately step to next state, without waiting for alarm.");
        pw.println("  force-idle [light|deep]");
        pw.println("    Force directly into idle mode, regardless of other device state.");
        pw.println("  force-inactive");
        pw.println("    Force to be inactive, ready to freely step idle states.");
        pw.println("  unforce");
        pw.println("    Resume normal functioning after force-idle or force-inactive.");
        pw.println("  get [light|deep|force|screen|charging|network]");
        pw.println("    Retrieve the current given state.");
        pw.println("  disable [light|deep|all]");
        pw.println("    Completely disable device idle mode.");
        pw.println("  enable [light|deep|all]");
        pw.println("    Re-enable device idle mode after it had previously been disabled.");
        pw.println("  enabled [light|deep|all]");
        pw.println("    Print 1 if device idle mode is currently enabled, else 0.");
        pw.println("  whitelist");
        pw.println("    Print currently whitelisted apps.");
        pw.println("  whitelist [package ...]");
        pw.println("    Add (prefix with +) or remove (prefix with -) packages.");
        pw.println("  except-idle-whitelist [package ...|reset]");
        pw.println("    Prefix the package with '+' to add it to whitelist or '=' to check if it is already whitelisted");
        pw.println("    [reset] will reset the whitelist to it's original state");
        pw.println("    Note that unlike <whitelist> cmd, changes made using this won't be persisted across boots");
        pw.println("  tempwhitelist");
        pw.println("    Print packages that are temporarily whitelisted.");
        pw.println("  tempwhitelist [-u USER] [-d DURATION] [package ..]");
        pw.println("    Temporarily place packages in whitelist for DURATION milliseconds.");
        pw.println("    If no DURATION is specified, 10 seconds is used");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    int onShellCommand(com.android.server.DeviceIdleController.Shell r22, java.lang.String r23) {
        /*
        r21 = this;
        r17 = r22.getOutPrintWriter();
        r2 = "step";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x009a;
    L_0x000f:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        monitor-enter(r21);
        r18 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x007d }
        r3 = r22.getNextArg();	 Catch:{ all -> 0x007d }
        if (r3 == 0) goto L_0x002e;
    L_0x0025:
        r2 = "deep";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x0078 }
        if (r2 == 0) goto L_0x0051;
    L_0x002e:
        r2 = "s:shell";
        r0 = r21;
        r0.stepIdleStateLocked(r2);	 Catch:{ all -> 0x0078 }
        r2 = "Stepped to deep: ";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x0078 }
        r0 = r21;
        r2 = r0.mState;	 Catch:{ all -> 0x0078 }
        r2 = stateToString(r2);	 Catch:{ all -> 0x0078 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0078 }
    L_0x004b:
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x007d }
    L_0x004e:
        monitor-exit(r21);
    L_0x004f:
        r2 = 0;
        return r2;
    L_0x0051:
        r2 = "light";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x0078 }
        if (r2 == 0) goto L_0x0080;
    L_0x005a:
        r2 = "s:shell";
        r0 = r21;
        r0.stepLightIdleStateLocked(r2);	 Catch:{ all -> 0x0078 }
        r2 = "Stepped to light: ";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x0078 }
        r0 = r21;
        r2 = r0.mLightState;	 Catch:{ all -> 0x0078 }
        r2 = lightStateToString(r2);	 Catch:{ all -> 0x0078 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0078 }
        goto L_0x004b;
    L_0x0078:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x007d }
        throw r2;	 Catch:{ all -> 0x007d }
    L_0x007d:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x0080:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0078 }
        r2.<init>();	 Catch:{ all -> 0x0078 }
        r6 = "Unknown idle mode: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x0078 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0078 }
        r2 = r2.toString();	 Catch:{ all -> 0x0078 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0078 }
        goto L_0x004b;
    L_0x009a:
        r2 = "force-idle";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x0199;
    L_0x00a5:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        monitor-enter(r21);
        r18 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x0125 }
        r3 = r22.getNextArg();	 Catch:{ all -> 0x0125 }
        if (r3 == 0) goto L_0x00c4;
    L_0x00bb:
        r2 = "deep";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x017a }
        if (r2 == 0) goto L_0x0128;
    L_0x00c4:
        r0 = r21;
        r2 = r0.mDeepEnabled;	 Catch:{ all -> 0x017a }
        if (r2 != 0) goto L_0x00d8;
    L_0x00ca:
        r2 = "Unable to go deep idle; not enabled";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x017a }
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0125 }
        r2 = -1;
        monitor-exit(r21);
        return r2;
    L_0x00d8:
        r2 = 1;
        r0 = r21;
        r0.mForceIdle = r2;	 Catch:{ all -> 0x017a }
        r21.becomeInactiveIfAppropriateLocked();	 Catch:{ all -> 0x017a }
        r0 = r21;
        r11 = r0.mState;	 Catch:{ all -> 0x017a }
    L_0x00e4:
        r2 = 5;
        if (r11 == r2) goto L_0x0118;
    L_0x00e7:
        r2 = "s:shell";
        r0 = r21;
        r0.stepIdleStateLocked(r2);	 Catch:{ all -> 0x017a }
        r0 = r21;
        r2 = r0.mState;	 Catch:{ all -> 0x017a }
        if (r11 != r2) goto L_0x0113;
    L_0x00f5:
        r2 = "Unable to go deep idle; stopped at ";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x017a }
        r0 = r21;
        r2 = r0.mState;	 Catch:{ all -> 0x017a }
        r2 = stateToString(r2);	 Catch:{ all -> 0x017a }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x017a }
        r21.exitForceIdleLocked();	 Catch:{ all -> 0x017a }
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0125 }
        r2 = -1;
        monitor-exit(r21);
        return r2;
    L_0x0113:
        r0 = r21;
        r11 = r0.mState;	 Catch:{ all -> 0x017a }
        goto L_0x00e4;
    L_0x0118:
        r2 = "Now forced in to deep idle mode";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x017a }
    L_0x0120:
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0125 }
        goto L_0x004e;
    L_0x0125:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x0128:
        r2 = "light";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x017a }
        if (r2 == 0) goto L_0x017f;
    L_0x0131:
        r2 = 1;
        r0 = r21;
        r0.mForceIdle = r2;	 Catch:{ all -> 0x017a }
        r21.becomeInactiveIfAppropriateLocked();	 Catch:{ all -> 0x017a }
        r0 = r21;
        r10 = r0.mLightState;	 Catch:{ all -> 0x017a }
    L_0x013d:
        r2 = 4;
        if (r10 == r2) goto L_0x0171;
    L_0x0140:
        r2 = "s:shell";
        r0 = r21;
        r0.stepIdleStateLocked(r2);	 Catch:{ all -> 0x017a }
        r0 = r21;
        r2 = r0.mLightState;	 Catch:{ all -> 0x017a }
        if (r10 != r2) goto L_0x016c;
    L_0x014e:
        r2 = "Unable to go light idle; stopped at ";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x017a }
        r0 = r21;
        r2 = r0.mLightState;	 Catch:{ all -> 0x017a }
        r2 = lightStateToString(r2);	 Catch:{ all -> 0x017a }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x017a }
        r21.exitForceIdleLocked();	 Catch:{ all -> 0x017a }
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0125 }
        r2 = -1;
        monitor-exit(r21);
        return r2;
    L_0x016c:
        r0 = r21;
        r10 = r0.mLightState;	 Catch:{ all -> 0x017a }
        goto L_0x013d;
    L_0x0171:
        r2 = "Now forced in to light idle mode";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x017a }
        goto L_0x0120;
    L_0x017a:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0125 }
        throw r2;	 Catch:{ all -> 0x0125 }
    L_0x017f:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x017a }
        r2.<init>();	 Catch:{ all -> 0x017a }
        r6 = "Unknown idle mode: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x017a }
        r2 = r2.append(r3);	 Catch:{ all -> 0x017a }
        r2 = r2.toString();	 Catch:{ all -> 0x017a }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x017a }
        goto L_0x0120;
    L_0x0199:
        r2 = "force-inactive";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x01f3;
    L_0x01a4:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        monitor-enter(r21);
        r18 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x01eb }
        r2 = 1;
        r0 = r21;
        r0.mForceIdle = r2;	 Catch:{ all -> 0x01ee }
        r21.becomeInactiveIfAppropriateLocked();	 Catch:{ all -> 0x01ee }
        r2 = "Light state: ";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x01ee }
        r0 = r21;
        r2 = r0.mLightState;	 Catch:{ all -> 0x01ee }
        r2 = lightStateToString(r2);	 Catch:{ all -> 0x01ee }
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x01ee }
        r2 = ", deep state: ";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x01ee }
        r0 = r21;
        r2 = r0.mState;	 Catch:{ all -> 0x01ee }
        r2 = stateToString(r2);	 Catch:{ all -> 0x01ee }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x01ee }
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x01eb }
        goto L_0x004e;
    L_0x01eb:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x01ee:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x01eb }
        throw r2;	 Catch:{ all -> 0x01eb }
    L_0x01f3:
        r2 = "unforce";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x0248;
    L_0x01fe:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        monitor-enter(r21);
        r18 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x0240 }
        r21.exitForceIdleLocked();	 Catch:{ all -> 0x0243 }
        r2 = "Light state: ";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x0243 }
        r0 = r21;
        r2 = r0.mLightState;	 Catch:{ all -> 0x0243 }
        r2 = lightStateToString(r2);	 Catch:{ all -> 0x0243 }
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x0243 }
        r2 = ", deep state: ";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x0243 }
        r0 = r21;
        r2 = r0.mState;	 Catch:{ all -> 0x0243 }
        r2 = stateToString(r2);	 Catch:{ all -> 0x0243 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0243 }
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0240 }
        goto L_0x004e;
    L_0x0240:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x0243:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0240 }
        throw r2;	 Catch:{ all -> 0x0240 }
    L_0x0248:
        r2 = "get";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x0314;
    L_0x0253:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        monitor-enter(r21);
        r3 = r22.getNextArg();	 Catch:{ all -> 0x0284 }
        if (r3 == 0) goto L_0x030a;
    L_0x0265:
        r18 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x0284 }
        r2 = "light";
        r2 = r3.equals(r2);	 Catch:{ all -> 0x029e }
        if (r2 == 0) goto L_0x0287;
    L_0x0272:
        r0 = r21;
        r2 = r0.mLightState;	 Catch:{ all -> 0x029e }
        r2 = lightStateToString(r2);	 Catch:{ all -> 0x029e }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x029e }
    L_0x027f:
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0284 }
        goto L_0x004e;
    L_0x0284:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x0287:
        r2 = "deep";
        r2 = r3.equals(r2);	 Catch:{ all -> 0x029e }
        if (r2 == 0) goto L_0x02a3;
    L_0x0290:
        r0 = r21;
        r2 = r0.mState;	 Catch:{ all -> 0x029e }
        r2 = stateToString(r2);	 Catch:{ all -> 0x029e }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x029e }
        goto L_0x027f;
    L_0x029e:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0284 }
        throw r2;	 Catch:{ all -> 0x0284 }
    L_0x02a3:
        r2 = "force";
        r2 = r3.equals(r2);	 Catch:{ all -> 0x029e }
        if (r2 == 0) goto L_0x02b6;
    L_0x02ac:
        r0 = r21;
        r2 = r0.mForceIdle;	 Catch:{ all -> 0x029e }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x029e }
        goto L_0x027f;
    L_0x02b6:
        r2 = "screen";
        r2 = r3.equals(r2);	 Catch:{ all -> 0x029e }
        if (r2 == 0) goto L_0x02c9;
    L_0x02bf:
        r0 = r21;
        r2 = r0.mScreenOn;	 Catch:{ all -> 0x029e }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x029e }
        goto L_0x027f;
    L_0x02c9:
        r2 = "charging";
        r2 = r3.equals(r2);	 Catch:{ all -> 0x029e }
        if (r2 == 0) goto L_0x02dc;
    L_0x02d2:
        r0 = r21;
        r2 = r0.mCharging;	 Catch:{ all -> 0x029e }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x029e }
        goto L_0x027f;
    L_0x02dc:
        r2 = "network";
        r2 = r3.equals(r2);	 Catch:{ all -> 0x029e }
        if (r2 == 0) goto L_0x02ef;
    L_0x02e5:
        r0 = r21;
        r2 = r0.mNetworkConnected;	 Catch:{ all -> 0x029e }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x029e }
        goto L_0x027f;
    L_0x02ef:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x029e }
        r2.<init>();	 Catch:{ all -> 0x029e }
        r6 = "Unknown get option: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x029e }
        r2 = r2.append(r3);	 Catch:{ all -> 0x029e }
        r2 = r2.toString();	 Catch:{ all -> 0x029e }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x029e }
        goto L_0x027f;
    L_0x030a:
        r2 = "Argument required";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0284 }
        goto L_0x004e;
    L_0x0314:
        r2 = "disable";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x03d8;
    L_0x031f:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        monitor-enter(r21);
        r18 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x03ce }
        r3 = r22.getNextArg();	 Catch:{ all -> 0x03ce }
        r8 = 0;
        r20 = 0;
        if (r3 == 0) goto L_0x034a;
    L_0x0338:
        r2 = "deep";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x03d3 }
        if (r2 != 0) goto L_0x034a;
    L_0x0341:
        r2 = "all";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x03d3 }
        if (r2 == 0) goto L_0x0360;
    L_0x034a:
        r20 = 1;
        r0 = r21;
        r2 = r0.mDeepEnabled;	 Catch:{ all -> 0x03d3 }
        if (r2 == 0) goto L_0x0360;
    L_0x0352:
        r2 = 0;
        r0 = r21;
        r0.mDeepEnabled = r2;	 Catch:{ all -> 0x03d3 }
        r8 = 1;
        r2 = "Deep idle mode disabled";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x03d3 }
    L_0x0360:
        if (r3 == 0) goto L_0x0374;
    L_0x0362:
        r2 = "light";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x03d3 }
        if (r2 != 0) goto L_0x0374;
    L_0x036b:
        r2 = "all";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x03d3 }
        if (r2 == 0) goto L_0x038a;
    L_0x0374:
        r20 = 1;
        r0 = r21;
        r2 = r0.mLightEnabled;	 Catch:{ all -> 0x03d3 }
        if (r2 == 0) goto L_0x038a;
    L_0x037c:
        r2 = 0;
        r0 = r21;
        r0.mLightEnabled = r2;	 Catch:{ all -> 0x03d3 }
        r8 = 1;
        r2 = "Light idle mode disabled";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x03d3 }
    L_0x038a:
        if (r8 == 0) goto L_0x03ae;
    L_0x038c:
        r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x03d3 }
        r6.<init>();	 Catch:{ all -> 0x03d3 }
        if (r3 != 0) goto L_0x03d1;
    L_0x0393:
        r2 = "all";
    L_0x0396:
        r2 = r6.append(r2);	 Catch:{ all -> 0x03d3 }
        r6 = "-disabled";
        r2 = r2.append(r6);	 Catch:{ all -> 0x03d3 }
        r2 = r2.toString();	 Catch:{ all -> 0x03d3 }
        r6 = android.os.Process.myUid();	 Catch:{ all -> 0x03d3 }
        r0 = r21;
        r0.becomeActiveLocked(r2, r6);	 Catch:{ all -> 0x03d3 }
    L_0x03ae:
        if (r20 != 0) goto L_0x03c9;
    L_0x03b0:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x03d3 }
        r2.<init>();	 Catch:{ all -> 0x03d3 }
        r6 = "Unknown idle mode: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x03d3 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x03d3 }
        r2 = r2.toString();	 Catch:{ all -> 0x03d3 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x03d3 }
    L_0x03c9:
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x03ce }
        goto L_0x004e;
    L_0x03ce:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x03d1:
        r2 = r3;
        goto L_0x0396;
    L_0x03d3:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x03ce }
        throw r2;	 Catch:{ all -> 0x03ce }
    L_0x03d8:
        r2 = "enable";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x047b;
    L_0x03e3:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        monitor-enter(r21);
        r18 = android.os.Binder.clearCallingIdentity();	 Catch:{ all -> 0x0473 }
        r3 = r22.getNextArg();	 Catch:{ all -> 0x0473 }
        r9 = 0;
        r20 = 0;
        if (r3 == 0) goto L_0x040e;
    L_0x03fc:
        r2 = "deep";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x0476 }
        if (r2 != 0) goto L_0x040e;
    L_0x0405:
        r2 = "all";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x0476 }
        if (r2 == 0) goto L_0x0424;
    L_0x040e:
        r20 = 1;
        r0 = r21;
        r2 = r0.mDeepEnabled;	 Catch:{ all -> 0x0476 }
        if (r2 != 0) goto L_0x0424;
    L_0x0416:
        r2 = 1;
        r0 = r21;
        r0.mDeepEnabled = r2;	 Catch:{ all -> 0x0476 }
        r9 = 1;
        r2 = "Deep idle mode enabled";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0476 }
    L_0x0424:
        if (r3 == 0) goto L_0x0438;
    L_0x0426:
        r2 = "light";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x0476 }
        if (r2 != 0) goto L_0x0438;
    L_0x042f:
        r2 = "all";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x0476 }
        if (r2 == 0) goto L_0x044e;
    L_0x0438:
        r20 = 1;
        r0 = r21;
        r2 = r0.mLightEnabled;	 Catch:{ all -> 0x0476 }
        if (r2 != 0) goto L_0x044e;
    L_0x0440:
        r2 = 1;
        r0 = r21;
        r0.mLightEnabled = r2;	 Catch:{ all -> 0x0476 }
        r9 = 1;
        r2 = "Light idle mode enable";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0476 }
    L_0x044e:
        if (r9 == 0) goto L_0x0453;
    L_0x0450:
        r21.becomeInactiveIfAppropriateLocked();	 Catch:{ all -> 0x0476 }
    L_0x0453:
        if (r20 != 0) goto L_0x046e;
    L_0x0455:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0476 }
        r2.<init>();	 Catch:{ all -> 0x0476 }
        r6 = "Unknown idle mode: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x0476 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0476 }
        r2 = r2.toString();	 Catch:{ all -> 0x0476 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0476 }
    L_0x046e:
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0473 }
        goto L_0x004e;
    L_0x0473:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x0476:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);	 Catch:{ all -> 0x0473 }
        throw r2;	 Catch:{ all -> 0x0473 }
    L_0x047b:
        r2 = "enabled";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x050e;
    L_0x0486:
        monitor-enter(r21);
        r3 = r22.getNextArg();	 Catch:{ all -> 0x04ac }
        if (r3 == 0) goto L_0x0496;
    L_0x048d:
        r2 = "all";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x04ac }
        if (r2 == 0) goto L_0x04b5;
    L_0x0496:
        r0 = r21;
        r2 = r0.mDeepEnabled;	 Catch:{ all -> 0x04ac }
        if (r2 == 0) goto L_0x04af;
    L_0x049c:
        r0 = r21;
        r2 = r0.mLightEnabled;	 Catch:{ all -> 0x04ac }
        if (r2 == 0) goto L_0x04af;
    L_0x04a2:
        r2 = "1";
    L_0x04a5:
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x04ac }
        goto L_0x004e;
    L_0x04ac:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x04af:
        r2 = 0;
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ all -> 0x04ac }
        goto L_0x04a5;
    L_0x04b5:
        r2 = "deep";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x04ac }
        if (r2 == 0) goto L_0x04d4;
    L_0x04be:
        r0 = r21;
        r2 = r0.mDeepEnabled;	 Catch:{ all -> 0x04ac }
        if (r2 == 0) goto L_0x04ce;
    L_0x04c4:
        r2 = "1";
    L_0x04c7:
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x04ac }
        goto L_0x004e;
    L_0x04ce:
        r2 = 0;
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ all -> 0x04ac }
        goto L_0x04c7;
    L_0x04d4:
        r2 = "light";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x04ac }
        if (r2 == 0) goto L_0x04f3;
    L_0x04dd:
        r0 = r21;
        r2 = r0.mLightEnabled;	 Catch:{ all -> 0x04ac }
        if (r2 == 0) goto L_0x04ed;
    L_0x04e3:
        r2 = "1";
    L_0x04e6:
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x04ac }
        goto L_0x004e;
    L_0x04ed:
        r2 = 0;
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ all -> 0x04ac }
        goto L_0x04e6;
    L_0x04f3:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x04ac }
        r2.<init>();	 Catch:{ all -> 0x04ac }
        r6 = "Unknown idle mode: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x04ac }
        r2 = r2.append(r3);	 Catch:{ all -> 0x04ac }
        r2 = r2.toString();	 Catch:{ all -> 0x04ac }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x04ac }
        goto L_0x004e;
    L_0x050e:
        r2 = "whitelist";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x06b7;
    L_0x0519:
        r3 = r22.getNextArg();
        if (r3 == 0) goto L_0x0605;
    L_0x051f:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        r18 = android.os.Binder.clearCallingIdentity();
    L_0x052e:
        r2 = r3.length();	 Catch:{ all -> 0x05c8 }
        r6 = 1;
        if (r2 < r6) goto L_0x0550;
    L_0x0535:
        r2 = 0;
        r2 = r3.charAt(r2);	 Catch:{ all -> 0x05c8 }
        r6 = 45;
        if (r2 == r6) goto L_0x056e;
    L_0x053e:
        r2 = 0;
        r2 = r3.charAt(r2);	 Catch:{ all -> 0x05c8 }
        r6 = 43;
        if (r2 == r6) goto L_0x056e;
    L_0x0547:
        r2 = 0;
        r2 = r3.charAt(r2);	 Catch:{ all -> 0x05c8 }
        r6 = 61;
        if (r2 == r6) goto L_0x056e;
    L_0x0550:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x05c8 }
        r2.<init>();	 Catch:{ all -> 0x05c8 }
        r6 = "Package must be prefixed with +, -, or =: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x05c8 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x05c8 }
        r2 = r2.toString();	 Catch:{ all -> 0x05c8 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x05c8 }
        r2 = -1;
        android.os.Binder.restoreCallingIdentity(r18);
        return r2;
    L_0x056e:
        r2 = 0;
        r14 = r3.charAt(r2);	 Catch:{ all -> 0x05c8 }
        r2 = 1;
        r16 = r3.substring(r2);	 Catch:{ all -> 0x05c8 }
        r2 = 43;
        if (r14 != r2) goto L_0x05cd;
    L_0x057c:
        r0 = r21;
        r1 = r16;
        r2 = r0.addPowerSaveWhitelistAppInternal(r1);	 Catch:{ all -> 0x05c8 }
        if (r2 == 0) goto L_0x05ac;
    L_0x0586:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x05c8 }
        r2.<init>();	 Catch:{ all -> 0x05c8 }
        r6 = "Added: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x05c8 }
        r0 = r16;
        r2 = r2.append(r0);	 Catch:{ all -> 0x05c8 }
        r2 = r2.toString();	 Catch:{ all -> 0x05c8 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x05c8 }
    L_0x05a1:
        r3 = r22.getNextArg();	 Catch:{ all -> 0x05c8 }
        if (r3 != 0) goto L_0x052e;
    L_0x05a7:
        android.os.Binder.restoreCallingIdentity(r18);
        goto L_0x004f;
    L_0x05ac:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x05c8 }
        r2.<init>();	 Catch:{ all -> 0x05c8 }
        r6 = "Unknown package: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x05c8 }
        r0 = r16;
        r2 = r2.append(r0);	 Catch:{ all -> 0x05c8 }
        r2 = r2.toString();	 Catch:{ all -> 0x05c8 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x05c8 }
        goto L_0x05a1;
    L_0x05c8:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);
        throw r2;
    L_0x05cd:
        r2 = 45;
        if (r14 != r2) goto L_0x05f7;
    L_0x05d1:
        r0 = r21;
        r1 = r16;
        r2 = r0.removePowerSaveWhitelistAppInternal(r1);	 Catch:{ all -> 0x05c8 }
        if (r2 == 0) goto L_0x05a1;
    L_0x05db:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x05c8 }
        r2.<init>();	 Catch:{ all -> 0x05c8 }
        r6 = "Removed: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x05c8 }
        r0 = r16;
        r2 = r2.append(r0);	 Catch:{ all -> 0x05c8 }
        r2 = r2.toString();	 Catch:{ all -> 0x05c8 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x05c8 }
        goto L_0x05a1;
    L_0x05f7:
        r0 = r21;
        r1 = r16;
        r2 = r0.getPowerSaveWhitelistAppInternal(r1);	 Catch:{ all -> 0x05c8 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x05c8 }
        goto L_0x05a1;
    L_0x0605:
        monitor-enter(r21);
        r13 = 0;
    L_0x0607:
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistAppsExceptIdle;	 Catch:{ all -> 0x06b4 }
        r2 = r2.size();	 Catch:{ all -> 0x06b4 }
        if (r13 >= r2) goto L_0x0640;
    L_0x0611:
        r2 = "system-excidle,";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistAppsExceptIdle;	 Catch:{ all -> 0x06b4 }
        r2 = r2.keyAt(r13);	 Catch:{ all -> 0x06b4 }
        r2 = (java.lang.String) r2;	 Catch:{ all -> 0x06b4 }
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r2 = ",";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistAppsExceptIdle;	 Catch:{ all -> 0x06b4 }
        r2 = r2.valueAt(r13);	 Catch:{ all -> 0x06b4 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x06b4 }
        r13 = r13 + 1;
        goto L_0x0607;
    L_0x0640:
        r13 = 0;
    L_0x0641:
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistApps;	 Catch:{ all -> 0x06b4 }
        r2 = r2.size();	 Catch:{ all -> 0x06b4 }
        if (r13 >= r2) goto L_0x067a;
    L_0x064b:
        r2 = "system,";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistApps;	 Catch:{ all -> 0x06b4 }
        r2 = r2.keyAt(r13);	 Catch:{ all -> 0x06b4 }
        r2 = (java.lang.String) r2;	 Catch:{ all -> 0x06b4 }
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r2 = ",";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistApps;	 Catch:{ all -> 0x06b4 }
        r2 = r2.valueAt(r13);	 Catch:{ all -> 0x06b4 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x06b4 }
        r13 = r13 + 1;
        goto L_0x0641;
    L_0x067a:
        r13 = 0;
    L_0x067b:
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistUserApps;	 Catch:{ all -> 0x06b4 }
        r2 = r2.size();	 Catch:{ all -> 0x06b4 }
        if (r13 >= r2) goto L_0x004e;
    L_0x0685:
        r2 = "user,";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistUserApps;	 Catch:{ all -> 0x06b4 }
        r2 = r2.keyAt(r13);	 Catch:{ all -> 0x06b4 }
        r2 = (java.lang.String) r2;	 Catch:{ all -> 0x06b4 }
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r2 = ",";
        r0 = r17;
        r0.print(r2);	 Catch:{ all -> 0x06b4 }
        r0 = r21;
        r2 = r0.mPowerSaveWhitelistUserApps;	 Catch:{ all -> 0x06b4 }
        r2 = r2.valueAt(r13);	 Catch:{ all -> 0x06b4 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x06b4 }
        r13 = r13 + 1;
        goto L_0x067b;
    L_0x06b4:
        r2 = move-exception;
        monitor-exit(r21);
        throw r2;
    L_0x06b7:
        r2 = "tempwhitelist";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x0744;
    L_0x06c2:
        r4 = 10000; // 0x2710 float:1.4013E-41 double:4.9407E-320;
    L_0x06c4:
        r15 = r22.getNextOption();
        if (r15 == 0) goto L_0x070a;
    L_0x06ca:
        r2 = "-u";
        r2 = r2.equals(r15);
        if (r2 == 0) goto L_0x06ec;
    L_0x06d3:
        r15 = r22.getNextArg();
        if (r15 != 0) goto L_0x06e3;
    L_0x06d9:
        r2 = "-u requires a user number";
        r0 = r17;
        r0.println(r2);
        r2 = -1;
        return r2;
    L_0x06e3:
        r2 = java.lang.Integer.parseInt(r15);
        r0 = r22;
        r0.userId = r2;
        goto L_0x06c4;
    L_0x06ec:
        r2 = "-d";
        r2 = r2.equals(r15);
        if (r2 == 0) goto L_0x06c4;
    L_0x06f5:
        r15 = r22.getNextArg();
        if (r15 != 0) goto L_0x0705;
    L_0x06fb:
        r2 = "-d requires a duration";
        r0 = r17;
        r0.println(r2);
        r2 = -1;
        return r2;
    L_0x0705:
        r4 = java.lang.Long.parseLong(r15);
        goto L_0x06c4;
    L_0x070a:
        r3 = r22.getNextArg();
        if (r3 == 0) goto L_0x073a;
    L_0x0710:
        r0 = r22;
        r6 = r0.userId;	 Catch:{ Exception -> 0x071e }
        r7 = "shell";
        r2 = r21;
        r2.addPowerSaveTempWhitelistAppChecked(r3, r4, r6, r7);	 Catch:{ Exception -> 0x071e }
        goto L_0x004f;
    L_0x071e:
        r12 = move-exception;
        r2 = new java.lang.StringBuilder;
        r2.<init>();
        r6 = "Failed: ";
        r2 = r2.append(r6);
        r2 = r2.append(r12);
        r2 = r2.toString();
        r0 = r17;
        r0.println(r2);
        r2 = -1;
        return r2;
    L_0x073a:
        r2 = 0;
        r0 = r21;
        r1 = r17;
        r0.dumpTempWhitelistSchedule(r1, r2);
        goto L_0x004f;
    L_0x0744:
        r2 = "except-idle-whitelist";
        r0 = r23;
        r2 = r2.equals(r0);
        if (r2 == 0) goto L_0x084c;
    L_0x074f:
        r2 = r21.getContext();
        r6 = "android.permission.DEVICE_POWER";
        r7 = 0;
        r2.enforceCallingOrSelfPermission(r6, r7);
        r18 = android.os.Binder.clearCallingIdentity();
        r3 = r22.getNextArg();	 Catch:{ all -> 0x0817 }
        if (r3 != 0) goto L_0x0771;
    L_0x0764:
        r2 = "No arguments given";
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0817 }
        r2 = -1;
        android.os.Binder.restoreCallingIdentity(r18);
        return r2;
    L_0x0771:
        r2 = "reset";
        r2 = r2.equals(r3);	 Catch:{ all -> 0x0817 }
        if (r2 == 0) goto L_0x07bb;
    L_0x077a:
        r21.resetPowerSaveWhitelistExceptIdleInternal();	 Catch:{ all -> 0x0817 }
    L_0x077d:
        android.os.Binder.restoreCallingIdentity(r18);
        goto L_0x004f;
    L_0x0782:
        r2 = 0;
        r14 = r3.charAt(r2);	 Catch:{ all -> 0x0817 }
        r2 = 1;
        r16 = r3.substring(r2);	 Catch:{ all -> 0x0817 }
        r2 = 43;
        if (r14 != r2) goto L_0x081c;
    L_0x0790:
        r0 = r21;
        r1 = r16;
        r2 = r0.addPowerSaveWhitelistExceptIdleInternal(r1);	 Catch:{ all -> 0x0817 }
        if (r2 == 0) goto L_0x07fb;
    L_0x079a:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0817 }
        r2.<init>();	 Catch:{ all -> 0x0817 }
        r6 = "Added: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x0817 }
        r0 = r16;
        r2 = r2.append(r0);	 Catch:{ all -> 0x0817 }
        r2 = r2.toString();	 Catch:{ all -> 0x0817 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0817 }
    L_0x07b5:
        r3 = r22.getNextArg();	 Catch:{ all -> 0x0817 }
        if (r3 == 0) goto L_0x077d;
    L_0x07bb:
        r2 = r3.length();	 Catch:{ all -> 0x0817 }
        r6 = 1;
        if (r2 < r6) goto L_0x07dd;
    L_0x07c2:
        r2 = 0;
        r2 = r3.charAt(r2);	 Catch:{ all -> 0x0817 }
        r6 = 45;
        if (r2 == r6) goto L_0x0782;
    L_0x07cb:
        r2 = 0;
        r2 = r3.charAt(r2);	 Catch:{ all -> 0x0817 }
        r6 = 43;
        if (r2 == r6) goto L_0x0782;
    L_0x07d4:
        r2 = 0;
        r2 = r3.charAt(r2);	 Catch:{ all -> 0x0817 }
        r6 = 61;
        if (r2 == r6) goto L_0x0782;
    L_0x07dd:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0817 }
        r2.<init>();	 Catch:{ all -> 0x0817 }
        r6 = "Package must be prefixed with +, -, or =: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x0817 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0817 }
        r2 = r2.toString();	 Catch:{ all -> 0x0817 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0817 }
        r2 = -1;
        android.os.Binder.restoreCallingIdentity(r18);
        return r2;
    L_0x07fb:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0817 }
        r2.<init>();	 Catch:{ all -> 0x0817 }
        r6 = "Unknown package: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x0817 }
        r0 = r16;
        r2 = r2.append(r0);	 Catch:{ all -> 0x0817 }
        r2 = r2.toString();	 Catch:{ all -> 0x0817 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0817 }
        goto L_0x07b5;
    L_0x0817:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r18);
        throw r2;
    L_0x081c:
        r2 = 61;
        if (r14 != r2) goto L_0x082e;
    L_0x0820:
        r0 = r21;
        r1 = r16;
        r2 = r0.getPowerSaveWhitelistExceptIdleInternal(r1);	 Catch:{ all -> 0x0817 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0817 }
        goto L_0x07b5;
    L_0x082e:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0817 }
        r2.<init>();	 Catch:{ all -> 0x0817 }
        r6 = "Unknown argument: ";
        r2 = r2.append(r6);	 Catch:{ all -> 0x0817 }
        r2 = r2.append(r3);	 Catch:{ all -> 0x0817 }
        r2 = r2.toString();	 Catch:{ all -> 0x0817 }
        r0 = r17;
        r0.println(r2);	 Catch:{ all -> 0x0817 }
        r2 = -1;
        android.os.Binder.restoreCallingIdentity(r18);
        return r2;
    L_0x084c:
        r2 = r22.handleDefaultCommands(r23);
        return r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.DeviceIdleController.onShellCommand(com.android.server.DeviceIdleController$Shell, java.lang.String):int");
    }

    void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        if (DumpUtils.checkDumpPermission(getContext(), TAG, pw)) {
            int i;
            if (args != null) {
                int userId = 0;
                i = 0;
                while (i < args.length) {
                    String arg = args[i];
                    if ("-h".equals(arg)) {
                        dumpHelp(pw);
                        return;
                    }
                    if ("-u".equals(arg)) {
                        i++;
                        if (i < args.length) {
                            userId = Integer.parseInt(args[i]);
                        }
                    } else if (!"-a".equals(arg)) {
                        if (arg.length() <= 0 || arg.charAt(0) != '-') {
                            Shell shell = new Shell();
                            shell.userId = userId;
                            String[] newArgs = new String[(args.length - i)];
                            System.arraycopy(args, i, newArgs, 0, args.length - i);
                            shell.exec(this.mBinderService, null, fd, null, newArgs, null, new ResultReceiver(null));
                            return;
                        }
                        pw.println("Unknown option: " + arg);
                        return;
                    }
                    i++;
                }
            }
            synchronized (this) {
                this.mConstants.dump(pw);
                if (this.mEventCmds[0] != 0) {
                    pw.println("  Idling history:");
                    long now = SystemClock.elapsedRealtime();
                    for (i = 99; i >= 0; i--) {
                        if (this.mEventCmds[i] != 0) {
                            String label;
                            switch (this.mEventCmds[i]) {
                                case 1:
                                    label = "     normal";
                                    break;
                                case 2:
                                    label = " light-idle";
                                    break;
                                case 3:
                                    label = "light-maint";
                                    break;
                                case 4:
                                    label = "  deep-idle";
                                    break;
                                case 5:
                                    label = " deep-maint";
                                    break;
                                default:
                                    label = "         ??";
                                    break;
                            }
                            pw.print("    ");
                            pw.print(label);
                            pw.print(": ");
                            TimeUtils.formatDuration(this.mEventTimes[i], now, pw);
                            pw.println();
                        }
                    }
                }
                int size = this.mPowerSaveWhitelistAppsExceptIdle.size();
                if (size > 0) {
                    pw.println("  Whitelist (except idle) system apps:");
                    for (i = 0; i < size; i++) {
                        pw.print("    ");
                        pw.println((String) this.mPowerSaveWhitelistAppsExceptIdle.keyAt(i));
                    }
                }
                size = this.mPowerSaveWhitelistApps.size();
                if (size > 0) {
                    pw.println("  Whitelist system apps:");
                    for (i = 0; i < size; i++) {
                        pw.print("    ");
                        pw.println((String) this.mPowerSaveWhitelistApps.keyAt(i));
                    }
                }
                size = this.mPowerSaveWhitelistUserApps.size();
                if (size > 0) {
                    pw.println("  Whitelist user apps:");
                    for (i = 0; i < size; i++) {
                        pw.print("    ");
                        pw.println((String) this.mPowerSaveWhitelistUserApps.keyAt(i));
                    }
                }
                size = this.mPowerSaveWhitelistExceptIdleAppIds.size();
                if (size > 0) {
                    pw.println("  Whitelist (except idle) all app ids:");
                    for (i = 0; i < size; i++) {
                        pw.print("    ");
                        pw.print(this.mPowerSaveWhitelistExceptIdleAppIds.keyAt(i));
                        pw.println();
                    }
                }
                size = this.mPowerSaveWhitelistUserAppIds.size();
                if (size > 0) {
                    pw.println("  Whitelist user app ids:");
                    for (i = 0; i < size; i++) {
                        pw.print("    ");
                        pw.print(this.mPowerSaveWhitelistUserAppIds.keyAt(i));
                        pw.println();
                    }
                }
                size = this.mPowerSaveWhitelistAllAppIds.size();
                if (size > 0) {
                    pw.println("  Whitelist all app ids:");
                    for (i = 0; i < size; i++) {
                        pw.print("    ");
                        pw.print(this.mPowerSaveWhitelistAllAppIds.keyAt(i));
                        pw.println();
                    }
                }
                dumpTempWhitelistSchedule(pw, true);
                size = this.mTempWhitelistAppIdArray != null ? this.mTempWhitelistAppIdArray.length : 0;
                if (size > 0) {
                    pw.println("  Temp whitelist app ids:");
                    for (i = 0; i < size; i++) {
                        pw.print("    ");
                        pw.print(this.mTempWhitelistAppIdArray[i]);
                        pw.println();
                    }
                }
                pw.print("  mLightEnabled=");
                pw.print(this.mLightEnabled);
                pw.print("  mDeepEnabled=");
                pw.println(this.mDeepEnabled);
                pw.print("  mForceIdle=");
                pw.println(this.mForceIdle);
                pw.print("  mMotionSensor=");
                pw.println(this.mMotionSensor);
                pw.print("  mScreenOn=");
                pw.println(this.mScreenOn);
                pw.print("  mNetworkConnected=");
                pw.println(this.mNetworkConnected);
                pw.print("  mCharging=");
                pw.println(this.mCharging);
                pw.print("  mMotionActive=");
                pw.println(this.mMotionListener.active);
                pw.print("  mNotMoving=");
                pw.println(this.mNotMoving);
                pw.print("  mLocating=");
                pw.print(this.mLocating);
                pw.print(" mHasGps=");
                pw.print(this.mHasGps);
                pw.print(" mHasNetwork=");
                pw.print(this.mHasNetworkLocation);
                pw.print(" mLocated=");
                pw.println(this.mLocated);
                if (this.mLastGenericLocation != null) {
                    pw.print("  mLastGenericLocation=");
                    pw.println(this.mLastGenericLocation);
                }
                if (this.mLastGpsLocation != null) {
                    pw.print("  mLastGpsLocation=");
                    pw.println(this.mLastGpsLocation);
                }
                pw.print("  mState=");
                pw.print(stateToString(this.mState));
                pw.print(" mLightState=");
                pw.println(lightStateToString(this.mLightState));
                pw.print("  mInactiveTimeout=");
                TimeUtils.formatDuration(this.mInactiveTimeout, pw);
                pw.println();
                if (this.mActiveIdleOpCount != 0) {
                    pw.print("  mActiveIdleOpCount=");
                    pw.println(this.mActiveIdleOpCount);
                }
                if (this.mNextAlarmTime != 0) {
                    pw.print("  mNextAlarmTime=");
                    TimeUtils.formatDuration(this.mNextAlarmTime, SystemClock.elapsedRealtime(), pw);
                    pw.println();
                }
                if (this.mNextIdlePendingDelay != 0) {
                    pw.print("  mNextIdlePendingDelay=");
                    TimeUtils.formatDuration(this.mNextIdlePendingDelay, pw);
                    pw.println();
                }
                if (this.mNextIdleDelay != 0) {
                    pw.print("  mNextIdleDelay=");
                    TimeUtils.formatDuration(this.mNextIdleDelay, pw);
                    pw.println();
                }
                if (this.mNextLightIdleDelay != 0) {
                    pw.print("  mNextIdleDelay=");
                    TimeUtils.formatDuration(this.mNextLightIdleDelay, pw);
                    pw.println();
                }
                if (this.mNextLightAlarmTime != 0) {
                    pw.print("  mNextLightAlarmTime=");
                    TimeUtils.formatDuration(this.mNextLightAlarmTime, SystemClock.elapsedRealtime(), pw);
                    pw.println();
                }
                if (this.mCurIdleBudget != 0) {
                    pw.print("  mCurIdleBudget=");
                    TimeUtils.formatDuration(this.mCurIdleBudget, pw);
                    pw.println();
                }
                if (this.mMaintenanceStartTime != 0) {
                    pw.print("  mMaintenanceStartTime=");
                    TimeUtils.formatDuration(this.mMaintenanceStartTime, SystemClock.elapsedRealtime(), pw);
                    pw.println();
                }
                if (this.mJobsActive) {
                    pw.print("  mJobsActive=");
                    pw.println(this.mJobsActive);
                }
                if (this.mAlarmsActive) {
                    pw.print("  mAlarmsActive=");
                    pw.println(this.mAlarmsActive);
                }
            }
        }
    }

    void dumpTempWhitelistSchedule(PrintWriter pw, boolean printTitle) {
        int size = this.mTempWhitelistAppIdEndTimes.size();
        if (size > 0) {
            String prefix = "";
            if (printTitle) {
                pw.println("  Temp whitelist schedule:");
                prefix = "    ";
            }
            long timeNow = SystemClock.elapsedRealtime();
            for (int i = 0; i < size; i++) {
                pw.print(prefix);
                pw.print("UID=");
                pw.print(this.mTempWhitelistAppIdEndTimes.keyAt(i));
                pw.print(": ");
                Pair<MutableLong, String> entry = (Pair) this.mTempWhitelistAppIdEndTimes.valueAt(i);
                TimeUtils.formatDuration(((MutableLong) entry.first).value, timeNow, pw);
                pw.print(" - ");
                pw.println((String) entry.second);
            }
        }
    }
}
