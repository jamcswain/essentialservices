package com.android.server.soundtrigger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.soundtrigger.IRecognitionStatusCallback;
import android.hardware.soundtrigger.SoundTrigger;
import android.hardware.soundtrigger.SoundTrigger.GenericRecognitionEvent;
import android.hardware.soundtrigger.SoundTrigger.GenericSoundModel;
import android.hardware.soundtrigger.SoundTrigger.KeyphraseRecognitionEvent;
import android.hardware.soundtrigger.SoundTrigger.KeyphraseRecognitionExtra;
import android.hardware.soundtrigger.SoundTrigger.ModuleProperties;
import android.hardware.soundtrigger.SoundTrigger.RecognitionConfig;
import android.hardware.soundtrigger.SoundTrigger.RecognitionEvent;
import android.hardware.soundtrigger.SoundTrigger.SoundModel;
import android.hardware.soundtrigger.SoundTrigger.SoundModelEvent;
import android.hardware.soundtrigger.SoundTrigger.StatusListener;
import android.hardware.soundtrigger.SoundTriggerModule;
import android.os.DeadObjectException;
import android.os.PowerManager;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Slog;
import com.android.internal.logging.MetricsLogger;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;

public class SoundTriggerHelper implements StatusListener {
    static final boolean DBG = false;
    private static final int INVALID_VALUE = Integer.MIN_VALUE;
    public static final int STATUS_ERROR = Integer.MIN_VALUE;
    public static final int STATUS_OK = 0;
    static final String TAG = "SoundTriggerHelper";
    private boolean mCallActive = false;
    private final Context mContext;
    private boolean mIsPowerSaveMode = false;
    private HashMap<Integer, UUID> mKeyphraseUuidMap;
    private final Object mLock = new Object();
    private final HashMap<UUID, ModelData> mModelDataMap;
    private SoundTriggerModule mModule;
    final ModuleProperties mModuleProperties;
    private final PhoneStateListener mPhoneStateListener;
    private final PowerManager mPowerManager;
    private PowerSaveModeListener mPowerSaveModeListener;
    private boolean mRecognitionRunning = false;
    private boolean mServiceDisabled = false;
    private final TelephonyManager mTelephonyManager;

    private static class ModelData {
        static final int MODEL_LOADED = 1;
        static final int MODEL_NOTLOADED = 0;
        static final int MODEL_STARTED = 2;
        private IRecognitionStatusCallback mCallback = null;
        private int mModelHandle = Integer.MIN_VALUE;
        private UUID mModelId;
        private int mModelState;
        private int mModelType = -1;
        private RecognitionConfig mRecognitionConfig = null;
        private boolean mRequested = false;
        private SoundModel mSoundModel = null;

        private ModelData(UUID modelId, int modelType) {
            this.mModelId = modelId;
            this.mModelType = modelType;
        }

        static ModelData createKeyphraseModelData(UUID modelId) {
            return new ModelData(modelId, 0);
        }

        static ModelData createGenericModelData(UUID modelId) {
            return new ModelData(modelId, 1);
        }

        static ModelData createModelDataOfUnknownType(UUID modelId) {
            return new ModelData(modelId, -1);
        }

        synchronized void setCallback(IRecognitionStatusCallback callback) {
            this.mCallback = callback;
        }

        synchronized IRecognitionStatusCallback getCallback() {
            return this.mCallback;
        }

        synchronized boolean isModelLoaded() {
            boolean z = true;
            synchronized (this) {
                if (!(this.mModelState == 1 || this.mModelState == 2)) {
                    z = false;
                }
            }
            return z;
        }

        synchronized boolean isModelNotLoaded() {
            boolean z = false;
            synchronized (this) {
                if (this.mModelState == 0) {
                    z = true;
                }
            }
            return z;
        }

        synchronized void setStarted() {
            this.mModelState = 2;
        }

        synchronized void setStopped() {
            this.mModelState = 1;
        }

        synchronized void setLoaded() {
            this.mModelState = 1;
        }

        synchronized boolean isModelStarted() {
            return this.mModelState == 2;
        }

        synchronized void clearState() {
            this.mModelState = 0;
            this.mModelHandle = Integer.MIN_VALUE;
            this.mRecognitionConfig = null;
            this.mRequested = false;
            this.mCallback = null;
        }

        synchronized void clearCallback() {
            this.mCallback = null;
        }

        synchronized void setHandle(int handle) {
            this.mModelHandle = handle;
        }

        synchronized void setRecognitionConfig(RecognitionConfig config) {
            this.mRecognitionConfig = config;
        }

        synchronized int getHandle() {
            return this.mModelHandle;
        }

        synchronized UUID getModelId() {
            return this.mModelId;
        }

        synchronized RecognitionConfig getRecognitionConfig() {
            return this.mRecognitionConfig;
        }

        synchronized boolean isRequested() {
            return this.mRequested;
        }

        synchronized void setRequested(boolean requested) {
            this.mRequested = requested;
        }

        synchronized void setSoundModel(SoundModel soundModel) {
            this.mSoundModel = soundModel;
        }

        synchronized SoundModel getSoundModel() {
            return this.mSoundModel;
        }

        synchronized int getModelType() {
            return this.mModelType;
        }

        synchronized boolean isKeyphraseModel() {
            boolean z = false;
            synchronized (this) {
                if (this.mModelType == 0) {
                    z = true;
                }
            }
            return z;
        }

        synchronized boolean isGenericModel() {
            boolean z = true;
            synchronized (this) {
                if (this.mModelType != 1) {
                    z = false;
                }
            }
            return z;
        }

        synchronized String stateToString() {
            switch (this.mModelState) {
                case 0:
                    return "NOT_LOADED";
                case 1:
                    return "LOADED";
                case 2:
                    return "STARTED";
                default:
                    return "Unknown state";
            }
        }

        synchronized String requestedToString() {
            return "Requested: " + (this.mRequested ? "Yes" : "No");
        }

        synchronized String callbackToString() {
            return "Callback: " + (this.mCallback != null ? this.mCallback.asBinder() : "null");
        }

        synchronized String uuidToString() {
            return "UUID: " + this.mModelId;
        }

        public synchronized String toString() {
            return "Handle: " + this.mModelHandle + "\n" + "ModelState: " + stateToString() + "\n" + requestedToString() + "\n" + callbackToString() + "\n" + uuidToString() + "\n" + modelTypeToString();
        }

        synchronized String modelTypeToString() {
            String type;
            type = null;
            switch (this.mModelType) {
                case -1:
                    type = "Unknown";
                    break;
                case 0:
                    type = "Keyphrase";
                    break;
                case 1:
                    type = "Generic";
                    break;
            }
            return "Model type: " + type + "\n";
        }
    }

    class MyCallStateListener extends PhoneStateListener {
        MyCallStateListener() {
        }

        public void onCallStateChanged(int state, String arg1) {
            boolean z = false;
            synchronized (SoundTriggerHelper.this.mLock) {
                SoundTriggerHelper soundTriggerHelper = SoundTriggerHelper.this;
                if (state != 0) {
                    z = true;
                }
                soundTriggerHelper.onCallStateChangedLocked(z);
            }
        }
    }

    class PowerSaveModeListener extends BroadcastReceiver {
        PowerSaveModeListener() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.os.action.POWER_SAVE_MODE_CHANGED".equals(intent.getAction())) {
                boolean active = SoundTriggerHelper.this.mPowerManager.getPowerSaveState(8).batterySaverEnabled;
                synchronized (SoundTriggerHelper.this.mLock) {
                    SoundTriggerHelper.this.onPowerSaveModeChangedLocked(active);
                }
            }
        }
    }

    SoundTriggerHelper(Context context) {
        ArrayList<ModuleProperties> modules = new ArrayList();
        int status = SoundTrigger.listModules(modules);
        this.mContext = context;
        this.mTelephonyManager = (TelephonyManager) context.getSystemService("phone");
        this.mPowerManager = (PowerManager) context.getSystemService("power");
        this.mModelDataMap = new HashMap();
        this.mKeyphraseUuidMap = new HashMap();
        this.mPhoneStateListener = new MyCallStateListener();
        if (status != 0 || modules.size() == 0) {
            Slog.w(TAG, "listModules status=" + status + ", # of modules=" + modules.size());
            this.mModuleProperties = null;
            this.mModule = null;
            return;
        }
        this.mModuleProperties = (ModuleProperties) modules.get(0);
    }

    int startGenericRecognition(UUID modelId, GenericSoundModel soundModel, IRecognitionStatusCallback callback, RecognitionConfig recognitionConfig) {
        MetricsLogger.count(this.mContext, "sth_start_recognition", 1);
        if (modelId == null || soundModel == null || callback == null || recognitionConfig == null) {
            Slog.w(TAG, "Passed in bad data to startGenericRecognition().");
            return Integer.MIN_VALUE;
        }
        synchronized (this.mLock) {
            ModelData modelData = getOrCreateGenericModelDataLocked(modelId);
            if (modelData == null) {
                Slog.w(TAG, "Irrecoverable error occurred, check UUID / sound model data.");
                return Integer.MIN_VALUE;
            }
            int startRecognition = startRecognition(soundModel, modelData, callback, recognitionConfig, Integer.MIN_VALUE);
            return startRecognition;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    int startKeyphraseRecognition(int r9, android.hardware.soundtrigger.SoundTrigger.KeyphraseSoundModel r10, android.hardware.soundtrigger.IRecognitionStatusCallback r11, android.hardware.soundtrigger.SoundTrigger.RecognitionConfig r12) {
        /*
        r8 = this;
        r4 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r7 = r8.mLock;
        monitor-enter(r7);
        r0 = r8.mContext;	 Catch:{ all -> 0x005e }
        r1 = "sth_start_recognition";
        r3 = 1;
        com.android.internal.logging.MetricsLogger.count(r0, r1, r3);	 Catch:{ all -> 0x005e }
        if (r10 == 0) goto L_0x0012;
    L_0x0010:
        if (r11 != 0) goto L_0x0014;
    L_0x0012:
        monitor-exit(r7);
        return r4;
    L_0x0014:
        if (r12 == 0) goto L_0x0012;
    L_0x0016:
        r2 = r8.getKeyphraseModelDataLocked(r9);	 Catch:{ all -> 0x005e }
        if (r2 == 0) goto L_0x002f;
    L_0x001c:
        r0 = r2.isKeyphraseModel();	 Catch:{ all -> 0x005e }
        r0 = r0 ^ 1;
        if (r0 == 0) goto L_0x002f;
    L_0x0024:
        r0 = "SoundTriggerHelper";
        r1 = "Generic model with same UUID exists.";
        android.util.Slog.e(r0, r1);	 Catch:{ all -> 0x005e }
        monitor-exit(r7);
        return r4;
    L_0x002f:
        if (r2 == 0) goto L_0x004b;
    L_0x0031:
        r0 = r2.getModelId();	 Catch:{ all -> 0x005e }
        r1 = r10.uuid;	 Catch:{ all -> 0x005e }
        r0 = r0.equals(r1);	 Catch:{ all -> 0x005e }
        r0 = r0 ^ 1;
        if (r0 == 0) goto L_0x004b;
    L_0x003f:
        r6 = r8.cleanUpExistingKeyphraseModelLocked(r2);	 Catch:{ all -> 0x005e }
        if (r6 == 0) goto L_0x0047;
    L_0x0045:
        monitor-exit(r7);
        return r6;
    L_0x0047:
        r8.removeKeyphraseModelLocked(r9);	 Catch:{ all -> 0x005e }
        r2 = 0;
    L_0x004b:
        if (r2 != 0) goto L_0x0053;
    L_0x004d:
        r0 = r10.uuid;	 Catch:{ all -> 0x005e }
        r2 = r8.createKeyphraseModelDataLocked(r0, r9);	 Catch:{ all -> 0x005e }
    L_0x0053:
        r0 = r8;
        r1 = r10;
        r3 = r11;
        r4 = r12;
        r5 = r9;
        r0 = r0.startRecognition(r1, r2, r3, r4, r5);	 Catch:{ all -> 0x005e }
        monitor-exit(r7);
        return r0;
    L_0x005e:
        r0 = move-exception;
        monitor-exit(r7);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.soundtrigger.SoundTriggerHelper.startKeyphraseRecognition(int, android.hardware.soundtrigger.SoundTrigger$KeyphraseSoundModel, android.hardware.soundtrigger.IRecognitionStatusCallback, android.hardware.soundtrigger.SoundTrigger$RecognitionConfig):int");
    }

    private int cleanUpExistingKeyphraseModelLocked(ModelData modelData) {
        int status = tryStopAndUnloadLocked(modelData, true, true);
        if (status != 0) {
            Slog.w(TAG, "Unable to stop or unload previous model: " + modelData.toString());
        }
        return status;
    }

    int startRecognition(SoundModel soundModel, ModelData modelData, IRecognitionStatusCallback callback, RecognitionConfig recognitionConfig, int keyphraseId) {
        synchronized (this.mLock) {
            if (this.mModuleProperties == null) {
                Slog.w(TAG, "Attempting startRecognition without the capability");
                return Integer.MIN_VALUE;
            }
            int status;
            if (this.mModule == null) {
                this.mModule = SoundTrigger.attachModule(this.mModuleProperties.id, this, null);
                if (this.mModule == null) {
                    Slog.w(TAG, "startRecognition cannot attach to sound trigger module");
                    return Integer.MIN_VALUE;
                }
            }
            if (!this.mRecognitionRunning) {
                initializeTelephonyAndPowerStateListeners();
            }
            if (modelData.getSoundModel() != null) {
                boolean stopModel = false;
                boolean unloadModel = false;
                if (modelData.getSoundModel().equals(soundModel) && modelData.isModelStarted()) {
                    stopModel = true;
                    unloadModel = false;
                } else if (!modelData.getSoundModel().equals(soundModel)) {
                    stopModel = modelData.isModelStarted();
                    unloadModel = modelData.isModelLoaded();
                }
                if (stopModel || unloadModel) {
                    status = tryStopAndUnloadLocked(modelData, stopModel, unloadModel);
                    if (status != 0) {
                        Slog.w(TAG, "Unable to stop or unload previous model: " + modelData.toString());
                        return status;
                    }
                }
            }
            IRecognitionStatusCallback oldCallback = modelData.getCallback();
            if (!(oldCallback == null || oldCallback.asBinder() == callback.asBinder())) {
                Slog.w(TAG, "Canceling previous recognition for model id: " + modelData.getModelId());
                try {
                    oldCallback.onError(Integer.MIN_VALUE);
                } catch (RemoteException e) {
                    Slog.w(TAG, "RemoteException in onDetectionStopped", e);
                }
                modelData.clearCallback();
            }
            if (!modelData.isModelLoaded()) {
                stopAndUnloadDeadModelsLocked();
                int[] handle = new int[]{Integer.MIN_VALUE};
                status = this.mModule.loadSoundModel(soundModel, handle);
                if (status != 0) {
                    Slog.w(TAG, "loadSoundModel call failed with " + status);
                    return status;
                } else if (handle[0] == Integer.MIN_VALUE) {
                    Slog.w(TAG, "loadSoundModel call returned invalid sound model handle");
                    return Integer.MIN_VALUE;
                } else {
                    modelData.setHandle(handle[0]);
                    modelData.setLoaded();
                    Slog.d(TAG, "Sound model loaded with handle:" + handle[0]);
                }
            }
            modelData.setCallback(callback);
            modelData.setRequested(true);
            modelData.setRecognitionConfig(recognitionConfig);
            modelData.setSoundModel(soundModel);
            int startRecognitionLocked = startRecognitionLocked(modelData, false);
            return startRecognitionLocked;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    int stopGenericRecognition(java.util.UUID r8, android.hardware.soundtrigger.IRecognitionStatusCallback r9) {
        /*
        r7 = this;
        r6 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r3 = r7.mLock;
        monitor-enter(r3);
        r2 = r7.mContext;	 Catch:{ all -> 0x007e }
        r4 = "sth_stop_recognition";
        r5 = 1;
        com.android.internal.logging.MetricsLogger.count(r2, r4, r5);	 Catch:{ all -> 0x007e }
        if (r9 == 0) goto L_0x0012;
    L_0x0010:
        if (r8 != 0) goto L_0x002e;
    L_0x0012:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x007e }
        r4.<init>();	 Catch:{ all -> 0x007e }
        r5 = "Null callbackreceived for stopGenericRecognition() for modelid:";
        r4 = r4.append(r5);	 Catch:{ all -> 0x007e }
        r4 = r4.append(r8);	 Catch:{ all -> 0x007e }
        r4 = r4.toString();	 Catch:{ all -> 0x007e }
        android.util.Slog.e(r2, r4);	 Catch:{ all -> 0x007e }
        monitor-exit(r3);
        return r6;
    L_0x002e:
        r2 = r7.mModelDataMap;	 Catch:{ all -> 0x007e }
        r0 = r2.get(r8);	 Catch:{ all -> 0x007e }
        r0 = (com.android.server.soundtrigger.SoundTriggerHelper.ModelData) r0;	 Catch:{ all -> 0x007e }
        if (r0 == 0) goto L_0x0040;
    L_0x0038:
        r2 = r0.isGenericModel();	 Catch:{ all -> 0x007e }
        r2 = r2 ^ 1;
        if (r2 == 0) goto L_0x005c;
    L_0x0040:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x007e }
        r4.<init>();	 Catch:{ all -> 0x007e }
        r5 = "Attempting stopRecognition on invalid model with id:";
        r4 = r4.append(r5);	 Catch:{ all -> 0x007e }
        r4 = r4.append(r8);	 Catch:{ all -> 0x007e }
        r4 = r4.toString();	 Catch:{ all -> 0x007e }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x007e }
        monitor-exit(r3);
        return r6;
    L_0x005c:
        r1 = r7.stopRecognition(r0, r9);	 Catch:{ all -> 0x007e }
        if (r1 == 0) goto L_0x007c;
    L_0x0062:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x007e }
        r4.<init>();	 Catch:{ all -> 0x007e }
        r5 = "stopGenericRecognition failed: ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x007e }
        r4 = r4.append(r1);	 Catch:{ all -> 0x007e }
        r4 = r4.toString();	 Catch:{ all -> 0x007e }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x007e }
    L_0x007c:
        monitor-exit(r3);
        return r1;
    L_0x007e:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.soundtrigger.SoundTriggerHelper.stopGenericRecognition(java.util.UUID, android.hardware.soundtrigger.IRecognitionStatusCallback):int");
    }

    int stopKeyphraseRecognition(int keyphraseId, IRecognitionStatusCallback callback) {
        synchronized (this.mLock) {
            MetricsLogger.count(this.mContext, "sth_stop_recognition", 1);
            if (callback == null) {
                Slog.e(TAG, "Null callback received for stopKeyphraseRecognition() for keyphraseId:" + keyphraseId);
                return Integer.MIN_VALUE;
            }
            ModelData modelData = getKeyphraseModelDataLocked(keyphraseId);
            if (modelData == null || (modelData.isKeyphraseModel() ^ 1) != 0) {
                Slog.e(TAG, "No model exists for given keyphrase Id " + keyphraseId);
                return Integer.MIN_VALUE;
            }
            int status = stopRecognition(modelData, callback);
            if (status != 0) {
                return status;
            }
            return status;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int stopRecognition(com.android.server.soundtrigger.SoundTriggerHelper.ModelData r7, android.hardware.soundtrigger.IRecognitionStatusCallback r8) {
        /*
        r6 = this;
        r5 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r3 = r6.mLock;
        monitor-enter(r3);
        if (r8 != 0) goto L_0x0009;
    L_0x0007:
        monitor-exit(r3);
        return r5;
    L_0x0009:
        r2 = r6.mModuleProperties;	 Catch:{ all -> 0x0078 }
        if (r2 == 0) goto L_0x0011;
    L_0x000d:
        r2 = r6.mModule;	 Catch:{ all -> 0x0078 }
        if (r2 != 0) goto L_0x001c;
    L_0x0011:
        r2 = "SoundTriggerHelper";
        r4 = "Attempting stopRecognition without the capability";
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x0078 }
        monitor-exit(r3);
        return r5;
    L_0x001c:
        r0 = r7.getCallback();	 Catch:{ all -> 0x0078 }
        if (r7 == 0) goto L_0x0024;
    L_0x0022:
        if (r0 != 0) goto L_0x002f;
    L_0x0024:
        r2 = "SoundTriggerHelper";
        r4 = "Attempting stopRecognition without a successful startRecognition";
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x0078 }
        monitor-exit(r3);
        return r5;
    L_0x002f:
        r2 = r7.isRequested();	 Catch:{ all -> 0x0078 }
        if (r2 != 0) goto L_0x003d;
    L_0x0035:
        r2 = r7.isModelStarted();	 Catch:{ all -> 0x0078 }
        r2 = r2 ^ 1;
        if (r2 != 0) goto L_0x0024;
    L_0x003d:
        r2 = r0.asBinder();	 Catch:{ all -> 0x0078 }
        r4 = r8.asBinder();	 Catch:{ all -> 0x0078 }
        if (r2 == r4) goto L_0x0052;
    L_0x0047:
        r2 = "SoundTriggerHelper";
        r4 = "Attempting stopRecognition for another recognition";
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x0078 }
        monitor-exit(r3);
        return r5;
    L_0x0052:
        r2 = 0;
        r7.setRequested(r2);	 Catch:{ all -> 0x0078 }
        r2 = r6.isRecognitionAllowed();	 Catch:{ all -> 0x0078 }
        r4 = 0;
        r1 = r6.updateRecognitionLocked(r7, r2, r4);	 Catch:{ all -> 0x0078 }
        if (r1 == 0) goto L_0x0063;
    L_0x0061:
        monitor-exit(r3);
        return r1;
    L_0x0063:
        r7.setLoaded();	 Catch:{ all -> 0x0078 }
        r7.clearCallback();	 Catch:{ all -> 0x0078 }
        r2 = 0;
        r7.setRecognitionConfig(r2);	 Catch:{ all -> 0x0078 }
        r2 = r6.computeRecognitionRunningLocked();	 Catch:{ all -> 0x0078 }
        if (r2 != 0) goto L_0x0076;
    L_0x0073:
        r6.internalClearGlobalStateLocked();	 Catch:{ all -> 0x0078 }
    L_0x0076:
        monitor-exit(r3);
        return r1;
    L_0x0078:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.soundtrigger.SoundTriggerHelper.stopRecognition(com.android.server.soundtrigger.SoundTriggerHelper$ModelData, android.hardware.soundtrigger.IRecognitionStatusCallback):int");
    }

    private int tryStopAndUnloadLocked(ModelData modelData, boolean stopModel, boolean unloadModel) {
        int status = 0;
        if (modelData.isModelNotLoaded()) {
            return 0;
        }
        if (stopModel && modelData.isModelStarted()) {
            status = stopRecognitionLocked(modelData, false);
            if (status != 0) {
                Slog.w(TAG, "stopRecognition failed: " + status);
                return status;
            }
        }
        if (unloadModel && modelData.isModelLoaded()) {
            Slog.d(TAG, "Unloading previously loaded stale model.");
            status = this.mModule.unloadSoundModel(modelData.getHandle());
            MetricsLogger.count(this.mContext, "sth_unloading_stale_model", 1);
            if (status != 0) {
                Slog.w(TAG, "unloadSoundModel call failed with " + status);
            } else {
                modelData.clearState();
            }
        }
        return status;
    }

    public ModuleProperties getModuleProperties() {
        return this.mModuleProperties;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    int unloadKeyphraseSoundModel(int r8) {
        /*
        r7 = this;
        r6 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r3 = r7.mLock;
        monitor-enter(r3);
        r2 = r7.mContext;	 Catch:{ all -> 0x007c }
        r4 = "sth_unload_keyphrase_sound_model";
        r5 = 1;
        com.android.internal.logging.MetricsLogger.count(r2, r4, r5);	 Catch:{ all -> 0x007c }
        r0 = r7.getKeyphraseModelDataLocked(r8);	 Catch:{ all -> 0x007c }
        r2 = r7.mModule;	 Catch:{ all -> 0x007c }
        if (r2 == 0) goto L_0x0018;
    L_0x0016:
        if (r0 != 0) goto L_0x001a;
    L_0x0018:
        monitor-exit(r3);
        return r6;
    L_0x001a:
        r2 = r0.getHandle();	 Catch:{ all -> 0x007c }
        if (r2 == r6) goto L_0x0018;
    L_0x0020:
        r2 = r0.isKeyphraseModel();	 Catch:{ all -> 0x007c }
        r2 = r2 ^ 1;
        if (r2 != 0) goto L_0x0018;
    L_0x0028:
        r2 = 0;
        r0.setRequested(r2);	 Catch:{ all -> 0x007c }
        r2 = r7.isRecognitionAllowed();	 Catch:{ all -> 0x007c }
        r4 = 0;
        r1 = r7.updateRecognitionLocked(r0, r2, r4);	 Catch:{ all -> 0x007c }
        if (r1 == 0) goto L_0x0051;
    L_0x0037:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x007c }
        r4.<init>();	 Catch:{ all -> 0x007c }
        r5 = "Stop recognition failed for keyphrase ID:";
        r4 = r4.append(r5);	 Catch:{ all -> 0x007c }
        r4 = r4.append(r1);	 Catch:{ all -> 0x007c }
        r4 = r4.toString();	 Catch:{ all -> 0x007c }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x007c }
    L_0x0051:
        r2 = r7.mModule;	 Catch:{ all -> 0x007c }
        r4 = r0.getHandle();	 Catch:{ all -> 0x007c }
        r1 = r2.unloadSoundModel(r4);	 Catch:{ all -> 0x007c }
        if (r1 == 0) goto L_0x0077;
    L_0x005d:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x007c }
        r4.<init>();	 Catch:{ all -> 0x007c }
        r5 = "unloadKeyphraseSoundModel call failed with ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x007c }
        r4 = r4.append(r1);	 Catch:{ all -> 0x007c }
        r4 = r4.toString();	 Catch:{ all -> 0x007c }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x007c }
    L_0x0077:
        r7.removeKeyphraseModelLocked(r8);	 Catch:{ all -> 0x007c }
        monitor-exit(r3);
        return r1;
    L_0x007c:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.soundtrigger.SoundTriggerHelper.unloadKeyphraseSoundModel(int):int");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    int unloadGenericSoundModel(java.util.UUID r9) {
        /*
        r8 = this;
        r7 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r6 = 0;
        r3 = r8.mLock;
        monitor-enter(r3);
        r2 = r8.mContext;	 Catch:{ all -> 0x00c4 }
        r4 = "sth_unload_generic_sound_model";
        r5 = 1;
        com.android.internal.logging.MetricsLogger.count(r2, r4, r5);	 Catch:{ all -> 0x00c4 }
        if (r9 == 0) goto L_0x0015;
    L_0x0011:
        r2 = r8.mModule;	 Catch:{ all -> 0x00c4 }
        if (r2 != 0) goto L_0x0017;
    L_0x0015:
        monitor-exit(r3);
        return r7;
    L_0x0017:
        r2 = r8.mModelDataMap;	 Catch:{ all -> 0x00c4 }
        r0 = r2.get(r9);	 Catch:{ all -> 0x00c4 }
        r0 = (com.android.server.soundtrigger.SoundTriggerHelper.ModelData) r0;	 Catch:{ all -> 0x00c4 }
        if (r0 == 0) goto L_0x0029;
    L_0x0021:
        r2 = r0.isGenericModel();	 Catch:{ all -> 0x00c4 }
        r2 = r2 ^ 1;
        if (r2 == 0) goto L_0x0045;
    L_0x0029:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00c4 }
        r4.<init>();	 Catch:{ all -> 0x00c4 }
        r5 = "Unload error: Attempting unload invalid generic model with id:";
        r4 = r4.append(r5);	 Catch:{ all -> 0x00c4 }
        r4 = r4.append(r9);	 Catch:{ all -> 0x00c4 }
        r4 = r4.toString();	 Catch:{ all -> 0x00c4 }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x00c4 }
        monitor-exit(r3);
        return r7;
    L_0x0045:
        r2 = r0.isModelLoaded();	 Catch:{ all -> 0x00c4 }
        if (r2 != 0) goto L_0x0067;
    L_0x004b:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00c4 }
        r4.<init>();	 Catch:{ all -> 0x00c4 }
        r5 = "Unload: Given generic model is not loaded:";
        r4 = r4.append(r5);	 Catch:{ all -> 0x00c4 }
        r4 = r4.append(r9);	 Catch:{ all -> 0x00c4 }
        r4 = r4.toString();	 Catch:{ all -> 0x00c4 }
        android.util.Slog.i(r2, r4);	 Catch:{ all -> 0x00c4 }
        monitor-exit(r3);
        return r6;
    L_0x0067:
        r2 = r0.isModelStarted();	 Catch:{ all -> 0x00c4 }
        if (r2 == 0) goto L_0x008e;
    L_0x006d:
        r2 = 0;
        r1 = r8.stopRecognitionLocked(r0, r2);	 Catch:{ all -> 0x00c4 }
        if (r1 == 0) goto L_0x008e;
    L_0x0074:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00c4 }
        r4.<init>();	 Catch:{ all -> 0x00c4 }
        r5 = "stopGenericRecognition failed: ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x00c4 }
        r4 = r4.append(r1);	 Catch:{ all -> 0x00c4 }
        r4 = r4.toString();	 Catch:{ all -> 0x00c4 }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x00c4 }
    L_0x008e:
        r2 = r8.mModule;	 Catch:{ all -> 0x00c4 }
        r4 = r0.getHandle();	 Catch:{ all -> 0x00c4 }
        r1 = r2.unloadSoundModel(r4);	 Catch:{ all -> 0x00c4 }
        if (r1 == 0) goto L_0x00bd;
    L_0x009a:
        r2 = "SoundTriggerHelper";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00c4 }
        r4.<init>();	 Catch:{ all -> 0x00c4 }
        r5 = "unloadGenericSoundModel() call failed with ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x00c4 }
        r4 = r4.append(r1);	 Catch:{ all -> 0x00c4 }
        r4 = r4.toString();	 Catch:{ all -> 0x00c4 }
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x00c4 }
        r2 = "SoundTriggerHelper";
        r4 = "unloadGenericSoundModel() force-marking model as unloaded.";
        android.util.Slog.w(r2, r4);	 Catch:{ all -> 0x00c4 }
    L_0x00bd:
        r2 = r8.mModelDataMap;	 Catch:{ all -> 0x00c4 }
        r2.remove(r9);	 Catch:{ all -> 0x00c4 }
        monitor-exit(r3);
        return r1;
    L_0x00c4:
        r2 = move-exception;
        monitor-exit(r3);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.soundtrigger.SoundTriggerHelper.unloadGenericSoundModel(java.util.UUID):int");
    }

    boolean isRecognitionRequested(UUID modelId) {
        boolean isRequested;
        synchronized (this.mLock) {
            ModelData modelData = (ModelData) this.mModelDataMap.get(modelId);
            isRequested = modelData != null ? modelData.isRequested() : false;
        }
        return isRequested;
    }

    public void onRecognition(RecognitionEvent event) {
        if (event == null) {
            Slog.w(TAG, "Null recognition event!");
        } else if ((event instanceof KeyphraseRecognitionEvent) || ((event instanceof GenericRecognitionEvent) ^ 1) == 0) {
            synchronized (this.mLock) {
                switch (event.status) {
                    case 0:
                        if (!isKeyphraseRecognitionEvent(event)) {
                            onGenericRecognitionSuccessLocked((GenericRecognitionEvent) event);
                            break;
                        } else {
                            onKeyphraseRecognitionSuccessLocked((KeyphraseRecognitionEvent) event);
                            break;
                        }
                    case 1:
                        onRecognitionAbortLocked(event);
                        break;
                    case 2:
                        onRecognitionFailureLocked();
                        break;
                }
            }
        } else {
            Slog.w(TAG, "Invalid recognition event type (not one of generic or keyphrase)!");
        }
    }

    private boolean isKeyphraseRecognitionEvent(RecognitionEvent event) {
        return event instanceof KeyphraseRecognitionEvent;
    }

    private void onGenericRecognitionSuccessLocked(GenericRecognitionEvent event) {
        MetricsLogger.count(this.mContext, "sth_generic_recognition_event", 1);
        if (event.status == 0) {
            ModelData model = getModelDataForLocked(event.soundModelHandle);
            if (model == null || (model.isGenericModel() ^ 1) != 0) {
                Slog.w(TAG, "Generic recognition event: Model does not exist for handle: " + event.soundModelHandle);
                return;
            }
            IRecognitionStatusCallback callback = model.getCallback();
            if (callback == null) {
                Slog.w(TAG, "Generic recognition event: Null callback for model handle: " + event.soundModelHandle);
                return;
            }
            model.setStopped();
            try {
                callback.onGenericSoundTriggerDetected(event);
            } catch (DeadObjectException e) {
                forceStopAndUnloadModelLocked(model, e);
                return;
            } catch (RemoteException e2) {
                Slog.w(TAG, "RemoteException in onGenericSoundTriggerDetected", e2);
            }
            RecognitionConfig config = model.getRecognitionConfig();
            if (config == null) {
                Slog.w(TAG, "Generic recognition event: Null RecognitionConfig for model handle: " + event.soundModelHandle);
                return;
            }
            model.setRequested(config.allowMultipleTriggers);
            if (model.isRequested()) {
                updateRecognitionLocked(model, isRecognitionAllowed(), true);
            }
        }
    }

    public void onSoundModelUpdate(SoundModelEvent event) {
        if (event == null) {
            Slog.w(TAG, "Invalid sound model event!");
            return;
        }
        synchronized (this.mLock) {
            MetricsLogger.count(this.mContext, "sth_sound_model_updated", 1);
            onSoundModelUpdatedLocked(event);
        }
    }

    public void onServiceStateChange(int state) {
        boolean z = true;
        synchronized (this.mLock) {
            if (1 != state) {
                z = false;
            }
            onServiceStateChangedLocked(z);
        }
    }

    public void onServiceDied() {
        Slog.e(TAG, "onServiceDied!!");
        MetricsLogger.count(this.mContext, "sth_service_died", 1);
        synchronized (this.mLock) {
            onServiceDiedLocked();
        }
    }

    private void onCallStateChangedLocked(boolean callActive) {
        if (this.mCallActive != callActive) {
            this.mCallActive = callActive;
            updateAllRecognitionsLocked(true);
        }
    }

    private void onPowerSaveModeChangedLocked(boolean isPowerSaveMode) {
        if (this.mIsPowerSaveMode != isPowerSaveMode) {
            this.mIsPowerSaveMode = isPowerSaveMode;
            updateAllRecognitionsLocked(true);
        }
    }

    private void onSoundModelUpdatedLocked(SoundModelEvent event) {
    }

    private void onServiceStateChangedLocked(boolean disabled) {
        if (disabled != this.mServiceDisabled) {
            this.mServiceDisabled = disabled;
            updateAllRecognitionsLocked(true);
        }
    }

    private void onRecognitionAbortLocked(RecognitionEvent event) {
        Slog.w(TAG, "Recognition aborted");
        MetricsLogger.count(this.mContext, "sth_recognition_aborted", 1);
        ModelData modelData = getModelDataForLocked(event.soundModelHandle);
        if (modelData != null && modelData.isModelStarted()) {
            modelData.setStopped();
            try {
                modelData.getCallback().onRecognitionPaused();
            } catch (DeadObjectException e) {
                forceStopAndUnloadModelLocked(modelData, e);
            } catch (RemoteException e2) {
                Slog.w(TAG, "RemoteException in onRecognitionPaused", e2);
            }
        }
    }

    private void onRecognitionFailureLocked() {
        Slog.w(TAG, "Recognition failure");
        MetricsLogger.count(this.mContext, "sth_recognition_failure_event", 1);
        try {
            sendErrorCallbacksToAllLocked(Integer.MIN_VALUE);
        } finally {
            internalClearModelStateLocked();
            internalClearGlobalStateLocked();
        }
    }

    private int getKeyphraseIdFromEvent(KeyphraseRecognitionEvent event) {
        if (event == null) {
            Slog.w(TAG, "Null RecognitionEvent received.");
            return Integer.MIN_VALUE;
        }
        KeyphraseRecognitionExtra[] keyphraseExtras = event.keyphraseExtras;
        if (keyphraseExtras != null && keyphraseExtras.length != 0) {
            return keyphraseExtras[0].id;
        }
        Slog.w(TAG, "Invalid keyphrase recognition event!");
        return Integer.MIN_VALUE;
    }

    private void onKeyphraseRecognitionSuccessLocked(KeyphraseRecognitionEvent event) {
        Slog.i(TAG, "Recognition success");
        MetricsLogger.count(this.mContext, "sth_keyphrase_recognition_event", 1);
        int keyphraseId = getKeyphraseIdFromEvent(event);
        ModelData modelData = getKeyphraseModelDataLocked(keyphraseId);
        if (modelData == null || (modelData.isKeyphraseModel() ^ 1) != 0) {
            Slog.e(TAG, "Keyphase model data does not exist for ID:" + keyphraseId);
        } else if (modelData.getCallback() == null) {
            Slog.w(TAG, "Received onRecognition event without callback for keyphrase model.");
        } else {
            modelData.setStopped();
            try {
                modelData.getCallback().onKeyphraseDetected(event);
            } catch (DeadObjectException e) {
                forceStopAndUnloadModelLocked(modelData, e);
                return;
            } catch (RemoteException e2) {
                Slog.w(TAG, "RemoteException in onKeyphraseDetected", e2);
            }
            RecognitionConfig config = modelData.getRecognitionConfig();
            if (config != null) {
                modelData.setRequested(config.allowMultipleTriggers);
            }
            if (modelData.isRequested()) {
                updateRecognitionLocked(modelData, isRecognitionAllowed(), true);
            }
        }
    }

    private void updateAllRecognitionsLocked(boolean notify) {
        boolean isAllowed = isRecognitionAllowed();
        for (ModelData modelData : new ArrayList(this.mModelDataMap.values())) {
            updateRecognitionLocked(modelData, isAllowed, notify);
        }
    }

    private int updateRecognitionLocked(ModelData model, boolean isAllowed, boolean notify) {
        boolean start = model.isRequested() ? isAllowed : false;
        if (start == model.isModelStarted()) {
            return 0;
        }
        if (start) {
            return startRecognitionLocked(model, notify);
        }
        return stopRecognitionLocked(model, notify);
    }

    private void onServiceDiedLocked() {
        try {
            MetricsLogger.count(this.mContext, "sth_service_died", 1);
            sendErrorCallbacksToAllLocked(SoundTrigger.STATUS_DEAD_OBJECT);
        } finally {
            internalClearModelStateLocked();
            internalClearGlobalStateLocked();
            if (this.mModule != null) {
                this.mModule.detach();
                this.mModule = null;
            }
        }
    }

    private void internalClearGlobalStateLocked() {
        this.mTelephonyManager.listen(this.mPhoneStateListener, 0);
        if (this.mPowerSaveModeListener != null) {
            this.mContext.unregisterReceiver(this.mPowerSaveModeListener);
            this.mPowerSaveModeListener = null;
        }
    }

    private void internalClearModelStateLocked() {
        for (ModelData modelData : this.mModelDataMap.values()) {
            modelData.clearState();
        }
    }

    void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
        synchronized (this.mLock) {
            pw.print("  module properties=");
            pw.println(this.mModuleProperties == null ? "null" : this.mModuleProperties);
            pw.print("  call active=");
            pw.println(this.mCallActive);
            pw.print("  power save mode active=");
            pw.println(this.mIsPowerSaveMode);
            pw.print("  service disabled=");
            pw.println(this.mServiceDisabled);
        }
    }

    private void initializeTelephonyAndPowerStateListeners() {
        boolean z = false;
        if (this.mTelephonyManager.getCallState() != 0) {
            z = true;
        }
        this.mCallActive = z;
        this.mTelephonyManager.listen(this.mPhoneStateListener, 32);
        if (this.mPowerSaveModeListener == null) {
            this.mPowerSaveModeListener = new PowerSaveModeListener();
            this.mContext.registerReceiver(this.mPowerSaveModeListener, new IntentFilter("android.os.action.POWER_SAVE_MODE_CHANGED"));
        }
        this.mIsPowerSaveMode = this.mPowerManager.getPowerSaveState(8).batterySaverEnabled;
    }

    private void sendErrorCallbacksToAllLocked(int errorCode) {
        for (ModelData modelData : this.mModelDataMap.values()) {
            IRecognitionStatusCallback callback = modelData.getCallback();
            if (callback != null) {
                try {
                    callback.onError(errorCode);
                } catch (RemoteException e) {
                    Slog.w(TAG, "RemoteException sendErrorCallbacksToAllLocked for model handle " + modelData.getHandle(), e);
                }
            }
        }
    }

    private void forceStopAndUnloadModelLocked(ModelData modelData, Exception exception) {
        forceStopAndUnloadModelLocked(modelData, exception, null);
    }

    private void forceStopAndUnloadModelLocked(ModelData modelData, Exception exception, Iterator modelDataIterator) {
        if (exception != null) {
            Slog.e(TAG, "forceStopAndUnloadModel", exception);
        }
        if (modelData.isModelStarted()) {
            Slog.d(TAG, "Stopping previously started dangling model " + modelData.getHandle());
            if (this.mModule.stopRecognition(modelData.getHandle()) != 0) {
                modelData.setStopped();
                modelData.setRequested(false);
            } else {
                Slog.e(TAG, "Failed to stop model " + modelData.getHandle());
            }
        }
        if (modelData.isModelLoaded()) {
            Slog.d(TAG, "Unloading previously loaded dangling model " + modelData.getHandle());
            if (this.mModule.unloadSoundModel(modelData.getHandle()) == 0) {
                if (modelDataIterator != null) {
                    modelDataIterator.remove();
                } else {
                    this.mModelDataMap.remove(modelData.getModelId());
                }
                Iterator it = this.mKeyphraseUuidMap.entrySet().iterator();
                while (it.hasNext()) {
                    if (((Entry) it.next()).getValue().equals(modelData.getModelId())) {
                        it.remove();
                    }
                }
                modelData.clearState();
                return;
            }
            Slog.e(TAG, "Failed to unload model " + modelData.getHandle());
        }
    }

    private void stopAndUnloadDeadModelsLocked() {
        Iterator it = this.mModelDataMap.entrySet().iterator();
        while (it.hasNext()) {
            ModelData modelData = (ModelData) ((Entry) it.next()).getValue();
            if (modelData.isModelLoaded() && (modelData.getCallback() == null || !(modelData.getCallback().asBinder() == null || (modelData.getCallback().asBinder().pingBinder() ^ 1) == 0))) {
                Slog.w(TAG, "Removing model " + modelData.getHandle() + " that has no clients");
                forceStopAndUnloadModelLocked(modelData, null, it);
            }
        }
    }

    private ModelData getOrCreateGenericModelDataLocked(UUID modelId) {
        ModelData modelData = (ModelData) this.mModelDataMap.get(modelId);
        if (modelData == null) {
            modelData = ModelData.createGenericModelData(modelId);
            this.mModelDataMap.put(modelId, modelData);
        } else if (!modelData.isGenericModel()) {
            Slog.e(TAG, "UUID already used for non-generic model.");
            return null;
        }
        return modelData;
    }

    private void removeKeyphraseModelLocked(int keyphraseId) {
        UUID uuid = (UUID) this.mKeyphraseUuidMap.get(Integer.valueOf(keyphraseId));
        if (uuid != null) {
            this.mModelDataMap.remove(uuid);
            this.mKeyphraseUuidMap.remove(Integer.valueOf(keyphraseId));
        }
    }

    private ModelData getKeyphraseModelDataLocked(int keyphraseId) {
        UUID uuid = (UUID) this.mKeyphraseUuidMap.get(Integer.valueOf(keyphraseId));
        if (uuid == null) {
            return null;
        }
        return (ModelData) this.mModelDataMap.get(uuid);
    }

    private ModelData createKeyphraseModelDataLocked(UUID modelId, int keyphraseId) {
        this.mKeyphraseUuidMap.remove(Integer.valueOf(keyphraseId));
        this.mModelDataMap.remove(modelId);
        this.mKeyphraseUuidMap.put(Integer.valueOf(keyphraseId), modelId);
        ModelData modelData = ModelData.createKeyphraseModelData(modelId);
        this.mModelDataMap.put(modelId, modelData);
        return modelData;
    }

    private ModelData getModelDataForLocked(int modelHandle) {
        for (ModelData model : this.mModelDataMap.values()) {
            if (model.getHandle() == modelHandle) {
                return model;
            }
        }
        return null;
    }

    private boolean isRecognitionAllowed() {
        return (this.mCallActive || (this.mServiceDisabled ^ 1) == 0) ? false : this.mIsPowerSaveMode ^ 1;
    }

    private int startRecognitionLocked(ModelData modelData, boolean notify) {
        IRecognitionStatusCallback callback = modelData.getCallback();
        int handle = modelData.getHandle();
        RecognitionConfig config = modelData.getRecognitionConfig();
        if (callback == null || handle == Integer.MIN_VALUE || config == null) {
            Slog.w(TAG, "startRecognition: Bad data passed in.");
            MetricsLogger.count(this.mContext, "sth_start_recognition_error", 1);
            return Integer.MIN_VALUE;
        } else if (isRecognitionAllowed()) {
            int status = this.mModule.startRecognition(handle, config);
            if (status != 0) {
                Slog.w(TAG, "startRecognition failed with " + status);
                MetricsLogger.count(this.mContext, "sth_start_recognition_error", 1);
                if (notify) {
                    try {
                        callback.onError(status);
                    } catch (DeadObjectException e) {
                        forceStopAndUnloadModelLocked(modelData, e);
                    } catch (RemoteException e2) {
                        Slog.w(TAG, "RemoteException in onError", e2);
                    }
                }
            } else {
                Slog.i(TAG, "startRecognition successful.");
                MetricsLogger.count(this.mContext, "sth_start_recognition_success", 1);
                modelData.setStarted();
                if (notify) {
                    try {
                        callback.onRecognitionResumed();
                    } catch (DeadObjectException e3) {
                        forceStopAndUnloadModelLocked(modelData, e3);
                    } catch (RemoteException e22) {
                        Slog.w(TAG, "RemoteException in onRecognitionResumed", e22);
                    }
                }
            }
            return status;
        } else {
            Slog.w(TAG, "startRecognition requested but not allowed.");
            MetricsLogger.count(this.mContext, "sth_start_recognition_not_allowed", 1);
            return 0;
        }
    }

    private int stopRecognitionLocked(ModelData modelData, boolean notify) {
        IRecognitionStatusCallback callback = modelData.getCallback();
        int status = this.mModule.stopRecognition(modelData.getHandle());
        if (status != 0) {
            Slog.w(TAG, "stopRecognition call failed with " + status);
            MetricsLogger.count(this.mContext, "sth_stop_recognition_error", 1);
            if (notify) {
                try {
                    callback.onError(status);
                } catch (DeadObjectException e) {
                    forceStopAndUnloadModelLocked(modelData, e);
                } catch (RemoteException e2) {
                    Slog.w(TAG, "RemoteException in onError", e2);
                }
            }
        } else {
            modelData.setStopped();
            MetricsLogger.count(this.mContext, "sth_stop_recognition_success", 1);
            if (notify) {
                try {
                    callback.onRecognitionPaused();
                } catch (DeadObjectException e3) {
                    forceStopAndUnloadModelLocked(modelData, e3);
                } catch (RemoteException e22) {
                    Slog.w(TAG, "RemoteException in onRecognitionPaused", e22);
                }
            }
        }
        return status;
    }

    private void dumpModelStateLocked() {
        for (UUID modelId : this.mModelDataMap.keySet()) {
            Slog.i(TAG, "Model :" + ((ModelData) this.mModelDataMap.get(modelId)).toString());
        }
    }

    private boolean computeRecognitionRunningLocked() {
        if (this.mModuleProperties == null || this.mModule == null) {
            this.mRecognitionRunning = false;
            return this.mRecognitionRunning;
        }
        for (ModelData modelData : this.mModelDataMap.values()) {
            if (modelData.isModelStarted()) {
                this.mRecognitionRunning = true;
                return this.mRecognitionRunning;
            }
        }
        this.mRecognitionRunning = false;
        return this.mRecognitionRunning;
    }
}
