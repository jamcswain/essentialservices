package com.android.server.media;

import android.app.ActivityManager;
import android.app.INotificationManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.PendingIntent.OnFinished;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.UserInfo;
import android.database.ContentObserver;
import android.media.AudioManagerInternal;
import android.media.AudioSystem;
import android.media.IAudioService;
import android.media.IRemoteVolumeController;
import android.media.session.IActiveSessionsListener;
import android.media.session.ICallback;
import android.media.session.IOnMediaKeyListener;
import android.media.session.IOnVolumeKeyLongPressListener;
import android.media.session.ISession;
import android.media.session.ISessionCallback;
import android.media.session.ISessionController;
import android.media.session.ISessionManager.Stub;
import android.media.session.MediaSession.Token;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.ViewConfiguration;
import com.android.internal.util.DumpUtils;
import com.android.server.LocalServices;
import com.android.server.SystemService;
import com.android.server.Watchdog;
import com.android.server.Watchdog.Monitor;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class MediaSessionService extends SystemService implements Monitor {
    static final boolean DEBUG = Log.isLoggable(TAG, 3);
    private static final boolean DEBUG_KEY_EVENT = true;
    private static final int MEDIA_KEY_LISTENER_TIMEOUT = 1000;
    private static final String TAG = "MediaSessionService";
    private static final int WAKELOCK_TIMEOUT = 5000;
    private AudioManagerInternal mAudioManagerInternal;
    private AudioPlaybackMonitor mAudioPlaybackMonitor;
    private IAudioService mAudioService;
    private ContentResolver mContentResolver;
    private FullUserRecord mCurrentFullUserRecord;
    private final SparseIntArray mFullUserIds = new SparseIntArray();
    private MediaSessionRecord mGlobalPrioritySession;
    private final MessageHandler mHandler = new MessageHandler();
    private boolean mHasFeatureLeanback;
    private KeyguardManager mKeyguardManager;
    private final Object mLock = new Object();
    private final int mLongPressTimeout;
    private final WakeLock mMediaEventWakeLock;
    private INotificationManager mNotificationManager;
    private IRemoteVolumeController mRvc;
    private final SessionManagerImpl mSessionManagerImpl = new SessionManagerImpl();
    private final ArrayList<SessionsListenerRecord> mSessionsListeners = new ArrayList();
    private SettingsObserver mSettingsObserver;
    private final SparseArray<FullUserRecord> mUserRecords = new SparseArray();

    final class FullUserRecord implements OnMediaButtonSessionChangedListener {
        private static final String COMPONENT_NAME_USER_ID_DELIM = ",";
        private ICallback mCallback;
        private final int mFullUserId;
        private boolean mInitialDownMusicOnly;
        private KeyEvent mInitialDownVolumeKeyEvent;
        private int mInitialDownVolumeStream;
        private PendingIntent mLastMediaButtonReceiver;
        private IOnMediaKeyListener mOnMediaKeyListener;
        private int mOnMediaKeyListenerUid;
        private IOnVolumeKeyLongPressListener mOnVolumeKeyLongPressListener;
        private int mOnVolumeKeyLongPressListenerUid;
        private final MediaSessionStack mPriorityStack;
        private ComponentName mRestoredMediaButtonReceiver;
        private int mRestoredMediaButtonReceiverUserId;

        public FullUserRecord(int fullUserId) {
            this.mFullUserId = fullUserId;
            this.mPriorityStack = new MediaSessionStack(MediaSessionService.this.mAudioPlaybackMonitor, this);
            String mediaButtonReceiver = Secure.getStringForUser(MediaSessionService.this.mContentResolver, "media_button_receiver", this.mFullUserId);
            if (mediaButtonReceiver != null) {
                String[] tokens = mediaButtonReceiver.split(COMPONENT_NAME_USER_ID_DELIM);
                if (tokens != null && tokens.length == 2) {
                    this.mRestoredMediaButtonReceiver = ComponentName.unflattenFromString(tokens[0]);
                    this.mRestoredMediaButtonReceiverUserId = Integer.parseInt(tokens[1]);
                }
            }
        }

        public void destroySessionsForUserLocked(int userId) {
            for (MediaSessionRecord session : this.mPriorityStack.getPriorityList(false, userId)) {
                MediaSessionService.this.destroySessionLocked(session);
            }
        }

        public void dumpLocked(PrintWriter pw, String prefix) {
            pw.print(prefix + "Record for full_user=" + this.mFullUserId);
            int size = MediaSessionService.this.mFullUserIds.size();
            int i = 0;
            while (i < size) {
                if (MediaSessionService.this.mFullUserIds.keyAt(i) != MediaSessionService.this.mFullUserIds.valueAt(i) && MediaSessionService.this.mFullUserIds.valueAt(i) == this.mFullUserId) {
                    pw.print(", profile_user=" + MediaSessionService.this.mFullUserIds.keyAt(i));
                }
                i++;
            }
            pw.println();
            String indent = prefix + "  ";
            pw.println(indent + "Volume key long-press listener: " + this.mOnVolumeKeyLongPressListener);
            pw.println(indent + "Volume key long-press listener package: " + MediaSessionService.this.getCallingPackageName(this.mOnVolumeKeyLongPressListenerUid));
            pw.println(indent + "Media key listener: " + this.mOnMediaKeyListener);
            pw.println(indent + "Media key listener package: " + MediaSessionService.this.getCallingPackageName(this.mOnMediaKeyListenerUid));
            pw.println(indent + "Callback: " + this.mCallback);
            pw.println(indent + "Last MediaButtonReceiver: " + this.mLastMediaButtonReceiver);
            pw.println(indent + "Restored MediaButtonReceiver: " + this.mRestoredMediaButtonReceiver);
            this.mPriorityStack.dump(pw, indent);
        }

        public void onMediaButtonSessionChanged(MediaSessionRecord oldMediaButtonSession, MediaSessionRecord newMediaButtonSession) {
            Log.d(MediaSessionService.TAG, "Media button session is changed to " + newMediaButtonSession);
            synchronized (MediaSessionService.this.mLock) {
                if (oldMediaButtonSession != null) {
                    MediaSessionService.this.mHandler.postSessionsChanged(oldMediaButtonSession.getUserId());
                }
                if (newMediaButtonSession != null) {
                    rememberMediaButtonReceiverLocked(newMediaButtonSession);
                    MediaSessionService.this.mHandler.postSessionsChanged(newMediaButtonSession.getUserId());
                }
                pushAddressedPlayerChangedLocked();
            }
        }

        public void rememberMediaButtonReceiverLocked(MediaSessionRecord record) {
            PendingIntent receiver = record.getMediaButtonReceiver();
            this.mLastMediaButtonReceiver = receiver;
            this.mRestoredMediaButtonReceiver = null;
            String componentName = "";
            if (receiver != null) {
                ComponentName component = receiver.getIntent().getComponent();
                if (component != null && record.getPackageName().equals(component.getPackageName())) {
                    componentName = component.flattenToString();
                }
            }
            Secure.putStringForUser(MediaSessionService.this.mContentResolver, "media_button_receiver", componentName + COMPONENT_NAME_USER_ID_DELIM + record.getUserId(), this.mFullUserId);
        }

        private void pushAddressedPlayerChangedLocked() {
            if (this.mCallback != null) {
                try {
                    MediaSessionRecord mediaButtonSession = getMediaButtonSessionLocked();
                    if (mediaButtonSession != null) {
                        this.mCallback.onAddressedPlayerChangedToMediaSession(new Token(mediaButtonSession.getControllerBinder()));
                    } else if (MediaSessionService.this.mCurrentFullUserRecord.mLastMediaButtonReceiver != null) {
                        this.mCallback.onAddressedPlayerChangedToMediaButtonReceiver(MediaSessionService.this.mCurrentFullUserRecord.mLastMediaButtonReceiver.getIntent().getComponent());
                    } else if (MediaSessionService.this.mCurrentFullUserRecord.mRestoredMediaButtonReceiver != null) {
                        this.mCallback.onAddressedPlayerChangedToMediaButtonReceiver(MediaSessionService.this.mCurrentFullUserRecord.mRestoredMediaButtonReceiver);
                    }
                } catch (RemoteException e) {
                    Log.w(MediaSessionService.TAG, "Failed to pushAddressedPlayerChangedLocked", e);
                }
            }
        }

        private MediaSessionRecord getMediaButtonSessionLocked() {
            return MediaSessionService.this.isGlobalPriorityActiveLocked() ? MediaSessionService.this.mGlobalPrioritySession : this.mPriorityStack.getMediaButtonSession();
        }
    }

    final class MessageHandler extends Handler {
        private static final int MSG_SESSIONS_CHANGED = 1;
        private static final int MSG_VOLUME_INITIAL_DOWN = 2;
        private final SparseArray<Integer> mIntegerCache = new SparseArray();

        MessageHandler() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    MediaSessionService.this.pushSessionsChanged(((Integer) msg.obj).intValue());
                    return;
                case 2:
                    synchronized (MediaSessionService.this.mLock) {
                        FullUserRecord user = (FullUserRecord) MediaSessionService.this.mUserRecords.get(msg.arg1);
                        if (!(user == null || user.mInitialDownVolumeKeyEvent == null)) {
                            MediaSessionService.this.dispatchVolumeKeyLongPressLocked(user.mInitialDownVolumeKeyEvent);
                            user.mInitialDownVolumeKeyEvent = null;
                        }
                    }
                    return;
                default:
                    return;
            }
        }

        public void postSessionsChanged(int userId) {
            Integer userIdInteger = (Integer) this.mIntegerCache.get(userId);
            if (userIdInteger == null) {
                userIdInteger = Integer.valueOf(userId);
                this.mIntegerCache.put(userId, userIdInteger);
            }
            removeMessages(1, userIdInteger);
            obtainMessage(1, userIdInteger).sendToTarget();
        }
    }

    class SessionManagerImpl extends Stub {
        private static final String EXTRA_WAKELOCK_ACQUIRED = "android.media.AudioService.WAKELOCK_ACQUIRED";
        private static final int WAKELOCK_RELEASE_ON_FINISHED = 1980;
        BroadcastReceiver mKeyEventDone = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        synchronized (MediaSessionService.this.mLock) {
                            if (extras.containsKey(SessionManagerImpl.EXTRA_WAKELOCK_ACQUIRED) && MediaSessionService.this.mMediaEventWakeLock.isHeld()) {
                                MediaSessionService.this.mMediaEventWakeLock.release();
                            }
                        }
                    }
                }
            }
        };
        private KeyEventWakeLockReceiver mKeyEventReceiver = new KeyEventWakeLockReceiver(MediaSessionService.this.mHandler);
        private boolean mVoiceButtonDown = false;
        private boolean mVoiceButtonHandled = false;

        class KeyEventWakeLockReceiver extends ResultReceiver implements Runnable, OnFinished {
            private final Handler mHandler;
            private int mLastTimeoutId = 0;
            private int mRefCount = 0;

            public KeyEventWakeLockReceiver(Handler handler) {
                super(handler);
                this.mHandler = handler;
            }

            public void onTimeout() {
                synchronized (MediaSessionService.this.mLock) {
                    if (this.mRefCount == 0) {
                        return;
                    }
                    this.mLastTimeoutId++;
                    this.mRefCount = 0;
                    releaseWakeLockLocked();
                }
            }

            public void aquireWakeLockLocked() {
                if (this.mRefCount == 0) {
                    MediaSessionService.this.mMediaEventWakeLock.acquire();
                }
                this.mRefCount++;
                this.mHandler.removeCallbacks(this);
                this.mHandler.postDelayed(this, 5000);
            }

            public void run() {
                onTimeout();
            }

            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode >= this.mLastTimeoutId) {
                    synchronized (MediaSessionService.this.mLock) {
                        if (this.mRefCount > 0) {
                            this.mRefCount--;
                            if (this.mRefCount == 0) {
                                releaseWakeLockLocked();
                            }
                        }
                    }
                }
            }

            private void releaseWakeLockLocked() {
                MediaSessionService.this.mMediaEventWakeLock.release();
                this.mHandler.removeCallbacks(this);
            }

            public void onSendFinished(PendingIntent pendingIntent, Intent intent, int resultCode, String resultData, Bundle resultExtras) {
                onReceiveResult(resultCode, null);
            }
        }

        private class MediaKeyListenerResultReceiver extends ResultReceiver implements Runnable {
            private boolean mHandled;
            private KeyEvent mKeyEvent;
            private boolean mNeedWakeLock;

            private MediaKeyListenerResultReceiver(KeyEvent keyEvent, boolean needWakeLock) {
                super(MediaSessionService.this.mHandler);
                MediaSessionService.this.mHandler.postDelayed(this, 1000);
                this.mKeyEvent = keyEvent;
                this.mNeedWakeLock = needWakeLock;
            }

            public void run() {
                Log.d(MediaSessionService.TAG, "The media key listener is timed-out for " + this.mKeyEvent);
                dispatchMediaKeyEvent();
            }

            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == 1) {
                    this.mHandled = true;
                    MediaSessionService.this.mHandler.removeCallbacks(this);
                    return;
                }
                dispatchMediaKeyEvent();
            }

            private void dispatchMediaKeyEvent() {
                if (!this.mHandled) {
                    this.mHandled = true;
                    MediaSessionService.this.mHandler.removeCallbacks(this);
                    synchronized (MediaSessionService.this.mLock) {
                        if (MediaSessionService.this.isGlobalPriorityActiveLocked() || !SessionManagerImpl.this.isVoiceKey(this.mKeyEvent.getKeyCode())) {
                            SessionManagerImpl.this.dispatchMediaKeyEventLocked(this.mKeyEvent, this.mNeedWakeLock);
                        } else {
                            SessionManagerImpl.this.handleVoiceKeyEventLocked(this.mKeyEvent, this.mNeedWakeLock);
                        }
                    }
                }
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void startVoiceInput(boolean r8) {
            /* JADX: method processing error */
/*
Error: jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x0081 in list [B:28:0x00c7]
	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:43)
	at jadx.core.dex.instructions.IfNode.initBlocks(IfNode.java:60)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.initBlocksInIfNodes(BlockFinish.java:48)
	at jadx.core.dex.visitors.blocksmaker.BlockFinish.visit(BlockFinish.java:33)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:31)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:17)
	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
	at jadx.core.ProcessClass.process(ProcessClass.java:34)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
*/
            /*
            r7 = this;
            r3 = 0;
            r4 = com.android.server.media.MediaSessionService.this;
            r4 = r4.getContext();
            r5 = "power";
            r2 = r4.getSystemService(r5);
            r2 = (android.os.PowerManager) r2;
            r4 = com.android.server.media.MediaSessionService.this;
            r4 = r4.mKeyguardManager;
            if (r4 == 0) goto L_0x0082;
        L_0x0018:
            r4 = com.android.server.media.MediaSessionService.this;
            r4 = r4.mKeyguardManager;
            r1 = r4.isKeyguardLocked();
        L_0x0022:
            if (r1 != 0) goto L_0x0084;
        L_0x0024:
            r4 = r2.isScreenOn();
            if (r4 == 0) goto L_0x0084;
        L_0x002a:
            r3 = new android.content.Intent;
            r4 = "android.speech.action.WEB_SEARCH";
            r3.<init>(r4);
            r4 = "MediaSessionService";
            r5 = "voice-based interactions: about to use ACTION_WEB_SEARCH";
            android.util.Log.i(r4, r5);
        L_0x003b:
            if (r8 == 0) goto L_0x0046;
        L_0x003d:
            r4 = com.android.server.media.MediaSessionService.this;
            r4 = r4.mMediaEventWakeLock;
            r4.acquire();
        L_0x0046:
            if (r3 == 0) goto L_0x0076;
        L_0x0048:
            r4 = 276824064; // 0x10800000 float:5.0487098E-29 double:1.3676926E-315;
            r3.setFlags(r4);	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r4 = com.android.server.media.MediaSessionService.DEBUG;	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            if (r4 == 0) goto L_0x006b;	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
        L_0x0051:
            r4 = "MediaSessionService";	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = new java.lang.StringBuilder;	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5.<init>();	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r6 = "voiceIntent: ";	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = r5.append(r6);	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = r5.append(r3);	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = r5.toString();	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            android.util.Log.d(r4, r5);	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
        L_0x006b:
            r4 = com.android.server.media.MediaSessionService.this;	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r4 = r4.getContext();	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = android.os.UserHandle.CURRENT;	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r4.startActivityAsUser(r3, r5);	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
        L_0x0076:
            if (r8 == 0) goto L_0x0081;
        L_0x0078:
            r4 = com.android.server.media.MediaSessionService.this;
            r4 = r4.mMediaEventWakeLock;
            r4.release();
        L_0x0081:
            return;
        L_0x0082:
            r1 = 0;
            goto L_0x0022;
        L_0x0084:
            r3 = new android.content.Intent;
            r4 = "android.speech.action.VOICE_SEARCH_HANDS_FREE";
            r3.<init>(r4);
            r5 = "android.speech.extras.EXTRA_SECURE";
            if (r1 == 0) goto L_0x00a8;
        L_0x0091:
            r4 = com.android.server.media.MediaSessionService.this;
            r4 = r4.mKeyguardManager;
            r4 = r4.isKeyguardSecure();
        L_0x009b:
            r3.putExtra(r5, r4);
            r4 = "MediaSessionService";
            r5 = "voice-based interactions: about to use ACTION_VOICE_SEARCH_HANDS_FREE";
            android.util.Log.i(r4, r5);
            goto L_0x003b;
        L_0x00a8:
            r4 = 0;
            goto L_0x009b;
        L_0x00aa:
            r0 = move-exception;
            r4 = "MediaSessionService";	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = new java.lang.StringBuilder;	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5.<init>();	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r6 = "No activity for search: ";	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = r5.append(r6);	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = r5.append(r0);	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            r5 = r5.toString();	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            android.util.Log.w(r4, r5);	 Catch:{ ActivityNotFoundException -> 0x00aa, all -> 0x00d1 }
            if (r8 == 0) goto L_0x0081;
        L_0x00c7:
            r4 = com.android.server.media.MediaSessionService.this;
            r4 = r4.mMediaEventWakeLock;
            r4.release();
            goto L_0x0081;
        L_0x00d1:
            r4 = move-exception;
            if (r8 == 0) goto L_0x00dd;
        L_0x00d4:
            r5 = com.android.server.media.MediaSessionService.this;
            r5 = r5.mMediaEventWakeLock;
            r5.release();
        L_0x00dd:
            throw r4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.media.MediaSessionService.SessionManagerImpl.startVoiceInput(boolean):void");
        }

        SessionManagerImpl() {
        }

        public ISession createSession(String packageName, ISessionCallback cb, String tag, int userId) throws RemoteException {
            int pid = Binder.getCallingPid();
            int uid = Binder.getCallingUid();
            long token = Binder.clearCallingIdentity();
            try {
                MediaSessionService.this.enforcePackageName(packageName, uid);
                int resolvedUserId = ActivityManager.handleIncomingUser(pid, uid, userId, false, true, "createSession", packageName);
                if (cb == null) {
                    throw new IllegalArgumentException("Controller callback cannot be null");
                }
                ISession sessionBinder = MediaSessionService.this.createSessionInternal(pid, uid, resolvedUserId, packageName, cb, tag).getSessionBinder();
                return sessionBinder;
            } finally {
                Binder.restoreCallingIdentity(token);
            }
        }

        public List<IBinder> getSessions(ComponentName componentName, int userId) {
            int pid = Binder.getCallingPid();
            int uid = Binder.getCallingUid();
            long token = Binder.clearCallingIdentity();
            try {
                int resolvedUserId = verifySessionsRequest(componentName, userId, pid, uid);
                ArrayList<IBinder> binders = new ArrayList();
                synchronized (MediaSessionService.this.mLock) {
                    for (MediaSessionRecord record : MediaSessionService.this.getActiveSessionsLocked(resolvedUserId)) {
                        binders.add(record.getControllerBinder().asBinder());
                    }
                }
                return binders;
            } finally {
                Binder.restoreCallingIdentity(token);
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void addSessionsListener(android.media.session.IActiveSessionsListener r13, android.content.ComponentName r14, int r15) throws android.os.RemoteException {
            /*
            r12 = this;
            r5 = android.os.Binder.getCallingPid();
            r6 = android.os.Binder.getCallingUid();
            r10 = android.os.Binder.clearCallingIdentity();
            r4 = r12.verifySessionsRequest(r14, r15, r5, r6);	 Catch:{ all -> 0x005f }
            r1 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x005f }
            r9 = r1.mLock;	 Catch:{ all -> 0x005f }
            monitor-enter(r9);	 Catch:{ all -> 0x005f }
            r1 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x005c }
            r8 = r1.findIndexOfSessionsListenerLocked(r13);	 Catch:{ all -> 0x005c }
            r1 = -1;
            if (r8 == r1) goto L_0x002e;
        L_0x0020:
            r1 = "MediaSessionService";
            r2 = "ActiveSessionsListener is already added, ignoring";
            android.util.Log.w(r1, r2);	 Catch:{ all -> 0x005c }
            monitor-exit(r9);	 Catch:{ all -> 0x005f }
            android.os.Binder.restoreCallingIdentity(r10);
            return;
        L_0x002e:
            r0 = new com.android.server.media.MediaSessionService$SessionsListenerRecord;	 Catch:{ all -> 0x005c }
            r1 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x005c }
            r2 = r13;
            r3 = r14;
            r0.<init>(r2, r3, r4, r5, r6);	 Catch:{ all -> 0x005c }
            r1 = r13.asBinder();	 Catch:{ RemoteException -> 0x004d }
            r2 = 0;
            r1.linkToDeath(r0, r2);	 Catch:{ RemoteException -> 0x004d }
            r1 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x005c }
            r1 = r1.mSessionsListeners;	 Catch:{ all -> 0x005c }
            r1.add(r0);	 Catch:{ all -> 0x005c }
            monitor-exit(r9);	 Catch:{ all -> 0x005f }
            android.os.Binder.restoreCallingIdentity(r10);
            return;
        L_0x004d:
            r7 = move-exception;
            r1 = "MediaSessionService";
            r2 = "ActiveSessionsListener is dead, ignoring it";
            android.util.Log.e(r1, r2, r7);	 Catch:{ all -> 0x005c }
            monitor-exit(r9);	 Catch:{ all -> 0x005f }
            android.os.Binder.restoreCallingIdentity(r10);
            return;
        L_0x005c:
            r1 = move-exception;
            monitor-exit(r9);	 Catch:{ all -> 0x005f }
            throw r1;	 Catch:{ all -> 0x005f }
        L_0x005f:
            r1 = move-exception;
            android.os.Binder.restoreCallingIdentity(r10);
            throw r1;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.media.MediaSessionService.SessionManagerImpl.addSessionsListener(android.media.session.IActiveSessionsListener, android.content.ComponentName, int):void");
        }

        public void removeSessionsListener(IActiveSessionsListener listener) throws RemoteException {
            synchronized (MediaSessionService.this.mLock) {
                int index = MediaSessionService.this.findIndexOfSessionsListenerLocked(listener);
                if (index != -1) {
                    SessionsListenerRecord record = (SessionsListenerRecord) MediaSessionService.this.mSessionsListeners.remove(index);
                    try {
                        record.mListener.asBinder().unlinkToDeath(record, 0);
                    } catch (Exception e) {
                    }
                }
            }
        }

        public void dispatchMediaKeyEvent(KeyEvent keyEvent, boolean needWakeLock) {
            if (keyEvent == null || (KeyEvent.isMediaKey(keyEvent.getKeyCode()) ^ 1) != 0) {
                Log.w(MediaSessionService.TAG, "Attempted to dispatch null or non-media key event.");
                return;
            }
            int pid = Binder.getCallingPid();
            int uid = Binder.getCallingUid();
            long token = Binder.clearCallingIdentity();
            try {
                if (MediaSessionService.DEBUG) {
                    Log.d(MediaSessionService.TAG, "dispatchMediaKeyEvent, pid=" + pid + ", uid=" + uid + ", event=" + keyEvent);
                }
                if (isUserSetupComplete()) {
                    synchronized (MediaSessionService.this.mLock) {
                        boolean isGlobalPriorityActive = MediaSessionService.this.isGlobalPriorityActiveLocked();
                        if (!isGlobalPriorityActive || uid == 1000) {
                            if (!isGlobalPriorityActive) {
                                if (MediaSessionService.this.mCurrentFullUserRecord.mOnMediaKeyListener != null) {
                                    Log.d(MediaSessionService.TAG, "Send " + keyEvent + " to the media key listener");
                                    try {
                                        MediaSessionService.this.mCurrentFullUserRecord.mOnMediaKeyListener.onMediaKey(keyEvent, new MediaKeyListenerResultReceiver(keyEvent, needWakeLock));
                                        Binder.restoreCallingIdentity(token);
                                        return;
                                    } catch (RemoteException e) {
                                        Log.w(MediaSessionService.TAG, "Failed to send " + keyEvent + " to the media key listener");
                                    }
                                }
                            }
                            if (isGlobalPriorityActive || !isVoiceKey(keyEvent.getKeyCode())) {
                                dispatchMediaKeyEventLocked(keyEvent, needWakeLock);
                            } else {
                                handleVoiceKeyEventLocked(keyEvent, needWakeLock);
                            }
                            Binder.restoreCallingIdentity(token);
                            return;
                        }
                        Slog.i(MediaSessionService.TAG, "Only the system can dispatch media key event to the global priority session.");
                        Binder.restoreCallingIdentity(token);
                        return;
                    }
                }
                Slog.i(MediaSessionService.TAG, "Not dispatching media key event because user setup is in progress.");
            } finally {
                Binder.restoreCallingIdentity(token);
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void setCallback(android.media.session.ICallback r12) {
            /*
            r11 = this;
            r1 = android.os.Binder.getCallingPid();
            r4 = android.os.Binder.getCallingUid();
            r2 = android.os.Binder.clearCallingIdentity();
            r7 = 1002; // 0x3ea float:1.404E-42 double:4.95E-321;
            r7 = android.os.UserHandle.isSameApp(r4, r7);	 Catch:{ all -> 0x001d }
            if (r7 != 0) goto L_0x0022;
        L_0x0014:
            r7 = new java.lang.SecurityException;	 Catch:{ all -> 0x001d }
            r8 = "Only Bluetooth service processes can set Callback";
            r7.<init>(r8);	 Catch:{ all -> 0x001d }
            throw r7;	 Catch:{ all -> 0x001d }
        L_0x001d:
            r7 = move-exception;
            android.os.Binder.restoreCallingIdentity(r2);
            throw r7;
        L_0x0022:
            r7 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x001d }
            r8 = r7.mLock;	 Catch:{ all -> 0x001d }
            monitor-enter(r8);	 Catch:{ all -> 0x001d }
            r6 = android.os.UserHandle.getUserId(r4);	 Catch:{ all -> 0x00bf }
            r7 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x00bf }
            r5 = r7.getFullUserRecordLocked(r6);	 Catch:{ all -> 0x00bf }
            if (r5 == 0) goto L_0x003b;
        L_0x0035:
            r7 = r5.mFullUserId;	 Catch:{ all -> 0x00bf }
            if (r7 == r6) goto L_0x005a;
        L_0x003b:
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00bf }
            r9.<init>();	 Catch:{ all -> 0x00bf }
            r10 = "Only the full user can set the callback, userId=";
            r9 = r9.append(r10);	 Catch:{ all -> 0x00bf }
            r9 = r9.append(r6);	 Catch:{ all -> 0x00bf }
            r9 = r9.toString();	 Catch:{ all -> 0x00bf }
            android.util.Log.w(r7, r9);	 Catch:{ all -> 0x00bf }
            monitor-exit(r8);	 Catch:{ all -> 0x001d }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x005a:
            r5.mCallback = r12;	 Catch:{ all -> 0x00bf }
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00bf }
            r9.<init>();	 Catch:{ all -> 0x00bf }
            r10 = "The callback ";
            r9 = r9.append(r10);	 Catch:{ all -> 0x00bf }
            r10 = r5.mCallback;	 Catch:{ all -> 0x00bf }
            r9 = r9.append(r10);	 Catch:{ all -> 0x00bf }
            r10 = " is set by ";
            r9 = r9.append(r10);	 Catch:{ all -> 0x00bf }
            r10 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x00bf }
            r10 = r10.getCallingPackageName(r4);	 Catch:{ all -> 0x00bf }
            r9 = r9.append(r10);	 Catch:{ all -> 0x00bf }
            r9 = r9.toString();	 Catch:{ all -> 0x00bf }
            android.util.Log.d(r7, r9);	 Catch:{ all -> 0x00bf }
            r7 = r5.mCallback;	 Catch:{ all -> 0x00bf }
            if (r7 != 0) goto L_0x0097;
        L_0x0092:
            monitor-exit(r8);	 Catch:{ all -> 0x001d }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x0097:
            r7 = r5.mCallback;	 Catch:{ RemoteException -> 0x00b0 }
            r7 = r7.asBinder();	 Catch:{ RemoteException -> 0x00b0 }
            r9 = new com.android.server.media.MediaSessionService$SessionManagerImpl$2;	 Catch:{ RemoteException -> 0x00b0 }
            r9.<init>(r5);	 Catch:{ RemoteException -> 0x00b0 }
            r10 = 0;
            r7.linkToDeath(r9, r10);	 Catch:{ RemoteException -> 0x00b0 }
            r5.pushAddressedPlayerChangedLocked();	 Catch:{ RemoteException -> 0x00b0 }
        L_0x00ab:
            monitor-exit(r8);	 Catch:{ all -> 0x001d }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x00b0:
            r0 = move-exception;
            r7 = "MediaSessionService";
            r9 = "Failed to set callback";
            android.util.Log.w(r7, r9, r0);	 Catch:{ all -> 0x00bf }
            r7 = 0;
            r5.mCallback = r7;	 Catch:{ all -> 0x00bf }
            goto L_0x00ab;
        L_0x00bf:
            r7 = move-exception;
            monitor-exit(r8);	 Catch:{ all -> 0x001d }
            throw r7;	 Catch:{ all -> 0x001d }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.media.MediaSessionService.SessionManagerImpl.setCallback(android.media.session.ICallback):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void setOnVolumeKeyLongPressListener(android.media.session.IOnVolumeKeyLongPressListener r12) {
            /*
            r11 = this;
            r1 = android.os.Binder.getCallingPid();
            r4 = android.os.Binder.getCallingUid();
            r2 = android.os.Binder.clearCallingIdentity();
            r7 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x0024 }
            r7 = r7.getContext();	 Catch:{ all -> 0x0024 }
            r8 = "android.permission.SET_VOLUME_KEY_LONG_PRESS_LISTENER";
            r7 = r7.checkPermission(r8, r1, r4);	 Catch:{ all -> 0x0024 }
            if (r7 == 0) goto L_0x0029;
        L_0x001b:
            r7 = new java.lang.SecurityException;	 Catch:{ all -> 0x0024 }
            r8 = "Must hold the SET_VOLUME_KEY_LONG_PRESS_LISTENER permission.";
            r7.<init>(r8);	 Catch:{ all -> 0x0024 }
            throw r7;	 Catch:{ all -> 0x0024 }
        L_0x0024:
            r7 = move-exception;
            android.os.Binder.restoreCallingIdentity(r2);
            throw r7;
        L_0x0029:
            r7 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x0024 }
            r8 = r7.mLock;	 Catch:{ all -> 0x0024 }
            monitor-enter(r8);	 Catch:{ all -> 0x0024 }
            r6 = android.os.UserHandle.getUserId(r4);	 Catch:{ all -> 0x010c }
            r7 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x010c }
            r5 = r7.getFullUserRecordLocked(r6);	 Catch:{ all -> 0x010c }
            if (r5 == 0) goto L_0x0042;
        L_0x003c:
            r7 = r5.mFullUserId;	 Catch:{ all -> 0x010c }
            if (r7 == r6) goto L_0x0061;
        L_0x0042:
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x010c }
            r9.<init>();	 Catch:{ all -> 0x010c }
            r10 = "Only the full user can set the volume key long-press listener, userId=";
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r9 = r9.append(r6);	 Catch:{ all -> 0x010c }
            r9 = r9.toString();	 Catch:{ all -> 0x010c }
            android.util.Log.w(r7, r9);	 Catch:{ all -> 0x010c }
            monitor-exit(r8);	 Catch:{ all -> 0x0024 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x0061:
            r7 = r5.mOnVolumeKeyLongPressListener;	 Catch:{ all -> 0x010c }
            if (r7 == 0) goto L_0x009b;
        L_0x0067:
            r7 = r5.mOnVolumeKeyLongPressListenerUid;	 Catch:{ all -> 0x010c }
            if (r7 == r4) goto L_0x009b;
        L_0x006d:
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x010c }
            r9.<init>();	 Catch:{ all -> 0x010c }
            r10 = "The volume key long-press listener cannot be reset by another app , mOnVolumeKeyLongPressListener=";
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r10 = r5.mOnVolumeKeyLongPressListenerUid;	 Catch:{ all -> 0x010c }
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r10 = ", uid=";
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r9 = r9.append(r4);	 Catch:{ all -> 0x010c }
            r9 = r9.toString();	 Catch:{ all -> 0x010c }
            android.util.Log.w(r7, r9);	 Catch:{ all -> 0x010c }
            monitor-exit(r8);	 Catch:{ all -> 0x0024 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x009b:
            r5.mOnVolumeKeyLongPressListener = r12;	 Catch:{ all -> 0x010c }
            r5.mOnVolumeKeyLongPressListenerUid = r4;	 Catch:{ all -> 0x010c }
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x010c }
            r9.<init>();	 Catch:{ all -> 0x010c }
            r10 = "The volume key long-press listener ";
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r9 = r9.append(r12);	 Catch:{ all -> 0x010c }
            r10 = " is set by ";
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r10 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x010c }
            r10 = r10.getCallingPackageName(r4);	 Catch:{ all -> 0x010c }
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r9 = r9.toString();	 Catch:{ all -> 0x010c }
            android.util.Log.d(r7, r9);	 Catch:{ all -> 0x010c }
            r7 = r5.mOnVolumeKeyLongPressListener;	 Catch:{ all -> 0x010c }
            if (r7 == 0) goto L_0x00e3;
        L_0x00d2:
            r7 = r5.mOnVolumeKeyLongPressListener;	 Catch:{ RemoteException -> 0x00e8 }
            r7 = r7.asBinder();	 Catch:{ RemoteException -> 0x00e8 }
            r9 = new com.android.server.media.MediaSessionService$SessionManagerImpl$3;	 Catch:{ RemoteException -> 0x00e8 }
            r9.<init>(r5);	 Catch:{ RemoteException -> 0x00e8 }
            r10 = 0;
            r7.linkToDeath(r9, r10);	 Catch:{ RemoteException -> 0x00e8 }
        L_0x00e3:
            monitor-exit(r8);	 Catch:{ all -> 0x0024 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x00e8:
            r0 = move-exception;
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x010c }
            r9.<init>();	 Catch:{ all -> 0x010c }
            r10 = "Failed to set death recipient ";
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r10 = r5.mOnVolumeKeyLongPressListener;	 Catch:{ all -> 0x010c }
            r9 = r9.append(r10);	 Catch:{ all -> 0x010c }
            r9 = r9.toString();	 Catch:{ all -> 0x010c }
            android.util.Log.w(r7, r9);	 Catch:{ all -> 0x010c }
            r7 = 0;
            r5.mOnVolumeKeyLongPressListener = r7;	 Catch:{ all -> 0x010c }
            goto L_0x00e3;
        L_0x010c:
            r7 = move-exception;
            monitor-exit(r8);	 Catch:{ all -> 0x0024 }
            throw r7;	 Catch:{ all -> 0x0024 }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.media.MediaSessionService.SessionManagerImpl.setOnVolumeKeyLongPressListener(android.media.session.IOnVolumeKeyLongPressListener):void");
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void setOnMediaKeyListener(android.media.session.IOnMediaKeyListener r12) {
            /*
            r11 = this;
            r1 = android.os.Binder.getCallingPid();
            r4 = android.os.Binder.getCallingUid();
            r2 = android.os.Binder.clearCallingIdentity();
            r7 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x0024 }
            r7 = r7.getContext();	 Catch:{ all -> 0x0024 }
            r8 = "android.permission.SET_MEDIA_KEY_LISTENER";
            r7 = r7.checkPermission(r8, r1, r4);	 Catch:{ all -> 0x0024 }
            if (r7 == 0) goto L_0x0029;
        L_0x001b:
            r7 = new java.lang.SecurityException;	 Catch:{ all -> 0x0024 }
            r8 = "Must hold the SET_MEDIA_KEY_LISTENER permission.";
            r7.<init>(r8);	 Catch:{ all -> 0x0024 }
            throw r7;	 Catch:{ all -> 0x0024 }
        L_0x0024:
            r7 = move-exception;
            android.os.Binder.restoreCallingIdentity(r2);
            throw r7;
        L_0x0029:
            r7 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x0024 }
            r8 = r7.mLock;	 Catch:{ all -> 0x0024 }
            monitor-enter(r8);	 Catch:{ all -> 0x0024 }
            r6 = android.os.UserHandle.getUserId(r4);	 Catch:{ all -> 0x0110 }
            r7 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x0110 }
            r5 = r7.getFullUserRecordLocked(r6);	 Catch:{ all -> 0x0110 }
            if (r5 == 0) goto L_0x0042;
        L_0x003c:
            r7 = r5.mFullUserId;	 Catch:{ all -> 0x0110 }
            if (r7 == r6) goto L_0x0061;
        L_0x0042:
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0110 }
            r9.<init>();	 Catch:{ all -> 0x0110 }
            r10 = "Only the full user can set the media key listener, userId=";
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r9 = r9.append(r6);	 Catch:{ all -> 0x0110 }
            r9 = r9.toString();	 Catch:{ all -> 0x0110 }
            android.util.Log.w(r7, r9);	 Catch:{ all -> 0x0110 }
            monitor-exit(r8);	 Catch:{ all -> 0x0024 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x0061:
            r7 = r5.mOnMediaKeyListener;	 Catch:{ all -> 0x0110 }
            if (r7 == 0) goto L_0x009b;
        L_0x0067:
            r7 = r5.mOnMediaKeyListenerUid;	 Catch:{ all -> 0x0110 }
            if (r7 == r4) goto L_0x009b;
        L_0x006d:
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0110 }
            r9.<init>();	 Catch:{ all -> 0x0110 }
            r10 = "The media key listener cannot be reset by another app. , mOnMediaKeyListenerUid=";
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r10 = r5.mOnMediaKeyListenerUid;	 Catch:{ all -> 0x0110 }
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r10 = ", uid=";
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r9 = r9.append(r4);	 Catch:{ all -> 0x0110 }
            r9 = r9.toString();	 Catch:{ all -> 0x0110 }
            android.util.Log.w(r7, r9);	 Catch:{ all -> 0x0110 }
            monitor-exit(r8);	 Catch:{ all -> 0x0024 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x009b:
            r5.mOnMediaKeyListener = r12;	 Catch:{ all -> 0x0110 }
            r5.mOnMediaKeyListenerUid = r4;	 Catch:{ all -> 0x0110 }
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0110 }
            r9.<init>();	 Catch:{ all -> 0x0110 }
            r10 = "The media key listener ";
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r10 = r5.mOnMediaKeyListener;	 Catch:{ all -> 0x0110 }
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r10 = " is set by ";
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r10 = com.android.server.media.MediaSessionService.this;	 Catch:{ all -> 0x0110 }
            r10 = r10.getCallingPackageName(r4);	 Catch:{ all -> 0x0110 }
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r9 = r9.toString();	 Catch:{ all -> 0x0110 }
            android.util.Log.d(r7, r9);	 Catch:{ all -> 0x0110 }
            r7 = r5.mOnMediaKeyListener;	 Catch:{ all -> 0x0110 }
            if (r7 == 0) goto L_0x00e7;
        L_0x00d6:
            r7 = r5.mOnMediaKeyListener;	 Catch:{ RemoteException -> 0x00ec }
            r7 = r7.asBinder();	 Catch:{ RemoteException -> 0x00ec }
            r9 = new com.android.server.media.MediaSessionService$SessionManagerImpl$4;	 Catch:{ RemoteException -> 0x00ec }
            r9.<init>(r5);	 Catch:{ RemoteException -> 0x00ec }
            r10 = 0;
            r7.linkToDeath(r9, r10);	 Catch:{ RemoteException -> 0x00ec }
        L_0x00e7:
            monitor-exit(r8);	 Catch:{ all -> 0x0024 }
            android.os.Binder.restoreCallingIdentity(r2);
            return;
        L_0x00ec:
            r0 = move-exception;
            r7 = "MediaSessionService";
            r9 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0110 }
            r9.<init>();	 Catch:{ all -> 0x0110 }
            r10 = "Failed to set death recipient ";
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r10 = r5.mOnMediaKeyListener;	 Catch:{ all -> 0x0110 }
            r9 = r9.append(r10);	 Catch:{ all -> 0x0110 }
            r9 = r9.toString();	 Catch:{ all -> 0x0110 }
            android.util.Log.w(r7, r9);	 Catch:{ all -> 0x0110 }
            r7 = 0;
            r5.mOnMediaKeyListener = r7;	 Catch:{ all -> 0x0110 }
            goto L_0x00e7;
        L_0x0110:
            r7 = move-exception;
            monitor-exit(r8);	 Catch:{ all -> 0x0024 }
            throw r7;	 Catch:{ all -> 0x0024 }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.media.MediaSessionService.SessionManagerImpl.setOnMediaKeyListener(android.media.session.IOnMediaKeyListener):void");
        }

        public void dispatchVolumeKeyEvent(KeyEvent keyEvent, int stream, boolean musicOnly) {
            if (keyEvent == null || !(keyEvent.getKeyCode() == 24 || keyEvent.getKeyCode() == 25 || keyEvent.getKeyCode() == 164)) {
                Log.w(MediaSessionService.TAG, "Attempted to dispatch null or non-volume key event.");
                return;
            }
            int pid = Binder.getCallingPid();
            int uid = Binder.getCallingUid();
            long token = Binder.clearCallingIdentity();
            Log.d(MediaSessionService.TAG, "dispatchVolumeKeyEvent, pid=" + pid + ", uid=" + uid + ", event=" + keyEvent);
            try {
                synchronized (MediaSessionService.this.mLock) {
                    if (MediaSessionService.this.isGlobalPriorityActiveLocked() || MediaSessionService.this.mCurrentFullUserRecord.mOnVolumeKeyLongPressListener == null) {
                        dispatchVolumeKeyEventLocked(keyEvent, stream, musicOnly);
                    } else if (keyEvent.getAction() == 0) {
                        if (keyEvent.getRepeatCount() == 0) {
                            MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeKeyEvent = KeyEvent.obtain(keyEvent);
                            MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeStream = stream;
                            MediaSessionService.this.mCurrentFullUserRecord.mInitialDownMusicOnly = musicOnly;
                            MediaSessionService.this.mHandler.sendMessageDelayed(MediaSessionService.this.mHandler.obtainMessage(2, MediaSessionService.this.mCurrentFullUserRecord.mFullUserId, 0), (long) MediaSessionService.this.mLongPressTimeout);
                        }
                        if (keyEvent.getRepeatCount() > 0 || keyEvent.isLongPress()) {
                            MediaSessionService.this.mHandler.removeMessages(2);
                            if (MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeKeyEvent != null) {
                                MediaSessionService.this.dispatchVolumeKeyLongPressLocked(MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeKeyEvent);
                                MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeKeyEvent = null;
                            }
                            MediaSessionService.this.dispatchVolumeKeyLongPressLocked(keyEvent);
                        }
                    } else {
                        MediaSessionService.this.mHandler.removeMessages(2);
                        if (MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeKeyEvent == null || MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeKeyEvent.getDownTime() != keyEvent.getDownTime()) {
                            MediaSessionService.this.dispatchVolumeKeyLongPressLocked(keyEvent);
                        } else {
                            dispatchVolumeKeyEventLocked(MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeKeyEvent, MediaSessionService.this.mCurrentFullUserRecord.mInitialDownVolumeStream, MediaSessionService.this.mCurrentFullUserRecord.mInitialDownMusicOnly);
                            dispatchVolumeKeyEventLocked(keyEvent, stream, musicOnly);
                        }
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(token);
            }
        }

        private void dispatchVolumeKeyEventLocked(KeyEvent keyEvent, int stream, boolean musicOnly) {
            boolean down = keyEvent.getAction() == 0;
            boolean up = keyEvent.getAction() == 1;
            int direction = 0;
            boolean isMute = false;
            switch (keyEvent.getKeyCode()) {
                case 24:
                    direction = 1;
                    break;
                case 25:
                    direction = -1;
                    break;
                case 164:
                    isMute = true;
                    break;
            }
            if (down || up) {
                int flags;
                if (musicOnly) {
                    flags = 4608;
                } else if (up) {
                    flags = 4116;
                } else {
                    flags = 4113;
                }
                if (direction != 0) {
                    if (up) {
                        direction = 0;
                    }
                    dispatchAdjustVolumeLocked(stream, direction, flags);
                } else if (isMute && down && keyEvent.getRepeatCount() == 0) {
                    dispatchAdjustVolumeLocked(stream, 101, flags);
                }
            }
        }

        public void dispatchAdjustVolume(int suggestedStream, int delta, int flags) {
            long token = Binder.clearCallingIdentity();
            try {
                synchronized (MediaSessionService.this.mLock) {
                    dispatchAdjustVolumeLocked(suggestedStream, delta, flags);
                }
            } finally {
                Binder.restoreCallingIdentity(token);
            }
        }

        public void setRemoteVolumeController(IRemoteVolumeController rvc) {
            int pid = Binder.getCallingPid();
            int uid = Binder.getCallingUid();
            long token = Binder.clearCallingIdentity();
            try {
                MediaSessionService.this.enforceSystemUiPermission("listen for volume changes", pid, uid);
                MediaSessionService.this.mRvc = rvc;
            } finally {
                Binder.restoreCallingIdentity(token);
            }
        }

        public boolean isGlobalPriorityActive() {
            boolean -wrap0;
            synchronized (MediaSessionService.this.mLock) {
                -wrap0 = MediaSessionService.this.isGlobalPriorityActiveLocked();
            }
            return -wrap0;
        }

        public void dump(FileDescriptor fd, PrintWriter pw, String[] args) {
            if (DumpUtils.checkDumpPermission(MediaSessionService.this.getContext(), MediaSessionService.TAG, pw)) {
                pw.println("MEDIA SESSION SERVICE (dumpsys media_session)");
                pw.println();
                synchronized (MediaSessionService.this.mLock) {
                    pw.println(MediaSessionService.this.mSessionsListeners.size() + " sessions listeners.");
                    pw.println("Global priority session is " + MediaSessionService.this.mGlobalPrioritySession);
                    if (MediaSessionService.this.mGlobalPrioritySession != null) {
                        MediaSessionService.this.mGlobalPrioritySession.dump(pw, "  ");
                    }
                    pw.println("User Records:");
                    int count = MediaSessionService.this.mUserRecords.size();
                    for (int i = 0; i < count; i++) {
                        ((FullUserRecord) MediaSessionService.this.mUserRecords.valueAt(i)).dumpLocked(pw, "");
                    }
                    MediaSessionService.this.mAudioPlaybackMonitor.dump(pw, "");
                }
            }
        }

        private int verifySessionsRequest(ComponentName componentName, int userId, int pid, int uid) {
            String str = null;
            if (componentName != null) {
                str = componentName.getPackageName();
                MediaSessionService.this.enforcePackageName(str, uid);
            }
            int resolvedUserId = ActivityManager.handleIncomingUser(pid, uid, userId, true, true, "getSessions", str);
            MediaSessionService.this.enforceMediaPermissions(componentName, pid, uid, resolvedUserId);
            return resolvedUserId;
        }

        private void dispatchAdjustVolumeLocked(final int suggestedStream, final int direction, final int flags) {
            MediaSessionRecord session;
            if (MediaSessionService.this.isGlobalPriorityActiveLocked()) {
                session = MediaSessionService.this.mGlobalPrioritySession;
            } else {
                session = MediaSessionService.this.mCurrentFullUserRecord.mPriorityStack.getDefaultVolumeSession();
            }
            boolean preferSuggestedStream = false;
            if (isValidLocalStreamType(suggestedStream) && AudioSystem.isStreamActive(suggestedStream, 0)) {
                preferSuggestedStream = true;
            }
            Log.d(MediaSessionService.TAG, "Adjusting " + session + " by " + direction + ". flags=" + flags + ", suggestedStream=" + suggestedStream + ", preferSuggestedStream=" + preferSuggestedStream);
            if (session != null && !preferSuggestedStream) {
                session.adjustVolume(direction, flags, MediaSessionService.this.getContext().getPackageName(), 1000, true);
            } else if ((flags & 512) == 0 || (AudioSystem.isStreamActive(3, 0) ^ 1) == 0) {
                MediaSessionService.this.mHandler.post(new Runnable() {
                    public void run() {
                        try {
                            MediaSessionService.this.mAudioService.adjustSuggestedStreamVolume(direction, suggestedStream, flags, MediaSessionService.this.getContext().getOpPackageName(), MediaSessionService.TAG);
                        } catch (RemoteException e) {
                            Log.e(MediaSessionService.TAG, "Error adjusting default volume.", e);
                        } catch (IllegalArgumentException e2) {
                            Log.e(MediaSessionService.TAG, "Cannot adjust volume: direction=" + direction + ", suggestedStream=" + suggestedStream + ", flags=" + flags, e2);
                        }
                    }
                });
            } else {
                if (MediaSessionService.DEBUG) {
                    Log.d(MediaSessionService.TAG, "No active session to adjust, skipping media only volume event");
                }
            }
        }

        private void handleVoiceKeyEventLocked(KeyEvent keyEvent, boolean needWakeLock) {
            int action = keyEvent.getAction();
            boolean isLongPress = (keyEvent.getFlags() & 128) != 0;
            if (action == 0) {
                if (keyEvent.getRepeatCount() == 0) {
                    this.mVoiceButtonDown = true;
                    this.mVoiceButtonHandled = false;
                } else if (this.mVoiceButtonDown && (this.mVoiceButtonHandled ^ 1) != 0 && isLongPress) {
                    this.mVoiceButtonHandled = true;
                    startVoiceInput(needWakeLock);
                }
            } else if (action == 1 && this.mVoiceButtonDown) {
                this.mVoiceButtonDown = false;
                if (!this.mVoiceButtonHandled && (keyEvent.isCanceled() ^ 1) != 0) {
                    dispatchMediaKeyEventLocked(KeyEvent.changeAction(keyEvent, 0), needWakeLock);
                    dispatchMediaKeyEventLocked(keyEvent, needWakeLock);
                }
            }
        }

        private void dispatchMediaKeyEventLocked(KeyEvent keyEvent, boolean needWakeLock) {
            int i = -1;
            MediaSessionRecord session = MediaSessionService.this.mCurrentFullUserRecord.getMediaButtonSessionLocked();
            if (session != null) {
                Log.d(MediaSessionService.TAG, "Sending " + keyEvent + " to " + session);
                if (needWakeLock) {
                    this.mKeyEventReceiver.aquireWakeLockLocked();
                }
                session.sendMediaButton(keyEvent, needWakeLock ? this.mKeyEventReceiver.mLastTimeoutId : -1, this.mKeyEventReceiver, 1000, MediaSessionService.this.getContext().getPackageName());
                if (MediaSessionService.this.mCurrentFullUserRecord.mCallback != null) {
                    try {
                        MediaSessionService.this.mCurrentFullUserRecord.mCallback.onMediaKeyEventDispatchedToMediaSession(keyEvent, new Token(session.getControllerBinder()));
                    } catch (RemoteException e) {
                        Log.w(MediaSessionService.TAG, "Failed to send callback", e);
                    }
                }
            } else if (MediaSessionService.this.mCurrentFullUserRecord.mLastMediaButtonReceiver != null || MediaSessionService.this.mCurrentFullUserRecord.mRestoredMediaButtonReceiver != null) {
                if (needWakeLock) {
                    this.mKeyEventReceiver.aquireWakeLockLocked();
                }
                Intent mediaButtonIntent = new Intent("android.intent.action.MEDIA_BUTTON");
                mediaButtonIntent.addFlags(268435456);
                mediaButtonIntent.putExtra("android.intent.extra.KEY_EVENT", keyEvent);
                try {
                    if (MediaSessionService.this.mCurrentFullUserRecord.mLastMediaButtonReceiver != null) {
                        PendingIntent receiver = MediaSessionService.this.mCurrentFullUserRecord.mLastMediaButtonReceiver;
                        Log.d(MediaSessionService.TAG, "Sending " + keyEvent + " to the last known PendingIntent " + receiver);
                        Context context = MediaSessionService.this.getContext();
                        if (needWakeLock) {
                            i = this.mKeyEventReceiver.mLastTimeoutId;
                        }
                        receiver.send(context, i, mediaButtonIntent, this.mKeyEventReceiver, MediaSessionService.this.mHandler);
                        if (MediaSessionService.this.mCurrentFullUserRecord.mCallback != null) {
                            ComponentName componentName = MediaSessionService.this.mCurrentFullUserRecord.mLastMediaButtonReceiver.getIntent().getComponent();
                            if (componentName != null) {
                                MediaSessionService.this.mCurrentFullUserRecord.mCallback.onMediaKeyEventDispatchedToMediaButtonReceiver(keyEvent, componentName);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    ComponentName receiver2 = MediaSessionService.this.mCurrentFullUserRecord.mRestoredMediaButtonReceiver;
                    Log.d(MediaSessionService.TAG, "Sending " + keyEvent + " to the restored intent " + receiver2);
                    mediaButtonIntent.setComponent(receiver2);
                    MediaSessionService.this.getContext().sendBroadcastAsUser(mediaButtonIntent, UserHandle.of(MediaSessionService.this.mCurrentFullUserRecord.mRestoredMediaButtonReceiverUserId));
                    if (MediaSessionService.this.mCurrentFullUserRecord.mCallback != null) {
                        MediaSessionService.this.mCurrentFullUserRecord.mCallback.onMediaKeyEventDispatchedToMediaButtonReceiver(keyEvent, receiver2);
                    }
                } catch (CanceledException e2) {
                    Log.i(MediaSessionService.TAG, "Error sending key event to media button receiver " + MediaSessionService.this.mCurrentFullUserRecord.mLastMediaButtonReceiver, e2);
                } catch (RemoteException e3) {
                    Log.w(MediaSessionService.TAG, "Failed to send callback", e3);
                }
            }
        }

        private boolean isVoiceKey(int keyCode) {
            if (keyCode != 79) {
                return !MediaSessionService.this.mHasFeatureLeanback && keyCode == 85;
            } else {
                return true;
            }
        }

        private boolean isUserSetupComplete() {
            return Secure.getIntForUser(MediaSessionService.this.getContext().getContentResolver(), "user_setup_complete", 0, -2) != 0;
        }

        private boolean isValidLocalStreamType(int streamType) {
            if (streamType < 0 || streamType > 5) {
                return false;
            }
            return true;
        }
    }

    final class SessionsListenerRecord implements DeathRecipient {
        private final ComponentName mComponentName;
        private final IActiveSessionsListener mListener;
        private final int mPid;
        private final int mUid;
        private final int mUserId;

        public SessionsListenerRecord(IActiveSessionsListener listener, ComponentName componentName, int userId, int pid, int uid) {
            this.mListener = listener;
            this.mComponentName = componentName;
            this.mUserId = userId;
            this.mPid = pid;
            this.mUid = uid;
        }

        public void binderDied() {
            synchronized (MediaSessionService.this.mLock) {
                MediaSessionService.this.mSessionsListeners.remove(this);
            }
        }
    }

    final class SettingsObserver extends ContentObserver {
        private final Uri mSecureSettingsUri;

        private SettingsObserver() {
            super(null);
            this.mSecureSettingsUri = Secure.getUriFor("enabled_notification_listeners");
        }

        private void observe() {
            MediaSessionService.this.mContentResolver.registerContentObserver(this.mSecureSettingsUri, false, this, -1);
        }

        public void onChange(boolean selfChange, Uri uri) {
            MediaSessionService.this.updateActiveSessionListeners();
        }
    }

    public MediaSessionService(Context context) {
        super(context);
        this.mMediaEventWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "handleMediaEvent");
        this.mLongPressTimeout = ViewConfiguration.getLongPressTimeout();
        this.mNotificationManager = INotificationManager.Stub.asInterface(ServiceManager.getService("notification"));
    }

    public void onStart() {
        publishBinderService("media_session", this.mSessionManagerImpl);
        Watchdog.getInstance().addMonitor(this);
        this.mKeyguardManager = (KeyguardManager) getContext().getSystemService("keyguard");
        this.mAudioService = getAudioService();
        this.mAudioPlaybackMonitor = AudioPlaybackMonitor.getInstance(getContext(), this.mAudioService);
        this.mAudioPlaybackMonitor.registerOnAudioPlaybackStartedListener(new OnAudioPlaybackStartedListener() {
            public void onAudioPlaybackStarted(int uid) {
                synchronized (MediaSessionService.this.mLock) {
                    FullUserRecord user = MediaSessionService.this.getFullUserRecordLocked(UserHandle.getUserId(uid));
                    if (user != null) {
                        user.mPriorityStack.updateMediaButtonSessionIfNeeded();
                    }
                }
            }
        });
        this.mAudioManagerInternal = (AudioManagerInternal) LocalServices.getService(AudioManagerInternal.class);
        this.mContentResolver = getContext().getContentResolver();
        this.mSettingsObserver = new SettingsObserver();
        this.mSettingsObserver.observe();
        this.mHasFeatureLeanback = getContext().getPackageManager().hasSystemFeature("android.software.leanback");
        updateUser();
    }

    private IAudioService getAudioService() {
        return IAudioService.Stub.asInterface(ServiceManager.getService("audio"));
    }

    private boolean isGlobalPriorityActiveLocked() {
        return this.mGlobalPrioritySession != null ? this.mGlobalPrioritySession.isActive() : false;
    }

    public void updateSession(MediaSessionRecord record) {
        synchronized (this.mLock) {
            FullUserRecord user = getFullUserRecordLocked(record.getUserId());
            if (user == null) {
                Log.w(TAG, "Unknown session updated. Ignoring.");
                return;
            }
            if ((record.getFlags() & 65536) != 0) {
                Log.d(TAG, "Global priority session is updated, active=" + record.isActive());
                user.pushAddressedPlayerChangedLocked();
            } else if (user.mPriorityStack.contains(record)) {
                user.mPriorityStack.onSessionStateChange(record);
            } else {
                Log.w(TAG, "Unknown session updated. Ignoring.");
                return;
            }
            this.mHandler.postSessionsChanged(record.getUserId());
        }
    }

    public void setGlobalPrioritySession(MediaSessionRecord record) {
        synchronized (this.mLock) {
            FullUserRecord user = getFullUserRecordLocked(record.getUserId());
            if (this.mGlobalPrioritySession != record) {
                Log.d(TAG, "Global priority session is changed from " + this.mGlobalPrioritySession + " to " + record);
                this.mGlobalPrioritySession = record;
                if (user != null && user.mPriorityStack.contains(record)) {
                    user.mPriorityStack.removeSession(record);
                }
            }
        }
    }

    private List<MediaSessionRecord> getActiveSessionsLocked(int userId) {
        List<MediaSessionRecord> records = new ArrayList();
        if (userId == -1) {
            int size = this.mUserRecords.size();
            for (int i = 0; i < size; i++) {
                records.addAll(((FullUserRecord) this.mUserRecords.valueAt(i)).mPriorityStack.getActiveSessions(userId));
            }
        } else {
            FullUserRecord user = getFullUserRecordLocked(userId);
            if (user == null) {
                Log.w(TAG, "getSessions failed. Unknown user " + userId);
                return records;
            }
            records.addAll(user.mPriorityStack.getActiveSessions(userId));
        }
        if (isGlobalPriorityActiveLocked() && (userId == -1 || userId == this.mGlobalPrioritySession.getUserId())) {
            records.add(0, this.mGlobalPrioritySession);
        }
        return records;
    }

    public void notifyRemoteVolumeChanged(int flags, MediaSessionRecord session) {
        if (this.mRvc != null && (session.isActive() ^ 1) == 0) {
            try {
                this.mRvc.remoteVolumeChanged(session.getControllerBinder(), flags);
            } catch (Exception e) {
                Log.wtf(TAG, "Error sending volume change to system UI.", e);
            }
        }
    }

    public void onSessionPlaystateChanged(MediaSessionRecord record, int oldState, int newState) {
        synchronized (this.mLock) {
            FullUserRecord user = getFullUserRecordLocked(record.getUserId());
            if (user == null || (user.mPriorityStack.contains(record) ^ 1) != 0) {
                Log.d(TAG, "Unknown session changed playback state. Ignoring.");
                return;
            }
            user.mPriorityStack.onPlaystateChanged(record, oldState, newState);
        }
    }

    public void onSessionPlaybackTypeChanged(MediaSessionRecord record) {
        synchronized (this.mLock) {
            FullUserRecord user = getFullUserRecordLocked(record.getUserId());
            if (user == null || (user.mPriorityStack.contains(record) ^ 1) != 0) {
                Log.d(TAG, "Unknown session changed playback type. Ignoring.");
                return;
            }
            pushRemoteVolumeUpdateLocked(record.getUserId());
        }
    }

    public void onStartUser(int userId) {
        if (DEBUG) {
            Log.d(TAG, "onStartUser: " + userId);
        }
        updateUser();
    }

    public void onSwitchUser(int userId) {
        if (DEBUG) {
            Log.d(TAG, "onSwitchUser: " + userId);
        }
        updateUser();
    }

    public void onStopUser(int userId) {
        if (DEBUG) {
            Log.d(TAG, "onStopUser: " + userId);
        }
        synchronized (this.mLock) {
            FullUserRecord user = getFullUserRecordLocked(userId);
            if (user != null) {
                if (user.mFullUserId == userId) {
                    user.destroySessionsForUserLocked(-1);
                    this.mUserRecords.remove(userId);
                } else {
                    user.destroySessionsForUserLocked(userId);
                }
            }
            updateUser();
        }
    }

    public void monitor() {
        synchronized (this.mLock) {
        }
    }

    protected void enforcePhoneStatePermission(int pid, int uid) {
        if (getContext().checkPermission("android.permission.MODIFY_PHONE_STATE", pid, uid) != 0) {
            throw new SecurityException("Must hold the MODIFY_PHONE_STATE permission.");
        }
    }

    void sessionDied(MediaSessionRecord session) {
        synchronized (this.mLock) {
            destroySessionLocked(session);
        }
    }

    void destroySession(MediaSessionRecord session) {
        synchronized (this.mLock) {
            destroySessionLocked(session);
        }
    }

    private void updateUser() {
        synchronized (this.mLock) {
            UserManager manager = (UserManager) getContext().getSystemService("user");
            this.mFullUserIds.clear();
            List<UserInfo> allUsers = manager.getUsers();
            if (allUsers != null) {
                for (UserInfo userInfo : allUsers) {
                    if (userInfo.isManagedProfile()) {
                        this.mFullUserIds.put(userInfo.id, userInfo.profileGroupId);
                    } else {
                        this.mFullUserIds.put(userInfo.id, userInfo.id);
                        if (this.mUserRecords.get(userInfo.id) == null) {
                            this.mUserRecords.put(userInfo.id, new FullUserRecord(userInfo.id));
                        }
                    }
                }
            }
            int currentFullUserId = ActivityManager.getCurrentUser();
            this.mCurrentFullUserRecord = (FullUserRecord) this.mUserRecords.get(currentFullUserId);
            if (this.mCurrentFullUserRecord == null) {
                Log.w(TAG, "Cannot find FullUserInfo for the current user " + currentFullUserId);
                this.mCurrentFullUserRecord = new FullUserRecord(currentFullUserId);
                this.mUserRecords.put(currentFullUserId, this.mCurrentFullUserRecord);
            }
            this.mFullUserIds.put(currentFullUserId, currentFullUserId);
        }
    }

    private void updateActiveSessionListeners() {
        synchronized (this.mLock) {
            SessionsListenerRecord listener;
            for (int i = this.mSessionsListeners.size() - 1; i >= 0; i--) {
                listener = (SessionsListenerRecord) this.mSessionsListeners.get(i);
                try {
                    enforceMediaPermissions(listener.mComponentName, listener.mPid, listener.mUid, listener.mUserId);
                } catch (SecurityException e) {
                    Log.i(TAG, "ActiveSessionsListener " + listener.mComponentName + " is no longer authorized. Disconnecting.");
                    this.mSessionsListeners.remove(i);
                    try {
                        listener.mListener.onActiveSessionsChanged(new ArrayList());
                    } catch (Exception e2) {
                    }
                }
            }
        }
    }

    private void destroySessionLocked(MediaSessionRecord session) {
        if (DEBUG) {
            Log.d(TAG, "Destroying " + session);
        }
        FullUserRecord user = getFullUserRecordLocked(session.getUserId());
        if (this.mGlobalPrioritySession == session) {
            this.mGlobalPrioritySession = null;
            if (session.isActive() && user != null) {
                user.pushAddressedPlayerChangedLocked();
            }
        } else if (user != null) {
            user.mPriorityStack.removeSession(session);
        }
        try {
            session.getCallback().asBinder().unlinkToDeath(session, 0);
        } catch (Exception e) {
        }
        session.onDestroy();
        this.mHandler.postSessionsChanged(session.getUserId());
    }

    private void enforcePackageName(String packageName, int uid) {
        if (TextUtils.isEmpty(packageName)) {
            throw new IllegalArgumentException("packageName may not be empty");
        }
        String[] packages = getContext().getPackageManager().getPackagesForUid(uid);
        int packageCount = packages.length;
        int i = 0;
        while (i < packageCount) {
            if (!packageName.equals(packages[i])) {
                i++;
            } else {
                return;
            }
        }
        throw new IllegalArgumentException("packageName is not owned by the calling process");
    }

    private void enforceMediaPermissions(ComponentName compName, int pid, int uid, int resolvedUserId) {
        if (!isCurrentVolumeController(uid, pid) && getContext().checkPermission("android.permission.MEDIA_CONTENT_CONTROL", pid, uid) != 0 && (isEnabledNotificationListener(compName, UserHandle.getUserId(uid), resolvedUserId) ^ 1) != 0) {
            throw new SecurityException("Missing permission to control media.");
        }
    }

    private boolean isCurrentVolumeController(int uid, int pid) {
        return getContext().checkPermission("android.permission.STATUS_BAR_SERVICE", pid, uid) == 0;
    }

    private void enforceSystemUiPermission(String action, int pid, int uid) {
        if (!isCurrentVolumeController(uid, pid)) {
            throw new SecurityException("Only system ui may " + action);
        }
    }

    private boolean isEnabledNotificationListener(ComponentName compName, int userId, int forUserId) {
        if (userId != forUserId) {
            return false;
        }
        if (DEBUG) {
            Log.d(TAG, "Checking if enabled notification listener " + compName);
        }
        if (compName != null) {
            try {
                return this.mNotificationManager.isNotificationListenerAccessGrantedForUser(compName, userId);
            } catch (RemoteException e) {
                Log.w(TAG, "Dead NotificationManager in isEnabledNotificationListener", e);
            }
        }
        return false;
    }

    private MediaSessionRecord createSessionInternal(int callerPid, int callerUid, int userId, String callerPackageName, ISessionCallback cb, String tag) throws RemoteException {
        MediaSessionRecord createSessionLocked;
        synchronized (this.mLock) {
            createSessionLocked = createSessionLocked(callerPid, callerUid, userId, callerPackageName, cb, tag);
        }
        return createSessionLocked;
    }

    private MediaSessionRecord createSessionLocked(int callerPid, int callerUid, int userId, String callerPackageName, ISessionCallback cb, String tag) {
        FullUserRecord user = getFullUserRecordLocked(userId);
        if (user == null) {
            Log.wtf(TAG, "Request from invalid user: " + userId);
            throw new RuntimeException("Session request from invalid user.");
        }
        MediaSessionRecord session = new MediaSessionRecord(callerPid, callerUid, userId, callerPackageName, cb, tag, this, this.mHandler.getLooper());
        try {
            cb.asBinder().linkToDeath(session, 0);
            user.mPriorityStack.addSession(session);
            this.mHandler.postSessionsChanged(userId);
            if (DEBUG) {
                Log.d(TAG, "Created session for " + callerPackageName + " with tag " + tag);
            }
            return session;
        } catch (RemoteException e) {
            throw new RuntimeException("Media Session owner died prematurely.", e);
        }
    }

    private int findIndexOfSessionsListenerLocked(IActiveSessionsListener listener) {
        for (int i = this.mSessionsListeners.size() - 1; i >= 0; i--) {
            if (((SessionsListenerRecord) this.mSessionsListeners.get(i)).mListener.asBinder() == listener.asBinder()) {
                return i;
            }
        }
        return -1;
    }

    private void pushSessionsChanged(int userId) {
        synchronized (this.mLock) {
            if (getFullUserRecordLocked(userId) == null) {
                Log.w(TAG, "pushSessionsChanged failed. No user with id=" + userId);
                return;
            }
            int i;
            List<MediaSessionRecord> records = getActiveSessionsLocked(userId);
            int size = records.size();
            ArrayList<Token> tokens = new ArrayList();
            for (i = 0; i < size; i++) {
                tokens.add(new Token(((MediaSessionRecord) records.get(i)).getControllerBinder()));
            }
            pushRemoteVolumeUpdateLocked(userId);
            for (i = this.mSessionsListeners.size() - 1; i >= 0; i--) {
                SessionsListenerRecord record = (SessionsListenerRecord) this.mSessionsListeners.get(i);
                if (record.mUserId == -1 || record.mUserId == userId) {
                    try {
                        record.mListener.onActiveSessionsChanged(tokens);
                    } catch (RemoteException e) {
                        Log.w(TAG, "Dead ActiveSessionsListener in pushSessionsChanged, removing", e);
                        this.mSessionsListeners.remove(i);
                    }
                }
            }
        }
    }

    private void pushRemoteVolumeUpdateLocked(int userId) {
        ISessionController iSessionController = null;
        if (this.mRvc != null) {
            try {
                FullUserRecord user = getFullUserRecordLocked(userId);
                if (user == null) {
                    Log.w(TAG, "pushRemoteVolumeUpdateLocked failed. No user with id=" + userId);
                    return;
                }
                MediaSessionRecord record = user.mPriorityStack.getDefaultRemoteSession(userId);
                IRemoteVolumeController iRemoteVolumeController = this.mRvc;
                if (record != null) {
                    iSessionController = record.getControllerBinder();
                }
                iRemoteVolumeController.updateRemoteController(iSessionController);
            } catch (RemoteException e) {
                Log.wtf(TAG, "Error sending default remote volume to sys ui.", e);
            }
        }
    }

    public void onMediaButtonReceiverChanged(MediaSessionRecord record) {
        synchronized (this.mLock) {
            FullUserRecord user = getFullUserRecordLocked(record.getUserId());
            MediaSessionRecord mediaButtonSession = user.mPriorityStack.getMediaButtonSession();
            if (record == mediaButtonSession) {
                user.rememberMediaButtonReceiverLocked(mediaButtonSession);
            }
        }
    }

    private String getCallingPackageName(int uid) {
        String[] packages = getContext().getPackageManager().getPackagesForUid(uid);
        if (packages == null || packages.length <= 0) {
            return "";
        }
        return packages[0];
    }

    private void dispatchVolumeKeyLongPressLocked(KeyEvent keyEvent) {
        try {
            this.mCurrentFullUserRecord.mOnVolumeKeyLongPressListener.onVolumeKeyLongPress(keyEvent);
        } catch (RemoteException e) {
            Log.w(TAG, "Failed to send " + keyEvent + " to volume key long-press listener");
        }
    }

    private FullUserRecord getFullUserRecordLocked(int userId) {
        int fullUserId = this.mFullUserIds.get(userId, -1);
        if (fullUserId < 0) {
            return null;
        }
        return (FullUserRecord) this.mUserRecords.get(fullUserId);
    }
}
