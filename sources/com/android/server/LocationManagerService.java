package com.android.server;

import android.app.ActivityManager;
import android.app.ActivityManager.OnUidImportanceListener;
import android.app.AppOpsManager;
import android.app.AppOpsManager.OnOpChangedInternalListener;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.PendingIntent.OnFinished;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageManager.OnPermissionsChangedListener;
import android.content.pm.PackageManagerInternal;
import android.content.pm.PackageManagerInternal.PackagesProvider;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.hardware.location.ActivityRecognitionHardware;
import android.location.Address;
import android.location.Criteria;
import android.location.GeocoderParams;
import android.location.Geofence;
import android.location.IBatchedLocationCallback;
import android.location.IFusedGeofenceHardware;
import android.location.IGnssMeasurementsListener;
import android.location.IGnssNavigationMessageListener;
import android.location.IGnssStatusListener;
import android.location.IGnssStatusProvider;
import android.location.IGpsGeofenceHardware;
import android.location.ILocationListener;
import android.location.ILocationManager.Stub;
import android.location.INetInitiatedListener;
import android.location.Location;
import android.location.LocationProvider;
import android.location.LocationRequest;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IBinder.DeathRecipient;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.WorkSource;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.EventLog;
import android.util.Log;
import android.util.Slog;
import com.android.internal.content.PackageMonitor;
import com.android.internal.location.ProviderProperties;
import com.android.internal.location.ProviderRequest;
import com.android.internal.os.BackgroundThread;
import com.android.internal.util.ArrayUtils;
import com.android.server.location.ActivityRecognitionProxy;
import com.android.server.location.FlpHardwareProvider;
import com.android.server.location.FusedProxy;
import com.android.server.location.GeocoderProxy;
import com.android.server.location.GeofenceManager;
import com.android.server.location.GeofenceProxy;
import com.android.server.location.GnssLocationProvider;
import com.android.server.location.GnssLocationProvider.GnssBatchingProvider;
import com.android.server.location.GnssLocationProvider.GnssMetricsProvider;
import com.android.server.location.GnssLocationProvider.GnssSystemInfoProvider;
import com.android.server.location.GnssMeasurementsProvider;
import com.android.server.location.GnssNavigationMessageProvider;
import com.android.server.location.LocationBlacklist;
import com.android.server.location.LocationFudger;
import com.android.server.location.LocationProviderInterface;
import com.android.server.location.LocationProviderProxy;
import com.android.server.location.LocationRequestStatistics;
import com.android.server.location.MockProvider;
import com.android.server.location.PassiveProvider;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;

public class LocationManagerService extends Stub {
    private static final String ACCESS_LOCATION_EXTRA_COMMANDS = "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS";
    private static final String ACCESS_MOCK_LOCATION = "android.permission.ACCESS_MOCK_LOCATION";
    public static final boolean D = Log.isLoggable("LocationManagerService", 3);
    private static final long DEFAULT_BACKGROUND_THROTTLE_INTERVAL_MS = 1800000;
    private static final LocationRequest DEFAULT_LOCATION_REQUEST = new LocationRequest();
    private static final int FOREGROUND_IMPORTANCE_CUTOFF = 125;
    private static final String FUSED_LOCATION_SERVICE_ACTION = "com.android.location.service.FusedLocationProvider";
    private static final long HIGH_POWER_INTERVAL_MS = 300000;
    private static final String INSTALL_LOCATION_PROVIDER = "android.permission.INSTALL_LOCATION_PROVIDER";
    private static final int MAX_PROVIDER_SCHEDULING_JITTER_MS = 100;
    private static final int MSG_LOCATION_CHANGED = 1;
    private static final long NANOS_PER_MILLI = 1000000;
    private static final String NETWORK_LOCATION_SERVICE_ACTION = "com.android.location.service.v3.NetworkLocationProvider";
    private static final int RESOLUTION_LEVEL_COARSE = 1;
    private static final int RESOLUTION_LEVEL_FINE = 2;
    private static final int RESOLUTION_LEVEL_NONE = 0;
    private static final String TAG = "LocationManagerService";
    private static final String WAKELOCK_KEY = "LocationManagerService";
    private ActivityManager mActivityManager;
    private final AppOpsManager mAppOps;
    private final ArraySet<String> mBackgroundThrottlePackageWhitelist = new ArraySet();
    private LocationBlacklist mBlacklist;
    private final Context mContext;
    private int mCurrentUserId = 0;
    private int[] mCurrentUserProfiles = new int[]{0};
    private final Set<String> mDisabledProviders = new HashSet();
    private final Set<String> mEnabledProviders = new HashSet();
    private GeocoderProxy mGeocodeProvider;
    private GeofenceManager mGeofenceManager;
    private IBatchedLocationCallback mGnssBatchingCallback;
    private LinkedCallback mGnssBatchingDeathCallback;
    private boolean mGnssBatchingInProgress = false;
    private GnssBatchingProvider mGnssBatchingProvider;
    private final ArrayMap<IGnssMeasurementsListener, Identity> mGnssMeasurementsListeners = new ArrayMap();
    private GnssMeasurementsProvider mGnssMeasurementsProvider;
    private GnssMetricsProvider mGnssMetricsProvider;
    private final ArrayMap<IGnssNavigationMessageListener, Identity> mGnssNavigationMessageListeners = new ArrayMap();
    private GnssNavigationMessageProvider mGnssNavigationMessageProvider;
    private IGnssStatusProvider mGnssStatusProvider;
    private GnssSystemInfoProvider mGnssSystemInfoProvider;
    private IGpsGeofenceHardware mGpsGeofenceProxy;
    private final HashMap<String, Location> mLastLocation = new HashMap();
    private final HashMap<String, Location> mLastLocationCoarseInterval = new HashMap();
    private LocationFudger mLocationFudger;
    private LocationWorkerHandler mLocationHandler;
    private final Object mLock = new Object();
    private final HashMap<String, MockProvider> mMockProviders = new HashMap();
    private INetInitiatedListener mNetInitiatedListener;
    private PackageManager mPackageManager;
    private final PackageMonitor mPackageMonitor = new PackageMonitor() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onPackageDisappeared(java.lang.String r7, int r8) {
            /*
            r6 = this;
            r4 = com.android.server.LocationManagerService.this;
            r5 = r4.mLock;
            monitor-enter(r5);
            r0 = 0;
            r4 = com.android.server.LocationManagerService.this;	 Catch:{ all -> 0x0057 }
            r4 = r4.mReceivers;	 Catch:{ all -> 0x0057 }
            r4 = r4.values();	 Catch:{ all -> 0x0057 }
            r3 = r4.iterator();	 Catch:{ all -> 0x0057 }
            r1 = r0;
        L_0x0017:
            r4 = r3.hasNext();	 Catch:{ all -> 0x0051 }
            if (r4 == 0) goto L_0x0039;
        L_0x001d:
            r2 = r3.next();	 Catch:{ all -> 0x0051 }
            r2 = (com.android.server.LocationManagerService.Receiver) r2;	 Catch:{ all -> 0x0051 }
            r4 = r2.mIdentity;	 Catch:{ all -> 0x0051 }
            r4 = r4.mPackageName;	 Catch:{ all -> 0x0051 }
            r4 = r4.equals(r7);	 Catch:{ all -> 0x0051 }
            if (r4 == 0) goto L_0x005b;
        L_0x002d:
            if (r1 != 0) goto L_0x0059;
        L_0x002f:
            r0 = new java.util.ArrayList;	 Catch:{ all -> 0x0051 }
            r0.<init>();	 Catch:{ all -> 0x0051 }
        L_0x0034:
            r0.add(r2);	 Catch:{ all -> 0x0057 }
        L_0x0037:
            r1 = r0;
            goto L_0x0017;
        L_0x0039:
            if (r1 == 0) goto L_0x0055;
        L_0x003b:
            r3 = r1.iterator();	 Catch:{ all -> 0x0051 }
        L_0x003f:
            r4 = r3.hasNext();	 Catch:{ all -> 0x0051 }
            if (r4 == 0) goto L_0x0055;
        L_0x0045:
            r2 = r3.next();	 Catch:{ all -> 0x0051 }
            r2 = (com.android.server.LocationManagerService.Receiver) r2;	 Catch:{ all -> 0x0051 }
            r4 = com.android.server.LocationManagerService.this;	 Catch:{ all -> 0x0051 }
            r4.removeUpdatesLocked(r2);	 Catch:{ all -> 0x0051 }
            goto L_0x003f;
        L_0x0051:
            r4 = move-exception;
            r0 = r1;
        L_0x0053:
            monitor-exit(r5);
            throw r4;
        L_0x0055:
            monitor-exit(r5);
            return;
        L_0x0057:
            r4 = move-exception;
            goto L_0x0053;
        L_0x0059:
            r0 = r1;
            goto L_0x0034;
        L_0x005b:
            r0 = r1;
            goto L_0x0037;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.LocationManagerService.1.onPackageDisappeared(java.lang.String, int):void");
        }
    };
    private PassiveProvider mPassiveProvider;
    private PowerManager mPowerManager;
    private final ArrayList<LocationProviderInterface> mProviders = new ArrayList();
    private final HashMap<String, LocationProviderInterface> mProvidersByName = new HashMap();
    private final ArrayList<LocationProviderProxy> mProxyProviders = new ArrayList();
    private final HashMap<String, LocationProviderInterface> mRealProviders = new HashMap();
    private final HashMap<Object, Receiver> mReceivers = new HashMap();
    private final HashMap<String, ArrayList<UpdateRecord>> mRecordsByProvider = new HashMap();
    private final LocationRequestStatistics mRequestStatistics = new LocationRequestStatistics();
    private UserManager mUserManager;

    private static final class Identity {
        final String mPackageName;
        final int mPid;
        final int mUid;

        Identity(int uid, int pid, String packageName) {
            this.mUid = uid;
            this.mPid = pid;
            this.mPackageName = packageName;
        }
    }

    private class LinkedCallback implements DeathRecipient {
        private final IBatchedLocationCallback mCallback;

        public LinkedCallback(IBatchedLocationCallback callback) {
            this.mCallback = callback;
        }

        public IBatchedLocationCallback getUnderlyingListener() {
            return this.mCallback;
        }

        public void binderDied() {
            Log.d("LocationManagerService", "Remote Batching Callback died: " + this.mCallback);
            LocationManagerService.this.stopGnssBatch();
            LocationManagerService.this.removeGnssBatchingCallback();
        }
    }

    private class LocationWorkerHandler extends Handler {
        public LocationWorkerHandler(Looper looper) {
            super(looper, null, true);
        }

        public void handleMessage(Message msg) {
            boolean z = true;
            switch (msg.what) {
                case 1:
                    LocationManagerService locationManagerService = LocationManagerService.this;
                    Location location = (Location) msg.obj;
                    if (msg.arg1 != 1) {
                        z = false;
                    }
                    locationManagerService.handleLocationChanged(location, z);
                    return;
                default:
                    return;
            }
        }
    }

    private final class Receiver implements DeathRecipient, OnFinished {
        final int mAllowedResolutionLevel;
        final boolean mHideFromAppOps;
        final Identity mIdentity;
        final Object mKey;
        final ILocationListener mListener;
        boolean mOpHighPowerMonitoring;
        boolean mOpMonitoring;
        int mPendingBroadcasts;
        final PendingIntent mPendingIntent;
        final HashMap<String, UpdateRecord> mUpdateRecords = new HashMap();
        WakeLock mWakeLock;
        final WorkSource mWorkSource;

        Receiver(ILocationListener listener, PendingIntent intent, int pid, int uid, String packageName, WorkSource workSource, boolean hideFromAppOps) {
            this.mListener = listener;
            this.mPendingIntent = intent;
            if (listener != null) {
                this.mKey = listener.asBinder();
            } else {
                this.mKey = intent;
            }
            this.mAllowedResolutionLevel = LocationManagerService.this.getAllowedResolutionLevel(pid, uid);
            this.mIdentity = new Identity(uid, pid, packageName);
            if (workSource != null && workSource.size() <= 0) {
                workSource = null;
            }
            this.mWorkSource = workSource;
            this.mHideFromAppOps = hideFromAppOps;
            updateMonitoring(true);
            this.mWakeLock = LocationManagerService.this.mPowerManager.newWakeLock(1, "LocationManagerService");
            if (workSource == null) {
                workSource = new WorkSource(this.mIdentity.mUid, this.mIdentity.mPackageName);
            }
            this.mWakeLock.setWorkSource(workSource);
        }

        public boolean equals(Object otherObj) {
            return otherObj instanceof Receiver ? this.mKey.equals(((Receiver) otherObj).mKey) : false;
        }

        public int hashCode() {
            return this.mKey.hashCode();
        }

        public String toString() {
            StringBuilder s = new StringBuilder();
            s.append("Reciever[");
            s.append(Integer.toHexString(System.identityHashCode(this)));
            if (this.mListener != null) {
                s.append(" listener");
            } else {
                s.append(" intent");
            }
            for (String p : this.mUpdateRecords.keySet()) {
                s.append(" ").append(((UpdateRecord) this.mUpdateRecords.get(p)).toString());
            }
            s.append("]");
            return s.toString();
        }

        public void updateMonitoring(boolean allow) {
            if (!this.mHideFromAppOps) {
                boolean requestingLocation = false;
                boolean requestingHighPowerLocation = false;
                if (allow) {
                    for (UpdateRecord updateRecord : this.mUpdateRecords.values()) {
                        if (LocationManagerService.this.isAllowedByCurrentUserSettingsLocked(updateRecord.mProvider)) {
                            requestingLocation = true;
                            LocationProviderInterface locationProvider = (LocationProviderInterface) LocationManagerService.this.mProvidersByName.get(updateRecord.mProvider);
                            ProviderProperties properties = locationProvider != null ? locationProvider.getProperties() : null;
                            if (properties != null && properties.mPowerRequirement == 3 && updateRecord.mRequest.getInterval() < 300000) {
                                requestingHighPowerLocation = true;
                                break;
                            }
                        }
                    }
                }
                this.mOpMonitoring = updateMonitoring(requestingLocation, this.mOpMonitoring, 41);
                boolean wasHighPowerMonitoring = this.mOpHighPowerMonitoring;
                this.mOpHighPowerMonitoring = updateMonitoring(requestingHighPowerLocation, this.mOpHighPowerMonitoring, 42);
                if (this.mOpHighPowerMonitoring != wasHighPowerMonitoring) {
                    LocationManagerService.this.mContext.sendBroadcastAsUser(new Intent("android.location.HIGH_POWER_REQUEST_CHANGE"), UserHandle.ALL);
                }
            }
        }

        private boolean updateMonitoring(boolean allowMonitoring, boolean currentlyMonitoring, int op) {
            boolean z = false;
            if (currentlyMonitoring) {
                if (!(allowMonitoring && LocationManagerService.this.mAppOps.checkOpNoThrow(op, this.mIdentity.mUid, this.mIdentity.mPackageName) == 0)) {
                    LocationManagerService.this.mAppOps.finishOp(op, this.mIdentity.mUid, this.mIdentity.mPackageName);
                    return false;
                }
            } else if (allowMonitoring) {
                if (LocationManagerService.this.mAppOps.startOpNoThrow(op, this.mIdentity.mUid, this.mIdentity.mPackageName) == 0) {
                    z = true;
                }
                return z;
            }
            return currentlyMonitoring;
        }

        public boolean isListener() {
            return this.mListener != null;
        }

        public boolean isPendingIntent() {
            return this.mPendingIntent != null;
        }

        public ILocationListener getListener() {
            if (this.mListener != null) {
                return this.mListener;
            }
            throw new IllegalStateException("Request for non-existent listener");
        }

        public boolean callStatusChangedLocked(String provider, int status, Bundle extras) {
            if (this.mListener != null) {
                try {
                    synchronized (this) {
                        this.mListener.onStatusChanged(provider, status, extras);
                        incrementPendingBroadcastsLocked();
                    }
                } catch (RemoteException e) {
                    return false;
                }
            }
            Intent statusChanged = new Intent();
            statusChanged.putExtras(new Bundle(extras));
            statusChanged.putExtra("status", status);
            try {
                synchronized (this) {
                    this.mPendingIntent.send(LocationManagerService.this.mContext, 0, statusChanged, this, LocationManagerService.this.mLocationHandler, LocationManagerService.this.getResolutionPermission(this.mAllowedResolutionLevel));
                    incrementPendingBroadcastsLocked();
                }
            } catch (CanceledException e2) {
                return false;
            }
            return true;
        }

        public boolean callLocationChangedLocked(Location location) {
            if (this.mListener != null) {
                try {
                    synchronized (this) {
                        this.mListener.onLocationChanged(new Location(location));
                        incrementPendingBroadcastsLocked();
                    }
                } catch (RemoteException e) {
                    return false;
                }
            }
            Intent locationChanged = new Intent();
            locationChanged.putExtra("location", new Location(location));
            try {
                synchronized (this) {
                    this.mPendingIntent.send(LocationManagerService.this.mContext, 0, locationChanged, this, LocationManagerService.this.mLocationHandler, LocationManagerService.this.getResolutionPermission(this.mAllowedResolutionLevel));
                    incrementPendingBroadcastsLocked();
                }
            } catch (CanceledException e2) {
                return false;
            }
            return true;
        }

        public boolean callProviderEnabledLocked(String provider, boolean enabled) {
            updateMonitoring(true);
            if (this.mListener != null) {
                try {
                    synchronized (this) {
                        if (enabled) {
                            this.mListener.onProviderEnabled(provider);
                        } else {
                            this.mListener.onProviderDisabled(provider);
                        }
                        incrementPendingBroadcastsLocked();
                    }
                } catch (RemoteException e) {
                    return false;
                }
            }
            Intent providerIntent = new Intent();
            providerIntent.putExtra("providerEnabled", enabled);
            try {
                synchronized (this) {
                    this.mPendingIntent.send(LocationManagerService.this.mContext, 0, providerIntent, this, LocationManagerService.this.mLocationHandler, LocationManagerService.this.getResolutionPermission(this.mAllowedResolutionLevel));
                    incrementPendingBroadcastsLocked();
                }
            } catch (CanceledException e2) {
                return false;
            }
            return true;
        }

        public void binderDied() {
            if (LocationManagerService.D) {
                Log.d("LocationManagerService", "Location listener died");
            }
            synchronized (LocationManagerService.this.mLock) {
                LocationManagerService.this.removeUpdatesLocked(this);
            }
            synchronized (this) {
                clearPendingBroadcastsLocked();
            }
        }

        public void onSendFinished(PendingIntent pendingIntent, Intent intent, int resultCode, String resultData, Bundle resultExtras) {
            synchronized (this) {
                decrementPendingBroadcastsLocked();
            }
        }

        private void incrementPendingBroadcastsLocked() {
            int i = this.mPendingBroadcasts;
            this.mPendingBroadcasts = i + 1;
            if (i == 0) {
                this.mWakeLock.acquire();
            }
        }

        private void decrementPendingBroadcastsLocked() {
            int i = this.mPendingBroadcasts - 1;
            this.mPendingBroadcasts = i;
            if (i == 0 && this.mWakeLock.isHeld()) {
                this.mWakeLock.release();
            }
        }

        public void clearPendingBroadcastsLocked() {
            if (this.mPendingBroadcasts > 0) {
                this.mPendingBroadcasts = 0;
                if (this.mWakeLock.isHeld()) {
                    this.mWakeLock.release();
                }
            }
        }
    }

    private class UpdateRecord {
        boolean mIsForegroundUid;
        Location mLastFixBroadcast;
        long mLastStatusBroadcast;
        final String mProvider;
        final LocationRequest mRealRequest;
        final Receiver mReceiver;
        LocationRequest mRequest;

        UpdateRecord(String provider, LocationRequest request, Receiver receiver) {
            this.mProvider = provider;
            this.mRealRequest = request;
            this.mRequest = request;
            this.mReceiver = receiver;
            this.mIsForegroundUid = LocationManagerService.isImportanceForeground(LocationManagerService.this.mActivityManager.getPackageImportance(this.mReceiver.mIdentity.mPackageName));
            ArrayList<UpdateRecord> records = (ArrayList) LocationManagerService.this.mRecordsByProvider.get(provider);
            if (records == null) {
                records = new ArrayList();
                LocationManagerService.this.mRecordsByProvider.put(provider, records);
            }
            if (!records.contains(this)) {
                records.add(this);
            }
            LocationManagerService.this.mRequestStatistics.startRequesting(this.mReceiver.mIdentity.mPackageName, provider, request.getInterval());
        }

        void disposeLocked(boolean removeReceiver) {
            LocationManagerService.this.mRequestStatistics.stopRequesting(this.mReceiver.mIdentity.mPackageName, this.mProvider);
            ArrayList<UpdateRecord> globalRecords = (ArrayList) LocationManagerService.this.mRecordsByProvider.get(this.mProvider);
            if (globalRecords != null) {
                globalRecords.remove(this);
            }
            if (removeReceiver) {
                HashMap<String, UpdateRecord> receiverRecords = this.mReceiver.mUpdateRecords;
                if (receiverRecords != null) {
                    receiverRecords.remove(this.mProvider);
                    if (receiverRecords.size() == 0) {
                        LocationManagerService.this.removeUpdatesLocked(this.mReceiver);
                    }
                }
            }
        }

        public String toString() {
            return "UpdateRecord[" + this.mProvider + " " + this.mReceiver.mIdentity.mPackageName + "(" + this.mReceiver.mIdentity.mUid + (this.mIsForegroundUid ? " foreground" : " background") + ")" + " " + this.mRealRequest + "]";
        }
    }

    public LocationManagerService(Context context) {
        this.mContext = context;
        this.mAppOps = (AppOpsManager) context.getSystemService("appops");
        ((PackageManagerInternal) LocalServices.getService(PackageManagerInternal.class)).setLocationPackagesProvider(new PackagesProvider() {
            public String[] getPackages(int userId) {
                return LocationManagerService.this.mContext.getResources().getStringArray(17236010);
            }
        });
        if (D) {
            Log.d("LocationManagerService", "Constructed");
        }
    }

    public void systemRunning() {
        synchronized (this.mLock) {
            if (D) {
                Log.d("LocationManagerService", "systemRunning()");
            }
            this.mPackageManager = this.mContext.getPackageManager();
            this.mPowerManager = (PowerManager) this.mContext.getSystemService("power");
            this.mActivityManager = (ActivityManager) this.mContext.getSystemService("activity");
            this.mLocationHandler = new LocationWorkerHandler(BackgroundThread.get().getLooper());
            this.mLocationFudger = new LocationFudger(this.mContext, this.mLocationHandler);
            this.mBlacklist = new LocationBlacklist(this.mContext, this.mLocationHandler);
            this.mBlacklist.init();
            this.mGeofenceManager = new GeofenceManager(this.mContext, this.mBlacklist);
            this.mAppOps.startWatchingMode(0, null, new OnOpChangedInternalListener() {
                public void onOpChanged(int op, String packageName) {
                    synchronized (LocationManagerService.this.mLock) {
                        for (Receiver receiver : LocationManagerService.this.mReceivers.values()) {
                            receiver.updateMonitoring(true);
                        }
                        LocationManagerService.this.applyAllProviderRequirementsLocked();
                    }
                }
            });
            this.mPackageManager.addOnPermissionsChangeListener(new OnPermissionsChangedListener() {
                public void onPermissionsChanged(int uid) {
                    synchronized (LocationManagerService.this.mLock) {
                        LocationManagerService.this.applyAllProviderRequirementsLocked();
                    }
                }
            });
            this.mActivityManager.addOnUidImportanceListener(new OnUidImportanceListener() {
                public void onUidImportance(final int uid, final int importance) {
                    LocationManagerService.this.mLocationHandler.post(new Runnable() {
                        public void run() {
                            LocationManagerService.this.onUidImportanceChanged(uid, importance);
                        }
                    });
                }
            }, FOREGROUND_IMPORTANCE_CUTOFF);
            this.mUserManager = (UserManager) this.mContext.getSystemService("user");
            updateUserProfiles(this.mCurrentUserId);
            updateBackgroundThrottlingWhitelistLocked();
            loadProvidersLocked();
            updateProvidersLocked();
        }
        this.mContext.getContentResolver().registerContentObserver(Secure.getUriFor("location_providers_allowed"), true, new ContentObserver(this.mLocationHandler) {
            public void onChange(boolean selfChange) {
                synchronized (LocationManagerService.this.mLock) {
                    LocationManagerService.this.updateProvidersLocked();
                }
            }
        }, -1);
        this.mContext.getContentResolver().registerContentObserver(Global.getUriFor("location_background_throttle_interval_ms"), true, new ContentObserver(this.mLocationHandler) {
            public void onChange(boolean selfChange) {
                synchronized (LocationManagerService.this.mLock) {
                    LocationManagerService.this.updateProvidersLocked();
                }
            }
        }, -1);
        this.mContext.getContentResolver().registerContentObserver(Global.getUriFor("location_background_throttle_package_whitelist"), true, new ContentObserver(this.mLocationHandler) {
            public void onChange(boolean selfChange) {
                synchronized (LocationManagerService.this.mLock) {
                    LocationManagerService.this.updateBackgroundThrottlingWhitelistLocked();
                    LocationManagerService.this.updateProvidersLocked();
                }
            }
        }, -1);
        this.mPackageMonitor.register(this.mContext, this.mLocationHandler.getLooper(), true);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.USER_SWITCHED");
        intentFilter.addAction("android.intent.action.MANAGED_PROFILE_ADDED");
        intentFilter.addAction("android.intent.action.MANAGED_PROFILE_REMOVED");
        intentFilter.addAction("android.intent.action.ACTION_SHUTDOWN");
        this.mContext.registerReceiverAsUser(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if ("android.intent.action.USER_SWITCHED".equals(action)) {
                    LocationManagerService.this.switchUser(intent.getIntExtra("android.intent.extra.user_handle", 0));
                } else if ("android.intent.action.MANAGED_PROFILE_ADDED".equals(action) || "android.intent.action.MANAGED_PROFILE_REMOVED".equals(action)) {
                    LocationManagerService.this.updateUserProfiles(LocationManagerService.this.mCurrentUserId);
                } else if ("android.intent.action.ACTION_SHUTDOWN".equals(action)) {
                    if (LocationManagerService.D) {
                        Log.d("LocationManagerService", "Shutdown received with UserId: " + getSendingUserId());
                    }
                    if (getSendingUserId() == -1) {
                        LocationManagerService.this.shutdownComponents();
                    }
                }
            }
        }, UserHandle.ALL, intentFilter, null, this.mLocationHandler);
    }

    private void onUidImportanceChanged(int uid, int importance) {
        boolean foreground = isImportanceForeground(importance);
        HashSet<String> affectedProviders = new HashSet(this.mRecordsByProvider.size());
        synchronized (this.mLock) {
            for (Entry<String, ArrayList<UpdateRecord>> entry : this.mRecordsByProvider.entrySet()) {
                String provider = (String) entry.getKey();
                for (UpdateRecord record : (ArrayList) entry.getValue()) {
                    if (record.mReceiver.mIdentity.mUid == uid && record.mIsForegroundUid != foreground) {
                        if (D) {
                            Log.d("LocationManagerService", "request from uid " + uid + " is now " + (foreground ? "foreground" : "background)"));
                        }
                        record.mIsForegroundUid = foreground;
                        if (!isThrottlingExemptLocked(record.mReceiver.mIdentity)) {
                            affectedProviders.add(provider);
                        }
                    }
                }
            }
            for (String provider2 : affectedProviders) {
                applyRequirementsLocked(provider2);
            }
            for (Entry<IGnssMeasurementsListener, Identity> entry2 : this.mGnssMeasurementsListeners.entrySet()) {
                if (((Identity) entry2.getValue()).mUid == uid) {
                    if (D) {
                        Log.d("LocationManagerService", "gnss measurements listener from uid " + uid + " is now " + (foreground ? "foreground" : "background)"));
                    }
                    if (foreground || isThrottlingExemptLocked((Identity) entry2.getValue())) {
                        this.mGnssMeasurementsProvider.addListener((IGnssMeasurementsListener) entry2.getKey());
                    } else {
                        this.mGnssMeasurementsProvider.removeListener((IGnssMeasurementsListener) entry2.getKey());
                    }
                }
            }
            for (Entry<IGnssNavigationMessageListener, Identity> entry3 : this.mGnssNavigationMessageListeners.entrySet()) {
                if (((Identity) entry3.getValue()).mUid == uid) {
                    if (D) {
                        Log.d("LocationManagerService", "gnss navigation message listener from uid " + uid + " is now " + (foreground ? "foreground" : "background)"));
                    }
                    if (foreground || isThrottlingExemptLocked((Identity) entry3.getValue())) {
                        this.mGnssNavigationMessageProvider.addListener((IGnssNavigationMessageListener) entry3.getKey());
                    } else {
                        this.mGnssNavigationMessageProvider.removeListener((IGnssNavigationMessageListener) entry3.getKey());
                    }
                }
            }
        }
    }

    private static boolean isImportanceForeground(int importance) {
        return importance <= FOREGROUND_IMPORTANCE_CUTOFF;
    }

    private void shutdownComponents() {
        if (D) {
            Log.d("LocationManagerService", "Shutting down components...");
        }
        LocationProviderInterface gpsProvider = (LocationProviderInterface) this.mProvidersByName.get("gps");
        if (gpsProvider != null && gpsProvider.isEnabled()) {
            gpsProvider.disable();
        }
        if (FlpHardwareProvider.isSupported()) {
            FlpHardwareProvider.getInstance(this.mContext).cleanup();
        }
    }

    void updateUserProfiles(int currentUserId) {
        int[] profileIds = this.mUserManager.getProfileIdsWithDisabled(currentUserId);
        synchronized (this.mLock) {
            this.mCurrentUserProfiles = profileIds;
        }
    }

    private boolean isCurrentProfile(int userId) {
        boolean contains;
        synchronized (this.mLock) {
            contains = ArrayUtils.contains(this.mCurrentUserProfiles, userId);
        }
        return contains;
    }

    private void ensureFallbackFusedProviderPresentLocked(ArrayList<String> pkgs) {
        PackageManager pm = this.mContext.getPackageManager();
        String systemPackageName = this.mContext.getPackageName();
        ArrayList<HashSet<Signature>> sigSets = ServiceWatcher.getSignatureSets(this.mContext, pkgs);
        for (ResolveInfo rInfo : pm.queryIntentServicesAsUser(new Intent(FUSED_LOCATION_SERVICE_ACTION), 128, this.mCurrentUserId)) {
            String packageName = rInfo.serviceInfo.packageName;
            try {
                if (!ServiceWatcher.isSignatureMatch(pm.getPackageInfo(packageName, 64).signatures, sigSets)) {
                    Log.w("LocationManagerService", packageName + " resolves service " + FUSED_LOCATION_SERVICE_ACTION + ", but has wrong signature, ignoring");
                } else if (rInfo.serviceInfo.metaData == null) {
                    Log.w("LocationManagerService", "Found fused provider without metadata: " + packageName);
                } else if (rInfo.serviceInfo.metaData.getInt(ServiceWatcher.EXTRA_SERVICE_VERSION, -1) == 0) {
                    if ((rInfo.serviceInfo.applicationInfo.flags & 1) == 0) {
                        if (D) {
                            Log.d("LocationManagerService", "Fallback candidate not in /system: " + packageName);
                        }
                    } else if (pm.checkSignatures(systemPackageName, packageName) != 0) {
                        if (D) {
                            Log.d("LocationManagerService", "Fallback candidate not signed the same as system: " + packageName);
                        }
                    } else if (D) {
                        Log.d("LocationManagerService", "Found fallback provider: " + packageName);
                        return;
                    } else {
                        return;
                    }
                } else if (D) {
                    Log.d("LocationManagerService", "Fallback candidate not version 0: " + packageName);
                }
            } catch (NameNotFoundException e) {
                Log.e("LocationManagerService", "missing package: " + packageName);
            }
        }
        throw new IllegalStateException("Unable to find a fused location provider that is in the system partition with version 0 and signed with the platform certificate. Such a package is needed to provide a default fused location provider in the event that no other fused location provider has been installed or is currently available. For example, coreOnly boot mode when decrypting the data partition. The fallback must also be marked coreApp=\"true\" in the manifest");
    }

    private void loadProvidersLocked() {
        FlpHardwareProvider flpHardwareProvider;
        IFusedGeofenceHardware geofenceHardware;
        PassiveProvider passiveProvider = new PassiveProvider(this);
        addProviderLocked(passiveProvider);
        this.mEnabledProviders.add(passiveProvider.getName());
        this.mPassiveProvider = passiveProvider;
        if (GnssLocationProvider.isSupported()) {
            GnssLocationProvider gnssLocationProvider = new GnssLocationProvider(this.mContext, this, this.mLocationHandler.getLooper());
            this.mGnssSystemInfoProvider = gnssLocationProvider.getGnssSystemInfoProvider();
            this.mGnssBatchingProvider = gnssLocationProvider.getGnssBatchingProvider();
            this.mGnssMetricsProvider = gnssLocationProvider.getGnssMetricsProvider();
            this.mGnssStatusProvider = gnssLocationProvider.getGnssStatusProvider();
            this.mNetInitiatedListener = gnssLocationProvider.getNetInitiatedListener();
            addProviderLocked(gnssLocationProvider);
            this.mRealProviders.put("gps", gnssLocationProvider);
            this.mGnssMeasurementsProvider = gnssLocationProvider.getGnssMeasurementsProvider();
            this.mGnssNavigationMessageProvider = gnssLocationProvider.getGnssNavigationMessageProvider();
            this.mGpsGeofenceProxy = gnssLocationProvider.getGpsGeofenceProxy();
        }
        Resources resources = this.mContext.getResources();
        ArrayList<String> providerPackageNames = new ArrayList();
        String[] pkgs = resources.getStringArray(17236010);
        if (D) {
            Log.d("LocationManagerService", "certificates for location providers pulled from: " + Arrays.toString(pkgs));
        }
        if (pkgs != null) {
            providerPackageNames.addAll(Arrays.asList(pkgs));
        }
        ensureFallbackFusedProviderPresentLocked(providerPackageNames);
        LocationProviderProxy networkProvider = LocationProviderProxy.createAndBind(this.mContext, "network", NETWORK_LOCATION_SERVICE_ACTION, 17956957, 17039680, 17236010, this.mLocationHandler);
        if (networkProvider != null) {
            this.mRealProviders.put("network", networkProvider);
            this.mProxyProviders.add(networkProvider);
            addProviderLocked(networkProvider);
        } else {
            Slog.w("LocationManagerService", "no network location provider found");
        }
        LocationProviderProxy fusedLocationProvider = LocationProviderProxy.createAndBind(this.mContext, "fused", FUSED_LOCATION_SERVICE_ACTION, 17956948, 17039659, 17236010, this.mLocationHandler);
        if (fusedLocationProvider != null) {
            addProviderLocked(fusedLocationProvider);
            this.mProxyProviders.add(fusedLocationProvider);
            this.mEnabledProviders.add(fusedLocationProvider.getName());
            this.mRealProviders.put("fused", fusedLocationProvider);
        } else {
            Slog.e("LocationManagerService", "no fused location provider found", new IllegalStateException("Location service needs a fused location provider"));
        }
        this.mGeocodeProvider = GeocoderProxy.createAndBind(this.mContext, 17956949, 17039660, 17236010, this.mLocationHandler);
        if (this.mGeocodeProvider == null) {
            Slog.e("LocationManagerService", "no geocoder provider found");
        }
        if (FlpHardwareProvider.isSupported()) {
            flpHardwareProvider = FlpHardwareProvider.getInstance(this.mContext);
            if (FusedProxy.createAndBind(this.mContext, this.mLocationHandler, flpHardwareProvider.getLocationHardware(), 17956952, 17039662, 17236010) == null) {
                Slog.d("LocationManagerService", "Unable to bind FusedProxy.");
            }
        } else {
            flpHardwareProvider = null;
            Slog.d("LocationManagerService", "FLP HAL not supported");
        }
        Context context = this.mContext;
        Handler handler = this.mLocationHandler;
        IGpsGeofenceHardware iGpsGeofenceHardware = this.mGpsGeofenceProxy;
        if (flpHardwareProvider != null) {
            geofenceHardware = flpHardwareProvider.getGeofenceHardware();
        } else {
            geofenceHardware = null;
        }
        if (GeofenceProxy.createAndBind(context, 17956950, 17039661, 17236010, handler, iGpsGeofenceHardware, geofenceHardware) == null) {
            Slog.d("LocationManagerService", "Unable to bind FLP Geofence proxy.");
        }
        boolean activityRecognitionHardwareIsSupported = ActivityRecognitionHardware.isSupported();
        ActivityRecognitionHardware activityRecognitionHardware = null;
        if (activityRecognitionHardwareIsSupported) {
            activityRecognitionHardware = ActivityRecognitionHardware.getInstance(this.mContext);
        } else {
            Slog.d("LocationManagerService", "Hardware Activity-Recognition not supported.");
        }
        if (ActivityRecognitionProxy.createAndBind(this.mContext, this.mLocationHandler, activityRecognitionHardwareIsSupported, activityRecognitionHardware, 17956942, 17039624, 17236010) == null) {
            Slog.d("LocationManagerService", "Unable to bind ActivityRecognitionProxy.");
        }
        for (String split : resources.getStringArray(17236032)) {
            String[] fragments = split.split(",");
            String name = fragments[0].trim();
            if (this.mProvidersByName.get(name) != null) {
                throw new IllegalArgumentException("Provider \"" + name + "\" already exists");
            }
            addTestProviderLocked(name, new ProviderProperties(Boolean.parseBoolean(fragments[1]), Boolean.parseBoolean(fragments[2]), Boolean.parseBoolean(fragments[3]), Boolean.parseBoolean(fragments[4]), Boolean.parseBoolean(fragments[5]), Boolean.parseBoolean(fragments[6]), Boolean.parseBoolean(fragments[7]), Integer.parseInt(fragments[8]), Integer.parseInt(fragments[9])));
        }
    }

    private void switchUser(int userId) {
        if (this.mCurrentUserId != userId) {
            this.mBlacklist.switchUser(userId);
            this.mLocationHandler.removeMessages(1);
            synchronized (this.mLock) {
                this.mLastLocation.clear();
                this.mLastLocationCoarseInterval.clear();
                for (LocationProviderInterface p : this.mProviders) {
                    updateProviderListenersLocked(p.getName(), false);
                }
                this.mCurrentUserId = userId;
                updateUserProfiles(userId);
                updateProvidersLocked();
            }
        }
    }

    public void locationCallbackFinished(ILocationListener listener) {
        synchronized (this.mLock) {
            Receiver receiver = (Receiver) this.mReceivers.get(listener.asBinder());
            if (receiver != null) {
                synchronized (receiver) {
                    long identity = Binder.clearCallingIdentity();
                    receiver.decrementPendingBroadcastsLocked();
                    Binder.restoreCallingIdentity(identity);
                }
            }
        }
    }

    public int getGnssYearOfHardware() {
        if (this.mGnssSystemInfoProvider != null) {
            return this.mGnssSystemInfoProvider.getGnssYearOfHardware();
        }
        return 0;
    }

    private boolean hasGnssPermissions(String packageName) {
        int allowedResolutionLevel = getCallerAllowedResolutionLevel();
        checkResolutionLevelIsSufficientForProviderUse(allowedResolutionLevel, "gps");
        int pid = Binder.getCallingPid();
        int uid = Binder.getCallingUid();
        long identity = Binder.clearCallingIdentity();
        try {
            boolean hasLocationAccess = checkLocationAccess(pid, uid, packageName, allowedResolutionLevel);
            return hasLocationAccess;
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    public int getGnssBatchSize(String packageName) {
        this.mContext.enforceCallingPermission("android.permission.LOCATION_HARDWARE", "Location Hardware permission not granted to access hardware batching");
        if (!hasGnssPermissions(packageName) || this.mGnssBatchingProvider == null) {
            return 0;
        }
        return this.mGnssBatchingProvider.getSize();
    }

    public boolean addGnssBatchingCallback(IBatchedLocationCallback callback, String packageName) {
        this.mContext.enforceCallingPermission("android.permission.LOCATION_HARDWARE", "Location Hardware permission not granted to access hardware batching");
        if (!hasGnssPermissions(packageName) || this.mGnssBatchingProvider == null) {
            return false;
        }
        this.mGnssBatchingCallback = callback;
        this.mGnssBatchingDeathCallback = new LinkedCallback(callback);
        try {
            callback.asBinder().linkToDeath(this.mGnssBatchingDeathCallback, 0);
            return true;
        } catch (RemoteException e) {
            Log.e("LocationManagerService", "Remote listener already died.", e);
            return false;
        }
    }

    public void removeGnssBatchingCallback() {
        try {
            this.mGnssBatchingCallback.asBinder().unlinkToDeath(this.mGnssBatchingDeathCallback, 0);
        } catch (NoSuchElementException e) {
            Log.e("LocationManagerService", "Couldn't unlink death callback.", e);
        }
        this.mGnssBatchingCallback = null;
        this.mGnssBatchingDeathCallback = null;
    }

    public boolean startGnssBatch(long periodNanos, boolean wakeOnFifoFull, String packageName) {
        this.mContext.enforceCallingPermission("android.permission.LOCATION_HARDWARE", "Location Hardware permission not granted to access hardware batching");
        if (!hasGnssPermissions(packageName) || this.mGnssBatchingProvider == null) {
            return false;
        }
        if (this.mGnssBatchingInProgress) {
            Log.e("LocationManagerService", "startGnssBatch unexpectedly called w/o stopping prior batch");
            stopGnssBatch();
        }
        this.mGnssBatchingInProgress = true;
        return this.mGnssBatchingProvider.start(periodNanos, wakeOnFifoFull);
    }

    public void flushGnssBatch(String packageName) {
        this.mContext.enforceCallingPermission("android.permission.LOCATION_HARDWARE", "Location Hardware permission not granted to access hardware batching");
        if (hasGnssPermissions(packageName)) {
            if (!this.mGnssBatchingInProgress) {
                Log.w("LocationManagerService", "flushGnssBatch called with no batch in progress");
            }
            if (this.mGnssBatchingProvider != null) {
                this.mGnssBatchingProvider.flush();
            }
            return;
        }
        Log.e("LocationManagerService", "flushGnssBatch called without GNSS permissions");
    }

    public boolean stopGnssBatch() {
        this.mContext.enforceCallingPermission("android.permission.LOCATION_HARDWARE", "Location Hardware permission not granted to access hardware batching");
        if (this.mGnssBatchingProvider == null) {
            return false;
        }
        this.mGnssBatchingInProgress = false;
        return this.mGnssBatchingProvider.stop();
    }

    public void reportLocationBatch(List<Location> locations) {
        checkCallerIsProvider();
        if (!isAllowedByCurrentUserSettingsLocked("gps")) {
            Slog.w("LocationManagerService", "reportLocationBatch() called without user permission, locations blocked");
        } else if (this.mGnssBatchingCallback == null) {
            Slog.e("LocationManagerService", "reportLocationBatch() called without active Callback");
        } else {
            try {
                this.mGnssBatchingCallback.onLocationBatch(locations);
            } catch (RemoteException e) {
                Slog.e("LocationManagerService", "mGnssBatchingCallback.onLocationBatch failed", e);
            }
        }
    }

    private void addProviderLocked(LocationProviderInterface provider) {
        this.mProviders.add(provider);
        this.mProvidersByName.put(provider.getName(), provider);
    }

    private void removeProviderLocked(LocationProviderInterface provider) {
        provider.disable();
        this.mProviders.remove(provider);
        this.mProvidersByName.remove(provider.getName());
    }

    private boolean isAllowedByCurrentUserSettingsLocked(String provider) {
        if (this.mEnabledProviders.contains(provider)) {
            return true;
        }
        if (this.mDisabledProviders.contains(provider)) {
            return false;
        }
        return Secure.isLocationProviderEnabledForUser(this.mContext.getContentResolver(), provider, this.mCurrentUserId);
    }

    private boolean isAllowedByUserSettingsLocked(String provider, int uid) {
        if (isCurrentProfile(UserHandle.getUserId(uid)) || (isUidALocationProvider(uid) ^ 1) == 0) {
            return isAllowedByCurrentUserSettingsLocked(provider);
        }
        return false;
    }

    private String getResolutionPermission(int resolutionLevel) {
        switch (resolutionLevel) {
            case 1:
                return "android.permission.ACCESS_COARSE_LOCATION";
            case 2:
                return "android.permission.ACCESS_FINE_LOCATION";
            default:
                return null;
        }
    }

    private int getAllowedResolutionLevel(int pid, int uid) {
        if (this.mContext.checkPermission("android.permission.ACCESS_FINE_LOCATION", pid, uid) == 0) {
            return 2;
        }
        if (this.mContext.checkPermission("android.permission.ACCESS_COARSE_LOCATION", pid, uid) == 0) {
            return 1;
        }
        return 0;
    }

    private int getCallerAllowedResolutionLevel() {
        return getAllowedResolutionLevel(Binder.getCallingPid(), Binder.getCallingUid());
    }

    private void checkResolutionLevelIsSufficientForGeofenceUse(int allowedResolutionLevel) {
        if (allowedResolutionLevel < 2) {
            throw new SecurityException("Geofence usage requires ACCESS_FINE_LOCATION permission");
        }
    }

    private int getMinimumResolutionLevelForProviderUse(String provider) {
        if ("gps".equals(provider) || "passive".equals(provider)) {
            return 2;
        }
        if ("network".equals(provider) || "fused".equals(provider)) {
            return 1;
        }
        LocationProviderInterface lp = (LocationProviderInterface) this.mMockProviders.get(provider);
        if (lp != null) {
            ProviderProperties properties = lp.getProperties();
            if (properties == null || properties.mRequiresSatellite) {
                return 2;
            }
            if (properties.mRequiresNetwork || properties.mRequiresCell) {
                return 1;
            }
        }
        return 2;
    }

    private void checkResolutionLevelIsSufficientForProviderUse(int allowedResolutionLevel, String providerName) {
        int requiredResolutionLevel = getMinimumResolutionLevelForProviderUse(providerName);
        if (allowedResolutionLevel < requiredResolutionLevel) {
            switch (requiredResolutionLevel) {
                case 1:
                    throw new SecurityException("\"" + providerName + "\" location provider " + "requires ACCESS_COARSE_LOCATION or ACCESS_FINE_LOCATION permission.");
                case 2:
                    throw new SecurityException("\"" + providerName + "\" location provider " + "requires ACCESS_FINE_LOCATION permission.");
                default:
                    throw new SecurityException("Insufficient permission for \"" + providerName + "\" location provider.");
            }
        }
    }

    private void checkDeviceStatsAllowed() {
        this.mContext.enforceCallingOrSelfPermission("android.permission.UPDATE_DEVICE_STATS", null);
    }

    private void checkUpdateAppOpsAllowed() {
        this.mContext.enforceCallingOrSelfPermission("android.permission.UPDATE_APP_OPS_STATS", null);
    }

    public static int resolutionLevelToOp(int allowedResolutionLevel) {
        if (allowedResolutionLevel != 0) {
            return allowedResolutionLevel == 1 ? 0 : 1;
        } else {
            return -1;
        }
    }

    boolean reportLocationAccessNoThrow(int pid, int uid, String packageName, int allowedResolutionLevel) {
        boolean z = false;
        int op = resolutionLevelToOp(allowedResolutionLevel);
        if (op >= 0 && this.mAppOps.noteOpNoThrow(op, uid, packageName) != 0) {
            return false;
        }
        if (getAllowedResolutionLevel(pid, uid) >= allowedResolutionLevel) {
            z = true;
        }
        return z;
    }

    boolean checkLocationAccess(int pid, int uid, String packageName, int allowedResolutionLevel) {
        boolean z = false;
        int op = resolutionLevelToOp(allowedResolutionLevel);
        if (op >= 0 && this.mAppOps.checkOp(op, uid, packageName) != 0) {
            return false;
        }
        if (getAllowedResolutionLevel(pid, uid) >= allowedResolutionLevel) {
            z = true;
        }
        return z;
    }

    public List<String> getAllProviders() {
        ArrayList<String> out;
        synchronized (this.mLock) {
            out = new ArrayList(this.mProviders.size());
            for (LocationProviderInterface provider : this.mProviders) {
                String name = provider.getName();
                if (!"fused".equals(name)) {
                    out.add(name);
                }
            }
        }
        if (D) {
            Log.d("LocationManagerService", "getAllProviders()=" + out);
        }
        return out;
    }

    public List<String> getProviders(Criteria criteria, boolean enabledOnly) {
        int allowedResolutionLevel = getCallerAllowedResolutionLevel();
        int uid = Binder.getCallingUid();
        long identity = Binder.clearCallingIdentity();
        try {
            ArrayList<String> out;
            synchronized (this.mLock) {
                out = new ArrayList(this.mProviders.size());
                for (LocationProviderInterface provider : this.mProviders) {
                    String name = provider.getName();
                    if (!"fused".equals(name) && allowedResolutionLevel >= getMinimumResolutionLevelForProviderUse(name)) {
                        if ((!enabledOnly || (isAllowedByUserSettingsLocked(name, uid) ^ 1) == 0) && (criteria == null || (LocationProvider.propertiesMeetCriteria(name, provider.getProperties(), criteria) ^ 1) == 0)) {
                            out.add(name);
                        }
                    }
                }
            }
            if (D) {
                Log.d("LocationManagerService", "getProviders()=" + out);
            }
            return out;
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    public String getBestProvider(Criteria criteria, boolean enabledOnly) {
        List<String> providers = getProviders(criteria, enabledOnly);
        String result;
        if (providers.isEmpty()) {
            providers = getProviders(null, enabledOnly);
            if (providers.isEmpty()) {
                if (D) {
                    Log.d("LocationManagerService", "getBestProvider(" + criteria + ", " + enabledOnly + ")=" + null);
                }
                return null;
            }
            result = pickBest(providers);
            if (D) {
                Log.d("LocationManagerService", "getBestProvider(" + criteria + ", " + enabledOnly + ")=" + result);
            }
            return result;
        }
        result = pickBest(providers);
        if (D) {
            Log.d("LocationManagerService", "getBestProvider(" + criteria + ", " + enabledOnly + ")=" + result);
        }
        return result;
    }

    private String pickBest(List<String> providers) {
        if (providers.contains("gps")) {
            return "gps";
        }
        if (providers.contains("network")) {
            return "network";
        }
        return (String) providers.get(0);
    }

    public boolean providerMeetsCriteria(String provider, Criteria criteria) {
        LocationProviderInterface p = (LocationProviderInterface) this.mProvidersByName.get(provider);
        if (p == null) {
            throw new IllegalArgumentException("provider=" + provider);
        }
        boolean result = LocationProvider.propertiesMeetCriteria(p.getName(), p.getProperties(), criteria);
        if (D) {
            Log.d("LocationManagerService", "providerMeetsCriteria(" + provider + ", " + criteria + ")=" + result);
        }
        return result;
    }

    private void updateProvidersLocked() {
        boolean changesMade = false;
        for (int i = this.mProviders.size() - 1; i >= 0; i--) {
            LocationProviderInterface p = (LocationProviderInterface) this.mProviders.get(i);
            boolean isEnabled = p.isEnabled();
            String name = p.getName();
            boolean shouldBeEnabled = isAllowedByCurrentUserSettingsLocked(name);
            if (isEnabled && (shouldBeEnabled ^ 1) != 0) {
                updateProviderListenersLocked(name, false);
                this.mLastLocation.clear();
                this.mLastLocationCoarseInterval.clear();
                changesMade = true;
            } else if (!isEnabled && shouldBeEnabled) {
                updateProviderListenersLocked(name, true);
                changesMade = true;
            }
        }
        if (changesMade) {
            this.mContext.sendBroadcastAsUser(new Intent("android.location.PROVIDERS_CHANGED"), UserHandle.ALL);
            this.mContext.sendBroadcastAsUser(new Intent("android.location.MODE_CHANGED"), UserHandle.ALL);
        }
    }

    private void updateProviderListenersLocked(String provider, boolean enabled) {
        int listeners = 0;
        LocationProviderInterface p = (LocationProviderInterface) this.mProvidersByName.get(provider);
        if (p != null) {
            ArrayList arrayList = null;
            ArrayList<UpdateRecord> records = (ArrayList) this.mRecordsByProvider.get(provider);
            if (records != null) {
                for (UpdateRecord record : records) {
                    if (isCurrentProfile(UserHandle.getUserId(record.mReceiver.mIdentity.mUid))) {
                        if (!record.mReceiver.callProviderEnabledLocked(provider, enabled)) {
                            if (arrayList == null) {
                                arrayList = new ArrayList();
                            }
                            arrayList.add(record.mReceiver);
                        }
                        listeners++;
                    }
                }
            }
            if (arrayList != null) {
                for (int i = arrayList.size() - 1; i >= 0; i--) {
                    removeUpdatesLocked((Receiver) arrayList.get(i));
                }
            }
            if (enabled) {
                p.enable();
                if (listeners > 0) {
                    applyRequirementsLocked(provider);
                }
            } else {
                p.disable();
            }
        }
    }

    private void applyRequirementsLocked(String provider) {
        LocationProviderInterface p = (LocationProviderInterface) this.mProvidersByName.get(provider);
        if (p != null) {
            ArrayList<UpdateRecord> records = (ArrayList) this.mRecordsByProvider.get(provider);
            WorkSource worksource = new WorkSource();
            ProviderRequest providerRequest = new ProviderRequest();
            long backgroundThrottleInterval = Global.getLong(this.mContext.getContentResolver(), "location_background_throttle_interval_ms", 1800000);
            if (records != null) {
                LocationRequest locationRequest;
                for (UpdateRecord record : records) {
                    if (isCurrentProfile(UserHandle.getUserId(record.mReceiver.mIdentity.mUid))) {
                        if (checkLocationAccess(record.mReceiver.mIdentity.mPid, record.mReceiver.mIdentity.mUid, record.mReceiver.mIdentity.mPackageName, record.mReceiver.mAllowedResolutionLevel)) {
                            locationRequest = record.mRealRequest;
                            long interval = locationRequest.getInterval();
                            if (!isThrottlingExemptLocked(record.mReceiver.mIdentity)) {
                                if (!record.mIsForegroundUid) {
                                    interval = Math.max(interval, backgroundThrottleInterval);
                                }
                                if (interval != locationRequest.getInterval()) {
                                    LocationRequest locationRequest2 = new LocationRequest(locationRequest);
                                    locationRequest2.setInterval(interval);
                                    locationRequest = locationRequest2;
                                }
                            }
                            record.mRequest = locationRequest;
                            providerRequest.locationRequests.add(locationRequest);
                            if (interval < providerRequest.interval) {
                                providerRequest.reportLocation = true;
                                providerRequest.interval = interval;
                            }
                        }
                    }
                }
                if (providerRequest.reportLocation) {
                    long thresholdInterval = ((providerRequest.interval + 1000) * 3) / 2;
                    for (UpdateRecord record2 : records) {
                        if (isCurrentProfile(UserHandle.getUserId(record2.mReceiver.mIdentity.mUid))) {
                            locationRequest = record2.mRequest;
                            if (providerRequest.locationRequests.contains(locationRequest) && locationRequest.getInterval() <= thresholdInterval) {
                                if (record2.mReceiver.mWorkSource == null || record2.mReceiver.mWorkSource.size() <= 0 || record2.mReceiver.mWorkSource.getName(0) == null) {
                                    worksource.add(record2.mReceiver.mIdentity.mUid, record2.mReceiver.mIdentity.mPackageName);
                                } else {
                                    worksource.add(record2.mReceiver.mWorkSource);
                                }
                            }
                        }
                    }
                }
            }
            if (D) {
                Log.d("LocationManagerService", "provider request: " + provider + " " + providerRequest);
            }
            p.setRequest(providerRequest, worksource);
        }
    }

    public String[] getBackgroundThrottlingWhitelist() {
        String[] strArr;
        synchronized (this.mLock) {
            strArr = (String[]) this.mBackgroundThrottlePackageWhitelist.toArray(new String[this.mBackgroundThrottlePackageWhitelist.size()]);
        }
        return strArr;
    }

    private void updateBackgroundThrottlingWhitelistLocked() {
        String setting = Global.getString(this.mContext.getContentResolver(), "location_background_throttle_package_whitelist");
        if (setting == null) {
            setting = "";
        }
        this.mBackgroundThrottlePackageWhitelist.clear();
        this.mBackgroundThrottlePackageWhitelist.addAll(SystemConfig.getInstance().getAllowUnthrottledLocation());
        this.mBackgroundThrottlePackageWhitelist.addAll(Arrays.asList(setting.split(",")));
    }

    private boolean isThrottlingExemptLocked(Identity identity) {
        if (identity.mUid == 1000 || this.mBackgroundThrottlePackageWhitelist.contains(identity.mPackageName)) {
            return true;
        }
        for (LocationProviderProxy provider : this.mProxyProviders) {
            if (identity.mPackageName.equals(provider.getConnectedPackageName())) {
                return true;
            }
        }
        return false;
    }

    private Receiver getReceiverLocked(ILocationListener listener, int pid, int uid, String packageName, WorkSource workSource, boolean hideFromAppOps) {
        IBinder binder = listener.asBinder();
        Receiver receiver = (Receiver) this.mReceivers.get(binder);
        if (receiver == null) {
            receiver = new Receiver(listener, null, pid, uid, packageName, workSource, hideFromAppOps);
            try {
                receiver.getListener().asBinder().linkToDeath(receiver, 0);
                this.mReceivers.put(binder, receiver);
            } catch (RemoteException e) {
                Slog.e("LocationManagerService", "linkToDeath failed:", e);
                return null;
            }
        }
        return receiver;
    }

    private Receiver getReceiverLocked(PendingIntent intent, int pid, int uid, String packageName, WorkSource workSource, boolean hideFromAppOps) {
        Receiver receiver = (Receiver) this.mReceivers.get(intent);
        if (receiver != null) {
            return receiver;
        }
        receiver = new Receiver(null, intent, pid, uid, packageName, workSource, hideFromAppOps);
        this.mReceivers.put(intent, receiver);
        return receiver;
    }

    private LocationRequest createSanitizedRequest(LocationRequest request, int resolutionLevel) {
        LocationRequest sanitizedRequest = new LocationRequest(request);
        if (resolutionLevel < 2) {
            switch (sanitizedRequest.getQuality()) {
                case 100:
                    sanitizedRequest.setQuality(HdmiCecKeycode.CEC_KEYCODE_RESTORE_VOLUME_FUNCTION);
                    break;
                case 203:
                    sanitizedRequest.setQuality(201);
                    break;
            }
            if (sanitizedRequest.getInterval() < LocationFudger.FASTEST_INTERVAL_MS) {
                sanitizedRequest.setInterval(LocationFudger.FASTEST_INTERVAL_MS);
            }
            if (sanitizedRequest.getFastestInterval() < LocationFudger.FASTEST_INTERVAL_MS) {
                sanitizedRequest.setFastestInterval(LocationFudger.FASTEST_INTERVAL_MS);
            }
        }
        if (sanitizedRequest.getFastestInterval() > sanitizedRequest.getInterval()) {
            request.setFastestInterval(request.getInterval());
        }
        return sanitizedRequest;
    }

    private void checkPackageName(String packageName) {
        if (packageName == null) {
            throw new SecurityException("invalid package name: " + packageName);
        }
        int uid = Binder.getCallingUid();
        String[] packages = this.mPackageManager.getPackagesForUid(uid);
        if (packages == null) {
            throw new SecurityException("invalid UID " + uid);
        }
        int i = 0;
        int length = packages.length;
        while (i < length) {
            if (!packageName.equals(packages[i])) {
                i++;
            } else {
                return;
            }
        }
        throw new SecurityException("invalid package name: " + packageName);
    }

    private void checkPendingIntent(PendingIntent intent) {
        if (intent == null) {
            throw new IllegalArgumentException("invalid pending intent: " + intent);
        }
    }

    private Receiver checkListenerOrIntentLocked(ILocationListener listener, PendingIntent intent, int pid, int uid, String packageName, WorkSource workSource, boolean hideFromAppOps) {
        if (intent == null && listener == null) {
            throw new IllegalArgumentException("need either listener or intent");
        } else if (intent != null && listener != null) {
            throw new IllegalArgumentException("cannot register both listener and intent");
        } else if (intent == null) {
            return getReceiverLocked(listener, pid, uid, packageName, workSource, hideFromAppOps);
        } else {
            checkPendingIntent(intent);
            return getReceiverLocked(intent, pid, uid, packageName, workSource, hideFromAppOps);
        }
    }

    public void requestLocationUpdates(LocationRequest request, ILocationListener listener, PendingIntent intent, String packageName) {
        if (request == null) {
            request = DEFAULT_LOCATION_REQUEST;
        }
        checkPackageName(packageName);
        int allowedResolutionLevel = getCallerAllowedResolutionLevel();
        checkResolutionLevelIsSufficientForProviderUse(allowedResolutionLevel, request.getProvider());
        WorkSource workSource = request.getWorkSource();
        if (workSource != null && workSource.size() > 0) {
            checkDeviceStatsAllowed();
        }
        boolean hideFromAppOps = request.getHideFromAppOps();
        if (hideFromAppOps) {
            checkUpdateAppOpsAllowed();
        }
        LocationRequest sanitizedRequest = createSanitizedRequest(request, allowedResolutionLevel);
        int pid = Binder.getCallingPid();
        int uid = Binder.getCallingUid();
        long identity = Binder.clearCallingIdentity();
        try {
            checkLocationAccess(pid, uid, packageName, allowedResolutionLevel);
            synchronized (this.mLock) {
                requestLocationUpdatesLocked(sanitizedRequest, checkListenerOrIntentLocked(listener, intent, pid, uid, packageName, workSource, hideFromAppOps), pid, uid, packageName);
            }
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    private void requestLocationUpdatesLocked(LocationRequest request, Receiver receiver, int pid, int uid, String packageName) {
        if (request == null) {
            request = DEFAULT_LOCATION_REQUEST;
        }
        String name = request.getProvider();
        if (name == null) {
            throw new IllegalArgumentException("provider name must not be null");
        } else if (((LocationProviderInterface) this.mProvidersByName.get(name)) == null) {
            throw new IllegalArgumentException("provider doesn't exist: " + name);
        } else {
            UpdateRecord record = new UpdateRecord(name, request, receiver);
            if (D) {
                Log.d("LocationManagerService", "request " + Integer.toHexString(System.identityHashCode(receiver)) + " " + name + " " + request + " from " + packageName + "(" + uid + " " + (record.mIsForegroundUid ? "foreground" : "background") + (isThrottlingExemptLocked(receiver.mIdentity) ? " [whitelisted]" : "") + ")");
            }
            UpdateRecord oldRecord = (UpdateRecord) receiver.mUpdateRecords.put(name, record);
            if (oldRecord != null) {
                oldRecord.disposeLocked(false);
            }
            if (isAllowedByUserSettingsLocked(name, uid)) {
                applyRequirementsLocked(name);
            } else {
                receiver.callProviderEnabledLocked(name, false);
            }
            receiver.updateMonitoring(true);
        }
    }

    public void removeUpdates(ILocationListener listener, PendingIntent intent, String packageName) {
        checkPackageName(packageName);
        int pid = Binder.getCallingPid();
        int uid = Binder.getCallingUid();
        synchronized (this.mLock) {
            Receiver receiver = checkListenerOrIntentLocked(listener, intent, pid, uid, packageName, null, false);
            long identity = Binder.clearCallingIdentity();
            try {
                removeUpdatesLocked(receiver);
                Binder.restoreCallingIdentity(identity);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    private void removeUpdatesLocked(Receiver receiver) {
        if (D) {
            Log.i("LocationManagerService", "remove " + Integer.toHexString(System.identityHashCode(receiver)));
        }
        if (this.mReceivers.remove(receiver.mKey) != null && receiver.isListener()) {
            receiver.getListener().asBinder().unlinkToDeath(receiver, 0);
            synchronized (receiver) {
                receiver.clearPendingBroadcastsLocked();
            }
        }
        receiver.updateMonitoring(false);
        HashSet<String> providers = new HashSet();
        HashMap<String, UpdateRecord> oldRecords = receiver.mUpdateRecords;
        if (oldRecords != null) {
            for (UpdateRecord record : oldRecords.values()) {
                record.disposeLocked(false);
            }
            providers.addAll(oldRecords.keySet());
        }
        for (String provider : providers) {
            if (isAllowedByCurrentUserSettingsLocked(provider)) {
                applyRequirementsLocked(provider);
            }
        }
    }

    private void applyAllProviderRequirementsLocked() {
        for (LocationProviderInterface p : this.mProviders) {
            if (isAllowedByCurrentUserSettingsLocked(p.getName())) {
                applyRequirementsLocked(p.getName());
            }
        }
    }

    public Location getLastLocation(LocationRequest request, String packageName) {
        if (D) {
            Log.d("LocationManagerService", "getLastLocation: " + request);
        }
        if (request == null) {
            request = DEFAULT_LOCATION_REQUEST;
        }
        int allowedResolutionLevel = getCallerAllowedResolutionLevel();
        checkPackageName(packageName);
        checkResolutionLevelIsSufficientForProviderUse(allowedResolutionLevel, request.getProvider());
        int pid = Binder.getCallingPid();
        int uid = Binder.getCallingUid();
        long identity = Binder.clearCallingIdentity();
        try {
            if (this.mBlacklist.isBlacklisted(packageName)) {
                if (D) {
                    Log.d("LocationManagerService", "not returning last loc for blacklisted app: " + packageName);
                }
                Binder.restoreCallingIdentity(identity);
                return null;
            } else if (reportLocationAccessNoThrow(pid, uid, packageName, allowedResolutionLevel)) {
                synchronized (this.mLock) {
                    String name = request.getProvider();
                    if (name == null) {
                        name = "fused";
                    }
                    if (((LocationProviderInterface) this.mProvidersByName.get(name)) == null) {
                        Binder.restoreCallingIdentity(identity);
                        return null;
                    } else if (isAllowedByUserSettingsLocked(name, uid)) {
                        Location location;
                        if (allowedResolutionLevel < 2) {
                            location = (Location) this.mLastLocationCoarseInterval.get(name);
                        } else {
                            location = (Location) this.mLastLocation.get(name);
                        }
                        if (location == null) {
                            Binder.restoreCallingIdentity(identity);
                            return null;
                        } else if (allowedResolutionLevel < 2) {
                            Location noGPSLocation = location.getExtraLocation("noGPSLocation");
                            if (noGPSLocation != null) {
                                r9 = new Location(this.mLocationFudger.getOrCreate(noGPSLocation));
                                Binder.restoreCallingIdentity(identity);
                                return r9;
                            }
                            Binder.restoreCallingIdentity(identity);
                            return null;
                        } else {
                            r9 = new Location(location);
                            Binder.restoreCallingIdentity(identity);
                            return r9;
                        }
                    } else {
                        Binder.restoreCallingIdentity(identity);
                        return null;
                    }
                }
            } else {
                if (D) {
                    Log.d("LocationManagerService", "not returning last loc for no op app: " + packageName);
                }
                Binder.restoreCallingIdentity(identity);
                return null;
            }
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(identity);
        }
    }

    public void requestGeofence(LocationRequest request, Geofence geofence, PendingIntent intent, String packageName) {
        if (request == null) {
            request = DEFAULT_LOCATION_REQUEST;
        }
        int allowedResolutionLevel = getCallerAllowedResolutionLevel();
        checkResolutionLevelIsSufficientForGeofenceUse(allowedResolutionLevel);
        checkPendingIntent(intent);
        checkPackageName(packageName);
        checkResolutionLevelIsSufficientForProviderUse(allowedResolutionLevel, request.getProvider());
        LocationRequest sanitizedRequest = createSanitizedRequest(request, allowedResolutionLevel);
        if (D) {
            Log.d("LocationManagerService", "requestGeofence: " + sanitizedRequest + " " + geofence + " " + intent);
        }
        int uid = Binder.getCallingUid();
        if (UserHandle.getUserId(uid) != 0) {
            Log.w("LocationManagerService", "proximity alerts are currently available only to the primary user");
            return;
        }
        long identity = Binder.clearCallingIdentity();
        try {
            this.mGeofenceManager.addFence(sanitizedRequest, geofence, intent, allowedResolutionLevel, uid, packageName);
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    public void removeGeofence(Geofence geofence, PendingIntent intent, String packageName) {
        checkPendingIntent(intent);
        checkPackageName(packageName);
        if (D) {
            Log.d("LocationManagerService", "removeGeofence: " + geofence + " " + intent);
        }
        long identity = Binder.clearCallingIdentity();
        try {
            this.mGeofenceManager.removeFence(geofence, intent);
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    public boolean registerGnssStatusCallback(IGnssStatusListener callback, String packageName) {
        if (!hasGnssPermissions(packageName) || this.mGnssStatusProvider == null) {
            return false;
        }
        try {
            this.mGnssStatusProvider.registerGnssStatusCallback(callback);
            return true;
        } catch (RemoteException e) {
            Slog.e("LocationManagerService", "mGpsStatusProvider.registerGnssStatusCallback failed", e);
            return false;
        }
    }

    public void unregisterGnssStatusCallback(IGnssStatusListener callback) {
        synchronized (this.mLock) {
            try {
                this.mGnssStatusProvider.unregisterGnssStatusCallback(callback);
            } catch (Exception e) {
                Slog.e("LocationManagerService", "mGpsStatusProvider.unregisterGnssStatusCallback failed", e);
            }
        }
    }

    public boolean addGnssMeasurementsListener(IGnssMeasurementsListener listener, String packageName) {
        if (!hasGnssPermissions(packageName) || this.mGnssMeasurementsProvider == null) {
            return false;
        }
        synchronized (this.mLock) {
            Identity callerIdentity = new Identity(Binder.getCallingUid(), Binder.getCallingPid(), packageName);
            this.mGnssMeasurementsListeners.put(listener, callerIdentity);
            long identity = Binder.clearCallingIdentity();
            try {
                if (isThrottlingExemptLocked(callerIdentity) || isImportanceForeground(this.mActivityManager.getPackageImportance(packageName))) {
                    boolean addListener = this.mGnssMeasurementsProvider.addListener(listener);
                    Binder.restoreCallingIdentity(identity);
                    return addListener;
                }
                Binder.restoreCallingIdentity(identity);
                return true;
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    public void removeGnssMeasurementsListener(IGnssMeasurementsListener listener) {
        if (this.mGnssMeasurementsProvider != null) {
            synchronized (this.mLock) {
                this.mGnssMeasurementsListeners.remove(listener);
                this.mGnssMeasurementsProvider.removeListener(listener);
            }
        }
    }

    public boolean addGnssNavigationMessageListener(IGnssNavigationMessageListener listener, String packageName) {
        if (!hasGnssPermissions(packageName) || this.mGnssNavigationMessageProvider == null) {
            return false;
        }
        synchronized (this.mLock) {
            Identity callerIdentity = new Identity(Binder.getCallingUid(), Binder.getCallingPid(), packageName);
            this.mGnssNavigationMessageListeners.put(listener, callerIdentity);
            long identity = Binder.clearCallingIdentity();
            try {
                if (isThrottlingExemptLocked(callerIdentity) || isImportanceForeground(this.mActivityManager.getPackageImportance(packageName))) {
                    boolean addListener = this.mGnssNavigationMessageProvider.addListener(listener);
                    Binder.restoreCallingIdentity(identity);
                    return addListener;
                }
                Binder.restoreCallingIdentity(identity);
                return true;
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    public void removeGnssNavigationMessageListener(IGnssNavigationMessageListener listener) {
        if (this.mGnssNavigationMessageProvider != null) {
            synchronized (this.mLock) {
                this.mGnssNavigationMessageListeners.remove(listener);
                this.mGnssNavigationMessageProvider.removeListener(listener);
            }
        }
    }

    public boolean sendExtraCommand(String provider, String command, Bundle extras) {
        if (provider == null) {
            throw new NullPointerException();
        }
        checkResolutionLevelIsSufficientForProviderUse(getCallerAllowedResolutionLevel(), provider);
        if (this.mContext.checkCallingOrSelfPermission(ACCESS_LOCATION_EXTRA_COMMANDS) != 0) {
            throw new SecurityException("Requires ACCESS_LOCATION_EXTRA_COMMANDS permission");
        }
        synchronized (this.mLock) {
            LocationProviderInterface p = (LocationProviderInterface) this.mProvidersByName.get(provider);
            if (p == null) {
                return false;
            }
            boolean sendExtraCommand = p.sendExtraCommand(command, extras);
            return sendExtraCommand;
        }
    }

    public boolean sendNiResponse(int notifId, int userResponse) {
        if (Binder.getCallingUid() != Process.myUid()) {
            throw new SecurityException("calling sendNiResponse from outside of the system is not allowed");
        }
        try {
            return this.mNetInitiatedListener.sendNiResponse(notifId, userResponse);
        } catch (RemoteException e) {
            Slog.e("LocationManagerService", "RemoteException in LocationManagerService.sendNiResponse");
            return false;
        }
    }

    public ProviderProperties getProviderProperties(String provider) {
        if (this.mProvidersByName.get(provider) == null) {
            return null;
        }
        checkResolutionLevelIsSufficientForProviderUse(getCallerAllowedResolutionLevel(), provider);
        synchronized (this.mLock) {
            LocationProviderInterface p = (LocationProviderInterface) this.mProvidersByName.get(provider);
        }
        if (p == null) {
            return null;
        }
        return p.getProperties();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getNetworkProviderPackage() {
        /*
        r5 = this;
        r4 = 0;
        r2 = r5.mLock;
        monitor-enter(r2);
        r1 = r5.mProvidersByName;	 Catch:{ all -> 0x0028 }
        r3 = "network";
        r1 = r1.get(r3);	 Catch:{ all -> 0x0028 }
        if (r1 != 0) goto L_0x0011;
    L_0x000f:
        monitor-exit(r2);
        return r4;
    L_0x0011:
        r1 = r5.mProvidersByName;	 Catch:{ all -> 0x0028 }
        r3 = "network";
        r0 = r1.get(r3);	 Catch:{ all -> 0x0028 }
        r0 = (com.android.server.location.LocationProviderInterface) r0;	 Catch:{ all -> 0x0028 }
        monitor-exit(r2);
        r1 = r0 instanceof com.android.server.location.LocationProviderProxy;
        if (r1 == 0) goto L_0x002b;
    L_0x0021:
        r0 = (com.android.server.location.LocationProviderProxy) r0;
        r1 = r0.getConnectedPackageName();
        return r1;
    L_0x0028:
        r1 = move-exception;
        monitor-exit(r2);
        throw r1;
    L_0x002b:
        return r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.LocationManagerService.getNetworkProviderPackage():java.lang.String");
    }

    public boolean isProviderEnabled(String provider) {
        boolean z = false;
        if ("fused".equals(provider)) {
            return false;
        }
        int uid = Binder.getCallingUid();
        long identity = Binder.clearCallingIdentity();
        try {
            synchronized (this.mLock) {
                if (((LocationProviderInterface) this.mProvidersByName.get(provider)) != null) {
                    z = isAllowedByUserSettingsLocked(provider, uid);
                }
            }
            return z;
        } finally {
            Binder.restoreCallingIdentity(identity);
        }
    }

    private boolean isUidALocationProvider(int uid) {
        if (uid == 1000) {
            return true;
        }
        if (this.mGeocodeProvider != null && doesUidHavePackage(uid, this.mGeocodeProvider.getConnectedPackageName())) {
            return true;
        }
        for (LocationProviderProxy proxy : this.mProxyProviders) {
            if (doesUidHavePackage(uid, proxy.getConnectedPackageName())) {
                return true;
            }
        }
        return false;
    }

    private void checkCallerIsProvider() {
        if (this.mContext.checkCallingOrSelfPermission(INSTALL_LOCATION_PROVIDER) != 0 && !isUidALocationProvider(Binder.getCallingUid())) {
            throw new SecurityException("need INSTALL_LOCATION_PROVIDER permission, or UID of a currently bound location provider");
        }
    }

    private boolean doesUidHavePackage(int uid, String packageName) {
        if (packageName == null) {
            return false;
        }
        String[] packageNames = this.mPackageManager.getPackagesForUid(uid);
        if (packageNames == null) {
            return false;
        }
        for (String name : packageNames) {
            if (packageName.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public void reportLocation(Location location, boolean passive) {
        int i = 1;
        checkCallerIsProvider();
        if (location.isComplete()) {
            this.mLocationHandler.removeMessages(1, location);
            Message m = Message.obtain(this.mLocationHandler, 1, location);
            if (!passive) {
                i = 0;
            }
            m.arg1 = i;
            this.mLocationHandler.sendMessageAtFrontOfQueue(m);
            return;
        }
        Log.w("LocationManagerService", "Dropping incomplete location: " + location);
    }

    private static boolean shouldBroadcastSafe(Location loc, Location lastLoc, UpdateRecord record, long now) {
        if (lastLoc == null) {
            return true;
        }
        if ((loc.getElapsedRealtimeNanos() - lastLoc.getElapsedRealtimeNanos()) / NANOS_PER_MILLI < record.mRealRequest.getFastestInterval() - 100) {
            return false;
        }
        double minDistance = (double) record.mRealRequest.getSmallestDisplacement();
        if (minDistance > 0.0d && ((double) loc.distanceTo(lastLoc)) <= minDistance) {
            return false;
        }
        if (record.mRealRequest.getNumUpdates() <= 0) {
            return false;
        }
        return record.mRealRequest.getExpireAt() >= now;
    }

    private void handleLocationChangedLocked(Location location, boolean passive) {
        if (D) {
            Log.d("LocationManagerService", "incoming location: " + location);
        }
        long now = SystemClock.elapsedRealtime();
        String provider = passive ? "passive" : location.getProvider();
        LocationProviderInterface p = (LocationProviderInterface) this.mProvidersByName.get(provider);
        if (p != null) {
            Location noGPSLocation = location.getExtraLocation("noGPSLocation");
            Location lastLocation = (Location) this.mLastLocation.get(provider);
            if (lastLocation == null) {
                lastLocation = new Location(provider);
                this.mLastLocation.put(provider, lastLocation);
            } else {
                Location lastNoGPSLocation = lastLocation.getExtraLocation("noGPSLocation");
                if (noGPSLocation == null && lastNoGPSLocation != null) {
                    location.setExtraLocation("noGPSLocation", lastNoGPSLocation);
                }
            }
            lastLocation.set(location);
            Location lastLocationCoarseInterval = (Location) this.mLastLocationCoarseInterval.get(provider);
            if (lastLocationCoarseInterval == null) {
                lastLocationCoarseInterval = new Location(location);
                this.mLastLocationCoarseInterval.put(provider, lastLocationCoarseInterval);
            }
            if (location.getElapsedRealtimeNanos() - lastLocationCoarseInterval.getElapsedRealtimeNanos() > 600000000000L) {
                lastLocationCoarseInterval.set(location);
            }
            noGPSLocation = lastLocationCoarseInterval.getExtraLocation("noGPSLocation");
            ArrayList<UpdateRecord> records = (ArrayList) this.mRecordsByProvider.get(provider);
            if (records != null && records.size() != 0) {
                Receiver receiver;
                Location coarseLocation = null;
                if (noGPSLocation != null) {
                    coarseLocation = this.mLocationFudger.getOrCreate(noGPSLocation);
                }
                long newStatusUpdateTime = p.getStatusUpdateTime();
                Bundle extras = new Bundle();
                int status = p.getStatus(extras);
                Iterable deadReceivers = null;
                Iterable deadUpdateRecords = null;
                for (UpdateRecord r : records) {
                    receiver = r.mReceiver;
                    boolean receiverDead = false;
                    int receiverUserId = UserHandle.getUserId(receiver.mIdentity.mUid);
                    if (!isCurrentProfile(receiverUserId)) {
                        if ((isUidALocationProvider(receiver.mIdentity.mUid) ^ 1) != 0) {
                            if (D) {
                                Log.d("LocationManagerService", "skipping loc update for background user " + receiverUserId + " (current user: " + this.mCurrentUserId + ", app: " + receiver.mIdentity.mPackageName + ")");
                            }
                        }
                    }
                    if (!this.mBlacklist.isBlacklisted(receiver.mIdentity.mPackageName)) {
                        if (reportLocationAccessNoThrow(receiver.mIdentity.mPid, receiver.mIdentity.mUid, receiver.mIdentity.mPackageName, receiver.mAllowedResolutionLevel)) {
                            Location notifyLocation;
                            if (receiver.mAllowedResolutionLevel < 2) {
                                notifyLocation = coarseLocation;
                            } else {
                                notifyLocation = lastLocation;
                            }
                            if (notifyLocation != null) {
                                Location lastLoc = r.mLastFixBroadcast;
                                if (lastLoc == null || shouldBroadcastSafe(notifyLocation, lastLoc, r, now)) {
                                    if (lastLoc == null) {
                                        r.mLastFixBroadcast = new Location(notifyLocation);
                                    } else {
                                        lastLoc.set(notifyLocation);
                                    }
                                    if (!receiver.callLocationChangedLocked(notifyLocation)) {
                                        Slog.w("LocationManagerService", "RemoteException calling onLocationChanged on " + receiver);
                                        receiverDead = true;
                                    }
                                    r.mRealRequest.decrementNumUpdates();
                                }
                            }
                            long prevStatusUpdateTime = r.mLastStatusBroadcast;
                            if (newStatusUpdateTime > prevStatusUpdateTime && !(prevStatusUpdateTime == 0 && status == 2)) {
                                r.mLastStatusBroadcast = newStatusUpdateTime;
                                if (!receiver.callStatusChangedLocked(provider, status, extras)) {
                                    receiverDead = true;
                                    Slog.w("LocationManagerService", "RemoteException calling onStatusChanged on " + receiver);
                                }
                            }
                            if (r.mRealRequest.getNumUpdates() <= 0 || r.mRealRequest.getExpireAt() < now) {
                                if (r8 == null) {
                                    deadUpdateRecords = new ArrayList();
                                }
                                deadUpdateRecords.add(r);
                            }
                            if (receiverDead) {
                                if (r7 == null) {
                                    deadReceivers = new ArrayList();
                                }
                                if (!deadReceivers.contains(receiver)) {
                                    deadReceivers.add(receiver);
                                }
                            }
                        } else if (D) {
                            Log.d("LocationManagerService", "skipping loc update for no op app: " + receiver.mIdentity.mPackageName);
                        }
                    } else if (D) {
                        Log.d("LocationManagerService", "skipping loc update for blacklisted app: " + receiver.mIdentity.mPackageName);
                    }
                }
                if (r7 != null) {
                    for (Receiver receiver2 : r7) {
                        removeUpdatesLocked(receiver2);
                    }
                }
                if (r8 != null) {
                    for (UpdateRecord r2 : r8) {
                        r2.disposeLocked(true);
                    }
                    applyRequirementsLocked(provider);
                }
            }
        }
    }

    private boolean isMockProvider(String provider) {
        boolean containsKey;
        synchronized (this.mLock) {
            containsKey = this.mMockProviders.containsKey(provider);
        }
        return containsKey;
    }

    private void handleLocationChanged(Location location, boolean passive) {
        Location myLocation = new Location(location);
        String provider = myLocation.getProvider();
        if (!myLocation.isFromMockProvider() && isMockProvider(provider)) {
            myLocation.setIsFromMockProvider(true);
        }
        synchronized (this.mLock) {
            if (isAllowedByCurrentUserSettingsLocked(provider)) {
                if (!passive) {
                    this.mPassiveProvider.updateLocation(myLocation);
                }
                handleLocationChangedLocked(myLocation, passive);
            }
        }
    }

    public boolean geocoderIsPresent() {
        return this.mGeocodeProvider != null;
    }

    public String getFromLocation(double latitude, double longitude, int maxResults, GeocoderParams params, List<Address> addrs) {
        if (this.mGeocodeProvider != null) {
            return this.mGeocodeProvider.getFromLocation(latitude, longitude, maxResults, params, addrs);
        }
        return null;
    }

    public String getFromLocationName(String locationName, double lowerLeftLatitude, double lowerLeftLongitude, double upperRightLatitude, double upperRightLongitude, int maxResults, GeocoderParams params, List<Address> addrs) {
        if (this.mGeocodeProvider != null) {
            return this.mGeocodeProvider.getFromLocationName(locationName, lowerLeftLatitude, lowerLeftLongitude, upperRightLatitude, upperRightLongitude, maxResults, params, addrs);
        }
        return null;
    }

    private boolean canCallerAccessMockLocation(String opPackageName) {
        return this.mAppOps.noteOp(58, Binder.getCallingUid(), opPackageName) == 0;
    }

    public void addTestProvider(String name, ProviderProperties properties, String opPackageName) {
        if (!canCallerAccessMockLocation(opPackageName)) {
            return;
        }
        if ("passive".equals(name)) {
            throw new IllegalArgumentException("Cannot mock the passive location provider");
        }
        long identity = Binder.clearCallingIdentity();
        synchronized (this.mLock) {
            if ("gps".equals(name) || "network".equals(name) || "fused".equals(name)) {
                LocationProviderInterface p = (LocationProviderInterface) this.mProvidersByName.get(name);
                if (p != null) {
                    removeProviderLocked(p);
                }
            }
            addTestProviderLocked(name, properties);
            updateProvidersLocked();
        }
        Binder.restoreCallingIdentity(identity);
    }

    private void addTestProviderLocked(String name, ProviderProperties properties) {
        if (this.mProvidersByName.get(name) != null) {
            throw new IllegalArgumentException("Provider \"" + name + "\" already exists");
        }
        MockProvider provider = new MockProvider(name, this, properties);
        addProviderLocked(provider);
        this.mMockProviders.put(name, provider);
        this.mLastLocation.put(name, null);
        this.mLastLocationCoarseInterval.put(name, null);
    }

    public void removeTestProvider(String provider, String opPackageName) {
        if (canCallerAccessMockLocation(opPackageName)) {
            synchronized (this.mLock) {
                clearTestProviderEnabled(provider, opPackageName);
                clearTestProviderLocation(provider, opPackageName);
                clearTestProviderStatus(provider, opPackageName);
                if (((MockProvider) this.mMockProviders.remove(provider)) == null) {
                    throw new IllegalArgumentException("Provider \"" + provider + "\" unknown");
                }
                long identity = Binder.clearCallingIdentity();
                removeProviderLocked((LocationProviderInterface) this.mProvidersByName.get(provider));
                LocationProviderInterface realProvider = (LocationProviderInterface) this.mRealProviders.get(provider);
                if (realProvider != null) {
                    addProviderLocked(realProvider);
                }
                this.mLastLocation.put(provider, null);
                this.mLastLocationCoarseInterval.put(provider, null);
                updateProvidersLocked();
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    public void setTestProviderLocation(String provider, Location loc, String opPackageName) {
        if (canCallerAccessMockLocation(opPackageName)) {
            synchronized (this.mLock) {
                MockProvider mockProvider = (MockProvider) this.mMockProviders.get(provider);
                if (mockProvider == null) {
                    throw new IllegalArgumentException("Provider \"" + provider + "\" unknown");
                }
                Location mock = new Location(loc);
                mock.setIsFromMockProvider(true);
                if (!(TextUtils.isEmpty(loc.getProvider()) || (provider.equals(loc.getProvider()) ^ 1) == 0)) {
                    EventLog.writeEvent(1397638484, new Object[]{"33091107", Integer.valueOf(Binder.getCallingUid()), provider + "!=" + loc.getProvider()});
                }
                long identity = Binder.clearCallingIdentity();
                mockProvider.setLocation(mock);
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    public void clearTestProviderLocation(String provider, String opPackageName) {
        if (canCallerAccessMockLocation(opPackageName)) {
            synchronized (this.mLock) {
                MockProvider mockProvider = (MockProvider) this.mMockProviders.get(provider);
                if (mockProvider == null) {
                    throw new IllegalArgumentException("Provider \"" + provider + "\" unknown");
                }
                mockProvider.clearLocation();
            }
        }
    }

    public void setTestProviderEnabled(String provider, boolean enabled, String opPackageName) {
        if (canCallerAccessMockLocation(opPackageName)) {
            synchronized (this.mLock) {
                MockProvider mockProvider = (MockProvider) this.mMockProviders.get(provider);
                if (mockProvider == null) {
                    throw new IllegalArgumentException("Provider \"" + provider + "\" unknown");
                }
                long identity = Binder.clearCallingIdentity();
                if (enabled) {
                    mockProvider.enable();
                    this.mEnabledProviders.add(provider);
                    this.mDisabledProviders.remove(provider);
                } else {
                    mockProvider.disable();
                    this.mEnabledProviders.remove(provider);
                    this.mDisabledProviders.add(provider);
                }
                updateProvidersLocked();
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    public void clearTestProviderEnabled(String provider, String opPackageName) {
        if (canCallerAccessMockLocation(opPackageName)) {
            synchronized (this.mLock) {
                if (((MockProvider) this.mMockProviders.get(provider)) == null) {
                    throw new IllegalArgumentException("Provider \"" + provider + "\" unknown");
                }
                long identity = Binder.clearCallingIdentity();
                this.mEnabledProviders.remove(provider);
                this.mDisabledProviders.remove(provider);
                updateProvidersLocked();
                Binder.restoreCallingIdentity(identity);
            }
        }
    }

    public void setTestProviderStatus(String provider, int status, Bundle extras, long updateTime, String opPackageName) {
        if (canCallerAccessMockLocation(opPackageName)) {
            synchronized (this.mLock) {
                MockProvider mockProvider = (MockProvider) this.mMockProviders.get(provider);
                if (mockProvider == null) {
                    throw new IllegalArgumentException("Provider \"" + provider + "\" unknown");
                }
                mockProvider.setStatus(status, extras, updateTime);
            }
        }
    }

    public void clearTestProviderStatus(String provider, String opPackageName) {
        if (canCallerAccessMockLocation(opPackageName)) {
            synchronized (this.mLock) {
                MockProvider mockProvider = (MockProvider) this.mMockProviders.get(provider);
                if (mockProvider == null) {
                    throw new IllegalArgumentException("Provider \"" + provider + "\" unknown");
                }
                mockProvider.clearStatus();
            }
        }
    }

    private void log(String log) {
        if (Log.isLoggable("LocationManagerService", 2)) {
            Slog.d("LocationManagerService", log);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void dump(java.io.FileDescriptor r28, java.io.PrintWriter r29, java.lang.String[] r30) {
        /*
        r27 = this;
        r0 = r27;
        r0 = r0.mContext;
        r24 = r0;
        r25 = "LocationManagerService";
        r0 = r24;
        r1 = r25;
        r2 = r29;
        r24 = com.android.internal.util.DumpUtils.checkDumpPermission(r0, r1, r2);
        if (r24 != 0) goto L_0x0016;
    L_0x0015:
        return;
    L_0x0016:
        r0 = r27;
        r0 = r0.mLock;
        r25 = r0;
        monitor-enter(r25);
        r0 = r30;
        r0 = r0.length;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        if (r24 <= 0) goto L_0x0050;
    L_0x0024:
        r24 = 0;
        r24 = r30[r24];	 Catch:{ all -> 0x00a2 }
        r26 = "--gnssmetrics";
        r0 = r24;
        r1 = r26;
        r24 = r0.equals(r1);	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x0050;
    L_0x0035:
        r0 = r27;
        r0 = r0.mGnssMetricsProvider;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        if (r24 == 0) goto L_0x004e;
    L_0x003d:
        r0 = r27;
        r0 = r0.mGnssMetricsProvider;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.getGnssMetricsAsProtoString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.append(r1);	 Catch:{ all -> 0x00a2 }
    L_0x004e:
        monitor-exit(r25);
        return;
    L_0x0050:
        r24 = "Current Location Manager state:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r24 = "  Location Listeners:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mReceivers;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.values();	 Catch:{ all -> 0x00a2 }
        r20 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x0072:
        r24 = r20.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x00a5;
    L_0x0078:
        r19 = r20.next();	 Catch:{ all -> 0x00a2 }
        r19 = (com.android.server.LocationManagerService.Receiver) r19;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r19;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x0072;
    L_0x00a2:
        r24 = move-exception;
        monitor-exit(r25);
        throw r24;
    L_0x00a5:
        r24 = "  Active Records by Provider:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mRecordsByProvider;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.entrySet();	 Catch:{ all -> 0x00a2 }
        r7 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x00bd:
        r24 = r7.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x0137;
    L_0x00c3:
        r6 = r7.next();	 Catch:{ all -> 0x00a2 }
        r6 = (java.util.Map.Entry) r6;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r26 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r6.getKey();	 Catch:{ all -> 0x00a2 }
        r24 = (java.lang.String) r24;	 Catch:{ all -> 0x00a2 }
        r0 = r26;
        r1 = r24;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = ":";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r6.getValue();	 Catch:{ all -> 0x00a2 }
        r24 = (java.util.ArrayList) r24;	 Catch:{ all -> 0x00a2 }
        r22 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x0107:
        r24 = r22.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x00bd;
    L_0x010d:
        r21 = r22.next();	 Catch:{ all -> 0x00a2 }
        r21 = (com.android.server.LocationManagerService.UpdateRecord) r21;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "      ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r21;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x0107;
    L_0x0137:
        r24 = "  Overlay Provider Packages:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mProviders;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r17 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x014b:
        r24 = r17.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x019e;
    L_0x0151:
        r15 = r17.next();	 Catch:{ all -> 0x00a2 }
        r15 = (com.android.server.location.LocationProviderInterface) r15;	 Catch:{ all -> 0x00a2 }
        r0 = r15 instanceof com.android.server.location.LocationProviderProxy;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        if (r24 == 0) goto L_0x014b;
    L_0x015d:
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = r15.getName();	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = ": ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r15 = (com.android.server.location.LocationProviderProxy) r15;	 Catch:{ all -> 0x00a2 }
        r26 = r15.getConnectedPackageName();	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x014b;
    L_0x019e:
        r24 = "  Historical Records by Provider:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mRequestStatistics;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r0 = r24;
        r0 = r0.statistics;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.entrySet();	 Catch:{ all -> 0x00a2 }
        r7 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x01bc:
        r24 = r7.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x0226;
    L_0x01c2:
        r4 = r7.next();	 Catch:{ all -> 0x00a2 }
        r4 = (java.util.Map.Entry) r4;	 Catch:{ all -> 0x00a2 }
        r11 = r4.getKey();	 Catch:{ all -> 0x00a2 }
        r11 = (com.android.server.location.LocationRequestStatistics.PackageProviderKey) r11;	 Catch:{ all -> 0x00a2 }
        r23 = r4.getValue();	 Catch:{ all -> 0x00a2 }
        r23 = (com.android.server.location.LocationRequestStatistics.PackageStatistics) r23;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r11.packageName;	 Catch:{ all -> 0x00a2 }
        r26 = r0;
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = ": ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r11.providerName;	 Catch:{ all -> 0x00a2 }
        r26 = r0;
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = ": ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r23;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x01bc;
    L_0x0226:
        r24 = "  Last Known Locations:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mLastLocation;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.entrySet();	 Catch:{ all -> 0x00a2 }
        r7 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x023e:
        r24 = r7.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x028b;
    L_0x0244:
        r5 = r7.next();	 Catch:{ all -> 0x00a2 }
        r5 = (java.util.Map.Entry) r5;	 Catch:{ all -> 0x00a2 }
        r16 = r5.getKey();	 Catch:{ all -> 0x00a2 }
        r16 = (java.lang.String) r16;	 Catch:{ all -> 0x00a2 }
        r12 = r5.getValue();	 Catch:{ all -> 0x00a2 }
        r12 = (android.location.Location) r12;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r16;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = ": ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r24 = r0.append(r12);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x023e;
    L_0x028b:
        r24 = "  Last Known Locations Coarse Intervals:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mLastLocationCoarseInterval;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.entrySet();	 Catch:{ all -> 0x00a2 }
        r7 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x02a3:
        r24 = r7.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x02f0;
    L_0x02a9:
        r5 = r7.next();	 Catch:{ all -> 0x00a2 }
        r5 = (java.util.Map.Entry) r5;	 Catch:{ all -> 0x00a2 }
        r16 = r5.getKey();	 Catch:{ all -> 0x00a2 }
        r16 = (java.lang.String) r16;	 Catch:{ all -> 0x00a2 }
        r12 = r5.getValue();	 Catch:{ all -> 0x00a2 }
        r12 = (android.location.Location) r12;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r16;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = ": ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r24 = r0.append(r12);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x02a3;
    L_0x02f0:
        r0 = r27;
        r0 = r0.mGeofenceManager;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r0 = r24;
        r1 = r29;
        r0.dump(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mEnabledProviders;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.size();	 Catch:{ all -> 0x00a2 }
        if (r24 <= 0) goto L_0x034b;
    L_0x0309:
        r24 = "  Enabled Providers:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mEnabledProviders;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r10 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x031d:
        r24 = r10.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x034b;
    L_0x0323:
        r8 = r10.next();	 Catch:{ all -> 0x00a2 }
        r8 = (java.lang.String) r8;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r24 = r0.append(r8);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x031d;
    L_0x034b:
        r0 = r27;
        r0 = r0.mDisabledProviders;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.size();	 Catch:{ all -> 0x00a2 }
        if (r24 <= 0) goto L_0x0399;
    L_0x0357:
        r24 = "  Disabled Providers:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mDisabledProviders;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r10 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x036b:
        r24 = r10.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x0399;
    L_0x0371:
        r8 = r10.next();	 Catch:{ all -> 0x00a2 }
        r8 = (java.lang.String) r8;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r24 = r0.append(r8);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x036b;
    L_0x0399:
        r24 = "  ";
        r0 = r29;
        r1 = r24;
        r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mBlacklist;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r0 = r24;
        r1 = r29;
        r0.dump(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mMockProviders;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.size();	 Catch:{ all -> 0x00a2 }
        if (r24 <= 0) goto L_0x03f3;
    L_0x03bc:
        r24 = "  Mock Providers:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mMockProviders;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.entrySet();	 Catch:{ all -> 0x00a2 }
        r10 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x03d4:
        r24 = r10.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x03f3;
    L_0x03da:
        r9 = r10.next();	 Catch:{ all -> 0x00a2 }
        r9 = (java.util.Map.Entry) r9;	 Catch:{ all -> 0x00a2 }
        r24 = r9.getValue();	 Catch:{ all -> 0x00a2 }
        r24 = (com.android.server.location.MockProvider) r24;	 Catch:{ all -> 0x00a2 }
        r26 = "      ";
        r0 = r24;
        r1 = r29;
        r2 = r26;
        r0.dump(r1, r2);	 Catch:{ all -> 0x00a2 }
        goto L_0x03d4;
    L_0x03f3:
        r0 = r27;
        r0 = r0.mBackgroundThrottlePackageWhitelist;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r24 = r24.isEmpty();	 Catch:{ all -> 0x00a2 }
        if (r24 != 0) goto L_0x0441;
    L_0x03ff:
        r24 = "  Throttling Whitelisted Packages:";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mBackgroundThrottlePackageWhitelist;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r14 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x0413:
        r24 = r14.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x0441;
    L_0x0419:
        r13 = r14.next();	 Catch:{ all -> 0x00a2 }
        r13 = (java.lang.String) r13;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = "    ";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r24 = r0.append(r13);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        goto L_0x0413;
    L_0x0441:
        r24 = "  fudger: ";
        r0 = r29;
        r1 = r24;
        r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r27;
        r0 = r0.mLocationFudger;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r0 = r24;
        r1 = r28;
        r2 = r29;
        r3 = r30;
        r0.dump(r1, r2, r3);	 Catch:{ all -> 0x00a2 }
        r0 = r30;
        r0 = r0.length;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        if (r24 <= 0) goto L_0x0476;
    L_0x0463:
        r24 = "short";
        r26 = 0;
        r26 = r30[r26];	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r26;
        r24 = r0.equals(r1);	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x0476;
    L_0x0474:
        monitor-exit(r25);
        return;
    L_0x0476:
        r0 = r27;
        r0 = r0.mProviders;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        r17 = r24.iterator();	 Catch:{ all -> 0x00a2 }
    L_0x0480:
        r24 = r17.hasNext();	 Catch:{ all -> 0x00a2 }
        if (r24 == 0) goto L_0x0505;
    L_0x0486:
        r15 = r17.next();	 Catch:{ all -> 0x00a2 }
        r15 = (com.android.server.location.LocationProviderInterface) r15;	 Catch:{ all -> 0x00a2 }
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = r15.getName();	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = " Internal State";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.print(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r15 instanceof com.android.server.location.LocationProviderProxy;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        if (r24 == 0) goto L_0x04f0;
    L_0x04b9:
        r0 = r15;
        r0 = (com.android.server.location.LocationProviderProxy) r0;	 Catch:{ all -> 0x00a2 }
        r18 = r0;
        r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00a2 }
        r24.<init>();	 Catch:{ all -> 0x00a2 }
        r26 = " (";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = r18.getConnectedPackageName();	 Catch:{ all -> 0x00a2 }
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r26 = ")";
        r0 = r24;
        r1 = r26;
        r24 = r0.append(r1);	 Catch:{ all -> 0x00a2 }
        r24 = r24.toString();	 Catch:{ all -> 0x00a2 }
        r0 = r29;
        r1 = r24;
        r0.print(r1);	 Catch:{ all -> 0x00a2 }
    L_0x04f0:
        r24 = ":";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
        r0 = r28;
        r1 = r29;
        r2 = r30;
        r15.dump(r0, r1, r2);	 Catch:{ all -> 0x00a2 }
        goto L_0x0480;
    L_0x0505:
        r0 = r27;
        r0 = r0.mGnssBatchingInProgress;	 Catch:{ all -> 0x00a2 }
        r24 = r0;
        if (r24 == 0) goto L_0x0517;
    L_0x050d:
        r24 = "  GNSS batching in progress";
        r0 = r29;
        r1 = r24;
        r0.println(r1);	 Catch:{ all -> 0x00a2 }
    L_0x0517:
        monitor-exit(r25);
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.LocationManagerService.dump(java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void");
    }
}
