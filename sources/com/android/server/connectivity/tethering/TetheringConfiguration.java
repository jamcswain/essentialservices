package com.android.server.connectivity.tethering;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.util.SharedLog;
import android.telephony.TelephonyManager;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringJoiner;

public class TetheringConfiguration {
    private static final String[] DHCP_DEFAULT_RANGE = new String[]{"192.168.42.2", "192.168.42.254", "192.168.43.2", "192.168.43.254", "192.168.44.2", "192.168.44.254", "192.168.45.2", "192.168.45.254", "192.168.46.2", "192.168.46.254", "192.168.47.2", "192.168.47.254", "192.168.48.2", "192.168.48.254", "192.168.49.2", "192.168.49.254"};
    public static final int DUN_NOT_REQUIRED = 0;
    public static final int DUN_REQUIRED = 1;
    public static final int DUN_UNSPECIFIED = 2;
    private static final String TAG = TetheringConfiguration.class.getSimpleName();
    private final String[] DEFAULT_IPV4_DNS = new String[]{"8.8.4.4", "8.8.8.8"};
    public final String[] defaultIPv4DNS;
    public final String[] dhcpRanges;
    public final int dunCheck;
    public final boolean isDunRequired;
    public final Collection<Integer> preferredUpstreamIfaceTypes;
    public final String[] tetherableBluetoothRegexs;
    public final String[] tetherableUsbRegexs;
    public final String[] tetherableWifiRegexs;

    public TetheringConfiguration(Context ctx, SharedLog log) {
        SharedLog configLog = log.forSubComponent("config");
        this.tetherableUsbRegexs = ctx.getResources().getStringArray(17236037);
        this.tetherableWifiRegexs = ctx.getResources().getStringArray(17236038);
        this.tetherableBluetoothRegexs = ctx.getResources().getStringArray(17236034);
        this.dunCheck = checkDunRequired(ctx);
        configLog.log("DUN check returned: " + dunCheckString(this.dunCheck));
        this.preferredUpstreamIfaceTypes = getUpstreamIfaceTypes(ctx, this.dunCheck);
        this.isDunRequired = this.preferredUpstreamIfaceTypes.contains(Integer.valueOf(4));
        this.dhcpRanges = getDhcpRanges(ctx);
        this.defaultIPv4DNS = copy(this.DEFAULT_IPV4_DNS);
        configLog.log(toString());
    }

    public boolean isUsb(String iface) {
        return matchesDownstreamRegexs(iface, this.tetherableUsbRegexs);
    }

    public boolean isWifi(String iface) {
        return matchesDownstreamRegexs(iface, this.tetherableWifiRegexs);
    }

    public boolean isBluetooth(String iface) {
        return matchesDownstreamRegexs(iface, this.tetherableBluetoothRegexs);
    }

    public void dump(PrintWriter pw) {
        dumpStringArray(pw, "tetherableUsbRegexs", this.tetherableUsbRegexs);
        dumpStringArray(pw, "tetherableWifiRegexs", this.tetherableWifiRegexs);
        dumpStringArray(pw, "tetherableBluetoothRegexs", this.tetherableBluetoothRegexs);
        pw.print("isDunRequired: ");
        pw.println(this.isDunRequired);
        dumpStringArray(pw, "preferredUpstreamIfaceTypes", preferredUpstreamNames(this.preferredUpstreamIfaceTypes));
        dumpStringArray(pw, "dhcpRanges", this.dhcpRanges);
        dumpStringArray(pw, "defaultIPv4DNS", this.defaultIPv4DNS);
    }

    public String toString() {
        StringJoiner sj = new StringJoiner(" ");
        sj.add(String.format("tetherableUsbRegexs:%s", new Object[]{makeString(this.tetherableUsbRegexs)}));
        sj.add(String.format("tetherableWifiRegexs:%s", new Object[]{makeString(this.tetherableWifiRegexs)}));
        sj.add(String.format("tetherableBluetoothRegexs:%s", new Object[]{makeString(this.tetherableBluetoothRegexs)}));
        sj.add(String.format("isDunRequired:%s", new Object[]{Boolean.valueOf(this.isDunRequired)}));
        sj.add(String.format("preferredUpstreamIfaceTypes:%s", new Object[]{makeString(preferredUpstreamNames(this.preferredUpstreamIfaceTypes))}));
        return String.format("TetheringConfiguration{%s}", new Object[]{sj.toString()});
    }

    private static void dumpStringArray(PrintWriter pw, String label, String[] values) {
        pw.print(label);
        pw.print(": ");
        if (values != null) {
            StringJoiner sj = new StringJoiner(", ", "[", "]");
            for (String value : values) {
                sj.add(value);
            }
            pw.print(sj.toString());
        } else {
            pw.print("null");
        }
        pw.println();
    }

    private static String makeString(String[] strings) {
        StringJoiner sj = new StringJoiner(",", "[", "]");
        for (String s : strings) {
            sj.add(s);
        }
        return sj.toString();
    }

    private static String[] preferredUpstreamNames(Collection<Integer> upstreamTypes) {
        String[] upstreamNames = null;
        if (upstreamTypes != null) {
            upstreamNames = new String[upstreamTypes.size()];
            int i = 0;
            for (Integer netType : upstreamTypes) {
                upstreamNames[i] = ConnectivityManager.getNetworkTypeName(netType.intValue());
                i++;
            }
        }
        return upstreamNames;
    }

    public static int checkDunRequired(Context ctx) {
        TelephonyManager tm = (TelephonyManager) ctx.getSystemService("phone");
        return tm != null ? tm.getTetherApnRequired() : 2;
    }

    private static String dunCheckString(int dunCheck) {
        switch (dunCheck) {
            case 0:
                return "DUN_NOT_REQUIRED";
            case 1:
                return "DUN_REQUIRED";
            case 2:
                return "DUN_UNSPECIFIED";
            default:
                return String.format("UNKNOWN (%s)", new Object[]{Integer.valueOf(dunCheck)});
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.Collection<java.lang.Integer> getUpstreamIfaceTypes(android.content.Context r10, int r11) {
        /*
        r9 = 4;
        r8 = 5;
        r7 = 1;
        r4 = 0;
        r3 = r10.getResources();
        r5 = 17236036; // 0x1070044 float:2.4795775E-38 double:8.5157333E-317;
        r1 = r3.getIntArray(r5);
        r2 = new java.util.ArrayList;
        r3 = r1.length;
        r2.<init>(r3);
        r5 = r1.length;
        r3 = r4;
    L_0x0017:
        if (r3 >= r5) goto L_0x002e;
    L_0x0019:
        r0 = r1[r3];
        switch(r0) {
            case 0: goto L_0x0028;
            case 1: goto L_0x001e;
            case 2: goto L_0x001e;
            case 3: goto L_0x001e;
            case 4: goto L_0x002b;
            case 5: goto L_0x0028;
            default: goto L_0x001e;
        };
    L_0x001e:
        r6 = java.lang.Integer.valueOf(r0);
        r2.add(r6);
    L_0x0025:
        r3 = r3 + 1;
        goto L_0x0017;
    L_0x0028:
        if (r11 != r7) goto L_0x001e;
    L_0x002a:
        goto L_0x0025;
    L_0x002b:
        if (r11 != 0) goto L_0x001e;
    L_0x002d:
        goto L_0x0025;
    L_0x002e:
        if (r11 != r7) goto L_0x0039;
    L_0x0030:
        appendIfNotPresent(r2, r9);
    L_0x0033:
        r3 = 9;
        prependIfNotPresent(r2, r3);
        return r2;
    L_0x0039:
        if (r11 != 0) goto L_0x0042;
    L_0x003b:
        appendIfNotPresent(r2, r4);
        appendIfNotPresent(r2, r8);
        goto L_0x0033;
    L_0x0042:
        r3 = 3;
        r3 = new java.lang.Integer[r3];
        r5 = java.lang.Integer.valueOf(r9);
        r3[r4] = r5;
        r5 = java.lang.Integer.valueOf(r4);
        r3[r7] = r5;
        r5 = java.lang.Integer.valueOf(r8);
        r6 = 2;
        r3[r6] = r5;
        r3 = containsOneOf(r2, r3);
        if (r3 != 0) goto L_0x0033;
    L_0x005e:
        r3 = java.lang.Integer.valueOf(r4);
        r2.add(r3);
        r3 = java.lang.Integer.valueOf(r8);
        r2.add(r3);
        goto L_0x0033;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.connectivity.tethering.TetheringConfiguration.getUpstreamIfaceTypes(android.content.Context, int):java.util.Collection<java.lang.Integer>");
    }

    private static boolean matchesDownstreamRegexs(String iface, String[] regexs) {
        for (String regex : regexs) {
            if (iface.matches(regex)) {
                return true;
            }
        }
        return false;
    }

    private static String[] getDhcpRanges(Context ctx) {
        String[] fromResource = ctx.getResources().getStringArray(17236035);
        if (fromResource.length <= 0 || fromResource.length % 2 != 0) {
            return copy(DHCP_DEFAULT_RANGE);
        }
        return fromResource;
    }

    private static String[] copy(String[] strarray) {
        return (String[]) Arrays.copyOf(strarray, strarray.length);
    }

    private static void prependIfNotPresent(ArrayList<Integer> list, int value) {
        if (!list.contains(Integer.valueOf(value))) {
            list.add(0, Integer.valueOf(value));
        }
    }

    private static void appendIfNotPresent(ArrayList<Integer> list, int value) {
        if (!list.contains(Integer.valueOf(value))) {
            list.add(Integer.valueOf(value));
        }
    }

    private static boolean containsOneOf(ArrayList<Integer> list, Integer... values) {
        for (Integer value : values) {
            if (list.contains(value)) {
                return true;
            }
        }
        return false;
    }
}
