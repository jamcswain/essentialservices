package com.android.server.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.UserInfo;
import android.net.Uri;
import android.os.INetworkManagementService;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import android.util.Log;
import com.android.server.NetworkManagementService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PermissionMonitor {
    private static final boolean DBG = true;
    private static final Boolean NETWORK = Boolean.FALSE;
    private static final Boolean SYSTEM = Boolean.TRUE;
    private static final String TAG = "PermissionMonitor";
    private final Map<Integer, Boolean> mApps = new HashMap();
    private final Context mContext;
    private final BroadcastReceiver mIntentReceiver;
    private final INetworkManagementService mNetd;
    private final PackageManager mPackageManager;
    private final UserManager mUserManager;
    private final Set<Integer> mUsers = new HashSet();

    public PermissionMonitor(Context context, INetworkManagementService netd) {
        this.mContext = context;
        this.mPackageManager = context.getPackageManager();
        this.mUserManager = UserManager.get(context);
        this.mNetd = netd;
        this.mIntentReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                int user = intent.getIntExtra("android.intent.extra.user_handle", -10000);
                int appUid = intent.getIntExtra("android.intent.extra.UID", -1);
                Uri appData = intent.getData();
                String schemeSpecificPart = appData != null ? appData.getSchemeSpecificPart() : null;
                if ("android.intent.action.USER_ADDED".equals(action)) {
                    PermissionMonitor.this.onUserAdded(user);
                } else if ("android.intent.action.USER_REMOVED".equals(action)) {
                    PermissionMonitor.this.onUserRemoved(user);
                } else if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
                    PermissionMonitor.this.onAppAdded(schemeSpecificPart, appUid);
                } else if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
                    PermissionMonitor.this.onAppRemoved(appUid);
                }
            }
        };
    }

    public synchronized void startMonitoring() {
        log("Monitoring");
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.USER_ADDED");
        intentFilter.addAction("android.intent.action.USER_REMOVED");
        this.mContext.registerReceiverAsUser(this.mIntentReceiver, UserHandle.ALL, intentFilter, null, null);
        intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        this.mContext.registerReceiverAsUser(this.mIntentReceiver, UserHandle.ALL, intentFilter, null, null);
        List<PackageInfo> apps = this.mPackageManager.getInstalledPackages(4096);
        if (apps == null) {
            loge("No apps");
            return;
        }
        for (PackageInfo app : apps) {
            int uid = app.applicationInfo != null ? app.applicationInfo.uid : -1;
            if (uid >= 0) {
                boolean isNetwork = hasNetworkPermission(app);
                boolean hasRestrictedPermission = hasRestrictedNetworkPermission(app);
                if (isNetwork || hasRestrictedPermission) {
                    Boolean permission = (Boolean) this.mApps.get(Integer.valueOf(uid));
                    if (permission == null || permission == NETWORK) {
                        this.mApps.put(Integer.valueOf(uid), Boolean.valueOf(hasRestrictedPermission));
                    }
                }
            }
        }
        List<UserInfo> users = this.mUserManager.getUsers(true);
        if (users != null) {
            for (UserInfo user : users) {
                this.mUsers.add(Integer.valueOf(user.id));
            }
        }
        log("Users: " + this.mUsers.size() + ", Apps: " + this.mApps.size());
        update(this.mUsers, this.mApps, true);
    }

    private boolean hasPermission(PackageInfo app, String permission) {
        if (app.requestedPermissions != null) {
            for (String p : app.requestedPermissions) {
                if (permission.equals(p)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean hasNetworkPermission(PackageInfo app) {
        return hasPermission(app, "android.permission.CHANGE_NETWORK_STATE");
    }

    private boolean hasRestrictedNetworkPermission(PackageInfo app) {
        boolean z = true;
        int flags = app.applicationInfo != null ? app.applicationInfo.flags : 0;
        if ((flags & 1) != 0 || (flags & 128) != 0) {
            return true;
        }
        if (!hasPermission(app, "android.permission.CONNECTIVITY_INTERNAL")) {
            z = hasPermission(app, "android.permission.CONNECTIVITY_USE_RESTRICTED_NETWORKS");
        }
        return z;
    }

    private int[] toIntArray(List<Integer> list) {
        int[] array = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = ((Integer) list.get(i)).intValue();
        }
        return array;
    }

    private void update(Set<Integer> users, Map<Integer, Boolean> apps, boolean add) {
        List<Integer> network = new ArrayList();
        List<Integer> system = new ArrayList();
        for (Entry<Integer, Boolean> app : apps.entrySet()) {
            List<Integer> list = ((Boolean) app.getValue()).booleanValue() ? system : network;
            for (Integer intValue : users) {
                list.add(Integer.valueOf(UserHandle.getUid(intValue.intValue(), ((Integer) app.getKey()).intValue())));
            }
        }
        if (add) {
            try {
                this.mNetd.setPermission(NetworkManagementService.PERMISSION_NETWORK, toIntArray(network));
                this.mNetd.setPermission(NetworkManagementService.PERMISSION_SYSTEM, toIntArray(system));
                return;
            } catch (RemoteException e) {
                loge("Exception when updating permissions: " + e);
                return;
            }
        }
        this.mNetd.clearPermission(toIntArray(network));
        this.mNetd.clearPermission(toIntArray(system));
    }

    private synchronized void onUserAdded(int user) {
        if (user < 0) {
            loge("Invalid user in onUserAdded: " + user);
            return;
        }
        this.mUsers.add(Integer.valueOf(user));
        Set<Integer> users = new HashSet();
        users.add(Integer.valueOf(user));
        update(users, this.mApps, true);
    }

    private synchronized void onUserRemoved(int user) {
        if (user < 0) {
            loge("Invalid user in onUserRemoved: " + user);
            return;
        }
        this.mUsers.remove(Integer.valueOf(user));
        Set<Integer> users = new HashSet();
        users.add(Integer.valueOf(user));
        update(users, this.mApps, false);
    }

    private Boolean highestPermissionForUid(Boolean currentPermission, String name) {
        if (currentPermission == SYSTEM) {
            return currentPermission;
        }
        try {
            PackageInfo app = this.mPackageManager.getPackageInfo(name, 4096);
            boolean isNetwork = hasNetworkPermission(app);
            boolean hasRestrictedPermission = hasRestrictedNetworkPermission(app);
            if (isNetwork || hasRestrictedPermission) {
                currentPermission = Boolean.valueOf(hasRestrictedPermission);
            }
        } catch (NameNotFoundException e) {
            loge("NameNotFoundException " + name);
        }
        return currentPermission;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void onAppAdded(java.lang.String r5, int r6) {
        /*
        r4 = this;
        monitor-enter(r4);
        r2 = android.text.TextUtils.isEmpty(r5);	 Catch:{ all -> 0x0066 }
        if (r2 != 0) goto L_0x0009;
    L_0x0007:
        if (r6 >= 0) goto L_0x002d;
    L_0x0009:
        r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0066 }
        r2.<init>();	 Catch:{ all -> 0x0066 }
        r3 = "Invalid app in onAppAdded: ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0066 }
        r2 = r2.append(r5);	 Catch:{ all -> 0x0066 }
        r3 = " | ";
        r2 = r2.append(r3);	 Catch:{ all -> 0x0066 }
        r2 = r2.append(r6);	 Catch:{ all -> 0x0066 }
        r2 = r2.toString();	 Catch:{ all -> 0x0066 }
        loge(r2);	 Catch:{ all -> 0x0066 }
        monitor-exit(r4);
        return;
    L_0x002d:
        r2 = r4.mApps;	 Catch:{ all -> 0x0066 }
        r3 = java.lang.Integer.valueOf(r6);	 Catch:{ all -> 0x0066 }
        r2 = r2.get(r3);	 Catch:{ all -> 0x0066 }
        r2 = (java.lang.Boolean) r2;	 Catch:{ all -> 0x0066 }
        r1 = r4.highestPermissionForUid(r2, r5);	 Catch:{ all -> 0x0066 }
        r2 = r4.mApps;	 Catch:{ all -> 0x0066 }
        r3 = java.lang.Integer.valueOf(r6);	 Catch:{ all -> 0x0066 }
        r2 = r2.get(r3);	 Catch:{ all -> 0x0066 }
        if (r1 == r2) goto L_0x0064;
    L_0x0049:
        r2 = r4.mApps;	 Catch:{ all -> 0x0066 }
        r3 = java.lang.Integer.valueOf(r6);	 Catch:{ all -> 0x0066 }
        r2.put(r3, r1);	 Catch:{ all -> 0x0066 }
        r0 = new java.util.HashMap;	 Catch:{ all -> 0x0066 }
        r0.<init>();	 Catch:{ all -> 0x0066 }
        r2 = java.lang.Integer.valueOf(r6);	 Catch:{ all -> 0x0066 }
        r0.put(r2, r1);	 Catch:{ all -> 0x0066 }
        r2 = r4.mUsers;	 Catch:{ all -> 0x0066 }
        r3 = 1;
        r4.update(r2, r0, r3);	 Catch:{ all -> 0x0066 }
    L_0x0064:
        monitor-exit(r4);
        return;
    L_0x0066:
        r2 = move-exception;
        monitor-exit(r4);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.connectivity.PermissionMonitor.onAppAdded(java.lang.String, int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void onAppRemoved(int r8) {
        /*
        r7 = this;
        r4 = 0;
        monitor-enter(r7);
        if (r8 >= 0) goto L_0x001d;
    L_0x0004:
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0081 }
        r4.<init>();	 Catch:{ all -> 0x0081 }
        r5 = "Invalid app in onAppRemoved: ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x0081 }
        r4 = r4.append(r8);	 Catch:{ all -> 0x0081 }
        r4 = r4.toString();	 Catch:{ all -> 0x0081 }
        loge(r4);	 Catch:{ all -> 0x0081 }
        monitor-exit(r7);
        return;
    L_0x001d:
        r0 = new java.util.HashMap;	 Catch:{ all -> 0x0081 }
        r0.<init>();	 Catch:{ all -> 0x0081 }
        r3 = 0;
        r5 = r7.mPackageManager;	 Catch:{ all -> 0x0081 }
        r2 = r5.getPackagesForUid(r8);	 Catch:{ all -> 0x0081 }
        if (r2 == 0) goto L_0x0040;
    L_0x002b:
        r5 = r2.length;	 Catch:{ all -> 0x0081 }
        if (r5 <= 0) goto L_0x0040;
    L_0x002e:
        r5 = r2.length;	 Catch:{ all -> 0x0081 }
    L_0x002f:
        if (r4 >= r5) goto L_0x0040;
    L_0x0031:
        r1 = r2[r4];	 Catch:{ all -> 0x0081 }
        r3 = r7.highestPermissionForUid(r3, r1);	 Catch:{ all -> 0x0081 }
        r6 = SYSTEM;	 Catch:{ all -> 0x0081 }
        if (r3 != r6) goto L_0x003d;
    L_0x003b:
        monitor-exit(r7);
        return;
    L_0x003d:
        r4 = r4 + 1;
        goto L_0x002f;
    L_0x0040:
        r4 = r7.mApps;	 Catch:{ all -> 0x0081 }
        r5 = java.lang.Integer.valueOf(r8);	 Catch:{ all -> 0x0081 }
        r4 = r4.get(r5);	 Catch:{ all -> 0x0081 }
        if (r3 != r4) goto L_0x004e;
    L_0x004c:
        monitor-exit(r7);
        return;
    L_0x004e:
        if (r3 == 0) goto L_0x0068;
    L_0x0050:
        r4 = r7.mApps;	 Catch:{ all -> 0x0081 }
        r5 = java.lang.Integer.valueOf(r8);	 Catch:{ all -> 0x0081 }
        r4.put(r5, r3);	 Catch:{ all -> 0x0081 }
        r4 = java.lang.Integer.valueOf(r8);	 Catch:{ all -> 0x0081 }
        r0.put(r4, r3);	 Catch:{ all -> 0x0081 }
        r4 = r7.mUsers;	 Catch:{ all -> 0x0081 }
        r5 = 1;
        r7.update(r4, r0, r5);	 Catch:{ all -> 0x0081 }
    L_0x0066:
        monitor-exit(r7);
        return;
    L_0x0068:
        r4 = r7.mApps;	 Catch:{ all -> 0x0081 }
        r5 = java.lang.Integer.valueOf(r8);	 Catch:{ all -> 0x0081 }
        r4.remove(r5);	 Catch:{ all -> 0x0081 }
        r4 = java.lang.Integer.valueOf(r8);	 Catch:{ all -> 0x0081 }
        r5 = NETWORK;	 Catch:{ all -> 0x0081 }
        r0.put(r4, r5);	 Catch:{ all -> 0x0081 }
        r4 = r7.mUsers;	 Catch:{ all -> 0x0081 }
        r5 = 0;
        r7.update(r4, r0, r5);	 Catch:{ all -> 0x0081 }
        goto L_0x0066;
    L_0x0081:
        r4 = move-exception;
        monitor-exit(r7);
        throw r4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.connectivity.PermissionMonitor.onAppRemoved(int):void");
    }

    private static void log(String s) {
        Log.d(TAG, s);
    }

    private static void loge(String s) {
        Log.e(TAG, s);
    }
}
