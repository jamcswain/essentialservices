package com.android.server.connectivity;

import android.app.AppGlobals;
import android.app.AppOpsManager;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.pm.UserInfo;
import android.net.ConnectivityManager;
import android.net.INetworkManagementEventObserver;
import android.net.IpPrefix;
import android.net.LinkAddress;
import android.net.LinkProperties;
import android.net.LocalSocket;
import android.net.Network;
import android.net.NetworkAgent;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.NetworkMisc;
import android.net.RouteInfo;
import android.net.UidRange;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.INetworkManagementService;
import android.os.Looper;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemService;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.Settings.Secure;
import android.security.KeyStore;
import android.text.TextUtils;
import android.util.ArraySet;
import android.util.Log;
import com.android.internal.annotations.GuardedBy;
import com.android.internal.net.LegacyVpnInfo;
import com.android.internal.net.VpnConfig;
import com.android.internal.net.VpnInfo;
import com.android.internal.net.VpnProfile;
import com.android.internal.notification.SystemNotificationChannels;
import com.android.server.display.DisplayTransformManager;
import com.android.server.net.BaseNetworkObserver;
import com.android.server.usb.UsbAudioDevice;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import libcore.io.IoUtils;

public class Vpn {
    private static final boolean LOGD = true;
    private static final String NETWORKTYPE = "VPN";
    private static final String TAG = "Vpn";
    private static final long VPN_LAUNCH_IDLE_WHITELIST_DURATION = 60000;
    private boolean mAlwaysOn;
    @GuardedBy("this")
    private Set<UidRange> mBlockedUsers;
    private VpnConfig mConfig;
    private Connection mConnection;
    private Context mContext;
    private volatile boolean mEnableTeardown;
    private String mInterface;
    private boolean mIsPackageIntentReceiverRegistered;
    private LegacyVpnRunner mLegacyVpnRunner;
    private boolean mLockdown;
    private final Looper mLooper;
    private final INetworkManagementService mNetd;
    private NetworkAgent mNetworkAgent;
    private final NetworkCapabilities mNetworkCapabilities;
    private NetworkInfo mNetworkInfo;
    private INetworkManagementEventObserver mObserver;
    private int mOwnerUID;
    private String mPackage;
    private final BroadcastReceiver mPackageIntentReceiver;
    private PendingIntent mStatusIntent;
    private final SystemServices mSystemServices;
    private final int mUserHandle;
    @GuardedBy("this")
    private Set<UidRange> mVpnUsers;

    private class Connection implements ServiceConnection {
        private IBinder mService;

        private Connection() {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            this.mService = service;
        }

        public void onServiceDisconnected(ComponentName name) {
            this.mService = null;
        }
    }

    private class LegacyVpnRunner extends Thread {
        private static final String TAG = "LegacyVpnRunner";
        private final String[][] mArguments;
        private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (LegacyVpnRunner.this.this$0.mEnableTeardown && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE") && intent.getIntExtra("networkType", -1) == LegacyVpnRunner.this.mOuterConnection.get()) {
                    NetworkInfo info = (NetworkInfo) intent.getExtra("networkInfo");
                    if (!(info == null || (info.isConnectedOrConnecting() ^ 1) == 0)) {
                        try {
                            LegacyVpnRunner.this.this$0.mObserver.interfaceStatusChanged(LegacyVpnRunner.this.mOuterInterface, false);
                        } catch (RemoteException e) {
                        }
                    }
                }
            }
        };
        private final String[] mDaemons;
        private final AtomicInteger mOuterConnection = new AtomicInteger(-1);
        private final String mOuterInterface;
        private final LocalSocket[] mSockets;
        private long mTimer = -1;
        final /* synthetic */ Vpn this$0;

        public LegacyVpnRunner(Vpn this$0, VpnConfig config, String[] racoon, String[] mtpd) {
            int i = 0;
            this.this$0 = this$0;
            super(TAG);
            this$0.mConfig = config;
            this.mDaemons = new String[]{"racoon", "mtpd"};
            this.mArguments = new String[][]{racoon, mtpd};
            this.mSockets = new LocalSocket[this.mDaemons.length];
            this.mOuterInterface = this$0.mConfig.interfaze;
            if (!TextUtils.isEmpty(this.mOuterInterface)) {
                ConnectivityManager cm = ConnectivityManager.from(this$0.mContext);
                Network[] allNetworks = cm.getAllNetworks();
                int length = allNetworks.length;
                while (i < length) {
                    Network network = allNetworks[i];
                    LinkProperties lp = cm.getLinkProperties(network);
                    if (lp != null && lp.getAllInterfaceNames().contains(this.mOuterInterface)) {
                        NetworkInfo networkInfo = cm.getNetworkInfo(network);
                        if (networkInfo != null) {
                            this.mOuterConnection.set(networkInfo.getType());
                        }
                    }
                    i++;
                }
            }
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            this$0.mContext.registerReceiver(this.mBroadcastReceiver, filter);
        }

        public void check(String interfaze) {
            if (interfaze.equals(this.mOuterInterface)) {
                Log.i(TAG, "Legacy VPN is going down with " + interfaze);
                exit();
            }
        }

        public void exit() {
            interrupt();
            this.this$0.agentDisconnect();
            try {
                this.this$0.mContext.unregisterReceiver(this.mBroadcastReceiver);
            } catch (IllegalArgumentException e) {
            }
        }

        public void run() {
            String[] strArr;
            int length;
            String[] strArr2;
            int i = 0;
            Log.v(TAG, "Waiting");
            synchronized (TAG) {
                Log.v(TAG, "Executing");
                int length2;
                try {
                    execute();
                    monitorDaemons();
                    interrupted();
                    for (LocalSocket socket : this.mSockets) {
                        IoUtils.closeQuietly(socket);
                    }
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    for (LocalSocket socket2 : this.mSockets) {
                        IoUtils.closeQuietly(socket2);
                    }
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e2) {
                    }
                    strArr = this.mDaemons;
                    length = strArr.length;
                    while (i < length) {
                        SystemService.stop(strArr[i]);
                        i++;
                    }
                } catch (InterruptedException e3) {
                } catch (Throwable th) {
                    for (LocalSocket socket22 : this.mSockets) {
                        IoUtils.closeQuietly(socket22);
                    }
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e4) {
                    }
                    strArr2 = this.mDaemons;
                    length2 = strArr2.length;
                    while (i < length2) {
                        SystemService.stop(strArr2[i]);
                        i++;
                    }
                }
                strArr = this.mDaemons;
                length = strArr.length;
                while (i < length) {
                    SystemService.stop(strArr[i]);
                    i++;
                }
                this.this$0.agentDisconnect();
            }
        }

        private void checkpoint(boolean yield) throws InterruptedException {
            long now = SystemClock.elapsedRealtime();
            if (this.mTimer == -1) {
                this.mTimer = now;
                Thread.sleep(1);
            } else if (now - this.mTimer <= 60000) {
                Thread.sleep((long) (yield ? DisplayTransformManager.LEVEL_COLOR_MATRIX_GRAYSCALE : 1));
            } else {
                this.this$0.updateState(DetailedState.FAILED, "checkpoint");
                throw new IllegalStateException("Time is up");
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void execute() {
            /*
            r26 = this;
            r16 = 0;
            r22 = 0;
            r0 = r26;
            r1 = r22;
            r0.checkpoint(r1);	 Catch:{ Exception -> 0x0030 }
            r0 = r26;
            r0 = r0.mDaemons;	 Catch:{ Exception -> 0x0030 }
            r23 = r0;
            r22 = 0;
            r0 = r23;
            r0 = r0.length;	 Catch:{ Exception -> 0x0030 }
            r24 = r0;
        L_0x0018:
            r0 = r22;
            r1 = r24;
            if (r0 >= r1) goto L_0x0054;
        L_0x001e:
            r9 = r23[r22];	 Catch:{ Exception -> 0x0030 }
        L_0x0020:
            r25 = android.os.SystemService.isStopped(r9);	 Catch:{ Exception -> 0x0030 }
            if (r25 != 0) goto L_0x0051;
        L_0x0026:
            r25 = 1;
            r0 = r26;
            r1 = r25;
            r0.checkpoint(r1);	 Catch:{ Exception -> 0x0030 }
            goto L_0x0020;
        L_0x0030:
            r11 = move-exception;
            r22 = "LegacyVpnRunner";
            r23 = "Aborting";
            r0 = r22;
            r1 = r23;
            android.util.Log.i(r0, r1, r11);
            r0 = r26;
            r0 = r0.this$0;
            r22 = r0;
            r23 = android.net.NetworkInfo.DetailedState.FAILED;
            r24 = r11.getMessage();
            r22.updateState(r23, r24);
            r26.exit();
        L_0x0050:
            return;
        L_0x0051:
            r22 = r22 + 1;
            goto L_0x0018;
        L_0x0054:
            r21 = new java.io.File;	 Catch:{ Exception -> 0x0030 }
            r22 = "/data/misc/vpn/state";
            r21.<init>(r22);	 Catch:{ Exception -> 0x0030 }
            r21.delete();	 Catch:{ Exception -> 0x0030 }
            r22 = r21.exists();	 Catch:{ Exception -> 0x0030 }
            if (r22 == 0) goto L_0x006e;
        L_0x0065:
            r22 = new java.lang.IllegalStateException;	 Catch:{ Exception -> 0x0030 }
            r23 = "Cannot delete the state";
            r22.<init>(r23);	 Catch:{ Exception -> 0x0030 }
            throw r22;	 Catch:{ Exception -> 0x0030 }
        L_0x006e:
            r22 = new java.io.File;	 Catch:{ Exception -> 0x0030 }
            r23 = "/data/misc/vpn/abort";
            r22.<init>(r23);	 Catch:{ Exception -> 0x0030 }
            r22.delete();	 Catch:{ Exception -> 0x0030 }
            r16 = 1;
            r19 = 0;
            r0 = r26;
            r0 = r0.mArguments;	 Catch:{ Exception -> 0x0030 }
            r23 = r0;
            r22 = 0;
            r0 = r23;
            r0 = r0.length;	 Catch:{ Exception -> 0x0030 }
            r24 = r0;
        L_0x008a:
            r0 = r22;
            r1 = r24;
            if (r0 >= r1) goto L_0x009e;
        L_0x0090:
            r7 = r23[r22];	 Catch:{ Exception -> 0x0030 }
            if (r19 != 0) goto L_0x0096;
        L_0x0094:
            if (r7 == 0) goto L_0x009b;
        L_0x0096:
            r19 = 1;
        L_0x0098:
            r22 = r22 + 1;
            goto L_0x008a;
        L_0x009b:
            r19 = 0;
            goto L_0x0098;
        L_0x009e:
            if (r19 != 0) goto L_0x00aa;
        L_0x00a0:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22.agentDisconnect();	 Catch:{ Exception -> 0x0030 }
            return;
        L_0x00aa:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r23 = android.net.NetworkInfo.DetailedState.CONNECTING;	 Catch:{ Exception -> 0x0030 }
            r24 = "execute";
            r22.updateState(r23, r24);	 Catch:{ Exception -> 0x0030 }
            r14 = 0;
        L_0x00b9:
            r0 = r26;
            r0 = r0.mDaemons;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r0 = r22;
            r0 = r0.length;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r0 = r22;
            if (r14 >= r0) goto L_0x01ca;
        L_0x00c8:
            r0 = r26;
            r0 = r0.mArguments;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r7 = r22[r14];	 Catch:{ Exception -> 0x0030 }
            if (r7 != 0) goto L_0x00d5;
        L_0x00d2:
            r14 = r14 + 1;
            goto L_0x00b9;
        L_0x00d5:
            r0 = r26;
            r0 = r0.mDaemons;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r9 = r22[r14];	 Catch:{ Exception -> 0x0030 }
            android.os.SystemService.start(r9);	 Catch:{ Exception -> 0x0030 }
        L_0x00e0:
            r22 = android.os.SystemService.isRunning(r9);	 Catch:{ Exception -> 0x0030 }
            if (r22 != 0) goto L_0x00f0;
        L_0x00e6:
            r22 = 1;
            r0 = r26;
            r1 = r22;
            r0.checkpoint(r1);	 Catch:{ Exception -> 0x0030 }
            goto L_0x00e0;
        L_0x00f0:
            r0 = r26;
            r0 = r0.mSockets;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r23 = new android.net.LocalSocket;	 Catch:{ Exception -> 0x0030 }
            r23.<init>();	 Catch:{ Exception -> 0x0030 }
            r22[r14] = r23;	 Catch:{ Exception -> 0x0030 }
            r5 = new android.net.LocalSocketAddress;	 Catch:{ Exception -> 0x0030 }
            r22 = android.net.LocalSocketAddress.Namespace.RESERVED;	 Catch:{ Exception -> 0x0030 }
            r0 = r22;
            r5.<init>(r9, r0);	 Catch:{ Exception -> 0x0030 }
        L_0x0106:
            r0 = r26;
            r0 = r0.mSockets;	 Catch:{ Exception -> 0x0156 }
            r22 = r0;
            r22 = r22[r14];	 Catch:{ Exception -> 0x0156 }
            r0 = r22;
            r0.connect(r5);	 Catch:{ Exception -> 0x0156 }
            r0 = r26;
            r0 = r0.mSockets;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22[r14];	 Catch:{ Exception -> 0x0030 }
            r23 = 500; // 0x1f4 float:7.0E-43 double:2.47E-321;
            r22.setSoTimeout(r23);	 Catch:{ Exception -> 0x0030 }
            r0 = r26;
            r0 = r0.mSockets;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22[r14];	 Catch:{ Exception -> 0x0030 }
            r17 = r22.getOutputStream();	 Catch:{ Exception -> 0x0030 }
            r22 = 0;
            r0 = r7.length;	 Catch:{ Exception -> 0x0030 }
            r23 = r0;
        L_0x0131:
            r0 = r22;
            r1 = r23;
            if (r0 >= r1) goto L_0x0188;
        L_0x0137:
            r6 = r7[r22];	 Catch:{ Exception -> 0x0030 }
            r24 = java.nio.charset.StandardCharsets.UTF_8;	 Catch:{ Exception -> 0x0030 }
            r0 = r24;
            r8 = r6.getBytes(r0);	 Catch:{ Exception -> 0x0030 }
            r0 = r8.length;	 Catch:{ Exception -> 0x0030 }
            r24 = r0;
            r25 = 65535; // 0xffff float:9.1834E-41 double:3.23786E-319;
            r0 = r24;
            r1 = r25;
            if (r0 < r1) goto L_0x0161;
        L_0x014d:
            r22 = new java.lang.IllegalArgumentException;	 Catch:{ Exception -> 0x0030 }
            r23 = "Argument is too large";
            r22.<init>(r23);	 Catch:{ Exception -> 0x0030 }
            throw r22;	 Catch:{ Exception -> 0x0030 }
        L_0x0156:
            r11 = move-exception;
            r22 = 1;
            r0 = r26;
            r1 = r22;
            r0.checkpoint(r1);	 Catch:{ Exception -> 0x0030 }
            goto L_0x0106;
        L_0x0161:
            r0 = r8.length;	 Catch:{ Exception -> 0x0030 }
            r24 = r0;
            r24 = r24 >> 8;
            r0 = r17;
            r1 = r24;
            r0.write(r1);	 Catch:{ Exception -> 0x0030 }
            r0 = r8.length;	 Catch:{ Exception -> 0x0030 }
            r24 = r0;
            r0 = r17;
            r1 = r24;
            r0.write(r1);	 Catch:{ Exception -> 0x0030 }
            r0 = r17;
            r0.write(r8);	 Catch:{ Exception -> 0x0030 }
            r24 = 0;
            r0 = r26;
            r1 = r24;
            r0.checkpoint(r1);	 Catch:{ Exception -> 0x0030 }
            r22 = r22 + 1;
            goto L_0x0131;
        L_0x0188:
            r22 = 255; // 0xff float:3.57E-43 double:1.26E-321;
            r0 = r17;
            r1 = r22;
            r0.write(r1);	 Catch:{ Exception -> 0x0030 }
            r22 = 255; // 0xff float:3.57E-43 double:1.26E-321;
            r0 = r17;
            r1 = r22;
            r0.write(r1);	 Catch:{ Exception -> 0x0030 }
            r17.flush();	 Catch:{ Exception -> 0x0030 }
            r0 = r26;
            r0 = r0.mSockets;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22[r14];	 Catch:{ Exception -> 0x0030 }
            r15 = r22.getInputStream();	 Catch:{ Exception -> 0x0030 }
        L_0x01a9:
            r22 = r15.read();	 Catch:{ Exception -> 0x01bf }
            r23 = -1;
            r0 = r22;
            r1 = r23;
            if (r0 == r1) goto L_0x00d2;
        L_0x01b5:
            r22 = 1;
            r0 = r26;
            r1 = r22;
            r0.checkpoint(r1);	 Catch:{ Exception -> 0x0030 }
            goto L_0x01a9;
        L_0x01bf:
            r11 = move-exception;
            goto L_0x01b5;
        L_0x01c1:
            r22 = 1;
            r0 = r26;
            r1 = r22;
            r0.checkpoint(r1);	 Catch:{ Exception -> 0x0030 }
        L_0x01ca:
            r22 = r21.exists();	 Catch:{ Exception -> 0x0030 }
            if (r22 != 0) goto L_0x0219;
        L_0x01d0:
            r14 = 0;
        L_0x01d1:
            r0 = r26;
            r0 = r0.mDaemons;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r0 = r22;
            r0 = r0.length;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r0 = r22;
            if (r14 >= r0) goto L_0x01c1;
        L_0x01e0:
            r0 = r26;
            r0 = r0.mDaemons;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r9 = r22[r14];	 Catch:{ Exception -> 0x0030 }
            r0 = r26;
            r0 = r0.mArguments;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22[r14];	 Catch:{ Exception -> 0x0030 }
            if (r22 == 0) goto L_0x0216;
        L_0x01f2:
            r22 = android.os.SystemService.isRunning(r9);	 Catch:{ Exception -> 0x0030 }
            r22 = r22 ^ 1;
            if (r22 == 0) goto L_0x0216;
        L_0x01fa:
            r22 = new java.lang.IllegalStateException;	 Catch:{ Exception -> 0x0030 }
            r23 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0030 }
            r23.<init>();	 Catch:{ Exception -> 0x0030 }
            r0 = r23;
            r23 = r0.append(r9);	 Catch:{ Exception -> 0x0030 }
            r24 = " is dead";
            r23 = r23.append(r24);	 Catch:{ Exception -> 0x0030 }
            r23 = r23.toString();	 Catch:{ Exception -> 0x0030 }
            r22.<init>(r23);	 Catch:{ Exception -> 0x0030 }
            throw r22;	 Catch:{ Exception -> 0x0030 }
        L_0x0216:
            r14 = r14 + 1;
            goto L_0x01d1;
        L_0x0219:
            r22 = 0;
            r23 = 0;
            r22 = android.os.FileUtils.readTextFile(r21, r22, r23);	 Catch:{ Exception -> 0x0030 }
            r23 = "\n";
            r24 = -1;
            r18 = r22.split(r23, r24);	 Catch:{ Exception -> 0x0030 }
            r0 = r18;
            r0 = r0.length;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r23 = 7;
            r0 = r22;
            r1 = r23;
            if (r0 == r1) goto L_0x0240;
        L_0x0237:
            r22 = new java.lang.IllegalStateException;	 Catch:{ Exception -> 0x0030 }
            r23 = "Cannot parse the state";
            r22.<init>(r23);	 Catch:{ Exception -> 0x0030 }
            throw r22;	 Catch:{ Exception -> 0x0030 }
        L_0x0240:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r23 = 0;
            r23 = r18[r23];	 Catch:{ Exception -> 0x0030 }
            r23 = r23.trim();	 Catch:{ Exception -> 0x0030 }
            r0 = r23;
            r1 = r22;
            r1.interfaze = r0;	 Catch:{ Exception -> 0x0030 }
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r23 = 1;
            r23 = r18[r23];	 Catch:{ Exception -> 0x0030 }
            r22.addLegacyAddresses(r23);	 Catch:{ Exception -> 0x0030 }
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r0 = r22;
            r0 = r0.routes;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            if (r22 == 0) goto L_0x0291;
        L_0x027b:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r0 = r22;
            r0 = r0.routes;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.isEmpty();	 Catch:{ Exception -> 0x0030 }
            if (r22 == 0) goto L_0x02a2;
        L_0x0291:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r23 = 2;
            r23 = r18[r23];	 Catch:{ Exception -> 0x0030 }
            r22.addLegacyRoutes(r23);	 Catch:{ Exception -> 0x0030 }
        L_0x02a2:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r0 = r22;
            r0 = r0.dnsServers;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            if (r22 == 0) goto L_0x02ca;
        L_0x02b4:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r0 = r22;
            r0 = r0.dnsServers;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.size();	 Catch:{ Exception -> 0x0030 }
            if (r22 != 0) goto L_0x02f5;
        L_0x02ca:
            r22 = 3;
            r22 = r18[r22];	 Catch:{ Exception -> 0x0030 }
            r10 = r22.trim();	 Catch:{ Exception -> 0x0030 }
            r22 = r10.isEmpty();	 Catch:{ Exception -> 0x0030 }
            if (r22 != 0) goto L_0x02f5;
        L_0x02d8:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r23 = " ";
            r0 = r23;
            r23 = r10.split(r0);	 Catch:{ Exception -> 0x0030 }
            r23 = java.util.Arrays.asList(r23);	 Catch:{ Exception -> 0x0030 }
            r0 = r23;
            r1 = r22;
            r1.dnsServers = r0;	 Catch:{ Exception -> 0x0030 }
        L_0x02f5:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r0 = r22;
            r0 = r0.searchDomains;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            if (r22 == 0) goto L_0x031d;
        L_0x0307:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r0 = r22;
            r0 = r0.searchDomains;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.size();	 Catch:{ Exception -> 0x0030 }
            if (r22 != 0) goto L_0x034a;
        L_0x031d:
            r22 = 4;
            r22 = r18[r22];	 Catch:{ Exception -> 0x0030 }
            r20 = r22.trim();	 Catch:{ Exception -> 0x0030 }
            r22 = r20.isEmpty();	 Catch:{ Exception -> 0x0030 }
            if (r22 != 0) goto L_0x034a;
        L_0x032b:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ Exception -> 0x0030 }
            r23 = " ";
            r0 = r20;
            r1 = r23;
            r23 = r0.split(r1);	 Catch:{ Exception -> 0x0030 }
            r23 = java.util.Arrays.asList(r23);	 Catch:{ Exception -> 0x0030 }
            r0 = r23;
            r1 = r22;
            r1.searchDomains = r0;	 Catch:{ Exception -> 0x0030 }
        L_0x034a:
            r22 = 5;
            r13 = r18[r22];	 Catch:{ Exception -> 0x0030 }
            r22 = r13.isEmpty();	 Catch:{ Exception -> 0x0030 }
            if (r22 != 0) goto L_0x0383;
        L_0x0354:
            r4 = java.net.InetAddress.parseNumericAddress(r13);	 Catch:{ IllegalArgumentException -> 0x0425 }
            r0 = r4 instanceof java.net.Inet4Address;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r22 = r0;
            if (r22 == 0) goto L_0x03f8;
        L_0x035e:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r0 = r22;
            r0 = r0.routes;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r22 = r0;
            r23 = new android.net.RouteInfo;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r24 = new android.net.IpPrefix;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r25 = 32;
            r0 = r24;
            r1 = r25;
            r0.<init>(r4, r1);	 Catch:{ IllegalArgumentException -> 0x0425 }
            r25 = 9;
            r23.<init>(r24, r25);	 Catch:{ IllegalArgumentException -> 0x0425 }
            r22.add(r23);	 Catch:{ IllegalArgumentException -> 0x0425 }
        L_0x0383:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ Exception -> 0x0030 }
            r23 = r0;
            monitor-enter(r23);	 Catch:{ Exception -> 0x0030 }
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ all -> 0x03f5 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ all -> 0x03f5 }
            r24 = android.os.SystemClock.elapsedRealtime();	 Catch:{ all -> 0x03f5 }
            r0 = r24;
            r2 = r22;
            r2.startTime = r0;	 Catch:{ all -> 0x03f5 }
            r22 = 0;
            r0 = r26;
            r1 = r22;
            r0.checkpoint(r1);	 Catch:{ all -> 0x03f5 }
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ all -> 0x03f5 }
            r22 = r0;
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            r24 = r24.mConfig;	 Catch:{ all -> 0x03f5 }
            r0 = r24;
            r0 = r0.interfaze;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            r0 = r22;
            r1 = r24;
            r22 = r0.jniCheck(r1);	 Catch:{ all -> 0x03f5 }
            if (r22 != 0) goto L_0x046f;
        L_0x03c7:
            r22 = new java.lang.IllegalStateException;	 Catch:{ all -> 0x03f5 }
            r24 = new java.lang.StringBuilder;	 Catch:{ all -> 0x03f5 }
            r24.<init>();	 Catch:{ all -> 0x03f5 }
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ all -> 0x03f5 }
            r25 = r0;
            r25 = r25.mConfig;	 Catch:{ all -> 0x03f5 }
            r0 = r25;
            r0 = r0.interfaze;	 Catch:{ all -> 0x03f5 }
            r25 = r0;
            r24 = r24.append(r25);	 Catch:{ all -> 0x03f5 }
            r25 = " is gone";
            r24 = r24.append(r25);	 Catch:{ all -> 0x03f5 }
            r24 = r24.toString();	 Catch:{ all -> 0x03f5 }
            r0 = r22;
            r1 = r24;
            r0.<init>(r1);	 Catch:{ all -> 0x03f5 }
            throw r22;	 Catch:{ all -> 0x03f5 }
        L_0x03f5:
            r22 = move-exception;
            monitor-exit(r23);	 Catch:{ Exception -> 0x0030 }
            throw r22;	 Catch:{ Exception -> 0x0030 }
        L_0x03f8:
            r0 = r4 instanceof java.net.Inet6Address;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r22 = r0;
            if (r22 == 0) goto L_0x0451;
        L_0x03fe:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r22 = r0;
            r22 = r22.mConfig;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r0 = r22;
            r0 = r0.routes;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r22 = r0;
            r23 = new android.net.RouteInfo;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r24 = new android.net.IpPrefix;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r25 = 128; // 0x80 float:1.794E-43 double:6.32E-322;
            r0 = r24;
            r1 = r25;
            r0.<init>(r4, r1);	 Catch:{ IllegalArgumentException -> 0x0425 }
            r25 = 9;
            r23.<init>(r24, r25);	 Catch:{ IllegalArgumentException -> 0x0425 }
            r22.add(r23);	 Catch:{ IllegalArgumentException -> 0x0425 }
            goto L_0x0383;
        L_0x0425:
            r12 = move-exception;
            r22 = "LegacyVpnRunner";
            r23 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0030 }
            r23.<init>();	 Catch:{ Exception -> 0x0030 }
            r24 = "Exception constructing throw route to ";
            r23 = r23.append(r24);	 Catch:{ Exception -> 0x0030 }
            r0 = r23;
            r23 = r0.append(r13);	 Catch:{ Exception -> 0x0030 }
            r24 = ": ";
            r23 = r23.append(r24);	 Catch:{ Exception -> 0x0030 }
            r0 = r23;
            r23 = r0.append(r12);	 Catch:{ Exception -> 0x0030 }
            r23 = r23.toString();	 Catch:{ Exception -> 0x0030 }
            android.util.Log.e(r22, r23);	 Catch:{ Exception -> 0x0030 }
            goto L_0x0383;
        L_0x0451:
            r22 = "LegacyVpnRunner";
            r23 = new java.lang.StringBuilder;	 Catch:{ IllegalArgumentException -> 0x0425 }
            r23.<init>();	 Catch:{ IllegalArgumentException -> 0x0425 }
            r24 = "Unknown IP address family for VPN endpoint: ";
            r23 = r23.append(r24);	 Catch:{ IllegalArgumentException -> 0x0425 }
            r0 = r23;
            r23 = r0.append(r13);	 Catch:{ IllegalArgumentException -> 0x0425 }
            r23 = r23.toString();	 Catch:{ IllegalArgumentException -> 0x0425 }
            android.util.Log.e(r22, r23);	 Catch:{ IllegalArgumentException -> 0x0425 }
            goto L_0x0383;
        L_0x046f:
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ all -> 0x03f5 }
            r22 = r0;
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            r24 = r24.mConfig;	 Catch:{ all -> 0x03f5 }
            r0 = r24;
            r0 = r0.interfaze;	 Catch:{ all -> 0x03f5 }
            r24 = r0;
            r0 = r22;
            r1 = r24;
            r0.mInterface = r1;	 Catch:{ all -> 0x03f5 }
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ all -> 0x03f5 }
            r22 = r0;
            r22.prepareStatusIntent();	 Catch:{ all -> 0x03f5 }
            r0 = r26;
            r0 = r0.this$0;	 Catch:{ all -> 0x03f5 }
            r22 = r0;
            r22.agentConnect();	 Catch:{ all -> 0x03f5 }
            r22 = "LegacyVpnRunner";
            r24 = "Connected!";
            r0 = r22;
            r1 = r24;
            android.util.Log.i(r0, r1);	 Catch:{ all -> 0x03f5 }
            monitor-exit(r23);	 Catch:{ Exception -> 0x0030 }
            goto L_0x0050;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.android.server.connectivity.Vpn.LegacyVpnRunner.execute():void");
        }

        private void monitorDaemons() throws InterruptedException {
            if (!this.this$0.mNetworkInfo.isConnected()) {
                return;
            }
            while (true) {
                Thread.sleep(2000);
                int i = 0;
                while (i < this.mDaemons.length) {
                    if (this.mArguments[i] == null || !SystemService.isStopped(this.mDaemons[i])) {
                        i++;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public static class SystemServices {
        private final Context mContext;

        public SystemServices(Context context) {
            this.mContext = context;
        }

        public PendingIntent pendingIntentGetActivityAsUser(Intent intent, int flags, UserHandle user) {
            return PendingIntent.getActivityAsUser(this.mContext, 0, intent, flags, null, user);
        }

        public void settingsSecurePutStringForUser(String key, String value, int userId) {
            Secure.putStringForUser(this.mContext.getContentResolver(), key, value, userId);
        }

        public void settingsSecurePutIntForUser(String key, int value, int userId) {
            Secure.putIntForUser(this.mContext.getContentResolver(), key, value, userId);
        }

        public String settingsSecureGetStringForUser(String key, int userId) {
            return Secure.getStringForUser(this.mContext.getContentResolver(), key, userId);
        }

        public int settingsSecureGetIntForUser(String key, int def, int userId) {
            return Secure.getIntForUser(this.mContext.getContentResolver(), key, def, userId);
        }
    }

    private native boolean jniAddAddress(String str, String str2, int i);

    private native int jniCheck(String str);

    private native int jniCreate(int i);

    private native boolean jniDelAddress(String str, String str2, int i);

    private native String jniGetName(int i);

    private native void jniReset(String str);

    private native int jniSetAddresses(String str, String str2);

    public Vpn(Looper looper, Context context, INetworkManagementService netService, int userHandle) {
        this(looper, context, netService, userHandle, new SystemServices(context));
    }

    protected Vpn(Looper looper, Context context, INetworkManagementService netService, int userHandle, SystemServices systemServices) {
        this.mEnableTeardown = true;
        this.mAlwaysOn = false;
        this.mLockdown = false;
        this.mVpnUsers = null;
        this.mBlockedUsers = new ArraySet();
        this.mPackageIntentReceiver = new BroadcastReceiver() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void onReceive(android.content.Context r9, android.content.Intent r10) {
                /*
                r8 = this;
                r1 = r10.getData();
                if (r1 != 0) goto L_0x000a;
            L_0x0006:
                r3 = 0;
            L_0x0007:
                if (r3 != 0) goto L_0x000f;
            L_0x0009:
                return;
            L_0x000a:
                r3 = r1.getSchemeSpecificPart();
                goto L_0x0007;
            L_0x000f:
                r5 = com.android.server.connectivity.Vpn.this;
                monitor-enter(r5);
                r4 = com.android.server.connectivity.Vpn.this;	 Catch:{ all -> 0x0087 }
                r4 = r4.getAlwaysOnPackage();	 Catch:{ all -> 0x0087 }
                r4 = r3.equals(r4);	 Catch:{ all -> 0x0087 }
                if (r4 != 0) goto L_0x0020;
            L_0x001e:
                monitor-exit(r5);
                return;
            L_0x0020:
                r0 = r10.getAction();	 Catch:{ all -> 0x0087 }
                r4 = "Vpn";
                r6 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0087 }
                r6.<init>();	 Catch:{ all -> 0x0087 }
                r7 = "Received broadcast ";
                r6 = r6.append(r7);	 Catch:{ all -> 0x0087 }
                r6 = r6.append(r0);	 Catch:{ all -> 0x0087 }
                r7 = " for always-on package ";
                r6 = r6.append(r7);	 Catch:{ all -> 0x0087 }
                r6 = r6.append(r3);	 Catch:{ all -> 0x0087 }
                r7 = " in user ";
                r6 = r6.append(r7);	 Catch:{ all -> 0x0087 }
                r7 = com.android.server.connectivity.Vpn.this;	 Catch:{ all -> 0x0087 }
                r7 = r7.mUserHandle;	 Catch:{ all -> 0x0087 }
                r6 = r6.append(r7);	 Catch:{ all -> 0x0087 }
                r6 = r6.toString();	 Catch:{ all -> 0x0087 }
                android.util.Log.i(r4, r6);	 Catch:{ all -> 0x0087 }
                r4 = "android.intent.action.PACKAGE_REPLACED";
                r4 = r0.equals(r4);	 Catch:{ all -> 0x0087 }
                if (r4 == 0) goto L_0x006a;
            L_0x0063:
                r4 = com.android.server.connectivity.Vpn.this;	 Catch:{ all -> 0x0087 }
                r4.startAlwaysOnVpn();	 Catch:{ all -> 0x0087 }
            L_0x0068:
                monitor-exit(r5);
                return;
            L_0x006a:
                r4 = "android.intent.action.PACKAGE_REMOVED";
                r4 = r0.equals(r4);	 Catch:{ all -> 0x0087 }
                if (r4 == 0) goto L_0x0068;
            L_0x0073:
                r4 = "android.intent.extra.REPLACING";
                r6 = 0;
                r4 = r10.getBooleanExtra(r4, r6);	 Catch:{ all -> 0x0087 }
                r2 = r4 ^ 1;
                if (r2 == 0) goto L_0x0068;
            L_0x007f:
                r4 = com.android.server.connectivity.Vpn.this;	 Catch:{ all -> 0x0087 }
                r6 = 0;
                r7 = 0;
                r4.setAlwaysOnPackage(r6, r7);	 Catch:{ all -> 0x0087 }
                goto L_0x0068;
            L_0x0087:
                r4 = move-exception;
                monitor-exit(r5);
                throw r4;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.android.server.connectivity.Vpn.1.onReceive(android.content.Context, android.content.Intent):void");
            }
        };
        this.mIsPackageIntentReceiverRegistered = false;
        this.mObserver = new BaseNetworkObserver() {
            public void interfaceStatusChanged(String interfaze, boolean up) {
                synchronized (Vpn.this) {
                    if (!up) {
                        if (Vpn.this.mLegacyVpnRunner != null) {
                            Vpn.this.mLegacyVpnRunner.check(interfaze);
                        }
                    }
                }
            }

            public void interfaceRemoved(String interfaze) {
                synchronized (Vpn.this) {
                    if (interfaze.equals(Vpn.this.mInterface) && Vpn.this.jniCheck(interfaze) == 0) {
                        Vpn.this.mStatusIntent = null;
                        Vpn.this.mVpnUsers = null;
                        Vpn.this.mConfig = null;
                        Vpn.this.mInterface = null;
                        if (Vpn.this.mConnection != null) {
                            Vpn.this.mContext.unbindService(Vpn.this.mConnection);
                            Vpn.this.mConnection = null;
                            Vpn.this.agentDisconnect();
                        } else if (Vpn.this.mLegacyVpnRunner != null) {
                            Vpn.this.mLegacyVpnRunner.exit();
                            Vpn.this.mLegacyVpnRunner = null;
                        }
                    }
                }
            }
        };
        this.mContext = context;
        this.mNetd = netService;
        this.mUserHandle = userHandle;
        this.mLooper = looper;
        this.mSystemServices = systemServices;
        this.mPackage = "[Legacy VPN]";
        this.mOwnerUID = getAppUid(this.mPackage, this.mUserHandle);
        try {
            netService.registerObserver(this.mObserver);
        } catch (RemoteException e) {
            Log.wtf(TAG, "Problem registering observer", e);
        }
        this.mNetworkInfo = new NetworkInfo(17, 0, NETWORKTYPE, "");
        this.mNetworkCapabilities = new NetworkCapabilities();
        this.mNetworkCapabilities.addTransportType(4);
        this.mNetworkCapabilities.removeCapability(15);
        loadAlwaysOnPackage();
    }

    public void setEnableTeardown(boolean enableTeardown) {
        this.mEnableTeardown = enableTeardown;
    }

    protected void updateState(DetailedState detailedState, String reason) {
        Log.d(TAG, "setting state=" + detailedState + ", reason=" + reason);
        this.mNetworkInfo.setDetailedState(detailedState, reason, null);
        if (this.mNetworkAgent != null) {
            this.mNetworkAgent.sendNetworkInfo(this.mNetworkInfo);
        }
        updateAlwaysOnNotification(detailedState);
    }

    public synchronized void setLockdown(boolean lockdown) {
        enforceControlPermissionOrInternalCaller();
        setVpnForcedLocked(lockdown);
        this.mLockdown = lockdown;
        if (this.mAlwaysOn) {
            saveAlwaysOnPackage();
        }
    }

    public boolean isAlwaysOnPackageSupported(String packageName) {
        enforceSettingsPermission();
        if (packageName == null) {
            return false;
        }
        PackageManager pm = this.mContext.getPackageManager();
        ApplicationInfo appInfo = null;
        try {
            appInfo = pm.getApplicationInfoAsUser(packageName, 0, this.mUserHandle);
        } catch (NameNotFoundException e) {
            Log.w(TAG, "Can't find \"" + packageName + "\" when checking always-on support");
        }
        if (appInfo == null || appInfo.targetSdkVersion < 24) {
            return false;
        }
        Intent intent = new Intent("android.net.VpnService");
        intent.setPackage(packageName);
        List<ResolveInfo> services = pm.queryIntentServicesAsUser(intent, 128, this.mUserHandle);
        if (services == null || services.size() == 0) {
            return false;
        }
        for (ResolveInfo rInfo : services) {
            Bundle metaData = rInfo.serviceInfo.metaData;
            if (metaData != null && (metaData.getBoolean("android.net.VpnService.SUPPORTS_ALWAYS_ON", true) ^ 1) != 0) {
                return false;
            }
        }
        return true;
    }

    public synchronized boolean setAlwaysOnPackage(String packageName, boolean lockdown) {
        enforceControlPermissionOrInternalCaller();
        if (!setAlwaysOnPackageInternal(packageName, lockdown)) {
            return false;
        }
        saveAlwaysOnPackage();
        return true;
    }

    @GuardedBy("this")
    private boolean setAlwaysOnPackageInternal(String packageName, boolean lockdown) {
        if ("[Legacy VPN]".equals(packageName)) {
            Log.w(TAG, "Not setting legacy VPN \"" + packageName + "\" as always-on.");
            return false;
        }
        if (packageName == null) {
            packageName = "[Legacy VPN]";
            this.mAlwaysOn = false;
        } else if (!setPackageAuthorization(packageName, true)) {
            return false;
        } else {
            this.mAlwaysOn = true;
        }
        if (!this.mAlwaysOn) {
            lockdown = false;
        }
        this.mLockdown = lockdown;
        if (isCurrentPreparedPackage(packageName)) {
            updateAlwaysOnNotification(this.mNetworkInfo.getDetailedState());
        } else {
            prepareInternal(packageName);
        }
        maybeRegisterPackageChangeReceiverLocked(packageName);
        setVpnForcedLocked(this.mLockdown);
        return true;
    }

    private static boolean isNullOrLegacyVpn(String packageName) {
        return packageName != null ? "[Legacy VPN]".equals(packageName) : true;
    }

    private void unregisterPackageChangeReceiverLocked() {
        if (this.mIsPackageIntentReceiverRegistered) {
            this.mContext.unregisterReceiver(this.mPackageIntentReceiver);
            this.mIsPackageIntentReceiverRegistered = false;
        }
    }

    private void maybeRegisterPackageChangeReceiverLocked(String packageName) {
        unregisterPackageChangeReceiverLocked();
        if (!isNullOrLegacyVpn(packageName)) {
            this.mIsPackageIntentReceiverRegistered = true;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
            intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
            intentFilter.addDataScheme("package");
            intentFilter.addDataSchemeSpecificPart(packageName, 0);
            this.mContext.registerReceiverAsUser(this.mPackageIntentReceiver, UserHandle.of(this.mUserHandle), intentFilter, null, null);
        }
    }

    public synchronized String getAlwaysOnPackage() {
        enforceControlPermissionOrInternalCaller();
        return this.mAlwaysOn ? this.mPackage : null;
    }

    @GuardedBy("this")
    private void saveAlwaysOnPackage() {
        long token = Binder.clearCallingIdentity();
        try {
            this.mSystemServices.settingsSecurePutStringForUser("always_on_vpn_app", getAlwaysOnPackage(), this.mUserHandle);
            SystemServices systemServices = this.mSystemServices;
            String str = "always_on_vpn_lockdown";
            int i = (this.mAlwaysOn && this.mLockdown) ? 1 : 0;
            systemServices.settingsSecurePutIntForUser(str, i, this.mUserHandle);
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    @GuardedBy("this")
    private void loadAlwaysOnPackage() {
        long token = Binder.clearCallingIdentity();
        try {
            setAlwaysOnPackageInternal(this.mSystemServices.settingsSecureGetStringForUser("always_on_vpn_app", this.mUserHandle), this.mSystemServices.settingsSecureGetIntForUser("always_on_vpn_lockdown", 0, this.mUserHandle) != 0);
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean startAlwaysOnVpn() {
        /*
        r14 = this;
        r12 = 1;
        r13 = 0;
        monitor-enter(r14);
        r3 = r14.getAlwaysOnPackage();	 Catch:{ all -> 0x005f }
        if (r3 != 0) goto L_0x000b;
    L_0x0009:
        monitor-exit(r14);
        return r12;
    L_0x000b:
        r2 = r14.isAlwaysOnPackageSupported(r3);	 Catch:{ all -> 0x005f }
        if (r2 != 0) goto L_0x0018;
    L_0x0011:
        r2 = 0;
        r4 = 0;
        r14.setAlwaysOnPackage(r2, r4);	 Catch:{ all -> 0x005f }
        monitor-exit(r14);
        return r13;
    L_0x0018:
        r2 = r14.getNetworkInfo();	 Catch:{ all -> 0x005f }
        r2 = r2.isConnected();	 Catch:{ all -> 0x005f }
        if (r2 == 0) goto L_0x0024;
    L_0x0022:
        monitor-exit(r14);
        return r12;
    L_0x0024:
        monitor-exit(r14);
        r10 = android.os.Binder.clearCallingIdentity();
        r2 = com.android.server.DeviceIdleController.LocalService.class;
        r1 = com.android.server.LocalServices.getService(r2);	 Catch:{ all -> 0x008a }
        r1 = (com.android.server.DeviceIdleController.LocalService) r1;	 Catch:{ all -> 0x008a }
        r2 = android.os.Process.myUid();	 Catch:{ all -> 0x008a }
        r6 = r14.mUserHandle;	 Catch:{ all -> 0x008a }
        r8 = "vpn";
        r4 = 60000; // 0xea60 float:8.4078E-41 double:2.9644E-319;
        r7 = 0;
        r1.addPowerSaveTempWhitelistApp(r2, r3, r4, r6, r7, r8);	 Catch:{ all -> 0x008a }
        r9 = new android.content.Intent;	 Catch:{ all -> 0x008a }
        r2 = "android.net.VpnService";
        r9.<init>(r2);	 Catch:{ all -> 0x008a }
        r9.setPackage(r3);	 Catch:{ all -> 0x008a }
        r2 = r14.mContext;	 Catch:{ RuntimeException -> 0x0064 }
        r4 = r14.mUserHandle;	 Catch:{ RuntimeException -> 0x0064 }
        r4 = android.os.UserHandle.of(r4);	 Catch:{ RuntimeException -> 0x0064 }
        r2 = r2.startServiceAsUser(r9, r4);	 Catch:{ RuntimeException -> 0x0064 }
        if (r2 == 0) goto L_0x0062;
    L_0x005a:
        r2 = r12;
    L_0x005b:
        android.os.Binder.restoreCallingIdentity(r10);
        return r2;
    L_0x005f:
        r2 = move-exception;
        monitor-exit(r14);
        throw r2;
    L_0x0062:
        r2 = r13;
        goto L_0x005b;
    L_0x0064:
        r0 = move-exception;
        r2 = "Vpn";
        r4 = new java.lang.StringBuilder;	 Catch:{ all -> 0x008a }
        r4.<init>();	 Catch:{ all -> 0x008a }
        r5 = "VpnService ";
        r4 = r4.append(r5);	 Catch:{ all -> 0x008a }
        r4 = r4.append(r9);	 Catch:{ all -> 0x008a }
        r5 = " failed to start";
        r4 = r4.append(r5);	 Catch:{ all -> 0x008a }
        r4 = r4.toString();	 Catch:{ all -> 0x008a }
        android.util.Log.e(r2, r4, r0);	 Catch:{ all -> 0x008a }
        android.os.Binder.restoreCallingIdentity(r10);
        return r13;
    L_0x008a:
        r2 = move-exception;
        android.os.Binder.restoreCallingIdentity(r10);
        throw r2;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.connectivity.Vpn.startAlwaysOnVpn():boolean");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean prepare(java.lang.String r4, java.lang.String r5) {
        /*
        r3 = this;
        r2 = 1;
        r1 = 0;
        monitor-enter(r3);
        if (r4 == 0) goto L_0x0048;
    L_0x0005:
        r0 = r3.mAlwaysOn;	 Catch:{ all -> 0x0071 }
        if (r0 == 0) goto L_0x0013;
    L_0x0009:
        r0 = r3.isCurrentPreparedPackage(r4);	 Catch:{ all -> 0x0071 }
        r0 = r0 ^ 1;
        if (r0 == 0) goto L_0x0013;
    L_0x0011:
        monitor-exit(r3);
        return r1;
    L_0x0013:
        r0 = r3.isCurrentPreparedPackage(r4);	 Catch:{ all -> 0x0071 }
        if (r0 != 0) goto L_0x002f;
    L_0x0019:
        r0 = "[Legacy VPN]";
        r0 = r4.equals(r0);	 Catch:{ all -> 0x0071 }
        if (r0 != 0) goto L_0x002d;
    L_0x0022:
        r0 = r3.isVpnUserPreConsented(r4);	 Catch:{ all -> 0x0071 }
        if (r0 == 0) goto L_0x002d;
    L_0x0028:
        r3.prepareInternal(r4);	 Catch:{ all -> 0x0071 }
        monitor-exit(r3);
        return r2;
    L_0x002d:
        monitor-exit(r3);
        return r1;
    L_0x002f:
        r0 = "[Legacy VPN]";
        r0 = r4.equals(r0);	 Catch:{ all -> 0x0071 }
        if (r0 != 0) goto L_0x0048;
    L_0x0038:
        r0 = r3.isVpnUserPreConsented(r4);	 Catch:{ all -> 0x0071 }
        r0 = r0 ^ 1;
        if (r0 == 0) goto L_0x0048;
    L_0x0040:
        r0 = "[Legacy VPN]";
        r3.prepareInternal(r0);	 Catch:{ all -> 0x0071 }
        monitor-exit(r3);
        return r1;
    L_0x0048:
        if (r5 == 0) goto L_0x0059;
    L_0x004a:
        r0 = "[Legacy VPN]";
        r0 = r5.equals(r0);	 Catch:{ all -> 0x0071 }
        if (r0 != 0) goto L_0x005b;
    L_0x0053:
        r0 = r3.isCurrentPreparedPackage(r5);	 Catch:{ all -> 0x0071 }
        if (r0 == 0) goto L_0x005b;
    L_0x0059:
        monitor-exit(r3);
        return r2;
    L_0x005b:
        r3.enforceControlPermission();	 Catch:{ all -> 0x0071 }
        r0 = r3.mAlwaysOn;	 Catch:{ all -> 0x0071 }
        if (r0 == 0) goto L_0x006c;
    L_0x0062:
        r0 = r3.isCurrentPreparedPackage(r5);	 Catch:{ all -> 0x0071 }
        r0 = r0 ^ 1;
        if (r0 == 0) goto L_0x006c;
    L_0x006a:
        monitor-exit(r3);
        return r1;
    L_0x006c:
        r3.prepareInternal(r5);	 Catch:{ all -> 0x0071 }
        monitor-exit(r3);
        return r2;
    L_0x0071:
        r0 = move-exception;
        monitor-exit(r3);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.connectivity.Vpn.prepare(java.lang.String, java.lang.String):boolean");
    }

    private boolean isCurrentPreparedPackage(String packageName) {
        return getAppUid(packageName, this.mUserHandle) == this.mOwnerUID;
    }

    private void prepareInternal(String newPackage) {
        long token = Binder.clearCallingIdentity();
        if (this.mInterface != null) {
            this.mStatusIntent = null;
            agentDisconnect();
            jniReset(this.mInterface);
            this.mInterface = null;
            this.mVpnUsers = null;
        }
        if (this.mConnection != null) {
            try {
                this.mConnection.mService.transact(UsbAudioDevice.kAudioDeviceClassMask, Parcel.obtain(), null, 1);
            } catch (Exception e) {
            }
            try {
                this.mContext.unbindService(this.mConnection);
                this.mConnection = null;
            } catch (Exception e2) {
                Log.wtf(TAG, "Failed to disallow UID " + this.mOwnerUID + " to call protect() " + e2);
            } catch (Throwable th) {
                Binder.restoreCallingIdentity(token);
            }
        } else if (this.mLegacyVpnRunner != null) {
            this.mLegacyVpnRunner.exit();
            this.mLegacyVpnRunner = null;
        }
        this.mNetd.denyProtect(this.mOwnerUID);
        Log.i(TAG, "Switched from " + this.mPackage + " to " + newPackage);
        this.mPackage = newPackage;
        this.mOwnerUID = getAppUid(newPackage, this.mUserHandle);
        try {
            this.mNetd.allowProtect(this.mOwnerUID);
        } catch (Exception e22) {
            Log.wtf(TAG, "Failed to allow UID " + this.mOwnerUID + " to call protect() " + e22);
        }
        this.mConfig = null;
        updateState(DetailedState.IDLE, "prepare");
        setVpnForcedLocked(this.mLockdown);
        Binder.restoreCallingIdentity(token);
    }

    public boolean setPackageAuthorization(String packageName, boolean authorized) {
        boolean z = true;
        enforceControlPermissionOrInternalCaller();
        int uid = getAppUid(packageName, this.mUserHandle);
        if (uid == -1 || "[Legacy VPN]".equals(packageName)) {
            return false;
        }
        long token = Binder.clearCallingIdentity();
        try {
            int i;
            AppOpsManager appOps = (AppOpsManager) this.mContext.getSystemService("appops");
            if (authorized) {
                i = 0;
            } else {
                i = 1;
            }
            appOps.setMode(47, uid, packageName, i);
            return z;
        } catch (Exception e) {
            String str = TAG;
            z = "Failed to set app ops for package " + packageName + ", uid " + uid;
            Log.wtf(str, z, e);
            return false;
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    private boolean isVpnUserPreConsented(String packageName) {
        if (((AppOpsManager) this.mContext.getSystemService("appops")).noteOpNoThrow(47, Binder.getCallingUid(), packageName) == 0) {
            return true;
        }
        return false;
    }

    private int getAppUid(String app, int userHandle) {
        if ("[Legacy VPN]".equals(app)) {
            return Process.myUid();
        }
        int result;
        try {
            result = this.mContext.getPackageManager().getPackageUidAsUser(app, userHandle);
        } catch (NameNotFoundException e) {
            result = -1;
        }
        return result;
    }

    public NetworkInfo getNetworkInfo() {
        return this.mNetworkInfo;
    }

    public int getNetId() {
        return this.mNetworkAgent != null ? this.mNetworkAgent.netId : 0;
    }

    private LinkProperties makeLinkProperties() {
        boolean allowIPv4 = this.mConfig.allowIPv4;
        boolean allowIPv6 = this.mConfig.allowIPv6;
        LinkProperties lp = new LinkProperties();
        lp.setInterfaceName(this.mInterface);
        if (this.mConfig.addresses != null) {
            for (LinkAddress address : this.mConfig.addresses) {
                lp.addLinkAddress(address);
                allowIPv4 |= address.getAddress() instanceof Inet4Address;
                allowIPv6 |= address.getAddress() instanceof Inet6Address;
            }
        }
        if (this.mConfig.routes != null) {
            for (RouteInfo route : this.mConfig.routes) {
                lp.addRoute(route);
                InetAddress address2 = route.getDestination().getAddress();
                allowIPv4 |= address2 instanceof Inet4Address;
                allowIPv6 |= address2 instanceof Inet6Address;
            }
        }
        if (this.mConfig.dnsServers != null) {
            for (String dnsServer : this.mConfig.dnsServers) {
                address2 = InetAddress.parseNumericAddress(dnsServer);
                lp.addDnsServer(address2);
                allowIPv4 |= address2 instanceof Inet4Address;
                allowIPv6 |= address2 instanceof Inet6Address;
            }
        }
        if (!allowIPv4) {
            lp.addRoute(new RouteInfo(new IpPrefix(Inet4Address.ANY, 0), 7));
        }
        if (!allowIPv6) {
            lp.addRoute(new RouteInfo(new IpPrefix(Inet6Address.ANY, 0), 7));
        }
        StringBuilder buffer = new StringBuilder();
        if (this.mConfig.searchDomains != null) {
            for (String domain : this.mConfig.searchDomains) {
                buffer.append(domain).append(' ');
            }
        }
        lp.setDomains(buffer.toString().trim());
        return lp;
    }

    private void agentConnect() {
        boolean z = false;
        LinkProperties lp = makeLinkProperties();
        if (lp.hasIPv4DefaultRoute() || lp.hasIPv6DefaultRoute()) {
            this.mNetworkCapabilities.addCapability(12);
        } else {
            this.mNetworkCapabilities.removeCapability(12);
        }
        this.mNetworkInfo.setDetailedState(DetailedState.CONNECTING, null, null);
        NetworkMisc networkMisc = new NetworkMisc();
        if (this.mConfig.allowBypass) {
            z = this.mLockdown ^ 1;
        }
        networkMisc.allowBypass = z;
        long token = Binder.clearCallingIdentity();
        try {
            this.mNetworkAgent = new NetworkAgent(this.mLooper, this.mContext, NETWORKTYPE, this.mNetworkInfo, this.mNetworkCapabilities, lp, 0, networkMisc) {
                public void unwanted() {
                }
            };
            this.mVpnUsers = createUserAndRestrictedProfilesRanges(this.mUserHandle, this.mConfig.allowedApplications, this.mConfig.disallowedApplications);
            this.mNetworkAgent.addUidRanges((UidRange[]) this.mVpnUsers.toArray(new UidRange[this.mVpnUsers.size()]));
            this.mNetworkInfo.setIsAvailable(true);
            updateState(DetailedState.CONNECTED, "agentConnect");
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    private boolean canHaveRestrictedProfile(int userId) {
        long token = Binder.clearCallingIdentity();
        try {
            boolean canHaveRestrictedProfile = UserManager.get(this.mContext).canHaveRestrictedProfile(userId);
            return canHaveRestrictedProfile;
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    private void agentDisconnect(NetworkAgent networkAgent) {
        if (networkAgent != null) {
            NetworkInfo networkInfo = new NetworkInfo(this.mNetworkInfo);
            networkInfo.setIsAvailable(false);
            networkInfo.setDetailedState(DetailedState.DISCONNECTED, null, null);
            networkAgent.sendNetworkInfo(networkInfo);
        }
    }

    private void agentDisconnect() {
        if (this.mNetworkInfo.isConnected()) {
            this.mNetworkInfo.setIsAvailable(false);
            updateState(DetailedState.DISCONNECTED, "agentDisconnect");
            this.mNetworkAgent = null;
        }
    }

    public synchronized ParcelFileDescriptor establish(VpnConfig config) {
        UserManager mgr = UserManager.get(this.mContext);
        if (Binder.getCallingUid() != this.mOwnerUID) {
            return null;
        }
        if (!isVpnUserPreConsented(this.mPackage)) {
            return null;
        }
        Intent intent = new Intent("android.net.VpnService");
        intent.setClassName(this.mPackage, config.user);
        long token = Binder.clearCallingIdentity();
        try {
            if (mgr.getUserInfo(this.mUserHandle).isRestricted()) {
                throw new SecurityException("Restricted users cannot establish VPNs");
            }
            ResolveInfo info = AppGlobals.getPackageManager().resolveService(intent, null, 0, this.mUserHandle);
            if (info == null) {
                throw new SecurityException("Cannot find " + config.user);
            } else if ("android.permission.BIND_VPN_SERVICE".equals(info.serviceInfo.permission)) {
                Binder.restoreCallingIdentity(token);
                VpnConfig oldConfig = this.mConfig;
                String oldInterface = this.mInterface;
                Connection oldConnection = this.mConnection;
                NetworkAgent oldNetworkAgent = this.mNetworkAgent;
                this.mNetworkAgent = null;
                Set<UidRange> oldUsers = this.mVpnUsers;
                ParcelFileDescriptor tun = ParcelFileDescriptor.adoptFd(jniCreate(config.mtu));
                try {
                    updateState(DetailedState.CONNECTING, "establish");
                    String interfaze = jniGetName(tun.getFd());
                    StringBuilder builder = new StringBuilder();
                    for (LinkAddress address : config.addresses) {
                        builder.append(" ").append(address);
                    }
                    if (jniSetAddresses(interfaze, builder.toString()) < 1) {
                        throw new IllegalArgumentException("At least one address must be specified");
                    }
                    Connection connection = new Connection();
                    if (this.mContext.bindServiceAsUser(intent, connection, 67108865, new UserHandle(this.mUserHandle))) {
                        this.mConnection = connection;
                        this.mInterface = interfaze;
                        config.user = this.mPackage;
                        config.interfaze = this.mInterface;
                        config.startTime = SystemClock.elapsedRealtime();
                        this.mConfig = config;
                        agentConnect();
                        if (oldConnection != null) {
                            this.mContext.unbindService(oldConnection);
                        }
                        agentDisconnect(oldNetworkAgent);
                        if (!(oldInterface == null || (oldInterface.equals(interfaze) ^ 1) == 0)) {
                            jniReset(oldInterface);
                        }
                        IoUtils.setBlocking(tun.getFileDescriptor(), config.blocking);
                        Log.i(TAG, "Established by " + config.user + " on " + this.mInterface);
                        return tun;
                    }
                    throw new IllegalStateException("Cannot bind " + config.user);
                } catch (IOException e) {
                    throw new IllegalStateException("Cannot set tunnel's fd as blocking=" + config.blocking, e);
                } catch (RuntimeException e2) {
                    IoUtils.closeQuietly(tun);
                    agentDisconnect();
                    this.mConfig = oldConfig;
                    this.mConnection = oldConnection;
                    this.mVpnUsers = oldUsers;
                    this.mNetworkAgent = oldNetworkAgent;
                    this.mInterface = oldInterface;
                    throw e2;
                }
            } else {
                throw new SecurityException(config.user + " does not require " + "android.permission.BIND_VPN_SERVICE");
            }
        } catch (RemoteException e3) {
            throw new SecurityException("Cannot find " + config.user);
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(token);
        }
    }

    private boolean isRunningLocked() {
        return (this.mNetworkAgent == null || this.mInterface == null) ? false : true;
    }

    private boolean isCallerEstablishedOwnerLocked() {
        return isRunningLocked() && Binder.getCallingUid() == this.mOwnerUID;
    }

    private SortedSet<Integer> getAppsUids(List<String> packageNames, int userHandle) {
        SortedSet<Integer> uids = new TreeSet();
        for (String app : packageNames) {
            int uid = getAppUid(app, userHandle);
            if (uid != -1) {
                uids.add(Integer.valueOf(uid));
            }
        }
        return uids;
    }

    Set<UidRange> createUserAndRestrictedProfilesRanges(int userHandle, List<String> allowedApplications, List<String> disallowedApplications) {
        Set<UidRange> ranges = new ArraySet();
        addUserToRanges(ranges, userHandle, allowedApplications, disallowedApplications);
        if (canHaveRestrictedProfile(userHandle)) {
            long token = Binder.clearCallingIdentity();
            try {
                List<UserInfo> users = UserManager.get(this.mContext).getUsers(true);
                for (UserInfo user : users) {
                    if (user.isRestricted() && user.restrictedProfileParentId == userHandle) {
                        addUserToRanges(ranges, user.id, allowedApplications, disallowedApplications);
                    }
                }
            } finally {
                Binder.restoreCallingIdentity(token);
            }
        }
        return ranges;
    }

    void addUserToRanges(Set<UidRange> ranges, int userHandle, List<String> allowedApplications, List<String> disallowedApplications) {
        int start;
        int uid;
        if (allowedApplications != null) {
            start = -1;
            int stop = -1;
            for (Integer intValue : getAppsUids(allowedApplications, userHandle)) {
                uid = intValue.intValue();
                if (start == -1) {
                    start = uid;
                } else if (uid != stop + 1) {
                    ranges.add(new UidRange(start, stop));
                    start = uid;
                }
                stop = uid;
            }
            if (start != -1) {
                ranges.add(new UidRange(start, stop));
            }
        } else if (disallowedApplications != null) {
            UidRange userRange = UidRange.createForUser(userHandle);
            start = userRange.start;
            for (Integer intValue2 : getAppsUids(disallowedApplications, userHandle)) {
                uid = intValue2.intValue();
                if (uid == start) {
                    start++;
                } else {
                    ranges.add(new UidRange(start, uid - 1));
                    start = uid + 1;
                }
            }
            if (start <= userRange.stop) {
                ranges.add(new UidRange(start, userRange.stop));
            }
        } else {
            ranges.add(UidRange.createForUser(userHandle));
        }
    }

    private List<UidRange> uidRangesForUser(int userHandle) {
        UidRange userRange = UidRange.createForUser(userHandle);
        List<UidRange> ranges = new ArrayList();
        for (UidRange range : this.mVpnUsers) {
            if (userRange.containsRange(range)) {
                ranges.add(range);
            }
        }
        return ranges;
    }

    private void removeVpnUserLocked(int userHandle) {
        if (this.mVpnUsers == null) {
            throw new IllegalStateException("VPN is not active");
        }
        List<UidRange> ranges = uidRangesForUser(userHandle);
        if (this.mNetworkAgent != null) {
            this.mNetworkAgent.removeUidRanges((UidRange[]) ranges.toArray(new UidRange[ranges.size()]));
        }
        this.mVpnUsers.removeAll(ranges);
    }

    public void onUserAdded(int userHandle) {
        UserInfo user = UserManager.get(this.mContext).getUserInfo(userHandle);
        if (user.isRestricted() && user.restrictedProfileParentId == this.mUserHandle) {
            synchronized (this) {
                if (this.mVpnUsers != null) {
                    try {
                        addUserToRanges(this.mVpnUsers, userHandle, this.mConfig.allowedApplications, this.mConfig.disallowedApplications);
                        if (this.mNetworkAgent != null) {
                            List<UidRange> ranges = uidRangesForUser(userHandle);
                            this.mNetworkAgent.addUidRanges((UidRange[]) ranges.toArray(new UidRange[ranges.size()]));
                        }
                    } catch (Exception e) {
                        Log.wtf(TAG, "Failed to add restricted user to owner", e);
                    }
                }
                setVpnForcedLocked(this.mLockdown);
            }
        }
    }

    public void onUserRemoved(int userHandle) {
        UserInfo user = UserManager.get(this.mContext).getUserInfo(userHandle);
        if (user.isRestricted() && user.restrictedProfileParentId == this.mUserHandle) {
            synchronized (this) {
                if (this.mVpnUsers != null) {
                    try {
                        removeVpnUserLocked(userHandle);
                    } catch (Exception e) {
                        Log.wtf(TAG, "Failed to remove restricted user to owner", e);
                    }
                }
                setVpnForcedLocked(this.mLockdown);
            }
        }
    }

    public synchronized void onUserStopped() {
        setLockdown(false);
        this.mAlwaysOn = false;
        unregisterPackageChangeReceiverLocked();
        agentDisconnect();
    }

    @GuardedBy("this")
    private void setVpnForcedLocked(boolean enforce) {
        setVpnForcedWithExemptionsLocked(enforce, isNullOrLegacyVpn(this.mPackage) ? null : Collections.singletonList(this.mPackage));
    }

    @GuardedBy("this")
    private void setVpnForcedWithExemptionsLocked(boolean enforce, List<String> exemptedPackages) {
        Set<UidRange> removedRanges = new ArraySet(this.mBlockedUsers);
        Set<UidRange> addedRanges = Collections.emptySet();
        if (enforce) {
            addedRanges = createUserAndRestrictedProfilesRanges(this.mUserHandle, null, exemptedPackages);
            removedRanges.removeAll(addedRanges);
            addedRanges.removeAll(this.mBlockedUsers);
        }
        setAllowOnlyVpnForUids(false, removedRanges);
        setAllowOnlyVpnForUids(true, addedRanges);
    }

    @GuardedBy("this")
    private boolean setAllowOnlyVpnForUids(boolean enforce, Collection<UidRange> ranges) {
        if (ranges.size() == 0) {
            return true;
        }
        try {
            this.mNetd.setAllowOnlyVpnForUids(enforce, (UidRange[]) ranges.toArray(new UidRange[ranges.size()]));
            if (enforce) {
                this.mBlockedUsers.addAll(ranges);
            } else {
                this.mBlockedUsers.removeAll(ranges);
            }
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Updating blocked=" + enforce + " for UIDs " + Arrays.toString(ranges.toArray()) + " failed", e);
            return false;
        }
    }

    public VpnConfig getVpnConfig() {
        enforceControlPermission();
        return this.mConfig;
    }

    @Deprecated
    public synchronized void interfaceStatusChanged(String iface, boolean up) {
        try {
            this.mObserver.interfaceStatusChanged(iface, up);
        } catch (RemoteException e) {
        }
    }

    private void enforceControlPermission() {
        this.mContext.enforceCallingPermission("android.permission.CONTROL_VPN", "Unauthorized Caller");
    }

    private void enforceControlPermissionOrInternalCaller() {
        this.mContext.enforceCallingOrSelfPermission("android.permission.CONTROL_VPN", "Unauthorized Caller");
    }

    private void enforceSettingsPermission() {
        this.mContext.enforceCallingOrSelfPermission("android.permission.NETWORK_SETTINGS", "Unauthorized Caller");
    }

    private void prepareStatusIntent() {
        long token = Binder.clearCallingIdentity();
        try {
            this.mStatusIntent = VpnConfig.getIntentForStatusPanel(this.mContext);
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    public synchronized boolean addAddress(String address, int prefixLength) {
        if (!isCallerEstablishedOwnerLocked()) {
            return false;
        }
        boolean success = jniAddAddress(this.mInterface, address, prefixLength);
        this.mNetworkAgent.sendLinkProperties(makeLinkProperties());
        return success;
    }

    public synchronized boolean removeAddress(String address, int prefixLength) {
        if (!isCallerEstablishedOwnerLocked()) {
            return false;
        }
        boolean success = jniDelAddress(this.mInterface, address, prefixLength);
        this.mNetworkAgent.sendLinkProperties(makeLinkProperties());
        return success;
    }

    public synchronized boolean setUnderlyingNetworks(Network[] networks) {
        if (!isCallerEstablishedOwnerLocked()) {
            return false;
        }
        if (networks == null) {
            this.mConfig.underlyingNetworks = null;
        } else {
            this.mConfig.underlyingNetworks = new Network[networks.length];
            for (int i = 0; i < networks.length; i++) {
                if (networks[i] == null) {
                    this.mConfig.underlyingNetworks[i] = null;
                } else {
                    this.mConfig.underlyingNetworks[i] = new Network(networks[i].netId);
                }
            }
        }
        return true;
    }

    public synchronized Network[] getUnderlyingNetworks() {
        if (!isRunningLocked()) {
            return null;
        }
        return this.mConfig.underlyingNetworks;
    }

    public synchronized VpnInfo getVpnInfo() {
        if (!isRunningLocked()) {
            return null;
        }
        VpnInfo info = new VpnInfo();
        info.ownerUid = this.mOwnerUID;
        info.vpnIface = this.mInterface;
        return info;
    }

    public synchronized boolean appliesToUid(int uid) {
        if (!isRunningLocked()) {
            return false;
        }
        for (UidRange uidRange : this.mVpnUsers) {
            if (uidRange.contains(uid)) {
                return true;
            }
        }
        return false;
    }

    public synchronized boolean isBlockingUid(int uid) {
        if (!this.mLockdown) {
            return false;
        }
        if (this.mNetworkInfo.isConnected()) {
            return appliesToUid(uid) ^ 1;
        }
        for (UidRange uidRange : this.mBlockedUsers) {
            if (uidRange.contains(uid)) {
                return true;
            }
        }
        return false;
    }

    private void updateAlwaysOnNotification(DetailedState networkState) {
        boolean visible = this.mAlwaysOn && networkState != DetailedState.CONNECTED;
        UserHandle user = UserHandle.of(this.mUserHandle);
        long token = Binder.clearCallingIdentity();
        try {
            NotificationManager notificationManager = NotificationManager.from(this.mContext);
            if (visible) {
                notificationManager.notifyAsUser(TAG, 17, new Builder(this.mContext, SystemNotificationChannels.VPN).setSmallIcon(17303680).setContentTitle(this.mContext.getString(17040949)).setContentText(this.mContext.getString(17040946)).setContentIntent(this.mSystemServices.pendingIntentGetActivityAsUser(new Intent("android.settings.VPN_SETTINGS"), 201326592, user)).setCategory("sys").setVisibility(1).setOngoing(true).setColor(this.mContext.getColor(17170763)).build(), user);
                Binder.restoreCallingIdentity(token);
                return;
            }
            notificationManager.cancelAsUser(TAG, 17, user);
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    private static RouteInfo findIPv4DefaultRoute(LinkProperties prop) {
        for (RouteInfo route : prop.getAllRoutes()) {
            if (route.isDefaultRoute() && (route.getGateway() instanceof Inet4Address)) {
                return route;
            }
        }
        throw new IllegalStateException("Unable to find IPv4 default gateway");
    }

    public void startLegacyVpn(VpnProfile profile, KeyStore keyStore, LinkProperties egress) {
        enforceControlPermission();
        long token = Binder.clearCallingIdentity();
        try {
            startLegacyVpnPrivileged(profile, keyStore, egress);
        } finally {
            Binder.restoreCallingIdentity(token);
        }
    }

    public void startLegacyVpnPrivileged(VpnProfile profile, KeyStore keyStore, LinkProperties egress) {
        UserManager mgr = UserManager.get(this.mContext);
        if (mgr.getUserInfo(this.mUserHandle).isRestricted() || mgr.hasUserRestriction("no_config_vpn", new UserHandle(this.mUserHandle))) {
            throw new SecurityException("Restricted users cannot establish VPNs");
        }
        RouteInfo ipv4DefaultRoute = findIPv4DefaultRoute(egress);
        String gateway = ipv4DefaultRoute.getGateway().getHostAddress();
        String iface = ipv4DefaultRoute.getInterface();
        String privateKey = "";
        String userCert = "";
        String caCert = "";
        String serverCert = "";
        if (!profile.ipsecUserCert.isEmpty()) {
            privateKey = "USRPKEY_" + profile.ipsecUserCert;
            byte[] value = keyStore.get("USRCERT_" + profile.ipsecUserCert);
            userCert = value == null ? null : new String(value, StandardCharsets.UTF_8);
        }
        if (!profile.ipsecCaCert.isEmpty()) {
            value = keyStore.get("CACERT_" + profile.ipsecCaCert);
            caCert = value == null ? null : new String(value, StandardCharsets.UTF_8);
        }
        if (!profile.ipsecServerCert.isEmpty()) {
            value = keyStore.get("USRCERT_" + profile.ipsecServerCert);
            serverCert = value == null ? null : new String(value, StandardCharsets.UTF_8);
        }
        if (privateKey == null || userCert == null || caCert == null || serverCert == null) {
            throw new IllegalStateException("Cannot load credentials");
        }
        String[] racoon = null;
        switch (profile.type) {
            case 1:
                racoon = new String[]{iface, profile.server, "udppsk", profile.ipsecIdentifier, profile.ipsecSecret, "1701"};
                break;
            case 2:
                racoon = new String[]{iface, profile.server, "udprsa", privateKey, userCert, caCert, serverCert, "1701"};
                break;
            case 3:
                racoon = new String[]{iface, profile.server, "xauthpsk", profile.ipsecIdentifier, profile.ipsecSecret, profile.username, profile.password, "", gateway};
                break;
            case 4:
                racoon = new String[]{iface, profile.server, "xauthrsa", privateKey, userCert, caCert, serverCert, profile.username, profile.password, "", gateway};
                break;
            case 5:
                racoon = new String[]{iface, profile.server, "hybridrsa", caCert, serverCert, profile.username, profile.password, "", gateway};
                break;
        }
        String[] mtpd = null;
        switch (profile.type) {
            case 0:
                mtpd = new String[20];
                mtpd[0] = iface;
                mtpd[1] = "pptp";
                mtpd[2] = profile.server;
                mtpd[3] = "1723";
                mtpd[4] = "name";
                mtpd[5] = profile.username;
                mtpd[6] = "password";
                mtpd[7] = profile.password;
                mtpd[8] = "linkname";
                mtpd[9] = "vpn";
                mtpd[10] = "refuse-eap";
                mtpd[11] = "nodefaultroute";
                mtpd[12] = "usepeerdns";
                mtpd[13] = "idle";
                mtpd[14] = "1800";
                mtpd[15] = "mtu";
                mtpd[16] = "1400";
                mtpd[17] = "mru";
                mtpd[18] = "1400";
                mtpd[19] = profile.mppe ? "+mppe" : "nomppe";
                break;
            case 1:
            case 2:
                mtpd = new String[]{iface, "l2tp", profile.server, "1701", profile.l2tpSecret, "name", profile.username, "password", profile.password, "linkname", "vpn", "refuse-eap", "nodefaultroute", "usepeerdns", "idle", "1800", "mtu", "1400", "mru", "1400"};
                break;
        }
        VpnConfig config = new VpnConfig();
        config.legacy = true;
        config.user = profile.key;
        config.interfaze = iface;
        config.session = profile.name;
        config.addLegacyRoutes(profile.routes);
        if (!profile.dnsServers.isEmpty()) {
            config.dnsServers = Arrays.asList(profile.dnsServers.split(" +"));
        }
        if (!profile.searchDomains.isEmpty()) {
            config.searchDomains = Arrays.asList(profile.searchDomains.split(" +"));
        }
        startLegacyVpn(config, racoon, mtpd);
    }

    private synchronized void startLegacyVpn(VpnConfig config, String[] racoon, String[] mtpd) {
        stopLegacyVpnPrivileged();
        prepareInternal("[Legacy VPN]");
        updateState(DetailedState.CONNECTING, "startLegacyVpn");
        this.mLegacyVpnRunner = new LegacyVpnRunner(this, config, racoon, mtpd);
        this.mLegacyVpnRunner.start();
    }

    public synchronized void stopLegacyVpnPrivileged() {
        if (this.mLegacyVpnRunner != null) {
            this.mLegacyVpnRunner.exit();
            this.mLegacyVpnRunner = null;
            synchronized ("LegacyVpnRunner") {
            }
        }
    }

    public synchronized LegacyVpnInfo getLegacyVpnInfo() {
        enforceControlPermission();
        return getLegacyVpnInfoPrivileged();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.android.internal.net.LegacyVpnInfo getLegacyVpnInfoPrivileged() {
        /*
        r3 = this;
        r2 = 0;
        monitor-enter(r3);
        r1 = r3.mLegacyVpnRunner;	 Catch:{ all -> 0x0029 }
        if (r1 != 0) goto L_0x0008;
    L_0x0006:
        monitor-exit(r3);
        return r2;
    L_0x0008:
        r0 = new com.android.internal.net.LegacyVpnInfo;	 Catch:{ all -> 0x0029 }
        r0.<init>();	 Catch:{ all -> 0x0029 }
        r1 = r3.mConfig;	 Catch:{ all -> 0x0029 }
        r1 = r1.user;	 Catch:{ all -> 0x0029 }
        r0.key = r1;	 Catch:{ all -> 0x0029 }
        r1 = r3.mNetworkInfo;	 Catch:{ all -> 0x0029 }
        r1 = com.android.internal.net.LegacyVpnInfo.stateFromNetworkInfo(r1);	 Catch:{ all -> 0x0029 }
        r0.state = r1;	 Catch:{ all -> 0x0029 }
        r1 = r3.mNetworkInfo;	 Catch:{ all -> 0x0029 }
        r1 = r1.isConnected();	 Catch:{ all -> 0x0029 }
        if (r1 == 0) goto L_0x0027;
    L_0x0023:
        r1 = r3.mStatusIntent;	 Catch:{ all -> 0x0029 }
        r0.intent = r1;	 Catch:{ all -> 0x0029 }
    L_0x0027:
        monitor-exit(r3);
        return r0;
    L_0x0029:
        r1 = move-exception;
        monitor-exit(r3);
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.server.connectivity.Vpn.getLegacyVpnInfoPrivileged():com.android.internal.net.LegacyVpnInfo");
    }

    public VpnConfig getLegacyVpnConfig() {
        if (this.mLegacyVpnRunner != null) {
            return this.mConfig;
        }
        return null;
    }
}
