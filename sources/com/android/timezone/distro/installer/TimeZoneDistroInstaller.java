package com.android.timezone.distro.installer;

import android.util.Slog;
import com.android.timezone.distro.DistroException;
import com.android.timezone.distro.DistroVersion;
import com.android.timezone.distro.FileUtils;
import com.android.timezone.distro.StagedDistroOperation;
import com.android.timezone.distro.TimeZoneDistro;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import libcore.util.ZoneInfoDB.TzData;

public class TimeZoneDistroInstaller {
    private static final String CURRENT_TZ_DATA_DIR_NAME = "current";
    public static final int INSTALL_FAIL_BAD_DISTRO_FORMAT_VERSION = 2;
    public static final int INSTALL_FAIL_BAD_DISTRO_STRUCTURE = 1;
    public static final int INSTALL_FAIL_RULES_TOO_OLD = 3;
    public static final int INSTALL_FAIL_VALIDATION_ERROR = 4;
    public static final int INSTALL_SUCCESS = 0;
    private static final String OLD_TZ_DATA_DIR_NAME = "old";
    private static final String STAGED_TZ_DATA_DIR_NAME = "staged";
    public static final int UNINSTALL_FAIL = 2;
    public static final int UNINSTALL_NOTHING_INSTALLED = 1;
    public static final int UNINSTALL_SUCCESS = 0;
    public static final String UNINSTALL_TOMBSTONE_FILE_NAME = "STAGED_UNINSTALL_TOMBSTONE";
    private static final String WORKING_DIR_NAME = "working";
    private final File currentTzDataDir;
    private final String logTag;
    private final File oldStagedDataDir;
    private final File stagedTzDataDir;
    private final File systemTzDataFile;
    private final File workingDir;

    public TimeZoneDistroInstaller(String logTag, File systemTzDataFile, File installDir) {
        this.logTag = logTag;
        this.systemTzDataFile = systemTzDataFile;
        this.oldStagedDataDir = new File(installDir, OLD_TZ_DATA_DIR_NAME);
        this.stagedTzDataDir = new File(installDir, STAGED_TZ_DATA_DIR_NAME);
        this.currentTzDataDir = new File(installDir, CURRENT_TZ_DATA_DIR_NAME);
        this.workingDir = new File(installDir, WORKING_DIR_NAME);
    }

    File getOldStagedDataDir() {
        return this.oldStagedDataDir;
    }

    File getStagedTzDataDir() {
        return this.stagedTzDataDir;
    }

    File getCurrentTzDataDir() {
        return this.currentTzDataDir;
    }

    File getWorkingDir() {
        return this.workingDir;
    }

    public boolean install(TimeZoneDistro distro) throws IOException {
        if (stageInstallWithErrorCode(distro) == 0) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int stageInstallWithErrorCode(com.android.timezone.distro.TimeZoneDistro r13) throws java.io.IOException {
        /*
        r12 = this;
        r11 = 4;
        r10 = 1;
        r7 = r12.oldStagedDataDir;
        r7 = r7.exists();
        if (r7 == 0) goto L_0x000f;
    L_0x000a:
        r7 = r12.oldStagedDataDir;
        com.android.timezone.distro.FileUtils.deleteRecursive(r7);
    L_0x000f:
        r7 = r12.workingDir;
        r7 = r7.exists();
        if (r7 == 0) goto L_0x001c;
    L_0x0017:
        r7 = r12.workingDir;
        com.android.timezone.distro.FileUtils.deleteRecursive(r7);
    L_0x001c:
        r7 = r12.logTag;
        r8 = "Unpacking / verifying time zone update";
        android.util.Slog.i(r7, r8);
        r7 = r12.workingDir;	 Catch:{ all -> 0x0183 }
        r12.unpackDistro(r13, r7);	 Catch:{ all -> 0x0183 }
        r7 = r12.workingDir;	 Catch:{ DistroException -> 0x0044 }
        r0 = r12.readDistroVersion(r7);	 Catch:{ DistroException -> 0x0044 }
        if (r0 != 0) goto L_0x006d;
    L_0x0031:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = "Update not applied: Distro version could not be loaded";
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r7);
        r7 = r12.workingDir;
        r12.deleteBestEffort(r7);
        return r10;
    L_0x0044:
        r1 = move-exception;
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Invalid distro version: ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = r1.getMessage();	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r7);
        r7 = r12.workingDir;
        r12.deleteBestEffort(r7);
        return r10;
    L_0x006d:
        r7 = com.android.timezone.distro.DistroVersion.isCompatibleWithThisDevice(r0);	 Catch:{ all -> 0x0183 }
        if (r7 != 0) goto L_0x0098;
    L_0x0073:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Update not applied: Distro format version check failed: ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r0);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = 2;
        r8 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r8);
        r8 = r12.workingDir;
        r12.deleteBestEffort(r8);
        return r7;
    L_0x0098:
        r7 = r12.workingDir;	 Catch:{ all -> 0x0183 }
        r7 = r12.checkDistroDataFilesExist(r7);	 Catch:{ all -> 0x0183 }
        if (r7 != 0) goto L_0x00b3;
    L_0x00a0:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = "Update not applied: Distro is missing required data file(s)";
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r7);
        r7 = r12.workingDir;
        r12.deleteBestEffort(r7);
        return r10;
    L_0x00b3:
        r7 = r12.systemTzDataFile;	 Catch:{ all -> 0x0183 }
        r7 = r12.checkDistroRulesNewerThanSystem(r7, r0);	 Catch:{ all -> 0x0183 }
        if (r7 != 0) goto L_0x00cf;
    L_0x00bb:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = "Update not applied: Distro rules version check failed";
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = 3;
        r8 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r8);
        r8 = r12.workingDir;
        r12.deleteBestEffort(r8);
        return r7;
    L_0x00cf:
        r6 = new java.io.File;	 Catch:{ all -> 0x0183 }
        r7 = r12.workingDir;	 Catch:{ all -> 0x0183 }
        r8 = "tzdata";
        r6.<init>(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r6.getPath();	 Catch:{ all -> 0x0183 }
        r4 = libcore.util.ZoneInfoDB.TzData.loadTzData(r7);	 Catch:{ all -> 0x0183 }
        if (r4 != 0) goto L_0x010e;
    L_0x00e3:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Update not applied: ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r6);	 Catch:{ all -> 0x0183 }
        r9 = " could not be loaded";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r7);
        r7 = r12.workingDir;
        r12.deleteBestEffort(r7);
        return r11;
    L_0x010e:
        r4.validate();	 Catch:{ IOException -> 0x014f }
        r4.close();	 Catch:{ all -> 0x0183 }
        r5 = new java.io.File;	 Catch:{ all -> 0x0183 }
        r7 = r12.workingDir;	 Catch:{ all -> 0x0183 }
        r8 = "tzlookup.xml";
        r5.<init>(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r5.exists();	 Catch:{ all -> 0x0183 }
        if (r7 != 0) goto L_0x018f;
    L_0x0124:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Update not applied: ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r5);	 Catch:{ all -> 0x0183 }
        r9 = " does not exist";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r7);
        r7 = r12.workingDir;
        r12.deleteBestEffort(r7);
        return r10;
    L_0x014f:
        r2 = move-exception;
        r7 = r12.logTag;	 Catch:{ all -> 0x017e }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x017e }
        r8.<init>();	 Catch:{ all -> 0x017e }
        r9 = "Update not applied: ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x017e }
        r8 = r8.append(r6);	 Catch:{ all -> 0x017e }
        r9 = " failed validation";
        r8 = r8.append(r9);	 Catch:{ all -> 0x017e }
        r8 = r8.toString();	 Catch:{ all -> 0x017e }
        android.util.Slog.i(r7, r8, r2);	 Catch:{ all -> 0x017e }
        r4.close();	 Catch:{ all -> 0x0183 }
        r7 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r7);
        r7 = r12.workingDir;
        r12.deleteBestEffort(r7);
        return r11;
    L_0x017e:
        r7 = move-exception;
        r4.close();	 Catch:{ all -> 0x0183 }
        throw r7;	 Catch:{ all -> 0x0183 }
    L_0x0183:
        r7 = move-exception;
        r8 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r8);
        r8 = r12.workingDir;
        r12.deleteBestEffort(r8);
        throw r7;
    L_0x018f:
        r7 = r5.getPath();	 Catch:{ IOException -> 0x0227 }
        r3 = libcore.util.TimeZoneFinder.createInstance(r7);	 Catch:{ IOException -> 0x0227 }
        r3.validate();	 Catch:{ IOException -> 0x0227 }
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = "Applying time zone update";
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.workingDir;	 Catch:{ all -> 0x0183 }
        com.android.timezone.distro.FileUtils.makeDirectoryWorldAccessible(r7);	 Catch:{ all -> 0x0183 }
        r7 = r12.stagedTzDataDir;	 Catch:{ all -> 0x0183 }
        r7 = r7.exists();	 Catch:{ all -> 0x0183 }
        if (r7 != 0) goto L_0x0253;
    L_0x01af:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Nothing to unstage at ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = r12.stagedTzDataDir;	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
    L_0x01ca:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Moving ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = r12.workingDir;	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = " to ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = r12.stagedTzDataDir;	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.workingDir;	 Catch:{ all -> 0x0183 }
        r8 = r12.stagedTzDataDir;	 Catch:{ all -> 0x0183 }
        com.android.timezone.distro.FileUtils.rename(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Install staged: ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = r12.stagedTzDataDir;	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = " successfully created";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = 0;
        r8 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r8);
        r8 = r12.workingDir;
        r12.deleteBestEffort(r8);
        return r7;
    L_0x0227:
        r2 = move-exception;
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Update not applied: ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r5);	 Catch:{ all -> 0x0183 }
        r9 = " failed validation";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8, r2);	 Catch:{ all -> 0x0183 }
        r7 = r12.oldStagedDataDir;
        r12.deleteBestEffort(r7);
        r7 = r12.workingDir;
        r12.deleteBestEffort(r7);
        return r11;
    L_0x0253:
        r7 = r12.logTag;	 Catch:{ all -> 0x0183 }
        r8 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0183 }
        r8.<init>();	 Catch:{ all -> 0x0183 }
        r9 = "Moving ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = r12.stagedTzDataDir;	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = " to ";
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r9 = r12.oldStagedDataDir;	 Catch:{ all -> 0x0183 }
        r8 = r8.append(r9);	 Catch:{ all -> 0x0183 }
        r8 = r8.toString();	 Catch:{ all -> 0x0183 }
        android.util.Slog.i(r7, r8);	 Catch:{ all -> 0x0183 }
        r7 = r12.stagedTzDataDir;	 Catch:{ all -> 0x0183 }
        r8 = r12.oldStagedDataDir;	 Catch:{ all -> 0x0183 }
        com.android.timezone.distro.FileUtils.rename(r7, r8);	 Catch:{ all -> 0x0183 }
        goto L_0x01ca;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.timezone.distro.installer.TimeZoneDistroInstaller.stageInstallWithErrorCode(com.android.timezone.distro.TimeZoneDistro):int");
    }

    public int stageUninstall() throws IOException {
        Slog.i(this.logTag, "Uninstalling time zone update");
        if (this.oldStagedDataDir.exists()) {
            FileUtils.deleteRecursive(this.oldStagedDataDir);
        }
        if (this.workingDir.exists()) {
            FileUtils.deleteRecursive(this.workingDir);
        }
        try {
            if (this.stagedTzDataDir.exists()) {
                Slog.i(this.logTag, "Moving " + this.stagedTzDataDir + " to " + this.oldStagedDataDir);
                FileUtils.rename(this.stagedTzDataDir, this.oldStagedDataDir);
            } else {
                Slog.i(this.logTag, "Nothing to unstage at " + this.stagedTzDataDir);
            }
            if (this.currentTzDataDir.exists()) {
                FileUtils.ensureDirectoriesExist(this.workingDir, true);
                FileUtils.createEmptyFile(new File(this.workingDir, UNINSTALL_TOMBSTONE_FILE_NAME));
                Slog.i(this.logTag, "Moving " + this.workingDir + " to " + this.stagedTzDataDir);
                FileUtils.rename(this.workingDir, this.stagedTzDataDir);
                Slog.i(this.logTag, "Uninstall staged: " + this.stagedTzDataDir + " successfully created");
                deleteBestEffort(this.oldStagedDataDir);
                deleteBestEffort(this.workingDir);
                return 0;
            }
            Slog.i(this.logTag, "Nothing to uninstall at " + this.currentTzDataDir);
            return 1;
        } finally {
            deleteBestEffort(this.oldStagedDataDir);
            deleteBestEffort(this.workingDir);
        }
    }

    public DistroVersion getInstalledDistroVersion() throws DistroException, IOException {
        if (this.currentTzDataDir.exists()) {
            return readDistroVersion(this.currentTzDataDir);
        }
        return null;
    }

    public StagedDistroOperation getStagedDistroOperation() throws DistroException, IOException {
        if (!this.stagedTzDataDir.exists()) {
            return null;
        }
        if (new File(this.stagedTzDataDir, UNINSTALL_TOMBSTONE_FILE_NAME).exists()) {
            return StagedDistroOperation.uninstall();
        }
        return StagedDistroOperation.install(readDistroVersion(this.stagedTzDataDir));
    }

    public String getSystemRulesVersion() throws IOException {
        return readSystemRulesVersion(this.systemTzDataFile);
    }

    private void deleteBestEffort(File dir) {
        if (dir.exists()) {
            Slog.i(this.logTag, "Deleting " + dir);
            try {
                FileUtils.deleteRecursive(dir);
            } catch (IOException e) {
                Slog.w(this.logTag, "Unable to delete " + dir, e);
            }
        }
    }

    private void unpackDistro(TimeZoneDistro distro, File targetDir) throws IOException {
        Slog.i(this.logTag, "Unpacking update content to: " + targetDir);
        distro.extractTo(targetDir);
    }

    private boolean checkDistroDataFilesExist(File unpackedContentDir) throws IOException {
        Slog.i(this.logTag, "Verifying distro contents");
        return FileUtils.filesExist(unpackedContentDir, TimeZoneDistro.TZDATA_FILE_NAME, TimeZoneDistro.ICU_DATA_FILE_NAME);
    }

    private DistroVersion readDistroVersion(File distroDir) throws DistroException, IOException {
        Slog.d(this.logTag, "Reading distro format version: " + distroDir);
        File distroVersionFile = new File(distroDir, TimeZoneDistro.DISTRO_VERSION_FILE_NAME);
        if (distroVersionFile.exists()) {
            return DistroVersion.fromBytes(FileUtils.readBytes(distroVersionFile, DistroVersion.DISTRO_VERSION_FILE_LENGTH));
        }
        throw new DistroException("No distro version file found: " + distroVersionFile);
    }

    private boolean checkDistroRulesNewerThanSystem(File systemTzDataFile, DistroVersion distroVersion) throws IOException {
        Slog.i(this.logTag, "Reading /system rules version");
        String systemRulesVersion = readSystemRulesVersion(systemTzDataFile);
        String distroRulesVersion = distroVersion.rulesVersion;
        boolean canApply = distroRulesVersion.compareTo(systemRulesVersion) >= 0;
        if (canApply) {
            Slog.i(this.logTag, "Passed rules version check: distroRulesVersion=" + distroRulesVersion + ", systemRulesVersion=" + systemRulesVersion);
        } else {
            Slog.i(this.logTag, "Failed rules version check: distroRulesVersion=" + distroRulesVersion + ", systemRulesVersion=" + systemRulesVersion);
        }
        return canApply;
    }

    private String readSystemRulesVersion(File systemTzDataFile) throws IOException {
        if (systemTzDataFile.exists()) {
            return TzData.getRulesVersion(systemTzDataFile);
        }
        Slog.i(this.logTag, "tzdata file cannot be found in /system");
        throw new FileNotFoundException("system tzdata does not exist: " + systemTzDataFile);
    }
}
