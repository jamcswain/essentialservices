package com.android.timezone.distro;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public final class TimeZoneDistro {
    private static final int BUFFER_SIZE = 8192;
    public static final String DISTRO_VERSION_FILE_NAME = "distro_version";
    public static final String FILE_NAME = "distro.zip";
    public static final String ICU_DATA_FILE_NAME = "icu/icu_tzdata.dat";
    private static final long MAX_GET_ENTRY_CONTENTS_SIZE = 131072;
    public static final String TZDATA_FILE_NAME = "tzdata";
    public static final String TZLOOKUP_FILE_NAME = "tzlookup.xml";
    private final InputStream inputStream;

    public TimeZoneDistro(byte[] bytes) {
        this(new ByteArrayInputStream(bytes));
    }

    public TimeZoneDistro(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public DistroVersion getDistroVersion() throws DistroException, IOException {
        byte[] contents = getEntryContents(this.inputStream, DISTRO_VERSION_FILE_NAME);
        if (contents != null) {
            return DistroVersion.fromBytes(contents);
        }
        throw new DistroException("Distro version file entry not found");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] getEntryContents(java.io.InputStream r17, java.lang.String r18) throws java.io.IOException {
        /*
        r12 = 0;
        r8 = 0;
        r9 = new java.util.zip.ZipInputStream;	 Catch:{ Throwable -> 0x00ce, all -> 0x00cb }
        r0 = r17;
        r9.<init>(r0);	 Catch:{ Throwable -> 0x00ce, all -> 0x00cb }
    L_0x0009:
        r6 = r9.getNextEntry();	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        if (r6 == 0) goto L_0x00b3;
    L_0x000f:
        r7 = r6.getName();	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r0 = r18;
        r10 = r0.equals(r7);	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        if (r10 == 0) goto L_0x0009;
    L_0x001b:
        r10 = r6.getSize();	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r14 = 131072; // 0x20000 float:1.83671E-40 double:6.47582E-319;
        r10 = (r10 > r14 ? 1 : (r10 == r14 ? 0 : -1));
        if (r10 <= 0) goto L_0x0062;
    L_0x0026:
        r10 = new java.io.IOException;	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r11 = new java.lang.StringBuilder;	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r11.<init>();	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r13 = "Entry ";
        r11 = r11.append(r13);	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r0 = r18;
        r11 = r11.append(r0);	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r13 = " too large: ";
        r11 = r11.append(r13);	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r14 = r6.getSize();	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r11 = r11.append(r14);	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r11 = r11.toString();	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r10.<init>(r11);	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        throw r10;	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
    L_0x0051:
        r10 = move-exception;
        r8 = r9;
    L_0x0053:
        throw r10;	 Catch:{ all -> 0x0054 }
    L_0x0054:
        r11 = move-exception;
        r16 = r11;
        r11 = r10;
        r10 = r16;
    L_0x005a:
        if (r8 == 0) goto L_0x005f;
    L_0x005c:
        r8.close();	 Catch:{ Throwable -> 0x00bf }
    L_0x005f:
        if (r11 == 0) goto L_0x00ca;
    L_0x0061:
        throw r11;
    L_0x0062:
        r10 = 8192; // 0x2000 float:1.14794E-41 double:4.0474E-320;
        r4 = new byte[r10];	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        r11 = 0;
        r2 = 0;
        r3 = new java.io.ByteArrayOutputStream;	 Catch:{ Throwable -> 0x00d5, all -> 0x00d0 }
        r3.<init>();	 Catch:{ Throwable -> 0x00d5, all -> 0x00d0 }
    L_0x006d:
        r5 = r9.read(r4);	 Catch:{ Throwable -> 0x0079, all -> 0x00d2 }
        r10 = -1;
        if (r5 == r10) goto L_0x008e;
    L_0x0074:
        r10 = 0;
        r3.write(r4, r10, r5);	 Catch:{ Throwable -> 0x0079, all -> 0x00d2 }
        goto L_0x006d;
    L_0x0079:
        r10 = move-exception;
        r2 = r3;
    L_0x007b:
        throw r10;	 Catch:{ all -> 0x007c }
    L_0x007c:
        r11 = move-exception;
        r16 = r11;
        r11 = r10;
        r10 = r16;
    L_0x0082:
        if (r2 == 0) goto L_0x0087;
    L_0x0084:
        r2.close();	 Catch:{ Throwable -> 0x00a7, all -> 0x008a }
    L_0x0087:
        if (r11 == 0) goto L_0x00b2;
    L_0x0089:
        throw r11;	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
    L_0x008a:
        r10 = move-exception;
        r8 = r9;
        r11 = r12;
        goto L_0x005a;
    L_0x008e:
        r10 = r3.toByteArray();	 Catch:{ Throwable -> 0x0079, all -> 0x00d2 }
        if (r3 == 0) goto L_0x0097;
    L_0x0094:
        r3.close();	 Catch:{ Throwable -> 0x009a, all -> 0x008a }
    L_0x0097:
        if (r11 == 0) goto L_0x009c;
    L_0x0099:
        throw r11;	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
    L_0x009a:
        r11 = move-exception;
        goto L_0x0097;
    L_0x009c:
        if (r9 == 0) goto L_0x00a1;
    L_0x009e:
        r9.close();	 Catch:{ Throwable -> 0x00a4 }
    L_0x00a1:
        if (r12 == 0) goto L_0x00a6;
    L_0x00a3:
        throw r12;
    L_0x00a4:
        r12 = move-exception;
        goto L_0x00a1;
    L_0x00a6:
        return r10;
    L_0x00a7:
        r13 = move-exception;
        if (r11 != 0) goto L_0x00ac;
    L_0x00aa:
        r11 = r13;
        goto L_0x0087;
    L_0x00ac:
        if (r11 == r13) goto L_0x0087;
    L_0x00ae:
        r11.addSuppressed(r13);	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
        goto L_0x0087;
    L_0x00b2:
        throw r10;	 Catch:{ Throwable -> 0x0051, all -> 0x008a }
    L_0x00b3:
        r10 = 0;
        if (r9 == 0) goto L_0x00b9;
    L_0x00b6:
        r9.close();	 Catch:{ Throwable -> 0x00bc }
    L_0x00b9:
        if (r12 == 0) goto L_0x00be;
    L_0x00bb:
        throw r12;
    L_0x00bc:
        r12 = move-exception;
        goto L_0x00b9;
    L_0x00be:
        return r10;
    L_0x00bf:
        r12 = move-exception;
        if (r11 != 0) goto L_0x00c4;
    L_0x00c2:
        r11 = r12;
        goto L_0x005f;
    L_0x00c4:
        if (r11 == r12) goto L_0x005f;
    L_0x00c6:
        r11.addSuppressed(r12);
        goto L_0x005f;
    L_0x00ca:
        throw r10;
    L_0x00cb:
        r10 = move-exception;
        r11 = r12;
        goto L_0x005a;
    L_0x00ce:
        r10 = move-exception;
        goto L_0x0053;
    L_0x00d0:
        r10 = move-exception;
        goto L_0x0082;
    L_0x00d2:
        r10 = move-exception;
        r2 = r3;
        goto L_0x0082;
    L_0x00d5:
        r10 = move-exception;
        goto L_0x007b;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.timezone.distro.TimeZoneDistro.getEntryContents(java.io.InputStream, java.lang.String):byte[]");
    }

    public void extractTo(File targetDir) throws IOException {
        extractZipSafely(this.inputStream, targetDir, true);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void extractZipSafely(java.io.InputStream r15, java.io.File r16, boolean r17) throws java.io.IOException {
        /*
        com.android.timezone.distro.FileUtils.ensureDirectoriesExist(r16, r17);
        r12 = 0;
        r8 = 0;
        r9 = new java.util.zip.ZipInputStream;	 Catch:{ Throwable -> 0x00b0, all -> 0x00ad }
        r9.<init>(r15);	 Catch:{ Throwable -> 0x00b0, all -> 0x00ad }
        r10 = 8192; // 0x2000 float:1.14794E-41 double:4.0474E-320;
        r1 = new byte[r10];	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
    L_0x000e:
        r3 = r9.getNextEntry();	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        if (r3 == 0) goto L_0x0096;
    L_0x0014:
        r7 = r3.getName();	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        r0 = r16;
        r4 = com.android.timezone.distro.FileUtils.createSubFile(r0, r7);	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        r10 = r3.isDirectory();	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        if (r10 == 0) goto L_0x0039;
    L_0x0024:
        r0 = r17;
        com.android.timezone.distro.FileUtils.ensureDirectoriesExist(r4, r0);	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        goto L_0x000e;
    L_0x002a:
        r10 = move-exception;
        r8 = r9;
    L_0x002c:
        throw r10;	 Catch:{ all -> 0x002d }
    L_0x002d:
        r11 = move-exception;
        r14 = r11;
        r11 = r10;
        r10 = r14;
    L_0x0031:
        if (r8 == 0) goto L_0x0036;
    L_0x0033:
        r8.close();	 Catch:{ Throwable -> 0x00a0 }
    L_0x0036:
        if (r11 == 0) goto L_0x00ab;
    L_0x0038:
        throw r11;
    L_0x0039:
        r10 = r4.getParentFile();	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        r10 = r10.exists();	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        if (r10 != 0) goto L_0x004c;
    L_0x0043:
        r10 = r4.getParentFile();	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        r0 = r17;
        com.android.timezone.distro.FileUtils.ensureDirectoriesExist(r10, r0);	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
    L_0x004c:
        r11 = 0;
        r5 = 0;
        r6 = new java.io.FileOutputStream;	 Catch:{ Throwable -> 0x00b8, all -> 0x00b3 }
        r6.<init>(r4);	 Catch:{ Throwable -> 0x00b8, all -> 0x00b3 }
    L_0x0053:
        r2 = r9.read(r1);	 Catch:{ Throwable -> 0x005f, all -> 0x00b5 }
        r10 = -1;
        if (r2 == r10) goto L_0x0072;
    L_0x005a:
        r10 = 0;
        r6.write(r1, r10, r2);	 Catch:{ Throwable -> 0x005f, all -> 0x00b5 }
        goto L_0x0053;
    L_0x005f:
        r10 = move-exception;
        r5 = r6;
    L_0x0061:
        throw r10;	 Catch:{ all -> 0x0062 }
    L_0x0062:
        r11 = move-exception;
        r14 = r11;
        r11 = r10;
        r10 = r14;
    L_0x0066:
        if (r5 == 0) goto L_0x006b;
    L_0x0068:
        r5.close();	 Catch:{ Throwable -> 0x0083, all -> 0x006e }
    L_0x006b:
        if (r11 == 0) goto L_0x008e;
    L_0x006d:
        throw r11;	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
    L_0x006e:
        r10 = move-exception;
        r8 = r9;
        r11 = r12;
        goto L_0x0031;
    L_0x0072:
        r10 = r6.getFD();	 Catch:{ Throwable -> 0x005f, all -> 0x00b5 }
        r10.sync();	 Catch:{ Throwable -> 0x005f, all -> 0x00b5 }
        if (r6 == 0) goto L_0x007e;
    L_0x007b:
        r6.close();	 Catch:{ Throwable -> 0x0081, all -> 0x006e }
    L_0x007e:
        if (r11 == 0) goto L_0x008f;
    L_0x0080:
        throw r11;	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
    L_0x0081:
        r11 = move-exception;
        goto L_0x007e;
    L_0x0083:
        r13 = move-exception;
        if (r11 != 0) goto L_0x0088;
    L_0x0086:
        r11 = r13;
        goto L_0x006b;
    L_0x0088:
        if (r11 == r13) goto L_0x006b;
    L_0x008a:
        r11.addSuppressed(r13);	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        goto L_0x006b;
    L_0x008e:
        throw r10;	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
    L_0x008f:
        if (r17 == 0) goto L_0x000e;
    L_0x0091:
        com.android.timezone.distro.FileUtils.makeWorldReadable(r4);	 Catch:{ Throwable -> 0x002a, all -> 0x006e }
        goto L_0x000e;
    L_0x0096:
        if (r9 == 0) goto L_0x009b;
    L_0x0098:
        r9.close();	 Catch:{ Throwable -> 0x009e }
    L_0x009b:
        if (r12 == 0) goto L_0x00ac;
    L_0x009d:
        throw r12;
    L_0x009e:
        r12 = move-exception;
        goto L_0x009b;
    L_0x00a0:
        r12 = move-exception;
        if (r11 != 0) goto L_0x00a5;
    L_0x00a3:
        r11 = r12;
        goto L_0x0036;
    L_0x00a5:
        if (r11 == r12) goto L_0x0036;
    L_0x00a7:
        r11.addSuppressed(r12);
        goto L_0x0036;
    L_0x00ab:
        throw r10;
    L_0x00ac:
        return;
    L_0x00ad:
        r10 = move-exception;
        r11 = r12;
        goto L_0x0031;
    L_0x00b0:
        r10 = move-exception;
        goto L_0x002c;
    L_0x00b3:
        r10 = move-exception;
        goto L_0x0066;
    L_0x00b5:
        r10 = move-exception;
        r5 = r6;
        goto L_0x0066;
    L_0x00b8:
        r10 = move-exception;
        goto L_0x0061;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.timezone.distro.TimeZoneDistro.extractZipSafely(java.io.InputStream, java.io.File, boolean):void");
    }
}
